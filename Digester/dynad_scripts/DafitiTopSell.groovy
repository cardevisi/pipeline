import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
	
	public static boolean _DEBUG_ = false;
	
	//public static String ISO2UTF8(s) {return new String(s, "ISO-8859-1").getBytes("UTF-8");}
	public static String ISO2UTF8(s) {String a = new String(s); return new String(a.getBytes("UTF8"));}

	private static  buscaTopSell (Boolean forceDownload ) { 
		//Connection conn = ConnHelper.get().reopen();
		String address = 'http://www.dafiti.com.br/recommendation/recommendation/?c=index&a=index&category=0&brand=0&color=0&price=0&upper_material=0&sole_material=0&inner_material=0&heel_shape=0&heel_height=0&trodden_type=0&gender=0&prod=0&new_products=0&special-price=0&YII_CSRF_TOKEN=0259ef3eaba409ca399e414c83a1fb048b4c7baa&page=1';
		String tmp = new URL(address).getText('ISO-8859-1');
		def lista = tmp.split("data-request");

		def primeiro = true
		def segundo = false
		def terceiro = false

		lista.each { linha ->
			if(!primeiro) {
				if(linha.indexOf("sku") != -1) {
					def sub = linha.substring(linha.indexOf("sku") + 4);
					sub = sub.substring(0, sub.indexOf("&"));
					if(segundo) {
						segundo = false;
						terceiro = true;
						println("update personalization set return_by_default = null ;");
					}

					println("update personalization set return_by_default = true where id in ( select personalization_id from value where value = '" + sub + "' and label = 'SKU_1' );");
					//PreparedStatement pStmt2 = conn.prepareStatement("update personalization set return_by_default = '1' where id in ( select personalization_id from value where value = ? and label = 'SKU_1' )");
					//pStmt2.setString(1, sub);
					//pStmt2.executeUpdate();
				}
			} else { 
				primeiro = false; 
				segundo = true; 
			}
		}
		//conn.close();
		if(!primeiro && terceiro)
			println("update campaign set version = version + 1;");
	}
	
	
	public static void main (String [] args ) {
		/**
		 * Carrega todo o catálogo disponível no site da Dafiti. 
		 */
		buscaTopSell();
		
		/**
		 * Verifica todos os produtos cadastrados na tabela catalogo com o campo recomendacoes em branco
		 */
		//atualizarRecomendacoes();
	}
	
}
