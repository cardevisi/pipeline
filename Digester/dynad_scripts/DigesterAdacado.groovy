import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterDafitiBrasil {
	static int key;
	static {
		ConnHelper.schema = "dynad_adacado";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

/*
<ad><id><![CDATA[1022020]]></id>
<title><![CDATA[Auxiliar de vendas ]]></title>
<url><![CDATA[http://clicklogger.rm.uol.com.br/?oper=11&prd=21&grp=src:140;size:86&msr=Cliques%20de%20Origem:1&redir=http://empregocerto.uol.com.br/search/jobView.html?id=1022020]]></url>
<content><![CDATA[Auxiliar de vendas  em SAO PAULO. Experiência em licitações públicas, equipamentos de proteção, sinalização e bombeiros.]]></content>
<city><![CDATA[SAO PAULO]]></city>
<region><![CDATA[SAO PAULO]]></region>
<salary><![CDATA[A combinar]]></salary>
<company><![CDATA[Confidencial]]></company>
<date><![CDATA[04/08/2014]]></date>



<category><![CDATA[Comercial e Vendas]]></category>

<time><![CDATA[12:00]]></time>
<working_hours><![CDATA[Comercial]]></working_hours>
</ad>

*/

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;
		String recomendacao1, recomendacao2, recomendacao3, recomendacao4, recomendacao5, recomendacao6, recomendacao7, recomendacao8, recomendacao9, recomendacao10;
		String content, city, region, salary, company, date, time, working_hours;

	        def rss = new XmlParser().parse(file);
		def list = rss.ad
        
		list.each {
			sku = it.id.text();
			nome = it.title.text()
			//nome = Digester.stripTags(it.name.text()) 
       			url = it.url.text()
			if(url.indexOf("?")>-1) url = url.substring(0, url.indexOf("?"));
			content = it.content.text();
			city = it.city.text();
			region = it.region.text();
			salary = it.salary.text();
			company = it.company.text();
			date = it.date.text();
			time = it.time.text();
			working_hours = it.working_hours.text();
			categoria = it.category.text();

println("----->" + sku + ";" + nome + ";" + url + ";");
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			//markXml( '/mnt/XML/dafiti.xml' );
			markXml( '/tmp/adacado.xml' );
			
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), true, 21);
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
			Digester.topSellersPorGenero(ConnHelper.get().reopen(), 'categoria3', ['masculino': "'masculino'", 'feminino' : "'feminino'", 'infantil' : "'infantil feminino', 'infantil masculino'", 'unissex' : "'unissex'"]);
			//Dafiti.populaSkusNaCategoria();
                        Digester.validacao(ConnHelper.get().reopen());
                        
		} else
                if(args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
			Digester.topSellersPorGenero(ConnHelper.get().reopen(), 'categoria3', ['masculino': "'masculino'", 'feminino' : "'feminino'", 'infantil' : "'infantil feminino', 'infantil masculino'", 'unissex' : "'unissex'"]);
                }
		DigesterV2.completaRecomendacoesDafiti(ConnHelper.get().reopen());
    	}
}


