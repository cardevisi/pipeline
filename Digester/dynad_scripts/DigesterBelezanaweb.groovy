import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([
  	@Grab(group='org.apache.poi', module='poi', version='3.7'),
  	@Grab(group='commons-codec', module='commons-codec', version='1.6'),
  	@Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  	@Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  	@GrabConfig(systemClassLoader=true)
])

class DigesterBelezanaweb {
    static int key;
    static {
    	ConnHelper.schema = "dynad_belezanaweb";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
            key = res.getInt(1);
        	ConnHelper.closeResources(pStmt, res);
    }

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;
		def rss = new XmlParser().parse(file);
		def list = rss.channel.item
        
        /*
		<item>
			<title>Loreal Profesionnel Liss Ultime - Shampoo 1500ml</title>
			<link>http://www.belezanaweb.com.br/loreal-professionnel/loreal-profesionnel-serie-expert-liss-ultime-shampoo-1500ml/?utm_source=_base_&amp;amp;utm_medium=emailmkt;utm_campaign=asapcode</link>
			<description>Shampoo nutritivo para cabelos rebeldes.Ã‚Â Domina, controla e suaviza os fios volumosos. Desembaraca e hidrata. Cabelos disciplinados, maciosÃ‚Â e brilhantes.</description>
			<g:id>25</g:id>
			<g:condition>new</g:condition>
			<g:price>187,9</g:price>
			<g:sale_price>187,9</g:sale_price>
			<g:image_link>http://www.belezanaweb.com.br/imagens/4/loreal-profesionnel-liss-ultime-shampoo-1500ml-25.jpg</g:image_link>
			<g:brand>Loreal Professionnel</g:brand>
			<g:quantity>1</g:quantity>
			<g:google_product_category>cabelos</g:google_product_category>
			<g:product_type>Shampoos</g:product_type>
			<g:product_review_average>92</g:product_review_average>
			<g:product_review_count>15</g:product_review_count>
			<g:installment>
				<g:months>6x</g:months>
				<g:amount>31,32</g:amount>
			</g:installment>
		</item>
		*/
		list.each {		
	        sku = it.'g:id'.text()
	        nome = Digester.stripTags(Digester.ISO2UTF8(it.title.text()))
			descricao = Digester.stripTags(Digester.ISO2UTF8(it.description.text()))
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
			
			if( descricao != null && descricao.length() > 256 )
				descricao = descricao.substring(0, 253) + '...';
			
	        url = it.link.text()
			if(url.indexOf("?") > -1)
				url = url.substring(0, url.indexOf("?"));

			fprice = it.'g:sale_price'.text()
			if(fprice.indexOf(",") > -1) {
				int tam = fprice.indexOf(",") + 3;
				if(tam > fprice.length()) tam = fprice.length();
				fprice = fprice.substring(0, tam);
			}
	        fprice = Digester.autoNormalizaMoeda(fprice, false, new Locale("pt", "BR"));


			oprice = it.'g:price'.text()
			if(oprice.indexOf(",") > -1) {
				int tam = oprice.indexOf(",") + 3;
				if(tam > oprice.length()) tam = oprice.length();
				oprice = oprice.substring(0, tam);
			}
			
	        oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"));


	        def tmp = it.'g:installment'.'g:months'.text();
			
			if(tmp.indexOf('x') > 0) nparcelas = tmp.substring(0, tmp.indexOf('x'));
			else nparcelas = tmp;
	        
			vparcelas = it.'g:installment'.'g:amount'.text()
			if(vparcelas.indexOf(",") > -1) {
				int tam = vparcelas.indexOf(",") + 3;
				if(tam > vparcelas.length()) tam = vparcelas.length();

				vparcelas = vparcelas.substring(0, tam);
			}

	        vparcelas = Digester.autoNormalizaMoeda(vparcelas, false, new Locale("pt", "BR"));

			image = it.'g:image_link'.text()

	     	c1 = it.'g:google_product_category'.text()
	     	c2 = "";
	     	c3 = "";

	     	marca = it.'g:brand'.text()

			fprice = fprice==null||fprice==""?"0,00":fprice;
			oprice = oprice==null||oprice==""?"0,00":oprice;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;
	        
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1 + '/' + c2, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA') );

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}	
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";
		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;
			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}
		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}
		return retorno.replace("%20", " ");
	}
	
	public static void recomendacoesMaisVendidos ( String file ) { 
		java.io.File f = new java.io.File( file ).getParentFile();
    	java.io.File fMaisVendidos = new java.io.File( f, "belezanaweb_mais_vendidos.xml" ); 
    	
    	ResultSet res;
    	PreparedStatement stmt;
    	Connection conn = ConnHelper.get().reopen();
    	
    	java.util.Map<String, java.util.List<String>> mapMaisVendidos = new java.util.HashMap();
    	def feedMaisVendidos = new XmlParser().parse(fMaisVendidos);
        def listMaisVendidos = feedMaisVendidos.produto
		listMaisVendidos.each {	
        	String nomeRec, skuRec, catRec;
        	nomeRec = it.nome.text();
        	skuRec = it.sku.text();
        	catRec = it.categoria.text();
        	
        	boolean produtoValido = true;
        	stmt = conn.prepareStatement("SELECT id_sku, ativo FROM catalogo WHERE sku = '" + skuRec + "'");
        	res = stmt.executeQuery();
        	if( res.next() ) { 
        		if( !"1".equals( res.getString(2) ) ) { 
        			println("***** WARNING: SKU inativo: " + skuRec);
        			produtoValido = false;
        		}
        	} else { 
        		println("***** WARNING: SKU invalido: " + skuRec);
        		produtoValido = false;
        	}
        	ConnHelper.closeResources( stmt, res );
        	
        	if( produtoValido ) { 
	        	java.util.List<String>listSks = null;
	        	if( !mapMaisVendidos.containsKey( catRec ) ) { 
	        		listSks = new java.util.ArrayList();
	        		mapMaisVendidos.put( catRec, listSks );
	        	} else { 
	        		listSks = mapMaisVendidos.get( catRec );
	        	}
	        	if( listSks.size() < 20 )
	        		listSks.add( skuRec );
        	}
        	
        }
        
        for(Map.Entry<String, java.util.List<String>> r : mapMaisVendidos.entrySet() ) { 
        	print( 'categoria ' + r.getValue().size() + ': ' + r.getKey() + ' -> ' );
        	if( r.getValue().size() < 10 ) { 
        		println("nao possui minimo de 10 produtos [" + r.getValue().size() + "]");
        		continue;
        	}
        	StringBuilder sbRecomendacoes = new StringBuilder();
        	for(String sku : r.getValue() ) { 
        		print( sku + ',' );
        		if( sbRecomendacoes.length() > 0 )
        			sbRecomendacoes.append(",");
        		sbRecomendacoes.append(sku);
        	}
        	println('');        	
        	String match = "'" + r.getKey().toLowerCase() + "'";
        	if( r.getKey().equals("corpo-e-banho") ) match = "'corpo-e-banho', 'Corpoebanho'".toLowerCase();	
        	else if( r.getKey().equals("cuidados-para-pele") ) match = "'cuidados-para-pele', 'Cuidadosparapele'".toLowerCase();	        	
        	stmt = conn.prepareStatement("UPDATE catalogo SET recomendacoes = '" + sbRecomendacoes.toString() + "' WHERE lower(categoria1) in (" + match + ")");
        	int registros = stmt.executeUpdate();
        	if( registros == 0 ) println("nenhum produto atualizado: " + r.getKey());
        	else println("atualizou " + registros + " recomendacoes para a categoria: " + r.getKey());
        }
        println( mapMaisVendidos.keySet() );
        return;
	}
	  
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/belezanaweb.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			recomendacoesMaisVendidos ( '/mnt/TMP/xml/belezanaweb_mais_vendidos.xml' );
			DigesterV2.completaRecomendacoesWithMetrics( ConnHelper.get().reopen(), "1452535340953", "recomendacoes");
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.DIFFERENT_LINK);
    		Digester.bestSellers(ConnHelper.get().reopen());
    		Digester.validacao(ConnHelper.get().reopen());
    	} else if(args[0] == 'BEST_SELLERS') {
        	Digester.bestSellers(ConnHelper.get().reopen());
        }

		///////// EXPORT UOL CLIQUES ///////////
		java.util.List<String> listTopSellers = new java.util.ArrayList();
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		ResultSet res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/belezanaweb_uolcliques.txt").withWriter { out ->
			out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while( res.next() ) {
				out.print(res.getString(1) + "|Beleza na Web|" + res.getString(5) + "|");
				if( res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1 )
					out.print('A partir de R$ ' + res.getString(8));
				else
					out.print(res.getString(9) + 'x de R$ ' + res.getString(10));
				out.print('|' + res.getString(6) + '?utm_source=UOL&utm_medium=display&utm_content=sku&utm_campaign=retargeting-uol-cliques|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3)) + '|' + res.getString(11) + '\n');
				out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
				if( listTopSellers.size() < 10 ) listTopSellers.add( res.getString(1) );
			}
		}
		ConnHelper.closeResources(pStmt, res);

		///////// EXPORT UOL CLIQUES TOPSELLERS ///////////
		pStmt = conn.prepareStatement("select a.sku from catalogo a join best_sellers b on a.id_sku = b.id_sku where a.ativo = '1' order by b.pos");
        res = pStmt.executeQuery();
        int numBsDb = 0;
        new File("/mnt/TMP/sna-files/belezanaweb_uolcliques_topsellers.txt").withWriter { out ->
            while (res.next()) {
            	out.println( res.getString(1) ); numBsDb++;
            }
            if( numBsDb < 20 ) {
                listTopSellers.each { itTS -> out.println( itTS ); }
            }
        }
        ConnHelper.closeResources(pStmt, res);

        ///////// EXPORT UOL BARRA DE OFERTAS ///////////
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
        res = pStmt.executeQuery();
        new File("/mnt/TMP/sna-files/belezanaweb_barra.txt").withWriter { out ->
			out.print("metadata|sku|nome|ativo|preco_promocional|numero_parcelas|valor_parcelas|link|tracking_barra|imagem|recomendacoes\n");
            while( res.next() ) {
                out.print(res.getString(2) + "|" + res.getString(5) + "|" + res.getString(4) + "|" + res.getString(8) + "|");
                if( res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1 ) out.print(res.getString(9) + "|" + res.getString(10));
                else out.print("1|" + res.getString(8));
				out.print("|" + getValidParameters( res.getString(6), null) + "|utm_source=UOL&utm_medium=display&utm_content="+res.getString(1)+ "&utm_campaign=retargeting");
				out.print("|http://static.dynad.net/let/" + URLCodec.encrypt(res.getString(3)) + "|" + res.getString(11) + "\n"); 	
                out.print("_"+res.getString(1)+"|"+res.getString(2)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);

        ///////// EXPORT SUPERNOVA 2 ///////////
        conn = ConnHelper.get().reopen();
        pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/belezanaweb.txt").withWriter { out ->
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);
    }

	private static String removeAcentos (String string){
		string = string.replaceAll("[ÂÀÁÄÃ]","A");
		string = string.replaceAll("[âãàáä]","a");
		string = string.replaceAll("[ÊÈÉË]","E");
		string = string.replaceAll("[êèéë]","e");
		string = string.replaceAll("ÎÍÌÏ","I");
		string = string.replaceAll("îíìï","i");
		string = string.replaceAll("[ÔÕÒÓÖ]","O");
		string = string.replaceAll("[ôõòóö]","o");
		string = string.replaceAll("[ÛÙÚÜ]","U");
		string = string.replaceAll("[ûúùü]","u");
		string = string.replaceAll("Ç","C");
		string = string.replaceAll("ç","c");
		string = string.replaceAll("[ýÿ]","y");
		string = string.replaceAll("Ý","Y");
		string = string.replaceAll("ñ","n");
		string = string.replaceAll("Ñ","N");
		return string;
    }

    private static String getStringForTracking( String conteudo ) {
		if( conteudo == null ) return null;
		conteudo = removeAcentos( conteudo );
		conteudo = Normalizer.normalize( conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]","-").toLowerCase();
    }
        
        
	private static String getValidParameters ( String productURL, String [] arrSkipParameters ) {
		StringBuilder sb = new StringBuilder();
		String urlParam = productURL.indexOf('?') > -1 ? ( productURL.substring( productURL.indexOf('?') + 1 ) ) : "";
		if( urlParam.trim().equals("") ) {
			if( productURL.indexOf('?') > -1 && !productURL.endsWith('&') ) productURL += '&';
        	else if( productURL.indexOf('?') == -1 ) productURL += '?';
			return productURL;
		}

		productURL = productURL.substring(0, productURL.indexOf('?'));
		for( String paramEntry : urlParam.split("&") ) {
			String paramName = paramEntry.substring(0, ( paramEntry.indexOf("=") > 0 ? paramEntry.indexOf("=") : 0 ) );
			String paramValue = ( paramEntry.indexOf("=") > 0 ? paramEntry.substring( paramEntry.indexOf("=") + 1 ) : "" );
			if( paramName.startsWith("utm_") ) continue;
			boolean found = false;
			for(String skipParamName : arrSkipParameters ) {
				if( skipParamName.trim().toLowerCase().equals( paramName.trim().toLowerCase() ) ) {
					found = true;
					break;
				}
			}
			if( !found ) {
				if(sb.length() > 0 ) sb.append('&');
				sb.append( paramName );
				if( paramValue != '' ) sb.append('=').append( paramValue );
			}
		}
		if( sb.length() > 0 ) productURL += "?" + sb.toString();
		if( productURL.indexOf('?') > -1 && !productURL.endsWith('&') ) productURL += '&';
		else if( productURL.indexOf('?') == -1 ) productURL += '?';
		return productURL;
	}
}