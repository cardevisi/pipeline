import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import java.text.Normalizer

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterCatho {
    static int key;
    static {
        ConnHelper.schema = "dynad_catho";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
                key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {		
        Connection conn = ConnHelper.get().reopen();
		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, nome, descricao, url, salario;
		String c1, c2, c3, c4, c5, vagas;				
		String estado, regiao, pais, cidade;
		Float latitude, longitude

        def summary = new XmlParser().parse(file);
        def list = summary.job

	
		Map<String, String> siglasEstados = new HashMap<String, String>();
		siglasEstados.put("Acre", "AC");
		siglasEstados.put("Alagoas", "AL");
		siglasEstados.put("Amapá", "AP");
		siglasEstados.put("Amazonas", "AM");
		siglasEstados.put("Bahia", "BA");
		siglasEstados.put("Ceará", "CE");
		siglasEstados.put("Distrito Federal", "DF");
		siglasEstados.put("Espírito Santo", "ES");
		siglasEstados.put("Goiás", "GO");
		siglasEstados.put("Maranhão", "MA");
		siglasEstados.put("Mato Grosso", "MT");
		siglasEstados.put("Mato Grosso do Sul", "MS");
		siglasEstados.put("Minas Gerais", "MG");
		siglasEstados.put("Pará", "PA");
		siglasEstados.put("Paraíba", "PB");
		siglasEstados.put("Paraná", "PR");
		siglasEstados.put("Pernambuco", "PE");
		siglasEstados.put("Piauí", "PI");
		siglasEstados.put("Rio de Janeiro", "RJ");
		siglasEstados.put("Rio Grande do Norte", "RN");
		siglasEstados.put("Rio Grande do Sul", "RS");
		siglasEstados.put("Rondônia", "RO");
		siglasEstados.put("Roraima", "RR");
		siglasEstados.put("Santa Catarina", "SC");
		siglasEstados.put("São Paulo", "SP");
		siglasEstados.put("Sergipe", "SE");
		siglasEstados.put("Tocantins", "TO");
	
        list.each {
        	sku = it.referencenumber.text();
        	nome = it.title.text().trim();
        	descricao = it.description.text().trim();
        	url = it.url.text();				
            if(url.indexOf("?") > -1) url = url.substring(0, url.indexOf("?"));
            salario = DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.salary.text(), false, new Locale("pt", "BR")), false, new Locale("pt", "BR"))	            
        	pais = it.country.text()
        	if (it.locations.location[0]) {
        		if (it.locations.location[0].state)
        			estado = it.locations.location[0].state.text()
        		if (it.locations.location[0].city)
        			cidade = it.locations.location[0].city.text()			
        	}        		
			
			if (it.classifications.classification[0])
				c1 = it.classifications.classification[0].text();						
			if (it.classifications.classification[1])
				c2 = it.classifications.classification[1].text();
			if (it.classifications.classification[2])
				c3 = it.classifications.classification[2].text();
			if (it.levels.level[0])
				c4 = it.levels.level[0].text();
			if (it.levels.level[1])
				c5 = it.levels.level[1].text();
			vagas = it.instock.text();

			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: (estado != null && estado != '' ? '1' : '0'), lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2 + '/' + c3), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria4', columnType: 'string', columnValue: c4, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria5', columnType: 'string', columnValue: c5, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );			
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: salario, lookupChange: true, platformType: 'NA') );				
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: salario, lookupChange: true, platformType: 'NA') );				
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: '1', lookupChange: true, platformType: 'NA') );				
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: salario, lookupChange: true, platformType: 'NA') );				
			fields.add( new FieldMetadata(columnName : 'pais', columnType: 'string', columnValue: pais, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'estado', columnType: 'string', columnValue: estado, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'cidade', columnType: 'string', columnValue: cidade, lookupChange: true, platformType: 'NA') );								
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}		
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/catho.xml' );
        	DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
        	DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
        	DigesterV2.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.GEO_LOCATION_DIFFERENT_CITY);
        	//DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
        	//DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		} else
		if(args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen(), true);
			//DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		}

		///////// EXPORT UOL CLIQUES ///////////
		java.util.List<String> listTopSellers = new java.util.ArrayList();
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		ResultSet res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/catho_uolcliques.txt").withWriter { out ->
			out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while( res.next() ) {
				out.print(res.getString(1) + "|Catho|" + res.getString(5) + "|");
				if( res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1 )
					out.print('A partir de R$ ' + res.getString(8));
				else
					out.print(res.getString(9) + 'x de R$ ' + res.getString(10));
				out.print('|' + res.getString(6) + '?utm_source=UOL&utm_medium=display&utm_content=sku&utm_campaign=retargeting-uol-cliques|http://siga.imguol.com/srv/LINKS/3008/c4214f0ec699416b9e0f9857ee83347b.jpeg' + '|' + res.getString(11) + '\n');
				out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
				if( listTopSellers.size() < 10 ) listTopSellers.add( res.getString(1) );
			}
		}
		ConnHelper.closeResources(pStmt, res);

		///////// EXPORT SUPERNOVA 2 ///////////
        conn = ConnHelper.get().reopen();
        pStmt = conn.prepareStatement("select sku, id, ativo, nome, link, preco_promocional, pais, estado, cidade, recomendacoes from catalogo where sku is not null and id is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and pais is not null and estado is not null and cidade is not null");
        res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/catho.txt").withWriter { out ->
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));                
                for(int i=3; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);

	}	
}