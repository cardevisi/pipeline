import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import java.net.*;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterCent_tmp {
	
        static int key;
	public static boolean _DEBUG_ = false;
	public static int _NOVIDADE_PERIODO_ = 21;
	
	//public static String ISO2UTF8(s) {String a = new String(s); return new String(a.getBytes("UTF8"));}
	public static String ISO2UTF8(s) {String a = new String(s.getBytes("ISO-8859-1"), "ISO-8859-1"); return new String(a.getBytes("UTF8"), "UTF-8");}

	public static String stripTags(String s) {
		return s.replaceAll("<(.|\n)*?>", " ").replaceAll("\\s+", " ")
	}

	public static String cortaDecimal(String s, Boolean cutThousands) { return Digester.cortaDecimal(s, cutThousands, Locale.US, Locale.US); }
	public static String cortaDecimal(String s, Boolean cutThousands, Locale locale) { return Digester.cortaDecimal(s, cutThousands, locale, locale); }
	public static String cortaDecimal(String s, Boolean cutThousands, Locale inLocale, Locale outLocale) {
		if(s!= null && s!='') {
			s = Digester.normalizaMoeda(s, cutThousands, inLocale, outLocale);
			def p = s.indexOf(""+DecimalFormatSymbols.getInstance(outLocale).getDecimalSeparator());
			if(p>-1) s = s.substring(0, p);
		}
	
		return s;
	}

	public static String normalizaMoeda(String s, Boolean cutThousands) { Digester.normalizaMoeda(s, cutThousands, Locale.US, Locale.US); }
	public static String normalizaMoeda(String s, Boolean cutThousands, Locale locale) { Digester.normalizaMoeda(s, cutThousands, locale, locale); }
	public static String normalizaMoeda(String s, Boolean cutThousands, Locale inLocale, Locale outLocale) {
		if(s!="") {
			s = s.replaceAll("[^0-9,.]", "");
			if(s == "") { println("suspicius value:" + s); return "0"; }
			NumberFormat fmt = NumberFormat.getInstance(outLocale);
			fmt.setMaximumFractionDigits(2);
			fmt.setMinimumFractionDigits(2);
			s = fmt.format(NumberFormat.getInstance(inLocale).parse(s).doubleValue());
		}
		if(cutThousands) s = s.replaceAll((DecimalFormatSymbols.getInstance(outLocale).getGroupingSeparator() == '.' ? "\\." : new String(DecimalFormatSymbols.getInstance(outLocale).getGroupingSeparator())), '');
		return s;
	}
	public static String autoNormalizaMoeda(String s, Boolean cutThousands, Locale outLocale) {
		if(s!= null && s!='' && s.length() > 2) {
			s = s.replaceAll("[^0-9,.]", "");
			def decimalChar = "";

			if(s.length() > 2 && (s.charAt(s.length()-2) == ',' || s.charAt(s.length()-2) == '.' )) { decimalChar = s.charAt(s.length()-2); }
			else if(s.length() > 3 && (s.charAt(s.length()-3) == ',' || s.charAt(s.length()-3) == '.' )) { decimalChar = s.charAt(s.length()-3); }
			else if(s.length() > 4 && (s.charAt(s.length()-4) == ',' || s.charAt(s.length()-4) == '.' )) { decimalChar = s.charAt(s.length()-4)==','?'.':(s.charAt(s.length()-4)=='.'?',':''); }

			Locale lBR = new Locale('pt', 'BR');
			Locale lEN = new Locale('en', 'US');

			if(decimalChar == ""+DecimalFormatSymbols.getInstance(lBR).getDecimalSeparator()) return normalizaMoeda(s, cutThousands, lBR, outLocale);
			else if(decimalChar == ""+DecimalFormatSymbols.getInstance(lEN).getDecimalSeparator()) return normalizaMoeda(s, cutThousands, lEN, outLocale);
			else return normalizaMoeda(s, cutThousands, lEN, outLocale);
		}

		return s;
	}
	static void main(String []args) {
		println(autoNormalizaMoeda("7,33", true, Locale.US));
	}


	public static void populaCategoriaSiteRecomendacoes ( Connection conn ) {
		PreparedStatement pStmt = conn.prepareStatement("select id_categoria_site from sku_categoria_site group by id_categoria_site having count(*) >= 10");
		ResultSet res = pStmt.executeQuery();
		int cntSkus = 0;
		while( res.next() ) {
			Integer idCategoria = res.getInt(1);
			PreparedStatement pStmt2 = conn.prepareStatement("""select a.categoria, b.sku, b.recomendacoes, b.ativo
				from categoria_site a, catalogo b, sku_categoria_site c 
				where c.id_categoria_site = a.id AND c.id_sku = b.id_sku AND a.id = ?
				order by b.score DESC limit 10""");
			pStmt2.setInt(1, idCategoria);
			ResultSet res2 = pStmt2.executeQuery();
			String recomendacoes = "";
			String categoria = "";
			String recSku = "";
			cntSkus = 0;
			while( res2.next() ) {
				if(recomendacoes=="") { categoria = res2.getString(1); recSku = res2.getString(3) };
				if(res2.getString(4)=='1') {recomendacoes += (recomendacoes==""?"":",") + res2.getString(2); cntSkus++;}
			}
			for(int k = 10; k>0 && cntSkus < 10 && !recSku.equals(""); k-- ) {
				if(recSku.indexOf(",") > 0) { 
					def r = recSku.substring(0, recSku.indexOf(",")).trim(); 
					try{recSku = recSku.substring(recSku.indexOf(",") + 1); }catch(Exception ex){recSku = "";}
					if(recomendacoes.indexOf(r) == -1) {
						recomendacoes += (recomendacoes==""?"":",") + r; 
						cntSkus++;
					}
				} else { 
					recomendacoes += (recomendacoes==""?"":",") + recSku;
					cntSkus++;
					break;
				}
			}
			PreparedStatement pStmt4 = conn.prepareStatement("""select count(*) from arvore_categoria where id = ?""");
			pStmt4.setInt(1, idCategoria);
			ResultSet res4 = pStmt4.executeQuery();
			if( res4.next() ) {
				Integer cnt = res4.getInt(1);
				PreparedStatement pStmt3;
				if(cnt == 0) 
					pStmt3 = conn.prepareStatement("insert into arvore_categoria (categoria, recomendacoes, versao, id) values (?, ?, " + (long)System.currentTimeMillis()/1000 + ", ?)");
				else
					pStmt3 = conn.prepareStatement("update arvore_categoria set categoria = ?, recomendacoes = ?, versao = " + (long)System.currentTimeMillis()/1000 + " where id = ?");
				pStmt3.setString(1, URLDecoder.decode(categoria, "UTF-8"));
				pStmt3.setString(2, recomendacoes);
				pStmt3.setInt(3, idCategoria);
				pStmt3.executeUpdate();
				ConnHelper.closeResources(pStmt3, null);
				print(".");
			}
			ConnHelper.closeResources(pStmt4, res4);
			ConnHelper.closeResources(pStmt2, res2);
		}
		ConnHelper.closeResources(pStmt, res);
	}

	public static int salvaCategoriaSite ( Connection conn, category, sku ) {
		int retorno = 0;
		PreparedStatement pStmt = conn.prepareStatement("select id_sku from catalogo where sku = ?");
		pStmt.setString(1, sku);
		ResultSet res = pStmt.executeQuery();
		if( res.next() ) {
			Integer idSku = res.getInt(1);
			PreparedStatement pStmt3 = conn.prepareStatement("select id from categoria_site where categoria = ?");
			pStmt3.setString(1,category);
		        ResultSet res3 = pStmt3.executeQuery();
               		if(res3.next()) {
				Integer idCategory = res3.getInt(1);
				PreparedStatement pStmt2 = conn.prepareStatement("select count(*) from sku_categoria_site where id_sku = ? and id_categoria_site = ?");
				pStmt2.setInt(1, idSku);
				pStmt2.setInt(2, idCategory);
				ResultSet res2 = pStmt2.executeQuery();
				if( !res2.next() || res2.getInt(1) == 0) {
					PreparedStatement pStmt4 = conn.prepareStatement("insert into sku_categoria_site(id_sku, id_categoria_site) values(?, ?)");
					pStmt4.setInt(1, idSku);
					pStmt4.setInt(2, idCategory);
					retorno = pStmt4.executeUpdate();
					ConnHelper.closeResources(pStmt4, null);
				}
				ConnHelper.closeResources(pStmt2, res2);
			} else {
				println("X");
			}
			ConnHelper.closeResources(pStmt3, res3);
		} else {
			print("x");
		}
		ConnHelper.closeResources(pStmt, res);
		return retorno;
	}

	public static void categoriasSite (Connection conn, Closure callback) {
		PreparedStatement pStmt = conn.prepareStatement("select max(last_update) - interval 10 day from categoria_site");
                ResultSet res = pStmt.executeQuery();
		Date lastUpdate = new Date(0);

                if( res.next() )
			lastUpdate = res.getDate(1) != null ? res.getDate(1) : lastUpdate;

		ConnHelper.closeResources(pStmt, res);

		PreparedStatement pStmt2 = conn.prepareStatement("select max(date), category from pre_category where category not in (select categoria from categoria_site) and score > 0 group by category");
		//PreparedStatement pStmt2 = conn.prepareStatement("select max(date), category from pre_category where date > ? and category not in (select categoria from categoria_site) group by category");
		//pStmt2.setDate(1, new java.sql.Date(lastUpdate.getTime()));
		ResultSet res2 = pStmt2.executeQuery();
		def date = null;
		def category = null;
		while(res2.next()) {
			date = res2.getDate(1);
			category = res2.getString(2);
		
			if(category != null) {
				PreparedStatement pStmt3 = conn.prepareStatement("select count(*) from categoria_site where categoria = ?");
				pStmt3.setString(1, category);
				ResultSet res3 = pStmt3.executeQuery();
				if(res3.next()) {
					def cnt = res3.getInt(1);
					if(cnt == 0) {
						PreparedStatement pStmt4 = conn.prepareStatement("insert into categoria_site (categoria, last_update) values (?, ?)");
						pStmt4.setString(1, category);
						pStmt4.setDate(2, new java.sql.Date(new Date().getTime()));
						pStmt4.executeUpdate();
						ConnHelper.closeResources(pStmt4, null);
					}
					if(callback != null) {
						callback(category);
					}
				}
				ConnHelper.closeResources(pStmt3, res3);
			}
		}
		ConnHelper.closeResources(pStmt2, res2);
	}

        public static void salvaCatalogo (Connection conn, String sku, String c1, String c2, String c3,
                String imagem, String precoOriginal, String precoPromocional, String numeroParcelas, String valorParcelas,
                String link, String marca, String nome, String codigo, String ativo, Boolean encodeImage) {
        	salvaCatalogo (conn, sku, c1, c2, c3, imagem, precoOriginal, precoPromocional, numeroParcelas, valorParcelas, link, marca, nome, codigo, ativo, encodeImage, null);
	}
        public static void salvaCatalogo (Connection conn, String sku, String c1, String c2, String c3,
                String imagem, String precoOriginal, String precoPromocional, String numeroParcelas, String valorParcelas,
                String link, String marca, String nome, String codigo, String ativo, Boolean encodeImage, String gratis) {
                def cnt;
                String recomendacoes;
                PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where sku = ?");
                pStmt.setString(1, sku);
                ResultSet res = pStmt.executeQuery();

		def categoria = '';
		categoria += c1 != null && c1 != '' ? c1 : '';
		categoria += c2 != null && c2 != '' ? '_'+c2 : '';
		categoria += c3 != null && c3 != '' ? '_'+c3 : '';

		ativo = (ativo=='sim'||ativo=='1')?'1':'0';

                if( res.next() )
                        cnt = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);

		if(encodeImage && imagem != null && imagem != '')
	                //imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=o8vtEg01qmUcZL-H_Dgv6Q"; //s=200x200
	                imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=ElXEFX1LcBgiF7zWSd1M-A"; //f=180x180&cr=3

                int col = 1;
                String stmt = "";

		def selo = 'novidade';
		def desconto = '';
		gratis = null;

                if( cnt == 0) {
			if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
				selo = 'desconto';
				desconto = Math.floor( ( 1 - ( Double.parseDouble(Digester.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(Digester.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
			}

                        stmt = """insert into catalogo ( 
		id, categoria, categoria1, categoria2, categoria3, nome, imagem, link, preco_original, preco_promocional,
		numero_parcelas, valor_parcelas, marca, status, codigo, recomendacoes, ativo, versao, selo, desconto, """ +(gratis!=null?"gratis, ":"")+ """sku, last_update, created_in) 
	select ifnull(max(id), 0) + 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now() from catalogo """;

                } else {
			desconto = '';
			if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
				selo = 'desconto';
				desconto = Math.floor( ( 1 - ( Double.parseDouble(Digester.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(Digester.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
			}

                        stmt = """update catalogo set
		categoria = ?,
                categoria1 = ?,
                categoria2 = ?,
                categoria3 = ?,
                nome = ?,
                imagem = ?,
                link = ?,
                preco_original = ?,
                preco_promocional = ?,
                numero_parcelas = ?,
                valor_parcelas = ?,
                marca = ?,
                status = ?,
		codigo = ?,
                recomendacoes = ?,
                ativo = if(ativo <> ?, if(ativo = '1', '9', '1'), ativo),
                versao = ?,
		selo = ?,
		desconto = ?,
		""" +(gratis!=null?"gratis = ?, ":"")+ """
		last_update = now()
        where sku = ? AND (
                        nome <> ? OR
                        imagem <> ? OR
                        link <> ? OR
                        preco_original <> ? OR
                        preco_promocional <> ? OR
                        valor_parcelas <> ? OR
                        categoria <> ? OR
                        ativo <> ? )""";
                }


                pStmt = conn.prepareStatement(stmt.replaceAll("\\n", " "));
		def idx = 1;

                try {
			pStmt.setString(idx++, categoria) ;
			pStmt.setString(idx++, c1) ;
			pStmt.setString(idx++, c2) ;
			pStmt.setString(idx++, c3) ;
			pStmt.setString(idx++, nome) ;
			pStmt.setString(idx++, imagem) ;
			pStmt.setString(idx++, link) ;
			pStmt.setString(idx++, precoOriginal) ;
			pStmt.setString(idx++, precoPromocional) ;
			pStmt.setString(idx++, numeroParcelas) ;
			pStmt.setString(idx++, valorParcelas) ;
			pStmt.setString(idx++, marca) ;
			pStmt.setString(idx++, '1') ;
			pStmt.setString(idx++, codigo) ;
			pStmt.setString(idx++, recomendacoes) ;
			pStmt.setString(idx++, ativo) ;
			pStmt.setLong(idx++, (long)System.currentTimeMillis()/1000) ;
			pStmt.setString(idx++, selo) ;
			pStmt.setString(idx++, "" + desconto) ;
			if(gratis != null)
				pStmt.setString(idx++, "" + gratis) ;
			pStmt.setString(idx++, sku) ;


			if(stmt.toLowerCase().trim().startsWith("update") ) {
				pStmt.setString(idx++, nome) ;
				pStmt.setString(idx++, imagem) ;
				pStmt.setString(idx++, link) ;
				pStmt.setString(idx++, precoOriginal) ;
				pStmt.setString(idx++, precoPromocional) ;
				pStmt.setString(idx++, valorParcelas) ;
				pStmt.setString(idx++, categoria) ;
				pStmt.setString(idx++, ativo) ;
			}

			def ret = pStmt.executeUpdate();
			ConnHelper.closeResources(pStmt, null);

			if(stmt.toLowerCase().trim().startsWith("insert") )
				println(sku + " -> ..N.O.V.O... -> " + nome);
			else if(ret > 0)
				println(sku + " -> .ATUALIZADO. -> " + nome);
			else {
				println(sku + " -> ....none.... -> " + nome);
				stmt = "update catalogo set last_update = now(), selo = if(preco_original <> preco_promocional, 'desconto', if(DATEDIFF(now(), created_in) <= " + _NOVIDADE_PERIODO_ + ", 'novidade', '')) where sku = ?";
				pStmt = conn.prepareStatement(stmt);
				pStmt.setString(1, sku) ;
				pStmt.executeUpdate();
				ConnHelper.closeResources(pStmt, null);
			}


                } catch(ex) {
                        println("erro: [" + sku + "]");
                        ex.printStackTrace();
                }

        }

	private static void inativaForaDoCatalogo(Connection conn) { Digester.inativaForaDoCatalogo(conn, false); }
	private static void inativaForaDoCatalogo(Connection conn, Boolean all) {
		print("iniciando inativacao de produtos ... \n");
		int _DAYS_HAS_DEACTIVATED_ = 0;
		def ret = 0;
		int cnt=0, printou=0, size=0;

		// MARCA COMO TEMPORARIAMENTE INATIVOS REGISTROS QUE SERAO INATIVADOS
		if(!all) {
			PreparedStatement pStmt = conn.prepareStatement("""update catalogo set ativo = '9', recomendacoes = '' where datediff(date_sub(curdate(),interval """ + _DAYS_HAS_DEACTIVATED_ + """ day), ifnull(last_update, '1970-01-01')) >= 1 AND ativo = '1'""");
			ret = pStmt.executeUpdate();
			println(ret);
			ConnHelper.closeResources(pStmt, null);
		}

		if(1==1) { //if(ret > 0 || all) {

                	PreparedStatement pStmt2 = conn.prepareStatement("select sku from catalogo where " + (all?"ativo<>'1'":"ativo='9'"));
                	ResultSet res2 = pStmt2.executeQuery();

			PreparedStatement pTmp = null;
			print("criando tab temporaria ... ");
			pTmp = conn.prepareStatement("""drop table if exists catalogo_tmp""");
			pTmp.executeUpdate();
			pTmp = conn.prepareStatement("""create table catalogo_tmp(id int(8), sku varchar(45), status char(1), INDEX(id), INDEX(sku), INDEX(status)) engine=memory""");
//			pTmp = conn.prepareStatement("""
//				create table catalogo_tmp(INDEX(id), INDEX(status), INDEX(recomendacoes)) engine=memory 
//				select id, recomendacoes, 0 as status from catalogo""");
			pTmp.executeUpdate();
			println("OK");
			print("populando tab temporaria ");
			PreparedStatement pInsTmp = null;
			pTmp = conn.prepareStatement("select id, recomendacoes from catalogo where recomendacoes is not null OR recomendacoes <> ''");
			pInsTmp = conn.prepareStatement("insert into catalogo_tmp(id, sku) values (?, ?)");
			ResultSet resTmp = pTmp.executeQuery();
			resTmp.beforeFirst();  
			resTmp.last();  
			size = resTmp.getRow();
			resTmp.beforeFirst();  
			cnt = 0;
			
			while (resTmp.next()) {
				String rec = resTmp.getString(2);
				String []skus = rec.split(",");
				for(String sku : skus) {
					pInsTmp.setInt(1, resTmp.getInt(1));
					pInsTmp.setString(2, sku);
					pInsTmp.executeUpdate();
				}
				if(cnt++%1000 == 0) { int a = cnt*100/size; if(a%10 == 0 && a != printou) { print("." + a + "%."); printou = a;} else print('.');}
			}

			def sku;
			cnt = 0; printou = 0;
			print("100%\nmarcando tab temporaria ");

			res2.beforeFirst();  
			res2.last();  
			size = res2.getRow();
			res2.beforeFirst();  

                	while( res2.next() ) {
				sku = res2.getString(1);
				PreparedStatement pStmt3 = conn.prepareStatement("update catalogo_tmp set status = 1 where sku = '" +sku+ "' and status is null");
				//PreparedStatement pStmt3 = conn.prepareStatement("update catalogo_tmp set status = 1 where recomendacoes regexp '" +sku+ "' AND status = 0");
				//PreparedStatement pStmt3 = conn.prepareStatement("update catalogo set recomendacoes = '' where id in ( select id from catalogo_tmp where recomendacoes regexp '" +sku+ "' ) ");
				ret = pStmt3.executeUpdate();
				//ret = pStmt3.executeQuery();
				if(ret>0) print(sku + ":(" + ret + ")");
				else if(cnt++%1000 == 0) { int a = cnt*100/size; if(a%10 == 0 && a != printou) { print("...." + a + "%...."); printou = a;} else print('.');}
				ConnHelper.closeResources(pStmt3, null);
			}

			print("100%\nremovendo as recomendacoes ... ");

                	ConnHelper.closeResources(pStmt2, res2);
                	PreparedStatement pStmt7 = conn.prepareStatement("update catalogo a set recomendacoes = '' where id in ( select distinct id from catalogo_tmp where status = 1)");
                	ret = pStmt7.executeUpdate();
                	ConnHelper.closeResources(pStmt7, null);
			println( "[" + ret + "] OK");

			print("dropando tabela temporaria ...");
                	pTmp = conn.prepareStatement("""drop table catalogo_tmp""");
                	pTmp.executeUpdate();
			pTmp.close();
			println("OK");

		}

                PreparedStatement pStmt5 = conn.prepareStatement("update catalogo set ativo = '0' where ativo = '9'");
                pStmt5.executeUpdate();
                ConnHelper.closeResources(pStmt5, null);

		println("\nfinalizando inativacao");
	}

	public static void bestSellers(Connection conn) {
		print("populando best sellers ... ");
                PreparedStatement pStmt = conn.prepareStatement("""delete from best_sellers""");
                pStmt.executeUpdate();
                ConnHelper.closeResources(pStmt, null);

                PreparedStatement pStmt2 = conn.prepareStatement("""
                                INSERT INTO best_sellers( id_sku, versao, pos ) 
                                SELECT id_sku, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT 
                                                distinct id_sku as id_sku, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM 
                                                catalogo
                                        WHERE 
                                                ativo = '1'
                                        ORDER BY
                                                score DESC,
                                                convert(preco_promocional, DECIMAL(10,2)) / convert(preco_original, DECIMAL(10,2))
                                        LIMIT 20 ) a, (SELECT @rownum:=0) r """ );

		def ret = pStmt2.executeUpdate();
		ConnHelper.closeResources(pStmt2, null);
		println(ret);
	}

	public static void topSellersPorGenero(Connection conn, String field, def generos) {

                print("populando top sellers por genero ... ");
                PreparedStatement pStmt = conn.prepareStatement("""delete from top_sellers_genero""");
                pStmt.executeUpdate();
                ConnHelper.closeResources(pStmt, null);

		PreparedStatement pStmt2;
		Integer res2;

                /////////// RECOMENDACOES //////////
		generos.each { key, value ->
                	pStmt2 = conn.prepareStatement("""
                                INSERT INTO top_sellers_genero ( id_sku, genero, tipo, versao, pos) 
                                SELECT id_sku, genero, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct a.id_sku as id_sku, '""" + key + """' as genero, 'rec' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
                                        WHERE 
                                                a.ativo = '1'
						AND a.""" + field + """ in ( """ + value + """)
                                        ORDER BY
                                                a.score DESC,
                                                convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
                                        LIMIT 20 ) a, (SELECT @rownum:=0) r """ );

       	         	res2 = pStmt2.executeUpdate();
       	         	ConnHelper.closeResources(pStmt2, null);
		}

                String exclusoes = "";
                PreparedStatement pStmt3 = conn.prepareStatement("SELECT id_sku FROM top_sellers_genero");
                ResultSet res3 = pStmt3.executeQuery();
                while( res3.next() ) {
                	exclusoes += (exclusoes == "" ? "" : ",") + res3.getString(1);
                }
                ConnHelper.closeResources(pStmt3, null);

                /////////// OFERTAS //////////
                generos.each { key, value ->
                        pStmt2 = conn.prepareStatement("""
                                INSERT INTO top_sellers_genero ( id_sku, genero, tipo, versao, pos) 
                                SELECT id_sku, genero, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct a.id_sku as id_sku, '""" + key + """' as genero, 'ofe' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
                                        WHERE 
                                                a.ativo = '1'
						AND a.preco_original <> a.preco_promocional
						""" + ( !exclusoes.equals("") ? ( "AND a.id_sku not in (" + exclusoes + ") " ) : "AND 1=1 " ) + """
                                                AND a.""" + field + """ in ( """ + value + """)
                                        ORDER BY
                                                a.score DESC,
                                                convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
                                        LIMIT 20 ) a, (SELECT @rownum:=0) r """ );

                        res2 = pStmt2.executeUpdate();
                        ConnHelper.closeResources(pStmt2, null);
                }

                exclusoes = "";
                pStmt3 = conn.prepareStatement("SELECT id_sku FROM top_sellers_genero");
                res3 = pStmt3.executeQuery();
                while( res3.next() ) {
                        exclusoes += (exclusoes == "" ? "" : ",") + res3.getString(1);
                }       
                ConnHelper.closeResources(pStmt3, null);


                /////////// NOVIDADES //////////
                generos.each { key, value ->
                        pStmt2 = conn.prepareStatement("""
                                INSERT INTO top_sellers_genero ( id_sku, genero, tipo, versao, pos) 
                                SELECT id_sku, genero, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct a.id_sku as id_sku, '""" + key + """' as genero, 'new' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
                                        WHERE 
                                                a.ativo = '1'
						AND DATEDIFF(now(), a.created_in) <= """ + _NOVIDADE_PERIODO_ + """
						""" + ( !exclusoes.equals("") ? ( "AND a.id_sku not in (" + exclusoes + ") " ) : "AND 1=1 " ) + """
                                                AND a.""" + field + """ in ( """ + value + """)
                                        ORDER BY
                                                a.score DESC,
                                                convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
                                        LIMIT 20 ) a, (SELECT @rownum:=0) r """ );

                        res2 = pStmt2.executeUpdate();
                        ConnHelper.closeResources(pStmt2, null);
                }

                exclusoes = "";
                pStmt3 = conn.prepareStatement("SELECT id_sku FROM top_sellers_genero");
                res3 = pStmt3.executeQuery();
                while( res3.next() ) {
                        exclusoes += (exclusoes == "" ? "" : ",") + res3.getString(1);
                }
                ConnHelper.closeResources(pStmt3, null);


                /////////// RECOMENDACOES //////////
                generos.each { key, value ->
                        pStmt2 = conn.prepareStatement("""
                                INSERT INTO top_sellers_genero ( id_sku, genero, tipo, versao, pos) 
                                SELECT id_sku, genero, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct a.id_sku as id_sku, '""" + key + """' as genero, 'email' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
                                        WHERE 
                                                a.ativo = '1'
                                                AND a.""" + field + """ in ( """ + value + """)
                                        ORDER BY
                                                a.score DESC,
                                                convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
                                        LIMIT 20 ) a, (SELECT @rownum:=0) r """ );

                        res2 = pStmt2.executeUpdate();
                        ConnHelper.closeResources(pStmt2, null);
                }

                PreparedStatement pStmtcc = conn.prepareStatement("""UPDATE codigos_categorias SET versao = unix_timestamp()""" );
                pStmtcc.executeUpdate();
                ConnHelper.closeResources(pStmtcc, null);

		println("OK");
	}

	public static void topSellersPorCategoria(Connection conn, Boolean reverso) { 
		print("populando top sellers por categoria ... ");
		PreparedStatement pStmt = conn.prepareStatement("""delete from top_sellers_categoria""");
		pStmt.executeUpdate();
		ConnHelper.closeResources(pStmt, null);

		pStmt = conn.prepareStatement("""
			SELECT 
				a.id,
				a.categoria1,
				a.categoria2,
				a.categoria3
			FROM 
				codigos_categorias a
				
		""");

		ResultSet res = pStmt.executeQuery();
		while( res.next() ) {
			def id_categoria = res.getString(1);
			def categoria1 = res.getString(2);
			def categoria2 = res.getString(3);
			def categoria3 = res.getString(4);
			/////////// RECOMENDACOES //////////
			PreparedStatement pStmt2 = conn.prepareStatement("""
				INSERT INTO top_sellers_categoria ( id_categoria, id_sku, tipo, versao, pos) 
				SELECT id_categoria, id_sku, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
					SELECT 
						distinct """ + id_categoria + """ as id_categoria, a.id_sku as id_sku, 'rec' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
					FROM 
						catalogo a, (SELECT @rownum:=0) r
					WHERE 
						a.ativo = '1'
					ORDER BY """ + 
						( reverso 
							? """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria3=? and a.categoria2=?, 1, if(a.categoria3=?, 2, 3))) """
							: """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria2=? and a.categoria1=?, 1, if(a.categoria1=?, 2, 3))) """
						) + """,
						a.score DESC,
						convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
					LIMIT 10 ) a, (SELECT @rownum:=0) r """ );

			if(reverso) {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria3);
				pStmt2.setString(5, categoria2);
				pStmt2.setString(6, categoria3);
			} else {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria2);
				pStmt2.setString(5, categoria1);
				pStmt2.setString(6, categoria1);
			}

			def res2 = pStmt2.executeUpdate();
			ConnHelper.closeResources(pStmt2, null);

			String exclusoes = "";
			PreparedStatement pStmt3 = conn.prepareStatement("SELECT id_sku FROM top_sellers_categoria WHERE id_categoria = " + id_categoria);
			ResultSet res3 = pStmt3.executeQuery();
	                while( res3.next() ) {
				exclusoes += (exclusoes == "" ? "" : ",") + res3.getString(1);
			}
			ConnHelper.closeResources(pStmt3, null);

			/////////// OFERTAS //////////
			pStmt2 = conn.prepareStatement("""
				INSERT INTO top_sellers_categoria ( id_categoria, id_sku, tipo, versao, pos) 
                                SELECT id_categoria, id_sku, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct """ + id_categoria + """ as id_categoria, a.id_sku as id_sku, 'ofe' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
					WHERE 
						a.ativo = '1'
						AND a.preco_original <> a.preco_promocional
						""" + ( !exclusoes.equals("") ? ( "AND a.id_sku not in (" + exclusoes + ") " ) : "AND 1=1 " ) + """
					ORDER BY """ + 
						( reverso 
							? """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria3=? and a.categoria2=?, 1, if(a.categoria3=?, 2, 3))) """
							: """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria2=? and a.categoria1=?, 1, if(a.categoria1=?, 2, 3))) """
						) + """,
						a.score DESC,
						convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
					LIMIT 10 ) a, (SELECT @rownum:=0) r """ );

			if(reverso) {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria3);
				pStmt2.setString(5, categoria2);
				pStmt2.setString(6, categoria3);
			} else {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria2);
				pStmt2.setString(5, categoria1);
				pStmt2.setString(6, categoria1);
			}

			res2 = pStmt2.executeUpdate();
			ConnHelper.closeResources(pStmt2, null);

			exclusoes = "";
			pStmt3 = conn.prepareStatement("SELECT id_sku FROM top_sellers_categoria WHERE id_categoria = " + id_categoria);
			res3 = pStmt3.executeQuery();
	                while( res3.next() ) {
				exclusoes += (exclusoes == "" ? "" : ",") + res3.getString(1);
			}
			ConnHelper.closeResources(pStmt3, null);

			/////////// NOVIDADES //////////
			pStmt2 = conn.prepareStatement("""
				INSERT INTO top_sellers_categoria ( id_categoria, id_sku, tipo, versao, pos) 
                                SELECT id_categoria, id_sku, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct """ + id_categoria + """ as id_categoria, a.id_sku as id_sku, 'new' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
					WHERE 
						a.ativo = '1'
						""" + ( !exclusoes.equals("") ? ( "AND a.id_sku not in (" + exclusoes + ") " ) : "AND 1=1 " ) + """
					ORDER BY """ + 
						( reverso 
							? """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria3=? and a.categoria2=?, 1, if(a.categoria3=?, 2, 3))) """
							: """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria2=? and a.categoria1=?, 1, if(a.categoria1=?, 2, 3))) """
						) + """,
						DATEDIFF(now(), a.created_in), 
						a.score DESC,
						convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
					LIMIT 10 ) a, (SELECT @rownum:=0) r """ );

			if(reverso) {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria3);
				pStmt2.setString(5, categoria2);
				pStmt2.setString(6, categoria3);
			} else {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria2);
				pStmt2.setString(5, categoria1);
				pStmt2.setString(6, categoria1);
			}

			res2 = pStmt2.executeUpdate();
			ConnHelper.closeResources(pStmt2, null);

			/////////// EMAIL //////////
			pStmt2 = conn.prepareStatement("""
				INSERT INTO top_sellers_categoria ( id_categoria, id_sku, tipo, versao, pos) 
                                SELECT id_categoria, id_sku, tipo, versao, @rownum:=@rownum+1 AS pos FROM  (
                                        SELECT
                                                distinct """ + id_categoria + """ as id_categoria, a.id_sku as id_sku, 'email' as tipo, """ + (long)System.currentTimeMillis()/1000 + """ as versao
                                        FROM
                                                catalogo a, (SELECT @rownum:=0) r
					WHERE 
						a.ativo = '1'
					ORDER BY """ + 
						( reverso 
							? """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria3=? and a.categoria2=?, 1, if(a.categoria3=?, 2, 3))) """
							: """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria2=? and a.categoria1=?, 1, if(a.categoria1=?, 2, 3))) """
						) + """,
						a.score DESC,
						convert(a.preco_promocional, DECIMAL(10,2)) / convert(a.preco_original, DECIMAL(10,2))
					LIMIT 10 ) a, (SELECT @rownum:=0) r """ );

			if(reverso) {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria3);
				pStmt2.setString(5, categoria2);
				pStmt2.setString(6, categoria3);
			} else {
				pStmt2.setString(1, categoria3);
				pStmt2.setString(2, categoria2);
				pStmt2.setString(3, categoria1);
				pStmt2.setString(4, categoria2);
				pStmt2.setString(5, categoria1);
				pStmt2.setString(6, categoria1);
			}

			res2 = pStmt2.executeUpdate();
			ConnHelper.closeResources(pStmt2, null);

		}
		ConnHelper.closeResources(pStmt, null);

                PreparedStatement pStmtcc = conn.prepareStatement("""UPDATE codigos_categorias SET versao = unix_timestamp()""" );
                pStmtcc.executeUpdate();
                ConnHelper.closeResources(pStmtcc, null);

		println("OK");
	}
	
	
	private static void completaRecomendacoes(Connection conn, Boolean reverso) { 
		this.completaRecomendacoes(conn, reverso, 11);
	}

	private static void completaRecomendacoes(Connection conn, Boolean reverso, Integer tamanho) { 
		print("completando recomendacoes ... ");
		print("criando tab temoraria ... ");
		PreparedStatement pTmp = null;
		pTmp = conn.prepareStatement("""drop table if exists catalogo_tmp""");
                pTmp.executeUpdate();
		pTmp = conn.prepareStatement("""
			create table catalogo_tmp(
				INDEX(id), 
				INDEX(desconto), 
				INDEX(sku), 
				INDEX(score), 
				INDEX(categoria1), 
				INDEX(categoria2), 
				INDEX(categoria3), 
				INDEX(preco_promocional),
				INDEX(largura) ) engine=memory 
			select id, sku, score, categoria1, categoria2, categoria3, desconto, autoNormalizaMoeda(preco_promocional) as preco_promocional, length(sku) as largura from catalogo where ativo = '1'
			ORDER BY """ +
			( reverso 
				? """ categoria3, categoria2, categoria1 """
				: """ categoria1, categoria2, categoria3 """
			) );
		pTmp.executeUpdate();
		ConnHelper.closeResources(pTmp, null);
		println("OK");

		PreparedStatement pStmt = conn.prepareStatement("""
			SELECT 
				a.id_categoria,
				a.id,
				a.recomendacoes,
				LENGTH(ifnull(a.recomendacoes, '')) - LENGTH(REPLACE(ifnull(a.recomendacoes, ''), ',', '')) ,
				a.categoria1,
				a.categoria2,
				a.categoria3,
				a.preco_promocional,
				length(a.sku)
			FROM 
				catalogo a
			WHERE 
				( LENGTH(ifnull(a.recomendacoes, '')) - LENGTH(REPLACE(ifnull(a.recomendacoes, ''), ',', '')) < """ + (tamanho - 1) + """ )
				
		""");

		def categoria1, categoria2, categoria3;
		ResultSet res = pStmt.executeQuery();
		while( res.next() ) {
			categoria1 = res.getString(5);
			categoria2 = res.getString(6);
			categoria3 = res.getString(7);

			def recom = res.getString( 3 );
			if(recom == null) recom = "";
			recom = "'" + recom.replaceAll(",", "','") + "'";
			int total = res.getInt(4); total += (recom=="''") ? 0 : 1;
			def preco_referencia = res.getString(8);
			preco_referencia = normalizaMoeda( ( preco_referencia==null || preco_referencia.trim()==''?'0.00':preco_referencia ) , true);
//			preco_referencia = preco_referencia.replaceAll(",", ".");
//			if(preco_referencia!=null&&preco_referencia.charAt(preco_referencia.length-3) == '.') {
//			}
			PreparedStatement pStmt2 = conn.prepareStatement("""
					SELECT 
						distinct a.sku
					FROM 
						catalogo_tmp a
					WHERE 
						a.id <> ?
						AND length(a.sku) = """ + res.getString(9) + """
						AND a.sku not in (""" + recom + """) 
					ORDER BY """ +
						( reverso 
							? """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria3=? and a.categoria2=?, 1, if(a.categoria3=?, 2, 3))) """
							: """ if(a.categoria3=? and a.categoria2=? and a.categoria1=?, 0, if(a.categoria2=? and a.categoria1=?, 1, if(a.categoria1=?, 2, 3))) """
						) + """,
						abs(a.preco_promocional - ?),
						a.score DESC
					LIMIT """ + (tamanho-total) );

			pStmt2.setString(1, res.getString(2));
                        if(reverso) {
                                pStmt2.setString(2, categoria3);
                                pStmt2.setString(3, categoria2);
                                pStmt2.setString(4, categoria1);
                                pStmt2.setString(5, categoria3);
                                pStmt2.setString(6, categoria2);
                                pStmt2.setString(7, categoria3);
                                pStmt2.setFloat(8, Float.parseFloat(preco_referencia));
                        } else {
                                pStmt2.setString(2, categoria3);
                                pStmt2.setString(3, categoria2);
                                pStmt2.setString(4, categoria1);
                                pStmt2.setString(5, categoria2);
                                pStmt2.setString(6, categoria1);
                                pStmt2.setString(7, categoria1);
                                pStmt2.setFloat(8, Float.parseFloat(preco_referencia));
                                //pStmt2.setString(8, preco_referencia);
                        }

			ResultSet res2 = pStmt2.executeQuery();

			def recomendacoes = res.getString(3);
			if(recomendacoes==null) recomendacoes = "";

			def cnt = 0;
			while( res2.next() ) {
				recomendacoes += (recomendacoes==""?"":",") + res2.getString(1);
				cnt++;
			}
			ConnHelper.closeResources(pStmt2, null)

			PreparedStatement pStmt3 = conn.prepareStatement("update catalogo set recomendacoes = ifnull(?, recomendacoes), versao = ? where id = ? ");
			pStmt3.setString(1, recomendacoes);
			pStmt3.setLong(2, (long)System.currentTimeMillis()/1000);
			pStmt3.setLong(3, res.getLong(2));
			def i = pStmt3.executeUpdate();
			if(i>0) println("COMPLETOU recomendacao para id: " + res.getLong(2) + " -add: " + (total) + "/" + cnt);
			ConnHelper.closeResources(pStmt3, null);
		}
		ConnHelper.closeResources(pStmt, null);
		print("\ndropando tabela temporaria ...");
                pTmp = conn.prepareStatement("""drop table catalogo_tmp""");
                pTmp.executeUpdate();
		ConnHelper.closeResources(pTmp, null);

		println("OK");
	}

	static void normalizaSupernova(Connection conn) {
		print("normalizando supernova ... ");
		CallableStatement cStmt = conn.prepareCall("{ call popula_supernova() }");
		cStmt.execute();
		ConnHelper.closeResources(cStmt, null);
		println("OK");
	}
	
        static String getField ( String buffer ) {
                def pi =  buffer.indexOf( '<![CDATA[' );
                def pf =  buffer.indexOf( ']]>' );
                if(pi > -1 && pf > -1)
                        buffer = buffer.substring( pi + 9 , pf );
                else {
                        pi = buffer.indexOf('>');
                        pf = buffer.indexOf('</');
                        buffer = buffer.substring( pi + 1 , pf );
                }
                return buffer;
        }


	public static void salvaCatalogoComDescricao (Connection conn, String sku, String c1, String c2, String c3,
                String imagem, String precoOriginal, String precoPromocional, String numeroParcelas, String valorParcelas,
                String link, String marca, String nome, String codigo, String ativo, Boolean encodeImage, String descricao) {
                def cnt;
                String recomendacoes;
                PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where sku = ?");
                pStmt.setString(1, sku);
                ResultSet res = pStmt.executeQuery();

		def categoria = '';
		categoria += c1 != null && c1 != '' ? c1 : '';
		categoria += c2 != null && c2 != '' ? '_'+c2 : '';
		categoria += c3 != null && c3 != '' ? '_'+c3 : '';

		ativo = (ativo=='sim'||ativo=='1')?'1':'0';

                if( res.next() )
                        cnt = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);

		if(encodeImage && imagem != null && imagem != '')
	                //imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=o8vtEg01qmUcZL-H_Dgv6Q"; //s=200x200
	                imagem = "http://static.dynad.net/let/" + URLCodec.encrypt(imagem) + "?hash=ElXEFX1LcBgiF7zWSd1M-A"; //f=180x180&cr=3

                int col = 1;
                String stmt = "";

		def selo = 'novidade';
		def desconto = '';

                if( cnt == 0) {
			if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
				selo = 'desconto';
				desconto = Math.floor( ( 1 - ( Double.parseDouble(Digester.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(Digester.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
			}

                        stmt = """insert into catalogo ( 
		id, categoria, categoria1, categoria2, categoria3, nome, imagem, link, preco_original, preco_promocional,
		numero_parcelas, valor_parcelas, marca, status, codigo, recomendacoes, ativo, versao, selo, desconto, descricao, sku, last_update, created_in) 
	select ifnull(max(id), 0) + 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now() from catalogo """;

                } else {
			desconto = '';
			if(precoOriginal != precoPromocional && precoPromocional != null && precoPromocional != '') {
				selo = 'desconto';
				desconto = Math.floor( ( 1 - ( Double.parseDouble(Digester.autoNormalizaMoeda(precoPromocional, true, Locale.US)) / Double.parseDouble(Digester.autoNormalizaMoeda(precoOriginal, true, Locale.US)) ) ) * 100 );
			}

                        stmt = """update catalogo set
		categoria = ?,
                categoria1 = ?,
                categoria2 = ?,
                categoria3 = ?,
                nome = ?,
                imagem = ?,
                link = ?,
                preco_original = ?,
                preco_promocional = ?,
                numero_parcelas = ?,
                valor_parcelas = ?,
                marca = ?,
                status = ?,
				codigo = ?,
                recomendacoes = ?,
                ativo = if(ativo <> ?, if(ativo = '1', '9', '1'), ativo),
                versao = ?,
				selo = ?,
				desconto = ?,
				last_update = now(),
				descricao = ?
		        where sku = ? AND (
                        nome <> ? OR
                        imagem <> ? OR
                        link <> ? OR
                        preco_original <> ? OR
                        preco_promocional <> ? OR
                        valor_parcelas <> ? OR
                        categoria <> ? OR
                        ativo <> ? )""";
                }


                pStmt = conn.prepareStatement(stmt.replaceAll("\\n", " "));
		def idx = 1;

                try {
			pStmt.setString(idx++, categoria) ;
			pStmt.setString(idx++, c1) ;
			pStmt.setString(idx++, c2) ;
			pStmt.setString(idx++, c3) ;
			pStmt.setString(idx++, nome) ;
			pStmt.setString(idx++, imagem) ;
			pStmt.setString(idx++, link) ;
			pStmt.setString(idx++, precoOriginal) ;
			pStmt.setString(idx++, precoPromocional) ;
			pStmt.setString(idx++, numeroParcelas) ;
			pStmt.setString(idx++, valorParcelas) ;
			pStmt.setString(idx++, marca) ;
			pStmt.setString(idx++, '1') ;
			pStmt.setString(idx++, codigo) ;
			pStmt.setString(idx++, recomendacoes) ;
			pStmt.setString(idx++, ativo) ;
			pStmt.setLong(idx++, (long)System.currentTimeMillis()/1000) ;
			pStmt.setString(idx++, selo) ;
			pStmt.setString(idx++, "" + desconto) ;
			pStmt.setString(idx++, descricao) ;
			pStmt.setString(idx++, sku) ;


			if(stmt.toLowerCase().trim().startsWith("update") ) {
				pStmt.setString(idx++, nome) ;
				pStmt.setString(idx++, imagem) ;
				pStmt.setString(idx++, link) ;
				pStmt.setString(idx++, precoOriginal) ;
				pStmt.setString(idx++, precoPromocional) ;
				pStmt.setString(idx++, valorParcelas) ;
				pStmt.setString(idx++, categoria) ;
				pStmt.setString(idx++, ativo) ;
			}

			def ret = pStmt.executeUpdate();
			ConnHelper.closeResources(pStmt, null);

			if(stmt.toLowerCase().trim().startsWith("insert") )
				println(sku + " -> ..N.O.V.O... -> " + nome);
			else if(ret > 0)
				println(sku + " -> .ATUALIZADO. -> " + nome);
			else {
				println(sku + " -> ....none.... -> " + nome);
				stmt = "update catalogo set last_update = now(), selo = if(preco_original <> preco_promocional, 'desconto', if(DATEDIFF(now(), created_in) <= " + _NOVIDADE_PERIODO_ + ", 'novidade', '')) where sku = ?";
				pStmt = conn.prepareStatement(stmt);
				pStmt.setString(1, sku) ;
				pStmt.executeUpdate();
				ConnHelper.closeResources(pStmt, null);
			}


                } catch(ex) {
                        println("erro: [" + sku + "]");
                        ex.printStackTrace();
                }

        } 

}
