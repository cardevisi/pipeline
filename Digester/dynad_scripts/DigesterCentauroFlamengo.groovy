import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_centauro_flamengo";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3, gratis;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                summary.PRODUTO.each {
/*
produtos[attributes={}; value=[PRODUTO[attributes={}; value=[SKU[attributes={}; value=[50002000]], NOME[attributes={}; value=[Barra de Ferro para Ani
<produtos>
  <PRODUTO>
    <SKU>500020 00</SKU>
    <NOME><![CDATA[Barra de Ferro para Anilha Polimet 40 cm]]></NOME>

    <CATEGORIA><![CDATA[Calcados]]></CATEGORIA>
    <SUBCATEGORIA><![CDATA[Futebol]]></SUBCATEGORIA>
    <GENERO>Unissex</GENERO>

    <PRECO>
      <DE>49.90</DE>
      <POR>49.90</POR>
    </PRECO>
    <PARCELAMENTO>
      <N_PARCELAS>2</N_PARCELAS>
      <V_PARCELAS>24.95</V_PARCELAS>
    </PARCELAMENTO>
    <FRETE>
      <GRATIS>0</GRATIS>
      <CONDICAO />
    </FRETE>
    <URL><![CDATA[ http://www.centauro.com.br/promocao?type=pdp&id=500020&utm_source=Asapcode&utm_medium=xml&utm_campaign=Asapcode-Training-Barra--500020-00&origem=Asapcode ]]></URL>
    <IMAGEM> http://images.centauro.com.br/900x900/50002000.jpg </IMAGEM>
    <DISPONIBILIDADE>1</DISPONIBILIDADE>
    <VARIAVEIS>
      <VARIAVEL_A />
      <VARIAVEL_B>00?utm_source=Asapcode&amp;utm_medium=xml&amp;utm_campaign=Asapcode-Training-Barra--500020-00&amp;origem=Asapcode </VARIAVEL_B>
      <VARIAVEL_C />
      <VARIAVEL_D />
      <VARIAVEL_E />
    </VARIAVEIS>
  </PRODUTO>


*/

                                sku = it.SKU.text()
//                              nome = it.name.text()
                                nome = Digester.stripTags(Digester.ISO2UTF8(it.NOME.text()))
				nome = nome.replace(' ? ', ' ');
                                url = it.URL.text()

				/*if(url.indexOf("?") > -1) {
					def c = -1
					if((c = url.indexOf('&utm_source')) > -1 || (c = url.indexOf('?utm_source')) > -1 )
						url = url.substring(0, c);
					if((c = url.indexOf('&utm_campaign')) > -1 || (c = url.indexOf('?utm_campaign')) > -1 )
						url = url.substring(0, c);
					if((c = url.indexOf('&utm_medium')) > -1 || (c = url.indexOf('?utm_medium')) > -1 )
						url = url.substring(0, c);
				}*/

                                fprice = Digester.autoNormalizaMoeda(it.PRECO.POR.text(), false, new Locale("pt", "BR"))
                                oprice = Digester.autoNormalizaMoeda(it.PRECO.DE.text(), false, new Locale("pt", "BR"))
                                nparcelas = it.PARCELAMENTO.N_PARCELAS.text()
                                vparcelas = Digester.autoNormalizaMoeda(it.PARCELAMENTO.V_PARCELAS.text(), false, new Locale("pt", "BR"))

                        	image = it.IMAGEM.text()
                         	promover = it.DISPONIBILIDADE.text() == '1' ? '1' : '0'
                         	c1 = it.CATEGORIA.text()
                         	c2 = it.SUBCATEGORIA.text()
                         	c3 = it.GENERO.text()
				gratis = it.FRETE.GRATIS.text()
                         	marca = ''

//println("sku:"+sku+";nome:"+nome+";url:"+url);
				if(promover == '1') {
	                                Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false, gratis);
					//sku = sku.length() > 6 ? sku.substring(0,6) : sku
	                                //Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false, gratis);
				}
 
				
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/centauro_flamengo.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), true);
			Digester.bestSellers(ConnHelper.get().reopen());
                        Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
                        Digester.validacao(ConnHelper.get().reopen());

                ///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/centauro_flamengo.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
				out.print("\n");
				out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
                        }
                }
                ConnHelper.closeResources(pStmt, res);



                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

        }
	
}
