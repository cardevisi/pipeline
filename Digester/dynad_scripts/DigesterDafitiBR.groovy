import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterDafitiBrasil {
	static int key;
	static {
		ConnHelper.schema = "dynad_dafiti";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

	/*
	<product>
		<bigimage><![CDATA[http://static.dafity.com.br/10/60681/1-zoom.jpg]]></bigimage>
		<category1><![CDATA[esporte]]></category1>
		<category2><![CDATA[roupas]]></category2>
		<category3><![CDATA[masculino]]></category3>
		<finalprice><![CDATA[53.90]]></finalprice>
		<gender><![CDATA[male]]></gender>
		<instock><![CDATA[sim]]></instock>
		<marca><![CDATA[Zoo York]]></marca>
		<name><![CDATA[Camiseta Zoo York Spray Preta]]></name>
		<nparcelas><![CDATA[1]]></nparcelas>
		<originalprice><![CDATA[72.00]]></originalprice>
		<product_id><![CDATA[ZO802SRM98WJP]]></product_id>
		<producturl><![CDATA[http://www.dafiti.com.br/Camiseta-Zoo-York-Spray-Preta-1860601.html?re=1294241807.masculino.esporte_roupas.1860601&utm_source=1294241807&utm_medium=re&utm_term=Camiseta-Zoo-York-Spray-Preta-1860601&utm_content=roupas&utm_campaign=masculino.esporte_roupas]]></producturl>
		<sizes><![CDATA[G, GG, M, P]]></sizes>
		<smallimage><![CDATA[http://static.dafity.com.br/10/60681/1-cart.jpg]]></smallimage>
		<vparcelas><![CDATA[53.90]]></vparcelas>
	</product>
	*/

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;
		String recomendacao1, recomendacao2, recomendacao3, recomendacao4, recomendacao5, recomendacao6, recomendacao7, recomendacao8, recomendacao9, recomendacao10;

        def rss = new XmlParser().parse(file);
		def list = rss.product
		def blind = new Date().getAt(Calendar.HOUR) > 7;
        
		list.each {
			sku = it.product_id.text();
			nome = it.name.text();
			//nome = Digester.stripTags(it.name.text()) 
   			url = it.producturl.text()
			if(url.indexOf("?")>-1) url = url.substring(0, url.indexOf("?"));

			oprice = Digester.autoNormalizaMoeda(it.originalprice.text(), false, new Locale("us", "EN"));
			fprice = Digester.autoNormalizaMoeda(it.finalprice.text(), false, new Locale("us", "EN"));
			vparcelas = Digester.autoNormalizaMoeda(it.vparcelas.text(), false, new Locale("us", "EN"));
			nparcelas = it.nparcelas.text();                    

			if( oprice.indexOf(',') > -1 ) oprice = oprice.replaceAll(',', '');
			if( fprice.indexOf(',') > -1 ) fprice = fprice.replaceAll(',', '');
			if( vparcelas.indexOf(',') > -1 ) oprice = vparcelas.replaceAll(',', '');

			image = it.bigimage.text();

			c1 = it.category1.text()
			c2 = it.category2.text()
			c3 = it.category3.text()

			categoria = c1 + '/' + c2 + '/' + c3;

			promover = it.instock.text() 
			if(promover != null && promover.trim().toLowerCase().equals("sim"))
				promover = '1'; 
			else
				promover = '0';

			recomendacao1 = it.recommendation1.text();                    
			recomendacao2 = it.recommendation2.text();                    
			recomendacao3 = it.recommendation3.text();                    
			recomendacao4 = it.recommendation4.text();                    
			recomendacao5 = it.recommendation5.text();                    
			recomendacao6 = it.recommendation6.text();                    
			recomendacao7 = it.recommendation7.text();                    
			recomendacao8 = it.recommendation8.text();                    
			recomendacao9 = it.recommendation9.text();                    
			recomendacao10 = it.recommendation10.text();                    
			
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: promover, lookupChange: true, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: false, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: false, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );

			
			///// VAI USAR BLIND /////
			fields.add( new FieldMetadata(columnName : 'recomendacao1', columnType: 'string', columnValue: recomendacao1, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao2', columnType: 'string', columnValue: recomendacao2, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao3', columnType: 'string', columnValue: recomendacao3, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao4', columnType: 'string', columnValue: recomendacao4, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao5', columnType: 'string', columnValue: recomendacao5, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao6', columnType: 'string', columnValue: recomendacao6, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao7', columnType: 'string', columnValue: recomendacao7, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao8', columnType: 'string', columnValue: recomendacao8, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao9', columnType: 'string', columnValue: recomendacao9, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao10', columnType: 'string', columnValue: recomendacao10, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: '2', lookupChange: true, platformType: 'NA') );

//println("----->" + sku + ";" + nome + ";" + oprice + ";" + fprice + ";" + nparcelas + ";" + vparcelas + ";" + recomendacao1 + ";" + categoria);
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/dafiti.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), true);
                        Digester.validacao(ConnHelper.get().reopen());
                        
		} else if(args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
                }

///////// ATENCAO 		DigesterV2.completaRecomendacoesDafiti(ConnHelper.get().reopen());

                Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmtR = conn.prepareStatement("update catalogo set recomendacoes_bi = recomendacoes");
		pStmtR.executeUpdate();
                ConnHelper.closeResources(pStmtR, null);

                ///////// EXPORT SUPERNOVA 2 ///////////
                conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes_bi,categoria3 from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/TMP/sna-files/dafiti.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount()-2; i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
                                def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
                                        PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
                                        ResultSet res2 = pStmt2.executeQuery();
                                        def recs = "";
                                        while( res2.next() ) { recs += (recs==""?"":",") + res2.getString(1); }
                                        ConnHelper.closeResources(pStmt2, res2);
                                out.print("|"+recs+"|"+res.getString(12));
                                out.println("\n_" + res.getString(2) + "|" + res.getString(1));
                        }
                }
                ConnHelper.closeResources(pStmt, res);
		
		///////// EXPORT UOL CLIQUES ///////////
		java.util.List<String> listTopSellers = new java.util.ArrayList();
		conn = ConnHelper.get().reopen();
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/dafiti_br_uolcliques.txt").withWriter { out ->
			out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while( res.next() ) {
				out.print(res.getString(1) + "|Dafiti - Aproveite|" + res.getString(5) + "|");
				if( res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1 )
					out.print('A partir de R$' + Digester.autoNormalizaMoeda(res.getString(8), false, new Locale("pt", "BR")) );
				else
					out.print(res.getString(9) + 'x de R$' + Digester.autoNormalizaMoeda(res.getString(10), false, new Locale("pt", "BR")));
				out.print('|' + res.getString(6) + '?dp=dp=1294241736.87x56_UOCLIQUES_UN_PM.87x56_RT00.87x56_CLIQU_UN_NA_161015_S&utm_source=1294241736&utm_medium=dp&utm_campaign=87x56_UOCLIQUES_UN_PM&utm_content=87x56_RT00&utm_term=87x56_CLIQU_UN_NA_161015_S|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3)) + '|' + res.getString(11) + '\n');
				out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
				if( listTopSellers.size() < 10 ) listTopSellers.add( res.getString(1) );
			}
		}
		ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select a.sku from catalogo a join best_sellers b on a.id_sku = b.id_sku where a.ativo = '1' order by b.pos");
                res = pStmt.executeQuery();
                int numBsDb = 0;
                new File("/mnt/TMP/sna-files/dafiti_br_uolcliques_topsellers.txt").withWriter { out ->
                        while (res.next()) { out.println( res.getString(1) ); numBsDb++; }
                        if( numBsDb < 20 ) {
                                listTopSellers.each { itTS -> out.println( itTS ); }
                        }
                }
                ConnHelper.closeResources(pStmt, res);

		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
                res = pStmt.executeQuery();
                new File("/mnt/TMP/sna-files/dafiti_br_barra.txt").withWriter { out ->
                        out.print("metadata|sku|nome|ativo|preco_promocional|numero_parcelas|valor_parcelas|link|tracking_barra|imagem|recomendacoes\n");
                        while( res.next() ) {
                                out.print(res.getString(1) + "|" + res.getString(5) + "|" + res.getString(4) + "|" + Digester.autoNormalizaMoeda(res.getString(8), false, new Locale("pt", "BR")) + "|");
                                if( res.getString(9) != null && !res.getString(9).trim().equals("") && !"1".equals( res.getString(9) ) )
                                        out.print(res.getString(9) + "|" + Digester.autoNormalizaMoeda(res.getString(10), false, new Locale("pt", "BR")) );
                                else
                                        out.print("1|" + Digester.autoNormalizaMoeda(res.getString(8), false, new Locale("pt", "BR")));
                                out.print("|" + res.getString(6) + "|dp=1294241736.BARRART.UOL_115x75_BARRART_CA_UN.UOL_115x75_BARRART_CA_UN_DAFITI&utm_source=1294241736&utm_medium=dp&utm_campaign=BARRART&utm_content=UOL_115x75_BARRART_CA_UN&utm_term=UOL_115x75_BARRART_CA_UN_DAFITI");
                                out.print("|http://static.dynad.net/let/" + URLCodec.encrypt(res.getString(3)) + "|" + res.getString(11) + "\n");
                                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
                        }
                }
                ConnHelper.closeResources(pStmt, res);

    	}

	private static String removeAcentos (String string){
		string = string.replaceAll("[ÂÀÁÄÃ]","A");
		string = string.replaceAll("[âãàáä]","a");
		string = string.replaceAll("[ÊÈÉË]","E");
		string = string.replaceAll("[êèéë]","e");
		string = string.replaceAll("ÎÍÌÏ","I");
		string = string.replaceAll("îíìï","i");
		string = string.replaceAll("[ÔÕÒÓÖ]","O");
		string = string.replaceAll("[ôõòóö]","o");
		string = string.replaceAll("[ÛÙÚÜ]","U");
		string = string.replaceAll("[ûúùü]","u");
		string = string.replaceAll("Ç","C");
		string = string.replaceAll("ç","c");
		string = string.replaceAll("[ýÿ]","y");
		string = string.replaceAll("Ý","Y");
		string = string.replaceAll("ñ","n");
		string = string.replaceAll("Ñ","N");
		return string;
        }

        private static String getStringForTracking( String conteudo ) {
		if( conteudo == null ) return null;
		conteudo = removeAcentos( conteudo );
		conteudo = Normalizer.normalize( conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]","-").toLowerCase();
        }
        
        
	private static String getValidParameters ( String productURL, String [] arrSkipParameters ) {
		StringBuilder sb = new StringBuilder();
		String urlParam = productURL.indexOf('?') > -1 ? ( productURL.substring( productURL.indexOf('?') + 1 ) ) : "";
		if( urlParam.trim().equals("") )
			return productURL;

		productURL = productURL.substring(0, productURL.indexOf('?'));
		for( String paramEntry : urlParam.split("&") ) {
			String paramName = paramEntry.substring(0, ( paramEntry.indexOf("=") > 0 ? paramEntry.indexOf("=") : 0 ) );
			String paramValue = ( paramEntry.indexOf("=") > 0 ? paramEntry.substring( paramEntry.indexOf("=") + 1 ) : "" );
			if( paramName.startsWith("utm_") )
				continue;

			boolean found = false;
			for(String skipParamName : arrSkipParameters ) {
				if( skipParamName.trim().toLowerCase().equals( paramName.trim().toLowerCase() ) ) {
					found = true;
					break;
				}
			}

			if( !found ) {
				if(sb.length() > 0 ) sb.append('&');
				sb.append( paramName );
				if( paramValue != '' )
					sb.append('=').append( paramValue );
			}
		}

		if( productURL.indexOf('?') > -1 && !productURL.endsWith('&') )
			productURL += '&';
		else if( productURL.indexOf('?') == -1 )
			productURL += '?';
		productURL += sb.toString();
		return productURL;
	}


}
