import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterDafitiBrasil {
	static int key;
	static {
		ConnHelper.schema = "dynad_dafiti_dese";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

/*
                <product id="0572-00584-0573">
                        <name><![CDATA[Tênis Gorge C2 Verde - Timberland]]></name>
                        <smallimage><![CDATA[http://static.dafity.com.br/p/-524-1-catalog.jpg]]></smallimage>
                        <bigimage><![CDATA[http://static.dafity.com.br/p/-524-1-zoom.jpg]]></bigimage>
                        <producturl><![CDATA[http://www.dafiti.com.br/Tenis-Gorge-C2-Verde-425.html?re=1294241807.feminino.calcados_tenis.425&utm_source=1294241807&utm_medium=re&utm_term=Tenis-Gorge-C2-Verde-425&utm_content=tenis&utm_campaign=feminino.calcados_tenis]]></producturl>
                        <originalprice><![CDATA[259.90]]></originalprice>
                        <finalprice><![CDATA[184.90]]></finalprice>
                        <nparcelas><![CDATA[9]]></nparcelas>
                        <vparcelas><![CDATA[20.54]]></vparcelas>
                        <marca><![CDATA[Timberland]]></marca>
                        <category1><![CDATA[calcados]]></category1>
                        <category2><![CDATA[tenis]]></category2>
                        <category3><![CDATA[feminino]]></category3>
                        <category4><![CDATA[calcados-femininos]]></category4>
                        <category5><![CDATA[tenis]]></category5>
                        <instock><![CDATA[sim]]></instock>
                        <recommendation1><![CDATA[SA232ACF06COV]]></recommendation1>
                        <recommendation2><![CDATA[CA957APM65RGG]]></recommendation2>
                        <recommendation3><![CDATA[PI931SHF78XDT]]></recommendation3>
                        <recommendation4><![CDATA[AC788SHM60RPZ]]></recommendation4>
                        <recommendation5><![CDATA[PO870APM95IHG]]></recommendation5>
                        <recommendation6><![CDATA[FI911APM49HGG]]></recommendation6>
                        <recommendation7><![CDATA[PO870APM94IHH]]></recommendation7>
                        <recommendation8><![CDATA[FI911APM52HGD]]></recommendation8>
                        <recommendation9><![CDATA[CA497APM15NHK]]></recommendation9>
                    <sizes></sizes>
                </product>

*/

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;
		String recomendacao1, recomendacao2, recomendacao3, recomendacao4, recomendacao5, recomendacao6, recomendacao7, recomendacao8, recomendacao9, recomendacao10;

	        def rss = new XmlParser().parse(file);
		def list = rss.product
		def blind = new Date().getAt(Calendar.HOUR) > 7;
        
		list.each {
			sku = it.'@id';
			nome = it.name.text()
			//nome = Digester.stripTags(it.name.text()) 
       			url = it.producturl.text()
			if(url.indexOf("?")>-1) url = url.substring(0, url.indexOf("?"));

			oprice = Digester.autoNormalizaMoeda(it.originalprice.text(), false, new Locale("us", "EN"));
			fprice = Digester.autoNormalizaMoeda(it.finalprice.text(), false, new Locale("us", "EN"));
			vparcelas = Digester.autoNormalizaMoeda(it.vparcelas.text(), false, new Locale("us", "EN"));
			nparcelas = it.nparcelas.text();                    

			image = it.bigimage.text();

			c1 = it.category1.text()
			c2 = it.category2.text()
			c3 = it.category3.text()

			categoria = c1 + '/' + c2 + '/' + c3;

			promover = it.instock.text() 
			if(promover != null && promover.trim().toLowerCase().equals("sim"))
				promover = '1'; 
			else
				promover = '0';

			recomendacao1 = it.recommendation1.text();                    
			recomendacao2 = it.recommendation2.text();                    
			recomendacao3 = it.recommendation3.text();                    
			recomendacao4 = it.recommendation4.text();                    
			recomendacao5 = it.recommendation5.text();                    
			recomendacao6 = it.recommendation6.text();                    
			recomendacao7 = it.recommendation7.text();                    
			recomendacao8 = it.recommendation8.text();                    
			recomendacao9 = it.recommendation9.text();                    
			recomendacao10 = it.recommendation10.text();                    
			
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: promover, lookupChange: true, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: false, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: false, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );

			
			///// VAI USAR BLIND /////
			fields.add( new FieldMetadata(columnName : 'recomendacao1', columnType: 'string', columnValue: recomendacao1, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao2', columnType: 'string', columnValue: recomendacao2, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao3', columnType: 'string', columnValue: recomendacao3, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao4', columnType: 'string', columnValue: recomendacao4, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao5', columnType: 'string', columnValue: recomendacao5, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao6', columnType: 'string', columnValue: recomendacao6, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao7', columnType: 'string', columnValue: recomendacao7, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao8', columnType: 'string', columnValue: recomendacao8, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao9', columnType: 'string', columnValue: recomendacao9, lookupChange: blind, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'recomendacao10', columnType: 'string', columnValue: recomendacao10, lookupChange: blind, platformType: 'NA') );

//println("----->" + sku + ";" + nome + ";" + oprice + ";" + fprice + ";" + nparcelas + ";" + vparcelas + ";" + recomendacao1 + ";" + categoria);
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
                ///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes_bi from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/dafiti.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount()-1; i++) out.print("|"+res.getString(i).replace("|", " "));
                                def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
                                        PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
                                        ResultSet res2 = pStmt2.executeQuery();
                                        def recs = "";
                                        while( res2.next() ) { recs += (recs==""?"":",") + res2.getString(1); }
                                        ConnHelper.closeResources(pStmt2, res2);
                                out.print("|"+recs);
                                out.println("\n_" + res.getString(2) + "|" + res.getString(1));
                        }
                }
                ConnHelper.closeResources(pStmt, res);

    	}

}


