import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
    static int key;
    static {
        ConnHelper.schema = "dynad_dafiti_co";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() ) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }
    

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();
		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3, c4, c5;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

        def summary = new XmlParser().parse(file);
        def list = summary.product

        list.each {
        	sku = it.identifier.text()
        	nome = it.fn.text()
        	url = it.url.text()
			
            fprice = '\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.price.text(), false, new Locale("es", "CO")), false, new Locale("es", "CO"))
            oprice = '\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.amount.text(), false, new Locale("es", "CO")), false, new Locale("es", "CO"))
        	nparcelas = "";//it.ninstallments.text()
        	vparcelas = "";//DigesterV2.cortaDecimal(it.vinstallments.text(), false)

        	image = it.photo.text()
        	desconto = "";//it.discount.text()
			
			def tmp = URLDecoder.decode(it.category.text())
			def cs = tmp.split('\\\\');
         	c1 = cs.length>0?cs[0].trim():tmp
         	c2 = cs.length>1?cs[1].trim():"empty"
         	c3 = cs.length>2?cs[2].trim():"empty"
			c4 = cs.length>3?cs[3].trim():"empty"
			c5 = cs.length>4?cs[4].trim():"empty"
         	marca = it.brand.text()
			if(url.indexOf("?") > -1) url = url.substring(0, url.indexOf("?"));
			//workaround
			if(image != null) {
				image = image.replace(".jpeg", ".jpg");
				image = image.replace(".JPEG", ".jpg");
				image = image.replace(".JPG", ".jpg");
				image = image.replace(".png", ".jpg");
				image = image.replace(".PNG", ".jpg");
			}			
            DigesterV2.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false, null, c4, c5);
			if( ++cline % 100 == 0 ) conn = ConnHelper.get().reopen();
		}		
	}	
	  
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/dafiti_co.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
    			Digester.validacao(ConnHelper.get().reopen(), 20);
		} else if(args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
		}

        ///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, marca, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/dafiti_co.txt").withWriter { out ->
        	//ESCREVE O HEADER
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            //ESCREVE OS DADOS
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);
        println 'SN2 - DATA GENERATED';
	}	
}

