import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])


class Dafiti {
	
        static int key;
        static {

                ConnHelper.schema = "dynad_dafiti";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);

        }


	public static void main (String [] args ) {
/*
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/dafiti.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), true, 21);
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
			Digester.topSellersPorGenero(ConnHelper.get().reopen(), 'categoria3', ['masculino': "'masculino'", 'feminino' : "'feminino'", 'infantil' : "'infantil feminino', 'infantil masculino'", 'unissex' : "'unissex'"]);
			//Dafiti.populaSkusNaCategoria();
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), true);
			Digester.topSellersPorGenero(ConnHelper.get().reopen(), 'categoria3', ['masculino': "'masculino'", 'feminino' : "'feminino'", 'infantil' : "'infantil feminino', 'infantil masculino'", 'unissex' : "'unissex'"]);
                }
*/
		

                ///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/dafiti.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount()-1; i++) out.print("|"+res.getString(i).replace("|", " "));
				def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
					PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
					ResultSet res2 = pStmt2.executeQuery();
					def recs = "";
					while( res2.next() ) {
						recs += (recs==""?"":",") + res2.getString(1);
					}
                			ConnHelper.closeResources(pStmt2, res2);
					out.print("|"+recs);

                                out.println("\n_" + res.getString(2) + "|" + res.getString(1));
                        }
                }
                ConnHelper.closeResources(pStmt, res);


	}

}

