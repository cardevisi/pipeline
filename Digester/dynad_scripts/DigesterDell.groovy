import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;

import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterDiSantinni {
	static int key;
	static {
		ConnHelper.schema = "dynad_dell";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;
    
    
    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto, cta;

        def rss = new XmlParser().parse(file);
        def list = rss.PRODUTOS
        
		list.each {
			sku = it.Code.text();
			//nome = Digester.stripTags(Digester.ISO2UTF8(it.Description.text())) 
			nome = Digester.stripTags(it.Description.text()) 
			if( nome.indexOf('-') > -1 )
				nome = nome.substring(0, nome.indexOf('-') );

            		url = it.URL.text()
			if( url.indexOf('&CAWELAID=') > -1 ) 
				url = url.replaceAll('&CAWELAID=', URLEncoder.encode('&CAWELAID=', 'UTF-8') );
	
			oprice = Digester.autoNormalizaMoeda(it.Price.text(), false, new Locale("pt", "BR"));
			fprice = oprice;
			nparcelas = it.Number_of_quotes.text();                    
			vparcelas = it.Value_of_quotes.text();
			image = it.Image.text();

			c1 = it.Title.text();
     		c2 = "empty";
     		c3 = "empty"
			categoria = c1;
			cta = it.Title.text();

			fprice = fprice==null||fprice==""?"0,00":fprice;
			oprice = oprice==null||oprice==""?"0,00":oprice;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;
               
			String ativo = (image == null || image.trim() == '' ? '0' : '1');
			if( ativo == '1' && ( Double.parseDouble(it.Price.text()) < 500.0d || Double.parseDouble(it.Price.text()) > 2699.0d ) )
				ativo = '0';
			if( sku == 'xps-8700' || sku == 'cax8700w8s161101brp352w1' ) 
				ativo = '0';
		
			
			int nrecord = 0;
			PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where sku = ?");
			pStmt.setString(1, sku);
			ResultSet res = pStmt.executeQuery();
			if( res.next() ) nrecord = res.getInt(1);
			ConnHelper.closeResources(pStmt, res);
			
			if( nrecord == 0 ) {
				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'cta', columnType: 'string', columnValue: cta, lookupChange: true, platformType: 'NA') );
				DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			} else { 
				pStmt = conn.prepareStatement("update catalogo set last_update = now(), link = ? where sku = ? or (referencia is not null and referencia = ?)");
				pStmt.setString(1, url);
				pStmt.setString(2, sku);
				pStmt.setString(3, sku);
				nrecord = pStmt.executeUpdate();
				if( nrecord > 0 ) println('**** ATUALIZOU REF: ' + sku + ' / ' + nrecord);
			}
		
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
	}

    private static markXml2 ( String file ) {
        Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
	
		String sku, c1, c2, c3, c4, categoria, modelId;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;

        def rss = new XmlParser().parse(file);
        def list = rss.Product
        
		java.util.Map<String, java.util.Map > listaModelId = new java.util.HashMap(); 
		
		list.each {
			sku = it.ProductID.text();
			modelId = it.ModelID.text();
			
			image = it.Image800x800.text();
			//nome = Digester.stripTags(Digester.ISO2UTF8(it.ProductName.text())) 
			nome = Digester.stripTags( it.ProductName.text() ) 
			if( nome.indexOf('-') > -1 )
                                nome = nome.substring(0, nome.indexOf('-') );
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");

            url = it.GenericURL.text()
			if( url.indexOf('?') > -1 )
				url += '&dgc=BA&cid=278726&lid=5265197';
				
			oprice = it.PriceWithDecimals.text()
			if(oprice.indexOf(",") > -1) {
				int tam = oprice.indexOf(",") + 3;
				if(tam > oprice.length()) tam = oprice.length();

				oprice = oprice.substring(0, tam);
			}
			oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"));
			fprice = oprice

			nparcelas = 1;                    
			vparcelas = fprice;
			image = it.Image.text();
			
			c1 = it.Category.text();
         	c2 = it.Systemtype.text();
         	c3 = it.SystemModel.text();
         	c4 = it.Type.text();
         	
         	if( c3 == null || c3 == '' ) c3 = 'empty';
         	if( c4 == null || c4 == '' ) c4 = 'empty';
         	
			categoria = c1 + '/' + c2 + '/' + c3 + "/" + c4;

			fprice = fprice==null||fprice==""?"0,00":fprice;
			oprice = oprice==null||oprice==""?"0,00":oprice;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;


			String ativo = (image == null || image.trim() == '' ? '0' : '1');
                        if( ativo == '1' && ( Double.parseDouble(it.Price.text()) < 500.0d || Double.parseDouble(it.Price.text()) > 2699.0d ) )
                                ativo = '0';
			if( sku == 'xps-8700' || sku == 'cax8700w8s161101brp352w1' )
                                ativo = '0';

			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria4', columnType: 'string', columnValue: c4, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'cta', columnType: 'string', columnValue: (c4 == null || c4 == '' ? 'Dell' : c4 ), lookupChange: true, platformType: 'NA') );

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( modelId != null && modelId.trim() != '' ) {
				if( listaModelId.containsKey( modelId ) ) { 
					java.util.Map dadosProduto = listaModelId.get( modelId );
					double precoAtual = Double.parseDouble( it.Price.text() );
					Double precoAnterior = dadosProduto.get("preco");
					if( precoAtual < precoAnterior && image != null && image.trim() != '' ) {
						dadosProduto.put("lista", fields);
						dadosProduto.put("sku", new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: modelId, lookupChange: false, platformType: 'NA'));
						dadosProduto.put("ativo", new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA'));
						dadosProduto.put("ref", new FieldMetadata(columnName : 'referencia', columnType: 'string', columnValue: sku, lookupChange: true, platformType: 'NA'));
					}
				} else { 
					java.util.Map dadosProduto = new java.util.HashMap();
					dadosProduto.put("preco", Double.parseDouble( it.Price.text() ));
					dadosProduto.put("lista", fields);
					dadosProduto.put("ref", new FieldMetadata(columnName : 'referencia', columnType: 'string', columnValue: sku, lookupChange: true, platformType: 'NA'));
					dadosProduto.put("sku", new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: modelId, lookupChange: false, platformType: 'NA'));
					dadosProduto.put("ativo", new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA'));
					listaModelId.put(modelId, dadosProduto);
				}
			}
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();
		}
		
		for(java.util.Map.Entry entryProduto : listaModelId.entrySet() ) { 
			java.util.Map dadosProduto = entryProduto.getValue();
			java.util.List<FieldMetadata> lista = (java.util.List<FieldMetadata>)dadosProduto.get("lista");
			lista.add( (FieldMetadata) dadosProduto.get("ref") );
			DigesterV2.salvaCatalogoMetadata(conn, dadosProduto.get("sku"), dadosProduto.get("ativo"), lista);
		}
	}
    
    public static void processaRecomendacoes () { 
    	Connection conn = ConnHelper.get().reopen();
    	
    	java.util.List< java.util.Map<String, Object> > listProdutos = getArvoreDistinta( conn );
    	for( java.util.Map<String, Object> dadosProduto : listProdutos ) {
    		//if( dadosProduto.get("ativo").toString().equals("1")) {
	    		java.util.List< java.util.Map<String, Object> > copiaListProdutos = new ArrayList( listProdutos.size() );
	    		copiaListProdutos.addAll( listProdutos );
				dadosProduto.put("recomendacoes", populaRecomendacoes( dadosProduto, copiaListProdutos ));
				
				PreparedStatement pStmt = conn.prepareStatement("UPDATE catalogo SET recomendacoes = ? WHERE sku = ?");
				pStmt.setString(1, dadosProduto.get("recomendacoes").toString() );
				pStmt.setString(2, dadosProduto.get("sku").toString() );
				pStmt.executeUpdate();
    		//}
    	}
    }
    
    
    public static java.util.List< java.util.Map<String, Object> > getArvoreDistinta ( Connection conn ) { 
    	java.util.List< java.util.Map<String, Object> > mapListDados = new java.util.ArrayList();
    	PreparedStatement pStmt = conn.prepareStatement("select categoria1, categoria2, categoria3, categoria4, sku, preco_promocional, ativo, nome from catalogo");
		ResultSet res = pStmt.executeQuery();
		while(res.next()) {
			java.util.Map<String, Object> dados = new java.util.HashMap();
			dados.put("categoria1", res.getString(1));
			dados.put("categoria2", res.getString(2));
			dados.put("categoria3", res.getString(3));
			dados.put("categoria4", res.getString(4));
			dados.put("sku", res.getString(5));
			dados.put("ativo", res.getString(7));
			dados.put("nome", res.getString(8));
			
			String preco = res.getString(6);
			preco = preco.replaceAll("[^0-9]", "");
			dados.put("preco", ( (Double.parseDouble( preco ) / 100) ) );
			mapListDados.add( dados );
		}
		ConnHelper.closeResources(pStmt, res);
		return mapListDados;
    }
    
    public static String populaRecomendacoes ( final java.util.Map<String, Object> produto, java.util.List< java.util.Map<String, Object> > listProdutosAtivos ) { 
    	//println("*********************************************");
    	//println("recomendacoes produto: " + produto.get("preco") + " -> " + produto.get("nome") + " [" + produto.get("categoria1") + " / " + produto.get("categoria2") + " / " + produto.get("categoria3") + "]" );
    	println("");
    	println("");
    	println("");
    	println("ativo;"+produto.get("ativo"));
    	println("sku;"+produto.get("sku"));
    	println("nome;"+produto.get("nome"));
    	println("preco;"+produto.get("preco"));
    	println("categoria1;"+produto.get("categoria1"));
    	println("categoria2;"+produto.get("categoria2"));
    	println("categoria3;"+produto.get("categoria3"));
    	
    	for( int x = 0; x < listProdutosAtivos.size(); x++ ) {
    		java.util.Map<String, Object> produto1 = listProdutosAtivos.get(x);
    		int rating = 0;
    		if( produto1.get("ativo").toString().equals( "1" ) )
    			rating = 1000000;
    		
    		if( produto1.get("sku").toString().equals( produto.get("sku").toString() ) )
    			rating = -100000;
    		
    		if( produto.get("categoria1").toString().equals( produto1.get("categoria1").toString() ) && 
					produto.get("categoria2").toString().equals( produto1.get("categoria2").toString() ) && 
					produto.get("categoria3").toString().equals( produto1.get("categoria3").toString() ) && 
					produto.get("categoria4").toString().equals( produto1.get("categoria4").toString() ) ) { 
    			rating += 500000;
			}
    		
    		if( produto.get("categoria1").toString().equals( produto1.get("categoria1").toString() ) && 
					produto.get("categoria2").toString().equals( produto1.get("categoria2").toString() ) && 
					produto.get("categoria3").toString().equals( produto1.get("categoria3").toString() ) )
    			rating += 200000;
			else if( produto.get("categoria1").toString().equals( produto1.get("categoria1").toString() ) && 
					produto.get("categoria2").toString().equals( produto1.get("categoria2").toString() ) )
				rating += 100000;
			else if( produto.get("categoria1").toString().equals( produto1.get("categoria1").toString() ) )
				rating += 50000;
    		
    		double precoProduto = (Double)produto.get("preco");
			double precoProduto1 = (Double)produto1.get("preco");
			rating -= Math.abs( (precoProduto - precoProduto1) );
			
			produto1.put("rating", rating);
    	}
    	
    	Collections.sort(listProdutosAtivos, new Comparator<java.util.Map<String, Object>>() {
    		public int compare(java.util.Map<String, Object> produto1, java.util.Map<String, Object> produto2) { 
    			if( ((Integer)produto1.get("rating")) > ((Integer)produto2.get("rating")) )
    				return -1;
    			else if( ((Integer)produto1.get("rating")) == ((Integer)produto2.get("rating")) )
    				return 0;
    			else
    				return 1;
    		}
    	});
    	
    	/**int index = 0;
    	for( java.util.Map<String, Object> dadosProduto : listProdutosAtivos ) { 
    		println("ordem;"+dadosProduto.get("ativo")+";" + dadosProduto.get("sku")+";" + dadosProduto.get("rating")+";" + dadosProduto.get("preco") + ";" + dadosProduto.get("nome") + ";" + dadosProduto.get("categoria1") + ";" + dadosProduto.get("categoria2") + ";" + dadosProduto.get("categoria3"));
    		if( ++index == 20 )
    			break;
    	}**/
    	
    	java.util.List<String> recomendacoes = new java.util.ArrayList();
    	java.util.List<String> nomesRecomendados = new java.util.ArrayList();

		String prefixoNome = (String) produto.get("nome"); 
		prefixoNome = prefixoNome.substring(0, (int) (prefixoNome.length() / 2) );
		if( prefixoNome.indexOf("-") > -1  )
			prefixoNome = prefixoNome.substring(0, prefixoNome.indexOf("-"));
    	nomesRecomendados.add( prefixoNome );
    	
    	println("")
    	println("tipo;ativo;sku;preco;nome;categoria1;categoria2;categoria3");
    	
    	for( java.util.Map<String, Object> dadosProduto : listProdutosAtivos ) { 
			if( produto.get("sku").toString().equals( (String) dadosProduto.get("sku") ) )
				continue;

			if( !dadosProduto.get("ativo").toString().equals( "1" ) )
				continue;
			
			prefixoNome = (String) dadosProduto.get("nome"); 
			prefixoNome = prefixoNome.substring(0, (int) (prefixoNome.length() / 2) );
			if( prefixoNome.indexOf("-") > -1  )
				prefixoNome = prefixoNome.substring(0, prefixoNome.indexOf("-"));
			
    		if( nomesRecomendados.indexOf( prefixoNome ) == -1 ) { 
    			
    			recomendacoes.add( (String) dadosProduto.get("sku") );
    			nomesRecomendados.add( prefixoNome );
    			//println("rec1["+dadosProduto.get("ativo")+"]: >> " + dadosProduto.get("preco") + " -> " + dadosProduto.get("nome").toString().substring(0, ( dadosProduto.get("nome").toString().length() > 25 ? 25 : dadosProduto.get("nome").toString().length() ) ) + " [" + dadosProduto.get("categoria1") + " / " + dadosProduto.get("categoria2") + " / " + dadosProduto.get("categoria3") + "]" );
    			println("rec1;"+dadosProduto.get("ativo")+";" + dadosProduto.get("sku")+";" + dadosProduto.get("preco") + ";" + dadosProduto.get("nome") + ";" + dadosProduto.get("categoria1") + ";" + dadosProduto.get("categoria2") + ";" + dadosProduto.get("categoria3"));
    			if( recomendacoes.size() == 7 )
    				break;
    		}
    	}
    	
    	if( recomendacoes.size() < 7 ) { 
    		for( java.util.Map<String, Object> dadosProduto : listProdutosAtivos ) { 
    			if( produto.get("sku").toString().equals( (String) dadosProduto.get("sku") ) )
    				continue;
    			if( !dadosProduto.get("ativo").toString().equals( "1" ) )
    				continue;
    			
    			if( recomendacoes.indexOf( (String) dadosProduto.get("sku") ) == -1  ) {
    				recomendacoes.add( (String) dadosProduto.get("sku") );
    				//println("rec2["+dadosProduto.get("ativo")+"]: >> " + dadosProduto.get("preco") + " -> " + dadosProduto.get("nome").toString().substring(0, ( dadosProduto.get("nome").toString().length() > 25 ? 25 : dadosProduto.get("nome").toString().length() ) ) + " [" + dadosProduto.get("categoria1") + " / " + dadosProduto.get("categoria2") + " / " + dadosProduto.get("categoria3") + "]" );
    				println("rec2;"+dadosProduto.get("ativo")+";" + dadosProduto.get("sku")+";" + dadosProduto.get("preco") + ";" + dadosProduto.get("nome") + ";" + dadosProduto.get("categoria1") + ";" + dadosProduto.get("categoria2") + ";" + dadosProduto.get("categoria3"));
    			}
    			
    			if( recomendacoes.size() == 7 )
    				break;
    		}
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	for(String skuRec : recomendacoes ) {
    		if( sb.length() > 0 ) sb.append(",");
    		sb.append( skuRec );
    	}
    	
    	return sb.toString();
    }
    
	
	public static void main (String [] args ) {
		
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml2( '/mnt/XML/dell2.xml' );
			markXml( '/mnt/XML/dell.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			processaRecomendacoes();
			//Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.DIFFERENT_NAME);
			Digester.bestSellers(ConnHelper.get().reopen());
            DigesterV2.validacao(ConnHelper.get().reopen(), 30, 8.0);
        } else
	        if(args[0] == 'BEST_SELLERS') {
                Digester.bestSellers(ConnHelper.get().reopen());
	        }
	        
    }
}



