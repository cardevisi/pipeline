import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([
    @Grab(group='org.apache.poi', module='poi', version='3.7'),
    @Grab(group='commons-codec', module='commons-codec', version='1.6'),
    @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
    @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
    @GrabConfig(systemClassLoader=true)
])

class DigesterHotelUrbano {
    static int key;
    static {
        ConnHelper.schema = "dynad_hotelurbano";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
                key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }

    public static boolean _DEBUG_ = false;


    public static String unaccent(String s) {
        String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
        return normalized.replaceAll("[^\\p{ASCII}]", "");
    }

    private static markXml ( String file ) {

        Connection conn = ConnHelper.get().reopen();

        int cline = 0;
        def p = null;
        String line = null;

        String sku, c1, c2, c3, c4, c5;
        String image, oprice, fprice, nparcelas, vparcelas;
        String url, marca, nome, codigo, promover, desconto;
        String dataInicio, dataFim, numDiarias, numPessoas, estado, regiao, pais, cidade, cta;
        String descricao, ativo;

        def summary = new XmlParser().parse(file);
        def list = summary.product;

        list.each {
            /***
            - sku
            - nome
            - preco_original
            - preco_promocional (quando nao houver replica original)
            - numero_parcelas (quando nao houver informa 1)
            - valor_parcela (quando nao houver replicar o original)
            - link
            - imagem (400x400)
            - categororia[1,2,3]
            - ativo (instock)
            **/

            sku = it.'@id';
            ativo = it.instock.text() == "1" ? "1" : "0";
            nome = it.Establishment.text().trim();
        if( nome.length() > 20 )
                nome = it.nome.text().trim();
            descricao = it.text_70.text().trim();
                if( descricao.length() > 30 )
                        descricao = it.text_25.text().trim();
            url = it.producturl.text();

            fprice = 'R\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR")), false, new Locale("pt", "BR"))
            oprice = 'R\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.retailprice.text(), false, new Locale("pt", "BR")), false, new Locale("pt", "BR"))
            desconto = it.discount.text();
            image = it.bigimage.text();

            nparcelas = "1";
            vparcelas = fprice;

            c1 = it.categoryid1.text();
            String [] temp = it.name.text().split(",");
            c2 = temp[1]; //RJ
            c3 = temp[0]; //Barra do Piraí

            if( c2 == null || c2.trim().equals("") ) c2 = "empty";
            if( c3 == null || c3.trim().equals("") ) c3 = "empty";

            FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
            FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA');
            java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
            fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2 + '/' + c3), lookupChange: true, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
            fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
            fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'desconto', columnType: 'string', columnValue: desconto, lookupChange: false, platformType: 'NA') );
            fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: descricao, lookupChange: true, platformType: 'NA') );
            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
            if( ++cline % 100 == 0 )
                conn = ConnHelper.get().reopen();
        }

    }

    public static void main (String [] args ) {
        if(args.length == 0 || args[0] != 'BEST_SELLERS') {
            markXml( '/mnt/TMP/xml/hu-hoteis.xml' );
            markXml( '/mnt/TMP/xml/hu-ofertas.xml' );
            DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
            DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
            Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11);
        }

        ///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, descricao, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and descricao is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and recomendacoes is not null and recomendacoes <> ''");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
            new File("/mnt/TMP/sna-files/hotelurbano.txt").withWriter { out ->
                for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                    out.print("\n");
                    while( res.next() ) {
                        out.print(res.getString(1)+"|"+res.getString(2));
                        out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                    for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
                        out.print("\n");
                        out.print("_"+res.getString(1)+"|"+res.getString(2)+"\n");
                    }
            }
        ConnHelper.closeResources(pStmt, res);


    }

    private static String removeAcentos (String string){
        string = string.replaceAll("[ÂÀÁÄÃ]","A");
        string = string.replaceAll("[âãàáä]","a");
        string = string.replaceAll("[ÊÈÉË]","E");
        string = string.replaceAll("[êèéë]","e");
        string = string.replaceAll("ÎÍÌÏ","I");
        string = string.replaceAll("îíìï","i");
        string = string.replaceAll("[ÔÕÒÓÖ]","O");
        string = string.replaceAll("[ôõòóö]","o");
        string = string.replaceAll("[ÛÙÚÜ]","U");
        string = string.replaceAll("[ûúùü]","u");
        string = string.replaceAll("Ç","C");
        string = string.replaceAll("ç","c");
        string = string.replaceAll("[ýÿ]","y");
        string = string.replaceAll("Ý","Y");
        string = string.replaceAll("ñ","n");
        string = string.replaceAll("Ñ","N");
        return string;
    }

    private static String getStringForTracking( String conteudo ) {
        if( conteudo == null ) return null;
        conteudo = removeAcentos( conteudo );
        conteudo = Normalizer.normalize( conteudo, Normalizer.Form.NFD);
        conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
        return conteudo.replaceAll("[^\\w]","-").toLowerCase();
    }


    private static String getValidParameters ( String productURL, String [] arrSkipParameters ) {
        StringBuilder sb = new StringBuilder();
        String urlParam = productURL.indexOf('?') > -1 ? ( productURL.substring( productURL.indexOf('?') + 1 ) ) : "";
        if( urlParam.trim().equals("") )
            return productURL;

        productURL = productURL.substring(0, productURL.indexOf('?'));
        for( String paramEntry : urlParam.split("&") ) {
            String paramName = paramEntry.substring(0, ( paramEntry.indexOf("=") > 0 ? paramEntry.indexOf("=") : 0 ) );
            String paramValue = ( paramEntry.indexOf("=") > 0 ? paramEntry.substring( paramEntry.indexOf("=") + 1 ) : "" );
            if( paramName.startsWith("utm_") )
                continue;

            boolean found = false;
            for(String skipParamName : arrSkipParameters ) {
                if( skipParamName.trim().toLowerCase().equals( paramName.trim().toLowerCase() ) ) {
                    found = true;
                    break;
                }
            }

            if( !found ) {
                if(sb.length() > 0 ) sb.append('&');
                sb.append( paramName );
                if( paramValue != '' )
                    sb.append('=').append( paramValue );
            }
        }

        if( productURL.indexOf('?') > -1 && !productURL.endsWith('&') )
            productURL += '&';
        else if( productURL.indexOf('?') == -1 )
            productURL += '?';
        productURL += sb.toString();
        return productURL;
    }

}