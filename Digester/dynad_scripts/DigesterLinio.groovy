import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_linio";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                def list = summary.product

/*
  <product id="7991">
    <name><![CDATA[Los 7 hábitos de la gente altamente efectiva]]></name>
    <smallimage><![CDATA[http://static.linio.com.mx/p/paidos-6325-1997-1-gallery.jpg]]></smallimage>
    <bigimage><![CDATA[http://static.linio.com.mx/p/paidos-6325-1997-1-product.jpg]]></bigimage>
    <producturl><![CDATA[http://www.linio.com.mx/Los-7-habitos-de-la-gente-altamente-efectiva-7991.html]]><![CDATA[?utm_source=google&utm_medium=Display&utm_term=ST380ME08LVJLMX]]></producturl>
    <originalprice><![CDATA[198.00]]></originalprice>
    <finalprice><![CDATA[198.00]]></finalprice>
    <discount></discount>
    <ninstallments></ninstallments>
    <vinstallments></vinstallments>
    <brand><![CDATA[Paidós]]></brand>
    <categorytree><![CDATA[Libros, Revistas y Musica > Desarrollo Humano > Salud, Belleza y Sexualidad > Todos los libros]]></categorytree>
    <category1><![CDATA[Libros, Revistas y Musica]]></category1>
    <category2><![CDATA[Desarrollo Humano]]></category2>
    <category3><![CDATA[Salud, Belleza y Sexualidad]]></category3>
    <category4><![CDATA[Todos los libros]]></category4>
  </product>
*/

                list.each {
                                sku = it.@id
//                              nome = it.name.text()
                                nome = Digester.stripTags(Digester.ISO2UTF8(it.name.text()))
                                url = it.producturl.text()

				if(url.indexOf("?") > -1)
					url = url.substring(0, url.indexOf("?"));

                                fprice = Digester.cortaDecimal(Digester.autoNormalizaMoeda(it.finalprice.text(), false, new Locale("es", "MX")), false, new Locale("es", "MX"));
                                oprice = Digester.cortaDecimal(Digester.autoNormalizaMoeda(it.originalprice.text(), false, new Locale("es", "MX")), false, new Locale("es", "MX"));
                                nparcelas = it.ninstallments.text()
                                vparcelas = Digester.cortaDecimal(Digester.autoNormalizaMoeda(it.vinstallments.text(), false, new Locale("es", "MX")), false, new Locale("es", "MX"));

//                                fprice = Digester.cortaDecimal(it.finalprice.text(), false)
//                                oprice = Digester.cortaDecimal(it.originalprice.text(), false)
//                                nparcelas = it.ninstallments.text()
//                                vparcelas = Digester.cortaDecimal(it.vinstallments.text(), false)

                        	image = it.bigimage.text()
                        	desconto = it.discount.text()
                         	//promover = it.veicular.text()
                         	c1 = it.category1.text()
                         	c2 = it.category2.text()
                         	c3 = it.category3.text()
                         	marca = it.brand.text()

				//workaround
				if(image != null) {
					image = image.replace(".jpeg", ".jpg");
					image = image.replace(".JPEG", ".jpg");
					image = image.replace(".JPG", ".jpg");
					image = image.replace(".png", ".jpg");
					image = image.replace(".PNG", ".jpg");
				}
	
				
//println("sku:"+sku+";nome:"+nome+";url:"+url);
	                                Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', true);
 
				
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/liniomx.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
                        Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

        }
	
}
