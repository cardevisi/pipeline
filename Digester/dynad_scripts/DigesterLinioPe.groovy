import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_liniope";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                def list = summary.product

                list.each {
/*
  <product>
    <id>7090</id>
    <sku>AN242EL09KMSPEAMZ</sku>
    <name>Andes Agenda Planeador Cuadriculado Teens Morado</name>
    <brand>Andes</brand>
    <categorytree>Oficina y utiles  &gt; Cartucheras y Agendas &gt; Agendas</categorytree>
    <category1>Oficina y utiles </category1>
    <category2>Cartucheras y Agendas</category2>
    <category3>Agendas</category3>
    <imagelarge>http://static.linio.com.pe/p/andes-1215-0907-1-zoom.jpg</imagelarge>
    <imagesmall>http://static.linio.com.pe/p/andes-1215-0907-1-list.jpg</imagesmall>
    <url>http://www.linio.com.pe/7090.html</url>
    <description>Andes Agenda Planeador Cuadriculado Teens Morado  - Andes</description>
    <currency>PEN</currency>
    <price>31.15</price>
    <pricespecial/>
    <discountvalue/>
    <discountpercentage/>
    <installmentnumber></installmentnumber>
    <installmentvalue></installmentvalue>
    <freeshipping>0</freeshipping>
    <lastmodified>2013-02-28 15:06:39</lastmodified>
    <changefrecuency>weekly</changefrecuency>
  </product>
*/
                        	sku = it.id.text()
//                        	nome = it.name.text()
                        	nome = Digester.stripTags(Digester.ISO2UTF8(it.name.text()))
                        	url = it.url.text()

                                if(url.indexOf("?") > -1)
                                        url = url.substring(0, url.indexOf("?"));
 
                        	fprice = Digester.autoNormalizaMoeda(it.pricespecial.text(), false, new Locale("es", "PE"));
                        	oprice = Digester.autoNormalizaMoeda(it.price.text(), false, new Locale("es", "PE"));
				if(fprice == "" || fprice == null) fprice = oprice;
                        	nparcelas = it.installmentnumber.text()
                        	vparcelas = Digester.autoNormalizaMoeda(it.installmentvalue.text(), false, new Locale("es", "PE"));

                        	image = it.imagelarge.text()
                        	desconto = it.discountpercentage.text()
                         	//promover = it.veicular.text()
                         	c1 = it.category1.text()
                         	c2 = it.category2.text()
                         	c3 = it.category3.text()
                         	marca = it.brand.text()

				//workaround
				if(image != null) {
					image = image.replace(".jpeg", ".jpg");
					image = image.replace(".JPEG", ".jpg");
					image = image.replace(".JPG", ".jpg");
					image = image.replace(".png", ".jpg");
					image = image.replace(".PNG", ".jpg");
				}
	
				
//println("sku:"+sku+";nome:"+nome+";c1:"+c1+";image:" + image + ";brand:" + marca);
	                                Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false);
 
				
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
//http://www.linio.com.pe/products.xml
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/liniope.xml' );
                        Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
                        Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
                        Digester.bestSellers(ConnHelper.get().reopen());
                        //Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

	}
	
}
