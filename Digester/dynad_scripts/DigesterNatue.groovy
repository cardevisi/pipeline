import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_natue";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;

                def summary = new XmlParser().parse(file);
                summary.record.each {
/*

<record>
<sku><![CDATA[1591041032]]></sku>
<product_name><![CDATA[Fiber Drink Chá Verde, sabor Maça Verde]]></product_name>
<Price_with_discount><![CDATA[22.30]]></Price_with_discount>
<Price><![CDATA[]]></Price>
<Brand><![CDATA[Slim30]]></Brand>
<Discount><![CDATA[]]></Discount>
<Product_image><![CDATA[http://cdn.natue.com.br/BR/product/fiber-drink-cha-verde-sabor-maca-verde-220g-slim30-5801-4893-1085-1-productbig.jpg]]></Product_image>
<Free_freight><![CDATA[]]></Free_freight>
<New><![CDATA[]]></New>
<Buy1Get2><![CDATA[]]></Buy1Get2>
<product_information><![CDATA[220g]]></product_information>
<URL><![CDATA[http://www.natue.com.br/fiber-drink-cha-verde-sabor-maca-verde-220g-slim30-5801.html]]></URL>
</record>

*/

                                sku = it.sku.text()
//                              nome = it.product_name.text()
                                nome = Digester.stripTags(Digester.ISO2UTF8(it.product_name.text()))
                                url = it.URL.text()

				if(url.indexOf("?") > -1) {
					url = url.substring(0, c);
				}

                                fprice = Digester.autoNormalizaMoeda(it.Price_with_discount.text(), false, new Locale("pt", "BR"))
                                oprice = Digester.autoNormalizaMoeda(it.Price.text(), false, new Locale("pt", "BR"))
				if(oprice == '') oprice = fprice
                                nparcelas = '1'
                                vparcelas = fprice

                        	image = it.Product_image.text()
                         	promover = it.DISPONIBILIDADE.text() == '1' ? '1' : '0'
                         	c1 = it.Brand.text()
                         	c2 = ''
                         	c3 = ''
                         	marca = it.Brand.text()
				promover = '1'

//println("sku:"+sku+";nome:"+nome+";url:"+url);
				if(promover == '1')
	                                Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, '1', false);
 
				
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/natue.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
                        Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

        }
	
}
