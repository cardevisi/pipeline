import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class DigesterNatue {	
	
	private static markXml(String file) {
		def rss = new XmlParser().parse(file);
		def list = rss.record
		def sku, imagem, url, descricao, callToAction, titulo = null;

		new File("/mnt/natue.txt").withWriter { out ->
		    list.each {
				sku = it['sku'].text();
				imagem = it['imagem_do_anuncio'].text();
				url = it['URL_de_destino'].text();
				descricao = it['descricao1'].text();
				callToAction = it['descricao2'].text();
				titulo = it['Titulo'].text();
				out.writeLine(sku + '|' + imagem + '|' + url + '|'	+ descricao + '|' + callToAction + '|' + titulo);
				println sku;
			}
		}	
		println 'Natue - UOL Cliques - Feito';	
	}

	public static void main(String[] args) {
		markXml('/mnt/XML/natue.xml');		
	}
}