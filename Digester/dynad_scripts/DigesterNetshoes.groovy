import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
        static int key;
        static {
                ConnHelper.schema = "dynad_netshoes";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;

        private static markXml ( String file ) {

                Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, codigo, marca, nome, promover, desconto;
		Long score, classe

                def summary = new XmlParser().parse(file);
                def list = summary.PRODUTO

                list.each {
/*

  <PRODUTO>
    <ID_PRODUTO>418-0003-397</ID_PRODUTO>
    <LINK_PRODUTO><![CDATA[http://www.netshoes.com.br/produto/418-0003-397-01?utm_source=me-s_uol_tnrg_&utm_medium=vitrine&utm_campaign=me-s_uol-_-tnrg_cama-mesa-e-banho_tcdria_tnrg__acod-_-vitrine-_-_var_me__conjunto-de-copos-palmeiras-long-drink-c-24-unidades-__]]></LINK_PRODUTO>
    <TITULO>Conjunto de Copos Palmeiras Long Drink c/ 24 unidades</TITULO>
    <PRECO>129,90</PRECO>
    <PARCELAMENTO>ou 5x de R$25,98 sem juros</PARCELAMENTO>
    <DISPONIBILIDADE></DISPONIBILIDADE>
    <IMAGEM>http://www.netshoes.com.br/Produtos/97/418-0003-397/418-0003-397_vitrine.jpg</IMAGEM>
    <CATEGORIA>Cama, Mesa e Banho</CATEGORIA>
    <INDICE>0.00</INDICE>
    <GENERO>Unisex</GENERO>
    <CLASSE>Sandalias</CLASSE>
  </PRODUTO>

*/
                                sku = it.ID_PRODUTO.text()
                                nome = Digester.stripTags(Digester.ISO2UTF8(it.TITULO.text()))
                                //nome = it.nome_produto.text()
                                url = it.LINK_PRODUTO.text()

//				if(url.indexOf("?") > -1)
//					url = url.substring(0, url.indexOf("?"));

                                oprice = Digester.autoNormalizaMoeda(it.PRECO_DE.text(), false, new Locale("pt", "BR"));
                                fprice = Digester.autoNormalizaMoeda(it.PRECO_POR.text(), false, new Locale("pt", "BR"));
				def tmp = it.PARCELAMENTO.text()
				def t;
				if((t = tmp.indexOf("x")) >-1) {
					def num = tmp.substring(0,t).replace("ou", "").trim();
					def resto = tmp.substring(t+1).trim();
					def p2 = resto.indexOf(" ");
					if(p2 > 0) resto = resto.substring(p2).trim();
					p2 = resto.indexOf(" ");
					if(p2 > 0) resto = resto.substring(0,p2).trim();
					nparcelas = num;
					vparcelas = resto;
				}
//                                nparcelas = it.Parcelas.text();
//				java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
//                                vparcelas = Digester.autoNormalizaMoeda(""+df.format(Float.parseFloat(Digester.autoNormalizaMoeda(it.PrecoPor.text(), true, new Locale("en", "US"))) / Integer.parseInt(nparcelas)), false, new Locale("pt", "BR"));

                        	image = it.IMAGEM.text();
				if( image != null && image.endsWith('_vitrine.jpg') ) { 
					image = image.replaceAll('_vitrine\\.jpg', '_detalhe1.jpg');
				}
                        	score = 0
                        	try { score = (long)(Math.round(Float.parseFloat(it.INDICE.text()) * 100)) } catch(Exception e){}

				c1 = it.GENERO.text()
                         	c2 = it.CATEGORIA.text()
				c3 = it.CLASSE.text()

                         	marca = "";

				fprice = "R\$ " + fprice==null||fprice==""?"0,00":fprice;
				oprice = "R\$ " + oprice==null||oprice==""?"0,00":oprice;
				vparcelas = "R\$ " + vparcelas==null||vparcelas==""?"0,00":vparcelas;

				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
                                FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');

				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
                                fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2 + '/' + c3), lookupChange: true, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
                                fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
                                fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
                                fields.add( new FieldMetadata(columnName : 'score', columnType: 'double', columnValue: score, lookupChange: false, platformType: 'NA') );

                                DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
                                if( ++cline % 100 == 0 )
                                        conn = ConnHelper.get().reopen()
		}
		
	}
	
	static String getField ( String buffer ) {
		def pi;// =  buffer.indexOf( '<![CDATA[' );
		def pf;// =  buffer.indexOf( ']]>' );
		def entrou = false;
		def retorno = "";

		while((pi = buffer.indexOf( '<![CDATA[' )) != -1 && (pf = buffer.indexOf( ']]>' )) != -1 && pi < pf) {
			entrou = true;

			if(pi+9<pf) {
				retorno += buffer.substring( pi + 9 , pf );
			}
			buffer = buffer.substring(pf + 3);
		}

		if(!entrou) {
			pi = buffer.indexOf('>');
			pf = buffer.indexOf('</');
			try {retorno = buffer.substring( pi + 1 , pf );} catch(Exception ex) { println(buffer); ex.printStackTrace(); }
		}

		return retorno.replace("%20", " ");
	}
	  
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/netshoes.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11, RecommenderMode.DIFFERENT_NAME);
			Digester.bestSellers(ConnHelper.get().reopen());
			//Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
                }

		///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes,categoria3 from catalogo");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/netshoes.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                                for(int i=4; i<=rsmd.getColumnCount()-2; i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
                                def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
                                        PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
                                        ResultSet res2 = pStmt2.executeQuery();
                                        def recs = "";
                                        while( res2.next() ) { recs += (recs==""?"":",") + res2.getString(1); }
                                        ConnHelper.closeResources(pStmt2, res2);
                                out.print("|"+recs+"|"+res.getString(12));
				out.print("\n");

				out.print(res.getString(2)+"|"+res.getString(1));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
				for(int i=4; i<=rsmd.getColumnCount()-2; i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
				out.print("|"+recs+"|"+res.getString(12));
				out.print("\n");

                                out.println("_" + res.getString(2) + "|" + res.getString(1));
                        }
                }
                ConnHelper.closeResources(pStmt, res);


        }
	
}
