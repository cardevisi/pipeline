import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class DigesterVivara {
	static int key;
	static {
		ConnHelper.schema = "dynad_onofre";
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
		ResultSet res = pStmt.executeQuery();
		if (res.next()) key = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
	}

	public static boolean _DEBUG_ = false;


	public static String unaccent(String s) {
		String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
		return normalized.replaceAll("[^\\p{ASCII}]", "");
	}

	private static markXml(String file) {

		Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;

		String sku, c1, c2, c3, c4, c5;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto, descricao;
		String dataInicio, dataFim, numDiarias, numPessoas, estado, regiao, pais, cidade, cta;
		Float latitude, longitude

		def summary = new XmlParser().parse(file);
		def list = summary.PRODUTO


		list.each {
			sku = it.CODIGO.text();
			if (sku == null || sku.trim() == '') return;

			nome = it.DESCRICAO.text().trim();
			url = it.URL.text();
			if (url != null && !url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;

			image = it.URL_IMAGEM.text();
			if (image != null && !image.startsWith("http://") && !image.startsWith("https://")) image = "http://" + image;

			oprice = DigesterV2.autoNormalizaMoeda(it.PRECO.text(), false, new Locale("pt", "BR"))
			fprice = DigesterV2.autoNormalizaMoeda(it.PRECO_PROMOCIONAL.text(), false, new Locale("pt", "BR"))
			nparcelas = it.NPARCELA.text();
			vparcelas = DigesterV2.autoNormalizaMoeda(it.VPARCELA.text(), false, new Locale("pt", "BR"))

			FieldMetadata fSku = new FieldMetadata(columnName: 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName: 'ativo', columnType: 'string', columnValue: (image != null && image != '' ? '1' : '0'), lookupChange: false, platformType: 'NA');
			java.util.List < FieldMetadata > fields = new java.util.ArrayList < FieldMetadata > ();
			fields.add(new FieldMetadata(columnName: 'categoria', columnType: 'string', columnValue: 'categoria/produto/unico', lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria1', columnType: 'string', columnValue: 'categoria', lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria2', columnType: 'string', columnValue: 'produto', lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria3', columnType: 'string', columnValue: 'unico', lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG'));
			fields.add(new FieldMetadata(columnName: 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK'));
			fields.add(new FieldMetadata(columnName: 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA'));

			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
		}

	}

	public static void main(String[] args) {
		if (args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml('/mnt/TMP/xml/onofre.xml');
			DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
			DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			DigesterV2.bestSellers(ConnHelper.get().reopen());
		} else if (args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen());
		}

		///////// EXPORT SUPERNOVA 2 ///////////
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
		ResultSet res = pStmt.executeQuery();
		java.sql.ResultSetMetaData rsmd = res.getMetaData();
		new File("/mnt/TMP/sna-files/onofre.txt").withWriter {
			out ->
			for (int i = 1; i <= rsmd.getColumnCount(); i++) out.print((i > 1 ? "|" : "metadata|") + rsmd.getColumnName(i));
			out.print("\n");
			while (res.next()) {
				out.print(res.getString(1) + "|" + res.getString(2));
				out.print("|http://static.dynad.net/let/" + URLCodec.encrypt(res.getString(3)));
				for (int i = 4; i <= rsmd.getColumnCount(); i++) {					
					if (res.getString(i)) {						
						if (i==6) {
							String url = (java.net.URLDecoder.decode(res.getString(i).replace("|", " "), "UTF-8")).split('&redirect_url=')[1];
							url = url.split('\\?')[0];
							out.print("|" + url);							
						}
						else out.print("|" + res.getString(i).replace("|", " "));		
					}
				}
				out.print("\n");
				out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
			}
		}
		ConnHelper.closeResources(pStmt, res);
		println 'SN2 - DATA GENERATED';
		
		///////// EXPORT UOL CLIQUES ///////////
		java.util.List < String > listTopSellers = new java.util.ArrayList();
		conn = ConnHelper.get().reopen();
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/onofre_uolcliques.txt").withWriter {
			out -> out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while (res.next()) {
				out.print(res.getString(1) + "|Onofre|" + res.getString(5) + "|");
				if (res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1) out.print('A partir de R$' + res.getString(8));
				else out.print(res.getString(9) + 'x de R$' + res.getString(10));
				out.print('|' + res.getString(6) + '|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3)) + '|' + res.getString(11) + '\n');
				out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
				if (listTopSellers.size() < 10) listTopSellers.add(res.getString(1));
			}
		}
		ConnHelper.closeResources(pStmt, res);

		new File("/mnt/TMP/sna-files/onofre_uolcliques_topsellers.txt").withWriter {
			out -> listTopSellers.each {
				itTS -> out.print(itTS + '\n');
			}
		}
	}

	private static String removeAcentos(String string) {
		string = string.replaceAll("[ÂÀÁÄÃ]", "A");
		string = string.replaceAll("[âãàáä]", "a");
		string = string.replaceAll("[ÊÈÉË]", "E");
		string = string.replaceAll("[êèéë]", "e");
		string = string.replaceAll("ÎÍÌÏ", "I");
		string = string.replaceAll("îíìï", "i");
		string = string.replaceAll("[ÔÕÒÓÖ]", "O");
		string = string.replaceAll("[ôõòóö]", "o");
		string = string.replaceAll("[ÛÙÚÜ]", "U");
		string = string.replaceAll("[ûúùü]", "u");
		string = string.replaceAll("Ç", "C");
		string = string.replaceAll("ç", "c");
		string = string.replaceAll("[ýÿ]", "y");
		string = string.replaceAll("Ý", "Y");
		string = string.replaceAll("ñ", "n");
		string = string.replaceAll("Ñ", "N");
		return string;
	}

	private static String getStringForTracking(String conteudo) {
		if (conteudo == null) return null;
		conteudo = removeAcentos(conteudo);
		conteudo = Normalizer.normalize(conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]", "-").toLowerCase();
	}

}