import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterRakuten {
	static int key;
	static {
		ConnHelper.schema = "dynad_rakuten";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if(res.next()) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();
        java.util.Map<String, String> mapSkusProcessados = new java.util.HashMap();
		int cline = 0;
	
		String sku, nome, link, imagem, marca, c1, c2, c3, precoOriginal, precoPromocional, nparcelas, vparcelas, ativo;
        def xml = new XmlParser().parse(file);
        def produtos = xml.PRODUTO;
                
		produtos.each {			
			ativo = '1';//ATIVO
			sku = it.CODIGO.text();//SKU
			nome = it.DESCRICAO.text();//NOME
    		link = it.URL.text();//LINK
			if(link.indexOf("?") > -1) link = link.substring(0, link.indexOf("?"));
			imagem = it.URL_IMAGEM.text();//IMAGEM									
			c1 = it.DEPARTAMENTO.text();//CATEGORIA 1
			c2 = c1;//CATEGORIA 1
			c3 = c1;//CATEGORIA 1
			precoOriginal = it.PRECO.text();//PRECO ORIGINAL
			precoOriginal = Digester.autoNormalizaMoeda(precoOriginal, false, new Locale("pt", "BR"));			
			precoPromocional = it.PRECO.text();//PRECO PROMOCIONAL
			precoPromocional = Digester.autoNormalizaMoeda(precoPromocional, false, new Locale("pt", "BR"));
			nparcelas = it.NPARCELA.text();//NUMERO DE PARCELAS	
    		vparcelas = it.VPARCELA.text();//VALOR DAS PARCELAS
    		vparcelas = Digester.autoNormalizaMoeda(vparcelas, false, new Locale("pt", "BR"));
			
			precoOriginal = precoOriginal==null||precoOriginal==""?"0,00":precoOriginal;
			precoPromocional = precoPromocional==null||precoPromocional==""?"0,00":precoPromocional;
			vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;					
			
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: link, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: imagem, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );		
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: precoOriginal, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: precoPromocional, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 ) conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/rakuten.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false, 11);
			Digester.bestSellers(ConnHelper.get().reopen());
    		//DigesterV2.validacao(ConnHelper.get().reopen(), 140, 15.0);
        } else if(args[0] == 'BEST_SELLERS') {
        	Digester.bestSellers(ConnHelper.get().reopen());
        }

   		///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/rakuten.txt").withWriter { out ->
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);

        ///////// EXPORT UOL CLIQUES ///////////
		java.util.List < String > listTopSellers = new java.util.ArrayList();
		conn = ConnHelper.get().reopen();
		pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null and ativo = '1'");
		res = pStmt.executeQuery();
		new File("/mnt/TMP/sna-files/rakuten_uolcliques.txt").withWriter {
			out -> out.print("metadata|sku|uolcliques_titulo|uolcliques_linha1|uolcliques_linha2|uolcliques_link|imagem|recomendacoes\n");
			while (res.next()) {
				out.print(res.getString(1) + "|Rakuten|" + res.getString(5) + "|");
				if (res.getString(9) == null || res.getString(9).trim().equals('') || res.getInt(9) <= 1) out.print('A partir de R$' + res.getString(8));
				else out.print(res.getString(9) + 'x de R$' + res.getString(10));
				out.print('|' + getValidParameters(res.getString(6), new String[0], true) + 'utm_source=display_Uol&utm_medium=100x80_Uolcliques&utm_campaign=100x80_Campanha_Uolcliques&origem=uol|http://static.dynad.net/let/' + URLCodec.encrypt(res.getString(3).trim()) + '|' + res.getString(11) + '\n');
				out.print("_" + res.getString(2) + "|" + res.getString(1) + "\n");
				if (listTopSellers.size() < 10) listTopSellers.add(res.getString(1));
			}
		}
		ConnHelper.closeResources(pStmt, res);
		new File("/mnt/TMP/sna-files/rakuten_uolcliques_topsellers.txt").withWriter {
			out -> listTopSellers.each {
				itTS -> out.print(itTS + '\n');
			}
		}
    }

    private static String removeAcentos(String string) {
		string = string.replaceAll("[ÂÀÁÄÃ]", "A");
		string = string.replaceAll("[âãàáä]", "a");
		string = string.replaceAll("[ÊÈÉË]", "E");
		string = string.replaceAll("[êèéë]", "e");
		string = string.replaceAll("ÎÍÌÏ", "I");
		string = string.replaceAll("îíìï", "i");
		string = string.replaceAll("[ÔÕÒÓÖ]", "O");
		string = string.replaceAll("[ôõòóö]", "o");
		string = string.replaceAll("[ÛÙÚÜ]", "U");
		string = string.replaceAll("[ûúùü]", "u");
		string = string.replaceAll("Ç", "C");
		string = string.replaceAll("ç", "c");
		string = string.replaceAll("[ýÿ]", "y");
		string = string.replaceAll("Ý", "Y");
		string = string.replaceAll("ñ", "n");
		string = string.replaceAll("Ñ", "N");
		return string;
	}

	private static String getStringForTracking(String conteudo) {
		if (conteudo == null) return null;
		conteudo = removeAcentos(conteudo);
		conteudo = Normalizer.normalize(conteudo, Normalizer.Form.NFD);
		conteudo = conteudo.replaceAll("[^\\p{ASCII}]", "");
		return conteudo.replaceAll("[^\\w]", "-").toLowerCase();
	}


	private static String getValidParameters(String productURL, String[] arrSkipParameters, boolean addParamDelimiter) {
		StringBuilder sb = new StringBuilder();
		String urlParam = productURL.indexOf('?') > -1 ? (productURL.substring(productURL.indexOf('?') + 1)) : "";
		if (urlParam.trim().equals("")) {
			if (addParamDelimiter) {
				if (productURL.indexOf('?') > -1 && !productURL.endsWith('&')) productURL += '&';
				else if (productURL.indexOf('?') == -1) productURL += '?';
			}
			return productURL;
		}

		productURL = productURL.substring(0, productURL.indexOf('?'));
		for (String paramEntry: urlParam.split("&")) {
			String paramName = paramEntry.substring(0, (paramEntry.indexOf("=") > 0 ? paramEntry.indexOf("=") : 0));
			String paramValue = (paramEntry.indexOf("=") > 0 ? paramEntry.substring(paramEntry.indexOf("=") + 1) : "");
			if (paramName.startsWith("utm_")) continue;

			boolean found = false;
			for (String skipParamName: arrSkipParameters) {
				if (skipParamName.trim().toLowerCase().equals(paramName.trim().toLowerCase())) {
					found = true;
					break;
				}
			}

			if (!found) {
				if (sb.length() > 0) sb.append('&');
				sb.append(paramName);
				if (paramValue != '') sb.append('=').append(paramValue);
			}
		}

		if (productURL.indexOf('?') > -1 && !productURL.endsWith('&')) productURL += '&';
		else if (productURL.indexOf('?') == -1) productURL += '?';
		productURL += sb.toString();

		if (addParamDelimiter) {
			if (productURL.indexOf('?') > -1 && !productURL.endsWith('&')) productURL += '&';
			else if (productURL.indexOf('?') == -1) productURL += '?';
		}

		return productURL;
	}
}