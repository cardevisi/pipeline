import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class DigesterSafarishop {
	static int key;
	static {
		ConnHelper.schema = "dynad_safarishop";
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
		ResultSet res = pStmt.executeQuery();
		if (res.next()) key = res.getInt(1);
		ConnHelper.closeResources(pStmt, res);
	}

	public static boolean _DEBUG_ = false;

	private static markXml(String file, String _mode) {
		Connection conn = ConnHelper.get().reopen();

		int cline = 0;
		def p = null;
		String line = null;
		double comissao;
		String sku, c1, c2, c3, categoria;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;

		def rss = new XmlParser().parse(file);
		def list = (_mode == "1" ? rss.Oferta : rss.OfertaExtra)

		list.each {
			sku = it.Identificador.text();
			nome = Digester.stripTags(Digester.ISO2UTF8(it.Nome.text()))
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");

			url = it.Link.text()
			//if(url.indexOf("?")>-1) url = url.substring(0,url.indexOf("?"));
			//url = url.split("\\?")
			//url = url[0]

			oprice = it.ValorDe.text()
			if (oprice.indexOf(",") > -1) {
				int tam = oprice.indexOf(",") + 3;
				if (tam > oprice.length()) tam = oprice.length();

				oprice = oprice.substring(0, tam);
			}
			oprice = Digester.autoNormalizaMoeda(oprice, false, new Locale("pt", "BR"));

			fprice = it.ValorPor.text()
			if (fprice.indexOf(",") > -1) {
				int tam = fprice.indexOf(",") + 3;
				if (tam > fprice.length()) tam = fprice.length();

				fprice = fprice.substring(0, tam);
			}
			fprice = Digester.autoNormalizaMoeda(fprice, false, new Locale("pt", "BR"));

			nparcelas = it.QuantidadeParcelas.text();
			vparcelas = Digester.autoNormalizaMoeda(it.ValorParcelas.text(), false, new Locale("pt", "BR")); //it.ValorParcelas.text()
			if (vparcelas.indexOf(",") > -1) {
				int tam = vparcelas.indexOf(",") + 3;
				if (tam > vparcelas.length()) tam = vparcelas.length();

				vparcelas = vparcelas.substring(0, tam);
			}
			vparcelas = Digester.autoNormalizaMoeda(vparcelas, false, new Locale("pt", "BR"));

			image = it.ImagemGrande02.text();

			c1 = it.Categorias.categoria.text();
			c2 = it.Categorias.subCategoria.text();
			if (c1 == null || c1.trim() == '') c1 = 'empty';
			if (c2 == null || c2.trim() == '') c2 = 'empty';
			c3 = "empty";
			categoria = c1 + '/' + c2 + '/' + c3;

			fprice = fprice == null || fprice == "" ? "0,00" : fprice;
			oprice = oprice == null || oprice == "" ? "0,00" : oprice;
			vparcelas = vparcelas == null || vparcelas == "" ? "0,00" : vparcelas;
			comissao = 0.0d;
			try {
				comissao = Double.parseDouble(it.Comissao.text());
			} catch (Exception exx) {}

			int produtosDisponiveis = 0;
			try {
				if (it.QuantidadeProdutosDisponiveis != null) {
					produtosDisponiveis = Integer.parseInt(it.QuantidadeProdutosDisponiveis.text());
				} else {
					produtosDisponiveis = 1;
				}
			} catch (Exception exx) {
				produtosDisponiveis = 1;
			}

			FieldMetadata fSku = new FieldMetadata(columnName: 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName: 'ativo', columnType: 'string', columnValue: (produtosDisponiveis > 2 ? '1' : '0'), lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList <FieldMetadata> ();
			fields.add(new FieldMetadata(columnName: 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria1', columnType: 'string', columnValue: c1, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria2', columnType: 'string', columnValue: c2, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG'));
			fields.add(new FieldMetadata(columnName: 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK'));
			fields.add(new FieldMetadata(columnName: 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'comissao', columnType: 'double', columnValue: comissao, lookupChange: false, platformType: 'NA'));
			fields.add(new FieldMetadata(columnName: 'score', columnType: 'double', columnValue: comissao, lookupChange: true, platformType: 'NA'));			
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if (++cline % 100 == 0) conn = ConnHelper.get().reopen();
		}
	}


	public static void completaRecomendacoes(Connection conn) {
		PreparedStatement pStmt = conn.prepareStatement("select id,categoria1, categoria2, categoria3 from codigos_categorias");
		ResultSet res = pStmt.executeQuery();
		while (res.next()) {
			StringBuilder sb = new StringBuilder();
			PreparedStatement pStmt2 = conn.prepareStatement("SELECT sku, comissao, CASE WHEN categoria1 = ? AND categoria2 = ? AND categoria3 = ? THEN 3 WHEN categoria1 = ? AND categoria2 = ? THEN 2 WHEN categoria1 = ? THEN 1 ELSE 0 END as ranking FROM catalogo WHERE ativo = '1' order by 2 DESC, 3");
			pStmt2.setString(1, res.getString(2));
			pStmt2.setString(2, res.getString(3));
			pStmt2.setString(3, res.getString(4));
			pStmt2.setString(4, res.getString(2));
			pStmt2.setString(5, res.getString(3));
			pStmt2.setString(6, res.getString(2));

			ResultSet res2 = pStmt2.executeQuery();
			int recordCount = 0;
			while (res2.next()) {
				if (sb.length() > 0) sb.append(',');
				sb.append(res2.getString(1));
				if (++recordCount == 12) break;
			}
			ConnHelper.closeResources(pStmt2, res2);

			pStmt2 = conn.prepareStatement("UPDATE catalogo set recomendacoes = TRIM(TRAILING ',' FROM substring(replace( concat(',', ?, ','), concat(',', sku, ','), ','), 2) ) WHERE id_categoria = ?");
			pStmt2.setString(1, sb.toString());
			pStmt2.setInt(2, res.getInt(1));
			pStmt2.executeUpdate();
			ConnHelper.closeResources(pStmt2, null);
		}
		ConnHelper.closeResources(pStmt, res);
	}

	public static void main(String[] args) {
		if (args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml('/mnt/XML/safarishop1.xml', "1");			
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);			
			completaRecomendacoes(ConnHelper.get().reopen());
			Digester.bestSellers(ConnHelper.get().reopen());
			DigesterV2.validacao(ConnHelper.get().reopen(), 50, 15.0);

			///////// EXPORT SUPERNOVA 2 ///////////
			Connection conn = ConnHelper.get().reopen();
			PreparedStatement pStmt = conn.prepareStatement("select id, sku, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo");
			ResultSet res = pStmt.executeQuery();
			java.sql.ResultSetMetaData rsmd = res.getMetaData();
			new File("/mnt/safarishop.txt").withWriter { out ->
				for (int i = 1; i <= rsmd.getColumnCount(); i++) out.print((i > 1 ? "|" : "metadata|") + rsmd.getColumnName(i));
				out.print("\n");
				while (res.next()) {					
					out.print(res.getString(1) + "|" + res.getString(2));
					out.print("|http://static.dynad.net/let/" + URLCodec.encrypt(res.getString(3)));			
					for (int i = 4; i <= rsmd.getColumnCount() - 1; i++) {
						if (i == 6) out.print("|" + res.getString(i).replace("|", " ").split('\\?')[0] );//REMOVE OS PARAMETROS DA URL
						else out.print("|" + res.getString(i).replace("|", " "));//APENAS INSERE OS VALORES
					}
					def _recs = "('" + res.getString(11).trim().replaceAll(",", "','") + "')";
					PreparedStatement pStmt2 = conn.prepareStatement("select id from catalogo where sku in " + _recs);
					ResultSet res2 = pStmt2.executeQuery();
					def recs = "";
					while (res2.next()) {
						recs += (recs == "" ? "" : ",") + res2.getString(1);
					}
					ConnHelper.closeResources(pStmt2, res2);
					out.print("|" + recs);
					out.println("\n_" + res.getString(2) + "|" + res.getString(1));
				}
			}
			ConnHelper.closeResources(pStmt, res);
		} else if (args[0] == 'BEST_SELLERS') {
			Digester.bestSellers(ConnHelper.get().reopen());
		}
	}
}