import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterHotelUrbano {
        static int key;
        static {
            ConnHelper.schema = "dynad_shopfato";
            Connection conn = ConnHelper.get().reopen();
            PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
            ResultSet res = pStmt.executeQuery();
            if( res.next() )
                    key = res.getInt(1);
            ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;


		public static String unaccent(String s) {
		    String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
		    return normalized.replaceAll("[^\\p{ASCII}]", "");
		}

        private static markXml ( String file ) {

	        Connection conn = ConnHelper.get().reopen();
	
			int cline = 0;
			def p = null;
			String line = null;
			
			String sku, c1, c2, c3, c4, c5, categoria;
			String image, oprice, fprice, nparcelas, vparcelas;
			String url, marca, nome, codigo, promover, desconto;
			String dataInicio, dataFim, numDiarias, numPessoas, estado, regiao, pais, cidade, cta;
	
	        def summary = new XmlParser().parse(file);
	        def list = summary.PRODUTO
	
	        list.each {
	        	sku = it.CODIGO.text();
	        	nome = it.titulo.text().trim();
	        	url = it.URL.text();
	            oprice = 'R\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.PRECO.text(), false, new Locale("pt", "BR")), false, new Locale("pt", "BR"))
	            fprice = 'R\$ ' + DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda(it.PRECO.text(), false, new Locale("pt", "BR")), false, new Locale("pt", "BR"))
	            desconto = it.desconto.text();
	        	image = it.url_imagem.text()
				marca = it.marca.text()
				
				String NParcelas = it.Nparcela.text();
				if( NParcelas != null && !NParcelas.trim().equals('') ) { 
					nparcelas = NParcelas.substring(0, NParcelas.indexOf(' ') - 1);
					vparcelas = DigesterV2.cortaDecimal(DigesterV2.autoNormalizaMoeda( NParcelas.substring( NParcelas.lastIndexOf(' ') + 1 ) , false, new Locale("pt", "BR")), false, new Locale("pt", "BR"));
				}
	        	c1 = ''; c2 = ''; c3 = ''; c4 = ''; c5 = ''; categoria = ''; 
				 
				 String departamento = it.DEPARTAMENTO.text();
				 if( departamento != null && !departamento.trim().equals('') ) {
					 String [] cs = departamento.split('-');
					 c1 = cs.length > 0 ? cs[0] : null;
					 c2 = cs.length > 1 ? cs[1] : null;
					 c3 = cs.length > 2 ? cs[2] : null;
					 c4 = cs.length > 3 ? cs[3] : null;
					 c5 = cs.length > 4 ? cs[4] : null;
					 categoria = c1; 
					 if( c2 != null ) categoria += '/' + c2;
					 if( c3 != null ) categoria += '/' + c3;
					 if( c4 != null ) categoria += '/' + c4;
					 if( c5 != null ) categoria += '/' + c5;					 
				 }

				if(url.indexOf("?") > -1) url = url.substring(0, url.indexOf("?"));
	
				//workaround
				if(image != null) {
					image = image.replace(".jpeg", ".jpg");
					image = image.replace(".JPEG", ".jpg");
					image = image.replace(".JPG", ".jpg");
					image = image.replace(".png", ".jpg");
					image = image.replace(".PNG", ".jpg");
				}
				
				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: categoria, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: false, platformType: 'NA') );

	            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
			}
		
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/shopfato.xml' );
        	DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
        	DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
        	DigesterV2.completaRecomendacoes(ConnHelper.get().reopen(), false);
        	DigesterV2.bestSellers(ConnHelper.get().reopen());
        	DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		} else
		if(args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen());
			DigesterV2.topSellersPorCategoria(ConnHelper.get().reopen(), false);
		}
	}
	
}

