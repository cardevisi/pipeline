import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.digester.buscape.offer.*;
import net.dynad.digester.buscape.product.*;
import net.dynad.digester.buscape.category.LookupCategories;
import net.dynad.digester.buscape.category.Category;
import net.dynad.digester.buscape.Config;
import net.dynad.digester.buscape.ConfigShoppingUOL;

import java.util.Scanner;
import java.net.URL;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import groovy.json.JsonSlurper;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterShoppingUol {
	static int key;
	static {
	    ConnHelper.schema = "dynad_shopping_uol_buscape";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        	ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

	public static void loadBestSellersGeral (Connection conn ) {
                PreparedStatement pStmt = conn.prepareStatement("update catalogo set topseller_geral = 0");
                pStmt.executeUpdate();
                ConnHelper.closeResources(pStmt, null);

		loadBestSellersFromCategory (conn, 77, true, 5, true);
                loadBestSellersFromCategory (conn, 3673, true, 5, true);
                loadBestSellersFromCategory (conn, 10232, true, 5, true);
                loadBestSellersFromCategory (conn, 3671, true, 5, true);
                loadBestSellersFromCategory (conn, 138, true, 5, true);
                loadBestSellersFromCategory (conn, 6058, true, 5, true);

                try {	
		java.util.Map< Integer, Integer > mapNumbers = new java.util.HashMap();
                LookupProducts queryProducts = new LookupProducts(new ConfigShoppingUOL(), Config.Operation.ProductTops);
                queryProducts.setMaxResults(100);
                for(Product product : queryProducts.read().getListResult() ) {
			if( product.getIdCategory() == 77 || product.getIdCategory() == 3673 || product.getIdCategory() == 10232 || product.getIdCategory() == 3671 || product.getIdCategory() == 138 || product.getIdCategory() == 6058 ) continue;

				if( !mapNumbers.containsKey( product.getIdCategory() ) )
                			mapNumbers.put( product.getIdCategory(), 1 );
                		else
                			mapNumbers.put( product.getIdCategory(), mapNumbers.get( product.getIdCategory() ) + 1 );
                		
                		if( mapNumbers.get( product.getIdCategory() ) > 5 ) {
					println("categ hit max products: " + product.getIdCategory() );
                			continue;
				}

                        FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: String.valueOf(product.getId()), lookupChange: false, platformType: 'NA');
                        FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ( product.getMaxPrice() > 1.0d && product.getMinPrice() > 1.0d &&  product.getQuantity() > 0 && product.getLink() != null ? "1" : "0" ), lookupChange: false, platformType: 'NA');

			

                        java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
                        fields.add( new FieldMetadata(columnName : 'id_sku', columnType: 'string', columnValue: product.getId(), lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'id_categoria', columnType: 'string', columnValue: product.getIdCategory(), lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: product.getName(), lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: product.getImage(), lookupChange: true, platformType: 'IMG') );
                        fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: product.getLink(), lookupChange: true, platformType: 'LINK') );
                        fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: product.getMaxPrice(), lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: true, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: 1, lookupChange: false, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: false, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'type', columnType: 'string', columnValue: 'product', lookupChange: false, platformType: 'NA') );
                        fields.add( new FieldMetadata(columnName : 'topseller_geral', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA') );
                        DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);

			loadTopOffersFromProduct (conn, product, true );
                }

		pStmt = conn.prepareStatement("update catalogo a set a.categoria = (select b.categoria from codigos_categorias b where b.id = a.id_categoria) where a.categoria is null and a.id_categoria is not null");
                pStmt.executeUpdate();
                ConnHelper.closeResources(pStmt, null);

                } catch ( Exception ex ) { println("****** FAILED TO LOAD PRODUCTS *******"); ex.printStackTrace(); }


        }

	public static void loadBestSellersFromCategory (Connection conn, int idCategory, boolean clearFlag ) {
                loadBestSellersFromCategory(conn, idCategory, clearFlag, 50, false);
        }

	public static void loadBestSellersFromCategory (Connection conn, int idCategory, boolean clearFlag, int limit, boolean setAsGeralSeller ) {
		if( clearFlag ) {
		PreparedStatement pStmt = conn.prepareStatement("update catalogo set topseller = 0 where id_categoria = ? and type = 'product'");
		pStmt.setInt(1, idCategory);
                pStmt.executeUpdate();
		ConnHelper.closeResources(pStmt, null);
		}
		try { 
		LookupProducts queryProducts = new LookupProducts(new ConfigShoppingUOL(), Config.Operation.ProductTops);
		queryProducts.setIdCategory(idCategory);
		queryProducts.setMaxResults(limit);
		for(Product product : queryProducts.read().getListResult() ) {
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: String.valueOf(product.getId()), lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ( product.getMaxPrice() > 1.0d && product.getMinPrice() > 1.0d && product.getQuantity() > 0 && product.getLink() != null ? "1" : "0" ), lookupChange: false, platformType: 'NA');
			
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'id_sku', columnType: 'string', columnValue: product.getId(), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'id_categoria', columnType: 'string', columnValue: product.getIdCategory(), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: product.getName(), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: product.getImage(), lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: product.getLink(), lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: product.getMaxPrice(), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: 1, lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'type', columnType: 'string', columnValue: 'product', lookupChange: false, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'topseller', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA') );
			if( setAsGeralSeller )
                                fields.add( new FieldMetadata(columnName : 'topseller_geral', columnType: 'string', columnValue: '1', lookupChange: true, platformType: 'NA') );
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
		
			loadTopOffersFromProduct (conn, product, setAsGeralSeller );
	
		}
		} catch ( Exception ex ) { println("****** FAILED TO LOAD PRODUCT *******"); }
	}

	public static void loadAllCategoriesFromDB ( ) { 
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = conn.prepareStatement("select id, categoria, has_offer from codigos_categorias");
		ResultSet res = pStmt.executeQuery();
		while( res.next() ){ 
			println("[" + res.getInt(1) + "] ..UPDATING..");
			loadBestSellersFromCategory(conn, res.getInt(1), false);
		}
		ConnHelper.closeResources(pStmt, res);
	}

	public static void loadAllCategoriesFromApi ( ) { 
		Map<Integer, Category> mapLoaded = new HashMap<Integer, Category>();
                List<Category> listCategories = new ArrayList<Category>();
                Category c = new Category();
                c.setId(0); c.setName("Inicio");
                listCategories.add(c);

		Connection conn = ConnHelper.get().reopen();
		
		while( listCategories.size() > 0 ) {
			Category lookupCategory = listCategories.remove(0);
			LookupCategories queryCategories = new LookupCategories(new ConfigShoppingUOL(), Config.Operation.CategoryList);
			queryCategories.setIdCategory(lookupCategory.getId());
			for(Category product : queryCategories.read().getListResult() ) {
				if( mapLoaded.containsKey( product.getId() ) ) continue;	
				print(product.getId() + ' => ');	
				int cnt = 0;
				PreparedStatement pStmt = conn.prepareStatement("select count(*) from codigos_categorias where id = ?");
				pStmt.setInt(1, product.getId());
				ResultSet res = pStmt.executeQuery();
				if( res.next() )
					cnt = res.getInt(1);
				ConnHelper.closeResources(pStmt, res);
				if( cnt == 0 ) { 			
					pStmt = conn.prepareStatement("insert into codigos_categorias (id, versao, categoria, parent_id, final, has_offer) values (?, ?, ?, ?, ?, ?)");
					pStmt.setInt(1, product.getId());
					pStmt.setInt(2, 0);
					pStmt.setString(3, product.getName());
					pStmt.setInt(4, product.getParentId());	
					pStmt.setString(5, product.isFinalCateg()? "1" : "0");
					pStmt.setString(6, product.isHasOffer()? "1" : "0");
					pStmt.executeUpdate();
					println('******* NEW *******');
					//loadBestSellersFromCategory(conn, product.getId(), false);
				} else { 
					pStmt = conn.prepareStatement("update codigos_categorias set categoria = ?, final = ?, has_offer = ? where id = ? and parent_id = ?");
                                        pStmt.setString(1, product.getName());
                                        pStmt.setString(2, product.isFinalCateg()? "1" : "0");
                                        pStmt.setString(3, product.isHasOffer()? "1" : "0");
					pStmt.setInt(4, product.getId());
					pStmt.setInt(5, product.getParentId());
                                        pStmt.executeUpdate();
					println('******* UPDATED *******');
					//loadBestSellersFromCategory( conn, product.getId(), true);
				}

				if( !product.isFinalCateg() ) listCategories.add(product);
			}
			mapLoaded.put(lookupCategory.getId(), lookupCategory);
		}
	}

	public static void buildSnaFileTopSellerGeral ( ) { 
		java.util.Map<String, java.util.List<String>> mapTopSellerGeral = new java.util.HashMap<String, java.util.List<String>>();
		
	}

	public static void buildSnaFile( ) { 
		 ///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select id_sku, sku, imagem, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, ifnull(topseller, 0) as topseller, ifnull(topseller_geral, 0) as topseller_geral, id_categoria, categoria, name_seller, id_product as id_product from catalogo where type = 'product' and link is not null and link <> '' and imagem is not null and imagem <> '' and preco_promocional <> '0.0' and preco_promocional <> '0' and preco_promocional <> '0.00' and preco_original <> '0' and preco_original <> '0.0' and preco_original <> '0.00' and preco_original <> '' and preco_promocional <> ''  order by ifnull(topseller_geral, 0) desc ");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        
		int indexTopSellerGeral = 0;
		def indexTopSellerCateg = [ : ]; 
		java.util.Map<String, java.util.List<String>> mapTopSellerGeral = new java.util.HashMap<String, java.util.List<String>>();
		new File("/mnt/shoppinguol_buscape_products.txt").withWriter { out ->
			for(int i=2; i<=rsmd.getColumnCount(); i++) out.print((i>2?"|":"metadata|id|")+rsmd.getColumnName(i));
                out.print("\n");
                while( res.next() ) {
					if( !indexTopSellerCateg.containsKey( res.getString(11) ) )
						indexTopSellerCateg[ res.getString(11) ] = 1;
	
					if( res.getInt(11) == 1 ) {
						//out.print('tsg_' + (indexTopSellerGeral++) );
						indexTopSellerGeral++;
						StringBuilder sbGeral = new StringBuilder();
						sbGeral.append('|' + res.getString(2) );
						sbGeral.append("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
						for(int i=4; i<=rsmd.getColumnCount(); i++) sbGeral.append("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
						sbGeral.append("\r\n");
						if( !mapTopSellerGeral.containsKey( res.getString(12) ) )
							mapTopSellerGeral.put( res.getString(12), new java.util.ArrayList() );
						mapTopSellerGeral.get( res.getString(12) ).add( sbGeral.toString() );
					}
				
					if( res.getInt(10) == 1 ) {
						out.print('tsc_' + res.getString(11) + '_' +  ( indexTopSellerCateg[ res.getString(11) ] ) );
						out.print('|' + res.getString(2) );
                        out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                        for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
						out.print("\r\n");
						indexTopSellerCateg[ res.getString(11) ] = indexTopSellerCateg[ res.getString(11) ] + 1;
					}

                    out.print(res.getString(1) + '|' + res.getString(2) );
                    out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                    for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
                    out.print("\r\n");
                }
                
                int index = 0;
                while ( index < indexTopSellerGeral ) {
	                for( String categ : mapTopSellerGeral.keySet() ) {
	                	if( mapTopSellerGeral.get(categ).size() > 0 ) {
					String productData = mapTopSellerGeral.get(categ).remove(0); 
	                		out.print('tsg_' + (++index) + productData );
					println('categ: ' + categ + ' / ' + productData );
				}
	                }
                }                
                
                out.println("numerotopsellersgeral|" + (indexTopSellerGeral-1));

           }
           ConnHelper.closeResources(pStmt, res);


	
		/** load offers data **/
		indexTopSellerGeral = 0;
		mapTopSellerGeral = new java.util.HashMap<String, java.util.List<String>>();
		pStmt = conn.prepareStatement("select id_sku, sku, imagem, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, " + 
				"ifnull(topseller_oferta, 0) as topseller, ifnull(topseller_geral_oferta, 0) as topseller_geral_oferta, id_categoria, categoria, id_product, ifnull(index_offer,1) as index_offer, name_seller, id_product from catalogo " + 
				"where type = 'offer' and link is not null and link <> '' and imagem is not null and imagem <> '' and preco_promocional <> '0.0' and preco_promocional <> '0' and preco_promocional <> '0.00' and preco_original <> '0' and preco_original <> '0.0' and preco_original <> '0.00' and preco_original <> '' and preco_promocional <> '' and index_offer <> '-1'  " + 
				"order by ifnull(topseller_geral_oferta, 0) desc");
		res = pStmt.executeQuery();
		new File("/mnt/shoppinguol_buscape_offers.txt").withWriterAppend { out ->
			for(int i=2; i<=rsmd.getColumnCount()-2; i++) out.print((i>2?"|":"metadata|id|")+rsmd.getColumnName(i));
			out.println('|seller_name|id_product');
			while( res.next() ) {
				if( res.getInt(11) == 1 ) {
					indexTopSellerGeral++;
					StringBuilder sbGeral = new StringBuilder();
					sbGeral.append('|' + res.getString(2) );
					sbGeral.append("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
					for(int i=4; i<=rsmd.getColumnCount() - 2; i++) sbGeral.append("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
					sbGeral.append("|"+(res.getString(16)?res.getString(16).replace("|", " "):""));
					sbGeral.append("|"+(res.getString(17)?res.getString(17).replace("|", " "):""));
					sbGeral.append("\r\n");
					if( !mapTopSellerGeral.containsKey( res.getString(12) ) )
						mapTopSellerGeral.put( res.getString(12), new java.util.ArrayList() );
					mapTopSellerGeral.get( res.getString(12) ).add( sbGeral.toString() );
				}
			
				out.print('offer_' + res.getString(14) + '_' + res.getString(15) );
				out.print('|' + res.getString(2) );
		        out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
		        for(int i=4; i<=rsmd.getColumnCount() - 2; i++) out.print("|"+(res.getString(i)?res.getString(i).replace("|", " "):""));
				out.print("|"+(res.getString(16)?res.getString(16).replace("|", " "):""));
				out.print("|"+(res.getString(17)?res.getString(17).replace("|", " "):""));
				out.print("\r\n");
			}			
			ConnHelper.closeResources(pStmt, res);
			
			int index = 0;
	        while ( index < indexTopSellerGeral ) {
	            for( String categ : mapTopSellerGeral.keySet() ) {
	            	if( mapTopSellerGeral.get(categ).size() > 0 ) {
	            		String offerData = mapTopSellerGeral.get(categ).remove(0); 
	            		out.print('tsgo_' + (++index) + offerData );
	            	}
	            }
	        }
	        out.println("numerotopsellersgeralofertas|" + (indexTopSellerGeral-1));
		}

		/** load products limits for offer **/
        pStmt = conn.prepareStatement("select id_product, max(ifnull(index_offer,1)) from catalogo where type = 'offer' group by id_product");
		res = pStmt.executeQuery();
		new File("/mnt/shoppinguol_buscape_offers.txt").withWriterAppend { out ->
			while( res.next() ) {
				out.println('limit_product_' + res.getString(1) + "|" + ( "-1".equals(res.getString(2)) ? "1" : res.getString(2) ) );
			}
		}

	}


	public static void downloadIdsFromMetrics ( eventType ) { 
		Connection conn = ConnHelper.get().reopen();
		PreparedStatement pStmt = null;
		ResultSet res = null;

		Scanner s = new Scanner(new URL("http://metrics.dynad.net/resources/counter/text/getRanking/uolshopping/trackingsite/" + eventType +"/100000/false").openStream(), "UTF-8").useDelimiter("\n");
		while( s.hasNext() ) {
			String sku = s.next().trim();
			if( sku.equals("") ) continue;
			print(sku);
			try {
                	pStmt = conn.prepareStatement("select COUNT(*) FROM catalogo where sku = ? and last_update >= ?");
			pStmt.setString(1, sku);
			pStmt.setDate(2, new java.sql.Date( System.currentTimeMillis() - 7200000 ));
                	res = pStmt.executeQuery();
			if( res.next() )
				if ( res.getInt(1) > 0 ) {
					println(" ..CACHED.. ");
					continue;
				}
			} finally { 
				ConnHelper.closeResources(pStmt, res);
			}

			println(" ..DOWNLOADING.. ");
			try {
	                LookupProducts queryProducts = new LookupProducts(new ConfigShoppingUOL(), Config.Operation.ProductList);
                	queryProducts.setIdProduct( Integer.parseInt(sku) );
        	        queryProducts.setMaxResults(1);
	                for(Product product : queryProducts.read().getListResult() ) {
                        	FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: String.valueOf(product.getId()), lookupChange: false, platformType: 'NA');
                	        FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ( product.getMaxPrice() > 1.0d && product.getMinPrice() > 1.0d && product.getImage() != null && product.getLink() != null ? "1" : "0" ), lookupChange: false, platformType: 'NA');

        	                java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
	                        fields.add( new FieldMetadata(columnName : 'id_sku', columnType: 'string', columnValue: product.getId(), lookupChange: true, platformType: 'NA') );
                        	fields.add( new FieldMetadata(columnName : 'id_categoria', columnType: 'string', columnValue: product.getIdCategory(), lookupChange: true, platformType: 'NA') );
                	        fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: product.getName(), lookupChange: true, platformType: 'NA') );
        	                fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: product.getImage(), lookupChange: true, platformType: 'IMG') );
	                        fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: product.getLink(), lookupChange: true, platformType: 'LINK') );
                        	fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: product.getMaxPrice(), lookupChange: true, platformType: 'NA') );
                	        fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: true, platformType: 'NA') );
        	                fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: 1, lookupChange: false, platformType: 'NA') );
	                        fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: product.getMinPrice(), lookupChange: false, platformType: 'NA') );
                        	fields.add( new FieldMetadata(columnName : 'type', columnType: 'string', columnValue: 'product', lookupChange: false, platformType: 'NA') );
                	        DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);

				LookupOffers queryOffers = new LookupOffers(new ConfigShoppingUOL(), Config.Operation.OfferList);
	                    	queryOffers.setIdProduct(product.getId());
	                    	queryOffers.setMaxResults(5);
				int indexOffer = 0;
	                    	for(Offer offer : queryOffers.read().getListResult() ) {
	                    		saveOffer( conn, product, offer, false, false, ++indexOffer);
	                    	}
        	        }
	                } catch ( Exception ex ) { println("****** FAILED TO LOAD PRODUCT *******"); ex.printStackTrace(); }		

		

		}
		s.close();	


		pStmt = conn.prepareStatement("update catalogo a set a.categoria = (select b.categoria from codigos_categorias b where b.id = a.id_categoria) where a.categoria is null and a.id_categoria is not null");
                pStmt.executeUpdate();
                ConnHelper.closeResources(pStmt, null);		

	}

	private static void loadTopOffersFromProduct (Connection conn, Product product, boolean setAsTopOfferGeral ) {
		PreparedStatement pStmt = conn.prepareStatement("update catalogo set topseller_oferta = 0, index_offer = -1 where id_product = ?");
		pStmt.setInt(1, product.getId());
        	pStmt.executeUpdate();
        	ConnHelper.closeResources(pStmt, null);
		
		LookupOffers queryOffers = new LookupOffers(new ConfigShoppingUOL(), Config.Operation.OfferList);
		queryOffers.setIdProduct(product.getId());
		queryOffers.setMaxResults(10);
		int indexOffer = 0;
		for(Offer offer : queryOffers.read().getListResult() ) {
			saveOffer( conn, product, offer, true, setAsTopOfferGeral, ++indexOffer);
		}
	}
	
	private static void saveOffer ( Connection conn, Product product, Offer offer, boolean setAsTopOffer, boolean setAsTopOfferGeral, int indexOffer ) { 
		try {
	    	FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: '10001' + String.valueOf(offer.getId()), lookupChange: false, platformType: 'NA');
	        FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ( offer.getFullPrice() > 1.0d && offer.getFromPrice() > 1.0d && offer.getImage() != null && offer.getLink() != null ? "1" : "0" ), lookupChange: false, platformType: 'NA');
	        java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
	        fields.add( new FieldMetadata(columnName : 'id_sku', columnType: 'string', columnValue: '10001' + String.valueOf(offer.getId()), lookupChange: true, platformType: 'NA') );
	    	fields.add( new FieldMetadata(columnName : 'id_categoria', columnType: 'string', columnValue: offer.getIdCategory(), lookupChange: true, platformType: 'NA') );
		fields.add( new FieldMetadata(columnName : 'id_product', columnType: 'string', columnValue: offer.getIdProduct(), lookupChange: true, platformType: 'NA') );
	    	fields.add( new FieldMetadata(columnName : 'id_offer', columnType: 'string', columnValue: offer.getId(), lookupChange: true, platformType: 'NA') );
	    	fields.add( new FieldMetadata(columnName : 'id_seller', columnType: 'string', columnValue: offer.getSellerId(), lookupChange: true, platformType: 'NA') );
	    	fields.add( new FieldMetadata(columnName : 'name_seller', columnType: 'string', columnValue: offer.getSellerName(), lookupChange: true, platformType: 'NA') );
	        fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: offer.getName(), lookupChange: true, platformType: 'NA') );
	        fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: (product.getImage() != null && !product.getImage().trim().equals("") ? product.getImage() : offer.getImage() ), lookupChange: true, platformType: 'IMG') );
	        fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: offer.getLink(), lookupChange: true, platformType: 'LINK') );
	    	fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: offer.getFullPrice(), lookupChange: true, platformType: 'NA') );
	        fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: offer.getFromPrice(), lookupChange: true, platformType: 'NA') );
	        fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: offer.getNumInstallments(), lookupChange: false, platformType: 'NA') );
	        fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: offer.getValueInstallments(), lookupChange: false, platformType: 'NA') );
	    	fields.add( new FieldMetadata(columnName : 'type', columnType: 'string', columnValue: 'offer', lookupChange: false, platformType: 'NA') );
		fields.add( new FieldMetadata(columnName : 'index_offer', columnType: 'string', columnValue: indexOffer, lookupChange: true, platformType: 'NA') );
	    	if( setAsTopOffer )
	    		fields.add( new FieldMetadata(columnName : 'topseller_oferta', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA') );
	    	if( setAsTopOfferGeral )
	    		fields.add( new FieldMetadata(columnName : 'topseller_geral_oferta', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA') );
	        DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
        	} catch ( Exception ex ) { println("****** FAILED TO LOAD OFFER *******"); ex.printStackTrace(); }		
	}


	public static void main (String [] args ) {
		println ( args );
		if( args != null && args.length > 0 && args[0] == 'TOP_CATEGORIES' )
			loadAllCategoriesFromApi();
		else if( args != null && args.length > 0 && args[0] == 'TOP_PRODUCTS_FROM_CATEGORIES' )
			loadAllCategoriesFromDB()
		else if( args != null && args.length > 0 && args[0] == 'TOP_PRODUCTS' )
			loadBestSellersGeral( ConnHelper.get().reopen() );
		else if( args != null && args.length > 0 && args[0] == 'BUILD_FILE' )
			buildSnaFile();
		else if( args != null && args.length > 0 && args[0] == 'DOWNLOAD_TRACKING' ) {
			downloadIdsFromMetrics("valido");
			downloadIdsFromMetrics("produtoinexistente");
		}
    	}
}


