import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;

import java.util.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import groovy.json.JsonSlurper;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterShoppingUol {
	static {
	    ConnHelper.schema = "dynad_shopping_uol_vitrine";
	}

    public static boolean _DEBUG_ = false;

	private static java.util.List<String> lastReadyField = null; 
    private static String getFieldAt( String line, int index ) {
    	return getFieldAt(line, index, false);
    }
    private static String getFieldAt( String line, int index, boolean reload ) { 
    	java.util.List<String> listFields = new ArrayList<String>();
    	int pos = -1;
    	
    	if( lastReadyField != null && !reload )
    		return lastReadyField.get( index );
    	
    	while( line.length() > 0 ) {
    		String fieldValue = null;
    		pos = -1;
    		boolean startWithQuote = line.charAt(0) == '"';
    		
    		if( startWithQuote )
    			line = line.substring(1);
    		
    		if( startWithQuote ) {
    			boolean foundClosingQuote = false, foundClosingComma = false;
    			fieldValue = "";
    			pos = 0;
    			for(; pos < line.length(); pos++ ) {
    				if( line.charAt(pos) == '"' ) { 
    					foundClosingQuote = !foundClosingQuote;
    				}
    				if( line.charAt(pos) == ',' && foundClosingQuote ) { 
    					foundClosingComma = true;
    					break;
    				}
    				fieldValue += line.charAt(pos);
    			
    			}
    			
    			if( fieldValue.endsWith("\"") )
    				fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
    		} else {
    			fieldValue = line.substring(0, (pos = (line.indexOf(',') == -1 ? line.length() : line.indexOf(',') ) ) );    			
    		}
    		listFields.add( fieldValue );
			if( pos >= line.length() )
				break;
			line = line.substring(pos + 1).trim();
    	}
    	listFields.add( "" );
    	if( reload )
    		lastReadyField = listFields;
    	return listFields.get( index );
    }
	
	private static void updateSlot( HashMap<String, Object> mapItemCategoria, String strSku, String strOfferEffectivePrice, String strOfferEffectiveBid ) {
		double value = Double.parseDouble( strOfferEffectivePrice );
		double bid = Double.parseDouble( strOfferEffectiveBid );
		
		if( value < (Double) mapItemCategoria.get("menorPreco") )
			mapItemCategoria.put("menorPreco", value);
		else if( value > (Double) mapItemCategoria.get("maiorPreco") )
			mapItemCategoria.put("maiorPreco", value);
		
		if( ((List<Double>)mapItemCategoria.get("listaPrecos")).indexOf( value ) == -1 ) 
			((List<Double>)mapItemCategoria.get("listaPrecos")).add( value );		
		
		HashMap<String, Object> mapBid = new HashMap<String, Object>();
		mapBid.put("bid", bid);
		mapBid.put("id", strSku);					
		
		List<Map<String, Object>> listMaioresBids = (List<Map<String, Object>>) mapItemCategoria.get("listasMaioresBids");
		boolean addAtTheEnd = true;
		int currentPos = -1;
		for( Map<String, Object> item : listMaioresBids ) {
			currentPos++;
			if( (Double)item.get("bid") < bid ) { 
				listMaioresBids.add(currentPos, mapBid);
				if( listMaioresBids.size() > 20 )
					listMaioresBids.remove( 20 );
				addAtTheEnd = false;
				break;
			}
		}
		
		if( addAtTheEnd ) {
			if( listMaioresBids.size() < 20 )
				listMaioresBids.add(mapBid);
		}		
	}
	
	private static HashMap<String, Object> initializeSlot( String strSku, String strOfferEffectivePrice, String strOfferEffectiveBid ) { 
		HashMap<String, Object> mapItemCategoria = new HashMap<String, Object>();
		mapItemCategoria.put("menorPreco", Double.parseDouble( strOfferEffectivePrice ));
		mapItemCategoria.put("maiorPreco", Double.parseDouble( strOfferEffectivePrice ));
		
		List<Double> listPrecos = new ArrayList(); listPrecos.add( (Double) mapItemCategoria.get("menorPreco") );
		mapItemCategoria.put("listaPrecos", listPrecos);
							
		List <Map<String, Object>> listMaioresBids = new ArrayList<Map<String, Object>>();
		HashMap<String, Object> mapBid = new HashMap<String, Object>();
		mapBid.put("bid", Double.parseDouble( strOfferEffectiveBid ));
		mapBid.put("id", strSku);		
		listMaioresBids.add(mapBid);
		
		mapItemCategoria.put("listasMaioresBids", listMaioresBids);
		
		return mapItemCategoria;
	}
	
    private static markText ( String file ) {
		
		Connection conn = ConnHelper.get().reopen();

		
		int cline = 0;
		def p = null;
		
		Map<String, Map<String, Object>> mapCategorias = new HashMap<String, Map<String, Object>>();
		
		BufferedReader br = null;
		Writer out = null;
		int lineIndex = 0;	String line = null;
		try {
			File fout = new File("/mnt/sna_shopping_uol.txt");
			if( fout.exists() ) fout.delete();
			
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream( fout ), "UTF-8"));
		while ( (line = br.readLine()) != null ) {
			if( line == null || line.trim().equals("") ) continue;
			if( ++lineIndex <= 1 ) continue;
			
			if( lineIndex % 500 == 0 )
				println('[' + lineIndex + '] -> ');
			
			if ( !getFieldAt(line, 1, true).trim().equals("") && !getFieldAt(line, 14).trim().equals("") && !getFieldAt(line, 15).trim().equals("") ) {
				int columnIndex = 0;
				String strSku = getFieldAt(line, columnIndex++);
				String strOfferName = Digester.stripTags( getFieldAt(line, columnIndex++) );
				String strOfferCategories = getFieldAt(line, columnIndex++);
				String strOfferInstallmentValue = getFieldAt(line, columnIndex++);
				String strOfferNumInstallments = getFieldAt(line, columnIndex++);
				String strOfferEffectivePrice = getFieldAt(line, columnIndex++);
				String strOfferRegularPrice = getFieldAt(line, columnIndex++);
				String strOfferDescription = Digester.stripTags( getFieldAt(line, columnIndex++) );
				String strOfferDiscountPercentage = getFieldAt(line, columnIndex++);
				String strOfferEffectiveBid = getFieldAt(line, columnIndex++);
				String strOfferFlgFreeShipping = getFieldAt(line, columnIndex++);
				String strOfferFlgPagseguro = getFieldAt(line, columnIndex++);
				String strOfferFlgAdult = getFieldAt(line, columnIndex++);
				String strOfferCurrency = getFieldAt(line, columnIndex++);
				String strOfferClickUrl = getFieldAt(line, columnIndex++);
				String strOfferImageUrl = getFieldAt(line, columnIndex++);
				String strOfferRpm = getFieldAt(line, columnIndex++);
				String strSellerId = getFieldAt(line, columnIndex++);
				String strSellerName = getFieldAt(line, columnIndex++);
				String strSellerAuthStatus = getFieldAt(line, columnIndex++);
				String strSellerUrl = getFieldAt(line, columnIndex++);
				String strProductId = getFieldAt(line, columnIndex++);
				String strProductName = getFieldAt(line, columnIndex++);
				String strProductDescription = Digester.stripTags( getFieldAt(line, columnIndex++) );
				String strProductImageUrls = getFieldAt(line, columnIndex++);
				String strProductLowestPrice = getFieldAt(line, columnIndex++);
				String strProductHighestPrice = getFieldAt(line, columnIndex++);
				
				java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
				if( strOfferInstallmentValue == null || strOfferInstallmentValue.equals("") ) strOfferInstallmentValue = "0.00";
				if( strOfferEffectivePrice == null || strOfferEffectivePrice.equals("") ) strOfferEffectivePrice = "0.00";
				if( strOfferRegularPrice == null || strOfferRegularPrice.equals("") ) strOfferRegularPrice = "0.00";
				if( strProductLowestPrice == null || strProductLowestPrice.equals("") ) strProductLowestPrice = "0.00";
				if( strProductHighestPrice == null || strProductHighestPrice.equals("") ) strProductHighestPrice = "0.00";
				
				if( strOfferRegularPrice == '0.00' && strOfferEffectivePrice != '0.00') strOfferRegularPrice = strOfferEffectivePrice;
				if( strOfferInstallmentValue == '0.00' && strOfferEffectivePrice != '0.00') {
					strOfferNumInstallments = '1';
					strOfferInstallmentValue = strOfferEffectivePrice;
					
				}

				if( strOfferNumInstallments == null || strOfferNumInstallments.equals("") ) { 
					strOfferNumInstallments = '1';
					strOfferInstallmentValue = strOfferEffectivePrice;
				}
				
				if( !mapCategorias.containsKey( strOfferCategories ) ) { 					
					mapCategorias.put(strOfferCategories, initializeSlot( strSku, strOfferEffectivePrice, strOfferEffectiveBid ) );
				} else { 
					HashMap<String, Object> mapItemCategoria = mapCategorias.get( strOfferCategories );
					updateSlot(mapItemCategoria, strSku, strOfferEffectivePrice, strOfferEffectiveBid);
				}
				
				if( !mapCategorias.containsKey( "TopSeller" ) ) { 					
					mapCategorias.put("TopSeller", initializeSlot( strSku, strOfferEffectivePrice, strOfferEffectiveBid ) );
				} else { 
					HashMap<String, Object> mapItemCategoria = mapCategorias.get( "TopSeller" );
					updateSlot(mapItemCategoria, strSku, strOfferEffectivePrice, strOfferEffectiveBid);
				}
				
				strOfferInstallmentValue = Digester.autoNormalizaMoeda(strOfferInstallmentValue, false, new Locale("pt", "BR"));				
				strOfferEffectivePrice = Digester.autoNormalizaMoeda(strOfferEffectivePrice, false, new Locale("pt", "BR"));				
				strOfferRegularPrice = Digester.autoNormalizaMoeda(strOfferRegularPrice, false, new Locale("pt", "BR"));				
				strProductLowestPrice = Digester.autoNormalizaMoeda(strProductLowestPrice, false, new Locale("pt", "BR"));				
				strProductHighestPrice = Digester.autoNormalizaMoeda(strProductHighestPrice, false, new Locale("pt", "BR"));				
				
				String ativo = '1';
				
				String c1 = strOfferCategories, c2 = '', c3 = '';
				if( c1.indexOf(',') > -1 ) { 
					String [] arrCategorias = c1.split(',');
					c1 = arrCategorias[0];
					if( arrCategorias.length > 1 ) c2 = arrCategorias[1];
					if( arrCategorias.length > 2 ) c3 = arrCategorias[2];
				}
				
				if( lineIndex == 2 ) { 
					out.write("metadata|id|sku|nome|arvore_categoria|categoria1|categoria2|categoria3|" + 
								"valor_parcelas|numero_parcelas|preco_promocional|preco_original|descricao|" + 
								"desconto|bid|flag_frete_gratis|flag_pag_seguro|flag_adulto|moeda|link|imagem|" + 
								"rpm|id_cliente|nome_cliente|auth_status|id_produto|nome_produto|" + 
								"descricao_produto|menor_preco|maior_preco\r\n");
				}
				
				out.write(strSku +"|"+strSku+"|"+strOfferName+"|"+strOfferCategories+"|"+c1+"|"+c2+"|"+c3+"|" + 
						""+strOfferNumInstallments+"|"+strOfferInstallmentValue+"|"+strOfferEffectivePrice+"|"+strOfferRegularPrice+"|"+strOfferDescription+"|" + 
						strOfferDiscountPercentage+"|"+strOfferEffectiveBid+"|"+strOfferFlgFreeShipping+"|"+strOfferFlgPagseguro+"|"+strOfferFlgAdult+"|"+strOfferCurrency+"|"+strOfferClickUrl+"|"+strOfferImageUrl+"|" + 
						strOfferRpm + "|"+strSellerId+"|"+strSellerName+"|"+strSellerAuthStatus+"|"+strProductId+"|"+strProductName+"|" + 
						strProductDescription + "|"+strProductLowestPrice+"|" + strProductHighestPrice + "\r\n");
				
				/**
	
				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: strSku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: true, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: strOfferCategories, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: strOfferName, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: strOfferImageUrl, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: strOfferClickUrl, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: strOfferRegularPrice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: strOfferEffectivePrice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: strOfferNumInstallments, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: strOfferInstallmentValue, lookupChange: true, platformType: 'NA') );				
				fields.add( new FieldMetadata(columnName : 'descricao', columnType: 'string', columnValue: strOfferDescription, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'desconto', columnType: 'string', columnValue: strOfferDiscountPercentage, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'bid', columnType: 'string', columnValue: strOfferEffectiveBid, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'flag_frete_gratis', columnType: 'string', columnValue: strOfferFlgFreeShipping, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'flag_pagseguro', columnType: 'string', columnValue: strOfferFlgPagseguro, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'flag_adulto', columnType: 'string', columnValue: strOfferFlgAdult, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'currency', columnType: 'string', columnValue: strOfferCurrency, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'rpm', columnType: 'string', columnValue: strOfferRpm, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'id_cliente', columnType: 'string', columnValue: strSellerId, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome_cliente', columnType: 'string', columnValue: strSellerName, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'auth_status', columnType: 'string', columnValue: strSellerAuthStatus, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'id_produto', columnType: 'string', columnValue: strProductId, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome_produto', columnType: 'string', columnValue: strProductName, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'descricao_produto', columnType: 'string', columnValue: strProductDescription, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'menor_preco', columnType: 'string', columnValue: strProductLowestPrice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'maior_preco', columnType: 'string', columnValue: strProductHighestPrice, lookupChange: true, platformType: 'NA') );
				
				
				
				DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
				**/
			}
		}
		} catch (IOException e) {
			e.printStackTrace();
		} finally { 
			try { if( br != null ) br.close(); } catch ( Exception exClose ) {}
			try { if( out != null ) out.close(); } catch ( Exception exClose ) {}
		}
		
		try { 
			File fout = new File("/mnt/sna_shopping_uol_categorias.txt");
			if( fout.exists() ) fout.delete();
			out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream( fout ), "UTF-8"));
			out.write("metadata|categoria|recomendacoes|bids\r\n");
			for(Map.Entry<String, Object> itemCategoria : mapCategorias.entrySet() ) {
				
				List <Map<String, Object>> bids = (List <Map<String, Object>>)itemCategoria.getValue().get("listasMaioresBids");
				if( bids.size() < 20 )
					bids.addAll( increaseBids( itemCategoria, mapCategorias, ( 20 - bids.size()) ) );
				
				out.write(itemCategoria.getKey() + "|");
				int currentBidIndex = 0;
				for( Map<String, Object> bidEntry : bids ) { 
					if( ++currentBidIndex > 1 ) out.write(',');
					out.write( bidEntry.get("id") );
				}
				out.write("|");
				currentBidIndex = 0;
				for( Map<String, Object> bidEntry : bids ) { 
					if( ++currentBidIndex > 1 ) out.write(',');
					out.write( bidEntry.get("bid").toString() );
				}
				out.write("\r\n");
			}
		} catch ( Exception ex ) { 
			ex.printStackTrace();
		} finally { 
			try { if( out != null ) out.close(); } catch ( Exception exClose ) {}
		}
	}
    
    private static increaseBids( Map.Entry<String, Object> currentItemCategoria, Map<String, Map<String, Object>> mapCategorias, int rc ) { 
    	List <Map<String, Object>> bids = new ArrayList();
    	List<String> listHandled = new ArrayList();
    	String currentPath = currentItemCategoria.getKey();
    	
    	while( true ) {     	
    		//println("lookup : " + currentPath );
    		if( currentPath.indexOf(',') > -1 ) 
    			currentPath = currentPath.substring(0, currentPath.lastIndexOf(','));
    		else
    			break;
    		
	    	for(Map.Entry<String, Object> itemCategoria : mapCategorias.entrySet() ) {
	    		if( itemCategoria.getKey().equals( currentItemCategoria.getKey() ) )
	    			continue;	    		
	    		if( listHandled.indexOf( itemCategoria.getKey() ) > -1 )
	    			continue;
	    		
	    		//println("checking drill up to : " + itemCategoria.getKey() + " / " + currentPath );
	    		if( itemCategoria.getKey().startsWith( currentPath ) ) { 
	    			
	    			try { 
		    			List <Map<String, Object>> itemBids = (List <Map<String, Object>>)itemCategoria.getValue().get("listasMaioresBids");
		    			main_loop:
		    			for( Map<String, Object> mapItemBid : itemBids ) { 
		    				if( bids.size() == rc )
		    					return bids;
		    				
		    				boolean addAtTheEnd = true;
		    				int currentPos = -1;
		    				for( Map<String, Object> mapReturnBid : bids ) { 
		    					currentPos++;
			    				if( (Double)mapReturnBid.get('bid') < mapItemBid.get('bid') ) { 
			    					bids.add(currentPos, mapItemBid);
			    					if( bids.size() > rc )
			    						bids.remove( rc );
			    					addAtTheEnd = false;
			    					continue main_loop;
			    				}
		    				}
		    				
		    				if( addAtTheEnd ) { 
		    					if( bids.size() < 20 )
		    						bids.add(mapItemBid);
		    				}

		    				
		    			}
	    			} finally { 
	    				println("drill up to : " + itemCategoria.getKey() + " / " + currentPath );
		    			println( bids );
	    			}
	    			
	    		}
	    	}
	    	
    	}
    	
    	if( bids.size() < rc ) { 
    		try { 
	    		List <Map<String, Object>> listBidsTopSeller = (List <Map<String, Object>>)mapCategorias.get("TopSeller").get("listasMaioresBids");
	    		for( Map<String, Object> bidTopSeller : listBidsTopSeller ) {
	    			bids.add( bidTopSeller );
	    			if( bids.size() == rc )
	    				break;
	    		}
    		} finally { 
    			println("filled with topsellers : " + currentItemCategoria.getKey() );
    			println( bids );
    		}
    	}
    	
    	
    	return bids;
    }
	
	public static void main (String [] args ) {
		
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markText( '/mnt/feed' );
			/**Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
            DigesterV2.validacao(ConnHelper.get().reopen(), 140, 15.0);**/
        } else
	        if(args[0] == 'BEST_SELLERS') {
                Digester.bestSellers(ConnHelper.get().reopen());
	        }
    }
}
