import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;
import groovy.json.JsonSlurper;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterShoppingUol {
	static int key;
	static {
	    ConnHelper.schema = "dynad_shopping_uol";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if( res.next() )
        	key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

	private static String getFieldAt( String line, int index ) { 
    	java.util.List<String> listFields = new ArrayList<String>();
    	int pos = -1;
    	
    	while( line.length() > 0 ) {
    		String fieldValue = null;
    		pos = -1;
    		boolean startWithQuote = line.charAt(0) == '"';
    		
    		if( startWithQuote )
    			line = line.substring(1);
    		
    		if( startWithQuote ) {
    			boolean foundClosingQuote = false, foundClosingComma = false;
    			fieldValue = "";
    			pos = 0;
    			for(; pos < line.length(); pos++ ) {
    				if( line.charAt(pos) == '"' ) { 
    					foundClosingQuote = !foundClosingQuote;
    				}
    				if( line.charAt(pos) == ',' && foundClosingQuote ) { 
    					foundClosingComma = true;
    					break;
    				}
    				fieldValue += line.charAt(pos);
    			}
    			
    			if( fieldValue.endsWith("\"") )
    				fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
    		} else {
    			fieldValue = line.substring(0, (pos = (line.indexOf(',') == -1 ? line.length() : line.indexOf(',') ) ) );    			
    		}
    		listFields.add( fieldValue );
			if( pos >= line.length() )
				break;
			line = line.substring(pos + 1).trim();
    	}
    	listFields.add( "" );
    	
    	return listFields.get( index );
    }

    
    private static markText ( String file ) {
		
		Connection conn = ConnHelper.get().reopen();

		
		int cline = 0;
		def p = null;
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, descricao, codigo, promover, desconto;
		
		int lineIndex = 0;	
		new File( file ).eachLine { String line ->
			if( line == null || line.trim().equals("") ) return;
			if( ++lineIndex <= 1 ) return;
			if ( !getFieldAt(line, 2).trim().equals("") ) {
				sku = getFieldAt(line, 0);
				nome = Digester.stripTags( getFieldAt(line, 1) )
				java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
				marca = ''
				
				url = getFieldAt(line, 3);
				if(url.indexOf("?") > -1)
					url = url.substring(0, url.indexOf("?"));
				
				
				oprice = Digester.autoNormalizaMoeda(getFieldAt(line, 7), false, new Locale("pt", "BR"));				
				fprice = Digester.autoNormalizaMoeda(getFieldAt(line, 6), false, new Locale("pt", "BR"));
				
				nparcelas = getFieldAt(line, 10) == null || getFieldAt(line, 10).trim().equals("") ? "1" : getFieldAt(line, 10);
				if( getFieldAt(line, 10) == null || getFieldAt(line, 10).trim().equals("") )
					vparcelas = fprice;
				else { 
					if( getFieldAt(line, 9) == null || getFieldAt(line, 9).trim().equals("") ) { 
						nparcelas = "1";
						vparcelas = fprice;
					} else { 
						vparcelas = Digester.autoNormalizaMoeda( getFieldAt(line, 9), false, new Locale("pt", "BR") );
					}
				}

				image = getFieldAt(line, 2);
				c1 = getFieldAt(line, 4);
				c2 = '';
				c3 = getFieldAt(line, 5);

				if( c1.indexOf(',') > -1 ) { 
					String [] categorias = c1.split(',');
					c1 = categorias[0];
					c2 = categorias[1];
				}

				String ativo = '1';
				String arvoreCategoria = c1 + '/' + c2 + '/' + c3;
				if( arvoreCategoria.toLowerCase().indexOf('livro') > -1 )
					ativo = '0';

	
				fprice = fprice==null||fprice==""?"0,00":fprice;
				oprice = oprice==null||oprice==""?"0,00":oprice;
				vparcelas = vparcelas==null||vparcelas==""?"0,00":vparcelas;
					
				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: true, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: arvoreCategoria, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );
				
				DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
				
			}
		}
	}
	
	public static void main (String [] args ) {
		
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markText( '/mnt/XML/shoppinguol_ws.txt' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
            DigesterV2.validacao(ConnHelper.get().reopen(), 140, 15.0);
        } else
	        if(args[0] == 'BEST_SELLERS') {
                Digester.bestSellers(ConnHelper.get().reopen());
	        }
    }
}


