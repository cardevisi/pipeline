import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.Normalizer;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Spider {
	
        static int key;
        static {
                ConnHelper.schema = "dynad_ultrafarma";
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);
        }


	public static boolean _DEBUG_ = false;
	
	private static markXml ( String file ) { 
		
		int cline = 0;
		def p = null;
		String line = null;
		String preline = null;
		String _preline = null;
		
		Connection conn = ConnHelper.get().reopen();
		
		String sku, c1, c2, c3;
		String image, oprice, fprice, nparcelas, vparcelas;
		String url, marca, nome, codigo, promover, desconto;
		//BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(file)));
		def summary = new XmlParser().parse(file);
		def list = summary.oferta

		list.each {
			
			sku = it.codigo_procfit.text()
			def categ = it.categorias.text()

				def tmp = categ

				if(tmp != null && tmp != '') {
					def idx = 0;
					c1 = tmp;
					if(c1 != null) c1 = c1.replace("@", "");
					tmp.split('/').each {
						it = it.replace("@", "");
						if(++idx == 1) c1 = it;	
						else if(idx == 2) c2 = it;	
						else if(idx == 3) c3 = it;	
					}
				}

			image = it.oferta_imgproduto.text()		
			oprice = it.preco_de.text()		
			fprice = it.preco_por.text()		
			nome = it.descricao.text()		
			marca = it.fabricante.text()		
			url = it.oferta_link.text()		
			promover = it.veicular.text()		
			desconto = it.desconto.text()		

				promover = promover=='S'?'sim':'nao';
				def tmp2 = Normalizer.normalize(nome.toUpperCase().replace(" ", "_").substring(0, nome.length()>50?50:nome.length()), Normalizer.Form.NFKD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
				def prod = "" ;
				for(int i=0; i<(tmp2.length()<50?tmp2.length():50); i++)
					prod += ((tmp2[i] >= 'A' && tmp2[i] <= 'Z') || tmp2[i] == '_' || (tmp2[i] >= '0' && tmp2[i] <= '9')) ? tmp2[i] : '';

				url = url + "?utm_source={PORTAL}&utm_medium={FORMATO}&utm_term=Produto_" + prod + "&utm_content=Produtos&utm_campaign={PORTAL}_{FORMATO}_Produto_" + prod;
				Digester.salvaCatalogo(conn, sku, c1, c2, c3, image, oprice, fprice, nparcelas, vparcelas, url, marca, nome, codigo, promover, true);

				c1 = c2 = c3 = null;
				image = null; oprice = null; fprice = null; nome = null; marca = null; url = null; promover = null; desconto = null; 
			
			if( ++cline % 100 == 0 )
				conn = ConnHelper.get().reopen();

		}
		
	}
	
	public static void main (String [] args ) {
                if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/XML/ultrafarma.xml' );
			Digester.normalizaSupernova(ConnHelper.get().reopen());
                	Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
			//////atualizarRecomendacoes();
			Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
			Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                        Digester.validacao(ConnHelper.get().reopen());

                } else
                if(args[0] == 'BEST_SELLERS') {
                        Digester.bestSellers(ConnHelper.get().reopen());
			Digester.topSellersPorCategoria(ConnHelper.get().reopen(), false);
                }


 
	}
	
}
