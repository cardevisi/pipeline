import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterVivara {
        static int key;
        static {
            ConnHelper.schema = "dynad_vivara";
            Connection conn = ConnHelper.get().reopen();
            PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
            ResultSet res = pStmt.executeQuery();
            if( res.next() )
                    key = res.getInt(1);
            ConnHelper.closeResources(pStmt, res);
        }

        public static boolean _DEBUG_ = false;


		public static String unaccent(String s) {
		    String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
		    return normalized.replaceAll("[^\\p{ASCII}]", "");
		}

        private static markXml ( String file ) {
			
	        Connection conn = ConnHelper.get().reopen();
	
			int cline = 0;
			def p = null;
			String line = null;
			
			String sku, c1, c2, c3, c4, c5;
			String image, oprice, fprice, nparcelas, vparcelas;
			String url, marca, nome, codigo, promover, desconto, descricao;
			String dataInicio, dataFim, numDiarias, numPessoas, estado, regiao, pais, cidade, cta;
			Float latitude, longitude
	
	        def summary = new XmlParser().parse(file);
	        def list = summary.products.product
	
		
	        list.each {
	        	sku = it.productId.text();
			if( sku == null || sku.trim() == '' ) return;

	        	nome = it.productName.text().trim();
	        	url = it.url.text();
			if( url != null && !url.startsWith("http://") && !url.startsWith("https://") )
                                url = "http://" + url;

	        	image = it.photo.text();
			if( image != null && !image.startsWith("http://") && !image.startsWith("https://") )
				image = "http://" + image;

	            	oprice = 'R\$ ' + DigesterV2.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR"))
	            	fprice = 'R\$ ' + DigesterV2.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR"))
			try { 
			String [] txtPrice = it.amount.text().trim().split(' ');
	        	nparcelas = txtPrice[0].replaceAll("[^0-9]", "");
	        	vparcelas = 'R\$ ' + DigesterV2.autoNormalizaMoeda( txtPrice[1], false, new Locale("pt", "BR") );
			} catch ( java.lang.ArrayIndexOutOfBoundsException ex ) { 
				nparcelas = '1';
				vparcelas = fprice;
			}
			c1 = it.brand.text();
			int indexCateg = 0 ;
			it.categories.each { itCateg -> indexCateg++; if( indexCateg == 1 ) { c2 = itCateg.text(); } else if( indexCateg == 2 ) { c3 = itCateg.text(); } }

				FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
				FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: (image != null && image != '' ? '1' : '0'), lookupChange: false, platformType: 'NA');
				java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
				fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2 + '/' + c3), lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG') );
				fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK') );
				fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: false, platformType: 'NA') );
				fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: false, platformType: 'NA') );

				DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
				if( ++cline % 100 == 0 )
					conn = ConnHelper.get().reopen();
			}
		
	}
	
	public static void main (String [] args ) {

		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
		markXml( '/mnt/XML/vivara.xml' );
        	DigesterV2.normalizaSupernova(ConnHelper.get().reopen());
        	DigesterV2.inativaForaDoCatalogo(ConnHelper.get().reopen());
        	Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);
        	DigesterV2.bestSellers(ConnHelper.get().reopen());
		} else
		if(args[0] == 'BEST_SELLERS') {
			DigesterV2.bestSellers(ConnHelper.get().reopen());
		}

		///////// EXPORT SUPERNOVA 2 ///////////
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
                ResultSet res = pStmt.executeQuery();
                java.sql.ResultSetMetaData rsmd = res.getMetaData();
                new File("/mnt/vivara.txt").withWriter { out ->
                        for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
                        out.print("\n");
                        while( res.next() ) {
                                out.print(res.getString(1)+"|"+res.getString(2));
                                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
				for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i).replace("|", " "));
				out.print("\n");
                                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
			}
                }
                ConnHelper.closeResources(pStmt, res);
	}
	
}


