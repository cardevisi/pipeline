import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

import java.text.Normalizer;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class DigesterWine {
	static int key;
	static {
		ConnHelper.schema = "dynad_wine";
	    Connection conn = ConnHelper.get().reopen();
	    PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if(res.next()) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
	}

    public static boolean _DEBUG_ = false;

    private static markXml ( String file ) {
        Connection conn = ConnHelper.get().reopen();
        java.util.Map<String, String> mapSkusProcessados = new java.util.HashMap();
		int cline = 0;
	
		String sku, nome, link, imagem, marca, c1, c2, c3, precoOriginal, precoPromocional, nparcelas, vparcelas, ativo;
        def xml = new XmlParser().parse(file);
        def produtos = xml.channel.item;        
                
		produtos.each {						
			ativo = '1';//ATIVO
			sku = it.'g:id'.text();//SKU
			nome = it.title.text();//NOME
    		link = it.link.text();//LINK
			if(link.indexOf("?") > -1) link = link.substring(0, link.indexOf("?"));
			imagem = it.'g:image_link'.text();//IMAGEM
			//marca = it.'g:brand'.text();//MARCA

			def categs = it.'g:google_product_category'.text().split('>');
			c1 = categs[categs.length-1]; 	//CATEGORIA 1
			c2 = (categs.length >= 2) ? categs[categs.length-2] : categs[0];	//CATEGORIA 2
			c3 = (categs.length >= 3) ? categs[categs.length-3] : categs[0];	//CATEGORIA 3

			precoOriginal = it.'g:price'.text();//PRECO ORIGINAL
			precoOriginal = Digester.autoNormalizaMoeda(precoOriginal, false, new Locale("pt", "BR"));			
			
			precoPromocional = precoOriginal;
			vparcelas = precoOriginal;
			
			precoOriginal = precoOriginal==null||precoOriginal==""?"0,00":precoOriginal;
			precoPromocional = precoPromocional==null||precoPromocional==""?"0,00":precoPromocional;
			nparcelas = (nparcelas==null||nparcelas=="")?"1":nparcelas;					
			vparcelas = (vparcelas==null||vparcelas=="")?"0,00":vparcelas;					
			
			FieldMetadata fSku = new FieldMetadata(columnName : 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
			FieldMetadata fAtivo = new FieldMetadata(columnName : 'ativo', columnType: 'string', columnValue: ativo, lookupChange: false, platformType: 'NA');
			java.util.List<FieldMetadata> fields = new java.util.ArrayList<FieldMetadata>();
			fields.add( new FieldMetadata(columnName : 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'link', columnType: 'string', columnValue: link, lookupChange: true, platformType: 'LINK') );
			fields.add( new FieldMetadata(columnName : 'imagem', columnType: 'string', columnValue: imagem, lookupChange: true, platformType: 'IMG') );
			fields.add( new FieldMetadata(columnName : 'marca', columnType: 'string', columnValue: marca, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria', columnType: 'string', columnValue: c1, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria1', columnType: 'string', columnValue: c1, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria2', columnType: 'string', columnValue: c2, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'categoria3', columnType: 'string', columnValue: c3, lookupChange: true, platformType: 'NA') );		
			fields.add( new FieldMetadata(columnName : 'preco_original', columnType: 'string', columnValue: precoOriginal, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'preco_promocional', columnType: 'string', columnValue: precoPromocional, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA') );
			fields.add( new FieldMetadata(columnName : 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA') );
			DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
			if( ++cline % 100 == 0 ) conn = ConnHelper.get().reopen();
		}
	}
	
	public static void main (String [] args ) {
		if(args.length == 0 || args[0] != 'BEST_SELLERS') {
			markXml( '/mnt/TMP/xml/wine.xml' );			
            Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
            Digester.completaRecomendacoes(ConnHelper.get().reopen(), false);            
            Digester.bestSellers(ConnHelper.get().reopen());			
        } else if(args[0] == 'BEST_SELLERS') {
        	//Digester.bestSellers(ConnHelper.get().reopen());
        }

   		///////// EXPORT SUPERNOVA 2 ///////////
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select sku, id, imagem, ativo, nome, link, preco_original, preco_promocional, numero_parcelas, valor_parcelas, recomendacoes from catalogo where sku is not null and id is not null and imagem is not null and ativo is not null and nome is not null and link is not null and preco_original is not null and preco_promocional is not null and numero_parcelas is not null and valor_parcelas is not null");
        ResultSet res = pStmt.executeQuery();
        java.sql.ResultSetMetaData rsmd = res.getMetaData();
        new File("/mnt/TMP/sna-files/wine.txt").withWriter { out ->
        	//ESCREVE O HEADER
            for(int i=1; i<=rsmd.getColumnCount(); i++) out.print((i>1?"|":"metadata|")+rsmd.getColumnName(i));
            out.print("\n");
            //ESCREVE OS DADOS
            while( res.next() ) {
                out.print(res.getString(1)+"|"+res.getString(2));
                out.print("|http://static.dynad.net/let/"+URLCodec.encrypt(res.getString(3)));
                for(int i=4; i<=rsmd.getColumnCount(); i++) out.print("|"+res.getString(i));
                out.print("\n");
                out.print("_"+res.getString(2)+"|"+res.getString(1)+"\n");
            }
        }
        ConnHelper.closeResources(pStmt, res);
        println 'SN2 - DATA GENERATED';
    }
}