import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([@Grab(group = 'org.apache.poi', module = 'poi', version = '3.7'), @Grab(group = 'commons-codec', module = 'commons-codec', version = '1.6'), @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.6.1'), @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.5'), @GrabConfig(systemClassLoader = true)])

class Spider {
    static int key;
    static {
        ConnHelper.schema = "dynad_zoom";
        Connection conn = ConnHelper.get().reopen();
        PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
        ResultSet res = pStmt.executeQuery();
        if (res.next()) key = res.getInt(1);
        ConnHelper.closeResources(pStmt, res);
    }

    private static markXml(String file) {
        //CONEXAO
        Connection conn = ConnHelper.get().reopen();
        //CONTADORES
        int cline = 0;
        //VARIAVEIS DOS CAMPOS
        String sku, nome, url, oprice, fprice, image, nparcelas, vparcelas, c1, c2, c3;        
        //VARIAVEIS DA LEITURA DO XML
        def xml = new XmlParser().parse(file);
        def itens = xml.item;

        /* EXEMPLO XML
        <item>
            <offer-id>7396787</offer-id>
            <name><![CDATA[Short Saia Mandi Prega]]></name>
            <category-id>463</category-id>
            <category-name><![CDATA[Bermuda e Short Infantil]]></category-name>
            <top-parent-category-id>1173</top-parent-category-id>
            <top-parent-category-name><![CDATA[Moda e AcessÃ³rios]]></top-parent-category-name>
            <price>105,90</price>
            <parcel-numbers>5</parcel-numbers>
            <parcel-values>21,18</parcel-values>
            <store-id>7</store-id>
            <store-name><![CDATA[Submarino]]></store-name>
            <url><![CDATA[http://www.zoom.com.br/lead?oid=7396787&sortorder=-1&index=&searchterm=&pagesize=&channel=12&og=19159&utm_source=uol&utm_medium=midia&utm_term=Bermuda+e+Short+Infantil&utm_campaign=uol_cliques&utm_content=Short+Saia+Mandi+Pre]]></url>
            <image-url><![CDATA[http://i1.zst.com.br/thumbs/49/3/19/41207371.jpg]]></image-url>
        </item>
        */

        itens.each {

            sku = it['offer-id'].text()                 //SKU
            nome = it.name.text()                       //NOME
            url = it.url.text()                         //LINK            
            fprice = Digester.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR"));//PRECO_PROMOCIONAL
            oprice = Digester.autoNormalizaMoeda(it.price.text(), false, new Locale("pt", "BR")); //PRECO_ORIGINAL
            nparcelas = it['parcel-numbers'].text();    //NUMERO_PARCELAS 
            vparcelas = it['parcel-values'].text();     //VALOR_PARCELAS
            image = it['image-url'].text();             //IMAGEM
            c1 = it['category-name'].text();            //CATEGORIA 1(PRINCIPAL)
            c2 = it['top-parent-category-name'].text(); //CATEGORIA 2(MENOS ESPECIFICA)
            c3 = '';


            FieldMetadata fSku = new FieldMetadata(columnName: 'sku', columnType: 'string', columnValue: sku, lookupChange: false, platformType: 'NA');
            FieldMetadata fAtivo = new FieldMetadata(columnName: 'ativo', columnType: 'string', columnValue: '1', lookupChange: false, platformType: 'NA');
            java.util.List <FieldMetadata> fields = new java.util.ArrayList < FieldMetadata > ();
            fields.add(new FieldMetadata(columnName: 'categoria', columnType: 'string', columnValue: (c1 + '/' + c2 + '/' + c3), lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria1', columnType: 'string', columnValue: c1, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria2', columnType: 'string', columnValue: c2, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'categoria3', columnType: 'string', columnValue: c3, lookupChange: false, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'nome', columnType: 'string', columnValue: nome, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'imagem', columnType: 'string', columnValue: image, lookupChange: true, platformType: 'IMG'));
            fields.add(new FieldMetadata(columnName: 'link', columnType: 'string', columnValue: url, lookupChange: true, platformType: 'LINK'));
            fields.add(new FieldMetadata(columnName: 'preco_original', columnType: 'string', columnValue: oprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'preco_promocional', columnType: 'string', columnValue: fprice, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'numero_parcelas', columnType: 'string', columnValue: nparcelas, lookupChange: true, platformType: 'NA'));
            fields.add(new FieldMetadata(columnName: 'valor_parcelas', columnType: 'string', columnValue: vparcelas, lookupChange: true, platformType: 'NA'));

            DigesterV2.salvaCatalogoMetadata(conn, fSku, fAtivo, fields);
            if (++cline % 100 == 0) conn = ConnHelper.get().reopen()
        }
    }

    public static void main(String[] args) {
        if (args.length == 0 || args[0] != 'BEST_SELLERS') {
            markXml('/mnt/XML/zoom.xml');
            //Digester.normalizaSupernova(ConnHelper.get().reopen());
            //Digester.inativaForaDoCatalogo(ConnHelper.get().reopen());
            //Digester.validacao(ConnHelper.get().reopen());
        } else if (args[0] == 'BEST_SELLERS') {
            Digester.bestSellers(ConnHelper.get().reopen());
        }
    }

}