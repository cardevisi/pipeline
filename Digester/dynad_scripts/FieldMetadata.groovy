import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import net.dynad.imageproxy.*;

public class FieldMetadata { 
	String columnName;
	String columnType;
	Object columnValue;
	boolean lookupChange;
	String platformType;

	public boolean isNull( ) { return ( columnValue == null || columnValue == '' ); }
	public void setStmtParameter ( int index, PreparedStatement pStmt ) { 
		if( isNull() ) { 
			if( columnType == 'string' )
				pStmt.setNull( index, java.sql.Types.VARCHAR);
			else if( columnType == 'long' || columnType == 'int' )
				pStmt.setNull( index, java.sql.Types.INTEGER);
			else if( columnType == 'float' || columnType == 'double' )
				pStmt.setNull( index, java.sql.Types.DOUBLE);
			else if( columnType == 'boolean' )
				pStmt.setNull( index, java.sql.Types.BIT);
			else if( columnType == 'date' )
				pStmt.setNull( index, java.sql.Types.DATETIME);
			else
				pStmt.setNull( index, java.sql.Types.VARCHAR);
		} else {
			if( platformType == 'IMG_ENC' && columnValue.toString().indexOf('http://static.dynad.net') == -1 )
	            columnValue = "http://static.dynad.net/let/" + URLCodec.encrypt(columnValue.toString()) + "?hash=ElXEFX1LcBgiF7zWSd1M-A"; //f=180x180&cr=3

			if( columnType == 'string' )
				pStmt.setString( index, (String) columnValue );
			else if( columnType == 'int' )
				pStmt.setInt( index, (Integer) columnValue );
			else if( columnType == 'long' )
				pStmt.setLong( index, (Long) columnValue );
			else if( columnType == 'float' )
				pStmt.setFloat( index, (Float) columnValue );
			else if( columnType == 'double' )
				pStmt.setDouble( index, (Double) columnValue );
			else if( columnType == 'boolean' )
				pStmt.setBoolean( index, (Boolean) columnValue );
			else if( columnType == 'date' )
				pStmt.setDate( index, (java.sql.Date) columnValue );
			else
				pStmt.setString( index, (String) columnValue );
		}
	}
}
