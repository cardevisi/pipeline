import java.lang.annotation.Documented;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab('org.jsoup:jsoup:1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Job {
	
        static int key;

	public static void main (String [] args ) {
		if(args.length < 2) {
			println("use: groovy LimpaInativos <schema> <num. de recomendacoes> [<so_popula>]");
			System.exit(0);
		}
                ConnHelper.schema = args[0];
                Connection conn = ConnHelper.get().reopen();
                PreparedStatement pStmt = conn.prepareStatement("select max(id) from catalogo");
                ResultSet res = pStmt.executeQuery();
                if( res.next() )
                        key = res.getInt(1);
                ConnHelper.closeResources(pStmt, res);

		if(args.length < 3)
			Digester.inativaForaDoCatalogo(ConnHelper.get().reopen(), true);
		Digester.completaRecomendacoes(ConnHelper.get().reopen(), true, Integer.parseInt(args[1]));
	}

}

