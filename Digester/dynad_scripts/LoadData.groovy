import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


class LoadData {
	Connection conn;
	int cLine = 0;
	java.util.Map<String, Integer> skus = new java.util.HashMap<String, Integer>(); 
	java.util.List<String> header = new java.util.ArrayList<String>();
	{ 
		header.add ( "SKU_1" );
		header.add ( "NAME_1"); 
		header.add ( "IMAGEM_1"); 
		header.add ( "IMAGEMP_1"); 
		header.add ( "URL_1"); 
		header.add ( "O_PRICE_1");
		header.add ( "F_PRICE_1");
		header.add ( "N_PARCELAS_1");
		header.add ( "V_PARCELAS_1");
		header.add ( "MARCA_1");
		
		header.add ( "SKU_2");
		header.add ( "NAME_2");
		header.add ( "IMAGEM_2");
		header.add ( "URL_2");
		header.add ( "O_PRICE_2");
		header.add ( "RATE_2");
		header.add ( "N_PARCELAS_2");
		header.add ( "V_PARCELAS_2");
		header.add ( "F_PRICE_2");
		header.add ( "MARCA_2");

		header.add ( "SKU_3");
		header.add ( "NAME_3");
		header.add ( "IMAGEM_3");
		header.add ( "URL_3");
		header.add ( "O_PRICE_3");
		header.add ( "RATE_3");
		header.add ( "N_PARCELAS_3");
		header.add ( "V_PARCELAS_3");
		header.add ( "F_PRICE_3");
		header.add ( "MARCA_3");
		
		header.add ( "SKU_4");
		header.add ( "NAME_4");
		header.add ( "IMAGEM_4");
		header.add ( "URL_4");
		header.add ( "O_PRICE_4");
		header.add ( "RATE_4");
		header.add ( "N_PARCELAS_4");
		header.add ( "V_PARCELAS_4");
		header.add ( "F_PRICE_4");
		header.add ( "MARCA_4");
	}	
	
	void openConnection () {
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://localhost/dynad_dafiti?user=root&password=1111");
	}
	
	void closeConnection () {
		try { conn.close(); } catch ( Exception ex) {}
	}
	
	
	void load () {
		
		String [] sHeader = new String[ header.size() ]; int index = 0;
		for(String h : header) sHeader[index++] = h;
		insertPersonalization( sHeader, 'header' );
		
		Reader  r = null;
		try { 
			String line = null;
			r = new InputStreamReader(new FileInputStream("/tmp/skus.out"), "UTF-8");
			while( (line = r.readLine() ) != null ) {
				String temp = line.trim().replaceAll('"', '').replaceAll('"', '');
				if( !skus.containsKey(temp) )
					skus.put(temp, 1);
			}

			r = new InputStreamReader(new FileInputStream("/tmp/saida_complementar.out"), "UTF-8");
			while( (line = r.readLine() ) != null ) { 
				String [] data = getFields( line );
				insertPersonalization( data, 'line' );
			}
		} catch ( Exception ex ) {
			throw ex;
		} finally { 
			try { r.closeWithWarning(); } catch ( Exception ex) {}
		}
	}
	
	void insertPersonalization ( String [] data, String type ) {
		if( data.length <= 10 ) return;
		if( !skus.containsKey( data[0] ) ) {
		 
			int indexOrder = 1;
			String stmt = "insert into personalization (version, campaign_id, return_by_default, type, novo) values (0, 1, false, '"+type+"', 1);";
			PreparedStatement pStmt = conn.prepareStatement(stmt);
			pStmt.executeUpdate();
			
			Long id = null;
			PreparedStatement rStmt = conn.prepareStatement('select max(id) as ID from personalization where campaign_id = 1');
			ResultSet res = rStmt.executeQuery();
			if( res.next() )
				id = res.getLong('ID');
			
			
			stmt = "insert into value (version, index_order, label, personalization_id, type, value, novo) values (0, ?, ?, "+ id +", 'Literal', ?, 1);";
			
			String sku = null;
			pStmt = conn.prepareStatement(stmt);
			for(int x = 0; x < data.length; x++ ) {
				if( header.get(x).startsWith('RATE_') || header.get(x).startsWith('IMAGEMP_') )
					continue;
				if( header.get(x).equals('SKU_1') )
					sku = data[x];
				
				if( header.get(x).indexOf('PRICE') > -1 || header.get(x).indexOf('V_PARCELAS') > -1 ) {
					data[x] = data[x].replaceAll(',', '.');
				}
	
				pStmt.setInt(1, indexOrder++);
				pStmt.setString(2, header.get(x) );
				pStmt.setString(3, data[x] );
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			
			if( type != 'header' ) { 
				stmt = "insert into match_value (version, campaign_id, filter_expression_id, match_var_id, personalization_ids, value) " + 
				" values (0, 1, 1, 2, " + id + ", '"+sku+"');";
				PreparedStatement pStmt2 = conn.prepareStatement(stmt);
				pStmt2.executeUpdate();
			}
			
			if( cLine++ % 50 == 0 ) {
				closeConnection();
				
				openConnection();
			}
			println('added: ' + cLine + ' record(s).');
		}
	}
	
	
	String [] getFields ( String line ) { 
		
		String [] temp = line.trim().split(";");
		for(int x = 0; x < temp.length; x++ ) { 
			temp[x] = temp[x].replaceAll('"', '').replaceAll('"', '');
		}
		
		return temp;
	}
	
	
	public static void main ( String [] args )	 { 
		
		LoadData i = new LoadData() ;
		try { 
			i.openConnection();
			
			i.load();
				
		} catch ( Exception ex) {
			ex.printStackTrace();
		} finally {
			i.closeConnection();
		} 
		
	}
}
