@Grab('org.jsoup:jsoup:1.6.1')
 
@Grab('org.jsoup:jsoup:1.6.1')
 
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

class SpiderUzGames {
	
	
	
	public static String toUTF8(s) {return new String(((String)s).getBytes("UTF-8")); }

	public static java.util.List<String> recuperarCategorias () {
		Document doc
		try {
				doc = Jsoup.connect("http://www.uzgames.com/").timeout(120000).userAgent("Mozilla").get();
		} catch(ex) {
				ex.printStackTrace();
				System.exit(0);
		}
		
		java.util.List<String> categorias = new java.util.ArrayList<String>();
		doc.select(".navTopDepartament .clearfix a").each { a->
			if( a.attr("href") != "#" && a.attr("href").startsWith("/busca") && !a.attr("href").startsWith("http://")  )
				categorias.add('http://www.uzgames.com' +  a.attr("href") );
		}
		categorias.each { 
			println( it );
		}
		
		return categorias;
	}
	
	public static String recuperaIdSku ( String url ) { 
		Document doc
		try {
			doc = Jsoup.connect(url).timeout(120000).userAgent("Mozilla").get();
		} catch(ex) {
				ex.printStackTrace();
				println("[" + url + "]");
				System.exit(0);
		}
		
		String id = "", categoria = "";
		doc.select("#image-main").each {a->
			id = ( a.attr("src").substring( a.attr("src").lastIndexOf("/") + 1  ) ).trim();
		}
		
		doc.select(".value-field + .Categoria").each {a->
			categoria = a.text();
		}
		println( id + " -> " + categoria);
		
		return id;
	}
	
	public static void recuperaProdutosDepto ( String url ) { 
		ConnHelper.schema = "dynad_uzgames";
		Connection conn = ConnHelper.get().reopen();
		String linkPagina = url;
		String categoria = "";
		String console = "";
		
		def pos = url.toLowerCase().indexOf("&ft=");
		if(pos != -1) {
			console = url.substring(pos+4);
			if(console.indexOf("&")) {
				console = console.substring(0, console.indexOf("&"));
			}

			categoria = url.substring(0, pos);
			def tmp = new StringBuffer(categoria).reverse().toString();
			
			if(tmp.indexOf(":")) {
				categoria = new StringBuffer(tmp.substring(0, tmp.indexOf(":"))).reverse().toString();
			}
		}
		
		try { 
			for ( i in 1..100 ) {
			
				def achou = false;
				
				Document doc
				try {
					println( 'processando url: ' + ( linkPagina + ( i > 1 ? i : "" ) ) );
					doc = Jsoup.connect( linkPagina + ( i > 1 ? i : "" ) ).timeout(120000).userAgent("Mozilla").get();
				} catch(ex) {
						ex.printStackTrace();
						System.exit(0);
				}
				
				String conteudo = doc.html();
				
				if( i == 1 && conteudo.indexOf("/buscapagina") > -1 ) { 
					linkPagina = conteudo.substring( conteudo.indexOf("/buscapagina") );
					linkPagina = linkPagina.substring(0, linkPagina.indexOf("\"" ) );
					linkPagina = 'http://www.uzgames.com' + linkPagina;
					
					//doc.select(".bread-crumb .last a").each {li->
					//	categoria = li.text().trim();
					//}
				}
				
				//log(doc);
				
				int index = 0;
				
				doc.select(".prateleira ul li").each {li->
					
					
				
					def sku;
					def name;
					def link;
					def image;
					def frete = "false";
					def price;
					def specialprice;
					def numParcelas;
					def parcela;
					def marca;
					def idProduto;
					def indisp = 'false';
					
					li.select(".prateleiraLink").each { a ->
						link = a.attr('href');
					}
					li.select(".productImage img").each { a ->
						image = a.attr('src');
					}
					li.select(".data h3 a").each { a ->
						name = a.text().trim();
					}
					li.select(".data .price .valor-por strong").each { a ->
						specialprice = a.text();
					}
					li.select(".data .price .valor-de strong").each { a ->
						price = a.text();
					}
					
					li.select(".data .price .valor-dividido").each { a ->
						try { 
							String temp = a.text().trim();
							if( temp.indexOf("R\$") == -1 ) {
								numParcelas = temp.trim();
							}  else { 
								numParcelas = temp.substring(7, temp.indexOf("R\$") - 4 );
								parcela = temp.substring(temp.indexOf("R\$") + 3 );
							}
						} catch ( Exception ex) { 
							println( a.text().trim() );
							throw ex;
						}
					}
					
					li.select(".promocao-exemplo-frete-gratis").each {dive->
						frete = 'true';
					}
					if(link == null) {
						println( "ERRO: sem link" );
					} else {
						if( specialprice == null && price != null )
							specialprice = price;
						if( price == null && specialprice != null )
							price = specialprice;

						sku = recuperaIdSku( link );
						
						Integer cnt = 0;
						PreparedStatement pStmt = conn.prepareStatement("select count(*) from catalogo where sku = ?");
						pStmt.setString(1, sku);
						ResultSet res = pStmt.executeQuery();
						if( res.next() )
							cnt = res.getInt(1);
						
						ConnHelper.closeResources(pStmt, res);
						
						int idxCol = 0;
						
						String stmt = "insert into catalogo (sku, nome, link, imagem, preco, numero_parcelas, valor_parcelas, console, categoria, preco_promocional) " + 
							" values (?, ?, ?, ?, ?, ? , ? , ? , ?, ? ) ";
						if( cnt > 0 )
							stmt = "update catalogo set sku = ?, nome = ?, link = ?, imagem = ?, preco = ?, " +
							"numero_parcelas = ?, valor_parcelas = ?, console = ?, categoria = ?, preco_promocional = ?  " +
							" where sku = ?";
						pStmt = conn.prepareStatement(stmt);
						
						pStmt.setString(++idxCol, sku);
						pStmt.setString(++idxCol, name);
						pStmt.setString(++idxCol, link);
						pStmt.setString(++idxCol, image);
						pStmt.setString(++idxCol, price);
						pStmt.setString(++idxCol, numParcelas);
						pStmt.setString(++idxCol, parcela);
						pStmt.setString(++idxCol, console);
						pStmt.setString(++idxCol, categoria);
						pStmt.setString(++idxCol, specialprice);
						if( cnt > 0 )
							pStmt.setString( ++idxCol , sku);
						pStmt.executeUpdate();
						ConnHelper.closeResources(pStmt, null);
						println('"' + ( cnt == 0 ? 'add' : 'update') +
								'";"' + sku +
								'";"' + console +
								'";"' + categoria +
								'";"' + name +
								'";"' + image +
								'";"' + link +
								'";"' + price +
								'";"' + numParcelas +
								'";"' + parcela + '"' );
						index++;
						i++;
						achou = true;
					}
				}
				
				if(!achou)
					break;
				
			}
		} finally { 
			ConnHelper.closeResources(conn);
		}
		
		return;
	}
	
	public static void executaProc () {
		CallableStatement callableStatement = null;
		Connection conn = ConnHelper.get().reopen();
		try { 
			callableStatement = conn.prepareCall(" { call atualizar_valores(?) } ");
			callableStatement.setInt(1, 100 );
			callableStatement.execute();
		} finally { 
			if( callableStatement != null ) callableStatement.close();
			ConnHelper.closeResources(conn);
		}
	}
	
	public static void main (String [] args ) {
		/**
		 * Carrega todo o catágo disponíl no site da Dafiti. 
		 */
		recuperarCategorias().each { 
			recuperaProdutosDepto( it );
		}
		
		executaProc();
		
		/**
		 * marca produtos ativos na tabela catalogo
		 */
		//markXml( downloadXml( true ) );
		
		/**
		 * Verifica todos os produtos cadastrados na tabela catalogo com o campo recomendacoes em branco
		 */
		//atualizarRecomendacoes();
	}
	
}

