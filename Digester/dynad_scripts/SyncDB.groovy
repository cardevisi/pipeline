import java.lang.annotation.Documented;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.dynad.imageproxy.*;
import java.util.*;
import java.io.FileReader;

@Grapes([
  @Grab(group='org.apache.poi', module='poi', version='3.7'),
  @Grab(group='commons-codec', module='commons-codec', version='1.6'),
  @Grab(group='org.jsoup', module='jsoup', version='1.6.1'),
  @Grab(group='mysql', module='mysql-connector-java', version='5.1.5'),
  @GrabConfig(systemClassLoader=true)
])

class Sync {
	
	public static Integer printChar(Integer printou, String signal, Integer cnt, Integer pos) {
		try {
                	if(Math.floor(cnt*100/pos)%5 == 0 && printou != Math.floor(cnt*100/pos)) {
                                print(signal + (int)Math.floor(cnt*100/pos) + "%");
				printou = Math.floor(cnt*100/pos);
                        } else {
                                print(signal);
			}
		} catch(Exception ex) {ex.printStackTrace();}

		return printou;
	}
	public static Integer ultimaVersao(Connection connTo, String tabela) {
                PreparedStatement pStmt;
                ResultSet res;
		Integer ver = 0;

                pStmt = connTo.prepareStatement("select max(versao) from " + tabela);
                res = pStmt.executeQuery();
                if( res.next() )
                        ver = res.getInt(1);

		ConnHelper.closeResources(pStmt, res);
		return ver;
	}

	public static Integer contaDesatualizados(Connection connFrom, String tabela, Integer versao) {
                PreparedStatement pStmt;
                ResultSet res;
		Integer cnt = 0;

		String tmp = "select count(*) from " + tabela + " where versao > " + versao;
                pStmt = connFrom.prepareStatement(tmp);
                res = pStmt.executeQuery();
                if( res.next() )
                        cnt = res.getInt(1);

		ConnHelper.closeResources(pStmt, res);
		return cnt;
	}

        public static void go (String fromHost, String toHost, String schemaFrom, String schemaTo) {
		def signal = ".";
		def printou = 0;
		def cnt_cat = 0;
		def cnt_sku = 0;
		def cnt_categ = 0;
		def cnt_bestsellers = 0;
		def cnt_topsellers = 0;
		def cnt_topsellersgenero = 0;

		ConnHelper.schema = schemaFrom;
		ConnHelper.host = fromHost;
                Connection connFrom = new ConnHelper(1).reopen();

		ConnHelper.schema = schemaTo;
		ConnHelper.host = toHost;
		ConnHelper.usuario = 'dynad';
		ConnHelper.senha = 'danyd';
                Connection connTo = new ConnHelper(1).reopen();
	
                PreparedStatement pStmt;
                ResultSet res;
		def versao_cat, versao_sku, versao_categ, versao_bestsellers, versao_topsellers, versao_topsellersgenero;

                versao_cat = ultimaVersao(connTo, "catalogo");
                versao_sku = ultimaVersao(connTo, "codigos_skus");
                versao_categ = ultimaVersao(connTo, "codigos_categorias");
                versao_bestsellers = ultimaVersao(connTo, "best_sellers");
                versao_topsellers = ultimaVersao(connTo, "top_sellers_categoria");
                versao_topsellersgenero = ultimaVersao(connTo, "top_sellers_genero");

                println("ultima versao: " + versao_cat + ", " + versao_sku + ", " + versao_categ + ", " + versao_bestsellers + ", " + versao_topsellers + ", " + versao_topsellersgenero);

		cnt_cat = contaDesatualizados(connFrom, "catalogo", versao_cat);
		cnt_sku = contaDesatualizados(connFrom, "codigos_skus", versao_sku);
		cnt_categ = contaDesatualizados(connFrom, "codigos_categorias", versao_categ);
		cnt_bestsellers = contaDesatualizados(connFrom, "best_sellers", versao_bestsellers);
		cnt_topsellers = contaDesatualizados(connFrom, "top_sellers_categoria", versao_topsellers);
		cnt_topsellersgenero = contaDesatualizados(connFrom, "top_sellers_genero", versao_topsellersgenero);

		def max = cnt_cat + cnt_sku + cnt_categ + cnt_bestsellers + cnt_topsellers + cnt_topsellersgenero;

		def cnt = 0;

//categoria2
//categoria3
//id
//versao

		println("\ntotal codigos_categorias: " + cnt_categ);
                pStmt = connFrom.prepareStatement("select categoria, categoria1, categoria2, categoria3, id, versao from codigos_categorias where versao > " + versao_categ + " order by versao");
                res = pStmt.executeQuery();
                while( res.next() ) {
                        String sql = "select count(*) from codigos_categorias where categoria1 = '" + res.getString(1) + "' and categoria2 = '" + res.getString(2) + "' and categoria3 = '" + res.getString(3) + "'" ;
                        PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                        ResultSet res2 = pStmt2.executeQuery();
                        res2.next();
                        def val = res2.getInt(1);
                        if(val == 0) {
				PreparedStatement pStmt3 = connTo.prepareStatement("insert into codigos_categorias ( categoria, categoria1, categoria2, categoria3, id, versao ) values ( ?, ?, ?, ?, ?, ? ) ");
                                pStmt3.setString(1, res.getString(1));
                                pStmt3.setString(2, res.getString(2));
                                pStmt3.setString(3, res.getString(3));
                                pStmt3.setString(4, res.getString(4));
                                pStmt3.setString(5, res.getString(5));
                                pStmt3.setString(6, res.getString(6));
                        	pStmt3.executeUpdate();
				signal = "+";
                        } else {
				signal = ".";
			}

			printou = printChar(printou, signal, ++cnt, cnt_categ);
                }

		cnt=0;
		printou=0;

//sku
//id
//versao
//id_categoria
		println("\ntotal codigos_skus: " + cnt_sku);
                pStmt = connFrom.prepareStatement("select sku, id, id_categoria, versao from codigos_skus where versao > " + versao_sku + " order by versao");
                res = pStmt.executeQuery();
                while( res.next() ) {
                	String sql = "select count(*) from codigos_skus where sku = '" + res.getString(1) + "'" ;
                	PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                	ResultSet res2 = pStmt2.executeQuery();
                	res2.next();
                        def val = res2.getInt(1);
			if(val == 0) {
				PreparedStatement pStmt3 = connTo.prepareStatement("insert into codigos_skus ( sku, id, id_categoria, versao ) values ( ?, ?, ?, ?)");
                                pStmt3.setString(1, res.getString(1));
                                pStmt3.setString(2, res.getString(2));
                                pStmt3.setString(3, res.getString(3));
                                pStmt3.setString(4, res.getString(4));
                        	pStmt3.executeUpdate();
				signal = "+";
                        } else {
                                PreparedStatement pStmt3 = connTo.prepareStatement("update codigos_skus set id = ?, id_categoria = ?, versao = ? where sku = ? ");
                                pStmt3.setString(2, res.getString(2));
                                pStmt3.setString(3, res.getString(3));
                                pStmt3.setString(4, res.getString(4));
                                pStmt3.setString(1, res.getString(1));
                                pStmt3.executeUpdate();
				signal = "o";
			}

			printou = printChar(printou, signal, ++cnt, cnt_sku);
		}

                cnt=0;
                printou=0;

//id
//sku
//nome
//imagem
//link 
//preco_original
//preco_promocional
//status
//numero_parcelas
//valor_parcelas 
//marca         
//ativo        
//recomendacoes
//codigo      
//categoria1 
//categoria2
//categoria3
//id_skIntu   
//id_categoria 
//versao      
//flag_recomendacao 
//flag_oferta      
//flag_novidade   
//timestamp      
//categoria      
//last_update      
//score
//created_in      

                println("\ntotal catalogo: " + cnt_cat);
                pStmt = connFrom.prepareStatement("""
select 
	id,
	sku,
	nome,
	imagem,
	link ,
	preco_original,
	preco_promocional,
	status,
	numero_parcelas,
	valor_parcelas ,
	marca         ,
	ativo        ,
	recomendacoes,
	codigo      ,
	categoria1 ,
	categoria2,
	categoria3,
	id_sku   ,
	id_categoria ,
	versao      ,
	flag_recomendacao ,
	flag_oferta      ,
	flag_novidade ,
	categoria ,
	last_update ,      
	score ,
	created_in      
from catalogo where versao > """ + versao_cat);
                res = pStmt.executeQuery();
                while( res.next() ) {
                        String sql = "select count(*) from catalogo where sku = '" + res.getString(2) + "' and versao = " + res.getString(20) + " order by versao";
                        PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                        ResultSet res2 = pStmt2.executeQuery();
                        res2.next();
                        def val = res2.getInt(1);

                        if(val == 0) {

                        	sql = "select count(*) from catalogo where sku = '" + res.getString(2) + "' ";
				PreparedStatement pStmt4 = connTo.prepareStatement(sql);
                        	ResultSet res4 = pStmt4.executeQuery();
                        	res4.next();
                        	val = res4.getInt(1);

                        	if(val == 0) {

	                                PreparedStatement pStmt3 = connTo.prepareStatement("""
insert into catalogo (
	id,
	sku,
	nome,
	imagem,
	link ,
	preco_original,
	preco_promocional,
	status,
	numero_parcelas,
	valor_parcelas ,
	marca         ,
	ativo        ,
	recomendacoes,
	codigo      ,
	categoria1 ,
	categoria2,
	categoria3,
	id_sku   ,
	id_categoria ,
	versao      ,
	flag_recomendacao ,
	flag_oferta      ,
	flag_novidade ,
	categoria ,
	last_update ,      
	score ,
	created_in )
values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) """);

					pStmt3.setInt(1, res.getInt(1));
                                        pStmt3.setString(2, res.getString(2));
                                        pStmt3.setString(3, res.getString(3));
                                        pStmt3.setString(4, res.getString(4));
                                        pStmt3.setString(5, res.getString(5));
                                        pStmt3.setString(6, res.getString(6));
                                        pStmt3.setString(7, res.getString(7));
                                        pStmt3.setString(8, res.getString(8));
                                        pStmt3.setString(9, res.getString(9));
                                        pStmt3.setString(10, res.getString(10));
                                        pStmt3.setString(11, res.getString(11));
                                        pStmt3.setString(12, res.getString(12));
                                        pStmt3.setString(13, res.getString(13));
                                        pStmt3.setString(14, res.getString(14));
                                        pStmt3.setString(15, res.getString(15));
                                        pStmt3.setString(16, res.getString(16));
                                        pStmt3.setString(17, res.getString(17));
                                        pStmt3.setInt(18, res.getInt(18));
                                        pStmt3.setInt(19, res.getInt(19));
                                        pStmt3.setInt(20, res.getInt(20));
                                        pStmt3.setInt(21, res.getInt(21));
                                        pStmt3.setInt(22, res.getInt(22));
                                        pStmt3.setInt(23, res.getInt(23));
                                        pStmt3.setString(24, res.getString(24));
                                        pStmt3.setString(25, res.getString(25));
                                        pStmt3.setInt(26, res.getInt(26));
                                        pStmt3.setString(27, res.getString(27));
					signal = "+";
                        		pStmt3.executeUpdate();
				} else {

					sql = """
update catalogo set 
	id = ?,
	nome = ?,
	imagem = ?,
	link  = ?,
	preco_original = ?,
	preco_promocional = ?,
	status = ?,
	numero_parcelas = ?,
	valor_parcelas  = ?,
	marca  = ?,
	ativo  = ?,
	recomendacoes = ?,
	codigo  = ?,
	categoria1  = ?,
	categoria2 = ?,
	categoria3 = ?,
	id_sku  = ?,
	id_categoria = ?,
	versao = ?,
	flag_recomendacao = ?,
	flag_oferta = ?,
	flag_novidade = ?,
	categoria = ?,
	last_update = ?,      
	score = ?,
	created_in = ?
where sku = ? """;

	                                PreparedStatement pStmt3 = connTo.prepareStatement(sql);
					pStmt3.setInt(1, res.getInt(1));
                                        pStmt3.setString(2, res.getString(3));
                                        pStmt3.setString(3, res.getString(4));
                                        pStmt3.setString(4, res.getString(5));
                                        pStmt3.setString(5, res.getString(6));
                                        pStmt3.setString(6, res.getString(7));
                                        pStmt3.setString(7, res.getString(8));
                                        pStmt3.setString(8, res.getString(9));
                                        pStmt3.setString(9, res.getString(10));
                                        pStmt3.setString(10, res.getString(11));
                                        pStmt3.setString(11, res.getString(12));
                                        pStmt3.setString(12, res.getString(13));
                                        pStmt3.setString(13, res.getString(14));
                                        pStmt3.setString(14, res.getString(15));
                                        pStmt3.setString(15, res.getString(16));
                                        pStmt3.setString(16, res.getString(17));
                                        pStmt3.setInt(17, res.getInt(18));
                                        pStmt3.setInt(18, res.getInt(19));
                                        pStmt3.setInt(19, res.getInt(20));
                                        pStmt3.setInt(20, res.getInt(21));
                                        pStmt3.setInt(21, res.getInt(22));
                                        pStmt3.setInt(22, res.getInt(23));
                                        pStmt3.setString(23, res.getString(24));
                                        pStmt3.setString(24, res.getString(25));
                                        pStmt3.setInt(25, res.getInt(26));
                                        pStmt3.setString(26, res.getString(27));
                                        pStmt3.setString(27, res.getString(2));
                                	signal = "o";
                        		pStmt3.executeUpdate();
                        		//try { ResultSet res3 = pStmt3.executeUpdate(); } catch(Exception ex) { signal = "E"; }
				}
                        } else {
                                signal = ".";
                        }

			printou = printChar(printou, signal, ++cnt, cnt_cat);
                }

//pos
//id_sku
//versao
                println("\ntotal bestsellers: " + cnt_bestsellers);
                pStmt = connFrom.prepareStatement("select pos, id_sku, versao from best_sellers where versao > " + versao_bestsellers + " order by versao" );
                res = pStmt.executeQuery();
                while( res.next() ) {
                        String sql = "select count(*) from best_sellers where pos = " + res.getInt(1)  ;
                        PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                        ResultSet res2 = pStmt2.executeQuery();
                        res2.next();
                        def val = res2.getInt(1);
                        if(val == 0) {
                                PreparedStatement pStmt3 = connTo.prepareStatement("insert into best_sellers ( pos, id_sku, versao ) values ( ?, ?, ?)");
                                pStmt3.setInt(1, res.getInt(1));
                                pStmt3.setInt(2, res.getInt(2));
                                pStmt3.setInt(3, res.getInt(3));
                                pStmt3.executeUpdate();
                                signal = "+";
                        } else {
                                PreparedStatement pStmt3 = connTo.prepareStatement("update best_sellers set id_sku = ?, versao = ? where pos = ? ");
                                pStmt3.setInt(2, res.getInt(2));
                                pStmt3.setInt(3, res.getInt(3));
                                pStmt3.setInt(1, res.getInt(1));
                                pStmt3.executeUpdate();
                                signal = "o";
                        }

                        printou = printChar(printou, signal, ++cnt, cnt_bestsellers);
                }

                cnt=0;
                printou=0;

//pos
//id_categoria
//id_sku
//tipo
//versao
                println("\ntotal top_sellers_categoria: " + cnt_topsellers);
                pStmt = connFrom.prepareStatement("select pos, id_categoria, id_sku, tipo, versao from top_sellers_categoria where versao > " + versao_topsellers + " order by versao");
                res = pStmt.executeQuery();
                while( res.next() ) {
                        String sql = "select count(*) from top_sellers_categoria where pos = " + res.getInt(1) + " AND id_categoria = " + res.getInt(2) ;
                        PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                        ResultSet res2 = pStmt2.executeQuery();
                        res2.next();
                        def val = res2.getInt(1);
                        if(val == 0) {
                                PreparedStatement pStmt3 = connTo.prepareStatement("insert into top_sellers_categoria ( pos, id_categoria, id_sku, tipo, versao ) values ( ?, ?, ?, ?, ?)");
                                pStmt3.setInt(1, res.getInt(1));
                                pStmt3.setInt(2, res.getInt(2));
                                pStmt3.setInt(3, res.getInt(3));
                                pStmt3.setString(4, res.getString(4));
                                pStmt3.setInt(5, res.getInt(5));
                                pStmt3.executeUpdate();
                                signal = "+";
                        } else {
                                PreparedStatement pStmt3 = connTo.prepareStatement("update top_sellers_categoria set id_sku = ?, tipo = ?, versao = ? where pos = ? AND id_categoria = ? ");
                                pStmt3.setInt(1, res.getInt(3));
                                pStmt3.setString(2, res.getString(4));
                                pStmt3.setInt(3, res.getInt(5));
                                pStmt3.setInt(4, res.getInt(1));
                                pStmt3.setInt(5, res.getInt(2));
                                pStmt3.executeUpdate();
                                signal = "o";
                        }

                        printou = printChar(printou, signal, ++cnt, cnt_topsellers);
                }

                cnt=0;
                printou=0;


//pos
//id_sku
//genero
//tipo
//versao
                println("\ntotal top_sellers_genero: " + cnt_topsellersgenero);
                pStmt = connFrom.prepareStatement("select pos, id_sku, genero, tipo, versao from top_sellers_genero where versao > " + versao_topsellersgenero +" order by versao");
                res = pStmt.executeQuery();
                while( res.next() ) {
                        String sql = "select count(*) from top_sellers_genero where pos = " + res.getInt(1) + " AND genero = '" + res.getString(3) + "' AND tipo = '" + res.getString(4) + """'""" ;
                        PreparedStatement pStmt2 = connTo.prepareStatement(sql);
                        ResultSet res2 = pStmt2.executeQuery();
                        res2.next();
                        def val = res2.getInt(1);
                        if(val == 0) {
                                PreparedStatement pStmt3 = connTo.prepareStatement("insert into top_sellers_genero ( pos, id_sku, genero, tipo, versao ) values ( ?, ?, ?, ?, ?)");
                                pStmt3.setInt(1, res.getInt(1));
                                pStmt3.setInt(2, res.getInt(2));
                                pStmt3.setString(3, res.getString(3));
                                pStmt3.setString(4, res.getString(4));
                                pStmt3.setInt(5, res.getInt(5));
                                pStmt3.executeUpdate();
                                signal = "+";
                        } else {
                                PreparedStatement pStmt3 = connTo.prepareStatement("update top_sellers_genero set id_sku = ?,  versao = ? where pos = ? AND genero = ? AND tipo = ?");
                                pStmt3.setInt(1, res.getInt(2));
                                pStmt3.setString(2, res.getString(5));
                                pStmt3.setInt(3, res.getInt(1));
                                pStmt3.setString(4, res.getString(3));
                                pStmt3.setString(5, res.getString(4));
                                pStmt3.executeUpdate();
                                signal = "o";
                        }

                        printou = printChar(printou, signal, ++cnt, cnt_topsellers);
                }

                cnt=0;
                printou=0;



        }

	public static void main (String [] args ) {
		println("sincronizando de: [" + args[0] + "] para: [" + args[1] + "] do schema: [" + args[2] + "] no schema: [" + args[3] + "]");
		go( args[0], args[1], args[2], args[3] );
	}
	
}
