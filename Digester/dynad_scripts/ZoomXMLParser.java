package net.dynad.parser.logs.barraofertas;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class ZoomXMLParser {
	private static String getFieldValue(String line) {
		String backupLine = line;
		try {
			if (line.indexOf(">") > -1) line = line.substring(line.indexOf('>') + 1);
			boolean cdata = false;
			if (line.indexOf("[CDATA[") > -1) {
				line = line.substring(line.indexOf("[CDATA[") + "[CDATA[".length());
				cdata = true;
			}
			if (cdata && line.indexOf("]]>") > -1) line = line.substring(0, line.indexOf("]]>"));

			if (!cdata && line.indexOf("</") > -1) line = line.substring(0, line.indexOf("</"));
			return line;
		} catch (java.lang.StringIndexOutOfBoundsException e) {
			throw new RuntimeException("[" + backupLine + "] = [" + line + "]", e);
		}
	}

	public static void main(String[] args) {
		File xmlFile = null, txtFile = null, recFile = null;
		for (String arg: args) {
			if (arg.startsWith("-x")) xmlFile = new File(arg.substring(2));
			if (arg.startsWith("-o")) txtFile = new File(arg.substring(2));
			if (arg.startsWith("-r")) recFile = new File(arg.substring(2));			
		}

		if (txtFile.exists()) txtFile.delete();

		try (
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(xmlFile), "UTF-8"));
		BufferedWriter bwtxt = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(txtFile), "UTF-8"));) {
			
			String line = null;
			//RECOMENDACOES
			Map < String, String > direct = new HashMap < String, String > ();
			try {
				BufferedReader directRec = new BufferedReader(new InputStreamReader(new FileInputStream(recFile), "UTF-8"));
				while ((line = directRec.readLine()) != null) {
					if (line == null || line.trim().equals("") || line.indexOf(";") == -1) continue;
					line = line.trim();
					if (!direct.containsKey(line.split(";")[0]))
						direct.put(line.split(";")[0], line.split(";")[1]);
				}
			} catch (Exception recError) {}

			line = null;
			boolean opened = false;
			String offerId = null, name = null, topCategoryId = null, topCategoryName = null, categoryId = null, categoryName = null, price = null, parcelNumbers = null, parcelValues = null, storeId = null, storeName = null, url = null, image = null;

			bwtxt.write("metadata|sku|nome|imagem|link|categoria|preco_original|preco_promocional|numero_parcelas|valor_parcelas|recomendacoes\r\n");
			bwtxt.flush();

			while ((line = br.readLine()) != null) {
				if (line == null || line.trim().equals("")) continue;
				line = line.trim();

				if (line.startsWith("<item>")) {
					opened = true;
				} else if (opened) {

					if (line.startsWith("<id>")) offerId = getFieldValue(line);
					if (line.startsWith("<offer-id>") && (offerId == null || offerId.trim().equals(""))) offerId = getFieldValue(line);
					if (line.startsWith("<prod-id>") && (offerId == null || offerId.trim().equals(""))) offerId = getFieldValue(line);
					if (line.startsWith("<name>")) name = getFieldValue(line);
					if (line.startsWith("<category-id>")) categoryId = getFieldValue(line);
					if (line.startsWith("<category-name>")) categoryName = getFieldValue(line);
					if (line.startsWith("<top-parent-category-id>")) topCategoryId = getFieldValue(line);
					if (line.startsWith("<top-parent-category-name>")) topCategoryName = getFieldValue(line);
					if (line.startsWith("<price>")) price = getFieldValue(line);
					if (line.startsWith("<parcel-numbers>")) parcelNumbers = getFieldValue(line);
					if (line.startsWith("<parcel-values>")) parcelValues = getFieldValue(line);
					if (line.startsWith("<url>")) url = getFieldValue(line);
					if (line.startsWith("<image-url>")) image = getFieldValue(line);

					if (line.startsWith("</item>")) {
						opened = false;
						if (parcelNumbers == null || parcelValues == null) {
							parcelNumbers = "1";
							parcelValues = price;
						}

						if (offerId != null && !offerId.trim().equals("")) {							
							bwtxt.write(offerId + "|" + name.replaceAll("\\|", "") + "|" + image.replaceAll("\\|", "") + "|" + url.replaceAll("\\|", "") + "|" + categoryName.replaceAll("\\|", "") + "|" + price.replaceAll("\\|", "") + "|" + price.replaceAll("\\|", "") + "|" + parcelNumbers.replaceAll("\\|", "") + "|" + parcelValues.replaceAll("\\|", "") + "|" + ((direct.get(offerId) != null) ? direct.get(offerId) : "") + "\r\n");
							bwtxt.flush();
						}

						offerId = null;
						name = null;
						categoryId = null;
						categoryName = null;
						topCategoryId = null;
						topCategoryName = null;
						price = null;
						parcelNumbers = null;
						parcelValues = null;
						storeId = null;
						storeName = null;
						url = null;
						image = null;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {}
	}
}