echo "INICIO - Barreds"

date
START=$(date +%s)

### PARAMETROS ###
TEMP_NAME=bomnegocio
SCHEMA=dynad_bomnegocio
DB_HOST=64.31.10.123
EXP_HOST="64.31.10.123"
DIGESTER=/root/dynad_scripts/DigesterBomNegocio.groovy
##################

RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'
cd /root/dynad_scripts

run_digester=0
if [ "$(ls -A /mnt/XML/bomnegocio/pendentes)" ]
then
run_digester=1
fi

if [ $run_digester -eq 1 ]
then
echo "executando script de importacao ..."
groovy $DIGESTER 2 || exit 1

echo "executando script de normalizacao ..."
mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

echo "fazendo export do banco ..."
mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

for ip in $EXP_HOST; do
    echo "exportando para $ip"
    mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
done
fi
