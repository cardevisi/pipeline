echo 'INICIO - Bonprix'

date
START=$(date +%s)

### PARAMETROS ###'
FTP_USER='BonprixBR'
FTP_PASSWORD='Ni3jKiphl7un'
FTP_FILE='bonprixbr_retargeting.csv'
FTP_HOST='ftp.hurra.com'
FILE='/mnt/XML/bonprix.csv'
TEMP_NAME='bonprix'
SCHEMA='dynad_bonprix'
DB_HOST='23.23.92.125'
EXP_HOST='23.23.92.125 68.233.252.114 216.144.240.50 54.232.127.115 54.232.127.124'
DIGESTER=/root/dynad_scripts/DigesterBonprix.groovy
SNFILE=/mnt/bonprix.txt
SNSERVER='200.147.166.24 200.147.166.25 200.147.166.26 200.147.166.27'
SNDATASOURCE=bonprix
##################


RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $FILE.old.4
mv $FILE.old.3 $FILE.old.4
mv $FILE.old.2 $FILE.old.3
mv $FILE.old.1 $FILE.old.2
mv $FILE.old $FILE.old.1
mv $FILE $FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo 'baixando csv ...'

ftp -n $FTP_HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
get $FTP_FILE $FILE
quit
END_SCRIPT

wget $FTP_HOST -O $FILE
#curl $FTP_HOST | iconv -f iso8859-1 -t utf-8 > $FILE

echo 'exportando ranking de skus da producao ...'
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo 'importando ranking de skus local ...'
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo 'gerando ranking de skus ...'
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi




cmp -s $FILE $FILE.old >/dev/null 
if [ $? -ne 0 ]
then

  echo 'executando script de importacao ...'
  groovy $DIGESTER 2 || exit 1

  echo 'executando script de normalizacao ...'
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo 'fazendo export do banco ...'
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
     echo 'exportando para $ip'
     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done

  echo 'atualizando supernova 2 ...'
  for ip in $SNSERVER; do
     echo 'exportando para $ip'
     curl -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
  done

else

  if [ $RANKING -eq 1 ]
  then
    echo 'executando script de importacao ...'  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo 'fazendo export do banco (best_sellers) ...'
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
       echo 'exportando para $ip'
       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done

    echo 'atualizando supernova 2 ...'
    for ip in $SNSERVER; do
       echo 'exportando para $ip'
       curl -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
    done

  else
    echo '*** nada a fazer ...'
  fi

fi

date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo 'operacao finalizada em $DIFF segundos - $SCHEMA'

echo 'FIM***'
