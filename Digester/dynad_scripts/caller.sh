#!/usr/bin/sh
cd /root/dynad_scripts

START=$(date +%s)
RAND=$RANDOM
LOG_FILE=/tmp/$START$RAND.log
LOG_ERROR=/tmp/error$START$RAND.log

echo "..." > $LOG_FILE
#echo "counting..."
#echo "counting..." >> $LOG_FILE

cnt=`ps -ef | grep 'caller.sh' | grep -v 'grep' | grep -v $$ | grep "$1" | grep -c '\.sh'`

#echo "[$cnt]"
#echo "[$cnt]" >> $LOG_FILE

if [ "$cnt" -eq 1 ]
then
        proc=$1.sh
        echo "STARTING: "$proc
        echo "STARTING: "$proc >> $LOG_FILE

        sh $proc >> $LOG_FILE  2>$LOG_ERROR || /root/dynad_scripts/sendEmail  -f "DynAd Digester <root@asapcode.com>" -t dynad.monitor@asapcode.com -s smtp.asapcode.com -u "smtp" -m "Falhou ao executar o script $proc\n\n------x------\n`cat $LOG_ERROR`\n------x------" -u "DynAd Digester Error | `date +%Y-%m-%d` | $proc"

        END=$(date +%s)
        DIFF=$(( $END - $START ))

        echo "ENDING: ($DIFF s)"$proc >> $LOG_FILE
        echo "ENDING: ($DIFF s)"$proc

	cat $LOG_FILE >> caller.log

	rm -f $LOG_FILE
	rm -f $LOG_ERROR
else
	echo "still running"
fi


