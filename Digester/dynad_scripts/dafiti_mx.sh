echo "INICIO - Dafiti MX"
date
START=$(date +%s)

### PARAMETROS ###'
XML_URL=XML_URL=http://marketplace.dafiti.com.mx/publicxml/shop.xml
XML_FILE=/mnt/TMP/xml/dafiti_mx.xml
DIGESTER=/root/dynad_scripts/DigesterDafitiMX.groovy
SNFILE=/mnt/TMP/sna-files/dafiti_mx.txt
SNSERVER="200.147.166.24 200.147.166.25 200.147.166.26 200.147.166.27"
SNDATASOURCE=dafiti_mx

#MANTENDO O HISTORICO DE XML'S
rm $XML_FILE.old.4
mv $XML_FILE.old.3 $XML_FILE.old.4
mv $XML_FILE.old.2 $XML_FILE.old.3
mv $XML_FILE.old.1 $XML_FILE.old.2
mv $XML_FILE.old $XML_FILE.old.1
mv $XML_FILE $XML_FILE.old

echo "baixando xml ..."
wget $XML_URL -O $XML_FILE

echo "atualizando supernova 2 ..."
for ip in $SNSERVER; do
        echo "exportando para $ip"
        curl -k -F file=@$SNFILE -u teste:teste https://$ip/updater/$SNDATASOURCE
done

date
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "operacao finalizada em $DIFF segundos - $SCHEMA"
echo "FIM***"