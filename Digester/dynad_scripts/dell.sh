echo "INICIO - Dell"

date
START=$(date +%s)

### PARAMETROS ###
TEMP_NAME=dellxml
SCHEMA=dynad_dell
DB_HOST=68.233.252.113
EXP_HOST="68.233.252.113 23.23.92.125 216.144.240.50 54.232.127.115 54.232.127.124"
DIGESTER=/root/dynad_scripts/DigesterDell.groovy
##################


RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE

RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi

if [ -e "/mnt/XML/dell.xml" ]
then
copyxml="/mnt/XML/dell/$START.dell.xml"
cp /mnt/XML/dell.xml $copyxml
fi

if [ -e "/mnt/XML/dell2.xml" ]
then
copyxml="/mnt/XML/dell/$START.dell2.xml"
cp /mnt/XML/dell2.xml $copyxml
fi


cmp1=`sh /root/dynad_scripts/download.sh "/mnt/XML/dell.xml" "http://webadapters.channeladvisor.com/CSEAdapter/Default.aspx?pid=V%5bP%5e%5eJcXA%3aHLqg(%2b%23%5b5u%2f%22QjvND_%5b%2b%253%5bG%5dPVgvz%404(*%27K%60F0QUkDv%3db%25%2c%25%5bbudYQ8EHH_U%2b%26c4L%5c%27)%3dHH"`
cmp2=`sh /root/dynad_scripts/download.sh "/mnt/XML/dell2.xml" "http://www.rf.nl/dellspider/DellFeed.aspx?c=br&l=pt&a=dhs&ena=0"`
echo "[cmp1: $cmp1 / cmp2 : $cmp2]"
if [ "$cmp1" -ne 0 -o "$cmp2" -ne 0  ]
then
  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  for ip in $EXP_HOST; do
     echo "exportando para $ip"
     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  done

else

  if [ $RANKING -eq 1 ]
  then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

    echo "fazendo export do banco (best_sellers) ..."
    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    for ip in $EXP_HOST; do
       echo "exportando para $ip"
       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    done

  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"

