STORE_FILE=$1
DOWNLOAD_FILE=$2

rm $STORE_FILE.old.4
mv $STORE_FILE.old.3 $STORE_FILE.old.4
mv $STORE_FILE.old.2 $STORE_FILE.old.3
mv $STORE_FILE.old.1 $STORE_FILE.old.2
mv $STORE_FILE.old $STORE_FILE.old.1
mv $STORE_FILE $STORE_FILE.old

wget $DOWNLOAD_FILE -O $STORE_FILE

RANKING=1
cmp -s "$STORE_FILE" "$STORE_FILE.old"
rescmp=$?

if [ $rescmp -ne 0 ]
then
  RANKING=1
else
  RANKING=0
fi
echo $RANKING
