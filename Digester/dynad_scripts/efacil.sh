echo "INICIO - Centauro"

date
START=$(date +%s)

### PARAMETROS ###
XML_URL=http://www.efacil.com.br/comparadores/NEXTPERFORMANCE/NEXTPERFORMANCE.XML
XML_FILE=/mnt/XML/efacil.xml
TEMP_NAME=efacil
SCHEMA=dynad_efacil
DB_HOST=10.147.159.189
EXP_HOST="64.31.10.122 68.233.252.114 74.81.70.46 216.144.240.50 54.232.127.124 54.163.247.172"
DIGESTER=/root/dynad_scripts/DigesterEfacil.groovy
#SNDATASOURCE=efacil
##################





RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

mv $XML_FILE.old.4 $XML_FILE.old.5
mv $XML_FILE.old.3 $XML_FILE.old.4
mv $XML_FILE.old.2 $XML_FILE.old.3
mv $XML_FILE.old.1 $XML_FILE.old.2
mv $XML_FILE.old $XML_FILE.old.1
mv $XML_FILE $XML_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."
wget $XML_URL -O $XML_FILE

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi




cmp -s $XML_FILE $XML_FILE.old >/dev/null 
if [ $? -ne 0 ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

  #for ip in $EXP_HOST; do
  #   echo "exportando para $ip"
  #   mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
  #done
else

  if [ $RANKING -eq 1 ]
  then
    echo "executando script de importacao ..."  
    groovy $DIGESTER BEST_SELLERS 2 || exit 1

#    echo "fazendo export do banco (best_sellers) ..."
#    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE

    #for ip in $EXP_HOST; do
    #   echo "exportando para $ip"
    #   mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
    #done
  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"

