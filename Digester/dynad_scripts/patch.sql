delimiter ;
update catalogo set categoria1 = IFNULL(categoria1, 'empty'),
categoria2 = IFNULL(categoria2, 'empty'),
categoria3 = IFNULL(categoria3, 'empty'),
categoria4 = IFNULL(categoria4, 'empty'),
categoria5 = IFNULL(categoria5, 'empty');

update codigos_categorias set categoria1 = IFNULL(categoria1, 'empty'),
categoria2 = IFNULL(categoria2, 'empty'),
categoria3 = IFNULL(categoria3, 'empty'),
categoria4 = IFNULL(categoria4, 'empty'),
categoria5 = IFNULL(categoria5, 'empty');

delimiter ;
DROP PROCEDURE IF EXISTS DEBUG;

delimiter ||

CREATE PROCEDURE `DEBUG`(mes varchar(512))
BEGIN
    SELECT mes  AS "Statement";
END
||

delimiter ;


delimiter ;
DROP PROCEDURE IF EXISTS popula_supernova;

delimiter ||

CREATE PROCEDURE popula_supernova()
BEGIN
   DECLARE done INT DEFAULT FALSE;
   DECLARE v_id int(8);
   DECLARE v_sku varchar(45);
   DECLARE v_categoria1 varchar(256);
   DECLARE v_categoria2 varchar(256);
   DECLARE v_categoria3 varchar(256);
   DECLARE v_categoria4 varchar(256);
   DECLARE v_categoria5 varchar(256);
   DECLARE cnt int(8);
   DECLARE v_id_sku int;
   DECLARE v_id_categoria int;
   DECLARE v_key int;

   DECLARE cur1 CURSOR FOR
       SELECT
          id,
          sku,
          IFNULL(categoria1, 'empty'),
          IFNULL(categoria2, 'empty'),
          IFNULL(categoria3, 'empty'),
          IFNULL(categoria4, 'empty'),
          IFNULL(categoria5, 'empty')
       FROM
          catalogo;

   DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

   SELECT max(id) + 1 INTO v_key FROM (select id from catalogo union select id from codigos_skus union select id from codigos_categorias) as a;

   OPEN cur1;

   read_loop: LOOP

          FETCH cur1 INTO v_id, v_sku, v_categoria1, v_categoria2, v_categoria3, v_categoria4, v_categoria5;
          IF done THEN
            LEAVE read_loop;
          END IF;

          SELECT count(*) INTO cnt FROM codigos_categorias WHERE
            IFNULL(categoria1, 'empty') = IFNULL(v_categoria1, 'empty')
            AND IFNULL(categoria2, 'empty') = IFNULL(v_categoria2, 'empty')
            AND IFNULL(categoria3, 'empty') = IFNULL(v_categoria3, 'empty')
            AND IFNULL(categoria4, 'empty') = IFNULL(v_categoria4, 'empty')
            AND IFNULL(categoria5, 'empty') = IFNULL(v_categoria5, 'empty');

          IF cnt = 0 THEN
             -- CALL DEBUG(concat(concat("categoria:",v_categoria1, v_categoria2),v_categoria3));
             SET v_key = v_key + 1;
            INSERT INTO codigos_categorias 
              (id, categoria, categoria1, categoria2, categoria3, categoria4, categoria5, versao) 
            VALUES 
              (v_key, concat( concat( concat(concat(IFNULL(v_categoria1, 'empty') , '/', IFNULL(v_categoria2, 'empty')), '/', IFNULL(v_categoria3, 'empty')), '/', IFNULL(v_categoria4, 'empty')), '/', IFNULL(v_categoria5, 'empty')),
              IFNULL(v_categoria1, 'empty'), IFNULL(v_categoria2, 'empty'), IFNULL(v_categoria3, 'empty'), IFNULL(v_categoria4, 'empty'), IFNULL(v_categoria5, 'empty'), UNIX_TIMESTAMP());
          END IF;
   END LOOP;

   CLOSE cur1;

   UPDATE catalogo a SET a.versao = unix_timestamp(), a.id_sku = a.id WHERE a.id_sku <> a.id or a.id_sku is null;
   UPDATE catalogo a SET a.id_categoria = null 
    WHERE a.id_categoria <> ( SELECT b.id FROM codigos_categorias b WHERE 
      IFNULL(a.categoria1, 'empty') = IFNULL(b.categoria1, 'empty')
      AND IFNULL(a.categoria2, 'empty') = IFNULL(b.categoria2, 'empty')
      AND IFNULL(a.categoria3, 'empty') = IFNULL(b.categoria3, 'empty')
      AND IFNULL(a.categoria4, 'empty') = IFNULL(b.categoria4, 'empty')
      AND IFNULL(a.categoria5, 'empty') = IFNULL(b.categoria5, 'empty') ) ;

   UPDATE catalogo a SET a.versao = unix_timestamp(), a.recomendacoes = '', 
    a.id_categoria = ( SELECT b.id FROM codigos_categorias b WHERE 
      IFNULL(a.categoria1, 'empty') = IFNULL(b.categoria1, 'empty') 
      AND IFNULL(a.categoria2, 'empty') = IFNULL(b.categoria2, 'empty') 
      AND IFNULL(a.categoria3, 'empty') = IFNULL(b.categoria3, 'empty')
      AND IFNULL(a.categoria4, 'empty') = IFNULL(b.categoria4, 'empty')
      AND IFNULL(a.categoria5, 'empty') = IFNULL(b.categoria5, 'empty') ) WHERE a.id_categoria IS NULL OR a.id_categoria = '';

END
||

delimiter ;

delimiter ||

 CREATE FUNCTION `autoNormalizaMoeda`(s varchar(16)) RETURNS decimal(30,2)
    NO SQL
    DETERMINISTIC
BEGIN

DECLARE saida decimal(30,2);
DECLARE tam INT(2);
DECLARE t1 INT(2);

set t1 = 0;

if length(substring_index(s, ',', -1)) = 2 or length(substring_index(s, '.', -1)) = 2 then set t1 = 2; end if;

set s = NumericOnly(s);

select length(s) into tam from dual;

if t1 = 2 then select concat(substring(s, 1, length(s)-2), '.', substring(s, length(s)-1)) into s from dual; end if;

if tam <= 2 then return cast(s as decimal(30,0)); end if;

if t1 = 2 then return CAST(s AS DECIMAL(30, 2)); else return CAST(s AS DECIMAL(30, 0));  end if;
END

||

delimiter ;


delimiter ||

CREATE FUNCTION `NumericOnly`(val VARCHAR(255)) RETURNS varchar(255) CHARSET latin1
BEGIN
 DECLARE idx INT DEFAULT 0;
 IF ISNULL(val) THEN RETURN NULL; END IF;

 IF LENGTH(val) = 0 THEN RETURN ""; END IF;

 SET idx = REPLACE(val,".","");
 SET idx = LENGTH(val);
  WHILE idx > 0 DO
  IF IsNumeric(SUBSTRING(val,idx,1)) = 0 THEN
   SET val = REPLACE(val,SUBSTRING(val,idx,1),"");
   SET idx = LENGTH(val)+1;
  END IF;
  SET idx = idx - 1;
  END WHILE;
  RETURN val;
END

||

delimiter ;


delimiter ||

CREATE FUNCTION `IsNumeric`(val varchar(255)) RETURNS tinyint(4)
RETURN val REGEXP '^(-|\\+){0,1}([0-9]+\\.[0-9]*|[0-9]*\\.[0-9]+|[0-9]+)$'

||

delimiter ;
