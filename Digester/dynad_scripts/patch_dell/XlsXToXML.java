
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.hssf.usermodel.HeaderFooter.file;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//"\Program Files\Java\jdk1.7.0_09\bin\java.exe" -cp .;poi-3.10-FINAL-20140208.jar;ooxml-schemas-1.1.jar;poi-ooxml-3.10-FINAL.jar;XlsXToXML.class;xmlbeans-2.4.0.jar;dom4j-20040902.021138.jar XlsXToXML xml_uol.xlsx xml.xml
public class XlsXToXML { 
	public static String getCellAsString ( Cell cell ) { 
		switch(cell.getCellType()){
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue().trim();
            case Cell.CELL_TYPE_NUMERIC:
                return String.valueOf( cell.getNumericCellValue() );
            default:
            	return null;    
        }
    }

	public static void convert( String file, String outFile ) {
		java.io.OutputStream out = null;
		try { 
			InputStream excel = new FileInputStream(file);

			XSSFWorkbook workbook = new XSSFWorkbook(excel);
	        XSSFSheet folha = workbook.getSheetAt(0);
	        XSSFRow  linhaAtual = null;
	        Iterator linhas = folha.rowIterator();
	        int index = 0;

	        java.io.File f = new java.io.File(outFile);
	        if( f.exists() ) f.delete();

	        out = new java.io.FileOutputStream( f );
	        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<products>\n".getBytes());
	        while( linhas.hasNext() ) {
	        	linhaAtual=(XSSFRow) linhas.next();
	        	if( ++index == 1 ) continue;

	        	String sku = linhaAtual.getCell(0).getStringCellValue();
	        	String image = linhaAtual.getCell(6).getStringCellValue();
	        	String oprice = getCellAsString( linhaAtual.getCell(2) );
	            String fprice = getCellAsString( linhaAtual.getCell(2) );
	            String nparcelas = getCellAsString( linhaAtual.getCell(3) );
	            //nparcelas = nparcelas.substring(0, nparcelas.indexOf("."));
	            String vparcelas = getCellAsString( linhaAtual.getCell(4) );
	            String url = linhaAtual.getCell(5).getStringCellValue();
	            String name = linhaAtual.getCell(1).getStringCellValue();
	            String marca = "Dell";
			try { marca = getCellAsString( linhaAtual.getCell(7) ); } catch ( Exception exxx ) { exxx.printStackTrace(); }
	            
	            out.write( ("<product>\n").getBytes());
	            out.write( ("<sku><![CDATA["+sku+"]]></sku>\n").getBytes("UTF-8"));
	            out.write( ("<image><![CDATA["+image+"]]></image>\n").getBytes("UTF-8"));
	            out.write( ("<oprice><![CDATA["+oprice+"]]></oprice>\n").getBytes("UTF-8"));
	            out.write( ("<fprice><![CDATA["+fprice+"]]></fprice>\n").getBytes("UTF-8"));
	            out.write( ("<nparcelas><![CDATA["+nparcelas+"]]></nparcelas>\n").getBytes("UTF-8"));
	            out.write( ("<vparcelas><![CDATA["+vparcelas+"]]></vparcelas>\n").getBytes("UTF-8"));
	            out.write( ("<url><![CDATA["+url+"]]></url>\n").getBytes("UTF-8"));
	            out.write( ("<name><![CDATA["+name+"]]></name>\n").getBytes("UTF-8"));
	            out.write( ("<cta><![CDATA["+marca+"]]></cta>\n").getBytes("UTF-8"));
	            out.write( ("</product>\n").getBytes());

	            System.out.println( "line: " + (index) + " / " + sku );
	        }
	        out.write("</products>".getBytes());

	    } catch (Exception ex ) { 
	    	ex.printStackTrace();
	    } finally { 
	    	try { if( out != null ) out.close(); } catch ( Exception exx ) {}
	    }
	}

	public static void main (String [] args ) { 
		XlsXToXML.convert( args[0], args[1] );
	}
}
