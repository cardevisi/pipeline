delete from ranking_by_day where date < date_add(now(), interval -7 day);
update catalogo set score = 0;
UPDATE catalogo c INNER JOIN ( SELECT sku, SUM(score) as score FROM ranking_by_day GROUP BY sku ) x ON c.sku = x.sku SET c.score = x.score;
