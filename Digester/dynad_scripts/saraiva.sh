echo "INICIO - Saraiva"

date
START=$(date +%s)

### PARAMETROS ###
XML_URL1="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0304&empresa=16&tipo=1"
XML_URL2="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0305&empresa=16&tipo=1"
XML_URL3="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0312&empresa=16&tipo=1"
XML_URL4="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0318&empresa=16&tipo=1"
XML_URL5="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0301&empresa=16&tipo=1"
XML_URL6="http://buscador.livrariasaraiva.com.br/entregador/default.aspx?buscador=150&familia=0303&empresa=16&tipo=1"
XML_FILE=/mnt/XML/saraiva.xml
TEMP_NAME=sara
SCHEMA=dynad_saraiva
DB_HOST=74.81.70.46
EXP_HOST="68.233.252.114 23.23.92.125 74.81.70.46 54.232.127.115"
DIGESTER=/root/dynad_scripts/DigesterSaraiva.groovy
##################





RANKING_FILE='/mnt/TMP/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='/mnt/TMP/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old.2
mv $XML_FILE.old.1 $XML_FILE.old.2
rm $XML_FILE.old.1
mv $XML_FILE.old $XML_FILE.old.1
rm $XML_FILE.old
mv $XML_FILE $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

echo "baixando xml ..."
wget $XML_URL1 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | head -n `expr $a - 1` > $XML_FILE
wget $XML_URL2 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | tail -n `expr $a - 2` | head -n `expr $a - 3` >> $XML_FILE
wget $XML_URL3 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | tail -n `expr $a - 2` | head -n `expr $a - 3` >> $XML_FILE
wget $XML_URL4 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | tail -n `expr $a - 2` | head -n `expr $a - 3` >> $XML_FILE
wget $XML_URL5 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | tail -n `expr $a - 2` | head -n `expr $a - 3` >> $XML_FILE
wget $XML_URL6 -O /tmp/saraiva.xml
a=`wc -l /tmp/saraiva.xml | awk '{print $1}'`; cat /tmp/saraiva.xml | tail -n `expr $a - 2` >> $XML_FILE
#curl $XML_URL |  iconv -f iso8859-1 -t utf-8 > $XML_FILE

echo "exportando ranking de skus da producao ..."
mysqldump -u dynad -pdanyd -h $DB_HOST $SCHEMA --skip-comments ranking_by_day > $RANKING_FILE


RANKING=1
cmp -s $RANKING_FILE $RANKING_FILE.old >/dev/null
if [ $? -eq 0 ]
then
  RANKING=0
else
  echo "importando ranking de skus local ..."
  mysql -u dynad -pdanyd $SCHEMA < $RANKING_FILE
  echo "gerando ranking de skus ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/ranking.sql
fi




cmp -s $XML_FILE $XML_FILE.old >/dev/null 
if [ $? -ne 0 ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

#  echo "executando script de normalizacao ..."
#  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql
#
#  echo "fazendo export do banco ..."
#  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE
#
#  for ip in $EXP_HOST; do
#     echo "exportando para $ip"
#     mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
#  done

else

  if [ $RANKING -eq 1 ]
  then
#    echo "executando script de importacao ..."  
#    groovy $DIGESTER BEST_SELLERS 2 || exit 1
#
#    echo "fazendo export do banco (best_sellers) ..."
#    mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero best_sellers > $EXPORT_FILE
#
#    for ip in $EXP_HOST; do
#       echo "exportando para $ip"
#       mysql -u digester -pretsegid $SCHEMA -h $ip < $EXPORT_FILE
#    done
#
  else
    echo "*** nada a fazer ..."
  fi

fi





date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"


