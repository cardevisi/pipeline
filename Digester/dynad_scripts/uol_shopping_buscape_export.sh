if [ -z ${JAVA_HOME} ]; 
then 
export JAVA_HOME=/var/java
export PATH=$PATH:$JAVA_HOME/bin
else 
echo "using java_home: $JAVA_HOME"
fi

cd /root/dynad_scripts

if [ -e "/mnt/shoppinguol_buscape_products.txt" ]
then
echo "removind previous products file"
rm /mnt/shoppinguol_buscape_products.txt
fi

if [ -e "/mnt/shoppinguol_buscape_offers.txt" ]
then
echo "removind previous offers file"
rm /mnt/shoppinguol_buscape_offers.txt
fi

echo "build SNA file"
groovy -cp net/dynad/digester/buscape/digester-buscape-0.0.1.jar:net/dynad/digester/buscape/libs-0.0.1/* DigesterShoppingUOLBuscape.groovy BUILD_FILE

echo "publishing SNA file"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_products.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/flushdb?force_server=1"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_offers.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/?force_server=1"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_products.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/flushdb?force_server=2"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_offers.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/?force_server=2"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_products.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/flushdb?force_server=3"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_offers.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/?force_server=3"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_products.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/flushdb?force_server=4"
curl --connect-timeout 6000 -L -k -F file=@/mnt/shoppinguol_buscape_offers.txt -u teste:teste "http://200.147.166.106/updater/shoppinguol_buscape/?force_server=4"
