<?php
// Autoloader (carregador automático)
 // Habilita as classes do Zend Framework
 set_include_path(implode(PATH_SEPARATOR, array(__DIR__,get_include_path())));
 spl_autoload_register(function($classe){$classe = str_replace('_', '/', $classe);require_once("{$classe}.php");});
 require_once('Zend/Loader/Autoloader.php');
 Zend_Loader_Autoloader::getInstance();

 // Faz a conexão com o banco de dados
 $db = Zend_Db::factory('Pdo_Mysql', array(
  'host'     => 'localhost',
  'username' => 'root',
  'password' => 'phenom02',
  'dbname'   => 'dynad_vivo',
  'charset'  => 'utf8'
 ));
 $db->getConnection();
 Zend_Db_Table_Abstract::setDefaultAdapter($db);

 // Configura o fuso-horário
 date_default_timezone_set('America/Sao_Paulo');

setlocale(LC_ALL, 'pt_BR.UTF-8');

class Crawller
{

	private $_url = 'https://lojaonline.vivo.com.br/lojaonline/appmanager/env/web?regional=';
	private $_pagingUrl = 'http://lojaonline.vivo.com.br:80/lojaonline/appmanager/env/web?_nfpb=true&amp;_windowLabel=vitrine_VcAparelhos&amp;vitrine_VcAparelhos_actionOverride=%2Fbr%2Fcom%2Fvivo%2Fveco%2FvitrineDeAparelhos%2FmudaPagina&regional=';
	private $_states = array('SP','RJ','MG','AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','PA','PB','PR','PE','PI','RN','RS','RO','RR','SC','SE','TO');
	private $_timeStart;

    public function fetch(){
    	$this->_timeStart = time();
    	foreach($this->_states as $state){
    		echo "Start - load for region {$state} - Time: {$this->getTimeTillNow()}\n";
    		$response = $this->getFirstPage($state);
	    	
	    	$dom = new Zend_Dom_Query($response);
	    	$dom->setEncoding('UTF-8');
	    	
	    	$items = array();
	    	$items[] = $dom->query('.box_vitrine_aparelho');
    		
    		$nextPage = $dom->query('.paging a.proximo');
	    	
	    	echo "	Start - paging for region {$state} - Time: {$this->getTimeTillNow()}\n";
	    	if($nextPage->count()){
	    		$new_items = $this->getPagingElements($state, preg_replace('/[^0-9]/i', '', $nextPage->current()->getAttribute('onclick')));
	    		if($new_items != false){
	    			$items = array_merge($items,$new_items);
	    		}
	    	}
	    	echo "	Finished - paging for region {$state} - Time: {$this->getTimeTillNow()}\n";
	    	$data = array();
    		echo "	Start - Prepare Data for region {$state} - Time: {$this->getTimeTillNow()}\n";
	    	foreach($items as $item){
				foreach($item as $product){
					try{																																				
						$dom2 = new Zend_Dom_Query($item->getDocument()->saveHTML($product));
						$img = $dom2->query('img.img');
						if($img->count()){
							$img = 'https://lojaonline.vivo.com.br'.$img->current()->getAttribute('src');
						}else{
							$img = '';
						}

						preg_match('/(([0-9]{1,2})x)/i', $this->getCurentDomSelect($dom2,'.box_valores_pos'), $parcela_pos);
						if($parcela_pos){
							$parcela_pos = $parcela_pos[0];
						}else{
							$parcela_pos = '';
						}

		   				preg_match('/(([0-9]{1,2})x)/i', $this->getCurentDomSelect($dom2,'.box_valores_controle'), $parcela_controle);
		   				if($parcela_controle){
							$parcela_controle = $parcela_controle[0];
						}else{
							$parcela_controle = '';
						}

						preg_match('/(([0-9]{1,2})x)/i', $this->getCurentDomSelect($dom2,'.box_valores_pre'), $parcela_pre);
						if($parcela_pre){
							$parcela_pre = $parcela_pre[0];
						}else{
							$parcela_pre = '';
						}

						$sku = str_replace('box_', '', $product->getAttribute('id'));

						
						
						$data[] = array(
							'sku'=>$state.'_'.$sku,
							'img'=>$img,
							'brand'=>$this->adjustString($this->getCurentDomSelect($dom2,'.nome_marca_vitrine')),
							'name'=>$this->adjustString($this->getCurentDomSelect($dom2,'.nome_aparelho_vitrine')),
							'pos_parcelas'=>$parcela_pos,
							'pos_valor_parcelas'=>$this->adjustPrice($this->getCurentDomSelect($dom2,'.box_valores_pos .box_valores span.vitrine_valor')),
							'pos_a_vista'=>$this->adjustPrice($this->getNextDomSelect($dom2,'.box_valores_pos .box_valores span')),
							'pos_plano'=>$this->adjustString($this->getCurentDomSelect($dom2,'.box_valores_pos .promo_vitrine')),
							'controle_parcelas'=>$parcela_controle,
							'controle_valor_parcelas'=>$this->adjustPrice($this->getCurentDomSelect($dom2,'.box_valores_controle .box_valores span.vitrine_valor')),
							'controle_a_vista'=>$this->adjustPrice($this->getNextDomSelect($dom2,'.box_valores_controle .box_valores span')),
							'controle_plano'=>$this->adjustString($this->getCurentDomSelect($dom2,'.box_valores_controle .promo_vitrine')),
							'pre_parcelas'=>$parcela_pre,
							'pre_valor_parcelas'=>$this->adjustPrice($this->getCurentDomSelect($dom2,'.box_valores_pre .box_valores span.vitrine_valor')),
							'pre_a_vista'=>$this->adjustPrice($this->getNextDomSelect($dom2,'.box_valores_pre .box_valores span')),
							'pre_plano'=>$this->adjustString($this->getCurentDomSelect($dom2, '.box_valores_pre .promo_vitrine')),
							'url'=>'https://lojaonline.vivo.com.br/lojaonline/appmanager/env/web?_nfls=false&_nfpb=true&_pageLabel=vecoVcDetalhesDeAparelhosPage&codSap='.$sku.'&_nfls=false#',
							'region'=>$state,
							'stock'=> 1,
							'quarantine'=>0,
							'last_updated'=> time()
						);
					}catch(Exception $e){
						Zend_Debug::dump($e->getMessage(), 'Error ----');
					}
				}
	    	}
	    	echo "	Finished - Prepare Data for region {$state} - Time: {$this->getTimeTillNow()}\n";
	    	echo "	Start - Save Data for region {$state} - Time: {$this->getTimeTillNow()}\n";
	    	$db = new DbTable_Products();
			foreach($data as $insert){
				if($db->find($insert['sku'])->count()){
					$db->update($insert, $db->getAdapter()->quoteInto('sku = ?', $insert['sku']));
				}else{
					$db->insert($insert);
				}

			}
			echo "	Finished - Prepare Data for region {$state} - Time: {$this->getTimeTillNow()}\n";
			echo "Finished - load for region {$state} - Time: {$this->getTimeTillNow()}\n";
			echo "------------------------------------------------------------------------------\n";
    	}
    	echo "Start - Prepare quarentine - Time: {$this->getTimeTillNow()}\n";
    	$this->quarantine($data);
    	echo "Finished -  Prepare quarentine - Time: {$this->getTimeTillNow()}\n";
    }

    protected function getTimeTillNow(){
    	return gmdate('H:i:s',time()-$this->_timeStart);
    }

    protected function quarantine($fullDataSkus){
    	$skus = array();
    	foreach($fullDataSkus as $sku){
    		$skus[] = $sku['sku'];
    	}
    	$db = new DbTable_Products();
    	$data = $db->fetchAll($db->select()->where('sku NOT in (?)', $skus));

		foreach($data as $item){

			if(abs((time()-(int)$item->last_updated)/60/60/24) >= 2){
				$quarantine = 2;
				$stock = 0;
			}else{
				$quarantine = 1;
				$stock = 1;
			}
			$db->update(array('stock'=>$stock,'quarantine'=>$quarantine, 'last_updated'=>time()), $db->getAdapter()->quoteInto('sku = ?', $item['sku']));
		}
    }

    protected function getFirstPage($region){
    	$client = new Zend_Http_Client();
		$client->setUri($this->_url.$region);
		$client->setConfig(array('timeout' => 60000));
		$client->setMethod(Zend_Http_Client::GET);
    	return $client->request()->getBody();
    }

    protected function getCurentDomSelect($dom, $selector){
    	if($dom == null || $selector == null) return '';
    	if($dom->query($selector)->count()){
			return $dom->query($selector)->current()->textContent;
		}
		return '';
    }
	protected function getNextDomSelect($dom, $selector){
    	if($dom == null || $selector == null) return '';
    	if($dom->query($selector)->count()){
			return $dom->query($selector)->next()->textContent;
		}
		return '';
    }

    protected function adjustPrice($price){
    	return (float)str_replace(array('.','R$ ', ','),array('','', '.'),$price);
    }

    protected function adjustString($string){
    	return (iconv('UTF-8', 'ASCII//TRANSLIT', utf8_decode(trim($string))));
    }

    protected function  getPagingElements($region = null, $page = null){
		$items = array();
		try{
	    	if($region == null || $page == null) return false;

	    	$client = new Zend_Http_Client();
			$client->setUri($this->_pagingUrl.$region);
			$client->setConfig(array('timeout' => 600000));
			$client->setMethod(Zend_Http_Client::POST);
			$client->setCookie('regional', $region);
			$client->setParameterPost('vitrine_VcAparelhos{actionForm.pagSelecionada}', $page);
			$client->setParameterPost('vitrine_VcAparelhos{actionForm.comparar}', '');

			$response = $client->request()->getBody();

			$dom = new Zend_Dom_Query($response);
	    	$items[] = $dom->query('.box_vitrine_aparelho');

	    	$nextPage = $dom->query('.paging a.proximo');

	    	if($nextPage->count()){
	    		$new_items = $this->getPagingElements($region, preg_replace('/[^0-9]/i', '', $nextPage->current()->getAttribute('onclick')));
	    		if($new_items != false){
	    			$items = array_merge($items,$new_items);
	    		}
	    	}
			return $items;
		}catch(Exception $e){
			return $items;
		}

    }


}
