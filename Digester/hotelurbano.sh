echo "INICIO - Fastshop"

date
START=$(date +%s)

### PARAMETROS ###
XML_URL1=http://s3.amazonaws.com/xml-parceiros-hu/prod/criteo/hoteis.xml
XML_URL2=http://s3.amazonaws.com/xml-parceiros-hu/prod/criteo/ofertas.xml
XML_FILE1=c:/mnt/TMP/xml/hu-hoteis.xml
XML_FILE2=c:/mnt/TMP/xml/hu-ofertas.xml
TEMP_NAME=hotelurbano
SCHEMA=dynad_hotelurbano
DIGESTER=/c/CASIFILES/WORKS/stg/dynad-digester/dynad_scripts/DigesterHotelUrbano.groovy

SNFILE=/mnt/TMP/sna-files/hotelurbano.txt
SNDATASOURCE=hotelurbano
##################


RANKING_FILE='c:/mnt/TMP/sqls/exp_'$TEMP_NAME'_ranking.sql'
EXPORT_FILE='c:/mnt/TMP/sqls/exp_'$TEMP_NAME'_db.sql'

cd /root/dynad_scripts

rm $XML_FILE.old
rm $RANKING_FILE.old
mv $RANKING_FILE $RANKING_FILE.old

if [ -e "c:/mnt/TMP/sna-files/hotelurbano_uolcliques.txt" ]; then
    rm /mnt/TMP/sna-files/hotelurbano_uolcliques.txt
    echo "removed /mnt/TMP/sna-files/hotelurbano_uolcliques.txt"
fi

if [ -e "c:/mnt/TMP/sna-files/hotelurbano_uolcliques.zip" ]; then
    rm /mnt/TMP/sna-files/hotelurbano_uolcliques.zip
    echo "removed /mnt/TMP/sna-files/hotelurbano_uolcliques.zip"
fi

if [ -e "c:/mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.zip" ]; then
    rm /mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.zip
    echo "removed /mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.zip"
fi

if [ -e "c:/mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.txt" ]; then
    rm /mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.txt
    echo "removed /mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.txt"
fi


echo "baixando xml ..."
wget $XML_URL1 -O $XML_FILE1
wget $XML_URL2 -O $XML_FILE2

cmp -s $XML_FILE1 $XML_FILE1.old >/dev/null
if [ $? -ne 0 ]
then

  echo "executando script de importacao ..."
  groovy $DIGESTER 2 || exit 1

  echo "executando script de normalizacao ..."
  mysql -u dynad -pdanyd $SCHEMA < /root/dynad_scripts/normaliza.sql

  echo "fazendo export do banco ..."
  mysqldump -u dynad -pdanyd $SCHEMA top_sellers_categoria top_sellers_genero codigos_categorias codigos_skus catalogo best_sellers > $EXPORT_FILE

    sh /root/dynad_scripts/sharding/upload.sh $SNFILE true 13 1296000 hotelurbano true || exit 1

        if [ -s "c:/mnt/TMP/sna-files/hotelurbano_uolcliques.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip hotelurbano_uolcliques.zip hotelurbano_uolcliques.txt

        echo "starting sync process"
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F flush=true -F ds=hotelurbano -F file=@hotelurbano_uolcliques.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F flush=true -F ds=hotelurbano -F file=@hotelurbano_uolcliques.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi

   if [ -s "c:/mnt/TMP/sna-files/hotelurbano_uolcliques_topsellers.txt" ]; then
        cd /mnt/TMP/sna-files
        echo "compressing top seller file"
        zip hotelurbano_uolcliques_topsellers.zip hotelurbano_uolcliques_topsellers.txt

        echo "starting sync process"
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=hotelurbano -F file=@hotelurbano_uolcliques_topsellers.zip http://200.147.166.29/UploadDataServlet
        curl -H "Host: uolcliques.dynad.net" -v -u teste:teste -F ds=hotelurbano -F file=@hotelurbano_uolcliques_topsellers.zip http://200.147.166.28/UploadDataServlet

        cd /root/dynad_scripts
   else
        echo "no data to work with"
   fi

fi

date
END=$(date +%s)
DIFF=$(( $END - $START ))

echo "operacao finalizada em $DIFF segundos - $SCHEMA"

echo "FIM***"