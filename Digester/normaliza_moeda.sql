DROP FUNCTION IF EXISTS `IsNumeric`;
CREATE FUNCTION `IsNumeric`(val varchar(255)) RETURNS tinyint(4)
RETURN val REGEXP '^(-|\\+){0,1}([0-9]+\\.[0-9]*|[0-9]*\\.[0-9]+|[0-9]+)$';

DROP FUNCTION IF EXISTS `regex_replace`;

DELIMITER $$

CREATE FUNCTION  `regex_replace`(pattern VARCHAR(1000), replacement VARCHAR(1000), original VARCHAR(1000))
 
RETURNS VARCHAR(1000)
DETERMINISTIC
BEGIN
 DECLARE temp VARCHAR(1000);
 DECLARE ch VARCHAR(1);
 DECLARE i INT;
 DECLARE j INT;
 DECLARE qbTemp VARCHAR(1000);
 
 SET i = 1;
 SET j = 1;
 SET temp = '';
 SET qbTemp = '';
  
 IF original REGEXP pattern THEN
  loop_label: LOOP
   IF i>CHAR_LENGTH(original) THEN
    LEAVE loop_label; 
   END IF;
   SET ch = SUBSTRING(original,i,1);
   IF NOT ch REGEXP pattern THEN
    SET temp = CONCAT(temp,ch);
   ELSE
    SET temp = CONCAT(temp,replacement);
   END IF;
   SET i=i+1;
  END LOOP;
 ELSE
  SET temp = original;
 END IF;
 SET temp = TRIM(BOTH replacement FROM temp);
 SET temp = REPLACE(REPLACE(REPLACE(temp , CONCAT(replacement,replacement),CONCAT(replacement,'#')),CONCAT('#',replacement),''),'#','');
 RETURN temp;
END

$$

DELIMITER ;


DROP FUNCTION IF EXISTS `NumericOnly`;

DELIMITER $
CREATE FUNCTION `NumericOnly`(val VARCHAR(255)) RETURNS varchar(255) CHARSET latin1
BEGIN
 return regex_replace('[^0-9]', '', val);
END 
$

DELIMITER ;


DROP FUNCTION IF EXISTS `autoNormalizaMoeda`;


DELIMITER ||

CREATE FUNCTION `autoNormalizaMoeda`(s varchar(16)) RETURNS decimal(30,2)
BEGIN

DECLARE saida decimal(30,2);
DECLARE tam INT(2);
DECLARE t1 INT(2);

set t1 = 0;

if length(substring_index(s, ',', -1)) = 2 or length(substring_index(s, '.', -1)) = 2 then set t1 = 2; end if;

set s = NumericOnly(s);

select length(s) into tam from dual;

if t1 = 2 then select concat(substring(s, 1, length(s)-2), '.', substring(s, length(s)-1)) into s from dual; end if;

if tam <= 2 then return cast(s as decimal(30,0)); end if;

if t1 = 2 then return CAST(s AS DECIMAL(30, 2)); else return CAST(s AS DECIMAL(30, 0));  end if;
END 

||

DELIMITER ;

