(function() {

    var tl = new TimelineLite();
    
    tl.from('.step-1', 1, { left: 700});
    tl.from('.step-2', 2, { left: 700}, '-=.9');
    
    var animation = document.querySelector("#animation");
    animation.style.display = 'block';
})();
