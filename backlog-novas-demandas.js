Workflow Dynad:

1) HTML, JS, CSS
2) Programação Ad Server
3) Programação DCO - Businesss Rule - Java
4) Pixels - Integração com catálogo XML (Javascript)
5) Suporte de Ad Server l-dynad@uolinc.com


Backlog para os Templates:

1) Replicar a funcionalidade do "Passe o Mouse" e "Clique aqui" para todos os templates. Além disso possibilitar que no step do passe o mouse seja possível também abrir a peça por clique.
2) Automação do Grunt (Opção para versionar o js).
3) Finalizar o Sidekick (Migrar a última versão do svn para o git).
3.1) Melhorar a animação e desabilitar o clique depois.
3.1) Correção do bug -> Se o banner estiver expandido desabilitar qualquer evento da primeira peça.
4) Viewport -> habilitar o scroll no caso do resize da janela para uma dimensão menor que a mínima.
5) Finalizar o Player do YouTube (Refactoring).
5.1) Pause e resume.
6) Criar uma tag script do lado do creativo do cliente reponsável pela comunicação do template com o peça.
7) FilmStrip -> Esta demanda foi transferida para a equipe de inserção.
8) Modificar os templates e inserir Opção de reposicionamento do botão fechar.
9) Pensar em uma solução para o indicador de carregamento que funcione mais precisamente.
10) Migrar as modificações da última versão do Viewport para o repositório git.
11) Deploy dos arquivos js dos templates em produção.
12) Implementar o template do cutting do nosso lado.
13) Eliminação do maven.