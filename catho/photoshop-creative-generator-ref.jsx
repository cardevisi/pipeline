app.preferences.rulerUnits = Units.PIXELS;

var activateButtonTranslate = false, offSetY, offSetX, buttonLayer;
var offSetToken = false;

var textList = {
    '120x600': {
        callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto\rseguro',
        },
        position : {
            offSetY : 16,
            offSetX : 0
        }
    },
    '160x600': {
        callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro',
        },
        position : {
            offSetY : 20,
            offSetX : 0
        }
    },
    '200x200': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundacao\rbradesco',
            'porto-seguro' : 'porto seguro',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '240x400': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundacao\rbradesco',
            'porto-seguro' : 'porto seguro',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé'
        },
        position : {
            offSetY : 22,
            offSetX : 0
        }
    },
    '250x250': {
        // callAction : 'Por menos de\rum brigadeiro\rpor dia, encontre\rvagas na Ambev.',
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '300x250': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto\rseguro',
        },
        position : {
            offSetY : 6,
            offSetX : 0
        }
    },
    '300x600': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto\rseguro',
        },
        position : {
            offSetY : 29,
            offSetX : 0
        }
    },
    '300x1050': {
        callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil\rkirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto\rseguro',
        },
        position : {
            offSetY : 39,
            offSetX : 0
        }
    },
    '320x50': {
        callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '320x100': {
        callAction : 'A Catho tem gente de\rverdade trabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '336x280': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '468x60': {
        callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro'
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '600x300': {
        callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação\rbradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto\rseguro',
        },
        position : {
            offSetY : 28,
            offSetX : 0
        }
    },
    '720x300': {
        callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro',
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '728x90': {
        callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro',
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '970x90': {
        callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro',
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    },
    '970x250': {
        callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
        textLogos : {
            'fundacao-bradesco' : 'fundação bradesco',
            'CEC' : 'c&c',
            'brasilkirin' : 'brasil kirin',
            'bioritmo': 'bio ritmo',
            'nestle': 'nestlé',
            'porto-seguro' : 'porto seguro',
        },
        position : {
            offSetY : 0,
            offSetX : 0
        }
    }
}

function getCanvasSize() {
    var docWIDTH = app.activeDocument.width;
    var docHEIGHT = app.activeDocument.height;
    docWIDTH = docWIDTH.toString().split(' ');
    docHEIGHT = docHEIGHT.toString().split(' ');
    return  docWIDTH[0] + "x" + docHEIGHT[0];
}

function moveLayer(layer, x, y, minus) {
    if (minus) y = -Math.abs(y);
    offSetToken = true;
    return layer.translate(x, y);
}

function positionButton(format, brandname) {

    offSetX = textList[format].position.offSetX;
    offSetY = textList[format].position.offSetY;

    switch(brandname) {
        case 'fundacao-bradesco':
            activateButtonTranslate = true;
            if (activateButtonTranslate) moveLayer(buttonLayer, offSetX, offSetY, false);
        break;
        case 'porto-seguro':
            activateButtonTranslate = true;
            if (activateButtonTranslate) moveLayer(buttonLayer, offSetX, offSetY, false);
        break;
    }
}

function replaceText(brandname) {
  var newText, textLayer = app.activeDocument.layerSets.getByName('textGroup');
  for (var i = 0; i < textLayer.layers.length; i++) {
    if (textLayer.layers[i].name === 'text') 
        var format = getCanvasSize();
        switch(brandname) {
            case 'applebees':
            case 'carrefour':
            case 'senai':
                newText = textList[format].callAction.replace('na Ambev', 'no '+brandname);
            break;
            default:
                var name = (textList[format].textLogos[brandname]) ? (textList[format].textLogos[brandname]) : brandname;
                newText = textList[format].callAction.replace('Ambev', name);
            break;
        }
        textLayer.layers[i].textItem.contents = newText;
        positionButton(format, brandname);
    }
  }
  return true;
}

function createFolder(path) {
    var folder = new Folder(path);
    if (!folder.exists) {
        folder.create();
    }
    return true;
}

function hideLayer(group) {
    for (var i = 0; i < group.length; i++) {
        if (group[i].visible === true) {
            group[i].visible = false;
        }
    }
    return true;
}

function saveFile(path, name, quality) {
    var jpgOptions = new JPEGSaveOptions();
    jpgOptions.embedColorProfile = true;
    jpgOptions.quality = quality;
    var newName = path+"/"+name.toLowerCase()+".jpg";
    activeDocument.saveAs(new File(newName), jpgOptions, true, Extension.LOWERCASE);
}

function openFilesByExtension(path, extension) {
    var inputFolder = new Folder(path);
    var inputFiles = inputFolder.getFiles(extension)
    for (var i = 0; i < inputFiles.length; i++) {
        var psd = new File(inputFiles[i]);
        app.open(psd);
        generate({
            quality: 12
        });
    }
}

function generate(options) {
    logosGroup = app.activeDocument.layerSets.getByName('logos');
    buttonLayer = app.activeDocument.layerSets.getByName('buttonGroup');
    logosInGroup = logosGroup.layers;

    var outputFolder = "~/Desktop/output/"+getCanvasSize();
    if (!createFolder(outputFolder)) {
        alert('Output folder not created');
        return;
    }

    for (var i = 0; i < logosInGroup.length; i++) {
        if (hideLayer(logosInGroup)) {
            logosInGroup[i].visible = true;
        }
        if (replaceText(logosInGroup[i].name)) {
            saveFile(outputFolder, logosInGroup[i].name, options.quality);
            if (offSetToken && activateButtonTranslate) {
                moveLayer(buttonLayer, offSetX, offSetY, true);
                offSetToken = false;
            }
        }
    }

    if (app.documents.length == 1) app.documents[0].close(SaveOptions.DONOTSAVECHANGES);

}

function init(options) {
    openFilesByExtension('~/Downloads/', '*.psd');
}

init();