var text = 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.';
var layerGroup = app.activeDocument.layerSets.getByName('logos');
var layerInGroup = layerGroup.layers;
//alert(app.path);

function getCanvasSize() {
    var docWIDTH = app.activeDocument.width;
    var docHEIGHT = app.activeDocument.height;
    docWIDTH = docWIDTH.toString().split(' ');
    docHEIGHT = docHEIGHT.toString().split(' ');
    return  docWIDTH[0] + "x" + docHEIGHT[0];
}

function formatNames(name) {
  switch(name) {
    case 'fundacao-bradesco':
        name = "fundação\rbradesco";
    break;
  }
  return name;
}

function replaceText(brandname) {
  var textLayer = app.activeDocument.layerSets.getByName('group');
  for (var i = 0; i < textLayer.layers.length; i++) {
    if (textLayer.layers[i].name === 'text') {
        var newText = text.replace('Ambev', formatNames(brandname));
        textLayer.layers[i].textItem.contents = newText;
    }
  }
  return true;
}

function createFolder(path) {
    var folder = new Folder(path);
    if (!folder.exists) {
        folder.create();
    }
    return true;
}

function hideLayer() {
    for (var i = 0; i < layerInGroup.length; i++) {
        if (layerInGroup[i].visible === true) {
            layerInGroup[i].visible = false;
        }
    }
    return true;
}

function saveFile(path, name, quality) {
    var jpgOptions = new JPEGSaveOptions();
    jpgOptions.embedColorProfile = true;
    jpgOptions.quality = quality;
    var newName = path+"/"+name.toLowerCase()+".jpg";
    activeDocument.saveAs(new File(newName), jpgOptions, true, Extension.LOWERCASE);
}

function init(options) {
    var outputFolder = "~/Desktop/output/"+getCanvasSize();
    if (!createFolder(outputFolder)) {
        alert('Output folder not created');
        return;
    }
    
    for (var i = 0; i < layerInGroup.length; i++) {
        if (hideLayer(layerInGroup)) {
            layerInGroup[i].visible = true;
        }
        if (replaceText(layerInGroup[i].name)) {
            saveFile(outputFolder, layerInGroup[i].name, options.quality);
        }
    }
}

init({
    quality: 12
});