(function(){
'use-sctrict'
function Config() {
    return {
        setup: {
            sourceFiles: '~/Downloads/',
            outputFiles: '~/Desktop/output/',
            quality: 12,
            spriteSheet: false
        },
        '120x600': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 16,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '160x600': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 20,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '200x200': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundacao\rbradesco',
                'porto-seguro' : 'porto seguro',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '240x400': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundacao\rbradesco',
                'porto-seguro' : 'porto seguro',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé'
            },
            position : {
                offSetY : 22,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '250x250': {
            // callAction : 'Por menos de\rum brigadeiro\rpor dia, encontre\rvagas na Ambev.',
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x250': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 6,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x600': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 29,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x1050': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil\rkirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 39,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '320x50': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '320x100': {
            callAction : 'A Catho tem gente de\rverdade trabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '336x280': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '468x60': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '600x300': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 28,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '720x300': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '728x90': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '970x90': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '970x250': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        }
    };
}

function AppPreferences(){}
	
AppPreferences.prototype.applyPreferences = function() {
	app.preferences.rulerUnits = Units.PIXELS;
	app.preferences.typeUnits = TypeUnits.PIXELS;
	app.displayDialogs = DialogModes.NO;
};

AppPreferences.prototype.createDocument = function(width, height) {
	var docWidth = width;
	var docHeight = height;
	var resolution = 72;
	var docName = width+'x'+height + '-' + 'sprite-sheet';
	
	if (app.documents.length === 0) {
		app.documents.add(docWidth, docHeight, resolution, docName, NewDocumentMode.RGB, DocumentFill.TRANSPARENT);
	}
};

AppPreferences.prototype.closeDocDoNotSave = function() {
	if (app.activeDocument) {
        app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
    }
};

AppPreferences.prototype.closeAll = function() {
	if (app.documents.length == 1) {
		for (var i = 0; i < app.documents.length; i++) {
			app.documents[i].close(SaveOptions.DONOTSAVECHANGES);
		}
    }
};
function Files() {}

Files.prototype.save = function(path, name, quality) {
    var options = new JPEGSaveOptions();
    options.embedColorProfile = true;
    options.quality = quality;
    var newName = path+"/"+name.toLowerCase()+".jpg";
    app.activeDocument.saveAs(new File(newName), options, true, Extension.LOWERCASE);
};

Files.prototype.savePNG = function(path, name, quality) {
    var options = new ExportOptionsSaveForWeb();
    options.format = SaveDocumentType.PNG;
    var newName = path+"/"+name.toLowerCase()+".png";
    app.activeDocument.exportDocument(new File(newName), ExportType.SAVEFORWEB, options);
};

Files.prototype.openByExtension = function(path, extension, callback) {
    var inputFolder = new Folder(path);
    var inputFiles = inputFolder.getFiles(extension);
    for (var i = 0; i < inputFiles.length; i++) {
        var psd = new File(inputFiles[i]);
        var file = app.open(psd);
        callback(file);
    }
};

Files.prototype.createFolder = function(path) {
    var folder = new Folder(path);
    if (!folder.exists) {
        folder.create();
    }
    return true;
};
function SpriteSheetCreator() {
    this.config = Config();
    this.files = new Files();
    this.appPreferences = new AppPreferences();
}

SpriteSheetCreator.prototype.makeGrid = function(rows, collumns, width, height, space) {
    var positions = [];
    var index = 0;
    for (var x = 0; x < rows; x++) {
        for (var y = 0; y < collumns; y++) {
            var posX = y*(width+space);
            var posY = x*(height+space);
            positions[index] = {
                x: posX,
                y: posY,
                w: width, 
                h: height
            };
            index++;
        }
    }
    return positions;
};

SpriteSheetCreator.prototype.positionLayers = function(width, height) {
    var that = this;
    var layerNum = app.activeDocument.layers.length;
    var raiz = Math.ceil(Math.sqrt(layerNum));
    var columns = raiz;
    var rows = (raiz < 5) ? 1 : raiz;
    var space = 10;
    var docHeight = rows * (height + space);
    var docWidth = columns * (width + space);
    var positions = that.makeGrid(rows,columns, width, height, space);

    for (var j = 0; j < layerNum; j++) {
        try {
            var posX = positions[j].x;
            var posY = positions[j].y;
            var layerA = app.activeDocument.layers[j];
            layerA.name = 'layer_'+j;
            layerA.translate(posX, posY);
        } catch(e) {
            
        }
    }
    app.activeDocument.resizeCanvas(docWidth, docHeight, AnchorPosition.TOPLEFT);
};

SpriteSheetCreator.prototype.openFileByExtension = function(path, extension, callback) {
    var inputFolder = new Folder(path);
    var inputFiles = inputFolder.getFiles(extension);
    for (var i = 0; i < inputFiles.length; i++) {
        var file = new File(inputFiles[i]);
        var currFile = app.open(file);
        callback(currFile);
    }
};

SpriteSheetCreator.prototype.makeSpriteSheet = function(file) {
    var that = this;
    var width = Number(file.width.toString().split(' ')[0]);
    var height = Number(file.height.toString().split(' ')[0]);
    var fileName = width+'x'+height;

    that.appPreferences.closeDocDoNotSave();
    that.appPreferences.createDocument(width, height);
    that.openFileByExtension(that.config.setup.outputFiles+fileName+'/', '*.jpg', function(file) {
        file.selection.selectAll();
        file.selection.copy();
        file.close(SaveOptions.DONOTSAVECHANGES);
        app.activeDocument.paste();
    });

    that.positionLayers(width, height);
    that.files.savePNG(that.config.setup.outputFiles, fileName);
    that.appPreferences.closeDocDoNotSave();
};

function CreativeGenerator() {
    this.config = Config();
    this.appPreferences = new AppPreferences();
    this.spriteSheetCreator = new SpriteSheetCreator();
    this.files = new Files();
    this.activateButtonTranslate = false;
    this.offSetToken = false;
}

CreativeGenerator.prototype.getCanvasSize = function() {
    var docWIDTH = app.activeDocument.width;
    var docHEIGHT = app.activeDocument.height;
    docWIDTH = docWIDTH.toString().split(' ');
    docHEIGHT = docHEIGHT.toString().split(' ');
    return  docWIDTH[0] + "x" + docHEIGHT[0];
};

CreativeGenerator.prototype.moveLayer = function(layer, x, y, minus) {
    var that = this;
    if (minus) y = -Math.abs(y);
    that.offSetToken = true;
    return layer.translate(x, y);
};

CreativeGenerator.prototype.positionButton = function(format, brandname) {
    var that = this;
    that.offSetX = that.config[format].position.offSetX;
    that.offSetY = that.config[format].position.offSetY;
    
    if(that.config[format].buttonTranslate[brandname] === true) {
        that.moveLayer(that.buttonLayer, that.offSetX, that.offSetY, false);
    }
};

CreativeGenerator.prototype.replaceText = function(brandname) {
    var that = this;
    var newText;
    var textLayer = app.activeDocument.layerSets.getByName('textGroup');
    
    for (var i = 0; i < textLayer.layers.length; i++) {
        if (textLayer.layers[i].name === 'text') {
            var format = that.getCanvasSize();
            switch(brandname) {
                case 'applebees':
                case 'carrefour':
                case 'senai':
                    newText = that.config[format].callAction.replace('na Ambev', 'no '+brandname);
                break;
                default:
                    var name = (that.config[format].textLogos[brandname]) ? (that.config[format].textLogos[brandname]) : brandname;
                    newText = that.config[format].callAction.replace('Ambev', name);
                break;
            }
            textLayer.layers[i].textItem.contents = newText;
            that.positionButton(format, brandname);
        }
    }
    return true;
};

CreativeGenerator.prototype.hideLayer = function(layers) {
    if (!layers || !(layers instanceof Object) || layers.length === 0) {
        return false;
    }

    for (var i = 0; i < layers.length; i++) {
        if (layers[i].visible === true) {
            layers[i].visible = false;
        }
    }
    
    return true;
};

CreativeGenerator.prototype.start = function(file) {
    var that = this, logosGroup, outputFolder, logosInGroup;
    
    try {
        logosGroup = app.activeDocument.layerSets.getByName('logos');
    } catch(error) {
        alert('Layer "logos" is not configured');
        return;
    }
    
    try {
        that.buttonLayer = app.activeDocument.layerSets.getByName('buttonGroup');
    } catch(error){
        alert('Layer "buttonGroup" is not configured');
        return;
    }
    
    outputFolder = that.config.setup.outputFiles+that.getCanvasSize();
    if (!that.files.createFolder(outputFolder)) {
        alert('Output folder not created');
        return;
    }

    logosInGroup = logosGroup.layers;
    for (var i = 0; i < logosInGroup.length; i++) {
        if (that.hideLayer(logosInGroup)) {
            logosInGroup[i].visible = true;
        }
        if (that.replaceText(logosInGroup[i].name)) {
            that.files.save(outputFolder, logosInGroup[i].name, that.config.setup.quality);
        }
    }
    
    if(!that.config.setup.spriteSheet) {
        that.appPreferences.closeDocDoNotSave();
    } else {
        that.spriteSheetCreator.makeSpriteSheet(file);
    }
};

CreativeGenerator.prototype.init = function() {
    var that = this;
    that.files.openByExtension(that.config.setup.sourceFiles, '*.psd', function(file){
        that.start(file);
    });
};
function Main() {
	this.creativeGenerator = new CreativeGenerator();
    this.app = new AppPreferences();
}

Main.prototype.init = function() {
	this.app.applyPreferences();
	this.app.closeAll();
	this.creativeGenerator.init();
};
var main = new Main();
main.init();
})()