const gulp = require('gulp');
const concat = require('gulp-concat-util');
const jshint = require('gulp-jshint');
const server = require('karma').Server;

gulp.task('concat', ['lint'], function(){
    return gulp.src([
        'src/js/Config.js',
        'src/js/AppPreferences.js',
        'src/js/Files.js',
        'src/js/SpriteSheetCreator.js',
        'src/js/CreativeGenerator.js',
        'src/js/Main.js',
        'src/js/Init.js'
        ])
    .pipe(concat('photoshop-creative-generator.jsx'))
    .pipe(concat.header('(function(){\n\'use-sctrict\'\n'))
    .pipe(concat.footer('\n})()'))
    .pipe(gulp.dest('dist'));
});

gulp.task('lint', function(){
    return gulp.src('src/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('test', function (done) {
    new server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: false
    }, done).start();
});

gulp.task('watch', function(){
    gulp.watch('src/js/**/*.js', ['concat']);
});

gulp.task('dev', ['watch', 'concat', 'test']);
gulp.task('default', ['concat', 'test']);