module.exports = function(config) {
    config.set({
        basePath: './',
        browsers: [
            'PhantomJS'
        ],
        frameworks: [
          'jasmine'
        ],
        exclude: [
          'src/js/Init.js'
        ],
        files: [
          'src/js/*.js',
          'test/**/*.spec.js'
        ],
        reporters: ['progress', 'kjhtml'],
        port: 8000,
        autoWatch: true,
        background: true
    });
}