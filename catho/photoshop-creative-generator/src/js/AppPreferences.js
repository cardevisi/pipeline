function AppPreferences(){}
	
AppPreferences.prototype.applyPreferences = function() {
	app.preferences.rulerUnits = Units.PIXELS;
	app.preferences.typeUnits = TypeUnits.PIXELS;
	app.displayDialogs = DialogModes.NO;
};

AppPreferences.prototype.createDocument = function(width, height) {
	var docWidth = width;
	var docHeight = height;
	var resolution = 72;
	var docName = width+'x'+height + '-' + 'sprite-sheet';
	
	if (app.documents.length === 0) {
		app.documents.add(docWidth, docHeight, resolution, docName, NewDocumentMode.RGB, DocumentFill.TRANSPARENT);
	}
};

AppPreferences.prototype.closeDocDoNotSave = function() {
	if (app.activeDocument) {
        app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);
    }
};

AppPreferences.prototype.closeAll = function() {
	if (app.documents.length == 1) {
		for (var i = 0; i < app.documents.length; i++) {
			app.documents[i].close(SaveOptions.DONOTSAVECHANGES);
		}
    }
};