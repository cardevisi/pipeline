function Config() {
    return {
        setup: {
            sourceFiles: '~/Downloads/',
            outputFiles: '~/Desktop/output/',
            quality: 12,
            spriteSheet: false
        },
        '120x600': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 16,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '160x600': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga na\rAmbev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 20,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '200x200': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundacao\rbradesco',
                'porto-seguro' : 'porto seguro',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '240x400': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundacao\rbradesco',
                'porto-seguro' : 'porto seguro',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé'
            },
            position : {
                offSetY : 22,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '250x250': {
            // callAction : 'Por menos de\rum brigadeiro\rpor dia, encontre\rvagas na Ambev.',
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x250': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 6,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x600': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 29,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '300x1050': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil\rkirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 39,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '320x50': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '320x100': {
            callAction : 'A Catho tem gente de\rverdade trabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '336x280': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando\rpara você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '468x60': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro'
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '600x300': {
            callAction : 'A Catho tem\rgente de verdade\rtrabalhando para\rvocê conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação\rbradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto\rseguro',
            },
            position : {
                offSetY : 28,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '720x300': {
            callAction : 'A Catho\rtem gente\rde verdade\rtrabalhando\rpara você\rconseguir\rsua vaga\rna Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '728x90': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você\rconseguir sua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '970x90': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        },
        '970x250': {
            callAction : 'A Catho tem gente de verdade\rtrabalhando para você conseguir\rsua vaga na Ambev.',
            textLogos : {
                'fundacao-bradesco' : 'fundação bradesco',
                'CEC' : 'c&c',
                'brasilkirin' : 'brasil kirin',
                'bioritmo': 'bio ritmo',
                'nestle': 'nestlé',
                'porto-seguro' : 'porto seguro',
            },
            position : {
                offSetY : 0,
                offSetX : 0
            },
            buttonTranslate: {
                'fundacao-bradesco': true,
                'porto-seguro': true
            }
        }
    };
}
