function CreativeGenerator() {
    this.config = Config();
    this.appPreferences = new AppPreferences();
    this.spriteSheetCreator = new SpriteSheetCreator();
    this.files = new Files();
    this.activateButtonTranslate = false;
    this.offSetToken = false;
    this.x = 0;
    this.y = 0;
    this.movedLayer = undefined;
}

CreativeGenerator.prototype.getCanvasSize = function() {
    var docWIDTH = app.activeDocument.width;
    var docHEIGHT = app.activeDocument.height;
    docWIDTH = docWIDTH.toString().split(' ');
    docHEIGHT = docHEIGHT.toString().split(' ');
    return  docWIDTH[0] + "x" + docHEIGHT[0];
};

CreativeGenerator.prototype.moveLayer = function(layer, x, y) {
    var that = this;
    that.x = x;
    that.y = y;
    that.offSetToken = true;
    that.movedLayer = layer;
    return layer.translate(x, y);
};

CreativeGenerator.prototype.resetMoveLayer = function() {
    var that = this;
    if(!that.offSetToken) return;
    that.y = that.y * -1;
    that.x = that.x * -1;
    that.movedLayer.translate(that.x, that.y);
    that.offSetToken = false;
};

CreativeGenerator.prototype.positionButton = function(format, brandname) {
    var that = this;
    that.offSetX = that.config[format].position.offSetX;
    that.offSetY = that.config[format].position.offSetY;
    
    if(that.config[format].buttonTranslate[brandname] === true) {
        that.moveLayer(that.buttonLayer, that.offSetX, that.offSetY);
    }
};

CreativeGenerator.prototype.replaceText = function(brandname) {
    var that = this;
    var newText;
    var textLayer = app.activeDocument.layerSets.getByName('textGroup');
    
    for (var i = 0; i < textLayer.layers.length; i++) {
        if (textLayer.layers[i].name === 'text') {
            var format = that.getCanvasSize();
            switch(brandname) {
                case 'applebees':
                case 'carrefour':
                case 'senai':
                    newText = that.config[format].callAction.replace('na Ambev', 'no '+brandname);
                break;
                default:
                    var name = (that.config[format].textLogos[brandname]) ? (that.config[format].textLogos[brandname]) : brandname;
                    newText = that.config[format].callAction.replace('Ambev', name);
                break;
            }
            textLayer.layers[i].textItem.contents = newText;
            that.positionButton(format, brandname);
        }
    }
    return true;
};

CreativeGenerator.prototype.hideLayer = function(layers) {
    if (!layers || !(layers instanceof Object) || layers.length === 0) {
        return false;
    }

    for (var i = 0; i < layers.length; i++) {
        if (layers[i].visible === true) {
            layers[i].visible = false;
        }
    }
    
    return true;
};

CreativeGenerator.prototype.start = function(file) {
    var that = this, logosGroup, outputFolder, logosInGroup;
    
    try {
        logosGroup = app.activeDocument.layerSets.getByName('logos');
    } catch(error) {
        alert('Layer "logos" is not configured');
        return;
    }
    
    try {
        that.buttonLayer = app.activeDocument.layerSets.getByName('buttonGroup');
    } catch(error){
        alert('Layer "buttonGroup" is not configured');
        return;
    }
    
    outputFolder = that.config.setup.outputFiles+that.getCanvasSize();
    if (!that.files.createFolder(outputFolder)) {
        alert('Output folder not created');
        return;
    }

    logosInGroup = logosGroup.layers;
    for (var i = 0; i < logosInGroup.length; i++) {
        if (that.hideLayer(logosInGroup)) {
            logosInGroup[i].visible = true;
        }
        if (that.replaceText(logosInGroup[i].name)) {
            that.files.save(outputFolder, logosInGroup[i].name, that.config.setup.quality);
            that.resetMoveLayer();
        }
    }
    
    if(!that.config.setup.spriteSheet) {
        that.appPreferences.closeDocDoNotSave();
    } else {
        that.spriteSheetCreator.makeSpriteSheet(file);
    }
};

CreativeGenerator.prototype.init = function() {
    var that = this;
    that.files.openByExtension(that.config.setup.sourceFiles, '*.psd', function(file){
        that.start(file);
    });
};