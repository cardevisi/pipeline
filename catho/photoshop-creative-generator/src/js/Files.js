function Files() {}

Files.prototype.save = function(path, name, quality) {
    var options = new JPEGSaveOptions();
    options.embedColorProfile = true;
    options.quality = quality;
    var newName = path+"/"+name.toLowerCase()+".jpg";
    app.activeDocument.saveAs(new File(newName), options, true, Extension.LOWERCASE);
};

Files.prototype.savePNG = function(path, name, quality) {
    var options = new ExportOptionsSaveForWeb();
    options.format = SaveDocumentType.PNG;
    var newName = path+"/"+name.toLowerCase()+".png";
    app.activeDocument.exportDocument(new File(newName), ExportType.SAVEFORWEB, options);
};

Files.prototype.openByExtension = function(path, extension, callback) {
    var inputFolder = new Folder(path);
    var inputFiles = inputFolder.getFiles(extension);
    for (var i = 0; i < inputFiles.length; i++) {
        var psd = new File(inputFiles[i]);
        var file = app.open(psd);
        callback(file);
    }
};

Files.prototype.createFolder = function(path) {
    var folder = new Folder(path);
    if (!folder.exists) {
        folder.create();
    }
    return true;
};