function Main() {
	this.creativeGenerator = new CreativeGenerator();
    this.app = new AppPreferences();
}

Main.prototype.init = function() {
	this.app.applyPreferences();
	this.app.closeAll();
	this.creativeGenerator.init();
};