function SpriteSheetCreator() {
    this.config = Config();
    this.files = new Files();
    this.appPreferences = new AppPreferences();
}

SpriteSheetCreator.prototype.makeGrid = function(rows, collumns, width, height, space) {
    var positions = [];
    var index = 0;
    for (var x = 0; x < rows; x++) {
        for (var y = 0; y < collumns; y++) {
            var posX = y*(width+space);
            var posY = x*(height+space);
            positions[index] = {
                x: posX,
                y: posY,
                w: width, 
                h: height
            };
            index++;
        }
    }
    return positions;
};

SpriteSheetCreator.prototype.positionLayers = function(width, height) {
    var that = this;
    var layerNum = app.activeDocument.layers.length;
    var raiz = Math.ceil(Math.sqrt(layerNum));
    var columns = raiz;
    var rows = (raiz < 5) ? 1 : raiz;
    var space = 10;
    var docHeight = rows * (height + space);
    var docWidth = columns * (width + space);
    var positions = that.makeGrid(rows,columns, width, height, space);

    for (var j = 0; j < layerNum; j++) {
        try {
            var posX = positions[j].x;
            var posY = positions[j].y;
            var layerA = app.activeDocument.layers[j];
            layerA.name = 'layer_'+j;
            layerA.translate(posX, posY);
        } catch(e) {
            
        }
    }
    app.activeDocument.resizeCanvas(docWidth, docHeight, AnchorPosition.TOPLEFT);
};

SpriteSheetCreator.prototype.openFileByExtension = function(path, extension, callback) {
    var inputFolder = new Folder(path);
    var inputFiles = inputFolder.getFiles(extension);
    for (var i = 0; i < inputFiles.length; i++) {
        var file = new File(inputFiles[i]);
        var currFile = app.open(file);
        callback(currFile);
    }
};

SpriteSheetCreator.prototype.makeSpriteSheet = function(file) {
    var that = this;
    var width = Number(file.width.toString().split(' ')[0]);
    var height = Number(file.height.toString().split(' ')[0]);
    var fileName = width+'x'+height;

    that.appPreferences.closeDocDoNotSave();
    that.appPreferences.createDocument(width, height);
    that.openFileByExtension(that.config.setup.outputFiles+fileName+'/', '*.jpg', function(file) {
        file.selection.selectAll();
        file.selection.copy();
        file.close(SaveOptions.DONOTSAVECHANGES);
        app.activeDocument.paste();
    });

    that.positionLayers(width, height);
    that.files.savePNG(that.config.setup.outputFiles, fileName);
    that.appPreferences.closeDocDoNotSave();
};
