describe('CreativeGenerator Test' , function(){

	var generator;

	beforeEach(function(){
		generator = new CreativeGenerator();
	});

	it('deve retornar false se os parâmetro não for passado', function(){
		var result = generator.hideLayer();
		expect(result).toBe(false);
	});

	it('deve retornar false se o parâmetro array for vazio', function(){
		var result = generator.hideLayer([]);
		expect(result).toBe(false);
	});

	it('deve retornar false se o parâmetro for diferente de objeto', function(){
		var result = generator.hideLayer('');
		expect(result).toBe(false);
	});

});