describe('SpriteSheet Test' , function(){

	it('deve retornar um array vazio', function(){
		var grid = new SpriteSheetCreator();
		var result = grid.makeGrid();
		var position = [];
        expect(result).toEqual([]);
	});
	it('deve retornar uma matriz de objetos de posicionamento de 1x1', function(){
		var grid = new SpriteSheetCreator();
		var result = grid.makeGrid(1, 1, 50, 50, 10);
		var position = [
			{
	            x: 0,
	            y: 0,
	            w: 50, 
	            h: 50
        	}
        ];
		expect(result[0]).toEqual(position[0]);
	});
	it('deve retornar uma matriz de objetos de posicionamento de 2x2', function(){
		var grid = new SpriteSheetCreator();
		var result = grid.makeGrid(2, 2, 50, 50, 10);
		var position = [
			{
	            x: 0,
	            y: 0,
	            w: 50, 
	            h: 50
	        },
	        {
	            x: 60,
	            y: 0,
	            w: 50, 
	            h: 50
	        }
	    ];
		expect(position[0]).toEqual(result[0]);
		expect(position[1]).toEqual(result[1]);
	});

});