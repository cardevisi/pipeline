
function click(e) {
  console.log('Hello', this);
   chrome.tabs.executeScript(null,
       {code:"document.body.style.backgroundColor='" + e.target.id + "'"});
   window.close();
}

document.addEventListener('DOMContentLoaded', function () {
  var btn = document.querySelector('#status');  
  console.log("BTN", btn);
  btn.addEventListener('click', click);

  // var divs = document.querySelectorAll('div');
  // for (var i = 0; i < divs.length; i++) {
  //   divs[i].addEventListener('click', click);
  // }
});