// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/*const tab_log = function(json_args) {
  var args = JSON.parse(unescape(json_args));
  console[args[0]].apply(console, Array.prototype.slice.call(args, 1));
}

chrome.extension.onRequest.addListener(function(request) {
  if (request.command !== 'sendToConsole') {
    return;
  }
  console.log('aqui', request.command, request.tabId, request.args);
  chrome.tabs.executeScript(request.tabId, {
      code: "("+ tab_log + ")('" + request.args + "');",
  });
});*/

/*chrome.devtools.panels.create("My Panel", "MyPanelIcon.png", "panel.html",
    function(panel) {
        var box = document.querySelector('.box');
        box.innerHTML = 'teste';
    }
);

chrome.devtools.panels.elements.createSidebarPane("My Sidebar",
    function(sidebar) {
        // sidebar initialization code here
        sidebar.setObject({ some_data: "Some data to show" });
});*/


/*chrome.devtools.network.onRequestFinished.addListener(
  function(request) {
    if (request.response.bodySize > 40*1024) {
        chrome.devtools.inspectedWindow.eval(
            'console.log("Large image: " + unescape("' +
            escape(request.request.url) + '"))');
    }
});*/

/*
    console.log('back hello');
    var iframes = document.querySelectorAll('.tm-ads iframe'); for (var i = 0; i < iframes.length; i++) {iframes[i].style.height = largura*1.33333;};
    top.window.innerWidth
    var tmAds = document.querySelectorAll('.tm-ads');if (top.window.innerWidth > 768) {for (var i = 0; i < tmAds.length; i++) {tmAds[i].setAttribute('style', 'float:left;width:33.33333333%;box-sizing:border-box;padding-right:5px;padding-left:5px;');};window.frameElement.height = 300;} else {for (var i = 0; i < tmAds.length; i++) {tmAds[i].setAttribute('style', '');};}
*/

/*float: left;
padding-left: 5px;
padding-right: 5px;
width: 33.33333333%;
box-sizing: border-box;*/


/* Keep track of the active tab in each window */
var activeTabs = {};

chrome.tabs.onActivated.addListener(function(details) {
    activeTabs[details.windowId] = details.tabId;
});

/* Clear the corresponding entry, whenever a window is closed */
chrome.windows.onRemoved.addListener(function(winId) {
    delete(activeTabs[winId]);
});

/* Listen for web-requests and filter them */
chrome.webRequest.onBeforeRequest.addListener(function(details) {
    if (details.tabId == -1) {
        console.log("Skipping request from non-tabbed context...");
        return;
    }

    var notInteresting = Object.keys(activeTabs).every(function(key) {
        if (activeTabs[key] == details.tabId) {
            /* We are interested in this request */
            console.log("Check this out:", details);
            return false;
        } else {
            return true;
        }
    });

    if (notInteresting) {
        /* We are not interested in this request */
        console.log("Just ignore this one:", details);
    }
}, { urls: ["<all_urls>"] });

/* Get the active tabs in all currently open windows */
chrome.tabs.query({ active: true }, function(tabs) {
    tabs.forEach(function(tab) {
        activeTabs[tab.windowId] = tab.id;
    });
    console.log("activeTabs = ", activeTabs);
});