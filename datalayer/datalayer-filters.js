

function filterPropertys(source, query) {
    var result = [];
    source.filter(function(item) {
        return item[query];
    }).forEach(function(item){
        result.push(item[query]);
    })
    return result;
}

filterPropertys(dataLayer, 'ecommerce')[0].detail.products.map(function(item){
    return {"product": {
        "sku_code": item.id,
        "description": item.name,
        "category": "",
        "subcategory": "",
        "unit_price": item.price,
        "currency": "BR"
    }};
})