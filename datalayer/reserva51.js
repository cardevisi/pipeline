Seletor
body > section.section-page.page-done.section-background.p-b-el > div:nth-child(1) > h2 > strong

Numero do pedido:
<h2 class="title-el m-b-es c-white text-center p-t-el m-t-m">Pedido <strong class="c-brown">Nº 14227</strong> efetuado com sucesso!</h2>

//document.querySelector('.container .container-inner .c-brown').textContent.replace(/Nº/, '').trim()


Seletor
body > section.section-page.page-done.section-background.p-b-el > div:nth-child(1) > div.bd-t-m.bd-gray-dashed.p-t-s > div.text-right.p-t-s.p-b-s.m-t-s.bd-t-m.bd-yellow > p:nth-child(3) > span.bold.basket-price.ib.f-roboto-bold

Total 
<span class="bold basket-price ib f-roboto-bold">R$138,51</span>

//Number(document.querySelector('.text-right p:nth-child(3) span.basket-price').textContent.replace('R$', '').replace(',', '.'))


URL para confirmação:
https://www.reserva51.com.br/carrinho/finalizar/confirmacao

Tag Manager:
https://tm.jsuol.com.br/uoltm.js?id=ckbzhy


try {
    window.universal_variable = window.universal_variable || {};
    window.triggerUOLTM = window.triggerUOLTM || [];

    var orderId, total, totalElement, orderElement;

    totalElement = document.querySelector('.text-right p:nth-child(3) span.basket-price');
    if(totalElement) {
        total = totalElement.textContent;
        total = total.replace('R$', '');
        total = total.replace(',', '.');
        total = Number(total)*100;
    }

    orderElement = document.querySelector('.container .container-inner .c-brown');
    if (orderElement) {
        orderId = orderElement.textContent;
        orderId = orderId.replace(/Nº|nº/g, '');
        orderId = orderId.trim();
    }

    var uv = {
        "page" : {
            "category" : "checkout"
        },
        "user" : {
            "user_id" : ""
        },
        "basket": {
            "currency": "BRL",
            "total": (total) ? total : 0,
            "line_items": [
                {
                    "product": {
                        "sku_code": "1",
                        "description": "",
                        "category": "",
                        "subcategory": "",
                        "unit_price": 0
                    }
                }
            ]            
        }
    };

    console.log('call-trigger', 'uvar-created');
    window.universal_variable  = uv;
    window.triggerUOLTM.push({"eventName": "uvar-created"}); 

} catch(error) {
    console.warn('Não foi possível criar a universal_variable: ', error);
}


//https://tm.jsuol.com.br/uoltm.js?id=ckbzhy