package net.dynad.digester.imps.vivareal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class CoordinateMapping {
	private Coordinate defaultCoordiante = new Coordinate(-15.9507558d,-48.1837257d);
	private Map<String, Coordinate> mapCoordinates;
	private File input;
	public CoordinateMapping(File input) {
		super();
		this.input = input;
		this.mapCoordinates = new HashMap<>();
	}
	
	public static String getKey( String state, String city ) { 
		return "[c][" + state.toLowerCase() + "][" + city.toLowerCase() + "]";
	}
	
	public void initialize () throws IOException { 
		System.out.print("reading coordinates input. 0%");
		try ( BufferedReader in = new BufferedReader(
				   new InputStreamReader (
		                      new FileInputStream( this.input ), "UTF8")); ) { 
			
	
			long sizeOutputFile = this.input.length();
			long partials = sizeOutputFile / 20l;
			long nextPartial = partials;
			long currentBytes = 0l;
			
			String line = null;
			while ( (line = in.readLine() ) != null ) { 
				currentBytes += line.getBytes().length;
				if( line == null || line.trim().equals("") )
					continue;
				
				String [] data = line.split(",");
				if( "uf".equals( data[0] ) )
					this.mapCoordinates.put( getKey( data[0], getUFFullName( data[1] ) ) , new Coordinate( Double.parseDouble( data[2] ) , Double.parseDouble( data[3] )) );
				else
					this.mapCoordinates.put( getKey( getUFFullName(data[0]).toLowerCase(), data[1]) , new Coordinate( Double.parseDouble( data[2] ) , Double.parseDouble( data[3] )) );
				if( currentBytes >= nextPartial ) {
					System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
					nextPartial += partials;
				}
			}
		} finally { 
			System.out.println("100%.");
		}
	}
	
	public Map<String, Coordinate> getMap() { 
		return this.mapCoordinates;
	}
	
	public Coordinate getCoordinates ( String state ) { 
		return this.getCoordinates("uf", state);
	}
	
	public Coordinate getCoordinates ( String state, String city ) {
		if( this.mapCoordinates.containsKey( getKey( state, city ) ) )
			return this.mapCoordinates.get( getKey( state, city ) );
		else {
			if( !"uf".equals(state) )
				if( this.mapCoordinates.containsKey( getKey( "uf", state ) ) )
					return this.mapCoordinates.get( getKey( "uf", state ) );
			return this.defaultCoordiante;
		}
	}
	
	private String getUFFullName( String uf ) { 
		if( "AC".equals(uf) ) return "Acre";
		else if( "AL".equals(uf) ) return "Alagoas";
		else if( "AP".equals(uf) ) return "Amapá";
		else if( "AM".equals(uf) ) return "Amazonas";
		else if( "BA".equals(uf) ) return "Bahia";
		else if( "CE".equals(uf) ) return "Ceará";
		else if( "DF".equals(uf) ) return "Distrito Federal";
		else if( "ES".equals(uf) ) return "Espírito Santo";
		else if( "GO".equals(uf) ) return "Goiás";
		else if( "MA".equals(uf) ) return "Maranhão";
		else if( "MT".equals(uf) ) return "Mato Grosso";
		else if( "MS".equals(uf) ) return "Mato Grosso do Sul";
		else if( "MG".equals(uf) ) return "Minas Gerais";
		else if( "PA".equals(uf) ) return "Pará";
		else if( "PB".equals(uf) ) return "Paraíba";
		else if( "PR".equals(uf) ) return "Paraná";
		else if( "PE".equals(uf) ) return "Pernambuco";
		else if( "RJ".equals(uf) ) return "Rio de Janeiro";
		else if( "RN".equals(uf) ) return "Rio Grande do Norte";
		else if( "RS".equals(uf) ) return "Rio Grande do Sul";
		else if( "RO".equals(uf) ) return "Rondônia";
		else if( "RR".equals(uf) ) return "Roraima";
		else if( "SC".equals(uf) ) return "Santa Catarina";
		else if( "SP".equals(uf) ) return "São Paulo";
		else if( "SE".equals(uf) ) return "Sergipe";
		else if( "TO".equals(uf) ) return "Tocantins";
		else return uf;
	}
	
	class Coordinate { 
		double longitude;
		double latitude;
		public Coordinate(double latitude, double longitude) {
			super();
			this.longitude = longitude;
			this.latitude = latitude;
		}
		public double getLongitude() {
			return longitude;
		}
		public double getLatitude() {
			return latitude;
		}
		
	}
	
	
}
