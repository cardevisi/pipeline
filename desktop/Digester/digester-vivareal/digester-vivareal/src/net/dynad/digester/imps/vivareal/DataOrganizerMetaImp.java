package net.dynad.digester.imps.vivareal;

import java.io.IOException;
import java.util.Map;
import net.dynad.digester.imps.vivareal.pojo.Empreendimento;

public class DataOrganizerMetaImp implements IDataOrganizer {
	private MetadataWriter writer;
	
	public DataOrganizerMetaImp(MetadataWriter writer) {
		super();
		this.writer = writer;
	}

	@Override
	public void initialize () {
		
	}
	
	@Override
	public void organize ( String strSku, String strName, String strDescription, String strAnchor, 
								String strPrice, String strImage, String strTypeEmpreendimento, 
								String strCity, String strRegion, String strNeibor, String strLatitude, 
								String strLongitude, String strType, String strArea, String strImage2 ) {
		try {
			writer.write(strSku, strName, strDescription, strAnchor, strPrice, strImage, strTypeEmpreendimento, strCity, strRegion, strNeibor, strLatitude, strLongitude, strType, strArea, strImage2);
		} catch (IOException ex){
			throw new RuntimeException(ex);
		}
	}
	
	@Override
	public void dispose() { 
		writer.terminate();
	}

	@Override
	public Map<String, Empreendimento> getMapEmpreendimentos() {
		return null;
	}
	
}
