package net.dynad.digester.imps.vivareal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import net.dynad.digester.imps.vivareal.pojo.Area;
import net.dynad.digester.imps.vivareal.pojo.City;
import net.dynad.digester.imps.vivareal.pojo.Empreendimento;
import net.dynad.digester.imps.vivareal.pojo.Region;
import net.dynad.digester.imps.vivareal.pojo.Type;
import net.dynad.digester.imps.vivareal.pojo.TypeEmpreendimento;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class DataOrganizerRecImp implements IDataOrganizer {

	private AtomicLong empreendimentoIdGenerator, areaIdGenerator, regionIdGenerator, cityIdGenerator, typeIdGenerator;
	private java.util.Map<String, Empreendimento> mapEmpreendimentos;
	private java.util.Map<String, Type> mapTypes;
	private java.util.Map<String, TypeEmpreendimento> mapTypeEmpreendimentos;
	private java.util.Map<String, Area> mapArea;
	private java.util.Map<String, City> mapCity;
	private java.util.Map<String, Region> mapRegion;
	
	private JedisPool jedisPool;
	private Jedis jedisInstance;
	private int dbIndex;
	private File dictionaryPath, fileScore = null, fileDictionary = null;	
	private BufferedWriter outScore = null, outDictionary = null;
	
	private CoordinateMapping coordinatesInstance;
	public DataOrganizerRecImp ( File dictionaryPath, int dbIndex, CoordinateMapping coordinatesInstance ) { 
		this.dictionaryPath = dictionaryPath;
		this.dbIndex = dbIndex;
		this.fileScore = new File(this.dictionaryPath, "score.txt");
		this.fileDictionary = new File(this.dictionaryPath, "dictionary.txt");
		this.coordinatesInstance = coordinatesInstance;
	}
	
	public File getDictionaryFile() { 
		return this.fileDictionary;
	}
	
	public File getScoreFile () { 
		return this.fileScore;
	}
	
	@Override
	public void initialize () { 
		this.empreendimentoIdGenerator = new AtomicLong(0);
		this.areaIdGenerator = new AtomicLong(0);
		this.regionIdGenerator = new AtomicLong(0);
		this.cityIdGenerator = new AtomicLong(0);
		this.typeIdGenerator = new AtomicLong(0);
		
		this.mapEmpreendimentos = new java.util.HashMap<>();
		this.mapTypes = new java.util.HashMap<>();
		this.mapTypeEmpreendimentos = new java.util.HashMap<>();
		this.mapArea = new java.util.HashMap<>();
		this.mapCity = new java.util.HashMap<>();
		this.mapRegion = new java.util.HashMap<>();
		this.jedisPool = new JedisConnectionFactory().build();
		this.jedisInstance = this.jedisPool.getResource();
		this.jedisInstance.select( this.dbIndex );
		
		if( this.fileScore.exists() )
			this.fileScore.delete();
		if( this.fileDictionary.exists() )
			this.fileDictionary.delete();
		
		try { 
			this.outScore = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileScore), "UTF8"));
		} catch ( UnsupportedEncodingException | FileNotFoundException  ex ) { 
			throw new RuntimeException("unable to create stream for score output.", ex);
		}
		
		try { 
			this.outDictionary = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDictionary), "UTF8"));
		} catch ( UnsupportedEncodingException | FileNotFoundException  ex ) { 
			throw new RuntimeException("unable to create stream for dictionary output.", ex);
		}		
	}
	
	@Override
	public void organize ( String strSku, String strName, String strDescription, String strAnchor, 
								String strPrice, String strImage, String strTypeEmpreendimento, 
								String strCity, String strRegion, String strNeibor, String strLatitude, 
								String strLongitude, String strType, String strArea, String strImage2 ) {
		Type type = null;
		if( hasType(strType) )
			type = getType(strType);
		else
			type = registerType(strType);
		
		TypeEmpreendimento typeEmpreendimento = null;
		if( hasTypeEmpreendimento(strTypeEmpreendimento) )
			typeEmpreendimento = getTypeEmpreendimento(strTypeEmpreendimento);
		else
			typeEmpreendimento = registerTypeEmpreendimento(strTypeEmpreendimento);
		
		Region region = null;
		if( hasRegion( strRegion ) )
			region = getRegion(strRegion);
		else
			region = registerRegion(strRegion);
		
		City city = null;
		if( hasCity(strCity, region) )
			city = getCity(strCity, region);
		else
			city = registerCity(strCity, region);
		
		Area area = null;
		if( hasArea(strArea, city) )
			area = getArea(strArea, city);
		else
			area = registerArea(strArea, city);
		
		Empreendimento empreendimento = new Empreendimento(getNextEmpreendimentoId(), "");
		//empreendimento.setDescription(strDescription);
		//empreendimento.setImage(strImage);
		empreendimento.setLatitude( Double.parseDouble((strLatitude == null || strLatitude.trim().equals("") ? "-9999999999" : strLatitude)) );
		empreendimento.setLongitude(Double.parseDouble((strLongitude == null || strLongitude.trim().equals("") ? "-9999999999" : strLongitude)));
		//empreendimento.setLink(strAnchor);
		//empreendimento.setNeibor(strNeibor);
		empreendimento.setPrice( ( strPrice == null || strPrice.trim().equals("") ? -1 : Double.parseDouble( strPrice ) ) );
		empreendimento.setSku(strSku);
		empreendimento.setType(type);
		empreendimento.setTypeEmpreendimento(typeEmpreendimento);
		empreendimento.setArea(area);
		//empreendimento.setImage2(strImage2);
		registerEmpreendimento(empreendimento);
		saveScore( empreendimento );
	}
	
	private void saveScore ( Empreendimento empreendimento ) { 
		if( empreendimento.getLatitude() != -9999999999.0d && empreendimento.getLongitude() != -9999999999.0d ) {
			CoordinateMapping.Coordinate coordinates = this.coordinatesInstance.getCoordinates( empreendimento.getArea().getCity().getRegion().getName(), empreendimento.getArea().getCity().getName() );
			double distance = (100000000.0d * empreendimento.distance(coordinates.getLatitude(), coordinates.getLongitude(), 'M')) + ( empreendimento.getPrice() );
			String key = "1." + empreendimento.getType().getId() + "." + empreendimento.getTypeEmpreendimento().getId() + "." + empreendimento.getArea().getId(),
					key2 = "2." + empreendimento.getType().getId() + "." + empreendimento.getTypeEmpreendimento().getId() + "." + empreendimento.getArea().getCity().getId();
			try { 
				this.jedisInstance.zadd( key, distance, empreendimento.getSku() );
				this.jedisInstance.zadd( key2, distance, empreendimento.getSku() );
			} catch ( JedisConnectionException exConn ) { 
				this.jedisInstance.close();
				this.jedisInstance = this.jedisPool.getResource();
				this.jedisInstance.select( this.dbIndex );
				this.jedisInstance.zadd( key, distance, empreendimento.getSku() );
				this.jedisInstance.zadd( key2, distance, empreendimento.getSku() );
			}
			
			try { 
				this.outScore.write( key + ";" + empreendimento.getSku() + ";" + distance + "\r\n" );				
				this.outScore.write( key2 + ";" + empreendimento.getSku() + ";" + distance + "\r\n" );
			} catch ( IOException ex ) { 
				throw new RuntimeException("unable to save score", ex);
			}
		}
	}
	
	private long getNextTypeId () { 
		return this.typeIdGenerator.incrementAndGet();
	}
	
	private long getNextCityId () { 
		return this.cityIdGenerator.incrementAndGet();
	}

	private long getNextAreaId () { 
		return this.areaIdGenerator.incrementAndGet();
	}

	private long getNextRegionId () { 
		return this.regionIdGenerator.incrementAndGet();
	}
	
	private long getNextEmpreendimentoId () { 
		return this.empreendimentoIdGenerator.incrementAndGet();
	}
	
	public boolean hasType( String type ) { 
		return this.mapTypes.containsKey(type);
	}
	
	public boolean hasTypeEmpreendimento( String typeEmpreendimento ) { 
		return this.mapTypeEmpreendimentos.containsKey(typeEmpreendimento);
	}
	
	public boolean hasEmpreendimento( String sku ) { 
		return this.mapEmpreendimentos.containsKey(sku);
	}

	public boolean hasArea( String area, City city ) { 
		return this.mapArea.containsKey( Area.getAreKey(area, city) );
	}
	
	public boolean hasCity( String city, Region region ) { 
		return this.mapCity.containsKey( City.getCityKey(city, region) );
	}
	
	public boolean hasRegion( String region ) { 
		return this.mapRegion.containsKey(region);
	}
	
	
	public Type getType( String strType ) { 
		return this.mapTypes.get(strType);
	}
	public TypeEmpreendimento getTypeEmpreendimento( String strTypeEmpreendimento ) { 
		return this.mapTypeEmpreendimentos.get(strTypeEmpreendimento);
	}
	public Empreendimento getEmpreendimento( String strSku ) { 
		return this.mapEmpreendimentos.get(strSku);
	}
	public Region getRegion( String strRegion ) { 
		return this.mapRegion.get(strRegion);
	}
	public City getCity( String strCity, Region regionInstance ) { 
		return this.mapCity.get( City.getCityKey(strCity, regionInstance) );
	}
	public Area getArea( String strArea, City city ) { 
		return this.mapArea.get( Area.getAreKey(strArea, city) );
	}
	
	
	public Type registerType( String strType ) {
		Type typeInstance = new Type(getNextTypeId(), strType);
		this.mapTypes.put( strType, typeInstance );
		return typeInstance;
	}
	public TypeEmpreendimento registerTypeEmpreendimento( String strTypeEmpreendimento ) {
		TypeEmpreendimento typeEmpreendimentoInstance = new TypeEmpreendimento(getNextTypeId(), strTypeEmpreendimento);
		this.mapTypeEmpreendimentos.put( strTypeEmpreendimento, typeEmpreendimentoInstance );
		return typeEmpreendimentoInstance;
	}
	public Empreendimento registerEmpreendimento( Empreendimento empreendimentoInstance ) { 
		this.mapEmpreendimentos.put( empreendimentoInstance.getSku(), empreendimentoInstance );
		empreendimentoInstance.createEdges();
		return empreendimentoInstance;
	}
	public Region registerRegion( String strRegion ) { 
		Region regionInstance = new Region(getNextRegionId(), strRegion);
		this.mapRegion.put( strRegion, regionInstance );
		return regionInstance;
	}
	public City registerCity( String strCity, Region regionInstance ) {
		City cityInstance = new City(getNextCityId(), strCity);
		cityInstance.setRegion(regionInstance);
		regionInstance.createCityEdge(cityInstance);
		this.mapCity.put(City.getCityKey(strCity, regionInstance) , cityInstance);
		return cityInstance;
	}
	public Area registerArea( String strArea, City cityInstance ) { 
		Area areaInstance = new Area(getNextAreaId(), strArea);
		areaInstance.setCity(cityInstance);
		cityInstance.createAreaEdge(areaInstance);
		this.mapArea.put(Area.getAreKey(strArea, cityInstance) , areaInstance);
		return areaInstance;
	}

	@Override
	public java.util.Map<String, Empreendimento> getMapEmpreendimentos() {
		return mapEmpreendimentos;
	}

	public java.util.Map<String, Type> getMapTypes() {
		return mapTypes;
	}

	public java.util.Map<String, TypeEmpreendimento> getMapTypeEmpreendimentos() {
		return mapTypeEmpreendimentos;
	}

	public java.util.Map<String, Area> getMapArea() {
		return mapArea;
	}

	public java.util.Map<String, City> getMapCity() {
		return mapCity;
	}

	public java.util.Map<String, Region> getMapRegion() {
		return mapRegion;
	}

	private void saveDictionary () throws IOException {
		for(Map.Entry<String, Type> entry : this.mapTypes.entrySet() ) {
			Type typeInstance = entry.getValue();			
			this.outDictionary.write( "[t][" + typeInstance.getName() + "];" + typeInstance.getId() + "\r\n");
		}
		for(Map.Entry<String, TypeEmpreendimento> entry : this.mapTypeEmpreendimentos.entrySet() ) {
			TypeEmpreendimento typeEmpreendimentoInstance = entry.getValue();			
			this.outDictionary.write( "[e][" + typeEmpreendimentoInstance.getName() + "];" + typeEmpreendimentoInstance.getId() + "\r\n");
		}
		
		for(Map.Entry<String, Area> entry : this.mapArea.entrySet() ) {
			Area areaInstance = entry.getValue();			
			this.outDictionary.write( "[a][" + areaInstance.getCity().getRegion().getName() + "].[" + areaInstance.getCity().getName() + "].[" + areaInstance.getName() + "];" + areaInstance.getId() + "\r\n");
		}
		
		for(Map.Entry<String, City> entry : this.mapCity.entrySet() ) {
			City cityInstance = entry.getValue();			
			this.outDictionary.write( "[c][" + cityInstance.getRegion().getName() + "].[" + cityInstance.getName() + "];" + cityInstance.getId() + "\r\n");
		}
	}
	
	@Override
	public void dispose() {
		try { this.saveDictionary(); } catch ( Exception exClose){}
		try { if( this.outScore != null ) this.outScore.close(); } catch ( Exception exClose){}
		try { if( this.outDictionary != null ) this.outDictionary.close(); } catch ( Exception exClose){}
		try { if( jedisPool != null ) jedisPool.close(); } catch ( Exception exClose){}
		this.mapArea.clear(); this.mapCity.clear(); this.mapEmpreendimentos.clear(); this.mapRegion.clear(); this.mapTypeEmpreendimentos.clear(); this.mapTypes.clear();
	}
	
}
