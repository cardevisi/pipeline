package net.dynad.digester.imps.vivareal;

import net.dynad.digester.imps.vivareal.pojo.Empreendimento;

public interface IDataOrganizer {
	
	void initialize ();
	void organize ( String strSku, String strName, String strDescription, String strAnchor, 
			String strPrice, String strImage, String strTypeEmpreendimento, 
			String strCity, String strRegion, String strNeibor, String strLatitude, 
			String strLongitude, String strType, String strArea, String strImage2 );
	void dispose();
	java.util.Map<String, Empreendimento> getMapEmpreendimentos();
}
