package net.dynad.digester.imps.vivareal;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ImportFeed {
	private IDataOrganizer dataOrganizerInstance;
	private java.io.File feedPath;

	public ImportFeed(IDataOrganizer dataOrganizerInstance, File feedPath) {
		super();
		this.dataOrganizerInstance = dataOrganizerInstance;
		this.feedPath = feedPath;
	}
	
	private String normalizaMoeda(String s, Boolean cutThousands, Locale inLocale, Locale outLocale) throws ParseException {
        if(!s.equals("")) {
                s = s.replaceAll("[^0-9,.]", "");
                if(s == "") { return "0"; }
                NumberFormat fmt = NumberFormat.getInstance(outLocale);
                fmt.setMaximumFractionDigits(0);
                fmt.setMinimumFractionDigits(0);
                s = fmt.format(NumberFormat.getInstance(inLocale).parse(s).doubleValue());
        }
        if(cutThousands) s = s.replaceAll((DecimalFormatSymbols.getInstance(outLocale).getGroupingSeparator() == '.' ? "\\." : String.valueOf( DecimalFormatSymbols.getInstance(outLocale).getGroupingSeparator() )), "");
        return s;
	}
	
	private String autoNormalizaMoeda(String s, Boolean cutThousands, Locale outLocale) {
        if(s!= null && !s.equals("")  && s.length() > 2) {
                s = s.replaceAll("[^0-9,.]", "");
                String decimalChar = "";

                if(s.length() > 2 && (s.charAt(s.length()-2) == ',' || s.charAt(s.length()-2) == '.' )) { decimalChar = String.valueOf( s.charAt(s.length()-2) ); }
                else if(s.length() > 3 && (s.charAt(s.length()-3) == ',' || s.charAt(s.length()-3) == '.' )) { decimalChar = String.valueOf( s.charAt(s.length()-3) ); }
                else if(s.length() > 4 && (s.charAt(s.length()-4) == ',' || s.charAt(s.length()-4) == '.' )) { decimalChar = s.charAt(s.length()-4)==','?".": (s.charAt(s.length()-4)=='.'?",":""); }

                Locale lBR = new Locale("pt", "BR");
                Locale lEN = new Locale("en", "US");
                try {
	                if(decimalChar == ""+DecimalFormatSymbols.getInstance(lBR).getDecimalSeparator()) return normalizaMoeda(s, cutThousands, lBR, outLocale);
	                else if(decimalChar == ""+DecimalFormatSymbols.getInstance(lEN).getDecimalSeparator()) return normalizaMoeda(s, cutThousands, lEN, outLocale);
	                else 
	                	return normalizaMoeda(s, cutThousands, lEN, outLocale);
                } catch ( ParseException ex ) {
                	return s;
                }
        }

        return s;
}

	public void run () throws SAXException, ParserConfigurationException, IOException { 
		
		SAXParserFactory parserFactor = SAXParserFactory.newInstance();
	    SAXParser parser = parserFactor.newSAXParser();
	    DefaultHandler handler = new DefaultHandler() {
	    	long rows = 0l;
	        String sku, nome, descricao, link, preco, imagem, imagem2, categoria, cidade, estado, bairro, latitude, longitude, data, tagAtual, type, regiao;
	    	public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
	    		tagAtual = qName;
	    	}
	    	
	    	public void characters(char[] ch, int start, int length) throws SAXException {
	            String valor = new String(ch, start, length).trim();
	            if (tagAtual.equals("id")) {
	                sku = valor;
	            }
	            else if (tagAtual.equals("url")) {
	            	link = valor;
	        		if( link != null ) { 
	        			if( link.indexOf('?') > -1 )
	        				link = link.substring(0, link.indexOf('?'));
	        		}
	            } else if (tagAtual.equals("title")) nome = valor;
	            else if (tagAtual.equals("content") || tagAtual.equals("description")) descricao = valor.replaceAll("\\t", "").replaceAll("\\n", "<br>").replaceAll("  ", "");
	            else if (tagAtual.equals("property_type") ) categoria = valor;
	            else if (tagAtual.equals("price")) preco = autoNormalizaMoeda( valor, ( ImportFeed.this.dataOrganizerInstance instanceof DataOrganizerRecImp) , new Locale("pt", "BR") );
	            else if (tagAtual.equals("region") || tagAtual.equals("state")) estado = valor;
	            else if (tagAtual.equals("city")) cidade = valor;
	            else if (tagAtual.equals("neighborhood")) bairro = valor;
	            else if (tagAtual.equals("latitude")) latitude = valor;
	            else if (tagAtual.equals("longitude")) longitude = valor;
	            else if (tagAtual.equals("city_area") || tagAtual.equals("zone")) regiao = valor;
	            else if (tagAtual.equals("type")) type = valor;
	            else if ( ( tagAtual.equals("picture_url") ) && imagem == null) imagem = valor; 
	            else if ( tagAtual.equals("picture_url") && imagem != null && imagem2 == null) imagem2 = valor; 
	            tagAtual = "";
	        }
	    	
	    	public void endElement(String uri, String localName, String qName) throws SAXException {
	            if (qName.equals("ad") ) {
	            	ImportFeed.this.dataOrganizerInstance.organize(sku, nome, descricao, link, preco, imagem, categoria, cidade, estado, bairro, latitude, longitude, type, regiao, imagem2);	            	
	            	if( ++rows > 1 && rows % 100000l == 0 )
	            		System.out.print( "..." + rows );
	            	sku = null; nome = null; descricao = null; link = null; preco = null; imagem = null; imagem2 = null; categoria = null; cidade = null; estado = null; bairro = null; latitude = null; longitude = null; regiao = null;
	            }
	        }
	    };
	    
	    System.out.print( "reading file content ["+this.feedPath.getName()+"]: 0");
	    long s = System.currentTimeMillis();
	    parser.parse(feedPath, handler);
	    System.out.println( " (" + (System.currentTimeMillis() - s) + "ms).");
	    
	}
	
	public static void main (String [] args ) throws IOException { 
		CoordinateMapping coordinatesInstance = new CoordinateMapping( new File("/tmp/vivareal/estados.txt") );
		coordinatesInstance.initialize();
		
		IDataOrganizer dataOrganizer = new DataOrganizerRecImp( new File("/tmp/vivareal"), 0, coordinatesInstance );
		dataOrganizer.initialize();
		RecommendationsWriter writer = new RecommendationsWriter(new java.io.File("/tmp/vivareal/out.txt"));
		ImportFeed instance = new ImportFeed( dataOrganizer, new java.io.File("/tmp/vivareal/general.lancamientos.xml") );
		try {
			instance.run();
			Recommender recommender = new Recommender(dataOrganizer, writer, 11, 100, coordinatesInstance);
			recommender.recommend();
		} catch (SAXException | ParserConfigurationException | IOException e) {
			e.printStackTrace();
		} finally { 
			writer.terminate();
			dataOrganizer.dispose();
		}
		
		try { 
			MetadataWriter metaWriter = new MetadataWriter(new java.io.File("/tmp/vivareal/out.txt"));
			metaWriter.initialize();
			dataOrganizer = new DataOrganizerMetaImp(metaWriter);
			instance = new ImportFeed( dataOrganizer, new java.io.File("/tmp/vivareal/general.lancamientos.xml") );
			instance.run();
		} catch (SAXException | ParserConfigurationException | IOException e) {
			e.printStackTrace();
		} finally { 
			dataOrganizer.dispose();
		}
		
	}
}
