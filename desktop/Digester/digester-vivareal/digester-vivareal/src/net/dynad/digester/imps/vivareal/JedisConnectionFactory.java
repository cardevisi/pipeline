package net.dynad.digester.imps.vivareal;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisConnectionFactory {
	private int maxTotal = 10;
	public JedisConnectionFactory size ( int size ) { 
		this.maxTotal = size;
		return this;
	}
	public JedisPool build () { 
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle( 2 );
		config.setMaxTotal( maxTotal );
		config.setMinIdle( 0 );
		config.setMaxWaitMillis( 20l );
		config.setTestWhileIdle(true);
		config.setTimeBetweenEvictionRunsMillis(1000);
		config.setTestOnReturn(true);
		JedisPool pool = new JedisPool(config, "localhost", 6379);
		
		Jedis jedisInstance = null;
		try { 
			jedisInstance = pool.getResource();
			if( !"PONG".equals( jedisInstance.ping() ) )
				throw new RuntimeException( "invalid connection settings, ping failed");
		} catch ( RuntimeException ex ) { 
			try { pool.close(); } catch ( Exception exClose){}
			throw ex;
		} finally { 
			try { if( jedisInstance != null ) jedisInstance.close(); } catch ( Exception exClose){}
		}
		
		return pool;
	}

}
