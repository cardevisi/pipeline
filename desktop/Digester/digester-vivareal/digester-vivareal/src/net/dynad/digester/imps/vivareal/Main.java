package net.dynad.digester.imps.vivareal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import net.dynad.db.redis.sharding.IPoolingManager;
import net.dynad.db.redis.sharding.ManagerFactory;
import net.dynad.db.redis.sharding.MappingFactory;
import net.dynad.db.redis.sharding.config.ConfigMainPoolFactory;
import net.dynad.db.redis.sharding.config.ConfigMainPoolImp;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class Main {

	public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
		//recupera os dados repassado na linha de comando.
		String xmlsDir = null, xmlNames = null, output = null, shardingConfigDir = null, flushDb = null, tempFolder = "/tmp", coordinatesPath = "./coordinates.txt";
		int numRecs = 11, numExecutors = 20, dbIndexForUpload = 31, dbIndexForIndex = 0;
		for(String arg : args ) { 
			if( arg.startsWith("-xmldir=") )
				xmlsDir = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-xmlnames=") )
				xmlNames = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-output=") )
				output = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-sharding=") )
				shardingConfigDir = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-flush=") )
				flushDb = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-num=") )
				numRecs = Integer.parseInt( arg.substring(arg.indexOf('=') + 1).trim() );
			if( arg.startsWith("-temp-folder=") )
				tempFolder = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-coordinates=") )
				coordinatesPath = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-executors=") )
				numExecutors = Integer.parseInt( arg.substring(arg.indexOf('=') + 1).trim() );
			if( arg.startsWith("-upload-db-index=") )
				dbIndexForUpload = Integer.parseInt( arg.substring(arg.indexOf('=') + 1).trim() );
			if( arg.startsWith("-index-db-index=") )
				dbIndexForIndex = Integer.parseInt( arg.substring(arg.indexOf('=') + 1).trim() );
		}
		
		//valida se diretorio contendo xmls foi informado 
		if( xmlsDir == null )
			throw new RuntimeException("inform the argument xmldir");
		
		//valida se nomes de arquivos xmls foi informado
		if( xmlNames == null )
			throw new RuntimeException("inform the argument xmlnames");
		
		//valida se diretorio content xmls existe
		java.io.File dir = new java.io.File( xmlsDir );
		if( !dir.exists() || !dir.isDirectory() )
			throw new RuntimeException("invalid xmldir argument");
		
		//armazena timestamp de inicio do processamento para contabilizar tempo total decorrido ao final
		long gs = System.currentTimeMillis();
		
		java.io.File tempDir = new java.io.File( tempFolder );
		if( !tempDir.exists() || !tempDir.isDirectory() )
			throw new RuntimeException("invalid temp-folder argument ["+ tempFolder +"]");
		
		//valida se diretorio content xmls existe
		java.io.File coordinatesFile = new java.io.File( coordinatesPath );
		if( !coordinatesFile.exists() || !coordinatesFile.isFile() )
			throw new RuntimeException("invalid coordinates argument");
		
		CoordinateMapping coordinatesInstance = new CoordinateMapping(coordinatesFile);
		coordinatesInstance.initialize();
		
		//inicializa objeto responsavel por organizar os dados presentes no XML em estrutura de memoria otimizada
		IDataOrganizer dataOrganizer = new DataOrganizerRecImp( tempDir, dbIndexForIndex, coordinatesInstance );
		dataOrganizer.initialize();
		
		//inicia a leitura dos arquivos xmls.
		for( String name : xmlNames.split(",") ) { 
			java.io.File f = new java.io.File( dir, name + ".xml" );
			if( !f.exists() || !f.isFile() )
				throw new RuntimeException("invalid file " + f.getName());
			ImportFeed instance = new ImportFeed( dataOrganizer, f );
			instance.run();
		}
		
		//verifica se arquivo de saida existe (caso exista remove o registro)
		java.io.File outputFile = new java.io.File(output);
		if( outputFile.exists() )
			outputFile.delete();
		
			
		//inicia objeto responsavel por salvar em disco as recomendacoes
		RecommendationsWriter writer = new RecommendationsWriter(outputFile);
		try {
			System.out.println("Building Recommendations...");
			//inicia objeto responsavel por percorrer todos os produtos carregados inicialmente em memoria e gerar recomendacoes para cada um deles. 
			Recommender recommender = new Recommender(dataOrganizer, writer, numRecs, numExecutors, coordinatesInstance);
			recommender.recommend();
		} finally {
			dataOrganizer.dispose();
			writer.terminate();
		}
		
		File dictionaryFile = ((DataOrganizerRecImp)dataOrganizer).getDictionaryFile(), 
				scoreFile = ((DataOrganizerRecImp)dataOrganizer).getScoreFile();
		
		try {
			System.out.println("Merging recommendations with product metadata...");
			MetadataWriter metaWriter = new MetadataWriter(outputFile);
			metaWriter.initialize();
			dataOrganizer = new DataOrganizerMetaImp(metaWriter);
			//inicia a leitura dos arquivos xmls.
			for( String name : xmlNames.split(",") ) { 
				java.io.File f = new java.io.File( dir, name + ".xml" );
				if( !f.exists() || !f.isFile() )
					throw new RuntimeException("invalid file " + f.getName());
				ImportFeed instance = new ImportFeed( dataOrganizer, f );
				instance.run();
			}
		} catch (SAXException | ParserConfigurationException | IOException e) {
			throw new RuntimeException("Failed to merge metadata with recommendation data.", e);
		} finally { 
			dataOrganizer.dispose();
		}
		
		
		
		IPoolingManager manager = null;
		try {
			
			System.out.print("Opening sharding connections.");
			ConfigMainPoolImp config = new ConfigMainPoolFactory().file(new File(shardingConfigDir)).build();
			manager = new ManagerFactory().config(config).build();

			manager.getMappingDelegatorInstance().map( new MappingFactory().name( "vivareal" ).dbIndex(dbIndexForUpload).ttl(1296000l).build() );
            manager.getMappingDelegatorInstance().compile();

            JedisPool pool = manager.getMappingDelegatorInstance().query("vivareal");
            Jedis jedis = pool.getResource();
			
			BufferedReader in = null;
			long sr = System.currentTimeMillis();
			try {
				System.out.print("Loading data into sharding ["+shardingConfigDir+"]: 0%");
				in = new BufferedReader(
						   new InputStreamReader (
				                      new FileInputStream(outputFile), "UTF8"));
				
				
				long sizeOutputFile = outputFile.length();
				long partials = sizeOutputFile / 20l;
				long nextPartial = partials;
				long currentBytes = 0l;
				
				if( "true".equals( flushDb ) )
					jedis.flushDB();
				String line = null;
				while ( (line = in.readLine() ) != null ) { 
					currentBytes += line.getBytes().length;
					if( line == null || line.trim().equals("") )
						continue;
					String key = line.substring(0, line.indexOf('|') );
					String value = line.substring( line.indexOf('|') + 1 );
					jedis.setex(key, 1296000, value.trim());
					
					if( currentBytes >= nextPartial ) {
						System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
						nextPartial += partials;
					}
				}
			} catch ( Exception ex ) { 
				throw new RuntimeException("error ... loading sharding data", ex);
			} finally {
				System.out.println("...100% ("+ (System.currentTimeMillis() - sr) +"ms)");
				try {
					if( in != null )
						in.close();
				} catch ( Exception exClose ){}
				
			}
			
			
			in = null;
			sr = System.currentTimeMillis();
			try {
				System.out.print("Loading dictionary data into sharding ["+ dictionaryFile +"]: 0%");
				in = new BufferedReader(
						   new InputStreamReader (
				                      new FileInputStream( dictionaryFile ), "UTF8"));
				
				
				long sizeOutputFile = outputFile.length();
				long partials = sizeOutputFile / 20l;
				long nextPartial = partials;
				long currentBytes = 0l;
				
				String line = null;
				while ( (line = in.readLine() ) != null ) { 
					currentBytes += line.getBytes().length;
					if( line == null || line.trim().equals("") )
						continue;
					String key = line.substring(0, line.indexOf(';') );
					String value = line.substring( line.indexOf(';') + 1 );
					jedis.setex(key, 1296000, value.trim());					
					if( currentBytes >= nextPartial ) {
						System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
						nextPartial += partials;
					}
				}
			} catch ( Exception ex ) { 
				throw new RuntimeException("error ... loading dictionary data", ex);
			} finally {
				System.out.println("...100% ("+ (System.currentTimeMillis() - sr) +"ms)");
				try {
					if( in != null )
						in.close();
				} catch ( Exception exClose ){}
				
				String tempKey = null, tempKey2 = null;
				double tempScore = 0.0d, tempScore2 = 0.0d;
				System.out.println("key: " + (tempKey = getKey(jedis, "For sale", "Apartamento", "Rio de Janeiro", "Rio de Janeiro", "Centro") ) );
				System.out.println("key2: " + (tempKey2 = getKey(jedis, "For sale", "Apartamento", "Rio de Janeiro", "Rio de Janeiro") ) );
				
				System.out.println("score: " + (tempScore = getScore(-22.8795267d, -43.2674216d, 393000.0d) ) );
				System.out.println("score: " + (tempScore2 = getScore(jedis, "Rio de Janeiro", "Rio de Janeiro", 393000.0d) ) );
				
				System.out.println("key 1 / score 1: " + getRecommendations(jedis, tempScore, tempKey, 5) );
				System.out.println("key 2 / score 1: " + getRecommendations(jedis, tempScore, tempKey2, 5) );
				System.out.println("key 1 / score 2: " + getRecommendations(jedis, tempScore2, tempKey, 5) );
				System.out.println("key 2 / score 2: " + getRecommendations(jedis, tempScore2, tempKey2, 5) );
			}
			
			in = null;
			sr = System.currentTimeMillis();
			try {
				System.out.print("Loading score data into sharding ["+ scoreFile +"]: 0%");
				in = new BufferedReader(
						   new InputStreamReader (
				                      new FileInputStream( scoreFile ), "UTF8"));
				
				
				long sizeOutputFile = outputFile.length();
				long partials = sizeOutputFile / 20l;
				long nextPartial = partials;
				long currentBytes = 0l;
				
				String line = null;
				while ( (line = in.readLine() ) != null ) { 
					currentBytes += line.getBytes().length;
					if( line == null || line.trim().equals("") )
						continue;
					String [] data = line.trim().split(";");
					jedis.zadd( data[0], Double.parseDouble( data[2] ), data[1]);
					if( currentBytes >= nextPartial ) {
						System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
						nextPartial += partials;
					}
				}
			} catch ( Exception ex ) { 
				throw new RuntimeException("error ... loading dictionary data", ex);
			} finally {
				System.out.println("...100% ("+ (System.currentTimeMillis() - sr) +"ms)");
				try {
					if( in != null )
						in.close();
				} catch ( Exception exClose ){}
				
			}
			
			
			sr = System.currentTimeMillis();
			try {
				System.out.print("Loading coordinates into sharding: 0%");
				
				long sizeOutputFile = coordinatesInstance.getMap().size();
				long partials = sizeOutputFile / 20l;
				long nextPartial = partials;
				long currentBytes = 0l;
				
				for(Map.Entry<String, CoordinateMapping.Coordinate> entry : coordinatesInstance.getMap().entrySet() ) { 
					jedis.setex( entry.getKey(), 1296000, entry.getValue().getLatitude() + "," + entry.getValue().getLongitude() );
					if( ++currentBytes >= nextPartial ) {
						System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
						nextPartial += partials;
					}
				}
			} catch ( Exception ex ) { 
				throw new RuntimeException("error ... loading coordinates data", ex);
			} finally {
				System.out.println("...100% ("+ (System.currentTimeMillis() - sr) +"ms)");
			}
		} finally { 
			try { 
				if( manager != null ) 
					manager.destroy();
			} catch ( Exception exClose ){}
		}
		
		System.out.println("process finished in " + (System.currentTimeMillis() - gs) + "ms.");
	}
	
	private static double getScore( Jedis jedisInstance, String state, String city, double price ) {
		double [] coordinates = getCityCoordinates(jedisInstance, state, city);
		return (100000000.0d * distance(coordinates[0], -15.9507558d, coordinates[0], -48.1837257d, 'M')) + ( price );
	}
	
	private static double [] getCityCoordinates( Jedis jedisInstance, String state, String city ) {
		String coordinates = jedisInstance.get( CoordinateMapping.getKey(state, city) );
		if( coordinates == null || coordinates.trim().equals("") )
			coordinates = jedisInstance.get( CoordinateMapping.getKey("uf", state) );
		if( coordinates != null ) {
			String [] strCoord = coordinates.split(",");
			return new double[]{ Double.parseDouble( strCoord[0] ), Double.parseDouble( strCoord[1] ) }; 
		} else
			return new double[]{-15.9507558d,-48.1837257d};
	}
	
	private static String getKey( Jedis jedisInstance, String typeUse, String typeProperty, String state, String city, String region ) { 
		List<String> lstResult = jedisInstance.mget( "[t][" + typeUse + "]", "[e][" + typeProperty + "]", "[a]["+state+"].["+city+"].["+region+"]" );
		return "1." + lstResult.get(0) + "." + lstResult.get(1) + "." + lstResult.get(2);
	}
	
	private static String getKey( Jedis jedisInstance, String typeUse, String typeProperty, String state, String city ) { 
		List<String> lstResult = jedisInstance.mget( "[t][" + typeUse + "]", "[e][" + typeProperty + "]", "[c]["+state+"].["+city+"]" );
		return "2." + lstResult.get(0) + "." + lstResult.get(1) + "." + lstResult.get(2);
	}
	
	private static double getScore ( double latitude, double longitude, double price ) { 
		return (100000000.0d * distance(latitude, -15.9507558d, longitude, -48.1837257d, 'M')) + ( price );
	}
	
	private static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
      double theta = lon1 - lon2;
      double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
      dist = Math.acos(dist);
      dist = rad2deg(dist);
      dist = dist * 60 * 1.1515;
      if (unit == 'K') {
        dist = dist * 1.609344;
      } else if (unit == 'N') {
        dist = dist * 0.8684;
        }
      return (dist);
    }

	private static double deg2rad(double deg) {
      return (deg * Math.PI / 180.0);
    }

	private static double rad2deg(double rad) {
      return (rad * 180.0 / Math.PI);
    }
	
	private static java.util.List<String> getRecommendations (redis.clients.jedis.Jedis jedisInstance, double score, String key, int numRecs ) {
		java.util.Set<String> composedSet = new HashSet<>();
		java.util.Set<String> lookupSet = new HashSet<>();
		if( (lookupSet =  jedisInstance.zrevrangeByScore(key, String.valueOf(score) , "-inf", 0, numRecs + 10) ) != null ) {
			composedSet.addAll( lookupSet ); lookupSet = null;
		}
		if( (lookupSet = jedisInstance.zrevrangeByScore (key, "+inf", String.valueOf(score), 0, numRecs + 10) ) != null ) {
			composedSet.addAll( lookupSet ); lookupSet = null;
		}
		
		if( composedSet.size() == 0 )
			return null;
		else {
			java.util.List<String> setToList = new java.util.ArrayList<>();
			setToList.addAll( composedSet );
			java.util.Collections.shuffle( setToList );
			return setToList;
		}
	}

}
