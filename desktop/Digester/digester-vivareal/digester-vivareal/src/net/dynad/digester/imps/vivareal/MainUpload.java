package net.dynad.digester.imps.vivareal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import net.dynad.db.redis.sharding.IPoolingManager;
import net.dynad.db.redis.sharding.ManagerFactory;
import net.dynad.db.redis.sharding.MappingFactory;
import net.dynad.db.redis.sharding.config.ConfigMainPoolFactory;
import net.dynad.db.redis.sharding.config.ConfigMainPoolImp;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class MainUpload {

	public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
		//recupera os dados repassado na linha de comando.
		String xmlsDir = null, xmlNames = null, output = null, shardingConfigDir = null, flushDb = null;
		int numRecs = 11;
		for(String arg : args ) { 
			if( arg.startsWith("-xmldir=") )
				xmlsDir = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-xmlnames=") )
				xmlNames = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-output=") )
				output = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-sharding=") )
				shardingConfigDir = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-flush=") )
				flushDb = arg.substring(arg.indexOf('=') + 1).trim();
			if( arg.startsWith("-num=") )
				numRecs = Integer.parseInt( arg.substring(arg.indexOf('=') + 1).trim() );
		}
		
		//valida se diretorio contendo xmls foi informado 
		if( xmlsDir == null )
			throw new RuntimeException("inform the argument xmldir");
		
		//valida se nomes de arquivos xmls foi informado
		if( xmlNames == null )
			throw new RuntimeException("inform the argument xmlnames");
		
		//valida se diretorio content xmls existe
		java.io.File dir = new java.io.File( xmlsDir );
		if( !dir.exists() || !dir.isDirectory() )
			throw new RuntimeException("invalid xmldir argument");
		
		//armazena timestamp de inicio do processamento para contabilizar tempo total decorrido ao final
		long gs = System.currentTimeMillis();
		
		//verifica se arquivo de saida existe (caso exista remove o registro)
		java.io.File outputFile = new java.io.File(output);
		if( !outputFile.exists() )
			throw new RuntimeException("invalid output argument");
		
		
		BufferedReader in = null;
		IPoolingManager manager = null;
		long sr = System.currentTimeMillis();
		try {
			System.out.print("Loading data into sharding ["+shardingConfigDir+"]: 0%");
			ConfigMainPoolImp config = new ConfigMainPoolFactory().file(new File(shardingConfigDir)).build();
			manager = new ManagerFactory().config(config).build();

			manager.getMappingDelegatorInstance().map( new MappingFactory().name( "vivareal" ).dbIndex(31).ttl(1296000l).build() );
            manager.getMappingDelegatorInstance().compile();

            JedisPool pool = manager.getMappingDelegatorInstance().query("vivareal");
            Jedis jedis = pool.getResource();
            
			in = new BufferedReader(
					   new InputStreamReader (
			                      new FileInputStream(outputFile), "UTF8"));
			
			
			long sizeOutputFile = outputFile.length();
			long partials = sizeOutputFile / 20l;
			long nextPartial = partials;
			long currentBytes = 0l;
			
			if( "true".equals( flushDb ) )
				jedis.flushDB();
			String line = null;
			while ( (line = in.readLine() ) != null ) { 
				currentBytes += line.getBytes().length;
				if( line == null || line.trim().equals("") )
					continue;
				String key = line.substring(0, line.indexOf('|') );
				String value = line.substring( line.indexOf('|') + 1 );
				jedis.setex(key, 1296000, value.trim());
				if( currentBytes >= nextPartial ) {
					System.out.print("..." + Math.round(( (double)currentBytes / (double)sizeOutputFile ) * 100) + "%" );
					nextPartial += partials;
				}
			}
		} catch ( Exception ex ) { 
			throw new RuntimeException("error ... loading sharding data", ex);
		} finally {
			System.out.println("...100% ("+ (System.currentTimeMillis() - sr) +"ms)");
			
			try { 
				if( manager != null ) 
					manager.destroy();
			} catch ( Exception exClose ){}
			try {
				if( in != null )
					in.close();
			} catch ( Exception exClose ){}
			
		}
		
		System.out.println("process finished in " + (System.currentTimeMillis() - gs) + "ms.");
	}

}
