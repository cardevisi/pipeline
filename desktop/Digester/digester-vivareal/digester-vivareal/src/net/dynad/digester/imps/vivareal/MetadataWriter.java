package net.dynad.digester.imps.vivareal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import net.dynad.digester.imps.vivareal.pojo.Empreendimento;

public class MetadataWriter {
	private java.util.Map<String, String> mapRecommendations;
	private java.io.File outputFile;
	private Writer writerInstance;

	public MetadataWriter(File outputFile) {
		super();
		this.outputFile = outputFile;
		this.mapRecommendations = new java.util.HashMap<>();
	}
	
	public void initialize() throws IOException { 
		try (BufferedReader in = new BufferedReader( new InputStreamReader( new FileInputStream(this.outputFile), "UTF8")); ) { 
			String line = null;
			while ((line = in.readLine()) != null) {
			    String [] data = line.split(";");
			    if( data.length == 2) {
			    	this.mapRecommendations.put(data[0], data[1]);
			    }
			}
		} finally {
			
		}
		
		this.outputFile.delete();		
		this.writerInstance = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.outputFile), "UTF-8"));
		this.writerInstance.write("metadata|sku|id|imagem|imagem_2|ativo|nome|descricao|link|preco_promocional|estado|cidade|bairro|latitude|longitude|categoria|recomendacoes\n");
	}
	
	
	public void write( String strSku, String strName, String strDescription, String strAnchor, 
			String strPrice, String strImage, String strTypeEmpreendimento, 
			String strCity, String strRegion, String strNeibor, String strLatitude, 
			String strLongitude, String strType, String strArea, String strImage2 ) throws IOException {
		if( this.mapRecommendations.containsKey(strSku) )
			this.writerInstance.write( strSku+ "|"+strSku+"|"+strImage+"|"+
					( strImage2 == null || strImage2.trim().equals("") ? strImage : strImage2 )+"|1|"+strName+"|"+strDescription+"|"+
					strAnchor+"|"+strPrice+"|"+strRegion+"|"+
					strCity+"|"+strNeibor+"|"+strLatitude+"|"+
					strLongitude+"|"+strTypeEmpreendimento+"|" +this.mapRecommendations.get(strSku)+ "\n");
		
	}
	
	public void terminate () { 
		try { this.writerInstance.flush(); } catch (IOException e) { }
		try { this.writerInstance.close(); } catch (IOException e) { }
	}
	
	
}
