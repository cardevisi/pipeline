package net.dynad.digester.imps.vivareal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import net.dynad.digester.imps.vivareal.pojo.Empreendimento;

public class RecommendationsWriter {
	private Writer writerInstance;
	private java.io.File outputFile;

	public RecommendationsWriter(File outputFile) {
		super();
		this.outputFile = outputFile;
	}
	
	public void initialize() throws IOException { 
		if( this.outputFile.exists() )
			this.outputFile.delete();
		this.writerInstance = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.outputFile), "UTF-8"));
		//this.writerInstance.write("metadata|sku|id|imagem|imagem_2|ativo|nome|descricao|link|preco_promocional|estado|cidade|bairro|latitude|longitude|categoria|recomendacoes\n");
	}
	
	public synchronized void write( Empreendimento empreendimento, java.util.List<Empreendimento> recomendacoes ) throws IOException {
		
		this.writerInstance.write(empreendimento.getSku()+";");
		int index = 0;
		for(Empreendimento recEmp : recomendacoes ) {
			if( recEmp.getSku().equals( empreendimento.getSku() ) ) continue;
			if( ++index > 1 ) this.writerInstance.write(",");
			this.writerInstance.write( recEmp.getSku() );
		}
		this.writerInstance.write("\n");
		
	}
	
	public void terminate () { 
		try { this.writerInstance.flush(); } catch (Exception e) { }
		try { this.writerInstance.close(); } catch (Exception e) { }
	}
}
