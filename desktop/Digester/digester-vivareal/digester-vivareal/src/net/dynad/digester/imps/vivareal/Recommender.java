package net.dynad.digester.imps.vivareal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.dynad.digester.imps.vivareal.pojo.Empreendimento;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class Recommender {
	private BlockingQueue<Empreendimento> queue;
	private IDataOrganizer dataOrganizerInstance;
	private RecommendationsWriter writer;
	private int numRecs = 11;
	
	private int numExecutors = 200;
	private int numExecutorsComplete;
	private JedisPool poolInstance;
	
	private CoordinateMapping coordinatesInstance;
	public Recommender(IDataOrganizer dataOrganizerInstance, RecommendationsWriter writer, int numRecs, int executors, CoordinateMapping coordinatesInstance ) {
		super();
		this.numExecutors = executors;
		this.queue = new LinkedBlockingQueue<>( this.numExecutors * 5 );
		this.dataOrganizerInstance = dataOrganizerInstance;
		this.writer = writer;
		if( numRecs > this.numRecs)
			this.numRecs = numRecs;
		this.coordinatesInstance = coordinatesInstance;
	}
	
	private void reportExecutorComplete () { 
		synchronized (this) {
			this.numExecutorsComplete++;
		}
	}
	
	private Jedis recommendWithScore (JedisPool poolInstance, Jedis jedisInstance, final Empreendimento empreendimento, List<Empreendimento> lstRecs ) { 
		if( empreendimento.getLatitude() != -9999999999.0d && empreendimento.getLongitude() != -9999999999.0d ) {
			CoordinateMapping.Coordinate coordinates = this.coordinatesInstance.getCoordinates( empreendimento.getArea().getCity().getRegion().getName(), empreendimento.getArea().getCity().getName() );
			double distance = (100000000.0d * empreendimento.distance(coordinates.getLatitude(),empreendimento.getLongitude(), 'M')) + ( empreendimento.getPrice() );
			String key = "1." + empreendimento.getType().getId() + "." + empreendimento.getTypeEmpreendimento().getId() + "." + empreendimento.getArea().getId();
			Set<String> result = null;
			
			try {
				result =  jedisInstance.zrevrangeByScore(key, String.valueOf(distance) , "-inf", 0, Recommender.this.numRecs + 10);
				result.addAll( jedisInstance.zrevrangeByScore (key, "+inf", String.valueOf(distance), 0, Recommender.this.numRecs + 10) );
	        } catch ( JedisConnectionException exConn ) {
	        	jedisInstance.close();
				jedisInstance = poolInstance.getResource();
				result =  jedisInstance.zrevrangeByScore(key, String.valueOf(distance) , "-inf", 0, Recommender.this.numRecs + 10);
				result.addAll( jedisInstance.zrevrangeByScore (key, "+inf", String.valueOf(distance), 0, Recommender.this.numRecs + 10) );
	        }
			
			if( result != null && result.size() > 0 ) { 
				for(String sku : result ) {
					if( empreendimento.getSku().equals( sku ) ) continue;
					if( this.dataOrganizerInstance.getMapEmpreendimentos().containsKey(sku) ) {						
						lstRecs.add( this.dataOrganizerInstance.getMapEmpreendimentos().get( sku ) );
					}
				}
			}
		}
		
		if( lstRecs.size() == 0 )
			recommendByLocation(empreendimento, lstRecs);
		
		if( lstRecs.size() > 1 ) {
			Collections.sort(lstRecs, new Comparator<Empreendimento>() {
				@Override
				public int compare(Empreendimento o1, Empreendimento o2) {
					double d1 = Recommender.this.distance(empreendimento.getLatitude(), empreendimento.getLongitude(), o1.getLatitude(), o1.getLongitude(), 'K');
					double d2 = Recommender.this.distance(empreendimento.getLatitude(), empreendimento.getLongitude(), o2.getLatitude(), o2.getLongitude(), 'K');
					return d1 > d2 ? -1 : (d2 > d1 ? 1 : 0);
				}
			});
		}
		
		return jedisInstance;
	}
	
	public void recommendByLocation ( Empreendimento empreendimento, List<Empreendimento> lstRecs ) { 
		for( java.util.Map.Entry<String, Empreendimento> entryEmp : empreendimento.getArea().getEdges().entrySet() ) {
			if( empreendimento.getSku().equals( entryEmp.getKey() ) ) continue;
			if( entryEmp.getValue().getType( ).getId() == empreendimento.getType().getId() )
				if( entryEmp.getValue().getTypeEmpreendimento().getId() == empreendimento.getTypeEmpreendimento().getId() ) {
					lstRecs.add( entryEmp.getValue() );
					if( lstRecs.size() == this.numRecs)
						break;
				}
		}
	}
		
	public void recommend () throws IOException {
		writer.initialize();
		this.poolInstance = new JedisConnectionFactory().size( this.numExecutors + 10 ).build();
		
		try { 
			for(int x = 0; x < numExecutors; x++ ) { 
				new Thread(new Runnable() {
					@Override
					public void run() {
						try { 
							Thread.currentThread().sleep( 2000 );
						} catch ( InterruptedException ex) {}
						
						
						try { 
							Jedis jedisInstance = poolInstance.getResource();
							
							while( true ) { 
								List<Empreendimento> lstRecs = new ArrayList<>();
								Empreendimento empreendimento = Recommender.this.queue.poll();
								if( empreendimento == null )
									break;
								try {
									jedisInstance = recommendWithScore(poolInstance, jedisInstance, empreendimento, lstRecs);
									if( lstRecs.size() > 0 )
										Recommender.this.writer.write(empreendimento, lstRecs.subList(0, lstRecs.size() < Recommender.this.numRecs + 1 ? lstRecs.size() : (Recommender.this.numRecs + 1) ) ); 
								} catch (IOException e) {
								}
							}
						} finally { 
							Recommender.this.reportExecutorComplete();						
						}					
					}
				}).start();
			}
			
			System.out.print("starting recommendations for [" + dataOrganizerInstance.getMapEmpreendimentos().size() + " products]: 0%..." );
			long s = System.currentTimeMillis();
			
			long total = dataOrganizerInstance.getMapEmpreendimentos().size();
			long partials = total / 20;
			long nextPartial = partials;
			long current = 0l;
			for( java.util.Map.Entry<String, Empreendimento> entryEmp : dataOrganizerInstance.getMapEmpreendimentos().entrySet() ) {
				try {
					this.queue.put( entryEmp.getValue() );
				} catch (InterruptedException e) {
					break;
				}
				if( ++current >= nextPartial ) { 
					System.out.print(  Math.round( ( (double)current / (double)total) * 100 ) + "%..." );
					nextPartial += partials;
				}
			}
			System.out.println("100% ("+ (System.currentTimeMillis() - s)+"ms)." );
			
			System.out.println("Waiting executors to die." );
			while( this.numExecutorsComplete < this.numExecutors ) {
				try { Thread.currentThread().sleep( 400 ); } catch ( InterruptedException ex) {}
			}
			System.out.println("All executors finished." );
		} finally { 
			try { if( poolInstance != null ) poolInstance.close(); } catch ( Exception exClose){}
		}
	}
	
	
	private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
      double theta = lon1 - lon2;
      double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
      dist = Math.acos(dist);
      dist = rad2deg(dist);
      dist = dist * 60 * 1.1515;
      if (unit == 'K') {
        dist = dist * 1.609344;
      } else if (unit == 'N') {
        dist = dist * 0.8684;
        }
      return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
      return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
      return (rad * 180.0 / Math.PI);
    }
	 
}
