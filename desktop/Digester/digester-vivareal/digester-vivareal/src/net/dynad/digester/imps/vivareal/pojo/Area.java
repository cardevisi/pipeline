package net.dynad.digester.imps.vivareal.pojo;

public class Area extends BaseAbs {
	private City city;
	public Area(long id, String name) {
		super(id, name);
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	public static String getAreKey( String strArea, City cityInstance ) { 
		return cityInstance.getId() + "." + strArea;
	}

}
