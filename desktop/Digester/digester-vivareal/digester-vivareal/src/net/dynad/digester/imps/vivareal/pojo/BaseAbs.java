package net.dynad.digester.imps.vivareal.pojo;

public abstract class BaseAbs {
	private java.util.Map<String, Empreendimento> mapEmpreendimentos;
	private long id;
	private String name;
	public BaseAbs(long id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.mapEmpreendimentos = new java.util.HashMap<>();
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	
	public void linkWithEmpreendimento( Empreendimento empreendimentInstance ) { 
		this.mapEmpreendimentos.put(empreendimentInstance.getSku(), empreendimentInstance);
	}
	
	public long sizeEdges( ) { 
		return this.mapEmpreendimentos.size();
	}
	
	public boolean hasEmpreendimento( String sku ) { 
		return this.mapEmpreendimentos.containsKey(sku);
	}
	
	public Empreendimento getEmpreendimento ( String sku ) { 
		return this.mapEmpreendimentos.get(sku);
	}
	
	public java.util.Map<String, Empreendimento> getEdges() {
		return this.mapEmpreendimentos;
	}
}
