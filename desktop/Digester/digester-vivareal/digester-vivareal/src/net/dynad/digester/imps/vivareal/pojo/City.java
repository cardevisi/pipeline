package net.dynad.digester.imps.vivareal.pojo;

public class City extends BaseAbs {
	private java.util.Map<String, Area> mapAreas;
	private Region region;
	
	public City(long id, String name) {
		super(id, name);
		this.mapAreas = new java.util.HashMap<>();
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
	
	public static String getCityKey( String strCity, Region regionInstance ) { 
		return regionInstance.getId() + "." + strCity;
	}
	
	public void createAreaEdge( Area areaInstance ) { 
		if( !this.mapAreas.containsKey(areaInstance.getName()) )
			this.mapAreas.put(areaInstance.getName(), areaInstance);
	}
	
	public long sizeAreaEdges( ) { 
		return this.mapAreas.size();
	}
	
	public java.util.Map<String, Area> getAreaEdges () { 
		return this.mapAreas;
	}
}
