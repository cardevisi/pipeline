package net.dynad.digester.imps.vivareal.pojo;

public class Empreendimento extends BaseAbs {
	private Type type;
	private TypeEmpreendimento typeEmpreendimento;
	private Area area;
	private String sku, description, link, image, neibor, image2;
	private double price;
	private double latitude, longitude;
	private transient long score;
	public Empreendimento(long id, String name) {
		super(id, name);
		this.score = -1l;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public TypeEmpreendimento getTypeEmpreendimento() {
		return typeEmpreendimento;
	}

	public void setTypeEmpreendimento(TypeEmpreendimento typeEmpreendimento) {
		this.typeEmpreendimento = typeEmpreendimento;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getNeibor() {
		return neibor;
	}

	public void setNeibor(String neibor) {
		this.neibor = neibor;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public void createEdges ( ) { 
		this.area.linkWithEmpreendimento(this);
		this.type.linkWithEmpreendimento(this);
		this.typeEmpreendimento.linkWithEmpreendimento(this);
		this.area.getCity().linkWithEmpreendimento(this);
		this.area.getCity().getRegion().linkWithEmpreendimento(this);
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}
	
	public double distance(double lat2, double lon2, char unit) {
	  double theta = this.getLongitude() - lon2;
	  double dist = Math.sin(deg2rad(this.getLatitude())) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(this.getLatitude())) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	  dist = Math.acos(dist);
	  dist = rad2deg(dist);
	  dist = dist * 60 * 1.1515;
	  if (unit == 'K') {
	    dist = dist * 1.609344;
	  } else if (unit == 'N') {
	    dist = dist * 0.8684;
	    }
	  return (dist);
	}
	
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double deg2rad(double deg) {
	  return (deg * Math.PI / 180.0);
	}
	
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double rad2deg(double rad) {
	  return (rad * 180.0 / Math.PI);
	}
}
