package net.dynad.digester.imps.vivareal.pojo;

public class Region extends BaseAbs {
	private java.util.Map<String, City> mapCities;
	
	public Region(long id, String name) {
		super(id, name);
		this.mapCities = new java.util.HashMap<>();
	}
	
	public void createCityEdge( City cityInstance ) { 
		if( !this.mapCities.containsKey(cityInstance.getName()) )
			this.mapCities.put(cityInstance.getName(), cityInstance);
	}
	
	public long sizeCityEdges( ) { 
		return this.mapCities.size();
	}
	
	public java.util.Map<String, City> getCityEdges () { 
		return this.mapCities;
	}
	
}
