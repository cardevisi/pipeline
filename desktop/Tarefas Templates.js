
Criação do Template Cutting Edge - HOME:

1) Criar casca do projeto, seguindo a mesma estrutura dos demais templates.
2) Criar classe Config com as propriedades do Cutting Edge.
3) Modificar os valores da task "replace" para automatizar a geração de novos templates.
4) Criar o arquivo init.js com as configurações de NameSpace e de inicialização do template.
5) Implementar a Classe CuttingEdge com toda a funcionalidade do template - seguir a mesma estrutura dos demais templates.
6) Implementar a escrita do template em modo Síncrono.
7) Implementar a escrita do template em modo Assícrono utilizando a Classe Appender.js


Template Mosaico, Template Glider, Template Sidekick, Template Viewport

1) Implementar funcionalidade de expansão por mouseover e clique.
	
	Funcionamento: Quando o usuário estiver com o mouse sobre a clickArea (mouseover) o banner deverá ser expandido depois de passados 1,5 segundos. Caso ele clique sobre o banner antes desse tempo terminar, o banner deverá ser expandido e a contagem de tempo deve ser interrompida. A clickArea, agora passa a ter 100% da dimensão do formato. Ex. Em um banner 728x90 a clickArea deverá ter as mesmas dimensões os mesmos 728x90.


Levantar uma forma dos templates não mais dependerem da bn.imguol.com (servidor) para armazenamento dos assets, imagens, scripts e html. A ideia é verificar a possibilidade de se utilizar o DFP para armazenamento completo das peças do cliente.

precisamos fazer com que esses templates não dependa de um outro servidor para que não seja o próprio DFP. Hoje estamos usando a  em modo de emergência, mas precisamos pensar esses templates sem esta dependência. – Este tem que ser o nosso próximo passo – Você alinha isso com o pessoal?