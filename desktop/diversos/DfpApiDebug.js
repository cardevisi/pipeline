(function(){
    function DfpApiDebug() {
        var length;
        
        try {
            length =  googletag.pubads().getSlots().length;
        } catch(e) {
            length = 0;
        }
        
        this.getAllUnitPathBySlot = function () {
            for(var i = 0; i < length; i++){
                try {
                    console.log(googletag.pubads().getSlots()[i].getAdUnitPath());
                } catch(e) {
                
                }
            }
        }
        this.getTargetingKeysBySlot = function () {
            for(var i = 0; i < length; i++){
                try {
                    console.log(googletag.pubads().getSlots()[i].getTargetingKeys());
                } catch(e) {
                
                }
            }
        }
        this.getSlotElementId = function () {
            for(var i = 0; i < length; i++){
                try {
                    console.log(googletag.pubads().getSlots()[i].getSlotElementId());
                } catch(e) {
                
                }
            }
        }
    }

    /*
        var length =  googletag.pubads().getSlots().length;
        for(var i = 0; i < length; i++){
            console.log(googletag.pubads().getSlots()[i].getTargetingKeys())
        }
    */

    var dfp = new DfpApiDebug();
    dfp.getAllUnitPathBySlot();
    dfp.getTargetingKeysBySlot();
    dfp.getSlotElementId();
    
})();