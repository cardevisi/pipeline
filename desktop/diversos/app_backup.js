(function(window, document, undefined){
/*
 * © UOL INTERFACE PROFILE ANALYSIS - Todos os direitos reservados 
 */ 
'use strict'; 
function AgeGenre() {
	
	var $public = this;
	var $private = {};
	var $static = $public.constructor.prototype;
		
	var female = [100,200,110,50,30,10,10];
	var male   = [90,160,110,70,40,30,10];
	var categories = ['13-17', '18-24', '25-34', '35-44', '45-54', '55-64', '65+'];
	var ARRAY_SIZE = categories.length;

    var dataMale   = male.slice();
    var dataFemale = female.slice();
    var totalValue  = 0;

   $public.create = function() {
		for(i = 0; i < ARRAY_SIZE; i++) {
			totalValue += (male[i] + female[i]);
		}

		for(i = 0; i < ARRAY_SIZE; i++) {
			dataMale[i] = ((male[i])/totalValue)*100*-1;
			dataFemale[i] = ((female[i])/totalValue)*100;
		}

		var minValue = Number.MAX_VALUE;
		var maxValue = Number.MIN_VALUE;

		for(i = 0; i < ARRAY_SIZE; i++) {
			if(minValue > dataMale[i]) {
				minValue = dataMale[i];
			}

			if(maxValue < dataFemale[i]) {
				maxValue = dataFemale[i];
			}
		}
	
		/*$(document).ready(function () {
			$('#container').highcharts();
		});*/

	};
}
function BarsChart() {

	var $public = this;
	var $private = {};
	var $static = $public.constructor.prototype;

	$public.showBarsChart = function(id){
		var randomScalingFactor = function() {return Math.round(Math.random()*100);};
		var barChartData = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					fillColor : "rgba(140,140,140,0.5)",
					strokeColor : "rgba(140,140,140,0.8)",
					highlightFill: "rgba(140,140,140,0.75)",
					highlightStroke: "rgba(140,140,140,1)",
					data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				},
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,0.8)",
					highlightFill : "rgba(151,187,205,0.75)",
					highlightStroke : "rgba(151,187,205,1)",
					data : [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
				}
			]
		};

		var options = {
			responsive: true
		};

		var ctx = document.getElementById(id).getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, options);

	};
}
function LinearDataChart () {
	
	var $public = this;
	var $private = {};
	var $static = $public.constructor.prototype;

	$public.showLinearData = function(id) {
		var data = {
			labels: [0,1,2,3,4,5,6,7,8,9,10,11, 13, 15],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(255,186,146,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40]
				},
				{
					label: "My Second dataset",
					fillColor: "rgba(2,70,230,0.2)",
					strokeColor: "rgba(151,187,205,1)",
					pointColor: "rgba(151,187,205,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(151,187,205,1)",
					data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86, 27, 90]
				}
			]
		};

		var options = {
			scaleShowGridLines : true,
			scaleShowHorizontalLines: true,
			scaleShowVerticalLines: true,
			bezierCurve : true,
			bezierCurveTension : 0.4,
			pointDot : true,
			pointDotRadius : 4,
			pointDotStrokeWidth : 1,
			pointHitDetectionRadius : 20,
			datasetStroke : false,
			datasetStrokeWidth : 2,
			datasetFill : true,
			responsive: true					
		};

		var ctx = document.getElementById(id).getContext("2d");
		var myLineChart = new Chart(ctx).Line(data, options);
	};
}
function PieChart() {
	
	var $public = this;
	var $private = {};
	var $static = $public.constructor.prototype;

	$public.showPie = function(id) {
		var data = [
			{
				value: 40,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Feminino"
			},
			{
				value: 20,
				color: "#337AB7",
				highlight: "#5AD3D1",
				label: "Masculino"
			},
			{
				value: 30,
				color: "#00ff00",
				highlight: "#00ff00",
				label: "Não identificados"
			}
		];

		var options = {
			animation: true,
			percentageInnerCutout:70,
			responsive : true,
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		};
	
		var ctx = document.getElementById(id).getContext("2d");
		window.myPieChart = new Chart(ctx).Pie(data, options);
		var legend = window.myPieChart.generateLegend();
		var setLegend = document.getElementById("legend-genre-sn-chart");
		setLegend.innerHTML = legend;
	};
}
function PieDoughnutChart() {

	var $private = {};
	var $public = this;
	var $static = $public.constructor.prototype;

	$public.create = function(id) {

		var quant = [1000, 2000, 5000];
		var template = "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%=segments[i].quant %><%}%></li><%}%></ul>";

		var data = {
			config: [
				{
					value: 5,
					color: "#1b1464",
					highlight: "#96b5b4",
					label: "Masculino",
					quant: 1000
				},
				{
					value: 50,
					color: "#337ab7",
					highlight: "#96b5b4",
					label: "Feminino",
					quant: 1000
				},
				{
					value: 45,
					color: "#707070",
					highlight: "#96b5b4",
					label: "Não idenficados",
					quant: 1000
				}
			],
			options: {
				tooltipTemplate : "<%if (label){%><%=label%>: <%}%><%= value %>%",
				percentageInnerCutout: 0,
				responsive : true,
				legendTemplate : template
			}
		};

		var ctx = document.getElementById(id).getContext("2d");
		window.myPieChartDoughnut = new Chart(ctx).Doughnut(data.config, data.options);
		
		var legend = window.myPieChartDoughnut.generateLegend();
		var setLegend = document.getElementById("legend-genre-sn-chart");
		setLegend.innerHTML = legend;
				
	};
}
function ProfileAnalysis() {

	var $public = this;
	var $private = {};
	var $static = $public.constructor.prototype;

	$public.calculateGenreSnVisitors = function(data){
		
		var USERS_DATA = data;
		var TOTAL_ROWS = USERS_DATA.length;
		var GENDER_ARRAY = [];
		var UNIQUE__VISITORS_ARRAY = [];
		var TOTAL_USERS_BY_GENDER = {};

		for (var i = 0; i < TOTAL_ROWS; i++) {
			GENDER_ARRAY.push(USERS_DATA[i].gender_sn);
		};

		var GENDER_ARRAY = _.uniq(GENDER_ARRAY);
		
		var currentGenre = 0;
		
		for (var j = 0; j < GENDER_ARRAY.length; j++) {
			console.log(GENDER_ARRAY[j]);
			
			TOTAL_USERS_BY_GENDER[GENDER_ARRAY[j]] = [];
			//if (GENDER_ARRAY[j] === currentGenre) {
			var sum = 0;
			for (var k = 0; k < TOTAL_ROWS; k++) {
				if (USERS_DATA[k].gender_sn === GENDER_ARRAY[j]) {
					sum += USERS_DATA[k].unique_visitors;
				}
			};
			
			TOTAL_USERS_BY_GENDER[currentGenre] = sum;
			//}
		};

		console.log("total", sum, TOTAL_USERS_BY_GENDER);

		//
		//var uniqueVisitors = USERS_DATA.unique_visitors;
		
		//var map = _.map(data, index, function(num){ consol.log(index); return num += data.unique_visitors[index]; });
		
		for (var i = 0; i < TOTAL_ROWS; i++) {

			//console.log("getvalues", genderSnUniq[i]);

			/*if (genderSnUniq[i] === 101 || genderSnUniq[i] === 102) {
				return;
			}*/

			/*_.each(USERS_DATA.gender_sn, function (index, num){
				if (genderSn[index] === 0) {
					console.log(num, index);
				}
				console.log("getvalues", genderSnUniq[i], index, num);
				
				

			});*/
		}
		
	};
	
	$public.addData = function(id) {
		var ctx = document.getElementById(id);
		window.myPieChart.addData({
			value: 30,
			color: "#2b2b2b",
			highlight: "#C69CBE",
			label: "Nao identificados"
		});

		var legend = window.myPieChart.generateLegend();
		var setLegend = document.getElementById("legend-genre-sn-chart");
		setLegend.innerHTML = legend;
	};

	$public.removeData = function(id) {
		var ctx = document.getElementById(id);
		window.myPieChart.removeData();
	};
	
	$public.init = function(data) {
		
		$private.csvData = data;

		$("#openForm").click(function(event) {
			$(".navform").css("display", "block");
			$(".navform").css("top", "0");
			//$(".navform").animate({top: "+=0"}, { duration: 5000, specialEasing: {top: "easeOutBounce"}},function() {console.log('console');});
			/*$(".navform").animate({
				top: "+=0",
			}, 5000, function() {
			// Animation complete.
			});*/
		});
		
		var chart = new PieDoughnutChart();
		chart.create("canvas-genre-sn");
		//$public.showLinearData("canvas-events-diary-genre");

		$public.calculateGenreSnVisitors($private.csvData);
	};
}

window.UOLPD = window.UOLPD || {};
window.UOLPD.ProfileAnalysis = window.UOLPD.ProfileAnalysis || {};
window.UOLPD.ProfileAnalysis.csv_data = [
			{
				"dt":20150101,
				"gender_sn":0,
				"age_sn":0,
				"gender_uol":0,
				"age_uol":0,
				"page_views":23358,
				"unique_visitors":20000
			},
			{
				"dt":20150101,
				"gender_sn":101,
				"age_sn":0,
				"gender_uol":0,
				"age_uol":104,
				"page_views":2,
				"unique_visitors":101101
			},
			{
				"dt":20150101,
				"gender_sn":0,
				"age_sn":0,
				"gender_uol":0,
				"age_uol":105,
				"page_views":8,
				"unique_visitors":8
			},
			{
				"dt":20150101,
				"gender_sn":0,
				"age_sn":0,
				"gender_uol":0,
				"age_uol":105,
				"page_views":8,
				"unique_visitors":400
			},
			{
				"dt":20150101,
				"gender_sn":102,
				"age_sn":0,
				"gender_uol":0,
				"age_uol":105,
				"page_views":8,
				"unique_visitors":102102
			}
];

window.onload = function() {
	var showCharts = new ProfileAnalysis();
	showCharts.init(window.UOLPD.ProfileAnalysis.csv_data);
};


})(this, document);