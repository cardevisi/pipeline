[DAL-272] - TAB - Criar classe tracking de video


Essa história consiste em criar uma classe de tracking para o video a partir da classe de tracking principal

Parametros da classe:

VideoTracking extends Tracking
{
    trackingEvents : {},
    trackingName: String,
    createTrackingImage: function () {},
    video: HTMLVideoElement
}


Cenário: Gerar uma nova instância com atributos inválidos
Resultado: interrompe a execução e a chamada de tracking não pode existir

Cenário: Gerar uma instância sem o atributo trackingEvents
Resultado: interrompe a execução

Cenário: Gerar uma instância com atributo trackingEvents diferente de objeto
Resultado: interrompe a execução

Cenário: Gerar uma instância com atributo trackingEvents inválido
Resultado: interrompe a execução e loga um erro

Cenário: Gerar uma instância sem o atributo trackingName
Resultado: interrompe a execução

Cenário: Gerar uma instância com o atributo trackingName diferente de String
Resultado: interrompe a execução

Cenário: Gerar uma instancia com o atributo trackingName inválido
Resultado: interrompe a execução e loga um erro

Cenário: Gerar uma instância com o atributo createTrackingImage
Resultado: interrompe a execução

Cenário: Gerar uma instancia com o atributo createTrackingImage diferente de Function 
Resultado: interrompe a execução 

Cenário: Gerar uma nova instância com atributo createTrackingImage inválido
Resultado: interrompe a execução e loga um erro

Cenário: Gerar uma instância com o atributo video inválido
Resultado: interrompe a execução e loga um erro

Cenário: Gerar uma instância com o atributo video diferente de HTMLVideoElement
Resultado: interrompe a execução e loga um erro

Cenário: Gerar uma instância sem o atributo video
Resultado: interrompe a execução 
