[DAL-271] TAB - Criar classe tracking principal


Essa história consiste em criar uma classe principal com as características dos trackings recebidos no request dos banners

Essa história consiste em criar uma classe principal com as características dos trackings recebidos no request dos banners

Parâmetros da classe
{
    trackingEvents : {},
    trackingName: String,
    createTrackingImage: function (trackingEvents, trackingName + "-tipo_de_tracking") {}
}


Cenário: Gerar uma nova instância com atributos válidos
Resultado: deve retornar um objeto do tipo Tracking

Cenário: Realizar tracking de start
Resultado: É criado um log no histórico indicando a realização do tracking de start

Cenário: Realizar tracking de pause
Resultado: É criado um log no histórico indicando a realização do tracking de pause

Cenário: Realizar tracking de resume
Resultado: É criado um log no histórico indicando a realização do tracking de pause e depois de resume

Cenário: Realizar tracking de complete
Resultado: É criado um log no histórico indicando a realização do tracking de complete

Cenário: Realizar tracking de firstQuartle
Resultado: É criado um log no histórico indicando a realização do tracking de firstQuartle

Cenário: Realizar tracking de midPoint
Resultado: É criado um log no histórico indicando a realização do tracking de midPoint

Cenário: Realizar tracking de thirdQuartle
Resultado: É criado um log no histórico indicando a realização do tracking de thirdQuartle



Cenário: Gerar uma instância sem o atributo trackingEvents
Resultado: interrompe a execução

Cenário: Gerar uma instância com atributo trackingEvents diferente de objeto
Resultado: interrompe a execução

Cenário: Gerar uma instância com atributo trackingEvents inválido
Resultado: interrompe a execução e loga um erro


Cenário: Gerar uma instância sem o atributo trackingName
Resultado: interrompe a execução

Cenário: Gerar uma instância com o atributo trackingName diferente de String
Resultado: interrompe a execução

Cenário: Gerar uma instancia com o atributo trackingName inválido
Resultado: interrompe a execução e loga um erro


Cenário: Gerar uma instância sem o atributo createTrackingImage
Resultado: interrompe a execução

Cenário: Gerar uma instancia com o atributo createTrackingImage diferente de Function 
Resultado: interrompe a execução 

Cenário: Gerar uma nova instância com atributo createTrackingImage inválido
Resultado: interrompe a execução e loga um erro



