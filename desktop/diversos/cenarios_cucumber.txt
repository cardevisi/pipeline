Caso não exista o slot 
Então não realiza o setTargeting 

Caso não exista a variável rtp[banner.id] 
Então deve buscar banner.dfpPos 

Caso exista a variável rtp[banner.id] 
E ela não seja uma string 
Então deve buscar banner.dfpPos 

Caso rtp[banner.id] esteja inválido 
E não exista banner.dfpPos 
Então realiza o setTargeting com uma string vazia 

Caso rtp[banner.id] esteja inválido 
E exista banner.dfpPos 
E banner.dfpPos não seja uma string 
Então realiza o setTargeting com uma string vazia 

Caso exista a variável rtp[banner.id] 
E seja uma string 
Então realiza o setTargeting com banner.id 

Caso rtp[banner.id] esteja inválido 
E exista banner.dfpPos 
E seja uma string 
Então realiza o setTargeting com banner.dfpPos