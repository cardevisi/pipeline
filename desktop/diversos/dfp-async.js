(function (window, document, undefined){
'use strict';
function Logs ($super) {
    var $private = {};
    var $protected = this;
    var $public = $protected.constructor.prototype;

    $private.prefix = '[UOLPD.TagManager.DfpAsync] ';
    $public.history = {};

    $private.getQueryString = function(name, query) {
        if (name === undefined) {
            return;
        }
        if (typeof name !== 'string') {
            return;
        }

        if (typeof query !== 'undefined' && typeof query !== 'string') {
            return;
        }

        var search = query || document.location.search;
        var length = search.length;
        var value;

        search = search.split(/\?|\&/);
        length = search.length;
        for (var i = 0; i < length; i++) {
            if (search[i].split('=')[0] == name) {
                value = search[i].split('=')[1];
                break;
            }
        }
        return value;
    };

    $private.isActive = function (query) {
        return ($private.getQueryString('tm', query) == 'debug' ? true : false);
    };

    $private.setHistory = function (fn, msg) {
        if (!fn || !msg) {
            return;
        }

        if ((typeof fn) !== 'string' || (typeof msg) !== 'string') {
            return;
        }

        $public.history[fn] = $public.history[fn] || [];
        $public.history[fn].push(msg);
    };

    $private.trim = function(string) {
        if (string === undefined) {
            return;
        }
        if (typeof string !== 'string') {
            return;
        }

        string = string.replace(/^(\s+)|(\s+)$/gm, '');
        string = string.replace(/\s+/gm, ' ');
        if (string.length === 0) {
            return;
        }
        return string;
    };

    $private.formatMessage = function(msg) {
        msg = $private.trim(msg);
        if (!msg || !$private.prefix) {
            return;
        }
        msg = $private.prefix + msg;
        return msg;
    };

    $private.validateFunction = function(fn, msg) {
        if (!fn) {
            return false;
        }

        if (!window.console || !console){
            $private.setHistory(fn, msg);
            return false;
        }

        if (!console[fn] && fn == 'log') {
            $private.setHistory(fn, msg);
            return false;
        }

        if (!fn || !console[fn]) {
            $private.setHistory(fn, msg);
            fn = "log";
        }

        return fn;
    };

    $private.print = function(msg, fn, isActive) {
        if (msg === undefined || fn === undefined) {
            return;
        }

        if (typeof msg !== 'string' || typeof fn !== 'string') {
            return;
        }

        msg = $private.formatMessage(msg);
        if (!msg) {
            return;
        }

        if (isActive === undefined) {
            isActive = $private.isActive();
        }

        if (typeof isActive !== 'boolean') {
            return;
        }

        if (isActive === false) {
            $private.setHistory(fn, msg);
            return;
        }

        fn = $private.validateFunction(fn, msg);
        if (!fn) {
            return;
        }

        if(console[fn]){
            console[fn](msg);
        }

        $private.setHistory(fn, msg);
        return msg;
    };

    $public.warn = function(msg, isActive) {
        return $private.print(msg, 'warn', isActive);
    };

    $public.error = function(msg, isActive) {
        return $private.print(msg, 'error', isActive);
    };

    $public.info = function(msg, isActive) {
        return $private.print(msg, 'info', isActive);
    };

    $public.debug = function(msg, isActive) {
        return $private.print(msg, 'debug', isActive);
    };

    $public.log = function(msg, isActive) {
        return $private.print(msg, 'log', isActive);
    };

    $public.getInnerMethods = function(method) {
        return $private[method];
    };

    return $protected;
}
function TypeValidator () {
    var $public = this;
    var $private = {};

    $public.isDefined = function (value) {
        return value !== undefined && value !== null;
    };

    $public.isString = function (value) {
        return value !== undefined && (typeof value) === 'string' && ($private.stringIsNotEmpty(value));
    };

    $public.isArray = function (value) {
        return value !== undefined && value.constructor === Array;
    };

    $public.isObject = function (entity) {
        return entity && entity.constructor === Object;
    };

    $public.isFunction = function (value) {
        return value !== undefined && value.constructor === Function;
    };

    $public.isNumber = function (value) {
        return Number(value) === value;
    };

    $public.isInt = function (value) {
        return $public.isNumber(value) && value % 1 === 0;
    };

    $public.isRegExp = function (value) {
        return value !== undefined && value.constructor === RegExp;
    };

    $public.isNumericString = function (value) {
        return $public.isString(value) && /^\d+$/.test(value);
    };

    $private.stringIsNotEmpty = function(string) {
        if (string === undefined) {
            return false;
        }
        if (typeof string !== 'string') {
            return false;
        }

        string = string.replace(/^(\s+)|(\s+)$/gm, '');
        string = string.replace(/\s+/gm, ' ');
        if (string.length === 0) {
            return false;
        }
        return true;
    };

    return $public;
}

function TrackManager () {
    var $private = {};
    var $public = this;

    $private.typeValidator = new TypeValidator();
    $public.imagesSrc = [];
    $private.baseUrl = '//clicklogger.rm.uol.com.br/splunk/?prd=98&grp=Origem:TM-dfp-async;';

    $private.raffledRate = Math.round(Math.random() * 100);
    $private.samplingRate = 1;

    $public.trackSuccess = function (msr) {
        var measure = $private.getMeasure(msr);
        $public.sendPixel($private.getPixelSrc(measure), 1);
    };

    $private.getMeasure = function (msr) {
        if (!$private.typeValidator.isString(msr)) {
            return;
        }
        return '&msr=' + encodeURIComponent(msr) + ':1';
    };

    $private.getPixelSrc = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }

        var repoId = $public.getRepoId();
        if (!$private.typeValidator.isString(repoId)) {
            return;
        }

        return $private.baseUrl + 'tm_repo_id=' + repoId + src + '&oper=1';
    };

    $public.getRepoId = function () {
        if (!$private.repoId) {
            $private.setRepoId();
        }

        return $private.repoId;
    };

    $private.setRepoId = function() {
        var src = $private.getScriptSrc();

        if (!src) {
            return;
        }

        var srcMatch = src.match(/uoltm.js\?id=(.{6}).*/);
        var repoId = srcMatch ? srcMatch[1] : null;

        $private.repoId = repoId;
    };

    $private.getScriptSrc = function() {
        var script = document.querySelector('script[src*="tm.jsuol.com.br/uoltm.js"]');
        var src = script ? script.src : null;
        return src;
    };

    $public.sendPixel = function (src, samplingRate) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }

        if (!$private.isTrackEnabled(samplingRate)) {
            return;
        }

        var img = document.createElement('img');
        img.setAttribute('src', src);

        $public.imagesSrc.push(img.src);
    };

    $private.isTrackEnabled = function (samplingRate) {
        if (window.localStorage.getItem('trackManager') == 'true') {
            return true;
        }

        if ($public.getRaffledRate() <= samplingRate) {
            return true;
        }

        return false;
    };

    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };

    $public.trackError = function (errorType, errorEffect) {
        errorType = $private.getErrorType(errorType);
        if (!errorType) {
            return;
        }

        errorEffect = $private.getErrorEffect(errorEffect);
        if (!errorEffect) {
            return;
        }

        var url = $private.getPixelSrc(errorType + errorEffect + $private.getMeasure('Erros'));
        $public.sendPixel(url, 100);
    };

    $private.getErrorType = function (errorType) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }

        return ';erro_tipo:' + errorType;
    };

    $private.getErrorEffect = function (errorEffect) {
        if (!$private.typeValidator.isString(errorEffect)) {
            return;
        }

        return ';erro_efeito:' + errorEffect;
    };
}

function NameSpace (packageName) {
    var $private = {};
    var $public = this;
    var $static = $public.constructor.prototype;

    $private.version = '${project.version}';
    $private.validator = new TypeValidator();

    $static.init = function (packageName) {
        if ($private.validator.isString(packageName)) {
            return $static.create(packageName);
        }
    };

    $static.create = function (packageName) {
        $private.createUOLPD();
        $private.createTagManager();
        return $private.createPackage(packageName);
    };

    $private.createUOLPD = function () {
        if (!$private.validator.isObject(window.UOLPD)) {
            window.UOLPD = {};
        }
    };

    $private.createTagManager = function () {
        if (!$private.validator.isObject(UOLPD.TagManager) && !$private.validator.isFunction(UOLPD.TagManager) && typeof UOLPD.TagManager !== "object") {
            UOLPD.TagManager = {};
        }
    };

    $private.createPackage = function (packageName) {
        if (!$private.validator.isObject(UOLPD.TagManager[packageName])) {
            UOLPD.TagManager[packageName] = {};
        }

        UOLPD.TagManager[packageName].version = $private.version;

        if (!$private.validator.isArray(UOLPD.TagManager[packageName].config)) {
            UOLPD.TagManager[packageName].config = [];
        }

        if (!$private.validator.isObject(UOLPD.TagManager[packageName].log)) {
            UOLPD.TagManager[packageName].log = new Logs();
        }

        return UOLPD.TagManager[packageName];
    };

    return $static.init(packageName);
}
function CookieUtils () {

    var $public = this;
    var $private = {};

    $private.validator = new TypeValidator();

    $public.getItem = function (name) {
        if (!$private.validator.isString(name)) {
            return '';
        }

        var cookie = $private.getCookies();
        var startIndex = cookie.indexOf(name + '=');

        if (startIndex === -1) {
            return '';
        }

        var middleIndex = cookie.indexOf('=', startIndex) + 1;
        var endIndex = cookie.indexOf(';', middleIndex);

        if (endIndex === -1) {
            endIndex = cookie.length;
        }

        return unescape(cookie.substring(middleIndex, endIndex));
    };

    $private.getCookies = function () {
        return document.cookie;
    };

    $public.setItem = function (name, value) {
        if (!$private.validator.isString(name)) {
            return;
        }

        if (!$private.validator.isString(value)) {
            return;
        }

        $private.setCookie(name + "=" + value);
    };

    $private.setCookie = function (value) {
        document.cookie = value;
    };
}

function BehaviouralTargeting () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();
    $private.cookieUtils = new CookieUtils();

    $public.getBTCookies = function () {
        var rtCodes = $private.getMergeCookies();
        var dntTrack = $public.getDoNotTrack();

        rtCodes.push(dntTrack);

        return rtCodes;
    };

    $public.getDoNotTrack = function () {
        var dntCode = 9000;

        if (!navigator.doNotTrack && !window.doNotTrack && !navigator.MsDoNotTrack) {
            dntCode = 9000;
            return dntCode;
        }

        if (navigator.doNotTrack === '1' || window.doNotTrack === '1' || navigator.MsDoNotTrack === '1') {
            dntCode = 9001;
            return dntCode;
        }

        if (navigator.doNotTrack === 'yes') {
            dntCode = 9003;
            return dntCode;
        }

        return dntCode;
    };

    $private.getMergeCookies = function () {
        var BTCookie = $private.getClientsArray($private.cookieUtils.getItem('BT'));
        var DECookie = $private.getClientsArray($private.cookieUtils.getItem('DEretargeting'));
        var retargeting = $private.mergeCookies(BTCookie, DECookie);

        if (!$private.validator.isArray(retargeting)) {
            return [];
        }

        return retargeting;
    };

    $private.getClientsArray = function (cookie) {
        if (!$private.validator.isString(cookie)) {
            return [];
        }

        if (cookie.length === 0) {
            return [];
        }

        var retargeting = cookie.split(';').sort();

        for (var i = 0; i < retargeting.length; i++) {
            var arr = retargeting[0];
            if (arr === '') {
                retargeting.splice(0, 1);
            }
        }

        return retargeting;
    };

    $private.mergeCookies = function (deRetar, bt) {
        var retargeting = deRetar.concat(bt).sort();
        for (var i = 0; i < retargeting.length; i++) {
            if (retargeting[i] === retargeting[i + 1]) {
                retargeting.splice(i, 1);
            }
        }
        return retargeting;
    };
}

function ScriptUtils () {

    var $private = {};
    var $public = this;

    $private.typeValidator = new TypeValidator();

    $public.hasTagScript = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return false;
        }
        if (src.length === 0) {
            return false;
        }

        src = $private.removeProtocol(src);

        var scripts = document.getElementsByTagName('script');
        var length = scripts.length;
        for (var i = 0; i < length; i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if (scriptSrc && $private.isSrcEqual(scriptSrc, src)) {
                return true;
            }
        }

        return false;
    };

    $private.removeProtocol = function (value) {
        return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };

    $private.isSrcEqual = function (src1, src2) {
        return (src1.indexOf(src2) > -1);
    };

    $public.createAsyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        tag.async = true;
        return tag;
    };

    $public.appendScript = function (script) {
        if (script instanceof HTMLScriptElement) {
            $private.lastScriptsParent().appendChild(script);
            return true;
        }
        return false;
    };

    $private.lastScriptsParent = function () {
        return document.getElementsByTagName('script')[0].parentNode;
    };

}
function Tailtarget () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();

    $public.getProfiles = function () {
        var profiles = {};
        var data = $private.getData();
        if (!data) {
            return profiles;
        }

        for (var key in data) {
            var attributeName = $private.getAttributeName(key);
            if (!attributeName) {
                continue;
            }
            var value = $private.getValue(data[key]);
            if (!value) {
                continue;
            }
            profiles[attributeName] = value;
        }

        return profiles;
    };

    $private.getData = function () {
        try {
            return JSON.parse(window.localStorage.getItem('tailtarget'));
        } catch (e) {
            return;
        }
    };

    $private.getValue = function (value) {
        if ($private.validator.isArray(value)) {
            value = value.join(', ');
        }

        if (!$private.validator.isString(value)) {
            return;
        }

        return value;
    };

    $private.getAttributeName = function (key) {
        if (!$private.validator.isString(key)) {
            return;
        }

        if (key == 'getProfiles') {
            return 'tt_cluster';
        }

        return 'tt_' + key.replace('get', '').toLowerCase();
    };
}

function Rubicon () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();

    $public.setRtpBanner = function (bannerId) {
        if (!$private.hasRtp(bannerId)) {
            return;
        }

        $private.bannerId = bannerId;
        $private.rtpBanner = window.rtp[bannerId];
    };

    $private.hasRtp = function (bannerId) {
        if (!$private.validator.isString(bannerId)) {
            return false;
        }

        if (!$private.validator.isObject(window.rtp)) {
            return false;
        }

        if (!$private.validator.isObject(window.rtp[bannerId])) {
            return false;
        }

        return true;
    };

    $public.getDeals = function () {
        if (!$private.rtpBanner) {
            return;
        }

        if (!$private.rtpBanner.pmp) {
            return;
        }

        if (!$private.validator.isArray($private.rtpBanner.pmp.deals)) {
            return;
        }

        if ($private.rtpBanner.pmp.deals.length === 0) {
            return;
        }

        return $private.rtpBanner.pmp.deals.join(', ');
    };

    $public.getCpm = function (bannerId) {
        if (!$private.hasValidEstimate()) {
            return;
        }

        if (!$private.validator.isNumber($private.rtpBanner.estimate.cpm)) {
            return;
        }

        return $private.rtpBanner.estimate.cpm;
    };

    $private.hasValidEstimate = function () {
        if (!$private.rtpBanner) {
            return false;
        }

        if (!$private.estimate) {
            return false;
        }

        if (!$private.validator.isObject(rtpBanner.estimate)) {
            return;
        }

        return true;
    };

    $public.getTier = function () {
        if (!$private.hasValidEstimate()) {
            return;
        }

        if(!$private.validator.isString($private.rtpBanner.estimate.tier)) {
            return;
        }

        return $private.rtpBanner.estimate.tier;
    };

}

function Sesame() {

    var $private = {};
    var $public = this;
    $private.scriptUtils = new ScriptUtils();
    $private.googleTopUrl = '//pagead2.googlesyndication.com/pagead/js/google_top.js';

    $public.createTagScript = function() {
        var script = document.createElement('script');
        script.setAttribute('src', $private.googleTopUrl);
        return script;
    };

    $public.createIframe = function(){
        var iframe = document.createElement('iframe');
        iframe.setAttribute('data-video-ads', 'true');
        iframe.setAttribute('id' , 'google_top');
        iframe.setAttribute('name' , 'google_top');
        iframe.setAttribute('style' , 'display:none;');
        return iframe;
    };

    $public.appendElement = function(element) {
        if(!element) {
            return;
        }
        if(!$private.hasIframe('iframe[id=google_top]')) {
            document.body.appendChild(element);
        }
    };

    $private.hasIframe = function(query) {
        var result = document.querySelectorAll(query);
        if (result.length === 1) {
            return true;
        }
        return false;
    };

    $public.track = function() {
        if (!$private.scriptUtils.hasTagScript($private.googleTopUrl)) {
            $private.scriptUtils.appendScript($public.createTagScript());
        }
        $public.appendElement($public.createIframe());
    };

}
function DfpPath () {
    var $public = this;
    var $private = {};

    $public.logger = new Logs();
    $private.validator = new TypeValidator();

    $public.setDfpPath = function (dfpPath) {
            console.log("DFPPATH", dfpPath, $private.dfpPath);
        if ($private.validator.isString(dfpPath)) {
            $private.dfpPath = $private.getPathElementByPriority($private.trimDfpPathSlashes(dfpPath));
        }
    };

    $private.trimDfpPathSlashes = function (dfpPath) {
        if (dfpPath.substring(0, 1) === '/') {
            dfpPath = dfpPath.substring(1, dfpPath.length);
        }

        if (dfpPath.substring(dfpPath.length - 1, dfpPath.length) === '/') {
            dfpPath = dfpPath.substring(0, dfpPath.length - 1);
        }

        return dfpPath.split('/');
    };

    $private.getPathElementByPriority = function (path) {
        $public.setNetwork(path[0]);
        var network = $public.getNetwork();
        if (!network) {
            return path;
        }
        path[0] = network;

        $public.setSite(path[1]);
        var site = $public.getSite();
        if (!site) {
            return path.join('/');
        }
        path[1] = site;

        $public.setChan(path[2]);
        var chan = $public.getChan();
        if (!chan) {
            return path.join('/');
        }
        path[2] = chan;

        $public.setSubChan(path[3]);
        var subchan = $public.getSubChan();
        if (!subchan) {
            return path.join('/');
        }
        path[3] = subchan;

        return path.join('/');
    };

    $public.setNetwork = function (network) {
        if ($private.validator.isString(network)) {
            $private.network = network;
        }
    };

    $public.getNetwork = function () {
        return $private.network;
    };

    $public.setSite = function (site) {
        if ($private.isValidSite()) {
            $private.site = window.universal_variable.dfp.custom_params.site;
        }

        if ($private.validator.isString(site)) {
            $private.site = site;
        }
    };

    $private.isValidSite = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }

        var site = window.universal_variable.dfp.custom_params.site;
        if (!$private.validator.isString(site)) {
            $public.logger.warn('O parâmetro "chan" não é uma string.');
            return false;
        }

        return true;
    };

    $public.getSite = function (site) {
        return $private.site;
    };

    $public.setChan = function (chan) {
        if ($private.isValidChan()) {
            $private.chan = window.universal_variable.dfp.custom_params.chan;
        }

        if ($private.validator.isString(chan)) {
            $private.chan = chan;
        }
    };

    $private.isValidChan = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }

        var chan = window.universal_variable.dfp.custom_params.chan;
        if (!$private.validator.isString(chan)) {
            $public.logger.warn('O parâmetro "chan" não é uma string.');
            return false;
        }

        return true;
    };

    $public.getChan = function () {
        return $private.chan;
    };

    $public.setSubChan = function (subchan) {
        if ($private.isValidSubchan()) {
            $private.subChan = window.universal_variable.dfp.custom_params.subchan;
        }

        if ($private.validator.isString(subchan)) {
            $private.subChan = subchan;
        }
    };

    $private.isValidSubchan = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }

        var subchan = window.universal_variable.dfp.custom_params.subchan;
        if (!$private.validator.isString(subchan)) {
            $public.logger.warn('O parâmetro subchan não é uma string.');
            return false;
        }

        return true;
    };

    $private.isValidCustomParams = function () {
        var universalVariable = window.universal_variable;

        if (!$private.validator.isDefined(universalVariable)) {
            return false;
        }

        if (!$private.validator.isObject(universalVariable)) {
            $public.logger.warn('A variável universal não é um objeto.');
            return false;
        }

        if (!$private.validator.isObject(universalVariable.dfp)) {
            $public.logger.warn('O parâmetro "dfp" da variável universal não é um objeto.');
            return false;
        }

        if (!$private.validator.isObject(universalVariable.dfp.custom_params)) {
            $public.logger.warn('O parâmetro "custom_params", do parâmetro "dfp", da variável universal, não é um objeto.');
            return false;
        }

        return true;
    };

    $public.getSubChan = function () {
        return $private.subChan;
    };

    $public.getDfpPath = function () {
        return $private.dfpPath;
    };
}

function DfpApi () {

    var $private = {};
    var $public = this;

    $private.scriptUtils = new ScriptUtils();
    $private.validator = new TypeValidator();
    $private.tailtarget = new Tailtarget();
    $private.behaviouralTargeting = new BehaviouralTargeting();
    $private.trackManager = new TrackManager();
    $public.logger = new Logs();

    $public.appendGPT = function () {
        var gptUrl = '//www.googletagservices.com/tag/js/gpt.js';
        if ($private.scriptUtils.hasTagScript(gptUrl)) {
            return;
        }
        var script = $private.scriptUtils.createAsyncScript(gptUrl);
        $private.scriptUtils.appendScript(script);
        return script;
    };

    $public.createNameSpace = function () {
        window.googletag = window.googletag || {};
        window.googletag.cmd = window.googletag.cmd || [];
    };

    $public.onDfpReady = function (callback, timeout) {
        timeout = timeout || 50;

        if (!$private.validator.isFunction(callback)) {
            return false;
        }

        if ($private.isGoogleTagReady()) {
            callback();
            return true;
        }

        setTimeout(function() {
            $public.onDfpReady(callback, timeout);
        }, timeout);
    };

    $public.setTargetings = function (slot, targetings) {
        if (!slot) {
            return;
        }

        if (!$private.validator.isArray(targetings)) {
            return;
        }
        for (var i = 0, length = targetings.length; i < length; i++) {
            var targeting = targetings[i];
            try {
                slot.setTargeting(targeting.key, targeting.value);
            } catch (e) {
                continue;
            }
        }
        return slot;
    };

    $public.getCampaignuol = function (campaignuol) {
        if (campaignuol !== '0' && campaignuol !== '1') {
            return;
        }

        return campaignuol;
    };

    $public.getExpandable = function (expble) {
        if (expble !== '0' && expble !== '1') {
            return;
        }

        return expble;
    };

    $public.getGroup = function (group) {
        if (!$private.validator.isString(group)) {
            return;
        }
        return group;
    };

    $public.getKeyword = function (slot, keyword) {
        if (!$private.validator.isString(keyword)) {
            keyword = $private.getKeywordFromUniversalVariable();
        }

        if (!$private.validator.isString(keyword)) {
            return;
        }

        return keyword;
    };

    $private.getKeywordFromUniversalVariable = function () {
        if (!$private.isValidCustomParams()) {
            return;
        }

        return window.universal_variable.dfp.custom_params.keyword;
    };

    $private.isValidCustomParams = function () {
        var universalVariable = window.universal_variable;

        if (!$private.validator.isDefined(universalVariable)) {
            return false;
        }

        if (!$private.validator.isObject(universalVariable)) {
            $public.logger.warn('A variável universal não é um objeto.');
            return false;
        }

        if (!$private.validator.isObject(universalVariable.dfp)) {
            $public.logger.warn('O parâmetro "dfp" da variável universal não é um objeto.');
            return false;
        }

        if (!$private.validator.isObject(universalVariable.dfp.custom_params)) {
            $public.logger.warn('O parâmetro "custom_params", do parâmetro "dfp", da variável universal, não é um objeto.');
            return false;
        }

        return true;
    };

    $public.hasTrackedPubads = function () {
        return $private.hasTrackedPubads;
    };

    $public.setPubadsTracking = function (targetings) {
        if (!$private.validator.isArray(targetings)) {
            return;
        }

        $private.hasTrackedPubads = true;
        for (var i = 0, length = targetings.length; i < length; i++) {
            var targeting = targetings[i];
            try {
                window.googletag.pubads().setTargeting(targeting.key, targeting.value);
            } catch (e) {
                continue;
            }
        }
    };

    $public.getGwd = function (placement) {
        if (!$private.validator.isString(placement)) {
            return;
        }

        return placement;
    };

    $private.trackError = function (errorType, errorMessage) {
        if (!$private.validator.isString(errorType)) {
            return;
        }
        if (!$private.validator.isString(errorMessage)) {
            return;
        }

        $public.logger.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };

    $public.defineSlot = function (path, id, width, height, outOfPage) {
        if (!$private.validator.isString(path)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }

        if (!$private.validator.isString(id)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }

        if (outOfPage === true) {
            return window.googletag.defineOutOfPageSlot(path, id);
        }

        if (!$private.validator.isNumericString(width)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }

        if (!$private.validator.isNumericString(height)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }

        try {
            return window.googletag.defineSlot(path, [parseInt(width), parseInt(height)], id);
        } catch (e) {
            return;
        }
    };

    $public.setCompanionAds = function(slot, isCompanion) {
        if (!slot) {
            return;
        }

        if (!isCompanion) {
            return slot;
        }

        try {
            return slot.addService(window.googletag.companionAds());
        } catch (e) {
            return;
        }
    };

    $public.addServices = function (slot) {
        if (!slot) {
            return;
        }

        if (!slot.addService) {
            return;
        }

        try {
            return slot.addService(window.googletag.pubads());
        } catch (e) {
            return;
        }
    };

    $public.enableAsyncRendering = function () {
        if(!$private.isGoogleTagReady()) {
            return false;
        }

        return window.googletag.pubads().enableAsyncRendering();
    };

    $private.isGoogleTagReady = function () {
        if (!window.googletag) {
            return false;
        }

        if (!window.googletag.apiReady) {
            return false;
        }

        return true;
    };

    $public.displayAd = function (bannerId) {
        if ($private.validator.isString(bannerId)) {
            $private.enableServices();
            try {
                window.googletag.cmd.push(function () {
                    googletag.display(bannerId);
                });
                return true;
            } catch (e) {
                return false;
            }
        }
        return false;
    };

    $private.enableServices = function () {
        if (!$public.servicesEnabled) {
            try {
                window.googletag.enableServices();
                $public.servicesEnabled = true;
            } catch (e){}
        }
    };

    $public.refreshAds = function (slotRefresh) {
        var slot;

        if (!$private.isValidRefresh()) {
            return;
        }

        if ($private.isValidSlotRefresh(slotRefresh)) {
            slot =  slotRefresh;
        }

        if (slotRefresh instanceof Object && !$private.validator.isArray(slotRefresh)) {
            slot = [];
            slot.push(slotRefresh);
        }

        try {
            return window.googletag.pubads().refresh(slot);
        } catch (e) {
            return;
        }
    };

    $private.isValidRefresh = function () {
        if (!window.googletag) {
            return false;
        }

        if (!window.googletag.pubads) {
            return false;
        }

        if (!window.googletag.pubads().refresh) {
            return false;
        }

        return true;
    };

    $private.isValidSlotRefresh = function (slotRefresh) {
        if (!$private.validator.isArray(slotRefresh) || !slotRefresh.length) {
            return false;
        }

        for (var i = 0; i < slotRefresh.length; i++) {
            if (!(slotRefresh[i] instanceof Object)) {
                return false;
            }
        }

        return true;
    };

    $public.setSizeMappings = function (slot, sizeMappings) {
        if (!slot) {
            return;
        }

        if (!$private.validator.isArray(sizeMappings)) {
            return slot;
        }

        var length = sizeMappings.length;
        if (length === 0) {
            return slot;
        }

        var mapping = googletag.sizeMapping();

        for (var i = 0; i < length; i++) {
            var sizeMapping = sizeMappings[i];
            mapping = mapping.addSize(sizeMapping.viewportSize, sizeMapping.slotSize);
        }

        try {
            mapping = mapping.build();
            slot = slot.defineSizeMapping(mapping);
        } catch (e) {}
        return slot;
    };

}

function Rotative () {

    var $public = this;
    var $private = {};

    $private.validator = new TypeValidator();
    $private.dfpApi = new DfpApi();

    $private.divs = [];

    $public.setAdsLength = function (adsLength) {
        if ($private.validator.isNumericString(adsLength) || $private.validator.isInt(adsLength)) {
            $private.adsLength = parseInt(adsLength);
        }
    };

    $public.getAdsLength = function () {
        return $private.adsLength;
    };

    $public.setCadence = function (cadence) {
        if ($private.validator.isNumericString(cadence) || $private.validator.isInt(cadence)) {
            $private.cadence = parseInt(cadence);
        }
    };

    $public.getCadence = function () {
        return $private.cadence;
    };

    $public.setDivs = function (bannerId) {
        if (!$private.adsLength) {
            return;
        }

        if (!bannerId) {
            return;
        }

        for (var i = 1, length = $private.adsLength; i <= length; i++) {
            var banner = $private.createDiv(bannerId + '-rotative-' + i);
            if (!banner) {
                continue;
            }
            banner.setAttribute('style', 'display: none;');
            $private.divs.push(banner);
        }
    };

    $private.createDiv = function (id) {
        if (!$private.validator.isString(id)) {
            return;
        }
        var div = document.createElement('div');
        div.setAttribute('id', id);
        return div;
    };

    $public.getDivs = function () {
        return $private.divs;
    };

}

function Slot () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();
    $public.logger = new Logs();
    $private.cookieUtils = new CookieUtils();

    $private.rubicon = new Rubicon();
    $public.getDeals = $private.rubicon.getDeals;
    $public.getCpm = $private.rubicon.getCpm;
    $public.getTier = $private.rubicon.getTier;

    $private.raffledRate = Math.round(Math.random() * 100);
    $private.targetings = [];
    $private.rotative = new Rotative();
    $private.rotatives = [];
    $private.sizeMappingsPattern = /(\[\d+\,\s?\d+])+(\,\s?)(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\;?\s?)?/g;

    $public.setDfpPath = function (dfpPath) {
        console.log("SETDFPPATH", dfpPath);
        if ($private.validator.isString(dfpPath)) {
            $private.dfpPath = dfpPath;
        }
    };

    $public.getDfpPath = function () {
        return $private.dfpPath;
    };

    $public.setBannerId = function (bannerId) {
        if ($private.validator.isString(bannerId)) {
            $private.bannerId = bannerId;
            $public.bannerId = bannerId;
            $private.rubicon.setRtpBanner(bannerId);
        }
    };

    $public.getBannerId = function () {
        return $private.bannerId;
    };

    $public.setWidth = function (width) {
        if ($private.validator.isNumericString(width)) {
            $private.width = width;
        }
    };

    $public.getWidth = function () {
        return $private.width;
    };

    $public.setHeight = function (height) {
        if ($private.validator.isNumericString(height)) {
            $private.height = height;
        }
    };

    $public.getHeight = function () {
        return $private.height;
    };

    $public.setCompanion = function (isCompanion) {
        if ($private.validator.isString(isCompanion) && isCompanion.toLowerCase() === 'true') {
            $private.isCompanion = true;
        }
    };

    $public.getCompanion = function () {
        return $private.isCompanion;
    };

    $public.setOutOfPage = function (outOfPage, chan) {
        $private.setOutOfPageCookieName(chan);
        if ($private.isOutOfPage(outOfPage)) {
            $private.outOfPage = true;
        }
    };

    $private.setOutOfPageCookieName = function (chan) {
        if ($private.validator.isString(chan)) {
            $private.outOfPageCookieName = 'CE' + $private.capitalize(chan);
        }
    };

    $private.capitalize = function (str) {
        return str.toLowerCase().replace(/(?:^|\s)\S/g, function(firstChar) {
            return firstChar.toUpperCase();
        });
    };

    $private.getOutOfPageCookieName = function () {
        return $private.outOfPageCookieName;
    };

    $private.isOutOfPage = function (outOfPage) {
        if (!$private.validator.isString(outOfPage)) {
            return false;
        }

        if (outOfPage.toLowerCase() === 'cookie') {
            if ($private.hasOutOfPageCookie()) {
                return false;
            }

            $private.cookieUtils.setItem($private.getOutOfPageCookieName(), '1');
            return true;
        }

        if (!$private.validator.isNumericString(outOfPage)) {
            return false;
        }

        if (!$private.raffle(outOfPage)) {
            return false;
        }

        return true;
    };

    $private.hasOutOfPageCookie = function () {
        if ($private.cookieUtils.getItem($private.getOutOfPageCookieName()).length > 0) {
            return true;
        }

        return false;
    };

    $private.raffle = function (samplingRate) {
        samplingRate = parseInt(samplingRate, 10);
        if (samplingRate === 0) {
            return false;
        }

        if (samplingRate === 100) {
            return true;
        }

        if ($public.getRaffledRate() <= samplingRate) {
            return true;
        }

        return false;
    };

    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };

    $public.getOutOfPage = function () {
        return $private.outOfPage;
    };

    $public.setPosition = function (position) {
        if ($private.validator.isString(position)) {
            $private.position = position;
        }
    };

    $public.getPosition = function () {
        return $private.position;
    };

    $public.addTargeting = function (key, value) {
        if ($private.validator.isString(key) && $private.validator.isDefined(value)) {
            $private.targetings.push({
                'key': key,
                'value': value
            });
        }
    };

    $public.getTargetings = function () {
        return $private.targetings;
    };

    $public.getRotative = function () {
        return $private.rotative;
    };

    $public.setRotatives = function (adsLength, cadence) {
        $private.rotative.setAdsLength(adsLength);
        $private.rotative.setCadence(cadence);
        $private.rotative.setDivs($private.bannerId);
    };

    $public.getRotatives = function () {
        return $private.rotative.getDivs();
    };

    $public.setHide = function(hide) {
        if(!$private.validator.isString(hide)) {
            return;
        }

        if (hide.toLowerCase() === 'true') {
            $private.hide = true;
        }
    };

    $public.getHide = function () {
        return $private.hide;
    };

    $public.setVideoEmbedded = function(hasVideo) {
        if(!$private.validator.isString(hasVideo)) {
            return;
        }

        if(hasVideo.toLowerCase() === 'true') {
            $private.videoEmbedded = true;
        }
    };

    $public.getVideoEmbedded = function () {
        return $private.videoEmbedded;
    };

    $public.setSizeMappings = function (sizeMappings) {
        if (!$private.validator.isString(sizeMappings)) {
            return;
        }

        var mappings = $private.getConvertedMappings(sizeMappings);
        if (mappings.length > 0) {
            $private.sizeMappings = mappings;
        }
    };

    $private.getConvertedMappings = function (sizeMappings) {
        var mappingsPattern = /(\[\d+\,\s?\d+])+(\,\s?)(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\;?\s?)?/g;
        var matchedMappings = sizeMappings.match(mappingsPattern);
        var mappings = [];

        if (!matchedMappings) {
            return mappings;
        }

        for (var i = 0, length = matchedMappings.length; i < length; i++) {
            var mapping = $private.getSizeMapping(matchedMappings[i]);
            if (mapping) {
                mappings.push(mapping);
            }
        }

        return mappings;
    };

    $private.getSizeMapping = function (sizeMapping) {
        if (!sizeMapping) {
            return;
        }

        if (!$private.validator.isString(sizeMapping)) {
            return;
        }

        var matches = sizeMapping.match(/(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\,(\s)?(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\]))+/);

        if (!matches || !matches[1] || !matches[6]) {
            return;
        }

        try {
            return {
                'viewportSize': JSON.parse(matches[1]),
                'slotSize': JSON.parse(matches[6])
            };
        } catch (e) {
            return;
        }
    };

    $public.getSizeMappings = function () {
        return $private.sizeMappings;
    };

    $public.setIncremental = function(incremental) {
        if(!$private.validator.isString(incremental)) {
            return;
        }

        if (incremental.toLowerCase() === 'true') {
            $private.incremental = true;
        }
    };

    $public.getIncremental = function () {
        return $private.incremental;
    };

}

function DomUtils () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();

    $public.divHasLoaded = function (id, callback, timeout) {

        timeout = timeout || 50;

        if (!$private.validator.isFunction(callback)) {
            return false;
        }

        var div = $public.getDiv(id);
        if (div) {
            callback();
            return true;
        }

        setTimeout(function() {
            if (!$public.loaderLimit) {
                $public.divHasLoaded(id, callback, timeout);
            }
        }, timeout);
    };



    $public.getDiv = function (bannerId) {
        if (!$private.validator.isString(bannerId)) {
            return;
        }

        return document.getElementById(bannerId);
    };

}
function Ad () {

    var $public = this;
    var $private = {};

    $private.trackManager = new TrackManager();
    $private.sesame = new Sesame();

    $public.setPubadsTargeting = function (pubadsTargeting) {
        $private.pubadsTargeting = pubadsTargeting;
    };

    $public.getPubadsTargeting = function () {
        return $private.pubadsTargeting;
    };

    $public.setSlot = function (dfpPath, bannerId, width, height, isCompanion, outOfPage, chan, pos, rotative, rotativeInterval, hide, hasVideo, sizeMappings, incremental) {
        var slot = new Slot();
        slot.setDfpPath(dfpPath);
        $private.dfpPath = dfpPath;
        slot.setBannerId(bannerId);
        $private.bannerId = bannerId;
        slot.setWidth(width);
        $private.width = width;
        slot.setHeight(height);
        $private.height = height;
        slot.setCompanion(isCompanion);
        $private.isCompanion = isCompanion;
        slot.setOutOfPage(outOfPage, chan);
        $private.outOfPage = outOfPage;
        slot.setPosition(pos);
        $private.pos = pos;
        slot.setRotatives(rotative, rotativeInterval);
        $private.rotative = rotative;
        $private.rotativeInterval = rotativeInterval;
        slot.setHide(hide);
        $private.hide = hide;
        slot.setSizeMappings(sizeMappings);
        $private.sizeMappings = sizeMappings;
        slot.setVideoEmbedded(hasVideo);
        $private.hasVideo = hasVideo;
        slot.setIncremental(incremental);
        $private.incremental = incremental;
        $private.slot = slot;
    };

    $public.getDfpPath = function () {
        return $private.dfpPath;
    };

    $public.getBannerId = function () {
        return $private.bannerId;
    };

    $public.getWidth = function () {
        return $private.width;
    };

    $public.getHeight = function () {
        return $private.height;
    };

    $public.getIsCompanion = function () {
        return $private.isCompanion;
    };

    $public.getOutOfPage = function () {
        return $private.outOfPage;
    };

    $public.getPos = function () {
        return $private.pos;
    };

    $public.getRotative = function () {
        return $private.rotative;
    };

    $public.getRotativeInterval = function () {
        return $private.rotativeInterval;
    };

    $public.getHide = function () {
        return $private.hide;
    };

    $public.getSizeMappings = function () {
        return $private.sizeMappings;
    };

    $public.getHasVideo = function () {
        return $private.hasVideo;
    };

    $public.getIncremental = function () {
        return $private.incremental;
    };

    $public.setSlotPosition = function (position) {
        $private.slot.setPosition(position);
    };

    $public.getSlot = function () {
        return $private.slot;
    };

    $public.setCampaignuol = function (campaignuol) {
        $private.campaignuol = campaignuol;
    };

    $public.getCampaignuol = function () {
        return $private.campaignuol;
    };

    $public.setExpble = function (expble) {
        $private.expble = expble;
    };

    $public.getExpble = function () {
        return $private.expble;
    };

    $public.setGroup = function (group) {
        $private.group = group;
    };

    $public.getGroup = function () {
        return $private.group;
    };

    $public.setKeyword = function (keyword) {
        $private.keyword = keyword;
    };

    $public.getKeyword = function () {
        return $private.keyword;
    };

    $public.setAudienceSciencePlacement = function (audienceSciencePlacement) {
        $private.audienceSciencePlacement = audienceSciencePlacement;
    };

    $public.getAudienceSciencePlacement = function () {
        return $private.audienceSciencePlacement;
    };

    $public.setDfpApi = function (dfpApi) {
        if (dfpApi !== undefined) {
            $private.dfpApi = dfpApi;
        }
    };

    $public.getDfpApi = function () {
        return $private.dfpApi;
    };

    $private.showAd = function () {
        if (!$private.slot) {
            return false;
        }

        $private.defineDfpSlot();
        if (!$private.slot.dfp) {
            return false;
        }

        if (!$private.dfpApi.enableAsyncRendering()) {
            return false;
        }

        if ($private.dfpApi.displayAd($private.slot.getBannerId())) {
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
            return true;
        }

        return false;
    };

    $private.addTargetings = function () {
        $private.slot.addTargeting('deals', $private.slot.getDeals());
        if ($private.campaignuol !== undefined) {
            $private.slot.addTargeting('campaignuol', $private.dfpApi.getCampaignuol($private.campaignuol));
        }

        if ($private.expble !== undefined) {
            $private.slot.addTargeting('expble', $private.dfpApi.getCampaignuol($private.expble));
        }

        if ($private.group !== undefined) {
            $private.slot.addTargeting('group', $private.dfpApi.getGroup($private.group));
        }

        if ($private.keyword !== undefined) {
            $private.slot.addTargeting('keyword', $private.dfpApi.getKeyword($private.keyword));
        }

        $private.slot.addTargeting('cpm', $private.slot.getCpm());
        $private.slot.addTargeting('tier', $private.slot.getTier());
        $private.slot.addTargeting('pos', $private.slot.getPosition());
        if ($private.audienceSciencePlacement !== undefined) {
            $private.slot.addTargeting('gwd', $private.dfpApi.getGwd($private.audienceSciencePlacement));
        }
    };

    $private.defineDfpSlot = function () {
        var dfpSlot = $private.dfpApi.defineSlot($private.slot.getDfpPath(), $private.slot.getBannerId(), $private.slot.getWidth(), $private.slot.getHeight(), $private.slot.getOutOfPage());
        if ($private.slot.getCompanion()) {
            dfpSlot = $private.dfpApi.setCompanionAds(dfpSlot, $private.slot.getCompanion());
        }

        dfpSlot = $private.dfpApi.addServices(dfpSlot);

        if ($private.slot.getSizeMappings()) {
            dfpSlot = $private.dfpApi.setSizeMappings(dfpSlot, $private.slot.getSizeMappings());
        }

        if (dfpSlot) {
            $private.addTargetings();
            $private.slot.dfp = $private.dfpApi.setTargetings(dfpSlot, $private.slot.getTargetings());
        }
    };

    $public.isRotative = function () {
        if ($private.slot.getHide()) {
            return false;
        }
        return $private.slot.getRotatives().length > 0;
    };

    $public.display = function (callback) {
        if ($private.slot.getHide()) {
            $public.hideSlot();
            return;
        }

        if ($private.slot.getVideoEmbedded()) {
            $private.sesame.track();
        }

        if (!window.googletag && !window.googletag.cmd) {
            setTimeout($public.display, 100);
        }

        try {
            window.googletag.cmd.push(function () {
                $private.dfpApi.onDfpReady(function () {
                    $private.showAd();
                    callback();
                });
            });
        } catch (e) {}
    };

    $private.hideSlot = function () {
        var element = document.getElementById($private.slot.getBannerId());
        if (!element) {
            return;
        }

        element.style.display = 'none';

        if (!element.parentNode) {
            return;
        }

        if(element.parentNode.className === 'publicidade' || element.parentNode.className === 'pub') {
            element.parentNode.style.display = 'none';
        }
    };

}

function Pubads () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();
    $private.tailtarget = new Tailtarget();
    $private.behaviouralTargeting = new BehaviouralTargeting();
    $private.pubadsTargeting = [];

    $public.defineTargetingList = function () {
        $private.addToPubads('referer', window.location.href);
        $private.addToPubads('tags', $public.getTagPages());
        $private.addTailtargetToPubads();
        $private.addToPubads('bt', $private.behaviouralTargeting.getBTCookies());
    };

    $private.addToPubads = function (key, value) {
        if ($private.validator.isString(key) && $private.validator.isDefined(value)) {
            $private.pubadsTargeting.push({
                'key': key,
                'value': value
            });
        }
    };

    $public.getTagPages = function () {
        var uVar = window.universal_variable;

        if (!$private.validator.isObject(uVar)) {
            return;
        }

        if (!$private.validator.isObject(uVar.page)) {
            return;
        }

        if (!$private.validator.isArray(uVar.page.tags)) {
            return;
        }

        return uVar.page.tags;
    };

    $private.addTailtargetToPubads = function () {
        var tailtargetProfiles = $private.tailtarget.getProfiles();
        if (!$private.validator.isObject(tailtargetProfiles)) {
            return;
        }

        for (var key in tailtargetProfiles) {
            $private.addToPubads(key, tailtargetProfiles[key]);
        }
    };

    $public.getTargetingList = function () {
        return $private.pubadsTargeting;
    };
}

function RotativeAds () {

    var $public = this;
    var $private = {};

    $public.setAd = function (ad) {
        $private.ad = ad;
        $private.rotatives = $private.ad.getSlot().getRotatives();
    };

    $public.getAd = function () {
        return $private.ad;
    };

    $public.setContainer = function (container) {
        container.style.display = 'none';
        container.style.height = $private.ad.getSlot().getHeight() + 'px';
        container.style.width = $private.ad.getSlot().getWidth() + 'px';
        container.style.overflow = 'hidden';
        $private.container = container;
    };

    $public.setCallback = function (callback) {
        $private.callback = callback;
    };

    $public.getCallback = function () {
        return $private.callback;
    };

    $public.display = function () {
        $private.appendRotatives();
        $private.showRotatives();
        $private.rotative = $private.ad.getSlot().getRotative();
        $public.rotate();
    };

    $private.appendRotatives = function () {
        var inner = '';
        for (var i = 0, length = $private.rotatives.length; i < length; i++) {
            inner+= $private.rotatives[i].outerHTML;
        }
        $private.container.innerHTML = inner;
    };

    $private.showRotatives = function () {
        for (var i = 0, length = $private.rotatives.length; i < length; i++) {
            $private.showCurrentAd(
                $private.rotatives[i].getAttribute('id'),
                $private.getRotativePosition(i)
            );
        }
    };

    $public.rotate = function() {
        if (!$private.rotative.getCadence()) {
            return;
        }
        setInterval(function() {
            $public.turnRotation();
        }, $private.rotative.getCadence());
    };

    $public.turnRotation = function() {
        var items = $private.container.childNodes;
        for (var i = 0; i < items.length; i++) {
            var children = items[i];
            if (children.style.display !== 'none') {
                children.style.display = 'none';
                items[$private.getNext(i, items.length)].style.display = 'block';
                break;
            }
        }
        $private.container.style.display = 'block';
    };

    $private.getNext = function(actual, size) {
        if (actual === (size - 1)) {
            return 0;
        }
        return ++actual;
    };

    $private.getRotativePosition = function (i) {
        return 'rotativo-' + (i + 1);
    };

    $private.showCurrentAd = function (id, rotativePosition) {
        $private.getAd(id, rotativePosition).display($private.callback);
    };

    $private.getAd = function (bannerId, position) {
        var ad = new Ad();
        ad.setPubadsTargeting($private.ad.getPubadsTargeting());
        ad.setSlot(
            $private.ad.getDfpPath(),
            bannerId,
            $private.ad.getWidth(),
            $private.ad.getHeight(),
            $private.ad.getIsCompanion(),
            $private.ad.getOutOfPage(),
            position,
            $private.ad.getRotative(),
            $private.ad.getRotativeInterval(),
            $private.ad.getHide(),
            $private.ad.getSizeMappings(),
            $private.ad.getHasVideo(),
            $private.ad.getIncremental()
        );
        ad.setCampaignuol($private.ad.getCampaignuol());
        ad.setExpble($private.ad.getExpble());
        ad.setGroup($private.ad.getGroup());
        ad.setKeyword($private.ad.getKeyword());
        ad.setAudienceSciencePlacement($private.ad.getAudienceSciencePlacement());
        ad.setDfpApi($private.ad.getDfpApi());
        return ad;
    };

}

function Main () {

    var $private = {};
    var $public = this;

    $private.validator = new TypeValidator();
    $private.dfpApi = new DfpApi();
    $private.trackManager = new TrackManager();
    $private.pubads = new Pubads();
    $private.domUtils = new DomUtils();
    $private.DfpPath = new DfpPath();

    $public.logger = new Logs();

    $public.adsDisplayed = [];
    $private.tailtargetProfiles = {};
    $public.refreshAds = $private.dfpApi.refreshAds;

    $public.init = function (config) {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if ($private.isValidBanner(config)) {
            $private.prepareDfpApi();
            $private.pubads.defineTargetingList();
            $private.displayBanner(config);
        }
    };

    $private.isValidBanner = function (config) {
        if (!$private.validator.isObject(config)) {
            $private.trackError('config_invalido', 'O objeto de configuração está inválido');
            return false;
        }

        if (!$private.validator.isString(config.bannerId)) {
            $private.trackError('bannerId_invalido', 'O atributo bannerId do objeto de configuração está inválido');
            return false;
        }

        return true;
    };

    $private.trackError = function (errorType, errorMessage) {
        if (!$private.validator.isString(errorType)) {
            return;
        }
        if (!$private.validator.isString(errorMessage)) {
            return;
        }

        $public.logger.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };

    $private.prepareDfpApi = function () {
        $private.dfpApi.appendGPT();
        $private.dfpApi.createNameSpace();
    };

    $private.displayBanner = function (config) {
        $private.domUtils.divHasLoaded(config.bannerId, function () {
            var div = $private.domUtils.getDiv(config.bannerId);
            if (!div) {
                return;
            }

            if ($private.isAdPopulated(config.bannerId)) {
                $private.trackError('anuncio_populado', 'O elemento já foi populado');
                return;
            }

            //if (!$private.DfpPath.getDfpPath()) {
            $private.DfpPath.setDfpPath(config.dfppath);
            //}

            if (!$private.dfpApi.hasTrackedPubads()) {
                $private.dfpApi.setPubadsTracking($private.pubads.getTargetingList());
            }

            $private.display(config, div);
        });
    };

    $private.isAdPopulated = function (bannerId) {
        for (var i = 0, length = $public.adsDisplayed.length; i < length; i++) {
            if ($public.adsDisplayed[i].getBannerId() == bannerId) {
                return true;
            }
        }

        return false;
    };

    $private.display = function (config, div) {
        $private.showAd($private.getAd(config), div);
    };

    $private.showAd = function (ad, element) {
        if (ad.isRotative()) {
            $private.showRotatives(ad, element);
        } else {
            ad.display($private.updateDfpApi(ad));
        }
    };

    $private.getAd = function (config) {
        var ad = new Ad();
        ad.setPubadsTargeting($private.pubads.getTargetingList());
        ad.setSlot($private.DfpPath.getDfpPath(), config.bannerId, config.bannerWidth, config.bannerHeight, config.isCompanion, config.outOfPage, $private.DfpPath.getChan(), config.pos, config.rotative, 3000, config.hide, config.videoEmbedded, config.sizeMapping, config.incremental);
        ad.setCampaignuol(config.campaignuol);
        ad.setExpble(config.expble);
        ad.setGroup(config.group);
        ad.setKeyword(config.keyword);
        ad.setAudienceSciencePlacement(config.audienceSciencePlacement);
        ad.setDfpApi($private.dfpApi);
        return ad;
    };

    $private.updateDfpApi = function (ad) {
        return function () {
            $private.dfpApi = ad.getDfpApi();
            $public.adsDisplayed.push(ad);
            $private.showIncrementalAds();
        };
    };

    $private.showRotatives = function (ad, container) {
        var rotativeAds = new RotativeAds();
        rotativeAds.setAd(ad);
        rotativeAds.setContainer(container);
        rotativeAds.setCallback($private.updateDfpApi(ad));
        rotativeAds.display();
    };

    $public.refreshSlot = function (id) {
        if (!$private.validator.isString(id)) {
            return;
        }

        $private.setSlotId();

        if ($private.slotId.length === 0) {
            return;
        }

        for (var i = 0; i < $private.slotId.length; i++) {
            if ($private.slotId[i].bannerId === id) {
                try {
                    $private.dfpApi.refreshAds($private.slotId[i].slot);
                } catch (e) {
                    return;
                }
            }
        }
    };

    $private.setSlotId = function () {
        $private.slotId = [];

        var length = googletag.pubads().getSlots().length;

        if (length === 0) {
            return;
        }

        for (var i = 0; i < length; i++) {
            try {
                var objSlot = {
                    'bannerId' : googletag.pubads().getSlots()[i].getSlotElementId(),
                    'slot' : googletag.pubads().getSlots()[i]
                };
                $private.slotId.push(objSlot);
            } catch (e) {
                continue;
            }
        }
    };

    $private.showIncrementalAds = function (argument) {
        var incrementalAds = setInterval(function () {
            var hasIncremental = false;
            for (var i = 0; i < $public.adsDisplayed.length; i++) {
                var displayed = $public.adsDisplayed[i];
                if (displayed.getSlot().getIncremental() === true) {
                    hasIncremental = true;
                    $private.showIncrementalAd(displayed);
                }
            }
            if (hasIncremental === false) {
                clearInterval(incrementalAds);
            }
        }, 100);
    };

    $private.showIncrementalAd = function (ad) {
        var newElememts = document.querySelectorAll('[id^="' + ad.getBannerId() + '"]');
        for (var i = 1; i < newElememts.length; i++) {
            var element = newElememts[i];
            var id = element.getAttribute('id');
            if (id.indexOf('rotative') > -1) {
                continue;
            }

            if (!$private.isAdPopulated(id)) {
                $private.showAd($private.getModifiedAd(ad, element), element);
            }
        }
    };

    $private.getModifiedAd = function (ad, element) {
        var config = {};
        config.bannerId = element.getAttribute('id');
        config.bannerWidth = ad.getWidth();
        config.bannerHeight = ad.getHeight();
        config.isCompanion = ad.getIsCompanion();
        config.outOfPage = ad.getOutOfPage();
        config.pos = ad.getPos();
        config.rotative = ad.getRotative();
        config.hide = ad.getHide();
        config.videoEmbedded = ad.getHasVideo();
        config.sizeMapping = ad.getSizeMappings();
        config.incremental = ad.getIncremental();
        return $private.getAd(config, element);
    };
}

var nameSpace = new NameSpace('DfpAsync');
var dfpAsync = new Main();
nameSpace.init = dfpAsync.init;
nameSpace.refreshAds = dfpAsync.refreshAds;
nameSpace.refreshSlot = dfpAsync.refreshSlot;

for (var i = 0; i < nameSpace.config.length; i++) {
    nameSpace.init(nameSpace.config[i]);
    console.log("CONFIG", nameSpace.config[i]);
}

var __monitoracaoJsuol = 'tm.jsuol.com.br/modules/dfp-async.js';

})(this, document);