/*
 * © UOL TAG MANAGER - Todos os direitos reservados
 * VERSAO: 1.5.628-SNAPSHOT
 */
window.TM = window.TM || {};
if(!window.TM_VERSION_LOGGED){
    TM_VERSION_LOGGED = true;
    if(window.console){
        console.log('[UOL Tag Manager] VERSION: 1.5.628-SNAPSHOT');
    }
}
(function() {
    var __monitoracaoJsuol = 'tm.uol.com.br/vm_tagmanager.js?ORIGINAL_PATH=/i/h/homenova.js';
    var DnaInjector = function() {
    var scriptsAreEqual = function (script1, script2) {
        if (script1.src != script2.src) {
            return false;
        }
        if (script1.async != script2.async) {
            return false;
        }
        return true;
    };
    var getProtocolFromSrc = function (src) {
        if (src.indexOf('//') === -1) {
            return '';
        }
        if (src.indexOf('//') === 0) {
            return '//';
        }
        var protocol = src.split('//')[0];
        if (protocol.length === 0) {
            protocol = window.location.protocol;
        }
        return protocol + '//';
    };
    var getScriptProperties = function (script) {
        var result = {};
        var src = script.getAttribute('src');
        if (!src) {
            return result;
        }
        result.async = script.async;
        result.src = src.substr(getProtocolFromSrc(src).length);
        return result;
    };
    var hasTagOnPage = function (tag) {
        var elements = document.getElementsByTagName('script');
        tag = getScriptProperties(tag);
        for (var i = 0; i < elements.length; i++) {
            var domTag = getScriptProperties(elements[i]);
            if (scriptsAreEqual(domTag, tag)) {
                return true;
            }
        }
        return false;
    };
    var getProtocol = function() {
        var protocol = window.location.protocol;
        var patt = /^http(s)?:$/;
        if(!patt.test(protocol)) {
            return 'http:';
        }
        return protocol;
    };
    var getTag = function() {
        var dna = document.createElement('script');
        dna.setAttribute('src', getProtocol() + '//tm.jsuol.com.br/modules/uol-dna.js');
        dna.async = true;
        return dna;
    };
    this.appendTag = function() {
        var tag = getTag();
        if (hasTagOnPage(tag)) {
            return false;
        }
        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.appendChild(tag);
        return true;
    };
};
var dna = new DnaInjector();
dna.appendTag();
})();
(function (){
var adServer = 'dfp';
    window.TM = window.TM || {};      
TM.StringUtils = function () {
    this.trim = function (string) {
        if (typeof string != 'string' || !string.length) {
            return;
        }
        string = string.replace(/\s\s/gm, '');
        if (string.length == 1) {
            string = string.replace(/\s/gm, '');
        }
        if (!string.length) {
            return;
        }
        return string;
    };
};
    TM.LogUtils = function () {
    var prefix = "[UOL Tag Manager] ";
    var history = {
        "warn"  : [],
        "error" : [],
        "info"  : [],
        "log"   : []
    };
    var getQueryString = function(name, query) {
        if (name === undefined) {
            return;
        }
        if (typeof name !== 'string') {
            return;
        }
        if (typeof query !== 'undefined' && typeof query !== 'string') {
            return;
        }
        var search = query || document.location.search;
        var length = search.length;
        var value;
        search = search.split(/\?|\&/);
        length = search.length;
        for (var i = 0; i < length; i++) {
            if (search[i].split('=')[0] == name) {
                value = search[i].split('=')[1];
                break;
            }
        }
        return value;
    };
    var isActive = function (query) {
        return (getQueryString('tm', query) == 'debug' ? true : false);
    };
    this.warn = function(msg) {
        print(msg, 'warn');
    };
    this.error = function(msg) {
        print(msg, 'error');
    };
    this.info = function(msg) {
        print(msg, 'info');
    };
    this.debug = function (msg) {
        debug(msg, 'debug');
    };
    this.log = function (msg) {
        print(msg, 'log');
    };
    var print = function (msg, fn) {
        fn = validateFunction(fn);
        if (!fn) {
            return;
        }
        msg = validateMessage(msg);
        if (!msg) {
            return;
        }
        if (isActive() === false) {
            history[fn].push(msg);
            return;
        }
        console[fn](msg);
        history[fn].push(msg);
    };
    this.getHistory = function (fn) {
        return history[fn];
    };
    var validateFunction = function (fn) {
        if (!window.console || !console || !console.log) {
            return;
        }
        if (!console[fn] && fn == 'log') {
            return;
        }
        if (!fn || !console[fn]) {
            fn = "log";
        }
        return fn;
    };
    var validateMessage = function (msg) {
        var string = new TM.StringUtils();
        msg = string.trim(msg);
        if (!msg || !prefix) {
            return;
        }
        msg = prefix + msg;
        return msg;
    };
};    
TM.CommonUtils = function () {
    var doc = document;
    this.getQueryString = function(name) {
        var search = document.location.search;
        var value;
        search = search.substr(1,search.length);
        search = search.split('&');
        for (var i = 0; i < search.length; i++) {
            if (search[i].split('=')[0] == name) {
                value = search[i].split('=')[1];
                break;
            }
        }
        return value;
    };
    this.debugMode = ((this.getQueryString('tm') == 1 || this.getQueryString('google_console') == 1 || this.getQueryString('google_force_console') == 1 || this.getQueryString('debug') == 'true') ? true : false);
    this.randomNumber = function (limit) {
        limit = limit || '123456';
        var result = Math.floor(Math.random() * limit);
        return result;
    };
    this.requestNumber = function () {
        var limit = new Date();
        limit     = limit.getTime();
        limit     = limit * 1000;
        var rad   = this.randomNumber(limit);
        return rad;
    };
    this.listenEventUntil = function (target, eventName, eventCallback, timeoutMs, timeoutCallback) {
        if (navigator.userAgent.indexOf('MSIE 8') != -1) {
            target[eventName] = 0;
            target.attachEvent('onpropertychange', function(event) {
                if (event.propertyName == eventName && target[eventName] != 0) {
                    eventCallback();
                    target.detachEvent('onpropertychange', arguments.callee);
                }
            });
            return;
        }
        var eventMethod = (target.addEventListener) ? "addEventListener" : "attachEvent";
        var tooLate = false;
        var bubbling = true;
        var timeoutId;
        if (timeoutMs && timeoutMs > 0 && timeoutCallback) {
            timeoutId = setTimeout(function () {
                tooLate = true;
                if (timeoutCallback !== undefined) {
                    timeoutCallback();
                }
            }, timeoutMs);
        }
        target[eventMethod](eventName, function (e) {
            if (tooLate) {
                return;
            }
            if (eventCallback(e) !== false) {
                clearTimeout(timeoutId);
            }
        }, bubbling);
    };
    this.dispatchEvent = function (target, eventName) {
        if (navigator.userAgent.indexOf('MSIE 8') != -1) {
            target[eventName]++;
            return;
        }
        if (document.createEventObject && typeof CustomEvent == 'undefined') {
            var evt = document.createEventObject();
            evt.eventType = eventName;
            target.fireEvent(eventName, evt);
        } else {
            var evt;
            try {
                evt = new CustomEvent(eventName);
            } catch (e) {
                evt = document.createEvent('CustomEvent');
                evt.initCustomEvent(eventName, false, false, null);
            }
            target.dispatchEvent(evt);
        }     
    };
    this.listenOnce = function (target, eventName, func) {
        var addMethod = (target.addEventListener) ? "addEventListener" : "attachEvent";
        var removeMethod = (target.removeEventListener) ? "removeEventListener" : "detachEvent";
        var run = false;
        var bubbling = true;
        target[addMethod](eventName, function (e) {
            if (!run) {
                run = true;
                func(e);
            }
        }, bubbling);
    };
    this.ready = function(fn, timeout) {
        var self = this;
        var readyState = document.readyState;
        timeout = (timeout ? timeout + 50 : 500);
        if (typeof fn != 'function') {
            return false;
        }
        if(readyState && readyState == 'complete') {
            fn();
        } else {
            setTimeout(function () {
                self.ready(fn, timeout);
            }, timeout);
        }
    };
    this.getDntCode = function () {
        var dntCode = 0;
        if (navigator.doNotTrack == "yes") {
            dntCode = 3;
        } else if ((navigator.doNotTrack == "1") || (window.doNotTrack == "1") || (navigator.msDoNotTrack == "1")) {
            dntCode = 1;
        }
        return dntCode;
    };
};
    TM.CookieUtils = function () {
    var doc = document;
    this.getCookie = function(name) {
        var key;
        var value;
        var cookie = doc.cookie.split(";");
        var length = cookie.length;
        var found = false;
        for (var i = 0; i < length; i++) {
            key = cookie[i].substr(0, cookie[i].indexOf("="));
            value = cookie[i].substr(cookie[i].indexOf("=") + 1);
            key = key.replace(/^\s+|\s+$/g, "");
            if (key == name) {
                found = true;
                break;
            }
        }
        if (!found) {
            return '';
        }
        return unescape(value);
    };
    this.setCookie = function(name, value, expiredays, domain){
        var cookie;
        if (!name) {
            return;
        }
        expiredays = expiredays || 45;
        domain = domain || '.uol.com.br';
        if(value.substring(0,1) == ";") {
            value = value.substr(1);
        }
        value = escape(value);
        var exdate = exdate || new Date();
        exdate.setDate(exdate.getDate() + expiredays);
        exdate = exdate.toUTCString();
        cookie = name + "=" + value + ";expires=" + exdate + ";path=/;domain="+ domain +";";
        return doc.cookie = cookie;
    };
    this.toArray = function (cookie, separator) {
        var arr = new TM.ArrayUtils();
        if (!separator || typeof separator != 'string') {
            separator = ';';
        }
        if (typeof cookie != 'string') {
            return;
        }
        var aux = [];
        cookie = cookie.split(separator).sort();
        for (var i = 0; i < cookie.length; i++) {
            if(cookie[i] && !arr.arrayContains(aux, cookie[i])) {
                aux.push(cookie[i]);
            }
        }
        return aux;
    };
    this.hasItem = function (cookieName, item) {
        var result = false;
        item = item.toString();
        var cookie = this.getCookie(cookieName);
        var cookieArr = this.toArray(cookie);
        for (var i = 0, length = cookieArr.length; i < length; i++) {
            if (cookieArr[i] == item) {
                result = true;
                break;
            }
        }
        return result;
    };
    this.deleteCookie = function (name, domain) {
        var cookie = this.getCookie(name);
        if(cookie) {
            this.setCookie(name, '', -1, domain);
        }
        cookie = this.getCookie(name);
        return cookie;
    };
};
TM.TagUtils = function () {
    var doc = document; 
    this.getElementsByClassName = function (classname, tagname) {
        if (!classname) {
            return;
        }
        tagname = tagname || '*';
        var nodesArr = [], 
        elements = doc.getElementsByTagName(tagname);
        for (var i = 0, length = elements.length; i < length; i++) {
            var element = elements[i], 
            classList = element.className;
            if (!classList.length) {
                continue;
            }
            classList = (classList ? classList.split(' ') : false);
            for (var j = 0, classListLength = classList.length; j < classListLength; j++) {
                var item = classList[j];
                if (item == classname) {
                    nodesArr.push(element);
                }
            }
        }
        return nodesArr;
    };
    this.getElementsByAttribute = function (attr, limit, group, tagName) {
        // Get elements by it's attribute
        var result = [];
        tagName = tagName || '*';
        var divs = group || document.getElementsByTagName(tagName);
        var length = divs.length;
        for (var i = 0; i < length; i++) {
            if (divs[i].getAttribute(attr)) {
                if (limit && result.length >= limit) {
                    break;
                }
                result.push(divs[i]);
            }
        }
        return result;
    };
    this.removeElement = function (element) {
        if (!element) {
            return;
        }
        element.parentNode.removeChild(element);
    };
    this.createTag = function(tagName, parentNode, attributes) {
        if (!tagName && !parentNode && !attributes) {
            return;
        }
        var tag = document.createElement(tagName);
        for (var key in attributes) {
            var value = attributes[key];
            tag.setAttribute(key, value);
        }
        parentNode.appendChild(tag);
    };
    this.createTagScript = function(id, src, parentNode) {
        if (!src) {
            return;
        }
        parentNode = parentNode || doc.body;
        var attributes = {};
        attributes.type = 'text/javascript';
        attributes.defer = true;
        if (id) {
            attributes.id = id;
        }
        attributes.src = src;
        this.createTag('script', parentNode, attributes);
    };
    this.createTagIframe = function(attributes, parentNode) {
        if (!attributes.src) {
            return;
        }
        parentNode = parentNode || doc.body;
        this.createTag('iframe', parentNode, attributes);
    };
    this.createTagScriptUnique = function(id, src, parentNode) {
        if (!src) {
            return;
        }
        var srcToMatch = src.replace(/(\/\/|http:\/\/|https:\/\/)/, '');
        var scripts = document.getElementsByTagName('script');
        for(var i = 0;i < scripts.length;i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if(scriptSrc && scriptSrc.indexOf(srcToMatch) >= 0) {
                return;
            }
        }
        this.createTagScript(id, src, parentNode);
    };
};
TM.ObjectUtils = function () {
    var self = this;
    this.getLength = function (obj) {
        if (typeof obj  !== 'object') {
            return;
        }
        var count = 0;
        for (var i in obj){
            count++;
        }
        return count;
    };
    this.cloneObject = function (obj1, temp) {
        if (obj1 === null || typeof obj1  !== 'object') {
            return;
        }
        temp = temp || obj1.constructor();
        for (var key in obj1) {
            temp[key] = obj1[key];
        }
        return temp;
    };
    this.getValueFromKey = function (obj, key) {
        if (typeof obj  !== 'object') {
            return;
        }
        var value = false;
        for (var i in obj){
            if(i.toLowerCase() == key.toLowerCase()) {
                value = obj[i];
                break;
            }
        }
        return value;
    };
    this.getValueFromMatch = function (key, argument, obj) {
        if (!key || !argument || !obj) {
            return;
        }
        var length = obj.length;
        var value;
        for (var i = 0; i < length; i++) {
            if(key.match(obj[i].re)){
                value = obj[i][argument];
                break;
            }
        }
        return value;
    };
    this.binarySearchInObject = function (values, target, key, start, end) {
        if (start > end) {
            return -1;
        }
        var middle = Math.floor((start + end) / 2);
        var value = values[middle][key];
        if (value > target) {
            return self.binarySearchInObject(values, target, key, start, middle-1);
        }
        if (value < target) {
            return self.binarySearchInObject(values, target, key, middle+1, end);
        }
        return middle;
    };
    return this;
};
    TM.ArrayUtils = function () {
    this.arrayContains = function (arr, item) {
        if (!arr) {
            return false;
        }
        var length = arr.length;
        for (var i = 0; i < length; i++) {
            if (arr[i] == item) {
                return true;
            }
        }
        return false;
    };
    this.matchesInArray = function (item, a1) {
        var length = a1.length;
        for (var i = 0; i < length; i++) {
            if (item.match(a1[i])) {
                return true;
            }
        }
        return false;
    };
};    var TmConfig = TmConfig || {};
TmConfig.ClientTrack = {
    'sites' : [
        {'re' : /^http(s)?:\/\/([a-z0-9-]*.)?netshoes.com.br(\/.*)?$/, 'id' : '501'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?sephora\.com\.br(\/.*)?$/, 'id' : '502'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*.)?dafiti.com.br(\/.*)?$/, 'id' : '503'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*.)?dell\.com\.br(\/.*)?$/, 'id' : '504'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?netflix\.com\.br(\/.*)?$/, 'id' : '505'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?cyrela\.com\.br(\/.*)?$/, 'id' : '506'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?fastshop\.com\.br(\/.*)?$/, 'id' : '507'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?even\.com\.br(\/.*)?$/, 'id' : '508'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*.)?kanui.com.br(\/.*)?$/, 'id' : '509'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?pank\.com\.br(\/.*)?$/, 'id' : '510'},
        {'re' : /^http(s)?:\/\/empregocerto\.uol\.com\.br(\/.*)?$/, 'id' : '513'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?centauro\.com\.br(\/.*)?$/, 'id' : '514'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?shopping\.uol\.com\.br(\/.*)?$/, 'id' : '515'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?compareja(\.uol)?\.com\.br(\/.*)?$/, 'id' : '515'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?catapreco(\.uol)?\.com\.br(\/.*)?$/, 'id' : '515'},
        {'re' : /^http(s)?:\/\/todaoferta\.uol\.com\.br(\/.*)?$/, 'id' : '516'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?bomnegocio\.com(\/.*)?$/, 'id' : '517'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?staples\.com\.br(\/.*)?$/, 'id' : '518'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?americanas\.com\.br(\/.*)?$/, 'id' : '519'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?submarino\.com\.br(\/.*)?$/, 'id' : '520'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?shoptime\.com\.br(\/.*)?$/, 'id' : '521'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?hotelurbano\.com(\/.*)?$/, 'id' : '522'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?exssence\.com\.br(\/.*)?$/, 'id' : '523'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?privalia\.com(\/.*)?$/, 'id' : '524'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?mrv\.com\.br(\/.*)?$/, 'id' : '525'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?shopfato\.com\.br(\/.*)?$/, 'id' : '526'},
        {'re' : /^http(s)?:\/\/www\.hopelingerie\.com\.br(\/.*)?$/, 'id' : '527'},
        {'re' : /^http(s)?:\/\/www\.belezanaweb\.com\.br(\/.*)?$/, 'id' : '528'},
        {'re' : /^http(s)?:\/\/www\.giseleintimates\.com(\/.*)?$/, 'id' : '529'},
        {'re' : /^http(s)?:\/\/www\.jacmotorsbrasil\.com\.br(\/.*)?$/, 'id' : '530'},
        {'re' : /^http(s)?:\/\/www\.visitorlando\.com(\/.*)?$/, 'id' : '531'},
        {'re' : /^http(s)?:\/\/www\.epocacosmeticos\.com\.br(\/.*)?$/, 'id' : '532'},
        {'re' : /^http(s)?:\/\/www\.glamour\.com\.br(\/.*)?$/, 'id' : '533'},
        {'re' : /^http(s)?:\/\/tm\.uol\.com\.br\/tam-banner-interativo\.html(.*)?$/, 'id' : '534'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]+\.)*safarishop\.com\.br(\/.*)?$/, 'id' : '535'},
        {'re' : /^http(s)?:\/\/www\.webmotors\.com\.br(\/.*)?$/, 'id' : '536'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*.)?copadomundo\.uol\.com\.br(\/.*)?$/, 'id' : '537'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?bemmaisseguro\.com(\/.*)?$/, 'id' : '538'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?tam\.com\.br(\/.*)?$/, 'id' : '539'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?bradesco\.com\.br(\/.*)?$/, 'id' : '540'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?colombo\.com\.br(\/.*)?$/, 'id' : '541'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?sky\.com\.br(\/.*)?$/, 'id' : '542'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?uninove\.br(\/.*)?$/, 'id' : '543'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?gvt\.com\.br(\/.*)?$/, 'id' : '544'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?renault\.com\.br(\/.*)?$/, 'id' : '545'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?hyundai\.com(\/.*)?$/, 'id' : '546'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?gojiberryburn\.com\.br(\/.*)?$/, 'id' : '547'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?passarela\.com(\.br)?(\/.*)?$/, 'id' : '548'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?catho\.com\.br(\/.*)?$/, 'id' : '549'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?dafitisports\.com\.br(\/.*)?$/, 'id' : '550'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?fiat\.com\.br(\/.*)?$/, 'id' : '551'},
        {'re' : /^http(s)?:\/\/www\.empiricus\.com\.br(\/.*)?$/, 'id' : '552'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?unigranrio(\.edu)?\.br(\/.*)?$/, 'id' : '553'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?vestibularunigranrio\.com\.br(\/.*)?$/, 'id' : '553'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?net(combo)?\.com\.br(\/.*)?$/, 'id' : '554'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?tecnisa\.com\.br\/lp\/promocaorelampago(\/)?$/, 'id' : '555'},
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?tecnisa\.com\.br(\/.*)?$/, 'id' : '556'}, 
        {'re' : /^http(s)?:\/\/([a-z0-9-]*\.)?esser\.com\.br(\/.*)?$/, 'id' : '557'}
    ]
};
TM.pageBanners = [
    {
        'id': 'banner-1190x70-2',
        'outofpage': true,
        'width': 985,
        'height': 60,
        'dfpPos': 'top',
        'pos': 2
    },
    {
        'id': 'banner-1x1-11',
        'width': 1,
        'height': 1,
        'dfpPos': 'top',
        'pos': 11
    },
    {
        'id': 'banner-180x110-1',
        'width': 180,
        'height': 110,
        'dfpPos': 'top',
        'pos': 1
    },
    {
        'id': 'banner-200x446-6',
        'width': 200,
        'height': 446,
        'dfpPos': 'top',
        'pos': 6
    },
    {
        'id': 'banner-300x250-5',
        'width': 300,
        'height': 250,
        'dfpPos': 'top',
        'pos': 5
    },
    {
        'id': 'banner-300x250-8',
        'width': 300,
        'height': 250,
        // 'action':'http://bn.imguol.com/1503/pagseguro/300x250.gif',
        'dfpPos': 'middle',
        'pos': 8
    },
    {
        'id': 'banner-300x250-10',
        'width': 300,
        'height': 250,
        // 'action':'http://bn.imguol.com/1503/pagseguro/300x250-3.gif',
        'dfpPos': 'bottom',
        'pos': 10
    },
    {
        'id': 'banner-300x250-14',
        'width': 300,
        'height': 250,
        // 'action':'http://bn.imguol.com/1503/pagseguro/300x250-4.gif',
        'dfpPos': 'footer',
        'pos': 14
    },
    {
        'id': 'banner-1190x70-3',
		//'action': 'hide',
        'outofpage': true,
        'width': 620,
        'height': 60,
        'dfpPos': 'top',
        'pos': 1
    },
    {
        'id': 'banner-728x90-2',
        'width': 728,
        'height': 90,
        'action':'http://bn.imguol.com/1503/pagseguro/728x90.gif',
        // 'MapSizeEnable': true,
        // 'autoSize': googletag.sizeMapping().addSize([0, 0], [[728, 90],[970, 90]]).build(),
        'dfpPos': 'middle',
        'pos': 3
    },
    {
        'id': 'banner-300x600-2',
        'width': 300,
        'height': 600,
        'dfpPos': 'bottom',
        'pos': 3
    },
    {
        'id': 'banner-300x600-3',
        'width': 300,
        'height': 600,
        'action':'http://bn.imguol.com/1503/pagseguro/300x600.gif',
        //'action': {
        //    'tagName': 'script',
        //    'attributes': {
        //        'src': 'http://jsuol.com.br/p/dfp/home-300x600.js?v=1',
        //        'async': ''
        //    }
        //},
        'dfpPos': 'bottom',
        'pos': 3
    },
    {
        'id': 'banner-728x90-1',
        'width': 728,
        'height': 90,
        'MapSizeEnable': true,
        // 'autoSize': googletag.sizeMapping().addSize([0, 0], [[728, 90],[970, 90]]).build(),
        'dfpPos': 'bottom',
        'pos': 1
    },
    {
        'id': 'banner-300x250-4',
        'width': 300,
        'height': 250,
        'dfpPos': 'tv',
        'pos': 4
    }
];
for (var i = 0; i < TM.pageBanners.length; i++) {
    var banner = TM.pageBanners[i];
//    if(banner.id == "banner-728x90-2" && banner.MapSizeEnable){
//					var check970x90_2 = setInterval(function () {
//										check970x90ID_2=false;
//										try{check970x90ID_2 = (check970x90ID_2 == false)?document.getElementById("banner-728x90-2").getElementsByTagName("IFRAME")[0].id:check970x90ID_2;}catch(e){}
//										try{check970x90ID_2 = (check970x90ID_2 == false)?document.getElementById("banner-728x90-2").getElementsByTagName("OBJECT")[0].id:check970x90ID_2;}catch(e){}
//										try{check970x90ID_2 = (check970x90ID_2 == false)?document.getElementById("banner-728x90-2").getElementsByTagName("IMG")[0].id:check970x90ID_2;}catch(e){}
//										try{
//											if(document.getElementById(check970x90ID_2).style.width == "970px"){document.getElementById("banner-728x90-2-Area").style.width = "970px";}
//											if(document.getElementById(check970x90ID_2).width == "970"){document.getElementById("banner-728x90-2-Area").style.width = "970px";}
//											if(document.getElementById("banner-728x90-2-Area").style.width == "970px"){clearInterval(check970x90_2);}
//											if(document.getElementById(check970x90ID_2).style.width == "728px"){clearInterval(check970x90_2);}
//											if(document.getElementById(check970x90ID_2).width == "728"){clearInterval(check970x90_2);						}
//										}catch(e){}
//					}, 1000);
//    }
    if(banner.id == "banner-728x90-1" && banner.MapSizeEnable){
					var check970x90_1 = setInterval(function () {
										check970x90ID_1=false;
										try{check970x90ID_1 = (check970x90ID_1 == false)?document.getElementById("banner-728x90-1").getElementsByTagName("IFRAME")[0].id:check970x90ID_1;}catch(e){}
										try{check970x90ID_1 = (check970x90ID_1 == false)?document.getElementById("banner-728x90-1").getElementsByTagName("OBJECT")[0].id:check970x90ID_1;}catch(e){}
										try{check970x90ID_1 = (check970x90ID_1 == false)?document.getElementById("banner-728x90-1").getElementsByTagName("IMG")[0].id:check970x90ID_1;}catch(e){}
										try{
											if(document.getElementById(check970x90ID_1).style.width == "970px"){document.getElementById("banner-728x90-1-Area").style.width = "970px";}
											if(document.getElementById(check970x90ID_1).width == "970"){document.getElementById("banner-728x90-1-Area").style.width = "970px";}
											if(document.getElementById("banner-728x90-1-Area").style.width == "970px"){clearInterval(check970x90_1);}
											if(document.getElementById(check970x90ID_1).style.width == "728px"){clearInterval(check970x90_1);}
											if(document.getElementById(check970x90ID_1).width == "728"){clearInterval(check970x90_1);}
										}catch(e){}
					}, 1000);
    }
}
TM.message = new TM.LogUtils();
TM.log = {}, 
TM.log.banners = {}, 
TM.log.banners.available = [], 
TM.log.banners.calls = [];
TM.allowCuttingEdge = function() {
    TM.cecfg = 'CEHome';
    return (document.cookie.indexOf(TM.cecfg) == -1);
};
TM.allowPopup = function() {
    TM.popupcfg = 'POPhomeUOL';
    return (document.cookie.indexOf(TM.popupcfg) == -1);
};
TM.suffix = '-Area';
TM.getCookie = function(name, cookie) {
        return cookie.match(new RegExp("(?:^|;)\\s?" + name.replace(/([.*+?^=!:${}()|[\]\/\\])/g, "\\$1") + "=(.*?)(?:;|$)", "i")) && unescape(cookie.match(new RegExp("(?:^|;)\\s?" + name.replace(/([.*+?^=!:${}()|[\]\/\\])/g, "\\$1") + "=(.*?)(?:;|$)", "i"))[1]);
    };
var pvt = {};
TM.dfpSetup = function() {
    var AsiPlacementsUtils = function() {
    var op = Object.prototype, // utils for calling methods
        $public = this,
        $private = {
            ostring: op.toString, // used to invoiced comparator
            hasOwn: op.hasOwnProperty // alias to hasOwnProperty
        };
    $public.isArray = function(it) {
        return $private.ostring.call(it) === '[object Array]';
    };
    $public.isObject = function(it) {
        return $private.ostring.call(it) === '[object Object]';
    };
    $public.isArrayClean = function(it) {
        return it.length === 0;
    };
    $public.isDefined = function(obj) {
        return obj != null || obj !== undefined;
    }
    $public.isEmpty = function(it) { //accept object,array, string
        return (!$public.isDefined(it) || $public.isArrayClean(it)) ? true : //object undefined or length native 0 
            $public.isObject(it) ? $public.isArrayClean($public.keys(it)) :
            false;
    };
    $public.hasProp = function(obj, prop) {
        return $private.hasOwn.call(obj, prop);
    };
    $public.getOwn = function(obj, prop) {
        return $public.hasProp(obj, prop) && obj[prop];
    };
    $public.copy = function(obj) {
        if (!$public.isObject(obj)) {
            return obj;
        }
        var temp = obj.constructor();
        for (key in obj) {
            if ($public.hasProp(obj, key)) {
                temp[key] = $public.copy(obj[key])
            }
        }
        return temp;
    };
    $public.keys = function(obj) {
        if (!$public.isObject(obj)) {
            return [];
        }
        var keys = [];
        for (var key in obj) {
            if ($public.hasProp(obj, key)) {
                keys.push(key);
            }
        }
        return keys;
    };
    return $public;
};
/**
 * Object AsiPlacements
 */
var AsiPlacements = function(globals) {
    var util = new AsiPlacementsUtils(),
        $public = this,
        $private = {};
    $private.asiPlacementsCopy = {};
    $private.pushInGlobalScope = function(key, value) {
        globals[key] = value;
    };
    $private.hasAsiPlacements = function() {
        var asiPlacements = util.getOwn(globals, "asiPlacements");
        return ((asiPlacements && util.isObject(asiPlacements)) &&
            !util.isEmpty(asiPlacements)) && asiPlacements
    };
    $private.hasDataPlacements = function(asiPlacement) {
        var dataPlacements = util.getOwn(asiPlacement, "data");
        return ((dataPlacements && util.isObject(dataPlacements)) &&
            !util.isEmpty(dataPlacements)) && dataPlacements;
    };
    $private.removeAsiPlacement = function(asiPlacements, key) {
        delete asiPlacements[key];
    };
    $private.hasGlobalScope = function() {
        var asiPlacements;
        if (!(asiPlacements = $private.hasAsiPlacements())) {
            return;
        }
        $private.asiPlacementsCopy = util.copy(asiPlacements);
        for (var i = 0,
                ary = util.keys($private.asiPlacementsCopy),
                length = ary.length; i < length; i++) {
            var currentItem = $private.asiPlacementsCopy[ary[i]],
                dataPlacements = $private.hasDataPlacements(currentItem);
            if (!dataPlacements) { //valid data in object
                $private.pushInGlobalScope("ASPQ_" + ary[i], "");
                $private.removeAsiPlacement($private.asiPlacementsCopy, ary[i]) // remove element empty 
                continue;
            }
            $private.pushInGlobalScope("ASPQ_" + ary[i], "PQ_" + ary[i] + "_" + util.keys(dataPlacements)[0]);
        }    };
    /**
     * public object AsiPlacements
     */
    $public.getGwdPlacementsList = function() {
        $public.init();
        return util.keys($private.asiPlacementsCopy);
    };
    $public.existsAsiPlacements = function() {
        return $private.hasAsiPlacements();
    };
    $public.init = function() {
        $private.hasGlobalScope();
    };
    return $public;
};
 asiPlacementsGlobals = new AsiPlacements(window);
 asiPlacementsGlobals.init();
    var cookieUtils = new TM.CookieUtils();
    var dfpSetup = '10411447',
    goog = window.googletag,
    pubads = goog.pubads;
    bannersLength = TM.pageBanners.length;
    TM.log.dfp = dfpSetup;
    for (var i = 0; i < bannersLength; i++) {
        var banner = TM.pageBanners[i],
        id = banner.id,
        width = banner.width,
        height = banner.height,
        size = width + "x" + height,
        dfpPos = banner.dfpPos;
        if (!banner.action) {
            if (width != 1 || height != 1 || TM.allowPopup()){
                if(document.location.href.match(/arquivo/i)) {
                    dfpSetup = '10411447';
                }
                dfpSetupBanner = dfpSetup + "/" + size + "_" + dfpPos;
                var rtp = window.rtp || false;
                if(rtp && rtp[banner.id] && rtp[banner.id].tier) {
                   var slot = goog.defineSlot(dfpSetup, [banner.width, banner.height], banner.id).addService(pubads());
                   slot.setTargeting('pos'  , banner.id);
                   slot.setTargeting('tier' , rtp[banner.id].tier);
                   slot.setTargeting('cpm'  , rtp[banner.id].cpm);
                   slot.setTargeting('deals', rtp[banner.id].deals);
                }else {
                    var autoSize = false;
                    autoSize = (banner.autoSize && !autoSize && banner.MapSizeEnable) ? mapping = banner.autoSize : false;
                    if(id != 'banner-985x60-2' && id != 'banner-620x60-1' && !banner.outofpage){
                        if(autoSize){
                            goog.defineSlot(dfpSetupBanner, [width, height], id).defineSizeMapping(mapping).addService(googletag.pubads()).setTargeting("pos", banner.dfpPos);
                        } else {
                            goog.defineSlot(dfpSetupBanner, [width, height], id).addService(googletag.pubads()).setTargeting("pos", dfpPos);
                        }
                    } else if (TM.allowCuttingEdge() || id == 'banner-620x60-1') {
                        goog.defineOutOfPageSlot(dfpSetupBanner, id).addService(googletag.pubads()).setTargeting("pos", dfpPos);
                    }
                }
            }
        }
        pubads().setTargeting("gwd",
            audienceScienceCollect.getPlacementByBanner(banner.id, asiPlacementsGlobals.getGwdPlacementsList()));
    }
    pubads().setTargeting("expble", "1");
    pubads().setTargeting("referer", TM.currHref);
    pubads().setTargeting("campaignuol", '1');
    var bt = cookieUtils.getCookie("BT");
    var retargeting = cookieUtils.getCookie("DEretargeting");
    var length = TmConfig.ClientTrack.sites.length;
    for (i = 0; i < length; i++) {
        var btId = TmConfig.ClientTrack.sites[i].id;
        if (bt.indexOf(btId) != -1) {
            bt = bt + ';1' + btId + ';';
        }
    }
    if(retargeting){
        bt = (bt || ";") + retargeting + ";";
    }
    if (bt) {
        var aux = bt.split(';').sort();
        bt = [];
        var arrayUtils = new TM.ArrayUtils();
        for (i = 0; i < aux.length; i++) {
            if( aux[i] && !arrayUtils.arrayContains(bt, aux[i]) ) {
                bt.push(aux[i]);
            }
        }
        var dntCode = 9000;  // DNT = 0
        if (navigator.doNotTrack == "yes") {
            dntCode = 9003;  // DNT = ?
        } else if ((navigator.doNotTrack == "1") || (window.doNotTrack == "1") || (navigator.msDoNotTrack == "1")) {
            dntCode = 9001;  // DNT = 1
        }
        bt.push(dntCode);
        pubads().setTargeting("bt", bt);
    }
    function TypeValidator () {
    var $public = this;
    var $static = $public.constructor.prototype;
    $static.isDefined = function (value) {
        return value !== undefined;
    };
    $static.isString = function (value) {
        return value !== undefined && (typeof value) === 'string';
    };
    $static.isArray = function (value) {
        return value !== undefined && value instanceof Array;
    };
    $static.isObject = function (entity) {
        return entity && entity.constructor === Object;
    };
    $static.isFunction = function (value) {
        return value !== undefined && value instanceof Function;
    };
    $static.isNumber = function (value) {
        return Number(value) === value;
    };
    $static.isInt = function (value) {
        return $static.isNumber(value) && value % 1 === 0;
    };
    $static.isRegExp = function (value) {
        return value !== undefined && value.constructor === RegExp;
    };
    return $public;
}
function DFPTrack () {
    var $private = {};
    var $public = this;
    var $static = $public.constructor.prototype;
    $private.validator = new TypeValidator();
    $private.targets = {
        'tt_age': 'getAge',
        'tt_gender': 'getGender',
        'tt_cluster': 'getProfiles',
        'tt_subjects': 'getSubjects',
        'tt_team': 'getTeam',
        'tt_socialclass' : 'getSocialClass',
        'tt_microsegments' : 'getMicrosegments',
        'tt_lists' : 'getLists',
        'tt_customaudience' : 'getCustomAudience'
    };
    $public.tracked = {};
    $private.getCookieValue = function (cookieName) {
        var cookie = document.cookie;
        var startIndex = cookie.indexOf(cookieName);
        if (startIndex === -1) {
            return "";
        }
        var middleIndex = cookie.indexOf('=', startIndex) + 1;
        var endIndex = cookie.indexOf(';', middleIndex);
        if (endIndex === -1){
            endIndex = cookie.length;
        }
        return unescape(cookie.substring(middleIndex, endIndex));
    };
    $private.getProfilesFromCookie = function() {
        var _ttprofiles = {};
        var cookie = $private.getCookieValue("_ttprofiles");
        if (typeof cookie == "undefined" || !cookie) {
            return;
        }
        cookie = cookie.split("|");
        for (var i = 0, length = cookie.length; i < length; i++) {
            var key = cookie[i].split(":")[0];
            var value = cookie[i].split(":")[1];
            value = (value.indexOf(",") != -1 ? value.split(",") : value);
            _ttprofiles[key] = value;
        }
        return _ttprofiles;
    };
    $public.setProfiles = function (key, value) {
        if (!$private.validator.isString(key)) {
            return false;
        }
        if (!$private.validator.isString(value)) {
            return false;
        }
        window.localStorage.setItem(key, value);
        return true;
    };
    $public.getProfiles = function (key) {
        var profiles = window.localStorage.getItem(key);
        if (!$private.validator.isString(profiles)) {
            var profilesFromCookie = $private.getProfilesFromCookie();
            if (profilesFromCookie) {
                return profilesFromCookie;
            }
            return;
        }
        try {
            return JSON.parse(profiles);
        } catch (e) {
            return;
        }
    };
    $private.setTargeting = function (obj) {
        for (var key in $private.targets) {
            var value = obj[$private.targets[key]];
            if (!$private.validator.isDefined(value)) {
                continue;
            }
            if ($private.validator.isArray(value)) {
                value = value.join(',');
            }
            value = value.toString();
            try {
                $public.tracked[key] = value;
                googletag.pubads().setTargeting(key, value);
            } catch (err) {
                continue;
            }
        }
        return true;
    };
    $private.setTargetFromLocalStorage = function () {
        var _ttprofiles = $public.getProfiles('tailtarget');
        if (!$private.validator.isObject(_ttprofiles)) {
            return false;
        }
        return $private.setTargeting(_ttprofiles);
    };
    $private.hasRemoteStorage = function () {
        var UOLPD = window.UOLPD;
        if (!$private.validator.isDefined(UOLPD)) {
            return false;
        }
        if (!$private.validator.isDefined(UOLPD.TagManager)) {
            return false;
        }
        if (!$private.validator.isDefined(UOLPD.TagManager.TailtargetTrack)) {
            return false;
        }
        if (!$private.validator.isDefined(UOLPD.TagManager.TailtargetTrack.RemoteStorage)) {
            return false;
        }
        return true;
    };
    $private.updateLocalStorage = function () {
        var count = 0;
        var interval = setInterval(function () {
            var UOLPD = window.UOLPD;
            count += 1;
            if (count > 2100) {
                clearInterval(interval);
                return;
            }
            if (!$private.hasRemoteStorage()) {
                return;
            }
            UOLPD.TagManager.TailtargetTrack.RemoteStorage.get('tailtarget', function (key, value) {
               $public.setProfiles(key, value);
            });
            clearInterval(interval);
        }, 10);
    };
    $static.init = function () {
        $private.updateLocalStorage();
        if ($private.setTargetFromLocalStorage()) {
            return true;
        }
        return false;
    };
    return $public;
};
dfpTrack = new DFPTrack();
dfpTrack.init();
    pubads().enableSyncRendering();
    pubads().enableSingleRequest();
    try {
        nvg23947.dfpnvg();
    } catch(e){}
    goog.enableServices();
    pvt.isSetup = true;
};
TM.deConfig = function() {
    deConfig = {
        'site'        : 'uolbr',
        'chan'        : 'homeuol',
        'subchan'     : 'capa',
        'affiliate'   : 'uolbrhomeuol',
        'tm'          : '1',
        'reso'        : screen.width + "x" + screen.height,
        'tile'        : Math.floor((+new Date()) * 1000 * Math.random()),
        'expble'      : 1
    };
    return deConfig;
};
TM.currentBanner = function() {
    var doc = document,
    banner = {},
    scripts = doc.getElementsByTagName('script'),
    bannerBlock = scripts[scripts.length - 1].parentNode,
    bannerId = bannerBlock.id,
    found = false,
    pageBanners = TM.pageBanners;
    for (var i = 0, length = TM.pageBanners.length; i < length; i++) {
        banner = pageBanners[i];
        if(bannerId == banner.id) {
            banner.block = bannerBlock;
            found = true;
            break;
        }
    }
    if(!found) {
        return false;
    }
    if (doc.getElementById(bannerId + TM.suffix)) {
        if(banner.width != 1) {
            banner.area = doc.getElementById(bannerId + TM.suffix);
        }
    }
    return banner;
};
TM.makeDeUri = function(banner) {
    var url_base = 'http://bn.uol.com.br/js.ng/',
    querystring = [],
    deConfig = TM.deConfig;
    deConfig.size = banner.width + 'x' + banner.height;
    deConfig.page = banner.pos;
    var conntype = document.body;
    if(conntype){
        conntype.style.behavior = 'url(#default#clientCaps)';
        conntype = conntype.connectionType == 'lan' ? 1 : 0;
        deConfig.conntype = conntype || '0';
    }
    for(var key in deConfig) {
        var paramValue = deConfig[key];
        if(typeof paramValue == 'string' || typeof paramValue == 'number') {
            querystring.push([key, paramValue].join('='));
        }
    }
    return url_base + querystring.join("&") + "?";
};
TM.getPositionTop = function(area) {
    var elem = area;
    var pos = elem.offsetTop;
    while(true){
        if(!elem.offsetParent){
            break;
        }
        elem = elem.offsetParent;
        pos += elem.offsetTop;
    }
    return pos;
};
TM.getPositionLeft = function(area) {
    var elem = area;
    var pos = elem.offsetLeft;
    while(true){
        if(!elem.offsetParent){
            break;
        }
        elem = elem.offsetParent;
        pos += elem.offsetLeft;
    }
    return pos;
};
TM.position = function(banner) {
    if (typeof banner == 'object' && banner.id && banner.width != 1) {
        var area = banner.area,
        block = banner.block;
        if (typeof area == 'object') {
            var topPos = TM.getPositionTop(area);
            var leftPos = TM.getPositionLeft(area);
            block.style.position = 'absolute';
            block.style.top = topPos + 'px';
            block.style.left = leftPos + 'px';
        }
    }
    return true;
};
TM.enableReposition = function() {
    var minInterval = 100;
    var brw = navigator.userAgent.toLowerCase();
    if(brw && brw.match(/msie.(5|6|7)/)){
        minInterval = 1000;
    }
    TM.checkInterval = TM.checkInterval || minInterval;
    TM.addEvent("resize", window, TM.reposition);
    setTimeout(TM.repositionLoop, TM.checkInterval);
};
TM.reposition = function() {
    for (var i = 0, length = TM.displayedBanners.length; i <= length; i++) {
        TM.position(TM.displayedBanners[i]);
    }
};
TM.repositionLoop = function() {
    TM.reposition();
    setTimeout(TM.repositionLoop, TM.checkInterval);
};
TM.addEvent = function(ev, elem, func) {
    if (window.addEventListener) {
        elem.addEventListener(ev, func, false);
    } else {
        if (window.attachEvent) {
            elem.attachEvent("on" + ev, func);
        }
    }
};
TM.display = function() {
    if (!pvt.isSetup) {
        TM.dfpSetup();
    }
    var log = new TM.LogUtils();
    var banner = TM.currentBanner();
    var doc = document;
    var displayAction = function (banner) {
        var action = banner.action;
        if (action.tagName) {
            var tag = doc.createElement(action.tagName);
            if (!action.attributes.width) {
                action.attributes.width = banner.width;
            }
            if (!action.attributes.height) {
                action.attributes.height = banner.height;
            }
            for (var attr in action.attributes) {
                tag.setAttribute(attr, action.attributes[attr]);
            }
            banner.block.appendChild(tag);
            return true;
        } else if (action.match(/^http(s)?:\/\/.+$/)) {
            doc.write('<img src="' + action + '" alt="">');
            return true;
        } else if (action == 'hide') {
            TM.hideBanner(banner);
            return true;
        }
        return;
    };
    var displayDE = function (banner) {
        var deUri = TM.makeDeUri(banner);
        TM.deCalls = TM.deCalls || [];
        for(var i = 0, length = TM.deCalls.length; i < length; i++) {
            if(deUri == TM.deCalls[i]) {
                TM.message.log('URL [' + deUri + '] já foi solicitada anteriormente ao AdServer');
                return false;
            }
        }
        TM.deCalls.push(deUri);
        doc.write('<script type="text/javascript" src="' + deUri + '"><\/script>');
    };
    if (!banner || typeof banner != 'object') {
        return false;
    }
    TM.displayedBanners = TM.displayedBanners || [];
    for (var i = 0, length = TM.displayedBanners.length; i < length; i++) {
        if(banner.id && banner.id == TM.displayedBanners[i].id) {
            TM.message.log('Banner [' + banner.id + '] já foi exibido anteriormente');
            return false;
        }
    }
    if(banner.width == 1) {
        if (!TM.allowPopup()) {
            return false;
        }
        doc.cookie = TM.popupcfg + "=0";
    }
    if (banner.outofpage === true && !TM.allowCuttingEdge()) {
        return false;
    }
    if (banner.action) {
        displayAction(banner);
    }
    if(TM.adServer == 'dfp' && !banner.action) {
        googletag.display(banner.id);
        TM.dfpCalls = TM.dfpCalls || [];
        TM.dfpCalls.push(banner);
    } else if (TM.adServer != 'dfp' || banner.action == 'de') {
        displayDE(banner);
    }
    if(banner.area){
        TM.position(banner);
        if(!TM.repositionEnable) {
            TM.repositionEnable = true;
            TM.enableReposition();
        }
    }
    TM.displayedBanners.push(banner);
    return true;
};
TM.hideBanner = function(banner) {
    var parent;
    banner.block.style.display = 'none';
    if(banner.area && typeof banner.area == 'object') {
        banner.area.style.display = 'none';
        parent = banner.area.parentNode || false;
        if(parent && parent.className && (parent.className == 'publicidade' || parent.className == 'pub')){
            parent.style.display = 'none';
        }
    } else {
        parent = banner.block.parentNode;
        if(parent && parent.className && (parent.className == 'publicidade' || parent.className == 'pub')){
            parent.style.display = 'none';
        }
    }
};
TM.SIGAReady = function (timeout) {
    if (!TM.SIGA) {
        return;
    }
    timeout = (timeout ? timeout + 50 : 10);
    var hasClass = function (element, className) {
        if (!element) {
            return false;
        }
        var classList = element.className.split(' ');
        var length = length = classList.length;
        for (var i = 0; i < length; i++) {
            if (classList[i] == className) {
                return true;
            }
        }
        return false;
    }
    var isSIGA = function (target) {
        if (!hasClass(target, 'tm-ads')) {
            return false;
        }
        return (target.getAttribute('siga') || target.getAttribute('data-siga'));
    }
    var hasSiga = function () {
        var elements = TM.getElementsByClassName('tm-ads');
        for (var i = 0; i < length; i++) {
            if(isSIGA(elements[i])) {
                return true;
            }
        }
        return false;
    };
    if (!hasSiga()) {
        setTimeout(function () {
            TM.SIGAReady(timeout);
        }, timeout);
    } else {
        TM.SIGA.init();
    }
};
TM.dfpReady = function(fn, timeout) {
    timeout = (timeout ? timeout + 50 : 1);
    if (typeof eval(fn) != "function") {
        return false;
    }
    if(window.googletag && googletag.apiReady) {
        fn();
    } else {
        setTimeout(function() {
            TM.dfpReady(fn, timeout);
        }, timeout);
    }
};
TM.initDFP = function () {
    TM.adServer = adServer;
    TM.pageBanners = TM.setupPageBanners();
    TM.deConfig = TM.deConfig();
    TM.dfpSetup();
};
TM.init = function(adServer) {
    TM.adServer = adServer;
    TM.currHref = document.location.href;
    TM.deConfig = TM.deConfig();
    if (TM.adServer == 'dfp') {
        TM.dfpReady(TM.dfpSetup, 10);
    }
    TM.SIGAReady();
};
TM.init(adServer);
        }());