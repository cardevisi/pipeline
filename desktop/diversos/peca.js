


1) Passo a passo do fluxo de renderização e funcionamento das peças em geral.

1.1) Evento de abertura 
    1.1.1) Os eventos atuais são click e mouseover.
1.2) Expansão 
    1.2.1) O comportamento atual pode ser resize do tamanho ou criação de um segundo container, fullscreen
1.3) Evento de clickTag
    1.3.1) A clicktag atualemente suporta a configuração de uma ou mais urls de destino.
1.4) Traqueamento
    1.4.1) Poderão ser traqueados os eventos de abertura, expansão e outros


2) Retração
    
2.1) Evento de fechamento
    2.1.1) Os eventos atuais são mouseleave e mouseout.
2.2) Retração
    1.2.1) O comportamento atual pode ser resize para o formato inicial ou fechamento do segundo container
2.3) Traqueamento
    2.3.1) Poderão ser traqueados os eventos de fechamento, retração e outros
2.4) Reload
    2.4.1) Reinicia a animação da primeira peça.


No DFP a implementação de uma peça como HTML5 é considerada uma implementação personalizada.