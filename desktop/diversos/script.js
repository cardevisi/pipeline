<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
        
        function UOLBannerCreator () {
            
            function create() {
                setEvents();
                registerFormat();
            };

            function resize (width, height) {
                var iframe = document.querySelector('iframe');
                iframe.height = height + "px !important";
            };

            function clickTag () {
                window.open('%%CLICK_URL_UNESC%%', 'clickTag');
            };

            function setEvents() {
                var iframe = document.querySelector('iframe');
                iframe.addEventListener('mouseover', function() {
                    expandByDimensions(210);
                    resize('728', '300');
                });
                iframe.addEventListener('mouseout', function() {
                    collapse();
                    resize('728', '90');
                });
                iframe.addEventListener('onclick', function() {
                    console.log("CLICK");
                    clickTag();
                });
            }
            
            this.loadIframe = function(url, width, height) {
                var iframe = document.createElement('iframe');
                iframe.setAttribute('width', width);
                iframe.setAttribute('height', height);
                iframe.setAttribute('marginwidth', '0');
                iframe.setAttribute('marginheight', '0');
                iframe.setAttribute('frameborder', '0');
                iframe.setAttribute('scrolling', 'no');
                iframe.setAttribute('src', url);
                iframe.setAttribute('background', 'transparent');

                
                var div = document.querySelector('#banner728x90');
                div.appendChild(iframe);
                create();

            }

            function expandByDimensions(height) {
                console.log("EXPANDBYDIMENSIONS::::: MOUSOVER");
                try{
                    $sf.ext.expand({l:0,t:0,r:0,b:height,push:false});
                } catch (e) {
                    console.log('safeframe desabilitado - abilitar opção no DFP');
                }
            }

            function collapse() {
                console.log("COLLAPSE::::: MOUSEOUT");
                try{
                    $sf.ext.collapse();
                } catch(e) {
                    console.log('safeframe desabilitado - abilitar opção no DFP');
                }
            }

            function registerFormat(){
                try {
                    $sf.ext.register(728, 90, function(status, data) {
                        if (status === 'geom-update') {
                            return;
                        }
                    });
                } catch (e) {
                    console.log('safeframe desabilitado - abilitar opção no DFP');
                }
            }

        }
        
        var banner = new UOLBannerCreator();
        window.load = banner.loadIframe;
    </script>
    <style type="text/css">
        #728x90 {
            width: 728px;
            height: 90px;
        }
        iframe {
            marginwidth: 0;
            marginheight: 0;
            frameborder: 0;
            scrolling: "no";
            background:transparent;
    }
        .main-text {
            width: 900px;
        }
    </style>
</head>
<body onload="load('%%FILE:HTML1%%', '728', 90);">
<div id="banner728x90"></div>
</body>
</html>


