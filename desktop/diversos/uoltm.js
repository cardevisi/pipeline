(function (window, document, undefined){
'use strict';
function Logs () {
    var $private = {};
    var $public = this;
    $private.prefix = "[UOLPD.TagManager] ";
    $public.history = {};
    $private.typeValidator = new TypeValidator();
    $private.stringUtils = new StringUtils();
    $public.warn = function(msg, isActive) {
        return $public.print(msg, 'warn', isActive);
    };
    $public.error = function(msg, isActive) {
        return $public.print(msg, 'error', isActive);
    };
    $public.info = function(msg, isActive) {
        return $public.print(msg, 'info', isActive);
    };
    $public.debug = function(msg, isActive) {
        return $public.print(msg, 'debug', isActive);
    };
    $public.log = function(msg, isActive) {
        return $public.print(msg, 'log', isActive);
    };
    $public.print = function(msg, fn, isActive) {
        if (!$private.typeValidator.isString(fn)) {
            return;
        }
        msg = $private.getMessage(msg);
        if (!msg) {
            return;
        }
        if (!$private.typeValidator.isBoolean(isActive)) {
            isActive = $public.isActive();
        }
        if (isActive === false) {
            $public.setHistory(fn, msg);
            return;
        }
        fn = $public.validateFunction(fn, msg);
        if (!fn) {
            return;
        }
        if(console[fn]){
            console[fn](msg);
        }
        $public.setHistory(fn, msg);
        return msg;
    };
    $private.getMessage = function (msg) {
        msg = $private.stringUtils.trim(msg);
        if (!$private.typeValidator.isString(msg)) {
            return;
        }
        return $private.prefix + msg;
    };
    $public.isActive = function (query) {
        if ($public.getQueryString('tm', query) == 'debug') {
            return true;
        }
        return false;
    };
    $public.getQueryString = function(name, query) {
        return $private.stringUtils.getValueFromKeyInString((query || $public.getLocationSearch()), name, '&');
    };
    $public.getLocationSearch = function () {
        return window.location.search;
    };
    $public.setHistory = function (fn, msg) {
        if (!$private.typeValidator.isString(fn)) {
            return;
        }
        if (!$private.typeValidator.isString(msg)) {
            return;
        }
        $public.history[fn] = $public.history[fn] || [];
        $public.history[fn].push(msg);
    };
    $public.validateFunction = function(fn, msg) {
        if (!fn) {
            return false;
        }
        if (!window.console || !console){
            $public.setHistory(fn, msg);
            return false;
        }
        if (!console[fn] && fn == 'log') {
            $public.setHistory(fn, msg);
            return false;
        }
        if (!fn || !console[fn]) {
            $public.setHistory(fn, msg);
            fn = "log";
        }
        return fn;
    };
    return $public;
}
function TypeValidator () {
    var $public = this;
    $public.isDefined = function (value) {
        return value !== undefined;
    };
    $public.isString = function (value) {
        return value !== undefined && (typeof value) === 'string';
    };
    $public.isBoolean = function (value) {
        return value !== undefined && (typeof value) === 'boolean';
    };
    $public.isArray = function (value) {
        return value !== undefined && value instanceof Array;
    };
    $public.isObject = function (entity) {
        return entity && entity.constructor === Object;
    };
    $public.isFunction = function (value) {
        return value !== undefined && value instanceof Function;
    };
    $public.isNumber = function (value) {
        return Number(value) === value;
    };
    $public.isInt = function (value) {
        return $public.isNumber(value) && value % 1 === 0;
    };
    $public.isRegExp = function (value) {
        return value !== undefined && value.constructor === RegExp;
    };
}
function ScriptUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.hasTagScript = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return false;
        }
        if (src.length === 0) {
            return false;
        }
        src = $private.removeProtocol(src);
        var scripts = document.getElementsByTagName('script');
        var length = scripts.length;
        for (var i = 0; i < length; i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if (scriptSrc && $private.isSrcEqual(scriptSrc, src)) {
                return true;
            }
        }
        return false;
    };
    $private.removeProtocol = function (value) {
        return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };
    $private.isSrcEqual = function (src1, src2) {
        return (src1.indexOf(src2) > -1);
    };
    $public.appendTag = function (script) {
        if (!$private.typeValidator.isObject(script)) {
            return false;
        }
        if (!$private.typeValidator.isBoolean(script.async)) {
            return false;
        }
        if (!$private.typeValidator.isString(script.src)) {
            return false;
        }
        if (script.async === true) {
            $private.createAsyncScript(script.src);
            return true;
        } else {
            $private.createSyncScript(script.src);
            return true;
        }
        return false;
    };
    $private.createAsyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        tag.async = true;
        $private.lastScriptsParent().appendChild(tag);
    };
    $private.lastScriptsParent = function () {
        return document.getElementsByTagName('script')[0].parentNode;
    };
    $private.createSyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        document.write('<scr' + 'ipt src="'+ src + '"></scr' + 'ipt>');
    };    
}
function StringUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.trim = function (value) {
        if (!$private.typeValidator.isString(value)) {
            return;
        }
        if (value.length === 0) {
            return;
        }
        value = value.replace(/^(\s+)|(\s+)$/gm, '').replace(/\s+/gm, ' ');
        return value;
    };
    $public.getValueFromKeyInString = function (str, name, separator) {
        if (!$private.typeValidator.isString(name) || name.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(str) || str.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(separator) || separator.length === 0) {
            return;
        }
        if (str.substring(str.length - 1)) {
            str += separator;
        }
        name += '=';
        var startIndex = str.indexOf(name);
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = str.indexOf('=', startIndex) + 1;
        var endIndex = str.indexOf(separator, middleIndex);
        if (endIndex === -1){
            endIndex = str.length;
        }
        return unescape(str.substring(middleIndex, endIndex));
    };
    return $public;
}
function TrackManager () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.imagesSrc = [];
    $private.raffledRate = Math.round(Math.random() * 100);
    $private.samplingRate = -1;
    $public.sendPixel = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }
        if (!$private.isTrackEnabled()) {
            return;
        }
        var img = document.createElement('img');
        img.setAttribute('src', src);
        $public.imagesSrc.push(img.src);
    };
    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };
    $private.isTrackEnabled = function () {
        var raffledRate = $public.getRaffledRate();
        var isTrackEnabled = raffledRate <= $private.samplingRate;
        return isTrackEnabled;
    };
    return $public;
}
function NameSpace (packageName, version) {
    var $private = {};
    var $public = this;
    $private.version = version;
    $private.validator = new TypeValidator();
    $public.init = function (packageName) {
        $private.createUOLPD();
        $private.createTagManager();
        return $private.createPackage(packageName);
    };
    $private.createUOLPD = function () {
        if (!$private.validator.isObject(window.UOLPD)) {
            window.UOLPD = {};
        }
    };
    $private.createTagManager = function () {
        if (!$private.validator.isObject(window.UOLPD.TagManager) && !$private.validator.isFunction(window.UOLPD.TagManager)) {
            window.UOLPD.TagManager = {};
        }
        $private.setCommons(window.UOLPD.TagManager);
    };
    $private.setCommons = function (nameSpace) {
        if ($private.validator.isObject(nameSpace.commons)) {
            return;
        }
        nameSpace.commons = {};
        nameSpace.commons.logs = new Logs();
        nameSpace.commons.stringUtils = new StringUtils();
        nameSpace.commons.scriptUtils = new ScriptUtils();
        nameSpace.commons.typeValidator = new TypeValidator();
    };
    $private.createPackage = function (packageName) {
        if (!$private.validator.isString(packageName)) {
            return window.UOLPD.TagManager;
        }
        $private.setPackage(window.UOLPD.TagManager);
        $private.setVersion(window.UOLPD.TagManager[packageName]);
        $private.setConfig(window.UOLPD.TagManager[packageName]);
        return window.UOLPD.TagManager[packageName];
    };
    $private.setPackage = function (nameSpace) {
        if (!$private.validator.isObject(nameSpace[packageName])) {
            nameSpace[packageName] = {};
        }
    };
    $private.setVersion = function (nameSpace) {
        nameSpace.version = $private.version;
    };
    $private.setConfig = function (nameSpace) {
        if (!$private.validator.isArray(nameSpace.config)) {
            nameSpace.config = [];
        }
    };
    return $public.init(packageName);
}
var defaultTags = [
    {
        'module' : 'DNA',
        'async' : true,
        'rules' : {
            'enable' : ['document.location.href.match(/\\.uol\\.com\\.br/)'],
            'disable' : []
        },
        'events': ['onload'],
        'src' : '//tm.jsuol.com.br/modules/uol-dna.js'
    },
    {
        "module" : "CodeInjector",
        "async" : true,
        "rules" : {
            "enable" : [ "document.location.href.match(/.*/)" ],
            "disable" : [ ]
        },
        "events" : [ "onload" ],
        "config" : {
            "codeInjector" : "(function () {var script = document.createElement('script'); if (window.location.protocol === 'http:') {script.setAttribute('src', '//tracker.bt.uol.com.br/partner?source=tagmanager'); } if (window.location.protocol === 'https:') {script.setAttribute('src', 'https://tracker.bt.uol.com.br/track?source=tagmanager'); } document.body.appendChild(script); })()"
        },
        "src" : "//tm.jsuol.com.br/modules/code-injector.js"
    }
];
function VersionManager () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $private.validator = new TypeValidator();
    $private.stringUtils = new StringUtils();
    $private.scriptUtils = new ScriptUtils();
    $private.tmVersionToken = 'uoltm_version';
    $public.setSpecificVersion = function () {
        if (!$public.specificVersionWasRequested()) {
            return;
        }
        var versionURL = $public.getSpecificVersionURL();
        $private.scriptUtils.appendTag({
            src: versionURL,
            async: false
        });
    };
    $public.specificVersionWasRequested = function () {
        var requestedVersion = $private.getRequestedVersion();
        var versionRegex = /^\d+$/gm;
        var isValidVersion = versionRegex.test(requestedVersion);
        if (!requestedVersion || !isValidVersion) {
            return false;
        }
        if ($private.hasVersionScriptInPage()) {
            return false;
        }
        return true;
    };
    $public.getSearch = function () {
        return window.location.search;
    };
    $public.getScriptUrl = function () {
        var __monitoracaoJsuol = 'tm.jsuol.com.br/bo/9v/bo9vwk.js';
        return __monitoracaoJsuol;
    };
    $public.getSpecificVersionURL = function () {
        var codRepository = $private.getCodRepository();
        var versionURL = $private.buildVersionURL(codRepository);
        return versionURL;
    };
    $private.getRequestedVersion = function () {
        var requestedVersion = $private.stringUtils.getValueFromKeyInString($public.getSearch(), $private.tmVersionToken, '&');
        return requestedVersion;
    };
    $private.hasVersionScriptInPage = function () {
        var versionURL = $public.getSpecificVersionURL();
        var scripts = document.querySelectorAll('script[src*="'+ versionURL +'"]');
        if (!scripts || !scripts.length) {
            return false;
        }
        return true;
    };
    $private.buildVersionURL = function (codRepository) {
        var mainPath = '//tm.jsuol.com.br/';
        var firstDirLevel = codRepository.substring(0, 2) + '/';
        var secondDirLevel = codRepository.substring(2, 4) + '/';
        var fullPath = mainPath + firstDirLevel + secondDirLevel + codRepository;
        var requestedVersion = $private.getRequestedVersion();
        if (requestedVersion != '0') {
            fullPath += '-' + $private.getRequestedVersion() + '.js';
        } else {
            fullPath += '.js';
        }
        return fullPath;
    };
    $private.getCodRepository = function () {
        var baseURLRegex = /[\w\d]{6}\.js$/gm;
        var codRepository = $public.getScriptUrl().match(baseURLRegex, '')[0];
        codRepository = codRepository.replace('.js', '');
        return codRepository;
    };
}
function TagManager (nameSpace) {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $private.validator = new TypeValidator();
    $private.scriptUtils = new ScriptUtils();
    $private.versionManager = new VersionManager();
    $private.trackManager = new TrackManager();
    $private.version = '1.1.109';
    $private.basePixel = "//clicklogger.rm.uol.com.br/splunk/?prd=98&grp=Origem:TM;tm_repo_id:bo9vwk&oper=1&";
    $public.tagsToLoad = [];
    $public.events = {
        'autostart': [],
        'onload': []
    };
    $public.init = function (tags) {
        if (!$private.validator.isArray(tags)) {
            return false;
        }
        tags = tags.concat(defaultTags);
        if ($private.versionManager.specificVersionWasRequested()) {
            return $private.versionManager.setSpecificVersion();
        }
        $private.trackManager.sendPixel($private.basePixel + "msr=Execucoes%20Iniciadas:1");
        return $private.createModules(tags);
    };
    $private.createModules = function (tags) {
        var result = true;
        for (var i = 0, length = tags.length; i < length; i++) {
            var tag = tags[i];
            if (!$private.configureTag(tag)) {
                result = false;
                continue;
            }
            $private.setScriptsToLoad(tag);
        }
        $private.loadScripts();
        $private.triggerOnDocComplete();
        window.triggerUOLTM = $private.triggerUOLTM;
        $private.trackManager.sendPixel($private.basePixel + "msr=Execucoes%20Finalizadas:1");
        return result;
    };
    $private.configureTag = function (tag) {
        if (!$private.isTagValid(tag)) {
            return false;
        }
        if (!$private.isTagEnabled(tag)) {
            return false;
        }
        return true;
    };
    $private.isTagValid = function (tag) {
        if (!$private.validator.isObject(tag)) {
            return false;
        }
        if (!$private.validator.isString(tag.module)) {
            return false;
        }
        if (!$private.validator.isBoolean(tag.async)) {
            return false;
        }
        if (!$private.validator.isObject(tag.rules)) {
            return false;
        }
        if (!$private.validator.isArray(tag.rules.enable)) {
            return false;
        }
        if (!$private.validator.isArray(tag.rules.disable)) {
            return false;
        }
        if (!$private.validator.isArray(tag.events)) {
            return false;
        }
        if (!$private.validator.isString(tag.src)) {
            return false;
        }
        if (tag.config && !$private.validator.isObject(tag.config)) {
            return false;
        }
        return true;
    };
    $private.isTagEnabled = function (tag) {
        var enable = tag.rules.enable;
        var disable = tag.rules.disable;
        if ($private.checkRules(disable)) {
            return false;
        }
        if ($private.checkRules(enable)) {
            return $private.checkEvents(tag);
        }
    };
    $private.checkRules = function (rules) {
        for (var i = 0; i < rules.length; i++) {
            if ($private.checkRule(rules[i])) {
                return true;
            }
        }
        return false;
    };
    $private.checkRule = function (rule) {
        try {
            if (eval(rule)) {
                return true;
            }
        } catch (e) {
            return false;
        }
    };
    $private.checkEvents = function (tag) {
        var events = tag.events;
        var length = events.length;
        if (length === 0) {
            $private.classifyTagEvent(tag, 'autostart');
            return true;
        }
        var hasValidEvents = false;
        for (var i = 0; i < length; i++) {
            var event = events[i];
            if (!$private.validator.isString(event)) {
                continue;
            }
            if (tag.async === false || tag.async === "false") {
                $private.classifyTagEvent(tag, 'autostart');
                hasValidEvents = true;
                continue;
            }
            $private.classifyTagEvent(tag, event);
            hasValidEvents = true;
        }
        return hasValidEvents;
    };
    $private.classifyTagEvent = function (tag, event) {
        // Será removido quando o init do autostart e de eventos custom forem implementados
        if (event === 'autostart') {
            $private.setConfig(tag);
        }
        $public.events[event] = $public.events[event] || [];
        $public.events[event].push({
            module: tag.module,
            config: tag.config
        });
    };
    $private.setConfig = function (tag) {
        nameSpace[tag.module] = nameSpace[tag.module] || {};
        nameSpace[tag.module].config = nameSpace[tag.module].config || [];
        nameSpace[tag.module].config.push(tag.config);
    };
    $private.setScriptsToLoad = function (tag) {
        var scriptToLoad = {
            async: tag.async,
            src: tag.src
        };
        if (!$public.tagsToLoad.length) {
            $public.tagsToLoad.push(scriptToLoad);
            return;
        }
        var srcIndex = $private.indexOfTagsToLoad(tag.src);
        if (srcIndex === -1) {
            $public.tagsToLoad.push(scriptToLoad);
            return;
        }
        var currentScript = $public.tagsToLoad[srcIndex];
        if (currentScript.async === false && tag.async === true) {
            scriptToLoad.async = false;
            $public.tagsToLoad[srcIndex] = scriptToLoad;
        }
    };
    $private.indexOfTagsToLoad = function (tagSrc) {
        for (var i = 0; i < $public.tagsToLoad.length; i++) {
            var currentTag = $public.tagsToLoad[i];
            if (currentTag.src === tagSrc) {
                return i;
            }
        }
        return -1;
    };
    $private.loadScripts = function () {
        for (var i = 0; i < $public.tagsToLoad.length; i++) {
            var script = $public.tagsToLoad[i];
            $private.scriptUtils.appendTag(script);
        }
    };
    $private.triggerOnDocComplete = function() {
        if (document.readyState === 'complete') {
            $private.triggerEvent('onload');
            return;
        }
        var method = (document.addEventListener) ? 'readystatechange' : 'onreadystatechange';
        var methodListener = (document.addEventListener) ? 'addEventListener' : 'attachEvent';
        document[methodListener](method, function() {
            if (document.readyState === 'complete') {
                $private.triggerEvent('onload');
            }
        });
    };
    $private.triggerEvent = function (eventType) {
        var currentEvent = $public.events[eventType];
        for (var i = 0, length = currentEvent.length; i < length; i++) {
            $private.initTrigger(currentEvent[i].module, currentEvent[i].config);
        }
    };
    $private.initTrigger = function (moduleName, config) {
        var plugin = $private.getModule(moduleName);
        if (!plugin) {
            setTimeout(function () {
                $private.initTrigger(moduleName, config);
            }, 5);
        } else if (typeof plugin.init === 'function') {
            plugin.init(config);
        }
    };
    $private.getModule = function (moduleName) {
        return UOLPD.TagManager[moduleName];
    };
    $private.triggerUOLTM = function(eventType) {
        if (!$private.validator.isString(eventType)) {
            $public.logs.warn('O gatilho solicitado deve ser uma string');
            return;
        }
        if (eventType === 'onload') {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "onload"');
            return;
        }
        if (eventType === 'autostart') {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "autostart"');
            return;
        }
        if (!$private.validator.isArray($public.events[eventType])) {
            $public.logs.warn('Não existe nenhuma tag a ser disparada com o gatilho solicitado');
            return;
        }
        $private.triggerEvent(eventType);
    };
    return $public;
}
var nameSpace = new NameSpace();
var tagManager = new TagManager(nameSpace);
tagManager.init([ {
  "module" : "ModulesInjectorAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "javascriptSource" : "//js.revsci.net/gateway/gw.js?csid=F09828&auto=t&bpid=UOL"
  },
  "src" : "//tm.jsuol.com.br/modules/modules-injector-async.js"
},
{
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "true" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "platform" : "web",
    "dfppath" : "/10411447/capa",
    "isCompanion" : "False",
    "outOfPage" : "0",
    "keyword" : null,
    "rotative" : null,
    "sizeMapping" : null,
    "bannerWidth" : "728",
    "pos" : "top",
    "expble" : "1",
    "campaignuol" : "1",
    "hide" : "False",
    "isIncremental" : "false",
    "bannerId" : "banner-728x90-area",
    "videoEmbedded" : "False",
    "bannerHeight" : "90",
    "group" : null
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "ModulesInjector",
  "async" : false,
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ "document.location.href.match(/^http(S)?:\\/\\/boaforma\\.uol\\.com\\.br(.*)?$/)", "document.location.href.match(/bannerasync/)" ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "javascriptSyncSource" : "//tm.uol.com.br/h/uol/mulher.js"
  },
  "src" : "//tm.jsuol.com.br/modules/modules-injector.js"
}, {
  "module" : "CodeInjector",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/bannerasync/)" ],
    "disable" : [ "document.location.href.match(/^http(S)?:\\/\\/boaforma\\.uol\\.com\\.br(.*)?$/)" ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "(function() {\n    window.TM = window.TM || {};\n    window.TM.display = window.TM.display || function() {};\n})()"
  },
  "src" : "//tm.jsuol.com.br/modules/code-injector.js"
}, {
  "module" : "ModulesInjector",
  "async" : false,
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "javascriptSyncSource" : "//www.googletagservices.com/tag/js/gpt.js"
  },
  "src" : "//tm.jsuol.com.br/modules/modules-injector.js"
}, {
  "module" : "ModulesInjector",
  "async" : false,
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "javascriptSyncSource" : "http://bn.imguol.com/1503/teads/teads_v1.js"
  },
  "src" : "//tm.jsuol.com.br/modules/modules-injector.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/boaforma\\.uol\\.com\\.br\\/infograficos(.*)?$/)", "document.location.href.match(/^http(S)?:\\/\\/boaforma\\.uol\\.com\\.br\\/(noticias|ultimas|quiz|blogs|busca|enquetes)(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://boaforma.uol.com.br/",
    "width" : "288",
    "urlColor" : "21D9BA",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "2",
    "titleColor" : "21D9BA",
    "descrColor" : "333333",
    "height" : "250",
    "formatId" : "90",
    "querySelector" : "[siga*=\"288x250\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/http(s)?:\\/\\/mulher\\.uol\\.com\\.br\\/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://mulher.uol.com.br/",
    "width" : "288",
    "urlColor" : "BF264D",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "2",
    "titleColor" : "BF264D",
    "descrColor" : "333333",
    "height" : "250",
    "formatId" : "90",
    "querySelector" : "[siga*=\"288x250\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/boaforma\\.uol\\.com\\.br\\/(fotos|album|segredo-das-famosas)(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://boaforma.uol.com.br/",
    "width" : "944",
    "urlColor" : "21D9BA",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "1",
    "titleColor" : "21D9BA",
    "descrColor" : "333333",
    "height" : "185",
    "formatId" : "85",
    "querySelector" : "[siga*=\"944x185\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/http(s)?:\\/\\/mulher\\.uol\\.com\\.br\\/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://mulher.uol.com.br/",
    "width" : "944",
    "urlColor" : "BF264D",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "1",
    "titleColor" : "BF264D",
    "descrColor" : "333333",
    "height" : "185",
    "formatId" : "85",
    "querySelector" : "[siga*=\"944x185\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(s)?:\\/\\/boaforma\\.uol\\.com\\.br(\\/)?(\\?.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://boaforma.uol.com.br/",
    "width" : "944",
    "urlColor" : "21D9BA",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "4",
    "titleColor" : "21D9BA",
    "descrColor" : "333333",
    "height" : "275",
    "formatId" : "{\"id\" : \"86_a\", \"weight\" : 50 }, {\"id\" : \"86_b\", \"weight\" : 50 }",
    "querySelector" : "[siga*=\"944x275\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(s)?:\\/\\/mulher\\.uol\\.com\\.br\\/((\\?|\\#).*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "ros" : null,
    "borderColor" : "f3f2f1",
    "flagRetarget" : "1",
    "flagBehaviour" : null,
    "urlReferer" : "http://mulher.uol.com.br/",
    "width" : "944",
    "urlColor" : "BF264D",
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "4",
    "titleColor" : "BF264D",
    "descrColor" : "333333",
    "height" : "275",
    "formatId" : "{\"id\" : \"86_a\", \"weight\" : 50 }, {\"id\" : \"86_b\", \"weight\" : 50 }",
    "querySelector" : "[siga*=\"944x275\"]",
    "flagKeyword" : null,
    "deslabel" : null
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "DynadVitrineShopping",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "placementId" : "121673",
    "channel" : "132",
    "samplingRate" : "100",
    "containerId" : "vitrine-shuol",
    "dominio" : "//t5.dynad.net"
  },
  "src" : "//tm.jsuol.com.br/modules/dynad-vitrine-shopping.js"
} ]);
})(this, document);
