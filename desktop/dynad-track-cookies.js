(function (window, document, undefined){
'use strict';
function TypeValidator(){var $public=this;var $private={};$public.isDefined=function(value){return value!==undefined&&value!==null;};$public.isString=function(value){return value!==undefined&&(typeof value)==="string"&&($private.stringIsNotEmpty(value));};$private.stringIsNotEmpty=function(string){if($private.trimString(string).length>0){return true;}return false;};$private.trimString=function(string){return string.replace(/^(\s+)|(\s+)$/gm,"").replace(/\s+/gm," ");};$public.isArray=function(value){return value&&value.constructor===Array;};$public.isObject=function(entity){return entity&&entity.constructor===Object;};$public.isFunction=function(value){return value!==undefined&&value.constructor===Function;};$public.isNumber=function(value){return Number(value)===value;};$public.isInt=function(value){return $public.isNumber(value)&&value%1===0;};$public.isRegExp=function(value){return value!==undefined&&value.constructor===RegExp;};$public.isNumericString=function(value){return $public.isString(value)&&!isNaN(value);};$public.isBoolean=function(value){return value!==undefined&&value.constructor==Boolean;};return $public;}function QueryString(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.getValue=function(name){if(!$private.queryStrings){return;}return $private.queryStrings[name];};$public.getQueryStrings=function(){return $private.queryStrings;};$public.setValues=function(){if(!$private.typeValidator.isString($public.getSearch())){return;}var substrings=$public.getSearch().substring(1).split("&");if(!$private.typeValidator.isArray(substrings)){return;}if(substrings.length===0){return;}for(var i=0,length=substrings.length;i<length;i++){if($private.typeValidator.isString(substrings[i])){$private.addQueryString(substrings[i]);}}};$public.getSearch=function(){return window.location.search;};$private.addQueryString=function(substring){var queryObject=$private.getQueryObject(substring);if(!queryObject){return;}$private.queryStrings=$private.queryStrings||{};$private.queryStrings[queryObject.key]=queryObject.value;};$private.getQueryObject=function(substring){var query=substring.split("=");if(!$private.typeValidator.isString(query[0])){return;}var result={"key":query[0],"value":"true"};if($private.typeValidator.isString(query[1])){result.value=query[1];}return result;};$public.setValues();}function Logs(){var $private={};var $public=this;$private.queryString=new QueryString();$private.typeValidator=new TypeValidator();$private.history={"warn":[],"error":[],"info":[],"debug":[],"log":[]};$public.getPrefix=function(prefix){return $private.prefix;};$public.setPrefix=function(prefix){if($private.typeValidator.isString(prefix)){$private.prefix="["+prefix+"] ";}};$public.warn=function(msg){return $private.print(msg,"warn");};$public.error=function(msg){return $private.print(msg,"error");};$public.info=function(msg){return $private.print(msg,"info");};$public.debug=function(msg){return $private.print(msg,"debug");};$public.log=function(msg){return $private.print(msg,"log");};$private.print=function(msg,fn){if(!$private.prefix){return;}if(!$private.typeValidator.isString(msg)){return;}msg=$private.prefix+msg;$public.setHistory(fn,msg);if($public.isActive()===false||!$private.hasConsole()){return;}return $private.runLogMethod(fn,msg);};$public.isActive=function(){if($private.queryString.getValue("tm")==="debug"){return true;}return false;};$public.getHistory=function(methodName){if($private.typeValidator.isArray($private.history[methodName])){return $private.history[methodName];}return;};$public.setHistory=function(fn,msg){if($private.typeValidator.isString(msg)&&$private.typeValidator.isArray($private.history[fn])){$private.history[fn].push(msg);}};$private.hasConsole=function(){if(!$private.typeValidator.isDefined($public.getConsole())){return false;}if(!$private.typeValidator.isDefined($public.getConsoleLog())){return false;}return true;};$public.getConsole=function(){return window.console;};$public.getConsoleLog=function(){return $public.getConsole().log;};$private.runLogMethod=function(fn,msg){if($private.typeValidator.isDefined($public.getConsole()[fn])){$public.getConsole()[fn](msg);return fn;}window.console.log(msg);return"log";};}function NameSpace(packageName){var $private={};var $public=this;$private.version="1.0.126";$private.validator=new TypeValidator();$public.init=function(packageName){if($private.validator.isString(packageName)){return $public.create(packageName);}};$public.create=function(packageName){$private.createUOLPD();$private.createTagManager();return $private.createPackage(packageName);};$private.createUOLPD=function(){if(!$private.validator.isObject(window.UOLPD)){window.UOLPD={};}};$private.createTagManager=function(){if(!$private.validator.isObject(UOLPD.TagManager)&&!$private.validator.isFunction(UOLPD.TagManager)&&typeof UOLPD.TagManager!=="object"){UOLPD.TagManager={};}};$private.createPackage=function(packageName){if(!$private.validator.isString(packageName)){return UOLPD.TagManager;}if(!$private.validator.isObject(UOLPD.TagManager[packageName])){UOLPD.TagManager[packageName]={};}UOLPD.TagManager[packageName].version=$private.version;if(!$private.validator.isArray(UOLPD.TagManager[packageName].config)){UOLPD.TagManager[packageName].config=[];}if(!$private.validator.isObject(UOLPD.TagManager[packageName].log)){UOLPD.TagManager[packageName].log=new Logs();UOLPD.TagManager[packageName].log.setPrefix("UOLPD.TagManager."+packageName);}return UOLPD.TagManager[packageName];};return $public.init(packageName);}function ScriptUtils(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$private.dataCalls="data-uoltm-calls";$public.hasTagScript=function(src){if(!$private.typeValidator.isString(src)){return false;}if(src.length===0){return false;}if($public.findScript($private.removeProtocol(src))){return true;}return false;};$private.removeProtocol=function(value){return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/,"");};$public.findScript=function(src){var scripts=document.getElementsByTagName("script");for(var i=0,length=scripts.length;i<length;i++){var scriptSrc=scripts[i].getAttribute("src");if(scriptSrc&&$private.isSrcEqual(scriptSrc,src)){return scripts[i];}}return;};$private.isSrcEqual=function(src1,src2){return(src1.indexOf(src2)>-1);};$public.createScript=function(src){if(!$private.typeValidator.isString(src)){return;}var tag=document.createElement("script");tag.setAttribute("src",src);tag.async=true;return tag;};$public.appendTag=function(script){if(!$private.typeValidator.isDefined(script)){return;}if(script.constructor===HTMLScriptElement){$private.lastScriptsParent().appendChild(script);return true;}};$private.lastScriptsParent=function(){return document.getElementsByTagName("script")[0].parentNode;};$public.createSyncScript=function(src){if(!$private.typeValidator.isString(src)){return;}document.write("<scr"+'ipt src="'+src+'"></scr'+"ipt>");};}function StringUtils(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.trim=function(value){if(!$private.typeValidator.isString(value)){return;}if(value.length===0){return;}value=value.replace(/^(\s+)|(\s+)$/gm,"").replace(/\s+/gm," ");return value;};$public.getValueFromKeyInString=function(str,name,separator){if(!$private.typeValidator.isString(name)||name.length===0){return;}if(!$private.typeValidator.isString(str)||str.length===0){return;}if(!$private.typeValidator.isString(separator)||separator.length===0){return;}if(str.substring(str.length-1)){str+=separator;}name+="=";var startIndex=str.indexOf(name);if(startIndex===-1){return"";}var middleIndex=str.indexOf("=",startIndex)+1;var endIndex=str.indexOf(separator,middleIndex);if(endIndex===-1){endIndex=str.length;}return unescape(str.substring(middleIndex,endIndex));};return $public;}function RemoteStorage(){var $private={};var $public=this;$public.logs=new Logs();$public.logs.setPrefix("UOLPD.TagManager");$private.validator=new TypeValidator();$private.path="https://tm.uol.com.br/mercurio.html";$private.iframe=undefined;$private.iframeLoaded=false;$private.queue=[];$private.requests={};$private.id=0;$public.init=function(){$private.hasLoaded(function(){if(!$private.isIframeInPage()){$private.setIframe();}$private.setListener($private.iframe);return true;});return $public;};$private.isIframeInPage=function(){var iframes=document.getElementsByTagName("iframe");for(var i=0;i<iframes.length;i++){if(iframes[i].getAttribute("src")==$private.path){$private.iframe=iframes[i];return true;}}return false;};$public.get=function(key,callback){$private.communicator("get",key,undefined,callback);};$public.set=function(key,value,callback){$private.communicator("set",key,value,callback);};$public.removeItem=function(key,callback){callback=callback||function(){};$private.communicator("delete",key,undefined,callback);};$private.hasLoaded=function(fn,timeout){timeout=(timeout?timeout+10:10);if(typeof fn!="function"){return false;}if(document.body!==null){fn();return true;}setTimeout(function(){$private.hasLoaded(fn,timeout);},timeout);};$private.setIframe=function(){$private.iframe=$private.buildIframe();};$private.buildIframe=function(){var iframe=document.createElement("iframe");iframe.setAttribute("src",$private.path);iframe.setAttribute("id","tm-remote-storage");iframe.setAttribute("style","position:absolute;width:1px;height:1px;left:-9999px;display:none;");document.body.appendChild(iframe);return iframe;};$private.setListener=function(iframe){var hasEventListener=(window.addEventListener!==undefined);var method=(hasEventListener)?"addEventListener":"attachEvent";var message=(hasEventListener)?"message":"onmessage";var load=(hasEventListener)?"load":"onload";iframe[method](load,$private.iframeLoadHandler);window[method](message,$private.messageHandler);return iframe;};$private.iframeLoadHandler=function(event){$private.iframeLoaded=true;if(!$private.queue.length){return;}for(var i=0,length=$private.queue.length;i<length;i++){$private.sendMessageToIframe($private.queue[i]);}$private.queue=[];};$private.messageHandler=function(event){try{var data=JSON.parse(event.data);$private.requests[data.id].callback(data.key,data.value);delete $private.requests[data.id];}catch(err){}};$private.communicator=function(method,key,value,callback){if(!$private.validator.isString(key)){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}if(method==="set"&&value===undefined){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}if(!$private.validator.isFunction(callback)){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}var request={id:++$private.id,key:key,method:method};if(value){request.value=value;}var data={request:request,callback:callback};if($private.iframeLoaded){$private.sendMessageToIframe(data);}else{$private.queue.push(data);}if(!$private.iframe){$public.init();}};$private.sendMessageToIframe=function(data){$private.requests[data.request.id]=data;data=JSON.stringify(data.request);if(!data){return;}$private.iframe.contentWindow.postMessage(data,$private.path);};return $public.init();}function TrackManager(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.imagesSrc=[];$private.baseUrl="//logger.rm.uol.com.br/v1/?prd=98&grp=Origem:";$private.raffledRate=Math.round(Math.random()*100);$private.samplingRate=1;$public.trackSuccess=function(msr){$public.sendPixel($private.getPixelSrc($private.getMeasure(msr)),$private.samplingRate);};$private.getMeasure=function(msr){if(!$private.typeValidator.isString(msr)){return;}return"&msr="+encodeURIComponent(msr)+":1";};$private.getPixelSrc=function(src){if(!$private.typeValidator.isString(src)){return;}if(!$public.getModuleName()){return;}if(!$public.getRepoId()){return;}return $private.baseUrl+$public.getModuleName()+$public.getRepoId()+src;};$public.getModuleName=function(moduleName){return $private.moduleName;};$public.setModuleName=function(moduleName){if($private.typeValidator.isString(moduleName)){$private.moduleName="TM-"+moduleName+";";}};$public.getRepoId=function(){if(!$private.repoId){$public.setRepoId();}return $private.repoId;};$public.setRepoId=function(){var src=$private.getScriptSrc();if(!src){return;}var srcMatch=src.match(/uoltm.js\?id=(.{6}).*/);var repoId=srcMatch?srcMatch[1]:null;if(repoId){$private.repoId="tm_repo_id:"+repoId;}};$private.getScriptSrc=function(){var script=document.querySelector('script[src*="tm.jsuol.com.br/uoltm.js"]');var src=script?script.src:null;return src;};$public.sendPixel=function(src,samplingRate){if(!$private.typeValidator.isString(src)){return;}if(!$private.isTrackEnabled(samplingRate)){return;}var img=document.createElement("img");img.setAttribute("src",src);$public.imagesSrc.push(img.src);};$private.isTrackEnabled=function(samplingRate){if(window.location.protocol.match("https")){return false;}try{if(window.localStorage.getItem("trackManager")=="true"){return true;}}catch(e){}if($public.getRaffledRate()<=samplingRate){return true;}return false;};$public.getRaffledRate=function(){return $private.raffledRate;};$public.trackError=function(errorType,errorEffect){errorType=$private.getErrorType(errorType);if(!errorType){return;}errorEffect=$private.getErrorEffect(errorEffect);if(!errorEffect){return;}var url=$private.getPixelSrc(errorType+errorEffect+$private.getMeasure("Erros"));$public.sendPixel(url,100);};$private.getErrorType=function(errorType){return $private.generateKeyValue("erro_tipo",errorType);};$private.generateKeyValue=function(key,value){if(!$private.typeValidator.isString(key)){return;}if(!$private.typeValidator.isString(value)){return;}return";"+key+":"+value;};$private.getErrorEffect=function(errorEffect){return $private.generateKeyValue("erro_efeito",errorEffect);};$public.trackCustom=function(measure,trackType,trackValue){var url;var src=$private.generateKeyValue(trackType,trackValue);if(!measure||!src){return;}measure=$private.getMeasure(measure);if(measure){url=$private.getPixelSrc(src+measure);$public.sendPixel(url,$private.samplingRate);}};}
function DomUtils (){
    var $private = {};
    var $public = this;
    $public.isReady = function(callback, timeout){
        if(!timeout && document.body) {
            callback();
        } else if(!timeout && document.readyState) {
            document.onreadystatechange = function(e) {
                if (document.readyState == "complete") {
                    callback();
                }
            };
        } else if(timeout) {
            setTimeout(function() {
                callback();
            }, timeout);
        }
    };
}
function ClientManager () {
    var $private = {};
    var $public = this;
    $private.validator = new TypeValidator();
    $private.domain = '.uol.com.br';
    $private.expDomain = '';
    $private.defaultExpiration = 60;
    $private.deleteClient = false;
    $private.defaultExpiratonDate = 60;
    $private.domainNameWhitelist = [
        'dynad_rt',
        'dynad_rt_exp',
        'DEretargeting',
        'DEretargetingExp',
        'toggle',
        'uoltm-toggle'
    ];
    $public.init = function () {
        $private.createNamespace();
        $public.setCookieName($private.getQueryString('name'));
        $public.setExpirationCookieName($private.getQueryString('expname'));
        $private.setExpirationDomain($private.getQueryString('expdomain'));
        $public.setCustomExpiration($private.getQueryString('expires'));
        $public.setDeleteClient($private.getQueryString('delete'));
        $public.setRedirectTo($private.getQueryString('redir'));
        $public.setClient($private.getQueryString('client'));
        if ($private.redirectTo) {
            window.location.href = $private.redirectTo;
        }
        return $public;
    };
    $private.createNamespace = function () {
        if (!$private.validator.isObject(window.UOLPD)) {
            window.UOLPD = {};
        }
        window.UOLPD = window.UOLPD || {};
        if (!$private.validator.isObject(window.UOLPD.DynadTrack)) {
            window.UOLPD.DynadTrack = {};
        }
        window.UOLPD.DynadTrack = window.UOLPD.DynadTrack || {};
        if (!$private.validator.isObject(window.UOLPD.DynadTrack.ClientManager)) {
            window.UOLPD.DynadTrack.ClientManager = $public;
        }
        window.UOLPD.DynadTrack.ClientManager = window.UOLPD.DynadTrack.ClientManager || {};
    };
    $public.setCookieName = function (name) {
        if($private.isCookieNameValid(name)) {
            $private.cookieName = name;
        }
    };
    $public.setRedirectTo = function (url) {
        $private.redirectTo = url;
    };
    $private.isCookieNameValid = function (name) {
        if(!$private.validator.isString(name)) {
            return false;
        }
        for (var i = 0; i < $private.domainNameWhitelist.length; i++) {
            if ($private.domainNameWhitelist[i] === name) {
                return true;
            }
        }
        return false;
    };
    $public.setExpirationCookieName = function (name) {
        if($private.isCookieNameValid(name)) {
            $private.expCookieName = name;
        }
    };
    $private.setExpirationDomain = function (expDomain) {
        if(expDomain === '1') {
            $private.expDomain = '.tm.uol.com.br';
        }
    };
    $public.setCustomExpiration = function (expires) {
        expires = parseInt(expires);
        if (!$private.validator.isNumber(expires) || expires <= 0 || expires > 60) {
            $private.customExpiration = $private.defaultExpiration;
            return false;
        }
        $private.customExpiration = expires;
        return true;
    };
    $public.setDeleteClient = function(deleteValue) {
        if (!$private.validator.isString(deleteValue)) {
            return false;
        }
        $private.deleteClient = (deleteValue == 'true');
        return $private.deleteClient;
    };
    $public.setClient = function (clientName) {
        $private.invalidClient = !$public.validateClient(clientName);
        /*if (!$private.expCookieName) {
            if ($private.invalidClient) {
                $private.customExpiration = -1;
            }
            console.log("AQUI");
            return $private.setCookie($private.cookieName, clientName, $public.getClientExpiration(), $private.domain);
        }*/

        var updatedExpCookies = $public.updateExpCookies(clientName);
        console.log("clientName",updatedExpCookies);
        $public.setExpCookie(updatedExpCookies);
        //return $private.setDefaultCookie(updatedExpCookies);
    };
    $public.validateClient = function (clientName) {
        if (!$private.validator.isString(clientName)) {
            return false;
        }
        if ($private.deleteClient) {
            return false;
        }
        return /^[a-zA-Z0-9-_\.]+$/i.test(clientName);
    };
    $public.updateExpCookies = function (clientName) {
        var clientExpiration = $public.getClientExpiration();
        var result = [];
        var cookies = $private.getCookieValues($private.expCookieName);
        if (cookies.length === 0) {
            result = $private.addDefaultValuesToExpiration(result, clientName);
        }
        if (!$private.findClientInCookie(clientName, cookies.replace(/\|\d{8}/g, '').split(';')) && !$private.isClientInResult(clientName, result) && !$private.invalidClient) {
            result.push(clientName + '|' + $private.getNewDate(clientExpiration));
        }
        cookies = cookies.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var client = cookies[i].split('|');
            if (client.length === 0) {
                continue;
            }
            if ($private.isClientInResult(client[0], result)) {
                continue;
            }
            if (client[0] == clientName) {
                if ($private.invalidClient === false) {
                    result.push(clientName + '|' + $private.getNewDate(clientExpiration));
                }
                continue;
            }
            if ($private.isUpdated(client[1])) {
                result.push(client.join('|'));
            }
        }
        result = $private.addDefaultValuesToExpiration(result, clientName).join(';');
        if (cookies.length === 0 && result.length === 0) {
            return clientName + '|' + $private.getNewDate(clientExpiration);
        }
        return result;
    };
    $public.getClientExpiration = function () {
        return $private.customExpiration || $private.defaultExpiration;
    };
    $private.getCookieValues = function (cookieName) {
        return $private.getValueFromKeyInString(document.cookie, cookieName, ';');
    };
    $private.getValueFromKeyInString = function (str, name, separator) {
        if (!$private.validator.isString(name)) {
            return;
        }
        if (str.substring(str.length - 1)) {
            str += separator;
        }
        name += '=';
        var startIndex = str.indexOf(name);
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = str.indexOf('=', startIndex) + 1;
        var endIndex = str.indexOf(separator, middleIndex);
        if (endIndex === -1){
            endIndex = str.length;
        }
        return unescape(str.substring(middleIndex, endIndex));
    };
    $private.addDefaultValuesToExpiration = function (result, clientName) {
        if (result.length > 0) {
            return result;
        }
        var defaultValues = $private.getCookieValues($private.cookieName).split(';');
        if(!$private.findClientInCookie(clientName, defaultValues) && !$private.invalidClient){
            defaultValues.push(clientName);
        }
        for (var i = 0; i < defaultValues.length; i++) {
            if (defaultValues[i].length === 0) {
                continue;
            }
            if ($private.isClientInResult(defaultValues[i], result)) {
                continue;
            }
            if(!$private.hasInExpirationCookie(defaultValues[i])) {
                result.push(defaultValues[i] + '|' + $private.getNewDate($public.getClientExpiration()));
            }
        }
        return result;
    };
    $private.findClientInCookie = function (clientName, cookies) {
        if (!clientName || clientName.length === 0)  {
            return false;
        }
        var length = cookies.length;
        for (var i = 0; i < length; i++) {
            if (cookies[i] == clientName) {
                return true;
            }
        }
        return false;
    };
    $private.isClientInResult = function (client, result) {
        if (!client || !result || client.length === 0 || result.length === 0) {
            return false;
        }
        for (var i = 0; i < result.length; i++) {
            if (client == result[i].substring(0, result[i].indexOf('|'))) {
                return true;
            }
        }
        return false;
    };
    $private.hasInExpirationCookie = function (item) {
        var expirationCookie = $private.getCookieValues($private.expCookieName);
        for (var i = 0; i < expirationCookie.length; i++) {
            if (expirationCookie.split('|')[0] == item) {
                return true;
            }
        }
        return false;
    };
    $private.getNewDate = function (expireDays) {
        var stringDate = '';
        var today = new Date();
        expireDays = expireDays || 0;
        today.setDate(today.getDate() + expireDays);
        if (today.getDate() <= 9) {
            stringDate += '0';
        }
        stringDate += today.getDate();
        if (today.getMonth() <= 8){
            stringDate += '0';
        }
        stringDate += today.getMonth() + 1;
        stringDate += today.getFullYear();
        return stringDate;
    };
    $public.setExpCookie = function (updatedExpCookies) {
        console.log("setExpCookie", $private.expCookieName, updatedExpCookies, $public.getClientExpiration(), $private.expDomain);
        return $private.setCookie($private.expCookieName, updatedExpCookies, $public.getClientExpiration(), $private.expDomain);
    };
    $private.setCookie = function (name, value, expiredays, domain) {
        if (!name) {
            return;
        }
        if(value.substring(0,1) == ';') {
            value = value.substr(1);
        }
        console.log(expiredays, value);
        value = escape(value);
        $private.setExpirationDate(expiredays);
        if(value.length === 0) {
            $private.setExpirationDate(-1);
        }

        var now = new Date();
        now.setTime(now.getTime() + 45 * 3600 * 1000);
        now.toUTCString();

        var cookie = name + "=" + 'teste' + ";expires=" + now + ";path=/;";
        if (domain) {
            cookie += "domain="+ domain +";";
        }
        console.log("COOKIE", cookie);
        document.cookie = cookie;
        return cookie;
    };
    $private.setExpirationDate = function (expiredays) {
        $private.expirationDate = new Date();
        $private.expirationDate.setDate($private.expirationDate.getDate() + expiredays);
        $private.expirationDate.setHours(23);
        $private.expirationDate.setMinutes(59);
        $private.expirationDate.setSeconds(59);
        console.log("EXPIRATIONDATE", $private.expirationDate);
        $private.expirationDate = $private.expirationDate.toUTCString();
    };
    $public.getExpirationDate = function () {
        return $private.expirationDate;
    };
    $public.hasClientInCookies = function (clientName) {
        if(clientName.length === 0) {
            return false;
        }
        var cookies = $private.findClientInCookie(clientName, $private.getCookieValues($private.cookieName).split(';'));
        var expCookies = $private.findClientInCookie(clientName, $private.getCookieValues($private.expCookieName).replace(/\|\d{8}/g, '').split(';'));
        return cookies && expCookies;
    };
    $private.getQueryString = function (param) {
        var queryString = $private.getValueFromKeyInString(document.location.search.substring(1), param, '&');
        if (queryString.length === 0) {
            return;
        }
        return queryString;
    };
    $private.formatDate = function (currentDate) {
        if(!currentDate) {
            return;
        }
        var match = currentDate.match(/(\d{2})(\d{2})(\d{4})/);
        currentDate = new Date();
        if (!match) {
            return;
        }
        currentDate.setDate(match[1]);
        currentDate.setMonth(match[2] - 1);
        currentDate.setYear(match[3]);
        currentDate.setHours(23);
        currentDate.setMinutes(59);
        currentDate.setSeconds(59);
        return currentDate;
    };
    $private.isUpdated = function (currentDate) {
        return $private.formatDate(currentDate) > $private.formatDate($private.getNewDate());
    };
    $private.setDefaultCookie = function (updatedExpCookies) {
        //console.log($private.getCookieValues($private.expCookieName), $private.expCookieName, updatedExpCookies);
        var value = $private.getCookieValues($private.expCookieName).replace(/\|\d{8}/g, '');
        console.log("setDefaultCookie", $private.cookieName, "value>>>", value, ">", $public.getClientExpiration(), $private.domain);
        return $private.setCookie($private.cookieName, value, $public.getClientExpiration(), $private.domain);
    };
}
var clientManager = new ClientManager();
clientManager.init();
})(window, document);
