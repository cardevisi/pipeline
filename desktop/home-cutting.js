(function (window, document, undefined){
'use strict';
/*
* © UOL RICHMEDIA - Todos os direitos reservados
* VERSAO: 1.0.1
*/
function Config () {
    return {
        title : 'gliderArroba',
        debug: true,
        screenshot : 'http://insercao.uol.com.br/teste/richmedia/2015/default/screenshot.jpg',
        format: {
            initial: {
                width : 1190, // largura do formato inicial
                height: 70, // altura do formato inicial
                mask : true, // em true habilita a mascara - false desabilita
                background : '#ffffff', // cor de fundo Ex. #000000
                src : 'http://placehold.it/1190x70', //path do html ou variavel do conteudo html
                reload: true, //recarrega a primeira peca
                openByHover: false, //flag para habilitar a expansão por mouseover
                delayToOpen: 1500 //tempo para abertura com mouseover
            },
            expanded : {
                width : 955, //largura do formato expandido
                height : 500, //altura do formato expandido
                mask : true, // em true habilita a mascara - false desabilita
                background : '#ffffff',
                src : 'http://insercao.uol.com.br/teste/richmedia/2015/default/expandido/', //path do html ou variavel do conteudo html
                reload: false, //recarrega a segunda peca
                closeBtn: {
                    marginRight : 1,
                    marginTop : 1
                },
            },
            write : false
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : 'banner-1190x70-2-Area' // banner ID do container
    };
}

function Appender() {

    var $public = this;
    var $private = {};

    $public.setContainer = function (bannerId) {
        $private.container = document.getElementById(bannerId) || top.document.getElementById(bannerId);
        //$private.hideChildren($private.container);
    };

    $public.getContainer = function () {
        return $private.container;
    };

    $public.insertOnContainer = function (element) {
        if (!$private.container) {
            return;
        }
        $private.container.appendChild(element);
    };

    $public.getDocument = function () {
        return $private.container.ownerDocument;
    };

    $public.selector = function (query) {
        return $private.container.querySelector(query);
    };

    $public.byId = function (id) {
        return $public.getDocument().getElementById(id);
    };

    $public.applyClass = function (id, className, override) {
        var element = $public.byId(id);
        var classValue = element.getAttribute('class');
        if (classValue && !override) {
            classValue = classValue.trim();
            classValue = classValue.split(' ');
            element.setAttribute('class', classValue[0] + ' ' + className);
            return element;
        }

        element.setAttribute('class', className);
        return element;
    };

    $public.applyAttributes = function (element, properties) {
        for (var property in properties) {
            if (!properties.hasOwnProperty(property)) {
                continue;
            }
            element.setAttribute(property, properties [property]);
        }
        return element;
    };

    $public.applyStyles = function (element, styles) {
        for (var style in styles) {
            if (!styles.hasOwnProperty(style)) {
                continue;
            }
            element.style[style] = styles[style];
        }
        return element;
    };

    $public.isBrowser = function (listBrowsers) {
        return new RegExp(listBrowsers.join('|'), 'i').test(navigator.userAgent);
    };

    $private.hideChildren = function (element) {
        if (!element) {
            return;
        }
        var childNodes = element.children;
        for (var i = 0, length = childNodes.length; i < length; i++) {
            $public.applyStyles(childNodes[i], {'display': 'none'});
        }
    };

    $private.getTopIframe = function () {
        var topIframes = top.document.getElementsByTagName("IFRAME");
        for (var i = 0, length = topIframes.length; i < length; i++) {
            if (topIframes[i].contentWindow == self) {
                return topIframes[i];
            }
        }
    };
}

function Glider(config) {

    var $public = this;
    var $private = {};

    $private.appender = new Appender();
    // settings
    $private.IMAGE_CLOSE_BUTTON = 'http://bn.imguol.com/1110/uol/cutting/fechar.gif';
    $private.IMAGE_PRELOADER = 'http://ci.i.uol.com.br/album/wait_inv.gif';
    $private.BASE_TRACK_PATH = 'http://www5.smartadserver.com/imp?imgid=';
    // containers
    $private.FIRST_IFRAME = 'uol-iframe-first-content-' + config.title;
    $private.FIRST_CONTAINER = 'first-container-' + config.title;
    $private.SECOND_CONTAINER = 'second-container-' + config.title;
    $private.GLIDER_WRAPPER = 'glider-wrapper';
    $private.GLIDER_CONTENT = 'glider-content-' + config.title;
    $private.PRELOAD_CONTAINER = 'preload-container-' + config.title;
    $private.GLIDER_IFRAME = 'glider-iframe-second-' + config.title;
    $private.WRAPPER_ID = 'uol-richmedia-' + config.title;

    $private.secondContainerIsOpened = false;

    $public.getConfigTitle = function () {
        return config.title.toUpperCase();
    };

    $public.getClickTag = function (value) {
        return config.clickTag[value || 0];
    };

    $public.clickTag = function () {
        $public.close();
        try {
            window.open($public.getClickTag(), '_blank');
        } catch (e) {
            console.log('[CLICKTAG COM VALOR INVÁLIDO]');
        }
        return false;
    };

    $public.open = function () {
        if ($private.appender.isBrowser(['msie 9.0', 'msie 10.0'])) {
            $public.clickTag();
            return;
        }
        $private.showGlider();
        $private.trackByParams('open', config.track.open);
    };

    $public.close = function () {
        $private.closeAnimation();
        $private.trackByParams('close', config.track.close);
    };

    $public.loadComplete = function (element) {
        $private.removePreloader(element.getAttribute('id'));
    };

    $private.getScrollHeight = function () {
        var doc = $private.appender.getDocument();
        return Math.max(
            Math.max(doc.body.scrollHeight, doc.documentElement.scrollHeight),
            Math.max(doc.body.offsetHeight, doc.documentElement.offsetHeight),
            Math.max(doc.body.clientHeight, doc.documentElement.clientHeight)
        );
    };

    $private.getBrowserWidth = function () {
        var doc = $private.appender.getDocument();
        return window.innerWidth || doc.documentElement.clientWidth || doc.body.clientWidth;
    };

    $private.reloadIframe = function (id) {
        $private.appender.applyAttributes($private.appender.byId(id), {'src': ''});
    };

    $private.toggleVisibility = function (id) {
        var div = $private.appender.byId(id);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    };

    $private.toggleContainerZindex = function (id) {
        var div = $private.appender.byId(id);
        $private.zIndexOrigin = $private.zIndexOrigin || div.style.zIndex;

        if ($private.zIndexOrigin === '' || $private.zIndexOrigin === '999') {
            $private.zIndexOrigin = 2147483647;
        }

        $private.appender.applyStyles(div, {'position': 'relative', 'zIndex': $private.zIndexOrigin});
    };

    $private.trackByParams = function (params, id) {
        if (id === "" || id === undefined) {
            return;
        }
        var url = $private.BASE_TRACK_PATH + id + "&tmstp=" + (new Date()).getTime() + "&tgt=&" + params;
        if (config.debug.toString() === 'true') {
            console.log('[UOL RICHMEDIA SMART TRACKING DEBUG MODE]', url);
            return;
        }
        $private.appender.applyAttributes(new Image(), {'src': url});
    };

    $private.createIframe = function (id) {
        var iframe = $private.appender.applyAttributes(document.createElement('iframe'), {
            'id': id,
            'marginwidth': 0,
            'marginheight': 0,
            'frameborder': 0,
            'scrolling': 'no'
        });
        return iframe;
    };

    $private.writeIframeContent = function (id, src) {
        var iframe = $private.appender.byId(id);
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    };

    $private.createPreloader = function () {
        var image = $private.appender.applyAttributes(new Image(), {
            'id': $private.PRELOAD_CONTAINER,
            'src': $private.IMAGE_PRELOADER
        });

        $private.appender.applyStyles(image, {
            'position': 'absolute',
            'top': '50%',
            'left': '50%',
            'zIndex': 1,
            'margin': '-11px 0 0 -11px'
        });
        return image;
    };

    $private.getContainerById = function (id) {
        var idFound;
        if (id.indexOf('first') > -1) {
            idFound = $private.FIRST_CONTAINER;
        } else if (id.indexOf('second') > -1) {
            idFound = $private.GLIDER_CONTENT;
        }
        return $private.appender.byId(idFound);
    };

    $private.addPreloader = function (id) {
        var div = $private.getContainerById(id);
        try {
            var preloadImage = $private.createPreloader();
            div.appendChild(preloadImage);
        } catch (e) {
        }
    };

    $private.removePreloader = function (id) {
        var div = $private.getContainerById(id);
        var preloadImage = $private.appender.byId($private.PRELOAD_CONTAINER);
        try {
            div.removeChild(preloadImage);
        } catch (e) {
        }
    };

    $private.sourceIframeContent = function (id, url) {
        $private.appender.applyAttributes($private.appender.selector('#' + id), {
            'src': url,
            'onload': 'window.UOLI.' + $public.getConfigTitle() + '.loadComplete(this);'
        });
    };

    $private.loadIframeContent = function (id, src) {
        $private.addPreloader(id);
        if (config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] src em modo write incorreto.');
                return;
            }
            $private.writeIframeContent(id, src);
        } else {
            $private.sourceIframeContent(id, src);
        }
    };

    $public.hideCuttingEdge = function() {
        $private.setCookie();
        var div = $private.appender.byId($private.FIRST_CONTAINER);
        div.style.height = '0px';
    };

    $private.setCookie = function () {
        var date = new Date();
        date.setTime(date.getTime()+(($private.config.cookieHours)*60*60*1000));
        var expires = 'expires='+date.toUTCString() + '; path=/';
        $private.appender.getDocument().cookie='CEHome=0;domain=.uol.com.br;' + (($private.config.cookieExpire) ? expires : '');
    };

    $private.getCookie = function () {
        if (document.cookie.indexOf("CEHome") != -1) {
           return true;
        }
        return false;
    };

    $private.createCustomCssOnHeader = function () {
        var head = $private.appender.getDocument().head || $private.appender.getDocument().getElementsByTagName('head')[0];
        console.log(config.bannerId);
        var css = '#'+ config.bannerId +' {height:auto !important; cursor:pointer; position:relative; background-color:#ffffff; width: 1190px; margin-top:5px; overflow: hidden; transition: .5s ease-in;} #'+config.bannerId+' span {position:absolute; top:0px; right:5px;} #closeCuttingBtn {top:0; right:0; width:55px; height:15px; position:absolute; cursor:pointer; z-index: 1;}';

        var styleElement = document.createElement("style");
        styleElement.type = "text/css";

        if (styleElement.styleSheet) {
            styleElement.styleSheet.cssText = css;
        } else {
            styleElement.appendChild(document.createTextNode(css));
        }

        head.insertBefore(styleElement, head.firstChild);
    };

    $private.createCloseButton = function () {
        var img = $private.appender.applyAttributes(document.createElement('img'), {'src': $private.IMAGE_CLOSE_BUTTON});
        var btn = $private.appender.applyAttributes(document.createElement('a'), {'value': 'Fechar'});

        $private.appender.applyStyles(btn, {
            'position': 'absolute',
            'width': '55px',
            'height': '15px',
            'top': '5px',
            'right': $private.getPosX() + 'px',
            'zIndex': 3, 'cursor': 'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            if ($private.secondContainerIsOpened === false) {
                return;
            }
            $public.close();
        });
        btn.appendChild(img);
        return btn;
    };

    $private.getPosX = function () {
        var posX = 0;
        var configWidth = config.format.expanded.width * 0.5;
        var browserWidth = $private.getBrowserWidth() * 0.5;

        if (configWidth > browserWidth) {
            posX = configWidth - browserWidth;
        } else {
            posX = browserWidth - configWidth;
        }
        return posX;
    };

    $private.createCloseButtom = function () {
        var img = $private.appender.applyAttributes(document.createElement('img'),
            {'src' : 'http://bn.imguol.com/1110/uol/cutting/fechar.gif'});

        var btn = $private.appender.applyAttributes(document.createElement('a'),
            {'value' : 'Fechar'
        });

        $private.appender.applyStyles(btn, {
            'position':'absolute',
            'width':'55px',
            'height':'15px',
            'top': (config.format.expanded.closeBtn.marginTop || 0)+'px',
            'right': (config.format.expanded.closeBtn.marginRight || 0)+'px',
            'zIndex':3,
            'cursor':'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            if($private.constants.STATE === 'retract') {
                $public.hideCuttingEdge();
            } else {
                $public.close();
            }
        });

        btn.appendChild(img);
        return btn;
    };

    $private.createFirstContainer = function (width, height) {
        var div = $private.createContainer($private.FIRST_CONTAINER);
        div.style.background = config.format.initial.background;

        $private.appender.applyStyles(div, {
            'position':'relative',
            'width':width+'px',
            'height':height+'px',
            'transition': '.5s ease-in',
            'overflow': 'hidden'
        });

        //div.appendChild($private.createClickArea(width, height));
        if (config.format.initial.mask.toString() === 'true') {
            div.appendChild($private.createMask(width, height, false, $public.open));
        }

        var iframe = $private.createIframe($private.FIRST_IFRAME);
        div.appendChild(iframe);

        $private.appender.applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width,
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top,
            'right' : iframe.parentNode.style.right,
            'transition': '.5s ease-in'
        });

        $private.btn = $private.createCloseButtom();
        div.appendChild($private.btn);

        return div;
    };

    $private.createSecondContent = function () {
        var div = $private.appender.applyAttributes($private.createContainer($private.GLIDER_WRAPPER), {'class': $private.GLIDER_WRAPPER});
        var img = $private.appender.applyAttributes(document.createElement('img'), {'src': config.screenshot});

        $private.appender.applyStyles(div, {
            'position': 'absolute',
            'width': '100%',
            'height': '1600px',
            'top': '0px',
            'display': 'none',
            'background': config.format.expanded.background
        });

        $private.appender.applyStyles(img, {
            'position': 'relative',
            'marginRight': '-50%',
            'marginLeft': '-50%'
        });

        div.appendChild(img);
        div.appendChild($private.createMask('100%', '100%', true, $public.close));

        var wrapper = $private.createSecondWrapper($private.SECOND_CONTAINER);
        var glider = $private.createGliderContent(config.format.expanded.width, config.format.expanded.height);
        var btn = $private.createCloseButton();

        wrapper.appendChild(div);
        wrapper.appendChild(glider);
        wrapper.appendChild(btn);
        return wrapper;
    };

    $private.createSecondWrapper = function (id) {
        var wrapper = $private.createContainer(id);
        $private.appender.applyStyles(wrapper, {
            'position': 'fixed',
            'width': '100%',
            'height': $private.getScrollHeight() + 'px',
            'top': '0px',
            'left': '0px',
            'display': 'none',
            'background': config.format.expanded.background,
            'zIndex': 1
        });
        return wrapper;
    };

    $private.createGliderContent = function (width, height) {
        var div = $private.appender.applyStyles($private.createContainer($private.GLIDER_CONTENT), {
            'position': 'relative',
            'width': width + 'px',
            'height': height + 'px',
            'top': '0px',
            'left': '0px',
            'display': 'none',
            'margin': '0 auto',
            'background': config.format.expanded.background
        });

        if (config.format.expanded.mask.toString() === 'true') {
            div.appendChild($private.createMask(width, height, true, $public.clickTag));
        }

        var iframe = $private.appender.applyStyles($private.createIframe($private.GLIDER_IFRAME), {
            'position': 'absolute',
            'width': div.style.width,
            'height': div.style.height,
            'top': div.style.top,
            'left': div.style.left
        });

        div.appendChild(iframe);
        return div;
    };

    $private.createContainer = function (name) {
        return $private.appender.applyAttributes(document.createElement('div'), {'id': name});
    };

    $private.createMask = function (width, height, flag, callback) {
        var mask = $private.appender.applyStyles(document.createElement('a'), {
            'position': 'absolute',
            'top': '0',
            'left': '0',
            'zIndex': 1,
            'cursor': 'pointer',
            'display': 'block'
        });

        if (flag) {
            mask.style.width = '100%';
            mask.style.height = '100%';
        } else {
            mask.style.width = width + 'px';
            mask.style.height = height + 'px';
        }
        $private.debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.target.hover = false;
            callback();
        });

        if (config.format.initial.openByHover && flag===false) {
            mask.addEventListener('mouseenter', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = true;
                window.setTimeout(function(){
                    if (e.target.hover) callback();
                }, config.format.initial.delayToOpen || 1500);
            });

            mask.addEventListener('mouseleave', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = false;
            });
        }

        return mask;
    };

    $private.debugMask = function (mask) {
        if (config.debug.toString() !== 'true') {
            return;
        }
        $private.appender.applyStyles(mask, {'background': '#ff0000', 'opacity': 0.5});
    };

    $private.showGlider = function () {
        top.window.scrollTo(0, 0);
        $private.toggleContainerZindex(config.bannerId);
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.toggleVisibility($private.GLIDER_WRAPPER);
        $private.applyAnimationEvents($private.appender.applyClass($private.GLIDER_WRAPPER, 'openGliderContent'), $private.animationEndToOpen);
    };

    $private.animationEndToOpen = function (e) {
        if (e.animationName === 'rotateToOpenReverse') {
            $private.toggleVisibility($private.GLIDER_CONTENT);
            $private.loadIframeContent($private.GLIDER_IFRAME, config.format.expanded.src);
            $private.appender.applyClass($private.GLIDER_CONTENT, 'showGliderContent', true);
            $private.removeAnimationEvents(this, $private.animationEndToOpen);
            $private.countAnimation = 0;
            $private.secondContainerIsOpened = true;
        }
    };

    $private.animationEndToCloseGliderContent = function (e) {
        if (e.animationName !== 'fadeOutGliderContent') {
            return;
        }
        $private.toggleVisibility($private.GLIDER_CONTENT);
        var wrapper = $private.appender.applyClass($private.GLIDER_WRAPPER, 'closeGliderContent');
        $private.applyAnimationEvents(wrapper, $private.animationEndToCloseSecondContainer);
        $private.removeAnimationEvents(this, $private.animationEndToCloseGliderContent);
    };

    $private.animationEndToCloseSecondContainer = function (e) {
        if (e.animationName !== 'fadeOutGliderContent') {
            return;
        }
        $private.reloadIframe($private.GLIDER_IFRAME);
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.toggleVisibility($private.GLIDER_WRAPPER);
        $private.toggleContainerZindex(config.bannerId);
        $private.removeAnimationEvents(this, $private.animationEndToCloseSecondContainer);
        $private.secondContainerIsOpened = false;
    };

    $private.closeAnimation = function () {
        var gliderContent = $private.appender.applyClass($private.GLIDER_CONTENT, 'hideGliderContent', true);
        $private.applyAnimationEvents(gliderContent, $private.animationEndToCloseGliderContent);
    };

    $public.render = function () {
        if ($private.getCookie()) {
            return;
        }

        $private.appender.setContainer(config.bannerId);

        if (!$private.appender.getContainer()) {
            return;
        }
        
        $private.createCustomCssOnHeader();
        $private.addCssAnimation();

        top.window.addEventListener("scroll", $private.scrollEventListener);

        var first = $private.createFirstContainer(config.format.initial.width, config.format.initial.height);
        var second = $private.createSecondContent();
        var wrapper = $private.appender.applyStyles($private.createContainer($private.WRAPPER_ID), {'position': 'relative'});
        wrapper.appendChild(first);
        wrapper.appendChild(second);

        $private.appender.insertOnContainer(wrapper);
        $private.loadIframeContent($private.FIRST_IFRAME, config.format.initial.src);

        var root = $private.appender.getContainer().getElementsByTagName('iframe')[0];
        console.log("ROOT", root, root.offsetTop);
        $private.appender.applyStyles(first, {
            'position' : 'absolute',
            'left': root.offsetLeft+'px',
            'top': root.offsetTop+'px'
        });
    };

    $private.scrollEventListener = function () {
        var scrollTop = $private.appender.getDocument().body.scrollTop || $private.appender.getDocument().documentElement.scrollTop;
        var secondContainer = $private.appender.byId($private.SECOND_CONTAINER);
        secondContainer.style.top = -scrollTop + 'px';
    };

    $private.addCssAnimation = function () {
        var style = $private.appender.applyAttributes(document.createElement('style'), {'type': 'text/css'});
        var css = '@keyframes moveToBottom{from{top: 0;left: 0}to{top: ' + config.format.expanded.height + 'px;left: 0}}@keyframes moveToTop{from{top: ' + config.format.expanded.height + 'px;left: 0}to{top: 0;left: 0}}@keyframes rotateToOpen{from{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}to{-webkit-transform: perspective(1200px) rotateX(20deg);-moz-transform: perspective(1200px) rotateX(20deg);-o-transform: perspective(1200px) rotateX(20deg);-ms-transform: perspective(1200px) rotateX(20deg);transform: perspective(1200px) rotateX(20deg)}}@keyframes rotateToOpenReverse{from{-webkit-transform: perspective(1200px) rotateX(20deg);-moz-transform: perspective(1200px) rotateX(20deg);-o-transform: perspective(1200px) rotateX(20deg);-ms-transform: perspective(1200px) rotateX(20deg);transform: perspective(1200px) rotateX(20deg)}to{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}}@keyframes rotateToClose{from{top: ' + config.format.expanded.height + 'px;-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}to{top: ' + config.format.expanded.height + 'px;-webkit-transform: perspective(1200px) rotateX(-20deg);-moz-transform: perspective(1200px) rotateX(-20deg);-o-transform: perspective(1200px) rotateX(-20deg);-ms-transform: perspective(1200px) rotateX(-20deg);transform: perspective(1200px) rotateX(-20deg)}}@keyframes stepAinit{from{-webkit-transform: perspective(1200px) rotateX(-20deg);-moz-transform: perspective(1200px) rotateX(-20deg);-o-transform: perspective(1200px) rotateX(-20deg);-ms-transform: perspective(1200px) rotateX(-20deg);transform: perspective(1200px) rotateX(-20deg)}to{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}}@keyframes fadeInGliderContent{from{opacity: 0}to{opacity: 1}}@keyframes fadeOutGliderContent{from{opacity: 1}to{opacity: 0}}.glider-wrapper{-webkit-perspective-origin: top,center;-moz-perspective-origin: top,center;-o-perspective-origin: top,center;-ms-perspective-origin: top,center;perspective-origin: top,center;-webkit-transform-origin: top center;-moz-transform-origin: top center;-o-transform-origin: top center;-ms-transform-origin: top center;transform-origin: top center;-webkit-transform-style: preserve-3d;-moz-transform-style: preserve-3d;-o-transform-style: preserve-3d;-ms-transform-style: preserve-3d;transform-style: preserve-3d;text-align: center}.openGliderContent{animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-webkit-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-moz-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-o-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s}.closeGliderContent{animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-webkit-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-moz-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-o-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s}.showGliderContent{animation: fadeInGliderContent 1s;-webkit-animation: fadeInGliderContent 1s;-moz-animation: fadeInGliderContent 1s;-o-animation: fadeInGliderContent 1s}.hideGliderContent{animation: fadeOutGliderContent .5s;-webkit-animation: fadeOutGliderContent .5s;-moz-animation: fadeOutGliderContent .5s;-o-animation: fadeOutGliderContent .5s}';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        var head = $private.appender.getDocument().head || $private.appender.getDocument().getElementsByTagName('head')[0];
        head.appendChild(style);
    };

    $private.removeAnimationEvents = function (element, eventCallback) {
        element.removeEventListener('animationend', eventCallback);
        element.removeEventListener('webkitAnimationEnd', eventCallback);
        element.removeEventListener('MSAnimationEnd', eventCallback);
        element.removeEventListener('oAnimationEnd', eventCallback);
    };

    $private.applyAnimationEvents = function (element, eventCallback) {
        element.addEventListener('animationend', eventCallback);
        element.addEventListener('webkitAnimationEnd', eventCallback);
        element.addEventListener('MSAnimationEnd', eventCallback);
        element.addEventListener('oAnimationEnd', eventCallback);
    };
}

var glider = new Glider(new Config());
glider.render();
top.window.UOLI = window.UOLI || {};
top.window.UOLI[glider.getConfigTitle()] = {};
top.window.UOLI[glider.getConfigTitle()].clickTag = glider.clickTag;
top.window.UOLI[glider.getConfigTitle()].getClickTag = glider.getClickTag;
top.window.UOLI[glider.getConfigTitle()].loadComplete = glider.loadComplete;
top.window.UOLI[glider.getConfigTitle()].open = glider.open;
top.window.UOLI[glider.getConfigTitle()].close = glider.close;
})(this, document);