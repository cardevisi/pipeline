<script>

(function (window, document, undefined){
'use strict';
/*
* @ UOL RICHMEDIA - Todos os diresitos reservados
* VERSAO: 1.0.0
*/
function Config () {
    return {
        title : 'viewport', // nome da campanha ou peca
        debug : false, // habilita o modo visual das clickAreas
        isCutting : true, // em true a peca configurada sera um cutting
        format:{
            initial : {
                width : 1190, // largura do formato inicial
                height : 180, // altura do formato inicial
                clickArea : true, // em true habilita a clickArea - em false desabilita
                src : 'http://bn.imguol.com/1604/uol/friboi/13/1190x180_150kb/index.html', // path do html ou variavel do conteudo html
                background : '#ffffff', // cor de fundo ex. #000000                                
                reload: false, // em true recarrega a primeira peca
                reloadContent: 'http://bn.imguol.com/1604/uol/friboi/13/1190x180_150kb/index.html', // path ou variavel do conteudo html para recarregar a peca utilizando o evento de click do mouse
                openByHover: false, //flag para habilitar a expansão por mouseover
                delayToOpen: 1500 //tempo para abertura com mouseover
            },
            expanded: {
                minWidth : 1280, // min-width da segunda peca
                minHeight : 600, // min-height da segunda peca
                clickArea : false, // em true habilita a clickArea - em false desbilita
                src : 'http://bn.imguol.com/1604/uol/friboi/13/1280x600_2000kb_clique_ie/index_2.html?c=https://adclick.g.doubleclick.net/aclk%253Fsa%253Dl%2526ai%253DC02mL3KsOV7vNKpWHxgSYiqKIBwEAexABILlgKASIAQGQAQDAAQLIAQngAgGoAwGqBFZP0AbKCmAMl6tYgN4WlPUBoQ7wn8S2mxWYkk7DNmox_m2UlKo07q5fL3xZa1sAaEPrLbRpboL-XN2_4Vs5Mf5mq9WD34s2Om0kpNjdPowPqqwa7WbiCLgEAaAGINgHAA%2526preview%253D%2526num%253D1%2526sig%253DAOD64_11AMIfHqQVqwXRxdodrn7L8z3upg%2526client%253Dca-pub-1027028252617386%2526adurl%253Dhttp://academiadacarnefriboi.globo.com/inicio%3Futm_source%3Duol%26utm_medium%3Ddisplay_desk%26utm_campaign%3Dlancamento%26utm_content%3Duol%26utm_term%3Dcuttingedge_15_04', // path do html ou variavel do conteudo html
                background : '#000000', // cor de fundo ex. #000000
                marginCloseButtom : { // atributos do botao de fechar da segunda peca
                    top : 10, // top do botao fechar
                    right: 10 // right do botao fechar
                },
                reload : true // em true recarrega a segunda peca
            },
            write : false // em false utiliza o modo de carregamento via src - em true utilizado o modo write
        },
        clickTag : [
            ''
        ],
        track : {
            open  : '',
            close : ''
        },
        bannerId : 'banner-1190x70-2-Area' // bannder ID do container
    };
}
function Appender () {
    var $public = this;
    var $private = {};

    $public.setContainder = function (bannerId) {
        $private.container = document.getElementById(bannerId) || top.document.getElementById(bannerId);
        $private.hideChildren($public.getContainer());
    };

    $private.hideChildren = function (element) {
        if (!element) {
            return;
        }
        var childNodes = element.children;
        for (var i = 0; i < childNodes.length; i++) {
            $public.applyStyles(childNodes[i], {'display' : 'none'});
        }
    };

    $public.getContainer = function () {
        return $private.container;
    };

    $public.insertOnContainer = function (element) {
        if (!$private.container) {
            return;
        }
        $private.container.appendChild(element);
    };

    $public.getOwnerDocument = function () {
        return $private.container.ownerDocument;
    };

    $public.byId = function (id) {
        return $public.getOwnerDocument().getElementById(id);
    };

    $public.selector = function (query) {
        return $public.getOwnerDocument().querySelector(query);
    };

    $public.applyStyles = function (element, styles) {
        for (var style in styles) {
            if (!styles.hasOwnProperty(style)) {
                continue;
            }
            element.style[style] = styles[style];
        }
        return element;
    };

    $public.applyAttributes = function (element, properties) {
        for (var property in properties) {
            if (!properties.hasOwnProperty(property)) {
                continue;
            }
            element.setAttribute(property, properties[property]);
        }
        return element;
    };
}
function Viewport (configViewport) {
    var $public = this;
    var $private = {};

    $private.config = configViewport;
    $private.appender = new Appender();
    $private.flagTimeOpen = $private.flagTimeOpen || 0;

    $private.FIRST_IFRAME = 'uol-iframe-first-content-' + $private.config.title;
    $private.SECOND_IFRAME = 'uol-iframe-second-content-' + $private.config.title;
    $private.FIRST_CONTAINER = 'first-container-' + $private.config.title;
    $private.SECOND_CONTAINER = 'second-container-' + $private.config.title;
    $private.PRELOAD_CONTAINER = 'preload-container-' + $private.config.title;
    $private.WRAPPER_ID = 'uol-richmedia-' + $private.config.title;
    $private.FIRST_CLICKAREA = 'initial-clickArea-' + $private.config.title;
    $private.SECOND_CLICKAREA = 'second-clickArea-' + $private.config.title;
    $private.isOpened = 'isOpened' + $private.config.title;

    $public.render = function() {
        if (document.cookie.indexOf("CEHome") != -1) {
            return;
        }

        $private.appender.setContainder($private.config.bannerId);
        if(!$private.appender.getContainer()) {
            return;
        }

        var wrapper = $private.createWrapper($private.WRAPPER_ID);
        var first = $private.createFirstContent($private.config.format.initial.src, $private.config.format.initial.width, $private.config.format.initial.height);
        wrapper.appendChild(first);
        var second = $private.createSecondContent($private.config.format.expanded.src, $private.config.format.expanded.width, $private.config.format.expanded.height);
        wrapper.appendChild(second);
        if ($private.config.isCutting.toString() === 'true') {
            var closeButtom = $private.createCuttingCloseButtom();
            $private.createCustomCssOnHeader();
            wrapper.appendChild(closeButtom);
        }
        $private.appender.insertOnContainer(wrapper);

        var contentToLoad = $private.config.format.initial.src;        
        $private.loadIframeContent($private.FIRST_IFRAME, contentToLoad);
    };

    $private.createWrapper = function (name) {
        var div = $private.createContainer(name);
        div.style.position = 'relative';
        return div;
    };

    $public.getConfigTitle = function () {
        return $private.config.title.toUpperCase();
    };

    $public.clickTag = function () {
        $public.close();
        window.open($public.getClickTag(), '_blank');
    };

    $public.getClickTag  = function (value) {
        var index = (value === undefined) ? 0 : value;
        return $private.config.clickTag[index];
    };

    $public.open = function() {
        $private.refreshHome('pause');
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.loadIframeContent($private.SECOND_IFRAME, $private.config.format.expanded.src);
        $private.toggleHomeOverflow();
        $private.track('open', $private.config.track.open);
    };

    $public.close = function() {
        $private.toggleHomeOverflow();
        var contentToLoad = $private.config.format.initial.src;        

        if ($private.config.format.initial.reload.toString() === 'true') {
            $private.reloadIframeContent($private.FIRST_IFRAME);
            setTimeout(function () {
                $private.loadIframeContent($private.FIRST_IFRAME, contentToLoad);
            }, 500);
        }

        if ($private.config.format.expanded.reload.toString() === 'true') {
            $private.reloadIframeContent($private.SECOND_IFRAME);
        }

        $private.appender.byId($private.SECOND_IFRAME).style.visibility = 'hidden';

        $private.refreshHome('start');
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.track('close', $private.config.track.close);
    };

    $public.loadComplete = function (element) {
        var id = element.getAttribute('id');
        if (id == $private.SECOND_IFRAME && element.getAttribute('src')) {
            element.style.visibility = 'visible';
        }
        $private.removePreloader(id);
    };

    $public.hideCuttingEdge = function() {
        $private.setCookie();
        $private.applyStyles($private.appender.byId($private.config.bannerId), { 'height': '0' });
    };

    $private.toggleHomeOverflow = function (element) {
        var body = $private.appender.selector('body');
        body.style.overflow = (body.style.overflow === '' || body.style.overflow === 'scroll' || body.style.overflow === 'auto') ? 'hidden' : 'auto';
    };    

    $private.setCookie = function () {
        $private.appender.getOwnerDocument().cookie='CEHome=0;domain=.uol.com.br;';
    };

    $private.refreshHome = function (value) {
        if (!window.homeUOL) {
            return;
        }
        if (!window.homeUOL.modules && !window.homeUOL.modules.refresh) {
            return;
        }
        if(value === 'pause') {
            window.homeUOL.modules.refresh.pause();
        } else if(value === 'start') {
            window.homeUOL.modules.refresh.start();
        }
    };

    $private.reloadIframeContent = function (id) {
        var iframe = $private.appender.byId(id);
        if($private.config.format.write.toString() === 'false') {
            $private.appender.applyAttributes(iframe, {'src' : ''});
        } else {
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write('');
            iframe.contentWindow.document.close();
        }
    };

    $private.createCuttingCloseButtom = function () {
        var path = 'http://bn.imguol.com/1110/uol/cutting/';
        var ceFecharBtn = path+'fechar.gif';
        var ceRetrairBtn = path+'retrair.gif';
        var div = $private.appender.applyAttributes(document.createElement('div'), {'id' : 'closeCuttingBtn'});
        div.innerHTML = '<img src="' + ceFecharBtn + '" width="55" height="15" onclick="window.UOLI.'+$public.getConfigTitle()+'.hideCuttingEdge();" style="cursor:pointer;">';
        return div;
    };

    $private.createPreloader = function () {
        var img = 'http://ci.i.uol.com.br/album/wait_inv.gif';
        var image = new Image();
        $private.applyStyles(image, {'position' : 'absolute', 'top' : '50%', 'left' : '50%', 'zIndex' : 1, 'margin' : '-16px 0 0 -16px'});
        $private.appender.applyAttributes(image, {'id' : $private.PRELOAD_CONTAINER ,'src' : img});
        return image;
    };

    $private.addPreloader = function (id) {
        var div = $private.getContainerById(id);
        try {
            var preloadImage = $private.createPreloader();
            div.appendChild(preloadImage);
        } catch(e) {}
    };

    $private.removePreloader = function (id) {
        var div = $private.getContainerById(id);
        var preloadImage = $private.appender.byId($private.PRELOAD_CONTAINER);
        try{
            div.removeChild(preloadImage);
        } catch(e) {}
    };

    $private.getContainerById = function (id) {
        var idFound;
        if(id.indexOf('first') > -1) {
            idFound = $private.FIRST_CONTAINER;
        } else if (id.indexOf('second') > -1) {
            idFound = $private.SECOND_CONTAINER;
        }
        return $private.appender.byId(idFound);
    };

    $private.createCustomCssOnHeader = function () {
        var head = $private.appender.getOwnerDocument().head || $private.appender.getOwnerDocument().getElementsByTagName('head')[0];
        var css = '#'+ $private.config.bannerId +'{height:180px; cursor:pointer; position:relative; background-color:#ffffff; width: 1190px;top:15px; overflow: hidden; transition: .5s ease-in;} #'+$private.config.bannerId+'span {position:absolute; top:0px; right:5px;} #closeCuttingBtn {top:0; right:0; width:55px; height:15px; position:absolute; cursor:pointer; z-index: 1;}';

        var styleElement = document.createElement("style");
        styleElement.type = "text/css";

        if (styleElement.styleSheet) {
            styleElement.styleSheet.cssText = css;
        } else {
            styleElement.appendChild(document.createTextNode(css));
        }

        head.insertBefore(styleElement, head.firstChild);
    };    

    $private.toggleVisibility = function (element) {
        var div = $private.appender.byId(element);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    };

    $private.track = function (params, id) {
        if (id !== "" && id !== undefined) {
            var imgTracker = new Image();
            imgTracker.src = 'http://www5.smartadserver.com/imp?imgid=' + id + '&tmstp=' + (new Date()).getTime() + '&tgt=&' + params;
        }
    };

    $private.createIframe = function (id) {
        var iframe = $private.appender.applyAttributes(document.createElement('iframe'), {
        'id' : id, 'marginwidth' : '0', 'marginheight' : '0', 'frameborder' : '0', 'scrolling' : 'no'});
        return iframe;
    };

    $private.sourceIframeContent = function (id, url) {
        $private.appender.applyAttributes($private.appender.byId(id), {
        'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);', 'src' : url});
    };

    $private.writeIframeContent = function (id, src) {
        var iframe = $private.appender.applyAttributes($private.appender.byId(id), {
        'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);'});
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    };

    $private.loadIframeContent = function (id, src) {
        $private.addPreloader(id);
        if($private.config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] ' + $public.getConfigTitle() + ' src em modo write incorreto');
                return;
            }
            $private.writeIframeContent(id, src);
        } else {
            $private.sourceIframeContent(id, src);
        }
    };

    $private.createCloseButtom = function () {
        var path = 'http://bn.imguol.com/1110/uol/cutting/';
        var img = $private.appender.applyAttributes(document.createElement('img'), {'src' : path+'fechar.gif'});
        var btn = $private.appender.applyAttributes(document.createElement('a'), {'value' : 'Fechar'});

        $private.applyStyles(btn, {
            'position' : 'absolute',
            'width' : '55px',
            'height' : '15px',
            'top' : ($private.config.format.expanded.marginCloseButtom.top || 0) + 'px',
            'right' : ($private.config.format.expanded.marginCloseButtom.right || 0) + 'px',
            'zIndex' : 3,
            'cursor' : 'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            $public.close();
        });

        btn.appendChild(img);
        return btn;
    };

    $private.applyStyles = function (element, styles) {
        for (var style in styles){
            element.style[style] = styles[style];
        }
    };

    $private.createFirstContent = function (src, width, height) {
        var div = $private.createContainer($private.FIRST_CONTAINER);

        $private.applyStyles(div, {
            'position' : 'relative',
            'width':width + 'px',
            'height' : height + 'px',
            'background' : $private.config.format.initial.background,
            'overflow': 'hidden'
        });

        if($private.config.format.initial.clickArea.toString() === 'true') {
            var clickArea = $private.createClickArea($private.FIRST_CLICKAREA);            
            div.appendChild($private.firstClickArea(clickArea, 'absolute'));
        }

        var iframe = $private.createIframe($private.FIRST_IFRAME);
        div.appendChild(iframe);

        $private.applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width,
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top,
            'right' : iframe.parentNode.style.right
        });

        return div;
    };

    $private.createSecondContent = function (src) {
        var div = $private.createContainer($private.SECOND_CONTAINER);

        $private.applyStyles(div, {
            'position' : 'fixed',
            'width' : '100%',
            'height' : '100%',
            'top' : '0px',
            'left' : '0px',
            'display' : 'none',
            'zIndex' : 7000000,
            'background' : $private.config.format.expanded.background,
            'overflow' : 'auto'
        });

        if ($private.config.format.expanded.clickArea.toString() === 'true') {
            var clickArea = $private.createClickArea($private.SECOND_CLICKAREA);
            div.appendChild($private.secondClickArea(clickArea, 'fixed', $public.clickTag));
        }

        var iframe = $private.createIframe($private.SECOND_IFRAME);
        div.appendChild(iframe);

        $private.applyStyles(iframe, {
            'visibility' : 'hidden',
            'position' : 'absolute',
            'width' : $private.config.format.expanded.minWidth,
            'height' : $private.config.format.expanded.height,
            'top' : '50%',
            'left' : '50%',
            'margin': '-300px 0 0 -640px'
            //'margin' : (-iframe.parentNode.style.height*0.5)+'px 0 0 '+(-iframe.parentNode.style.width*0.5)+'px'
        });

        var btn = $private.createCloseButtom();
        div.appendChild(btn);
        return div;
    };

    $private.createContainer = function (name) {
        var div = $private.appender.applyAttributes(document.createElement('div'), {'id' : name});
        return div;
    };

    $private.createClickArea = function (id) {
        var clickArea = $private.appender.applyAttributes(document.createElement('a'), {'id' : id});
        return clickArea;
    };

    $private.firstClickArea = function (clickArea, position) {        
        $private.applyStyles(clickArea, {
            'position' : position,
            'width' : '100%',
            'height' : '100%',
            'top' : '0',
            'left' : '0', 'zIndex' : 1, 'cursor' : 'pointer'});

        $private.debugClickArea(clickArea);     

        if ($private.config.format.initial.openByHover) {
            clickArea.addEventListener('mouseenter', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = true;
                window.setTimeout(function(){
                    if (e.target.hover) $private.handleMouseClick(e);
                }, $private.config.format.initial.hoverTime || 1500);
            });

            clickArea.addEventListener('mouseleave', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = false;
            });   
        }

        clickArea.addEventListener('click', function (e) {
            e.target.hover = false;
            $private.handleMouseClick(e);
        });        
        return clickArea;
    };

    $private.handleMouseClick = function (e) {
        $public.open();
        return false;
    };

    $private.secondClickArea = function (clickArea, position, callback) {
        $private.applyStyles(clickArea, {
            'position' : position,
            'width' : '100%',
            'height' : '100%',
            'top' : '0',
            'left' : '0',
            'zIndex' : 1,
            'cursor' : 'pointer'
        });
        $private.debugClickArea(clickArea);

        clickArea.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            callback();
        });
        return clickArea;
    };

    $private.debugClickArea = function (clickArea) {
        if ($private.config.debug.toString() === 'true') {
            clickArea.style.background = '#ff0000';
            clickArea.style.opacity = 0.5;
        }
    };
}
var viewport = new Viewport(new Config());
viewport.render();
top.window.UOLI = top.window.UOLI || {};
top.window.UOLI[viewport.getConfigTitle()] = {};
top.window.UOLI[viewport.getConfigTitle()].clickTag = viewport.clickTag;
top.window.UOLI[viewport.getConfigTitle()].getClickTag = viewport.getClickTag;
top.window.UOLI[viewport.getConfigTitle()].loadComplete = viewport.loadComplete;
top.window.UOLI[viewport.getConfigTitle()].open = viewport.open;
top.window.UOLI[viewport.getConfigTitle()].close = viewport.close;
top.window.UOLI[viewport.getConfigTitle()].handleMouseLeave = viewport.handleMouseLeave;
top.window.UOLI[viewport.getConfigTitle()].handleMouseEnter = viewport.handleMouseEnter;
top.window.UOLI[viewport.getConfigTitle()].hideCuttingEdge = viewport.hideCuttingEdge;
})(this, document);

</script>