(function (window, document, undefined){
'use strict';
/*
* © UOL RICHMEDIA - Todos os direitos reservados
* VERSAO: 1.0.1
*/
function Config () {
    return {
        debug: true, //habilita o modo visual das mascaras
        title : 'cutting', //nome da campanha ou peca
        expand : true, //habilita a expansão
        cookieExpire : true, //o cookie de fechamento expira apenas depois do numero de horas definido na varaivel "cookieHours"
        cookieHours : 8, //define o tempo(horas) para a expiração do cookie
        format: {
            initial: {
                width : '1190', // largura do formato inicial
                height : '70', // altura do formato inicial
                background : '#ffffff', // cor de fundo Ex. #000000
                src : 'http://placehold.it/1190x70', //path do html ou variavel do conteudo html
                openByHover: false, //flag para habilitar a expansão por mouseover
                delayToOpen: 1000 //tempo para abertura com mouseover
            },
            expanded : {
                width : '1190', //largura do formato expandido
                height : '460', //altura do formato expandido
                clickArea : 'true', // em true habilita a mascara - false desabilita
                src : 'http://placehold.it/1190x500', //path do html ou variavel do conteudo html
                closeBtn: {
                    marginRight : 0,
                    marginTop : 0
                },
            },
            write: false // em false utilizado o modo de carregamento via src - em true utilizado o modo write
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : 'banner-1190x70-2-Area' // banner ID do container
    };
}

function Appender () {
    var $public = this;
    var $private = {};

    $public.setContainer = function (bannerId) {
        $private.container = document.getElementById(bannerId) || top.document.getElementById(bannerId);
        //$private.hideChildren($public.getContainer());
    };

    $private.hideChildren = function (element) {
        if (!element) {
            return;
        }
        var childNodes = element.children;
        for (var i = 0, length = childNodes.length; i < length; i++) {
            $public.applyStyles(childNodes[i], {'display': 'none'});
        }
    };

    $public.getContainer = function () {
        return $private.container;
    };

    $public.insertOnContainer = function (element) {
        if (!$private.container) {
            return;
        }
        $private.container.appendChild(element);
    };

    $public.getOwnerDocument = function () {
        return $private.container.ownerDocument;
    };

    $public.byId = function (id) {
        return $public.getOwnerDocument().getElementById(id);
    };

    $private.byClassName = function (id) {
        return $public.getOwnerDocument().getElementsByClassName(id);
    };

    $public.getContainerById = function (id, first, second) {
        var idFound;
        if(id.indexOf('first') > -1) {
            idFound = first;
        } else if (id.indexOf('second') > -1) {
            idFound = second;
        }
        return $public.byId(idFound);
    };


    $public.applyAttributes = function (element, properties) {
        for (var property in properties) {
            if (!properties.hasOwnProperty(property)) {
                continue;
            }
            element.setAttribute(property, properties[property]);
        }
        return element;
    };

    $public.applyStyles = function (element, styles) {
        for (var style in styles) {
            if (!styles.hasOwnProperty(style)) {
                continue;
            }
            element.style[style] = styles[style];
        }
        return element;
    };

    $public.appenderMainContainer = function (id, element) {
        var main = $public.byId(id) || $private.byClassName(id);
        if (main.length > 0) {
            main[0].appendChild(element);
            return;
        }
        main.appendChild(element);
    };
}

function Tracking(debug) {
    
    var $private = {};
    var $public = this;
    $private.appender = new Appender();

    $public.trackByParams = function (params, id) {
        if (id === "" || id === undefined) {
            return;
        }
        var url = 'http://www5.smartadserver.com/imp?imgid=' + id + '&tmstp=' + (new Date()).getTime() + '&tgt=&' + params;
        if (debug.toString() === 'true') {
            console.log('[UOL RICHMEDIA SMART TRACKING DEBUG MODE]', url);
            return;
        }
        $private.appender.applyAttributes(new Image(), {'src': url});
    };
    
}
function Constants(configConstants) {
    var config = configConstants || {};
    return {
        FIRST_CONTAINER   : 'uol-container-' + config.title,
        FIRST_IFRAME      : 'uol-iframe-' + config.title,
        PRELOAD_CONTAINER : 'uol-preloader-' + config.title,
        STATE : 'retract'
    };
}
function CuttingEdge(configCutting){

    var $public = this;
    var $private = {};

    $private.config = configCutting || {};
    $private.appender = new Appender();
    $private.constants = new Constants($private.config);
    $private.tracking = new Tracking($private.config.debug);

    $public.render = function () {
        if ($private.getCookie()) {
            return;
        }

        $private.appender.setContainer($private.config.bannerId);
        if (!$private.appender.getContainer()) {
            if ($private.config.debug.toString() === 'true') {
                console.log('[UOL RICHMEDIA] banner id nao encontrado');
            }
            return;
        }

        var firstContainer = $private.createFirstContainer($private.config.format.initial.width,$private.config.format.initial.height);

        $private.createCustomCssOnHeader();
        $private.addCssAnimation();
        $private.appender.insertOnContainer(firstContainer);
        $private.loadIframeContent($private.constants.FIRST_IFRAME, $private.config.format.initial.src);

        var root = $private.appender.getContainer().getElementsByTagName('iframe')[0];
        $private.appender.applyStyles(firstContainer, {
            'position' : 'absolute',
            'left': root.offsetLeft+'px',
            'top': root.offsetTop+'px'
        });
    };

    $private.loadIframeContent = function (id, src) {
        if($private.config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] src em modo write incorreto');
                return;
            }
            $private.writeIframeContent(id, src);
        } else {
            $private.sourceIframeContent(id, src);
        }
    };

    $public.getConfigTitle = function () {
        return $private.config.title.toUpperCase();
    };

    $public.clickTag = function () {
        $public.close();
        try {
            window.open($public.getClickTag(), '_blank');
        } catch (e) {
            console.log('[CLICKTAG COM VALOR INVALIDO]');
        }
    };

    $public.getClickTag = function (value) {
        var index = (value === undefined) ? 0 : value;
        return $private.config.clickTag[index];
    };

    $public.open = function() {
        if ($private.appender.isBrowser(['msie 9.0', 'msie 10.0'])) {
            $public.clickTag();
            return;
        }
        $private.showGlider();
        $private.refreshHome('pause');
        $private.tracking.trackByParams('open', $private.config.track.open);
        alert("ABRIR");
    };

    $public.close = function() {
        $private.closeAnimation();
        $private.refreshHome('start');
        $private.tracking.trackByParams('close', $private.config.track.close);
    };

    $private.createCustomCssOnHeader = function () {
        var head = $private.appender.getOwnerDocument().head || $private.appender.getOwnerDocument().getElementsByTagName('head')[0];
        var css = '#'+ $private.config.bannerId +'{height:auto; cursor:pointer; position:relative; background-color:#ffffff; width: 1190px; margin-top:5px; overflow: hidden; transition: .5s ease-in;} #'+$private.config.bannerId+'span {position:absolute; top:0px; right:5px;} #closeCuttingBtn {top:0; right:0; width:55px; height:15px; position:absolute; cursor:pointer; z-index: 1;}';

        var styleElement = document.createElement("style");
        styleElement.type = "text/css";

        if (styleElement.styleSheet) {
            styleElement.styleSheet.cssText = css;
        } else {
            styleElement.appendChild(document.createTextNode(css));
        }

        head.insertBefore(styleElement, head.firstChild);
    };

    $public.hideCuttingEdge = function() {
        $private.setCookie();
        var div = $private.appender.byId($private.constants.FIRST_CONTAINER);
        div.style.height = '0px';
    };

    $private.setCookie = function () {
        var date = new Date();
        date.setTime(date.getTime()+(($private.config.cookieHours)*60*60*1000));
        var expires = 'expires='+date.toUTCString() + '; path=/';
        $private.appender.getOwnerDocument().cookie='CEHome=0;domain=.uol.com.br;' + (($private.config.cookieExpire) ? expires : '');
    };

    $private.getCookie = function () {
        if (document.cookie.indexOf("CEHome") != -1) {
           return true;
        }
        return false;
    };

    $private.refreshHome = function(value) {
        if (window.homeUOL === undefined) {
            return;
        }
        if (window.homeUOL.modules === undefined) {
            return;
        }
        if (window.homeUOL.modules.refresh === undefined) {
            return;
        }
        if(value === 'pause') {
            window.homeUOL.modules.refresh.pause();
        } else if(value === 'start') {
            window.homeUOL.modules.refresh.start();
        }
    };

    $private.reloadIframeContent = function (id) {
        var iframe = $private.appender.byId(id);
        if($private.config.format.write.toString() === 'false') {
            $private.appender.applyAttributes(iframe, {'src' : ''});
        } else {
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write('');
            iframe.contentWindow.document.close();
        }
    };

    $public.loadComplete = function (element) {};


    $private.writeIframeContent = function (id, src) {
        var iframe = $private.appender.applyAttributes($private.appender.byId(id), {
            'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);'
        });
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    };

    $private.sourceIframeContent = function (id, url) {
        $private.appender.applyAttributes($private.appender.byId(id), {
            'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);',
            'src' : url
        });
    };

    $private.createFirstContainer = function (width, height) {
        var div = $private.createContainer($private.constants.FIRST_CONTAINER);
        div.style.background = $private.config.format.initial.background;

        $private.appender.applyStyles(div, {
            'position':'relative',
            'width':width+'px',
            'height':height+'px',
            'transition': '.5s ease-in',
            'overflow': 'hidden'
        });

        div.appendChild($private.createClickArea(width, height));

        var iframe = $private.createIframe($private.constants.FIRST_IFRAME);
        div.appendChild(iframe);

        $private.appender.applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width,
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top,
            'right' : iframe.parentNode.style.right,
            'transition': '.5s ease-in'
        });

        $private.btn = $private.createCloseButtom();
        div.appendChild($private.btn);

        return div;
    };

    $private.createContainer = function (name) {
        var div = $private.appender.applyAttributes(document.createElement('div'), {'id' : name});
        return div;
    };

    $private.createClickArea = function (width, height) {
        var clickArea = document.createElement('a');
        $private.appender.applyStyles(clickArea, {
            'position':'absolute',
            'width':width+'px',
            'height':height+'px',
            'top':'0',
            'left':'0',
            'zIndex':1,
            'cursor':'pointer',
            'transition': '.5s ease-in'
        });

        $private.debugClickArea(clickArea);

        clickArea.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.target.hover = false;
            $public.open();
        });

        if ($private.config.format.initial.openByHover) {
            clickArea.addEventListener('mouseenter', function(e){
                if($private.constants.STATE === 'expanded') {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = true;
                window.setTimeout(function(){
                    if (e.target.hover) {
                        $public.open();
                    }
                }, $private.config.format.initial.delayToOpen || 1500);
            });

            clickArea.addEventListener('mouseleave', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = false;
            });
        }
        return clickArea;
    };

    $private.debugClickArea = function (clickArea) {
        if ($private.config.debug.toString() === 'true') {
            clickArea.style.background = '#ff0000';
            clickArea.style.opacity = 0.5;
        }
    };

    $private.createIframe = function (id) {
        var iframe = $private.appender.applyAttributes(document.createElement('iframe'), {
            'id' : id,
            'marginwidth' : '0',
            'marginheight' : '0',
            'frameborder' : '0',
            'scrolling' : 'no'
        });
        return iframe;
    };

    $private.createCloseButtom = function () {
        var img = $private.appender.applyAttributes(document.createElement('img'),
            {'src' : 'http://bn.imguol.com/1110/uol/cutting/fechar.gif'});

        var btn = $private.appender.applyAttributes(document.createElement('a'),
            {'value' : 'Fechar'
        });

        $private.appender.applyStyles(btn, {
            'position':'absolute',
            'width':'55px',
            'height':'15px',
            'top': ($private.config.format.expanded.closeBtn.marginTop || 0)+'px',
            'right': ($private.config.format.expanded.closeBtn.marginRight || 0)+'px',
            'zIndex':3,
            'cursor':'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            if($private.constants.STATE === 'retract') {
                $public.hideCuttingEdge();
            } else {
                $public.close();
            }
        });

        btn.appendChild(img);
        return btn;
    };
}

var cuttingEdge = new CuttingEdge(new Config());
cuttingEdge.render();
top.window.UOLI = top.window.UOLI || {};
top.window.UOLI[cuttingEdge.getConfigTitle()] = {};
top.window.UOLI[cuttingEdge.getConfigTitle()].clickTag = cuttingEdge.clickTag;
top.window.UOLI[cuttingEdge.getConfigTitle()].getClickTag = cuttingEdge.getClickTag;
top.window.UOLI[cuttingEdge.getConfigTitle()].loadComplete = cuttingEdge.loadComplete;
top.window.UOLI[cuttingEdge.getConfigTitle()].open = cuttingEdge.open;
top.window.UOLI[cuttingEdge.getConfigTitle()].close = cuttingEdge.close;
})(this, document);