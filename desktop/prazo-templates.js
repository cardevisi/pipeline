//*********************
// Templates Dynad
//*********************

Tipos de players suportados: UOL TV(sem suporte para o momento), Youtube, Vimeo.
Veiculação em safeframe: suportado desde que haja área mínima para expansão, do contrário a expansão não irá ocorrer.

1) Retângulo com vídeo expansível - versão desktop - 3 dias.
2) Retângulo com vídeo 300x250 (WIFI - Conexao de dados 3g /4g)- 2 dias. (x2)
3) Superbanner com vídeo expansível - 728x90 para 728x409.5 - 2 dias.
4) Retângulo com vídeo tela cheia - 300x250 para 100%x100% - 3 dias.
5) Retângulo com vídeo expansível - 300x250 para 800x450 - 3 dias.
6) Retângulo com vídeo expansível - 300x250 para 500x281 - 3 dias.
7) Billboard com vídeo 970x250 - 1 dias.
8) Floating com vídeo Tela cheia - 3 dias.
9) Halfpage Multimídia expansível - 300x600 para 500x280 - 3 dias.
10) Superbanner multimídia expansível - 728x90 para 728x280 - 3 dias.

Prazo estimado em 30 dias. Média estimada de 3 dias por template.



//****************************
// Tarefas no Backlog:
//****************************

Inserção - Refactoring dos templates.
Dynad - Implementação dos templates.
Native - templates para o DFP.




//****************************
