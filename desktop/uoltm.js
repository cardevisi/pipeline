/* UOL Tag Manager Copyright 2016. Todos os diretos reservados | Versao do repositório: 61 */ 
(function (window, document, undefined){
'use strict';
var tmConsole;
function TypeValidator () {
    var $public = this;
    var $private = {};
    $public.isDefined = function (value) {
        return value !== undefined && value !== null;
    };
    $public.isString = function (value) {
        return value !== undefined && (typeof value) === 'string' && ($private.stringIsNotEmpty(value));
    };
    $private.stringIsNotEmpty = function(string) {
        if ($private.trimString(string).length > 0) {
            return true;
        }
        return false;
    };
    $private.trimString = function (string) {
        return string.replace(/^(\s+)|(\s+)$/gm, '').replace(/\s+/gm, ' ');
    };
    $public.isArray = function (value) {
        return value && value.constructor === Array;
    };
    $public.isObject = function (entity) {
        return entity && entity.constructor === Object;
    };
    $public.isFunction = function (value) {
        return value !== undefined && value.constructor === Function;
    };
    $public.isNumber = function (value) {
        return Number(value) === value;
    };
    $public.isInt = function (value) {
        return $public.isNumber(value) && value % 1 === 0;
    };
    $public.isRegExp = function (value) {
        return value !== undefined && value.constructor === RegExp;
    };
    $public.isNumericString = function (value) {
        return $public.isString(value) && !isNaN(value);
    };
    $public.isBoolean = function (value) {
        return value !== undefined && value.constructor == Boolean;
    };
    return $public;
}
function QueryString () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.getValue = function (name) {
        if (!$private.queryStrings) {
            return;
        }
        return $private.queryStrings[name];
    };
    $public.getQueryStrings = function () {
        return $private.queryStrings;
    };
    $public.setValues = function () {
        if (!$private.typeValidator.isString($public.getSearch())) {
            return;
        }
        var substrings = $public.getSearch().substring(1).split('&');
        if (!$private.typeValidator.isArray(substrings)) {
            return;
        }
        if (substrings.length === 0) {
            return;
        }
        for (var i = 0, length = substrings.length; i < length; i++) {
            if ($private.typeValidator.isString(substrings[i])) {
                $private.addQueryString(substrings[i]);
            }
        }
    };
    $public.getSearch = function () {
        return window.location.search;
    };
    $private.addQueryString = function (substring) {
        var queryObject = $private.getQueryObject(substring);
        if (!queryObject) {
            return;
        }
        $private.queryStrings = $private.queryStrings || {};
        $private.queryStrings[queryObject.key] = queryObject.value;
    };
    $private.getQueryObject = function (substring) {
        var query = substring.split('=');
        if (!$private.typeValidator.isString(query[0])) {
            return;
        }
        var result = {
            'key': query[0],
            'value': 'true'
        };
        if ($private.typeValidator.isString(query[1])) {
            result.value = query[1];
        }
        return result;
    };
    $public.setValues();
}
function Logs () {
    var $private = {};
    var $public = this;
    $private.queryString = new QueryString();
    $private.typeValidator = new TypeValidator();
    $private.tmConsole = (typeof tmConsole !== 'undefined' ? tmConsole : undefined);
    $private.history = {
        'warn'  : [],
        'error' : [],
        'info'  : [],
        'debug' : [],
        'log'   : []
    };
    $public.getPrefix = function (prefix) {
        return $private.prefix;
    };
    $public.setPrefix = function (prefix) {
        if ($private.typeValidator.isString(prefix)) {
            $private.prefix = '[' + prefix + '] ';
        }
    };
    $public.warn = function (message, namTag, namespace, executionTime) {
        if ($public.consoleValidator($private.tmConsole)) {
            $private.tmConsole.append(namTag, namespace, message, executionTime, 'warn');
        }
        return $private.print(message, 'warn');
    };
    $public.error = function (message, namTag, namespace, executionTime) {
        if ($public.consoleValidator($private.tmConsole)) {
            $private.tmConsole.append(namTag, namespace, message, executionTime, 'error');
        }
        return $private.print(message, 'error');
    };
    $public.info = function (message) {
        return $private.print(message, 'info');
    };
    $public.debug = function (message) {
        return $private.print(message, 'debug');
    };
    $public.log = function (message, namTag, namespace, executionTime) {
        if ($public.consoleValidator($private.tmConsole)) {
            $private.tmConsole.append(namTag, namespace, message, executionTime, 'success');
        }
        return $private.print(message, 'log');
    };
    $public.consoleValidator = function (tmConsole) {
        if (!tmConsole) {
            return false;
        }
        if (!$private.typeValidator.isFunction(tmConsole.append)) {
            return false;
        }
        return true;
    };
    $private.print = function (msg, fn) {
        if (!$private.prefix) {
            return;
        }
        if (!$private.typeValidator.isString(msg)) {
            return;
        }
        msg = $private.prefix + msg;
        $public.setHistory(fn, msg);
        if ($public.isActive() === false || !$private.hasConsole()) {
            return;
        }
        return $private.runLogMethod(fn, msg);
    };
    $public.isActive = function () {
        if ($private.queryString.getValue('tm') === 'debug') {
            return true;
        }
        return false;
    };
    $public.getHistory = function (methodName) {
        if ($private.typeValidator.isArray($private.history[methodName])) {
            return $private.history[methodName];
        }
        return;
    };
    $public.setHistory = function (fn, msg) {
        if ($private.typeValidator.isString(msg) && $private.typeValidator.isArray($private.history[fn])) {
            $private.history[fn].push(msg);
        }
    };
    $private.hasConsole = function () {
        if (!$private.typeValidator.isDefined($public.getConsole())){
            return false;
        }
        if (!$private.typeValidator.isDefined($public.getConsoleLog())) {
            return false;
        }
        return true;
    };
    $public.getConsole = function () {
        return window.console;
    };
    $public.getConsoleLog = function () {
        return $public.getConsole().log;
    };
    $private.runLogMethod = function (fn, msg) {
        if ($private.typeValidator.isDefined($public.getConsole()[fn])) {
            $public.getConsole()[fn](msg);
            return fn;
        }
        window.console.log(msg);
        return 'log';
    };
}
function NameSpace (packageName) {
    var $private = {};
    var $public = this;
    $private.version = '1.0.129';
    $private.validator = new TypeValidator();
    $public.init = function (packageName) {
        if ($private.validator.isString(packageName)) {
            return $public.create(packageName);
        }
    };
    $public.create = function (packageName) {
        $private.createUOLPD();
        $private.createTagManager();
        return $private.createPackage(packageName);
    };
    $private.createUOLPD = function () {
        if (!$private.validator.isObject(window.UOLPD)) {
            window.UOLPD = {};
        }
    };
    $private.createTagManager = function () {
        if (!$private.validator.isObject(UOLPD.TagManager) && !$private.validator.isFunction(UOLPD.TagManager) && typeof UOLPD.TagManager !== "object") {
            UOLPD.TagManager = {};
        }
    };
    $private.createPackage = function (packageName) {
        if (!$private.validator.isString(packageName)) {
            return UOLPD.TagManager;
        }
        if (!$private.validator.isObject(UOLPD.TagManager[packageName])) {
            UOLPD.TagManager[packageName] = {};
        }
        UOLPD.TagManager[packageName].version = $private.version;
        if (!$private.validator.isArray(UOLPD.TagManager[packageName].config)) {
            UOLPD.TagManager[packageName].config = [];
        }
        if (!$private.validator.isObject(UOLPD.TagManager[packageName].log)) {
            UOLPD.TagManager[packageName].log = new Logs();
            UOLPD.TagManager[packageName].log.setPrefix('UOLPD.TagManager.' + packageName);
        }
        return UOLPD.TagManager[packageName];
    };
    return $public.init(packageName);
}
function ScriptUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.dataCalls = 'data-uoltm-calls';
    $public.hasTagScript = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return false;
        }
        if (src.length === 0) {
            return false;
        }
        if ($public.findScript($private.removeProtocol(src))) {
            return true;
        }
        return false;
    };
    $private.removeProtocol = function (value) {
        return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };
    $public.findScript = function (src) {
        var scripts = document.getElementsByTagName('script');
        for (var i = 0, length = scripts.length; i < length; i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if (scriptSrc && $private.isSrcEqual(scriptSrc, src)) {
                return scripts[i];
            }
        }
        return;
    };
    $private.isSrcEqual = function (src1, src2) {
        return (src1.indexOf(src2) > -1);
    };
    $public.createScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        tag.async = true;
        return tag;
    };
    $public.appendTag = function (script) {
        if (!$private.typeValidator.isDefined(script)) {
            return;
        }
        if (script.constructor === HTMLScriptElement) {
            $private.lastScriptsParent().appendChild(script);
            return true;
        }
    };
    $private.lastScriptsParent = function () {
        return document.getElementsByTagName('script')[0].parentNode;
    };
    $public.createSyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        document.write('<scr' + 'ipt src="'+ src + '"></scr' + 'ipt>');
    };
}
function StringUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.trim = function (value) {
        if (!$private.typeValidator.isString(value)) {
            return;
        }
        if (value.length === 0) {
            return;
        }
        value = value.replace(/^(\s+)|(\s+)$/gm, '').replace(/\s+/gm, ' ');
        return value;
    };
    $public.getValueFromKeyInString = function (str, name, separator) {
        if (!$private.typeValidator.isString(name) || name.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(str) || str.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(separator) || separator.length === 0) {
            return;
        }
        if (str.substring(str.length - 1)) {
            str += separator;
        }
        name += '=';
        var startIndex = str.indexOf(name);
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = str.indexOf('=', startIndex) + 1;
        var endIndex = str.indexOf(separator, middleIndex);
        if (endIndex === -1){
            endIndex = str.length;
        }
        return unescape(str.substring(middleIndex, endIndex));
    };
    return $public;
}
function RemoteStorage () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.validator = new TypeValidator();
    $private.path = 'https://tm.uol.com.br/mercurio.html';
    $private.iframe = undefined;
    $private.iframeLoaded = false;
    $private.queue = [];
    $private.requests = {};
    $private.id = 0;
    $public.init = function () {
        $private.hasLoaded(function () {
            if (!$private.isIframeInPage()) {
                $private.setIframe();
            }
            $private.setListener($private.iframe);
            return true;
        });
        return $public;
    };
    $private.isIframeInPage = function () {
        var iframes = document.getElementsByTagName('iframe');
        for (var i = 0; i < iframes.length; i++) {
            if (iframes[i].getAttribute('src') == $private.path) {
                $private.iframe = iframes[i];
                return true;
            }
        }
        return false;
    };
    $public.get = function (key, callback) {
        $private.communicator('get', key, undefined, callback);
    };
    $public.set = function (key, value, callback) {
        $private.communicator('set', key, value, callback);
    };
    $public.removeItem = function (key, callback) {
        callback = callback || function () {};
        $private.communicator('removeItem', key, undefined, callback);
    };
    $private.hasLoaded = function (fn, timeout) {
        timeout = (timeout ? timeout + 10 : 10);
        if (typeof fn != "function") {
            return false;
        }
        if (document.body !== null) {
            fn();
            return true;
        }
        setTimeout(function() {
            $private.hasLoaded(fn, timeout);
        }, timeout);
    };
    $private.setIframe = function () {
        $private.iframe = $private.buildIframe();
    };
    $private.buildIframe = function () {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('src', $private.path);
        iframe.setAttribute('id', 'tm-remote-storage');
        iframe.setAttribute('style', 'position:absolute;width:1px;height:1px;left:-9999px;display:none;');
        document.body.appendChild(iframe);
        return iframe;
    };
    $private.setListener = function (iframe) {
        var hasEventListener = (window.addEventListener !== undefined);
        var method = (hasEventListener) ? 'addEventListener' : 'attachEvent';
        var message = (hasEventListener) ? 'message' : 'onmessage';
        var load = (hasEventListener) ? 'load' : 'onload';
        iframe[method](load, $private.iframeLoadHandler);
        window[method](message, $private.messageHandler);
        return iframe;
    };
    $private.iframeLoadHandler = function (event) {
        $private.iframeLoaded = true;
        if (!$private.queue.length) {
            return;
        }
        for (var i = 0, length = $private.queue.length; i < length; i++) {
            $private.sendMessageToIframe($private.queue[i]);
        }
        $private.queue = [];
    };
    $private.messageHandler = function (event) {
        try {
            var data = JSON.parse(event.data);
            $private.requests[data.id].callback(data.key, data.value);
            delete $private.requests[data.id];
        } catch (err) {}
    };
    $private.communicator = function (method, key, value, callback) {
        if (!$private.validator.isString(key)) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        if (method === 'set' && value === undefined) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        if (!$private.validator.isFunction(callback)) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        var request = {
            id: ++$private.id,
            key: key,
            method: method
        };
        if (value) {
            request.value = value;
        }
        var data = {
            request: request,
            callback: callback
        };
        if ($private.iframeLoaded) {
            $private.sendMessageToIframe(data);
        } else {
            $private.queue.push(data);
        }
        if (!$private.iframe) {
            $public.init();
        }
    };
    $private.sendMessageToIframe = function (data) {
        $private.requests[data.request.id] = data;
        data = JSON.stringify(data.request);
        if (!data) {
            return;
        }
        $private.iframe.contentWindow.postMessage(data, $private.path);
    };
    return $public.init();
}
function TrackManager () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.imagesSrc = [];
    $private.baseUrl = '//logger.rm.uol.com.br/v1/?prd=98&grp=Origem:';
    $private.raffledRate = Math.round(Math.random() * 100);
    $private.samplingRate = 1;
    $public.trackSuccess = function (msr) {
        $public.sendPixel($private.getPixelSrc($private.getMeasure(msr)), $private.samplingRate);
    };
    $private.getMeasure = function (msr) {
        if (!$private.typeValidator.isString(msr)) {
            return;
        }
        return '&msr=' + encodeURIComponent(msr) + ':1';
    };
    $private.getPixelSrc = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }
        if (!$public.getModuleName()) {
            return;
        }
        if (!$public.getRepoId()) {
            return;
        }
        return $private.baseUrl + $public.getModuleName() + $public.getRepoId() + src;
    };
    $public.getModuleName = function (moduleName) {
        return $private.moduleName;
    };
    $public.setModuleName = function (moduleName) {
        if ($private.typeValidator.isString(moduleName)) {
            $private.moduleName = 'TM-' + moduleName + ';';
        }
    };
    $public.getRepoId = function () {
        if (!$private.repoId) {
            $public.setRepoId();
        }
        return $private.repoId;
    };
    $public.setRepoId = function() {
        var src = $private.getScriptSrc();
        if (!src) {
            return;
        }
        var srcMatch = src.match(/uoltm.js\?id=(.{6}).*/);
        var repoId = srcMatch ? srcMatch[1] : null;
        if (repoId) {
            $private.repoId = 'tm_repo_id:' + repoId;
        }
    };
    $private.getScriptSrc = function() {
        var script = document.querySelector('script[src*="tm.jsuol.com.br/uoltm.js"]');
        var src = script ? script.src : null;
        return src;
    };
    $public.sendPixel = function (src, samplingRate) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }
        if (!$private.isTrackEnabled(samplingRate)) {
            return;
        }
        var img = document.createElement('img');
        img.setAttribute('src', src);
        $public.imagesSrc.push(img.src);
    };
    $private.isTrackEnabled = function (samplingRate) {
        if (window.location.protocol.match('https')) {
            return false;
        }
        try {
            if (window.localStorage.getItem('trackManager') == 'true') {
                return true;
            }
        } catch (e) {}
        if ($public.getRaffledRate() <= samplingRate) {
            return true;
        }
        return false;
    };
    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };
    $public.trackError = function (errorType, errorEffect) {
        errorType = $private.getErrorType(errorType);
        if (!errorType) {
            return;
        }
        errorEffect = $private.getErrorEffect(errorEffect);
        if (!errorEffect) {
            return;
        }
        var url = $private.getPixelSrc(errorType + errorEffect + $private.getMeasure('Erros'));
        $public.sendPixel(url, 100);
    };
    $private.getErrorType = function (errorType) {
        return $private.generateKeyValue('erro_tipo', errorType);
    };
    $private.generateKeyValue = function (key, value) {
        if (!$private.typeValidator.isString(key)) {
            return;
        }
        if (!$private.typeValidator.isString(value)) {
            return;
        }
        return ';' + key + ':' + value;
    };
    $private.getErrorEffect = function (errorEffect) {
        return $private.generateKeyValue('erro_efeito', errorEffect);
    };
    $public.trackCustom = function (measure, trackType, trackValue) {
        var url;
        var src = $private.generateKeyValue(trackType, trackValue);
        if (!measure || !src) {
            return;
        }
        measure = $private.getMeasure(measure);
        if (measure) {
            url = $private.getPixelSrc(src + measure);
            $public.sendPixel(url, $private.samplingRate);
        }
    };
}
(function (window, document, undefined){
'use strict';
function UniversalVariable (namTag, namespace, timeStampInit) {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DfpSalesConversion');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-track');
    $private.typeValidator = new TypeValidator();
    $private.namTag = namTag;
    $private.namespace = namespace;
    $private.timeStampInit = timeStampInit;
    $private.uVar = {
        'transaction' : {}
    };
    $public.getUvar = function () {
        var uVar = window.universal_variable;
        if (!$private.typeValidator.isObject(uVar)) {
            $private.trackError('universal_variable_invalida', 'A variável universal não foi configurada corretamente');
            return false;
        }
        if (!$private.typeValidator.isObject(uVar.transaction)) {
            $private.trackError('universal_variable_invalida', 'A variável universal não foi configurada corretamente');
            return false;
        }
        if (!$private.typeValidator.isArray(uVar.transaction.line_items)) {
            $private.trackError('universal_variable_invalida', 'A variável universal não foi configurada corretamente');
            uVar.transaction.line_items = [];
        }
        if (isNaN(parseFloat(uVar.transaction.total))) {
            $private.trackError('universal_variable_invalida', 'A variável universal não foi configurada corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(uVar.transaction.order_id)) {
            $private.trackError('universal_variable_invalida', 'A variável universal não foi configurada corretamente');
            uVar.transaction.order_id = Math.random().toString(36).substr(2);
        }
        $private.uVar.transaction.line_items = uVar.transaction.line_items;
        $private.uVar.transaction.total = uVar.transaction.total;
        $private.uVar.transaction.order_id = uVar.transaction.order_id;
        return $private.uVar;
    };
    $private.trackError = function (errorReason, errorMessage) {
        if (!$private.typeValidator.isString(errorReason)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $private.trackManager.trackError(errorReason, 'fluxo_interrompido');
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
    };
}
function Url (namTag, namespace, timeStampInit) {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DfpSalesConversion');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-track');
    $private.typeValidator = new TypeValidator();
    $private.namTag = namTag;
    $private.namespace = namespace;
    $private.timeStampInit = timeStampInit;
    $private.patternUrl = '//pubads.g.doubleclick.net/activity;';
    $public.getUrl = function (config) {
        $private.uVar = new UniversalVariable($private.namTag, $private.namespace, $private.timeStampInit);
        var uvar = $private.uVar.getUvar();
        if (!$private.isValidConfig(config)) {
            return false;
        }
        if (!$private.typeValidator.isObject(uvar)) {
            return false;
        }
        return $private.buildUrl($private.buildUrlAttributes(uvar, config));
    };
    $private.isValidConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(config.activityId)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(config.activityType)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        if (!config.activityType.match(/^itemsPurchase$|^transaction$/)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorReason, errorMessage) {
        if (!$private.typeValidator.isString(errorReason)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $private.trackManager.trackError(errorReason, 'fluxo_interrompido');
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
    };
    $private.buildUrlAttributes = function (uvar, config) {
        return {
            'ord': uvar.transaction.order_id,
            'cost': uvar.transaction.total,
            'qty': $private.getQty(config.activityType, uvar),
            'xsp': config.activityId
        };
    };
    $private.getQty = function (activityType, uvar) {
        var lineItems = uvar.transaction.line_items;
        if (activityType === 'transaction') {
            return '1';
        }
        if (lineItems.length < 2) {
            return '1';
        }
        return lineItems.length.toString();
    };
    $private.buildUrl = function (attributes) {
        var url = $private.patternUrl;
        if (!$private.typeValidator.isObject(attributes)) {
            return false;
        }
        for (var attr in attributes) {
            if (attributes.hasOwnProperty(attr)) {
                url += attr + '=' + attributes[attr] + ';';
            }
        }
        $private.url = url.replace(/;$/, '?');
        return $private.url;
    };
}
function DfpTracker() {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DfpSalesConversion');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-track');
    $private.typeValidator = new TypeValidator();
    $public.calls = [];
    $public.init = function (config, namTag, namespace) {
        $private.namTag = namTag;
        $private.namespace = namespace;
        $private.timeStampInit = (new Date()).getTime();
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        $private.url = new Url($private.namTag, $private.namespace, $private.timeStampInit);
        var url = $private.url.getUrl(config);
        $private.ready(function () {
            $private.makeDomElement(url);
        }, 10);
    };
    $private.makeDomElement = function (url) {
        if (!$private.typeValidator.isString(url)) {
            $private.logWarn('O parâmetro url deve ser do tipo string');
            return false;
        }
        if (!url.match(/\/\/pubads\.g\.doubleclick\.net\/activity;ord=\w+;cost=[0-9]+(\.[0-9]+)?;qty=[0-9]+;xsp=[0-9]+\?/)) {
            $private.logWarn('O parâmetro url é inválido');
            return false;
        }
        var pixel = document.createElement('img');
        pixel.setAttribute('src', url);
        $public.calls.push(pixel);
        $public.logger.log('Realizando o tracking da url: ' + url, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackSuccess('Execucoes Finalizadas');
    };
    $private.logWarn = function (message) {
        $public.logger.warn(message, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
    };
    $private.ready = function (fn, timeout) {
        if (!$private.typeValidator.isFunction(fn)) {
            return false;
        }
        var interval = window.setInterval(function() {
            if (document.readyState === 'complete') {
                window.clearInterval(interval);
                fn();
            }
        }, timeout);
    };
}
var __monitoracaoJsuol = "tm.jsuol.com.br/modules/dfp-conversion.js";
var nameSpace = new NameSpace('DfpSalesConversion');
var dfpTracker = new DfpTracker();
nameSpace.init = function (config, namTag) {
    return dfpTracker.init(config, namTag, 'DfpSalesConversion');
};
nameSpace.calls = dfpTracker.calls;
})(window, document);
(function (window, document, undefined){
'use strict';
function IframeCreator () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DNA');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dna');
    $private.src = '//tm.uol.com.br/uoldna.html';
    $public.init = function (namTag, namespace) {
        $private.namTag = namTag;
        $private.namespace = namespace;
        $private.timeStampInit = (new Date()).getTime();
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if($private.isIframeInPage()) {
            $private.trackError('duplicate-call', 'Já existe o iframe com o src do DNA na página');
            return;
        }
        $public.setIframe();
        $private.appendIframe();
    };
    $private.isIframeInPage = function() {
        if (document.querySelector('[src*="' + $private.src + '"]')) {
            return true;
        }
        return false;
    };
    $private.trackError = function (errorType, errorMessage) {
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $public.getIframe = function () {
        return $private.iframe;
    };
    $public.setIframe = function () {
        $private.iframe = document.createElement("iframe");
        $private.iframe.style.display = 'none';
        $private.iframe.setAttribute("src", 'https:' + $private.src);
    };
    $private.appendIframe = function () {
        if (!document.body) {
            setTimeout($private.appendIframe, 100);
        } else {
            document.body.appendChild($private.iframe);
            $public.logger.log('O iframe do UOL DNA foi incluído com sucesso', $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        }
    };
}
var nameSpace = new NameSpace('DNA');
nameSpace.init = function (config, namTag) {
    return (new IframeCreator()).init(namTag, 'DNA');
};
})(window, document);
(function (window, document, undefined){
'use strict';
function Injector () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.CodeInjector');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('code-injector');
    $public.codes = [];
    $private.typeValidator = new TypeValidator();
    $public.init = function (config, namTag, namespace) {
        $private.timeStampInit = (new Date()).getTime();
        $private.namTag = namTag;
        $private.namespace = namespace;
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if ($private.isValidConfig(config)) {
            $private.evalCode($private.trimScriptTag(config.codeInjector));
        }
    };
    $private.isValidConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(config.codeInjector)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.trimScriptTag = function (code) {
        var replaceScriptRegex = /^(\s+)?<script>(\s+)?|(\s+)?<\/script>(\s+)?$/gm;
        code = code.replace(replaceScriptRegex, '');
        $public.codes.push(code);
        return code;
    };
    $private.evalCode = function (code) {
        try {
            var dynMet = new Function(code);
            dynMet();
            $public.logger.log('Tag do CodeInjector executada com sucesso', $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        } catch (err) {
            $private.trackError('parse_error', 'O Tag Manager previniu um erro de execução de javascript. Favor verificar a implementação de sua tag');
        }
    };
}
var nameSpace = new NameSpace('CodeInjector');
nameSpace.init = function (config, namTag) {
    return (new Injector()).init(config, namTag, 'CodeInjector');
};
})(window, document);
(function (window, document, undefined){
'use strict';
function CallExternal () {
    var $private = {};
    var $public = this;
    $private.createTagScript = function (attributes) {
        var parentNode = document.getElementsByTagName('script')[0].parentNode;
        if (!attributes || !attributes.src) {
            return;
        }
        if(!$private.hasTagScript(attributes.src)){
            var tag = document.createElement('script');
            for ( var key in attributes) {
                var value = attributes[key];
                tag.setAttribute(key, value);
            }
            parentNode.appendChild(tag);
        }
    };
    $private.hasTagScript = function (src) {
        src = $private.removeProtocolFromUrl(src);
        if (!src) {
            return;
        }
        return document.querySelectorAll('script[src*="' + src + '"]').length > 0;
    };
    $private.removeProtocolFromUrl = function (url) {
        if (!url) {
            return;
        }
        return url.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };
    $public.makeDomElement = function () {
        var src = '//event.siga.uol.com.br/notify.js';
        var attributes = {};
        attributes.src = src;
        $private.createTagScript(attributes);
    };
    return $public;
}
function UniversalVariable (namTag, namespace, timeStampInit) {
    var $private = {};
    var $public = this;
    $private.namTag = namTag;
    $private.namespace = namespace;
    $private.timeStampInit = timeStampInit;
    $public.logger = new Logs();
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('TM-siga-conversion');
    $public.get = function () {
        if (!$private.isValidUvar(window.universal_variable)) {
            return;
        }
        return window.universal_variable;
    };
    $private.isValidUvar = function (uVar) {
        if (!$private.typeValidator.isObject(uVar)) {
            $private.trackError('uvar_invalida', 'Váriável universal inexistente ou mal implementada');
            return false;
        }
        if (!$private.getTransaction(uVar.transaction)) {
            return false;
        }
        if (!$private.hasOrderId(uVar.transaction) || !$private.hasTotal(uVar.transaction)) {
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.getTransaction = function (transaction) {
        if (!$private.typeValidator.isObject(transaction)) {
            $private.trackError('uvar_transaction_invalida', 'Atributo transaction da variável universal é inexistente ou está mal implementado');
            return false;
        }
        return true;
    };
    $private.hasOrderId = function (transaction) {
        if (typeof transaction.order_id !== 'string') {
            $private.trackError('uvar_transaction__order_id_invalida', 'Atributo order_id da variável universal é inexistente ou está mal implementado');
            return false;
        }
        return true;
    };
    $private.hasTotal = function (transaction) {
        var total = transaction.total;
        if ($private.typeValidator.isString(transaction.total)) {
            total = Number(total.replace(',', '.'));
        }
        if (!$private.typeValidator.isNumber(total) || isNaN(total)) {
            window.universal_variable.transaction.total = 1;
        }
        return true;
    };
}
function SigaObject (namTag, namespace, timeStampInit) {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.CliquesConversion');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('TM-siga-conversion');
    $private.typeValidator = new TypeValidator();
    $private.namTag = namTag;
    $private.namespace = namespace;
    $private.timeStampInit = timeStampInit;
    $private.universalVariable = new UniversalVariable($private.namTag, $private.namespace, $private.timeStampInit);
    $public.set = function (config) {
        if (!$private.isValidAdvertiserId(config)) {
            return;
        }
        $private.setId(config.advertiserId);
        if ($private.object.id) {
            $private.setLabel(config.label);
            $private.setTotal(1);
            $private.addUvarItemsToObject();
        }
    };
    $private.isValidAdvertiserId = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'Objeto de configuração está inválido');
            return false;
        }
        if (!$private.typeValidator.isString(config.advertiserId) && !$private.typeValidator.isNumber(config.advertiserId)) {
            $private.trackError('config_invalido', 'Código id do anúnciante inválido');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.setId = function (id) {
        if (id) {
            $private.object = $private.object || {};
            $private.object.id = id;
        }
    };
    $private.setTotal = function (total) {
        if (total) {
            $private.object = $private.object || {};
            $private.object.value = total;
        }
    };
    $private.setLabel = function (label) {
        if ($private.typeValidator.isString(label)) {
            $private.object = $private.object || {};
            $private.object.label = label;
        }
    };
    $private.setOrderId = function (order_id) {
        if (order_id) {
            $private.object = $private.object || {};
            $private.object.orderId = order_id;
        }
    };
    $private.addUvarItemsToObject = function () {
        var uvar = $private.universalVariable.get();
        if (!$private.typeValidator.isObject(uvar)) {
            return;
        }
        $private.setTotal(uvar.transaction.total);
        $private.setOrderId(uvar.transaction.order_id);
        if (!$private.object.label) {
            $private.setLabel(uvar.transaction.order_id);
        }
    };
    $public.get = function () {
        return $private.object;
    };
}
function Track () {
    var $private = {};
    var $public = this;
    var $static = $public.constructor.prototype;
    $private.typeValidator = new TypeValidator();
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.CliquesConversion');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('TM-siga-conversion');
    $public.calls = [];
    $public.init = function (config, namTag, namespace) {
        $private.timeStampInit = (new Date()).getTime();
        $private.namTag = namTag;
        $private.namespace = namespace;
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        $private.callExternal = new CallExternal();
        $private.sigaObject = new SigaObject($private.namTag, $private.namespace, $private.timeStampInit);
        if ($private.isValidConfig(config)) {
            $private.sigaObject.set(config);
            $private.makeConversion();
        }
    };
    $private.isValidConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'Configuração do Tag Manager está incorreta');
            return false;
        }
        if (!$private.typeValidator.isString(config.advertiserId) && !$private.typeValidator.isNumber(config.advertiserId)) {
            $private.trackError('config_invalido', 'O id de anunciante configurado no Tag Manager está incorreto');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.makeConversion = function () {
        var sigaObject = $private.sigaObject.get();
        if (!$private.typeValidator.isObject(sigaObject)) {
            $public.logger.warn('Não foi possível requisitar o objeto de parametrização. Verifique a implementação da variável universal');
            return;
        }
        $public.calls.push(sigaObject);
        $private.callExternal.makeDomElement();
        $private.sigaReady(sigaObject);
    };
    $private.sigaReady = function(obj) {
        var timeout = 3000;
        var interval = window.setInterval(function() {
            timeout -= 200;
            if ($private.hasSIGA()) {
                window.clearInterval(interval);
                window.SIGA.check(obj);
                $private.logSuccess(obj);
            }
            if (timeout <= 0) {
                window.clearInterval(interval);
                $private.trackError('siga_timeout' , 'Não foi possível encontrar o objeto SIGA após 3 segundos');
            }
        }, 200);
    };
    $private.logSuccess = function (obj) {
        var message = 'Conversão realizada com';
        var includeComma = false;
        for (var key in obj) {
            if (includeComma) {
                message += ',';
            }
            message += ' ' + key + ': ' + obj[key];
            includeComma = true;
        }
        $public.logger.log(message, $private.namTag, $private.namespace, ((new Date()).getTime() - $private.timeStampInit));
        $private.trackManager.trackSuccess('Execucoes Finalizadas');
    };
    $private.hasSIGA = function () {
        if (!$private.typeValidator.isObject(window.SIGA)) {
            $public.logger.debug('Objeto SIGA não existe ou está mal implementado');
            return false;
        }
        if (!$private.typeValidator.isFunction(window.SIGA.check)) {
            $public.logger.debug('O método de checkagem do SIGA não existe ou está mal implementado');
            return false;
        }
        return true;
    };
}
var __monitoracaoJsuol = "tm.jsuol.com.br/modules/cliques-conversion.js";
var nameSpace = new NameSpace('CliquesConversion');
nameSpace.init = function (config, namTag) {
    return (new Track()).init(config, namTag, 'CliquesConversion');
};
})(window, document);
(function (window, document, undefined){
'use strict';
function Uvar (namTag, nameSpace, timeStampInit) {
    var $private = {};
    var $public = this;
    $private.namTag = namTag;
    $private.nameSpace = nameSpace;
    $private.timeStampInit = timeStampInit;
    $private.typeValidator = new TypeValidator();
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DynadTrack');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dynad-track');
    $public.getType = function (uvarType) {
        return $private.uvarType;
    };
    $public.setType = function (uvarType) {
        if ($private.typeValidator.isString(uvarType)) {
            $private.uvarType = uvarType;
        }
    };
    $public.getUvar = function () {
        return $private.uvar;
    };
    $public.setUvar = function () {
        var uvar = window.universal_variable;
        if ($private.isValidUvar(uvar)) {
            $private.uvar = uvar;
        }
    };
    $private.isValidUvar = function (uvar) {
        if (!$private.typeValidator.isObject(uvar)) {
            $private.trackError('invalid_uvar_product_configuration', 'O atributo de produtos da variável universal não foi configurado corretamente');
            return false;
        }
        switch ($private.uvarType) {
            case 'product':
                return $private.isValidProduct(uvar);
            case 'category':
                return $private.isValidCategory(uvar);
            case 'basket':
                return $private.isValidBasket(uvar);
            case 'checkout':
                return $private.isValidBasket(uvar);
            case 'custom':
                return $public.isValidCustom(uvar);
            default:
                return false;
        }
    };
    $private.isValidProduct = function (uvar) {
        if (!$private.typeValidator.isObject(uvar.product)) {
            $private.trackError('invalid_uvar_product_configuration', 'O atributo de produtos da variável universal não foi configurado corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(uvar.product.sku_code)) {
            $private.trackError('invalid_uvar_product_sku_code_configuration', 'O atributo sku_code da variável universal não foi configurado corretamente');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, (new Date().getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.isValidCategory = function (uvar) {
        if (!$private.typeValidator.isObject(uvar.listing)) {
            $private.trackError('invalid_uvar_listing_config', 'O atributo de listagem da variável universal não foi configurado corretamente');
            return false;
        }
        if (!$private.isValidListItems(uvar.listing.items)) {
            return false;
        }
        return true;
    };
    $private.isValidListItems = function (items) {
        if (!$private.typeValidator.isArray(items)) {
            $private.trackError('invalid_uvar_listing_config_items', 'O atributo configs da listagem da variável universal não foi configurado corretamente');
            return false;
        }
        var length = items.length;
        if (length === 0) {
            $private.trackError('invalid_uvar_listing_config_items', 'O atributo configs da listagem da variável universal não foi configurado corretamente');
            return false;
        }
        for (var i = 0; i < length; i++) {
            if (!$private.isValidCategoryConfig(items[i])) {
                return false;
            }
        }
        return true;
    };
    $private.isValidCategoryConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('invalid_configuration', 'O atributo item de um dos produtos da variável universal não foi configurado corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(config.sku_code)) {
            $private.trackError('invalid_configuration_sku_code', 'O atributo sku_code de um dos produtos da variável universal não foi configurado corretamente');
            return false;
        }
        return true;
    };
    $private.isValidBasket = function (uvar) {
        if(!$private.typeValidator.isObject(uvar.basket)) {
            $private.trackError('invalid_uvar_basket', 'O atributo basket da variável universal não é um objeto válido');
            return false;
        }
        if(!$private.typeValidator.isArray(uvar.basket.line_items) || uvar.basket.line_items.length === 0) {
            $private.trackError('invalid_uvar_basket_line_items', 'O atributo line_items do atributo basket da variável universal não é uma lista válida');
            return false;
        }
        for (var i = 0; i < uvar.basket.line_items.length; i++) {
            if (!$private.validateBasketItem(uvar.basket.line_items[i])) {
                return false;
            }
        }
        return true;
    };
    $private.validateBasketItem = function (item) {
        if (!$private.typeValidator.isObject(item)) {
            $private.trackError('invalid_uvar_basket_config', 'O atributo item de um dos produtos da variável universal não foi configurado corretamente');
            return false;
        }
        if (!$private.typeValidator.isObject(item.product)) {
            $private.trackError('invalid_uvar_basket_product', 'O atributo de um dos produtos da variável universal não foi configurado corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(item.product.sku_code)) {
            $private.trackError('invalid_uvar_basket_product_sku_code', 'O atributo sku_code de um dos produtos da variável universal não foi configurado corretamente');
            return false;
        }
        return true;
    };
    $public.isValidCustom = function (uvar) {
        if (!$private.typeValidator.isObject(uvar.dynad)) {
            $private.trackError('invalid_uvar_dynad', 'O atributo dynad da variável universal não é um objeto válido');
            return false;
        }
        if(!$private.hasCustomParams(uvar.dynad.custom_params)) {
            return false;
        }
        return true;
    };
    $private.hasCustomParams = function (customParams) {
        if(!$private.typeValidator.isObject(customParams)) {
            $private.trackError('invalid_uvar_dynad_custom_params', 'O atributo custom_params do atributo dynad da variável universal não é um objeto válido');
            return false;
        }
        for (var param in customParams) {
            if (!$private.typeValidator.isString(customParams[param])) {
                $private.trackError('invalid_uvar_dynad_custom_params_param_value', 'O valor do parametro do atributo custom_params do atributo dynad da variável universal não é um objeto válido');
                return false;
            }
        }
        return true;
    };
}
function DomUtils (){
    var $private = {};
    var $public = this;
    $public.isReady = function(callback, timeout){
        if(!timeout && document.body) {
            callback();
        } else if(!timeout && document.readyState) {
            document.onreadystatechange = function(e) {
                if (document.readyState == "complete") {
                    callback();
                }
            };
        } else if(timeout) {
            setTimeout(function() {
                callback();
            }, timeout);
        }
    };
}
function SetCookie () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.url = '//tm.uol.com.br/dynad-track-cookies.html?';
    $public.init = function () {
        if (!$private.client) {
            return;
        }
        var container = $private.createContainer();
        if (container && container.childNodes.length > 0) {
            document.body.appendChild(container);
        }
    };
    $private.createContainer = function () {
        var container = document.createElement('div');
        container.setAttribute('id', 'dynad-track');
        if (!$private.client.getId()) {
            return;
        }
        var cookies = $private.createCookies($private.client.getId(), $private.getDateDifference($private.client.getExpiration()));
        var biddingCookies = $public.createBidding();
        cookies = cookies.concat(biddingCookies);
        for (var i = 0, length = cookies.length; i < length; i++) {
            container.appendChild(cookies[i]);
        }
        return container;
    };
    $private.createCookies = function (clientId, expires) {
        var cookies = [];
        var iframe = $private.createIframe($private.buildUrl('DEretargeting', 'DEretargetingExp', clientId, expires, '1'));
        if (iframe) {
            cookies.push(iframe);
        }
        iframe = $private.createIframe($private.buildUrl('dynad_rt', 'dynad_rt_exp', clientId, expires));
        if (iframe) {
            cookies.push(iframe);
        }
        return cookies;
    };
    $private.buildUrl = function (name, expname, clientId, expires, domain) {
        var queryString = $private.getQueryString(name, expname, clientId, expires, domain);
        if (queryString) {
            return $private.url + queryString;
        }
    };
    $private.getQueryString = function (name, expname, clientId, expires, domain) {
        var queryString = [];
        var params = $private.createQueryStringObject(name, expname, clientId, expires, domain);
        for (var key in params) {
            queryString.push(key + '=' + params[key]);
        }
        return queryString.join('&');
    };
    $private.createQueryStringObject = function (name, expname, clientId, expires, domain) {
        var queryStringObject = {};
        queryStringObject.name = name;
        queryStringObject.expname = expname;
        queryStringObject.client = clientId;
        if ($private.typeValidator.isDefined(expires)) {
            var expirationParam = $private.getExpirationParam(parseInt(expires));
            if (!expirationParam) {
                return;
            }
            queryStringObject[expirationParam.key] = expirationParam.value;
        }
        if (domain) {
            queryStringObject.expdomain = domain;
        }
        return queryStringObject;
    };
    $private.getExpirationParam = function (expires) {
        var param = {};
        if (!$private.typeValidator.isInt(expires)) {
            return;
        }
        if (expires > 0) {
            param.key = 'expires';
            param.value = expires;
        } else {
            param.key = 'delete';
            param.value = 'true';
        }
        return param;
    };
    $public.createBidding = function () {
        var biddingCookies = [];
        var bidding= $private.client.getBidding();
        for (var key in bidding) {
            var cookies = $private.createCookies(key, $private.getDateDifference(bidding[key]));
            if (cookies) {
                biddingCookies = biddingCookies.concat(cookies);
            }
        }
        return biddingCookies;
    };
    $private.getDateDifference = function (expirationDate) {
        var now = (new Date());
        return Math.floor((expirationDate.getTime() - now.getTime())/(1000*60*60*24));
    };
    $private.createIframe = function (src) {
        if (!src) {
            return;
        }
        var iframe = document.createElement('iframe');
        iframe.setAttribute('src', src);
        iframe.setAttribute('width', 0);
        iframe.setAttribute('height', 0);
        iframe.style.display = 'none';
        return iframe;
    };
    $private.appendIframes = function (container, iframes) {
        if (iframes.length === 0) {
            return;
        }
        for (var i = 0, length = iframes.length; i < length; i++) {
            container.appendChild(iframes[i]);
        }
        return container;
    };
    $public.getClient = function (client) {
        return $private.client;
    };
    $public.setClient = function (client) {
        if (client) {
            $private.client = client;
        }
    };
}
function ExpirationController (namTag, nameSpace, timeStampInit) {
    var $private = {};
    var $public = this;
    $private.namTag = namTag;
    $private.nameSpace = nameSpace;
    $private.timeStampInit = timeStampInit;
    $private.typeValidator = new TypeValidator();
    $private.remoteStorage = new RemoteStorage();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dynad-track');
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DynadTrack');
    $private.clients = [];
    $public.getClient = function () {
        return $private.client;
    };
    $public.setClient = function (client) {
        if (!client) {
            return;
        }
        if (!client.getId()) {
            return;
        }
        $private.client = {
            'id': client.getId(), 
            'expiration': client.getExpiration() 
        };
        if (client.getBidding()) {
            $private.client.bidding = client.getBidding();
        }
        $private.trackManager.trackCustom('Tracking', 'client', $private.client.id);
    };
    $private.isValidClient = function (id, bidding) {
        if (!id) {
            return false;
        }
        if (!bidding) {
            return false;
        }
        return true;
    };
    $public.getClients = function () {
        return $private.clients;
    };
    $public.setClients = function (clients) {
        try {
            $private.clients = JSON.parse(clients);
        } catch (e) {
            $public.logger.warn('Erro na leitura da lista de clientes', $private.namTag, $private.nameSpace, ((new Date()).getTime() - $private.timeStampInit));
        }
        $private.clients = $private.clients || [];
        $public.updateExpiredClients();
    };
    $public.updateExpiredClients = function () {
        for (var i = 0; i < $private.clients.length; i++) {
            var client = $private.clients[i];
            if ($private.isvalidStoredClient(client) && !$private.shouldExpire(client.expiration)) {
                $private.updateExpiredBidding(client);
                continue;
            }
            $private.clients.splice(i, 1);
            $public.logger.debug('Removendo o cliente de id ' + client.id + ' pois este expirou');
            $private.trackManager.trackCustom('Expiration', 'client', client.id);
            i--;
        }
    };
    $private.updateExpiredBidding = function (client) {
        if (!client.bidding) {
            return client;
        }
        for (var key in client.bidding) {
            if ($private.shouldExpire(client.bidding[key])) {
                delete client.bidding[key];
            }
        }
        if ($private.objectIsEmpty(client.bidding)) {
            delete client.bidding;
        }
        return client;
    };
    $private.objectIsEmpty = function (obj) {
        for (var key in obj) {
            return false;
        }
        return true;
    };
    $private.isvalidStoredClient = function (client) {
        if (!$private.typeValidator.isObject(client)) {
            return false;
        }
        return $private.isValidClient(client.id, client.bidding);
    };
    $private.getClientDate = function (date) {
        var time;
        try {
            time = (new Date(date));
        } catch (e) {}
        return time;
    };
    $private.shouldExpire = function (expiration) {
        var today = new Date();
        if ($private.getClientDate(expiration).getTime() > today.getTime()) {
            return false;
        }
        return true;
    };
    $public.updateClient = function () {
        $private.remoteStorage.get('dynad_rt', $public.updateClientCallback);
    };
    $public.updateClientCallback = function (key, value) {
        $public.setClients(value);
        $private.addClient();
        $private.remoteStorage.set('dynad_rt', JSON.stringify($private.clients), $private.setClientsCallback);
    };
    $private.setClientsCallback = function (key, value) {
        $public.logger.debug('Armazenando os clientes: ' + value + ' no atributo ' + key + ' do localStorage');
    };
    $private.addClient = function () {
        if (!$private.client) {
            return;
        }
        var index = $private.getClientIndex($private.client.id);
        if (index < 0) {
            $private.clients.push($private.client);
            $public.logger.debug('Adicionando o cliente de id ' + $private.client.id);
        } else {
            $private.clients[index] = $private.client;
            $public.logger.debug('Adicionando o cliente de id ' + $private.client.id);
        }
        $public.logger.log('Adicionando o cliente de id ' + $private.client.id, $private.namTag, $private.nameSpace, (new Date().getTime() - $private.timeStampInit));
        for (var key in $private.client.bidding) {
            $public.logger.log('Adicionando o Bidding ' + key + ' com expiração para ' + $private.getDateDifference($private.client.bidding[key]) + ' dias.', $private.namTag, $private.nameSpace, (new Date().getTime() - $private.timeStampInit));
        }
    };
    $private.getClientIndex = function (id) {
        var index = $private.clients.length;
        while (index) {
            index--;
            if ($private.clients[index].id === id) {
                return index;
            }
        }
        return -1;
    };
    $private.getDateDifference = function (expirationDate) {
        var now = (new Date());
        return Math.floor((expirationDate.getTime() - now.getTime())/(1000*60*60*24));
    };
}
function Client () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.DEFAULT_EXPIRATION = 60;
    $public.getId = function () {
        return $private.id;
    };
    $public.setId = function (id) {
        if ($private.typeValidator.isString(id)) {
            $private.id = id;
        }
    };
    $public.getBiddingValues = function () {
        return $private.biddingValues;
    };
    $public.setBiddingValues = function (biddingValues) {
        if ($private.isValidBidding(biddingValues)) {
            $private.biddingValues = $private.createBiddingArray(biddingValues);
        }
    };
    $public.getBiddingExpirations = function () {
        return $private.biddingExpirations;
    };
    $public.setBiddingExpirations = function (biddingExpirations) {
        if (!$private.biddingValues || !$private.isValidBidding(biddingExpirations)) {
            return;
        }
        biddingExpirations = $private.createBiddingArray(biddingExpirations);
        for (var key in biddingExpirations) {
            biddingExpirations[key] = $private.getIncreasedDate(parseInt(biddingExpirations[key]));
        }
        if ($private.biddingValues.length === biddingExpirations.length) {
            $private.biddingExpirations = biddingExpirations;
        }
    };
    $public.getBidding = function () {
        return $private.bidding;
    };
    $public.setBidding = function () {
        if (!$private.expiration) {
            $public.setExpiration();
        }
        if ($private.biddingValues) {
            $private.bidding = $private.createBiddingObject();
        }
    };
    $public.getExpiration = function () {
        if (!$private.expiration) {
            $public.setExpiration();
        }
        return $private.expiration;
    };
    $public.setExpiration = function (expiration) {
        if (isNaN(expiration)) {
            $private.expiration = $private.getIncreasedDate($private.DEFAULT_EXPIRATION);
            return;
        }
        $private.expiration = $private.getIncreasedDate(parseInt(expiration));
    };
    $public.getTrackingDate = function () {
        return $private.trackingDate;
    };
    $public.setTrackingDate = function () {
        $private.trackingDate = new Date();
    };
    $private.isValidBidding = function (biddingValues) {
        if (!$private.typeValidator.isString(biddingValues)) {
            return false;
        }
        var areValuesInvalid = /((\w+)(\s)?(;)?)+/gm;
        if (!areValuesInvalid.test(biddingValues)) {
            return false;
        }
        return true;
    };
    $private.createBiddingArray = function (biddingArray) {
        if (biddingArray.charAt(0) === ';' || biddingArray.charAt(biddingArray.length -1) === ';') {
            biddingArray = biddingArray.replace(/^;|;$/, '');
        }
        return biddingArray.replace(/\s/g, '').split(';');
    };
    $private.createBiddingObject = function () {
        var biddingObject = {};
        if ($private.biddingExpirations && $private.biddingValues.length !== $private.biddingExpirations.length) {
            return;
        }
        var today = new Date();
        for (var i = 0, length = $private.biddingValues.length; i < length; i++) {
            var value;
            if (today > $private.expiration) {
                biddingObject[$private.biddingValues[i]] = $private.expiration;
                continue;
            }
            if (!$private.biddingExpirations) {
                biddingObject[$private.biddingValues[i]] = $private.expiration;
                continue;
            }
            biddingObject[$private.biddingValues[i]] = $private.biddingExpirations[i];
        }
        return biddingObject;
    };
    $private.getIncreasedDate = function (value) {
        var date = new Date();
        date.setMilliseconds(59);
        date.setSeconds(59);
        date.setMinutes(59);
        date.setHours(23);
        if (value === 0) {
            date.setDate(date.getDate() - 1);
        }
        date.setDate(date.getDate() + value);
        return date;
    };
}
function TrackingObject (namTag, nameSpace, timeStampInit) {
    var $private = {};
    var $public = this;
    $private.namTag = namTag;
    $private.nameSpace = nameSpace;
    $private.timeStampInit = timeStampInit;
    $private.typeValidator = new TypeValidator();
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DynadTrack');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dynad-track');
    $private.uvar = new Uvar($private.namTag, $private.nameSpace, $private.timeStampInit);
    $private.types = ['product', 'category', 'basket', 'checkout', 'custom'];
    $public.getType = function (trackingType) {
        return $private.trackingType;
    };
    $public.setType = function (trackingType) {
        if ($private.isValidUvarType(trackingType)) {
            $private.trackingType = trackingType;
        }
    };
    $private.isValidUvarType = function (uvarType) {
        if (!$private.typeValidator.isString(uvarType)) {
            return false;
        }
        if ($private.types.indexOf(uvarType) < 0) {
            return false;
        }
        return true;
    };
    $public.getTrackingObject = function () {
        return $private.trackingObject;
    };
    $public.setTrackingObject = function (clientId) {
        $private.uvar.setType($private.trackingType);
        $private.uvar.setUvar();
        if (!$private.uvar.getUvar()) {
            return;
        }
        switch ($private.trackingType) {
            case 'custom': 
                $private.createCustom(); 
                break;
            case 'product': 
                $private.createProduct();
                break;
            case 'category': 
                $private.createList($private.uvar.getUvar().listing.items);
                break;
            case 'basket': 
                $private.createBasket();
                break;
            case 'checkout': 
                $private.createBasket();
                break;
        }
        $private.addItemsToTrackingObject({
            'page' : $private.trackingType,
            'customer' : clientId,
            'ord' : (new Date()).getTime()
        });
    };
    $private.createCustom = function () {
        $private.addItemsToTrackingObject($private.getCustomUvarParams(), 'dynad-');
    };
    $private.getCustomUvarParams = function () {
        $private.uvar.setType('custom');
        $private.uvar.setUvar();
        if ($private.uvar.getUvar() && $private.uvar.getUvar().dynad) {
            return $private.uvar.getUvar().dynad.custom_params;
        }
    };
    $private.createProduct = function () {
        $private.addItemsToTrackingObject({
            'sku': $private.uvar.getUvar().product.sku_code
        });
        $private.createCustom();
    };
    $private.addItemsToTrackingObject = function (items, prefix) {
        prefix = prefix || '';
        if (!$private.typeValidator.isObject(items)) {
            return;
        }
        for (var key in items) {
            $private.trackingObject = $private.trackingObject || {};
            $private.trackingObject[prefix + key] = items[key];
        }
    };
    $private.createList = function (list) {
        var skus = [];
        for (var i = 0, length = list.length; i < length; i++) {
            skus.push(list[i].sku_code);
        }
        $private.addItemsToTrackingObject({'sku': skus.join(';')});
        $private.createCustom();
    };
    $private.createBasket = function () {
        var list = [];
        var items = $private.uvar.getUvar().basket.line_items;
        for (var key in items) {
           list.push(items[key].product); 
        }
        $private.createList(list);
    };
}
function DynadTrack () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.scriptUtils = new ScriptUtils();
    $public.logger = new Logs();
    $private.trackUrl = '//iruol.dynad.net/wildcard/tracking/?';
    $public.__constructor = (function () {
        $public.logger.setPrefix('UOLPD.TagManager.DynadTrack');
        $private.trackManager.setModuleName('dynad-track');
    })();
    $public.init = function (config, namTag, nameSpace) {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if (!$private.isValidConfig(config)) {
            return;
        }
        $private.configureLogger(namTag, nameSpace);
        $private.configureClient(config.clientId, config.biddingValues, config.biddingExpirations);
        $private.makeTrack(config.type, config.clientId);
    };
    $public.setCookie = function (expires) {
        if (!$private.client) {
            return;
        }
        $private.client.setExpiration(expires);
        $private.client.setBidding();
        $private.setCookie = new SetCookie($private.namTag, $private.namespace, $private.timeStampInit);
        $private.setCookie.setClient($private.client);
        $private.setCookie.init(expires);
        $private.updateLocalStorage();
    };
    $private.isValidConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('Um item da configuração no Tag Manager está incorreto', 'invalid_config');
            return false;
        }
        if (!$private.typeValidator.isString(config.type)) {
            $private.trackError('O atributo type de um item da configuração no Tag Manager está incorreto', 'invalid_config_type');
            return false;
        }
        if (!$private.typeValidator.isString(config.clientId)) {
            $private.trackError('O atributo clientId de um item da configuração no Tag Manager está incorreto', 'invalid_config_clientId');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorMessage, errorType) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logger.warn(errorMessage, $private.namTag, $private.namespace, (new Date().getTime() - $private.timeStampInit));
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.configureLogger = function (namTag, nameSpace) {
        $private.namTag = namTag;
        $private.nameSpace = nameSpace;
        $private.timeStampInit = (new Date()).getTime();
    };
    $private.configureClient = function (clientId, biddingValues, biddingExpirations) {
        $private.client = new Client($private.namTag, $private.namespace, $private.timeStampInit);
        $private.client.setId(clientId);
        $private.client.setBiddingValues(biddingValues);
        $private.client.setBiddingExpirations(biddingExpirations);
        $private.client.setTrackingDate();
    };
    $private.makeTrack = function (trackingType, clientId) {
        $private.trackingObject = new TrackingObject($private.namTag, $private.nameSpace, $private.timeStampInit);
        $private.trackingObject.setType(trackingType);
        $private.trackingObject.setTrackingObject(clientId);
        var script = $private.scriptUtils.createScript($private.buildUrl());
        if (script) {
            $private.scriptUtils.appendTag(script);
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
            $public.logger.log('Realizou o tracking da URL ' + script.src + ' com sucesso', $private.namTag, $private.namespace, (new Date().getTime() - $private.timeStampInit));
        }
    };
    $private.buildUrl = function () {
        if ($private.trackingObject.getTrackingObject()) {
            return $private.trackUrl + $private.buildQueryString($private.trackingObject.getTrackingObject());
        }
    };
    $private.buildQueryString = function (obj) {
        if (!$private.typeValidator.isObject(obj)) {
            return;
        }
        var query = [];
        for (var key in obj) {
            query.push(key + '=' + obj[key]);
        }
        return query.join('&');
    };
    $private.updateLocalStorage = function () {
        $private.expirationController = new ExpirationController($private.namTag, $private.nameSpace, $private.timeStampInit);
        $private.expirationController.setClient($private.client);
        $private.expirationController.updateClient();
    };
}
var __monitoracaoJsuol = "tm.jsuol.com.br/modules/dynad-track.js";
var nameSpace = new NameSpace('DynadTrack');
var dynadTrack = new DynadTrack();
var uVar = window.universal_variable ? window.universal_variable : null;
nameSpace.init = function (config, namTag) {
    return dynadTrack.init(config, namTag, 'DynadTrack');
};
nameSpace.setCookie = dynadTrack.setCookie;
})(window, document);
var defaultTags = [
    {
        "name": "UOL DNA",
        "module" : "DNA",
        "rules" : {
            "enable" : ["/\\.uol\\.com\\.br/.test(window.location.href)"],
            "disable" : []
        },
        "events": ["onload"]
    },
    {
        "name": "Behavioural Targeting",
        "module" : "CodeInjector",
        "rules" : {
            "enable" : [ "document.location.href.match(/.*/)" ],
            "disable" : [ ]
        },
        "events" : [ "onload" ],
        "config" : {
            "codeInjector" : "(function() {\n var script = document.createElement('script');\n var src = 'http://tracker.bt.uol.com.br/partner?source=tagmanager';\n if (window.location.protocol === 'https:') {\n src = 'https://tracker.bt.uol.com.br/track?source=tagmanager';\n }\n \n script.setAttribute('src', src);\n document.head.appendChild(script);\n})()"
        }
    },
    {
        "name" : "Purga de clients de RT expirados",
        "module" : "CodeInjector",
        "rules" : {
            "enable" : [ "document.location.href.match(/.*/)" ],
            "disable" : [ ]
        },
        "events" : [ "autostart" ],
        "config" : {
            "codeInjector" : "(function () {\n    var createIframe = function () {\n        var iframe = document.createElement('iframe');\n        iframe.style.width = '0';\n        iframe.style.heigth = '0';\n        iframe.style.display = 'none';\n        iframe.style.opacity = '0';\n        iframe.style.border = '0';\n        iframe.setAttribute('src', '//tm.uol.com.br/purge-clients.html');\n        document.head.appendChild(iframe);\n    }\n\n    createIframe()\n})();"
        }
    }
];
function CookieUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.cookieValidator = function (name, value) {
        if ($public.getItem(name).length > 0) {
            return false;
        }
        if (!$private.typeValidator.isString(name)) {
            return false;
        }
        if (!$private.typeValidator.isString(value)) {
            return false;
        }
        return true;
    };
    $public.getItem = function (name) {
        if (!$private.typeValidator.isString(name)) {
            return '';
        }
        var cookie = $private.getCookies();
        var startIndex = cookie.indexOf(name + '=');
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = cookie.indexOf('=', startIndex) + 1;
        var endIndex = cookie.indexOf(';', middleIndex);
        if (endIndex === -1) {
            endIndex = cookie.length;
        }
        return unescape(cookie.substring(middleIndex, endIndex));
    };
    $private.getCookies = function () {
        return document.cookie;
    };
}
function VersionManager () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $private.validator = new TypeValidator();
    $private.stringUtils = new StringUtils();
    $private.scriptUtils = new ScriptUtils();
    $private.tmVersionToken = 'uoltm_version';
    $public.setSpecificVersion = function () {
        if (!$public.specificVersionWasRequested()) {
            return;
        }
        $private.scriptUtils.createSyncScript($public.getSpecificVersionURL());
    };
    $public.specificVersionWasRequested = function () {
        var requestedVersion = $private.getRequestedVersion();
        var versionRegex = /^\d+$/gm;
        var isValidVersion = versionRegex.test(requestedVersion);
        if (!requestedVersion || !isValidVersion) {
            return false;
        }
        if ($private.hasVersionScriptInPage()) {
            return false;
        }
        return true;
    };
    $public.getSearch = function () {
        return window.location.search;
    };
    $public.getScriptUrl = function () {
        var __monitoracaoJsuol = 'tm.jsuol.com.br/ch/62/ch6251.js';
        return __monitoracaoJsuol;
    };
    $public.getSpecificVersionURL = function () {
        var codRepository = $private.getCodRepository();
        var versionURL = $private.buildVersionURL(codRepository);
        return versionURL;
    };
    $private.getRequestedVersion = function () {
        var requestedVersion = $private.stringUtils.getValueFromKeyInString($public.getSearch(), $private.tmVersionToken, '&');
        return requestedVersion;
    };
    $private.hasVersionScriptInPage = function () {
        var versionURL = $public.getSpecificVersionURL();
        var scripts = document.querySelectorAll('script[src*="'+ versionURL +'"]');
        if (!scripts || !scripts.length) {
            return false;
        }
        return true;
    };
    $private.buildVersionURL = function (codRepository) {
        var mainPath = '//tm.jsuol.com.br/';
        var firstDirLevel = codRepository.substring(0, 2) + '/';
        var secondDirLevel = codRepository.substring(2, 4) + '/';
        var fullPath = mainPath + firstDirLevel + secondDirLevel + codRepository;
        var requestedVersion = $private.getRequestedVersion();
        if (requestedVersion != '0') {
            fullPath += '-' + $private.getRequestedVersion() + '.js';
        } else {
            fullPath += '.js';
        }
        return fullPath;
    };
    $private.getCodRepository = function () {
        var baseURLRegex = /[\w\d]{6}\.js$/gm;
        var codRepository = $public.getScriptUrl().match(baseURLRegex, '')[0];
        codRepository = codRepository.replace('.js', '');
        return codRepository;
    };
}
function TagController () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.dataLayer = new DataLayer();
    $private.executedTags = [];
    $public.addEvents = function (tag) {
        if (!$private.isTagValid(tag)) {
            return;
        }
        if (tag.events.length === 0) {
            tag.events.push('autostart');
        }
        for (var i = 0, length = tag.events.length; i < length; i++) {
            if ($private.typeValidator.isString(tag.events[i])) {
                $private.addEvent(tag.events[i], tag);
            }
        }
    };
    $private.isTagValid = function (tag) {
        if (!$private.typeValidator.isObject(tag)) {
            return false;
        }
        if (!$private.typeValidator.isString(tag.module)) {
            return false;
        }
        if (!$private.typeValidator.isObject(tag.rules)) {
            return false;
        }
        if (!$private.typeValidator.isArray(tag.rules.enable)) {
            return false;
        }
        if (!$private.typeValidator.isArray(tag.rules.disable)) {
            return false;
        }
        if (!$private.typeValidator.isArray(tag.events)) {
            return false;
        }
        if (tag.config && !$private.typeValidator.isObject(tag.config)) {
            return false;
        }
        return true;
    };
    $public.getEvents = function (eventName) {
        return $private.events;
    };
    $private.addEvent = function (eventName, tag) {
        $private.events = $private.events || {};
        var events = $private.events[eventName] || [];
        events.push($private.cloneTag(tag, eventName));
        $private.events[eventName] = events;
    };
    $private.cloneTag = function (tag, eventName) {
        var result = {};
        for (var key in tag) {
            result[key] = tag[key];
        }
        if ($private.typeValidator.isString(tag.module)) {
            result.moduleName = tag.module;
            result.module = $public.getModuleByName(tag.module);
            result.event = [eventName];
        }
        return result;
    };
    $public.getModuleByName = function (moduleName) {
        var module;
        if (!$private.typeValidator.isString(moduleName)) {
            return;
        }
        module = window.UOLPD.TagManager[moduleName];
        if (!$private.typeValidator.isDefined(module)) {
            return;
        }
        module.nameSpace = moduleName;
        if ($private.typeValidator.isFunction(module.init)) {
            return module;
        }
    };
    $public.triggerEvents = function (eventName) {
        var currentEvent = $public.getCurrentEvent(eventName);
        if (!currentEvent) {
            return;
        }
        for (var i = 0, length = currentEvent.length; i < length; i++) {
            try {
                $private.activateTag(currentEvent[i], eventName);
            } catch (e) {}
        }
    };
    $public.getCurrentEvent = function (eventName) {
        if (!$private.events) {
            return;
        }
        var currentEvent = $private.events[eventName];
        if ($private.typeValidator.isArray(currentEvent)) {
            return currentEvent;
        }
    };
    $private.activateTag = function (tag, eventName) {
        if ($private.isTagEnabled(tag)) {
            tag.module.init(tag.config, tag.name);
            $private.executedTags.push(tag);
            $private.dataLayer.setDataLayer(tag.module.nameSpace);
        }
    };
    $private.isTagEnabled = function (tag) {
        if ($private.checkRules(tag.rules.disable)) {
            return false;
        }
        if ($private.checkRules(tag.rules.enable)) {
            return true;
        }
    };
    $private.checkRules = function (rules) {
        try {
            if (eval(rules.join(' || '))) {
                return true;
            }
        } catch (e) {
            return false;
        }
    };
    $public.getExecutedTags = function () {
        return $private.executedTags;
    };
}
function DataLayer () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.typeValidator = new TypeValidator();
    $public.createDataLayer = function () {
        var namespace = window.UOLPD;
        if (!$private.typeValidator.isObject(namespace)) {
            window.UOLPD = {};
        }
        var dataLayer = window.UOLPD.dataLayer;
        if (!$private.typeValidator.isObject(dataLayer)) {
            window.UOLPD.dataLayer = {};
        }
        $private.addReferer();
        $private.addFocusStatus();
    };
    $private.addReferer = function () {
        window.UOLPD.dataLayer.referer = window.UOLPD.dataLayer.referer || window.location.href;
    };
    $private.addFocusStatus = function () {
        setInterval(function () {
            window.UOLPD.dataLayer.hasFocus = document.hasFocus().toString();
        }, 750);
    };
    $public.setDataLayer = function (module) {
        var dataLayer = $private.hasModuleDataLayer(module);
        if (!$private.typeValidator.isObject(dataLayer)) {
            return;
        }
        for (var key in dataLayer) {
            window.UOLPD.dataLayer[key] = dataLayer[key];
            $public.logs.debug('Foi adicionado "'+ key + ' : ' + dataLayer[key] + '" à window.UOLPD.dataLayer pelo módulo: ' + module);
        }
    };
    $private.hasModuleDataLayer = function (module) {
        var data = {};
        try {
            data = JSON.parse(window.localStorage.getItem('uolDataLayer'));
        } catch (e) {}
        if (!$private.typeValidator.isObject(data)) {
            return;
        }
        if (!$private.typeValidator.isString(module)) {
            return;
        }
        var dataLayer = data[module];
        if (!$private.typeValidator.isObject(dataLayer)) {
            return;
        }
        return dataLayer;
    };
}
function TMConsole (codRepository, version) {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager');
    $private.typeValidator = new TypeValidator();
    $private.cookieUtils = new CookieUtils();
    $private.cookieName = 'uoltm_version';
    $public.__constructor = function (codRepository, version) {
        if (!$private.isValidConstructorArguments(codRepository, version)) {
            return;
        }
        if ($private.hasOtherRepositoryCookie(codRepository)) {
            return;
        }
        $private.setCookie(codRepository);
        $private.appendOnDocComplete(codRepository, version);
        return $public;
    };
    $private.appendOnDocComplete = function (codRepository, version) {
        $private.container = $private.createMainContainer(codRepository, version);
        if (document.readyState === "complete") {
            document.body.appendChild($private.container);
            return;
        }
        var method = (document.addEventListener) ? "readystatechange" : "onreadystatechange";
        var methodListener = (document.addEventListener) ? "addEventListener" : "attachEvent";
        document[methodListener](method, function() {
            if (document.readyState === "complete") {
                document.body.appendChild($private.container);
            }
        });
    };
    $private.isValidConstructorArguments = function (codRepository, version) {
        if (!$private.typeValidator.isString(codRepository)) {
            $public.logger.error('O parâmetro fornecido como código do repositório é inválido');
            return false;
        }
        if (!$private.typeValidator.isNumericString(version)) {
            $public.logger.error('O parâmetro fornecido como versão do repositório é inválido');
            return false;
        }
        return true;
    };
    $private.hasOtherRepositoryCookie = function (codRepository) {
        var currentCookie = $private.cookieUtils.getItem($private.cookieName);
        if (currentCookie.length > 0 && currentCookie !== codRepository) {
            return true;
        }
        return false;
    };
    $private.setCookie = function (codRepository) {
        if (!$private.cookieUtils.cookieValidator($private.cookieName, codRepository)) {
            return;
        }
        var date = new Date();
        date.setTime(date.getTime() + (15 * 60000));
        document.cookie = $private.cookieName + "=" + codRepository + " ; expires=" + date.toGMTString() + " ; path=/";
    };
    $private.createMainContainer = function (codRepository, version) {
        var mainContainer = document.createElement('div');
        mainContainer.setAttribute('id', codRepository);
        mainContainer.style.backgroundColor = '#fafafa';
        mainContainer.style.bottom = 0;
        mainContainer.style.display = 'block';
        mainContainer.style.height = Math.round(window.innerHeight / 3) + 'px';
        mainContainer.style.position = 'fixed';
        mainContainer.style.width = '100%';
        mainContainer.style.fontFamily = 'Verdana, Sans-serif';
        mainContainer.style.zIndex = '99999999999';
        mainContainer.appendChild($private.createHeaderContainer(codRepository, version));
        mainContainer.appendChild($private.createLogContainer());
        return mainContainer;
    };
    $private.createHeaderContainer = function (codRepository, version) {
        var headerContainer = document.createElement('div');
        headerContainer.style.width = '100%';
        headerContainer.style.borderTop = 'solid 0.5px #aaaaaa';
        headerContainer.style.color = '#757575';
        headerContainer.style.position = 'relative';
        headerContainer.appendChild($private.createLogo());
        headerContainer.appendChild($private.createRepositoryInfo(codRepository, version));
        headerContainer.appendChild($private.createCloseButton(codRepository));
        headerContainer.appendChild($private.createControlButton(codRepository));
        return headerContainer;
    };
    $private.createLogo = function () {
        var logo = $private.createHeaderSubDivision();
        logo.appendChild($private.createLogoAnchor());
        return logo;
    };
    $private.createHeaderSubDivision = function () {
        var headerSubDivision = document.createElement('div');
        headerSubDivision.style.display = 'inline-block';
        headerSubDivision.style.padding = '5px';
        return headerSubDivision;
    };
    $private.createLogoAnchor = function () {
        var anchor = document.createElement('a');
        anchor.setAttribute('href', '//tagmanager.uol.com.br');
        anchor.style.backgroundImage = 'url(http://imguol.com/tag-manager/icons-sprite.png)';
        anchor.style.backgroundPosition = '0 0';
        anchor.style.backgroundRepeat = 'no-repeat';
        anchor.style.width = '155px';
        anchor.style.height = '38px';
        anchor.style.display = 'block';
        anchor.style.textIndent = '100%';
        anchor.style.whiteSpace = 'nowrap';
        anchor.style.overflow = 'hidden';
        anchor.setAttribute('title', 'Logo do console do UOL Tag Manager');
        var h1 = document.createElement('h1');
        h1.innerHTML = 'UOL Tag Manager';
        anchor.appendChild(h1);
        return anchor;
    };
    $private.createRepositoryInfo = function (codRepository, version) {
        var repositoryInfo = $private.createHeaderSubDivision();
        var repositoryName = $private.createRepositoryName('Bonprix');
        codRepository = $private.createHeaderSubText(codRepository);
        version = $private.createHeaderSubText('Versão ' + version);
        version.style.marginLeft = '28px';
        repositoryInfo.innerHTML = repositoryName.outerHTML + '-' + codRepository.outerHTML + version.outerHTML;
        repositoryInfo.style.position = 'absolute';
        repositoryInfo.style.left = Math.round(window.innerWidth/3) + 'px';
        repositoryInfo.style.marginRight = '40px';
        return repositoryInfo;
    };
    $private.createRepositoryName = function (name) {
        var h2 = document.createElement('h2');
        h2.style.margin = '0px 5px 0px 0px';
        h2.style.padding = '0px';
        h2.style.fontWeight = 'lighter';
        h2.style.display = 'inline';
        h2.style.color = '#757575';
        h2.style.fontSize = '27px';
        h2.innerHTML = name;
        return h2;
    };
    $private.createHeaderSubText = function (innerText) {
        var h4 = document.createElement('h4');
        h4.style.margin = '0px 0px 0px 5px';
        h4.style.padding = '0px';
        h4.style.display = 'inline';
        h4.style.fontWeight = 'lighter';
        h4.style.color = '#757575';
        h4.style.fontSize = '17px';
        h4.innerHTML = innerText;
        return h4;
    };
    $private.createLogContainer = function () {
        var logContainer = document.createElement('div');
        logContainer.style.overflowX = 'hidden';
        logContainer.style.overflowY = 'auto';
        logContainer.style.height = (Math.round(window.innerHeight/3) - 49) + 'px';
        logContainer.setAttribute('class', 'logContainer');
        var table = document.createElement('table');
        table.style.width = '100%';
        table.style.textAlign = 'left';
        table.style.borderCollapse = 'collapse';
        table.style.marginTop = '15px';
        table.appendChild($private.createThead());
        table.appendChild($private.createTbody());
        logContainer.appendChild(table);
        return logContainer;
    };
    $private.createThead = function () {
        var thead = document.createElement('thead');
        thead.style.backgroundColor = '#FAFAFA';
        var headers = ['', 'Nome da Tag', 'Módulo', 'Mensagem', 'Tempo de execução'];
        for (var i = 0, length = headers.length; i < length; i++) {
            var th = document.createElement('th');
            th.style.fontWeight = 'lighter';
            th.style.color = '#757575';
            th.style.padding = '5px 0px';
            th.style.fontSize = '17px';
            th.innerHTML = headers[i];
            thead.appendChild(th);
        }
        return thead;
    };
    $private.createTbody = function () {
        var tbody = document.createElement('tbody');
        tbody.setAttribute('class', 'log-messages');
        return tbody;
    };
    $private.createCloseButton = function (codRepository) {
        var anchor = document.createElement('a');
        anchor.style.backgroundImage = "url('http://imguol.com/tag-manager/icons-sprite.png')";
        anchor.style.backgroundPosition = '-102px -49px';
        anchor.style.backgroundRepeat = 'no-repeat';
        anchor.style.height = '15px';
        anchor.style.width = '15px';
        anchor.style.display = 'inline-block';
        anchor.style.position = 'absolute';
        anchor.style.right = '12px';
        anchor.style.marginTop = '14px';
        anchor.style.cursor = 'pointer';
        anchor.setAttribute('title', 'Fechar o console do UOL Tag Manager');
        $private.applyEventClosed(anchor, codRepository);
        return anchor;
    };
    $private.applyEventClosed = function (element, codRepository) {
        var methodListener = (document.addEventListener) ? "addEventListener" : "attachEvent";
        var nameEvent = (document.addEventListener) ? "click" : "onclick";
        element[methodListener](nameEvent, function (e) {
            e.preventDefault();
            e.stopPropagation();
            $private.close(codRepository);
        });
    };
    $private.createControlButton = function (codRepository) {
        var ctrlButton = document.createElement('a');
        ctrlButton.style.backgroundImage = 'url(http://imguol.com/tag-manager/icons-sprite.png)';
        ctrlButton.style.backgroundPosition = '-122px -49px';
        ctrlButton.style.backgroundRepeat = 'no-repeat';
        ctrlButton.style.height = '25px';
        ctrlButton.style.width = '25px';
        ctrlButton.style.display = 'inline-block';
        ctrlButton.style.position = 'absolute';
        ctrlButton.style.right = '30px';
        ctrlButton.style.marginTop = '14px';
        ctrlButton.style.cursor = 'pointer';
        ctrlButton.className = 'maximized';
        ctrlButton.setAttribute('title', 'Minimizar o console do UOL Tag Manager');
        $private.applyEventControlButton(ctrlButton, codRepository);
        return ctrlButton;
    };
    $private.applyEventControlButton = function (element, codRepository) {
        var methodListener = (document.addEventListener) ? "addEventListener" : "attachEvent";
        var nameEvent = (document.addEventListener) ? "click" : "onclick";
        element[methodListener](nameEvent, function (event) {
            event.preventDefault();
            event.stopPropagation();
            $private.controlConsole(event.target, codRepository);
        });
    };
    $private.close = function (codRepository) {
        var mainContainer = document.getElementById(codRepository);
        if (mainContainer) {
            mainContainer.style.display = 'none';
        }
        document.cookie = $private.cookieName + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
    };
    $private.controlConsole = function (target, codRepository) {
        var className = target.className;
        var mainContainer = document.getElementById(codRepository);
        if (!mainContainer) {
            return false;
        }
        target.classList.remove(className);
        if (className === 'minimized') {
            mainContainer.style.height = Math.round(window.innerHeight / 3) + 'px';
            target.classList.add('maximized');
            target.style.backgroundPosition = '-122px -49px';
            target.setAttribute('title', 'Minimizar o console do UOL Tag Manager');
        } else if (className === 'maximized') {
            mainContainer.style.height = '50px';
            target.classList.add('minimized');
            target.style.backgroundPosition = '-78px -49px';
            target.setAttribute('title', 'Maximizar o console do UOL Tag Manager');
        } else {
            return false;
        }
    };
    $public.append = function (namTag, namespace, message, executionTime, messageType) {
        var logContainer = $private.getLogContainer();
        if (!logContainer) {
            return false;
        }
        if (!$private.isValidLog(namTag, namespace, message, executionTime, messageType)) {
            return false;
        }
        logContainer.appendChild($private.createLogLine(namTag, namespace, message, executionTime, messageType));
        return true;
    };
    $private.createLogLine = function (namTag, namespace, message, executionTime, messageType) {
        var logLine = document.createElement('tr');
        logLine.style.border = 'solid 0.5px #AAAAAA';
        logLine.style.backgroundColor = '#FFFFFF';
        var statusIcon = $private.createLogTd($private.createStatusIcon(messageType));
        statusIcon.style.padding = '10px';
        logLine.appendChild(statusIcon);
        logLine.appendChild($private.createLogTd(namTag));
        logLine.appendChild($private.createLogTd(namespace));
        logLine.appendChild($private.createLogTd(message));
        logLine.appendChild($private.createLogTd(executionTime + ' ms'));
        return logLine;
    };
    $private.getLogContainer = function () {
        if (!$private.container) {
            return;
        }
        var container = $private.findChildByClass($private.container, 'logContainer');
        var tbody = $private.findChildByClass(container.firstChild, 'log-messages');
        if (!tbody) {
            return;
        }
        return tbody;
    };
    $private.findChildByClass = function (element, className) {
        if (!element) {
            return;
        }
        var childNodes = element.childNodes;
        for (var i = 0, length = childNodes.length; i < length; i++) {
            var classes = childNodes[i].getAttribute('class');
            if (classes && classes.indexOf(className) > -1) {
                return childNodes[i];
            }
        }
        return;
    };
    $private.isValidLog = function (namTag, namespace, message, executionTime, messageType) {
        if (!$private.typeValidator.isString(namTag)) {
            return false;
        }
        if (!$private.typeValidator.isString(namespace)) {
            return false;
        }
        if (!$private.typeValidator.isString(message)) {
            return false;
        }
        if (!$private.typeValidator.isNumber(executionTime)) {
            return false;
        }
        if (!$private.isValidMessageType(messageType)) {
            return false;
        }
        return true;
    };
    $private.isValidMessageType = function (messageType) {
        if (!$private.typeValidator.isString(messageType)) {
            return false;
        }
        messageType = messageType.toLocaleLowerCase();
        if (messageType === 'error') {
            return true;
        }
        if (messageType === 'warn') {
            return true;
        }
        if (messageType === 'success') {
            return true;
        }
        return false;
    };
    $private.createStatusIcon = function (messageType) {
       var i = document.createElement('i');
       i.style.backgroundImage = 'url(http://imguol.com/tag-manager/icons-sprite.png)';
       i.style.backgroundPosition = $private.getStatusIconPosition(messageType);
       i.style.width = '20px';
       i.style.height = '20px';
       i.style.display = 'block';
       i.style.backgroundRepeat = 'no-repeat';
       return i.outerHTML;
    };
    $private.getStatusIconPosition = function (messageType) {
        messageType = messageType.toLocaleLowerCase();
        if (messageType == 'success') {
            return '-55px -49px';
        }
        if (messageType == 'warn') {
            return '-26px -49px';
        }
        if (messageType == 'error') {
            return '-1px -49px';
        }
    };
    $private.createLogTd = function (innerHTML) {
        var td = document.createElement('td');
        td.style.fontWeight = 'lighter';
        td.style.color = '#757575';
        td.style.fontSize = '17px';
        td.style.padding = '10px 0px';
        td.innerHTML = innerHTML;
        return td;
    };
    return $public.__constructor(codRepository, version);
}
function TagManager (nameSpace, codRepository, version) {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.validator = new TypeValidator();
    $private.versionManager = new VersionManager();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('core');
    $private.tagController = new TagController();
    $public.queryString = new QueryString();
    $private.cookieUtils = new CookieUtils();
    $private.dataLayer = new DataLayer();
    $private.version = '${parent.version}';
    $private.triggerQueue = [];
    $public.init = function (tags) {
        $private.startTime = (new Date()).getTime();
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        $private.startTMConsole();
        $private.dataLayer.createDataLayer();
        if (!$private.validator.isArray(tags)) {
            return false;
        }
        if ($private.versionManager.specificVersionWasRequested()) {
            return $private.versionManager.setSpecificVersion();
        }
        tags = tags.concat(defaultTags);
        $private.cloneTriggerQueue();
        return $private.createModules(tags);
    };
    $private.cloneTriggerQueue = function () {
        var triggerUOLTM = window.triggerUOLTM;
        if ($private.validator.isArray(triggerUOLTM)) {
            while (triggerUOLTM.length) {
                $private.triggerQueue.push(triggerUOLTM.shift());
            }
        }
        window.triggerUOLTM = $private.triggerUOLTM;
        window.triggerUOLTM.push = $private.pushTrigger;
    };
    $private.createModules = function (tags) {
        var result = true;
        if (tags.length === 0) {
            return false;
        }
        while (tags.length) {
            $private.tagController.addEvents(tags.shift());
        }
        $private.tagController.triggerEvents('autostart');
        $private.triggerOnDocComplete();
        $private.updateEvents();
        $private.trackManager.trackSuccess('Execucoes Finalizadas');
        var execTime = (new Date()).getTime() - $private.startTime;
        $public.logs.debug('Tempo de execuçao: ' + execTime);
        $private.trackManager.trackCustom('Execution', 'time', execTime);
        return result;
    };
    $private.triggerOnDocComplete = function() {
        if (document.readyState === "complete") {
            $private.tagController.triggerEvents('onload');
            return;
        }
        var method = (document.addEventListener) ? "readystatechange" : "onreadystatechange";
        var methodListener = (document.addEventListener) ? "addEventListener" : "attachEvent";
        document[methodListener](method, function() {
            if (document.readyState === "complete") {
                $private.tagController.triggerEvents('onload');
            }
        });
    };
    $private.updateEvents = function () {
        while ($private.triggerQueue.length) {
            $private.pushTrigger($private.triggerQueue.shift());
        }
    };
    $private.pushTrigger = function (trigger) {
        if ($private.validator.isObject(trigger)) {
            $private.triggerUOLTM(trigger.eventName);
            return;
        }
        $private.triggerUOLTM(trigger);
    };
    $private.triggerUOLTM = function(eventType) {
        if (!$private.validator.isString(eventType)) {
            $public.logs.warn("O gatilho solicitado deve ser uma string");
            return;
        }
        if (eventType === "onload") {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "onload"');
            return;
        }
        if (eventType === "autostart") {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "autostart"');
            return;
        }
        $private.tagController.triggerEvents(eventType);
    };
    $public.tagsHistory = function (tagName) {
        return $private.tagController.getTriggerHistory(tagName);
    };
    $private.startTMConsole = function () {
        var queryString = $public.queryString.getValue('uoltm_console');
        var repositoryCookie = $private.cookieUtils.getItem('uoltm_version');
        if (queryString === codRepository || repositoryCookie === codRepository) {
            tmConsole = new TMConsole(codRepository, version);
        }
    };
}
var nameSpace = (new NameSpace()).create();
nameSpace['ch6251'] = nameSpace['ch6251'] || {};
nameSpace['ch6251'].version = '61';
var tagManager = new TagManager(nameSpace, 'ch6251', '61');
nameSpace.trigger = tagManager.trigger;
tagManager.init([ {
  "module" : "DynadTrack",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)cart(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "onload" ],
  "config" : {
    "biddingValues" : null,
    "type" : "basket",
    "clientId" : "2015052901.2"
  },
  "name" : "Tracking de retargeting de carrinho"
}, {
  "module" : "DynadTrack",
  "rules" : {
    "enable" : [ "document.location.href.match(/produto/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "biddingValues" : null,
    "type" : "product",
    "clientId" : "2015052901"
  },
  "name" : "Tracking de retargeting de produto"
}, {
  "module" : "DynadTrack",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "onload" ],
  "config" : {
    "biddingValues" : null,
    "type" : "checkout",
    "clientId" : "2015052901"
  },
  "name" : "Tracking de retargeting de checkout"
}, {
  "module" : "DynadTrack",
  "rules" : {
    "enable" : [ "document.location.href.match(/categoria/)" ],
    "disable" : [ ]
  },
  "events" : [ "onload" ],
  "config" : {
    "biddingValues" : null,
    "type" : "category",
    "clientId" : "2015052901.1"
  },
  "name" : "Tracking de retargeting de categoria"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "activityId" : "175846",
    "activityType" : "itemsPurchase"
  },
  "name" : "DFP - Conversão"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "activityId" : "173806",
    "activityType" : "transaction"
  },
  "name" : "_reuso"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/lastcheck/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "activityId" : "172606",
    "activityType" : "transaction"
  },
  "name" : "_pendente DFP - LastCheck"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/bonprix.com.br\\/registration/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "activityId" : "172846",
    "activityType" : "transaction"
  },
  "name" : "_pendente DFP - Formulário Cadastro"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/op_product_quantity|op_product_price|op_product_size_id|op_page_type/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "activityId" : "172486",
    "activityType" : "transaction"
  },
  "name" : "_pendente DFP - Carrinho - Cadastro"
}, {
  "module" : "DfpSalesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(s)?:\\/\\/www\\.bonprix\\.com\\.br\\/payment(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "activityId" : "172726",
    "activityType" : "transaction"
  },
  "name" : "DFP - Pagamento"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)cart(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "var axel = Math.random()+\"\";\nvar a = axel * 10000000000000;\nvar IntPix = new Image();\nIntPix.src= \"//pubads.g.doubleclick.net/activity;xsp=173926;ord=\"+ a +\"?\";"
  },
  "name" : "DFP - Carrinho de Compras"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "codeInjector" : null
  },
  "name" : "_reutilizar 3"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : null
  },
  "name" : "_nao usar 4"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "var axel = Math.random()+\"\";\nvar a = axel * 10000000000000;\nvar IntPix = new Image();\nIntPix.src= \"//pubads.g.doubleclick.net/activity;xsp=172966;ord=\"+ a +\"?\";"
  },
  "name" : "DFP - Page Views"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)cart(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "    (function() {\n        var mr = '&r=' + Math.floor(Math.random() * 99999999999);\n        var mhl = document.createElement('script'); mhl.type = 'text/javascript'; mhl.async = true;\n        mhl.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tags.meltdsp.com/platform/tagHigh?p=6053_0_0_0_0_0_1_3' + mr;\n        var pms = document.getElementsByTagName('script')[0]; pms.parentNode.insertBefore(mhl, pms);\n    })();         "
  },
  "name" : "Melt - Carrinho de compra"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : " (function () {\n      var val = '&id='+universal_variable.transaction.total+'&l_id='+universal_variable.transaction.order_id+'';\n      var mr = '&r=' + Math.floor(Math.random() * 99999999999);\n      var mhl = document.createElement('script');\n      mhl.type = 'text/javascript';\n      mhl.async = true;\n      mhl.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tracker.meltdsp.com/platform/l?c=6053&t=js&p=6053_0_0_0_0_0_1_4' + val + mr;\n      var pms = document.getElementsByTagName('script')[0];\n      pms.parentNode.insertBefore(mhl, pms);\n  })();"
  },
  "name" : "Melt - Compra efetuada"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)www.bonprix.com.br%2F$/)" ],
    "disable" : [ "document.location.href.match(/(.*)cart(.*)$/)", "document.location.href.match(/categoria/)", "document.location.href.match(/(.*)(leadid|sale|orderid)(.*)$/)", "document.location.href.match(/produto/)" ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "  (function() {\n    var mr = '&r=' + Math.floor(Math.random() * 99999999999);\n    var mhl = document.createElement('script'); mhl.type = 'text/javascript'; mhl.async = true;\n    mhl.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tags.meltdsp.com/platform/tagHigh?p=6053_0_0_0_0_0_1_1' + mr;\n    var pms = document.getElementsByTagName('script')[0]; pms.parentNode.insertBefore(mhl, pms);\n  })();"
  },
  "name" : "Melt - Page View"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/categoria/)", "document.location.href.match(/produto/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "(function() {\n    var u_vars = \"&um1=[PRODUCT_ID]&um2=[CATEGORY_ID]\";\n    var mr = '&r=' + Math.floor(Math.random() * 99999999999);\n    var mhl = document.createElement('script'); mhl.type = 'text/javascript'; mhl.async = true;\n    mhl.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tags.meltdsp.com/platform/tagHigh?p=6053_0_0_0_0_0_1_2' + u_vars + mr;\n    var pms = document.getElementsByTagName('script')[0]; pms.parentNode.insertBefore(mhl, pms);\n})();"
  },
  "name" : "Melt - Produto e Categoria"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "codeInjector" : "(function(){\n  try {\n    var l, c, v, src, script,\n        ord = Date.now(),\n        uvar = window.universal_variable,\n        type = uvar.page.type.toLowerCase();\n\n    switch(type) {\n      case 'product':\n        l = \"121\";\n        c = uvar.product.sku_code;\n        break;\n      case 'category':\n        var items = [];\n        for(var i = 0; i < uvar.listing.items.length; i++) {\n          items.push(uvar.listing.items[i].sku_code);\n        }\n        l = \"120\";\n        c = items.join(\",\");\n        break;\n      case 'basket':\n        var items = [];\n        for(var i = 0; i < uvar.basket.line_items.length; i++) {\n          items.push(uvar.basket.line_items[i].product.sku_code);\n        }\n        l = \"122\";\n        c = items.join(\",\");\n        break;\n      case 'checkout':\n        var items = [];\n        for(var i = 0; i < uvar.transaction.line_items.length; i++) {\n          items.push(uvar.transaction.line_items[i].product.sku_code);\n        }\n        l = \"123\";\n        c = items.join(\",\");\n        v = uvar.transaction.total;\n        break;\n      default:\n        return;\n        break;\n    }\n\n    src = \"https://t5.dynad.net/lsep/?l=\"+l+\"&ord=\"+ord+\"&c=\"+c;\n    src += v ? (\"&v=\" + v) : \"\";\n\n    // script = document.createElement(\"script\");\n    // script.src = src;\n\n    // document.body.appendChild(script);\n\n    var req = new XMLHttpRequest();\n    req.addEventListener(\"load\", function(){\n      eval(this.responseText);\n    });\n    req.open(\"get\", src, true);\n    req.send();\n  } catch (e) {\n    console.warn(\"Não foi possível criar a pixel Dynad.\");\n    console.warn(e.message);\n  }\n})();"
  },
  "name" : "Pixels Dynad"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "var axel = Math.random()+\"\";\nvar a = axel * 10000000000000;\nvar IntPix = new Image();\nIntPix.src= \"//c.t.tailtarget.com/view/TT-10162-1/G5OP4A4983/track?tZ=\"+ a +\"?\";"
  },
  "name" : "Tail Target - Compra Efetuada"
}, {
  "module" : "CodeInjector",
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "codeInjector" : "var axel = Math.random()+\"\";\nvar a = axel * 10000000000000;\nvar IntPix = new Image();\nIntPix.src= \"//c.t.tailtarget.com/view/TT-10162-1/2BYDR3EO36/track?tZ=\"+ a +\"?\";"
  },
  "name" : "Tail Target - Geral"
}, {
  "module" : "CliquesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid|confirmation)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "label" : "venda",
    "advertiserId" : "35209"
  },
  "name" : "Tracking - bonprix.ger"
}, {
  "module" : "CliquesConversion",
  "rules" : {
    "enable" : [ "document.location.href.match(/(.*)(leadid|sale|orderid|confirmation)(.*)$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "label" : "venda",
    "advertiserId" : "35211"
  },
  "name" : "Tracking bonprixrtg.ger"
} ]);
})(window, document);
alert("Hello");