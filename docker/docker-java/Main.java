public class Main {
    public static void main(String[] args) {
        System.out.println("Eu sou o seu primeiro programa.");
        Main pp = new Main();
        pp.primeiroMetodo();
        pp.segundoMetodo();
    }

    private void primeiroMetodo() {
        System.out.println("Executando o primeiro metodo.");
    }

    private void segundoMetodo() {
        System.out.println("Executando o segundo metodo.");
    }
}