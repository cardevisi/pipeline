# Data Science Deploy

Projeto contendo o necessário para rodar o Jenkins e Docker Registry para docker.

#### Dependências necessárias

1. [Docker 1.11+](https://docs.docker.com/engine/installation/)
2. [Docker-compose 1.7+](https://docs.docker.com/compose/install/)

#### Configurações
##### Hosts
Inicialmente é necessário configurar o arquivo de host para enxergar as máquinas pela url

```
# QA JENKINS/REGISTRY
10.138.146.125 jenkins.ds.intranet registry.ds.intranet
# PROD JENKINS/REGISTRY
10.139.152.32 jenkins.ds.intranet registry.ds.intranet
```

**OBS: Nas máquinas de QA e PROD é necessário apenas configurar o host referente a própria máquina.**

##### Certificados
É preciso adicionar um arquivo de certificado à pasta do ca-certificate do seu sistema operacional para que seja possível o acesso ao Docker Registry. Para isso, crie uma pasta chamada **"docker"** dentro da pasta correta de acordo com seu SO:


- OSX/Linux: [http://kb.kerio.com/product/kerio-connect/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html](http://kb.kerio.com/product/kerio-connect/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html)
- Windows: 

Depois de criar a pasta, copie o arquivo **ds-deploy/nginx/certs/docker-cert.crt** para dentro da pasta **"docker"** que foi criada no passo anterior.

##### Arquivos do Projeto

- **Máquina QA/PROD**: Copiar a pasta __ds-deploy__ para dentro de **/usr/local/share/**
- **Localmente**: Você pode colocar em qualquer lugar, porém, é melhor junto com os demais projetos

##### Rodando o Projeto:
Acesse a raiz do projeto e execute o comando:

```sh
$ sudo docker-compose up -d
```

**OBS: Caso necessário rebuildar as imagens novamente, adicione ao final do comando o parâmetro ```--build```. Ficando assim:**

```sh
$ sudo docker-compose up -d --build
```

**OBS 2: Esse projeto roda nas portas 80, 443, 8080, se o Apache/Nginx estiverem rodando haverá conflito e não rodará o projeto.**

##### Parando a Execução do Projeto

Acesse a raiz do projeto e execute o comando:

```sh
$ sudo docker-compose down
```


___


### Usando o Docker Registry
O Docker Registry é onde será armazenado todas as imagens docker do Data Science. Para utiliza-lo você precisa configurar o certificado em sua máquina, da mesma forma que foi demonstrado anteriormente em **Certificados** dentro da seção de **Configurações**.

Segue os principais comandos que serão utilizados:


#### Logando no Registry
Para logar, execute o seguinte comando no terminal:

```sh
$ sudo docker login https://registry.ds.intranet
```

Após executar o comando, será pedido seu usuário e logo em seguida a senha. A partir disso, você está pronto para baixar e enviar suas imagens do Registry.

**OBS: Para todos os comando ligados ao Registry (pull, push, etc), é necessário que você esteja devidamente logado.**


#### Baixando Imagens do Registry
Para baixar uma imagem de nosso repositório, logue-se e execute o comando abaixo:

```sh
$ sudo docker pull registry.ds.intranet/$NOME-DA-IMAGEM
```

**OBS: Substitua "$NOME-DA-IMAGEM" pelo nome da imagem que deseja baixar.**


#### Enviando Imagens para o Registry
Para criar imagens para serem enviadas para o Registry, é necessário seguir uma nomenclatura:

**registry.ds.intranet/$NOME-DA-IMAGEM**

Já para enviar para o repositório, logue-se e execute o comando:

```sh
$ sudo docker push registry.ds.intranet/$NOME-DA-IMAGEM
```

**OBS: Substitua "$NOME-DA-IMAGEM" pelo nome da imagem.**


#### Listando Imagens Disponíveis no Registry
Abra [https://registry.ds.intranet/v2/_catalog](https://registry.ds.intranet/v2/_catalog) no browser, digite sua senha e será exibido um JSON com os repositórios disponíveis para download.


#### Listando Tags Disponíveis para Imagem no Registry
Abra no browser o link, substituíndo **"$NOME-DA-IMAGEM"** pela a imagem desejada:
[https://registry.ds.intranet/v2/$NOME-DA-IMAGEM/tags/list](https://registry.ds.intranet/v2/$IMAGE/tags/list)