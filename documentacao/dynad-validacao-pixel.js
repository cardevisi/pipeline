###Fluxo de validação de pixels DYNAD:

Com o Customer selecionado, acessar a aba Lists

#Exemplo de pixels implementadas para o site Dafiti CO:

|ID	 |Name				List Type Days to Expire
|120 |Home Genérica		Pixel Home Genérica	30
|115 |Home Masculina	Pixel Masculino	30
|114 |Home Feminina	Pixel Feminino	30
|63	 |Pixel Checkout Page	Checkout Page	30
|62	 |Pixel Shopping Cart Page	Shopping Cart	30
|61	 |Pixel Category Page	Category Page	30
|60	 |Pixel Product Page	Product Details	30
|41	 |Categoria	Pixel Category Core	30
|40	 |Detalhe de Produto	Pixel Detail Core	30
|39	 |Carrinho de compras	Pixel Cart Core	30
|38	 |Checkout	Pixel Checkout Core	30