/* UOL Tag Manager Copyright 2016. Todos os diretos reservados | Versao do repositório: 36 */ 
(function (window, document, undefined){
'use strict';
function TypeValidator () {
    var $public = this;
    var $private = {};
    $public.isDefined = function (value) {
        return value !== undefined && value !== null;
    };
    $public.isString = function (value) {
        return value !== undefined && (typeof value) === 'string' && ($private.stringIsNotEmpty(value));
    };
    $private.stringIsNotEmpty = function(string) {
        if ($private.trimString(string).length > 0) {
            return true;
        }
        return false;
    };
    $private.trimString = function (string) {
        return string.replace(/^(\s+)|(\s+)$/gm, '').replace(/\s+/gm, ' ');
    };
    $public.isArray = function (value) {
        return value && value.constructor === Array;
    };
    $public.isObject = function (entity) {
        return entity && entity.constructor === Object;
    };
    $public.isFunction = function (value) {
        return value !== undefined && value.constructor === Function;
    };
    $public.isNumber = function (value) {
        return Number(value) === value;
    };
    $public.isInt = function (value) {
        return $public.isNumber(value) && value % 1 === 0;
    };
    $public.isRegExp = function (value) {
        return value !== undefined && value.constructor === RegExp;
    };
    $public.isNumericString = function (value) {
        return $public.isString(value) && !isNaN(value);
    };
    $public.isBoolean = function (value) {
        return value !== undefined && value.constructor == Boolean;
    };
    return $public;
}
function QueryString () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.getValue = function (name) {
        if (!$private.queryStrings) {
            return;
        }
        return $private.queryStrings[name];
    };
    $public.getQueryStrings = function () {
        return $private.queryStrings;
    };
    $public.setValues = function () {
        if (!$private.typeValidator.isString($public.getSearch())) {
            return;
        }
        var substrings = $public.getSearch().substring(1).split('&');
        if (!$private.typeValidator.isArray(substrings)) {
            return;
        }
        if (substrings.length === 0) {
            return;
        }
        for (var i = 0, length = substrings.length; i < length; i++) {
            if ($private.typeValidator.isString(substrings[i])) {
                $private.addQueryString(substrings[i]);
            }
        }
    };
    $public.getSearch = function () {
        return window.location.search;
    };
    $private.addQueryString = function (substring) {
        var queryObject = $private.getQueryObject(substring);
        if (!queryObject) {
            return;
        }
        $private.queryStrings = $private.queryStrings || {};
        $private.queryStrings[queryObject.key] = queryObject.value;
    };
    $private.getQueryObject = function (substring) {
        var query = substring.split('=');
        if (!$private.typeValidator.isString(query[0])) {
            return;
        }
        var result = {
            'key': query[0],
            'value': 'true'
        };
        if ($private.typeValidator.isString(query[1])) {
            result.value = query[1];
        }
        return result;
    };
    $public.setValues();
}
function Logs () {
    var $private = {};
    var $public = this;
    $private.queryString = new QueryString();
    $private.typeValidator = new TypeValidator();
    $private.history = {
        'warn'  : [],
        'error' : [],
        'info'  : [],
        'debug' : [],
        'log'   : []
    };
    $public.getPrefix = function (prefix) {
        return $private.prefix;
    };
    $public.setPrefix = function (prefix) {
        if ($private.typeValidator.isString(prefix)) {
            $private.prefix = '[' + prefix + '] ';
        }
    };
    $public.warn = function (msg) {
        return $private.print(msg, 'warn');
    };
    $public.error = function (msg) {
        return $private.print(msg, 'error');
    };
    $public.info = function (msg) {
        return $private.print(msg, 'info');
    };
    $public.debug = function (msg) {
        return $private.print(msg, 'debug');
    };
    $public.log = function (msg) {
        return $private.print(msg, 'log');
    };
    $private.print = function (msg, fn) {
        if (!$private.prefix) {
            return;
        }
        if (!$private.typeValidator.isString(msg)) {
            return;
        }
        msg = $private.prefix + msg;
        $public.setHistory(fn, msg);
        if ($public.isActive() === false || !$private.hasConsole()) {
            return;
        }
        return $private.runLogMethod(fn, msg);
    };
    $public.isActive = function () {
        if ($private.queryString.getValue('tm') === 'debug') {
            return true;
        }
        return false;
    };
    $public.getHistory = function (methodName) {
        if ($private.typeValidator.isArray($private.history[methodName])) {
            return $private.history[methodName];
        }
        return;
    };
    $public.setHistory = function (fn, msg) {
        if ($private.typeValidator.isString(msg) && $private.typeValidator.isArray($private.history[fn])) {
            $private.history[fn].push(msg);
        }
    };
    $private.hasConsole = function () {
        if (!$private.typeValidator.isDefined($public.getConsole())){
            return false;
        }
        if (!$private.typeValidator.isDefined($public.getConsoleLog())) {
            return false;
        }
        return true;
    };
    $public.getConsole = function () {
        return window.console;
    };
    $public.getConsoleLog = function () {
        return $public.getConsole().log;
    };
    $private.runLogMethod = function (fn, msg) {
        if ($private.typeValidator.isDefined($public.getConsole()[fn])) {
            $public.getConsole()[fn](msg);
            return fn;
        }
        window.console.log(msg);
        return 'log';
    };
}
function NameSpace (packageName) {
    var $private = {};
    var $public = this;
    $private.version = '1.0.126';
    $private.validator = new TypeValidator();
    $public.init = function (packageName) {
        if ($private.validator.isString(packageName)) {
            return $public.create(packageName);
        }
    };
    $public.create = function (packageName) {
        $private.createUOLPD();
        $private.createTagManager();
        return $private.createPackage(packageName);
    };
    $private.createUOLPD = function () {
        if (!$private.validator.isObject(window.UOLPD)) {
            window.UOLPD = {};
        }
    };
    $private.createTagManager = function () {
        if (!$private.validator.isObject(UOLPD.TagManager) && !$private.validator.isFunction(UOLPD.TagManager) && typeof UOLPD.TagManager !== "object") {
            UOLPD.TagManager = {};
        }
    };
    $private.createPackage = function (packageName) {
        if (!$private.validator.isString(packageName)) {
            return UOLPD.TagManager;
        }
        if (!$private.validator.isObject(UOLPD.TagManager[packageName])) {
            UOLPD.TagManager[packageName] = {};
        }
        UOLPD.TagManager[packageName].version = $private.version;
        if (!$private.validator.isArray(UOLPD.TagManager[packageName].config)) {
            UOLPD.TagManager[packageName].config = [];
        }
        if (!$private.validator.isObject(UOLPD.TagManager[packageName].log)) {
            UOLPD.TagManager[packageName].log = new Logs();
            UOLPD.TagManager[packageName].log.setPrefix('UOLPD.TagManager.' + packageName);
        }
        return UOLPD.TagManager[packageName];
    };
    return $public.init(packageName);
}
function ScriptUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.dataCalls = 'data-uoltm-calls';
    $public.hasTagScript = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return false;
        }
        if (src.length === 0) {
            return false;
        }
        if ($public.findScript($private.removeProtocol(src))) {
            return true;
        }
        return false;
    };
    $private.removeProtocol = function (value) {
        return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };
    $public.findScript = function (src) {
        var scripts = document.getElementsByTagName('script');
        for (var i = 0, length = scripts.length; i < length; i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if (scriptSrc && $private.isSrcEqual(scriptSrc, src)) {
                return scripts[i];
            }
        }
        return;
    };
    $private.isSrcEqual = function (src1, src2) {
        return (src1.indexOf(src2) > -1);
    };
    $public.createScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        tag.async = true;
        return tag;
    };
    $public.appendTag = function (script) {
        if (!$private.typeValidator.isDefined(script)) {
            return;
        }
        if (script.constructor === HTMLScriptElement) {
            $private.lastScriptsParent().appendChild(script);
            return true;
        }
    };
    $private.lastScriptsParent = function () {
        return document.getElementsByTagName('script')[0].parentNode;
    };
    $public.createSyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        document.write('<scr' + 'ipt src="'+ src + '"></scr' + 'ipt>');
    };
}
function StringUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.trim = function (value) {
        if (!$private.typeValidator.isString(value)) {
            return;
        }
        if (value.length === 0) {
            return;
        }
        value = value.replace(/^(\s+)|(\s+)$/gm, '').replace(/\s+/gm, ' ');
        return value;
    };
    $public.getValueFromKeyInString = function (str, name, separator) {
        if (!$private.typeValidator.isString(name) || name.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(str) || str.length === 0) {
            return;
        }
        if (!$private.typeValidator.isString(separator) || separator.length === 0) {
            return;
        }
        if (str.substring(str.length - 1)) {
            str += separator;
        }
        name += '=';
        var startIndex = str.indexOf(name);
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = str.indexOf('=', startIndex) + 1;
        var endIndex = str.indexOf(separator, middleIndex);
        if (endIndex === -1){
            endIndex = str.length;
        }
        return unescape(str.substring(middleIndex, endIndex));
    };
    return $public;
}
function RemoteStorage () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.validator = new TypeValidator();
    $private.path = 'https://tm.uol.com.br/mercurio.html';
    $private.iframe = undefined;
    $private.iframeLoaded = false;
    $private.queue = [];
    $private.requests = {};
    $private.id = 0;
    $public.init = function () {
        $private.hasLoaded(function () {
            if (!$private.isIframeInPage()) {
                $private.setIframe();
            }
            $private.setListener($private.iframe);
            return true;
        });
        return $public;
    };
    $private.isIframeInPage = function () {
        var iframes = document.getElementsByTagName('iframe');
        for (var i = 0; i < iframes.length; i++) {
            if (iframes[i].getAttribute('src') == $private.path) {
                $private.iframe = iframes[i];
                return true;
            }
        }
        return false;
    };
    $public.get = function (key, callback) {
        $private.communicator('get', key, undefined, callback);
    };
    $public.set = function (key, value, callback) {
        $private.communicator('set', key, value, callback);
    };
    $public.removeItem = function (key, callback) {
        callback = callback || function () {};
        $private.communicator('delete', key, undefined, callback);
    };
    $private.hasLoaded = function (fn, timeout) {
        timeout = (timeout ? timeout + 10 : 10);
        if (typeof fn != "function") {
            return false;
        }
        if (document.body !== null) {
            fn();
            return true;
        }
        setTimeout(function() {
            $private.hasLoaded(fn, timeout);
        }, timeout);
    };
    $private.setIframe = function () {
        $private.iframe = $private.buildIframe();
    };
    $private.buildIframe = function () {
        var iframe = document.createElement('iframe');
        iframe.setAttribute('src', $private.path);
        iframe.setAttribute('id', 'tm-remote-storage');
        iframe.setAttribute('style', 'position:absolute;width:1px;height:1px;left:-9999px;display:none;');
        document.body.appendChild(iframe);
        return iframe;
    };
    $private.setListener = function (iframe) {
        var hasEventListener = (window.addEventListener !== undefined);
        var method = (hasEventListener) ? 'addEventListener' : 'attachEvent';
        var message = (hasEventListener) ? 'message' : 'onmessage';
        var load = (hasEventListener) ? 'load' : 'onload';
        iframe[method](load, $private.iframeLoadHandler);
        window[method](message, $private.messageHandler);
        return iframe;
    };
    $private.iframeLoadHandler = function (event) {
        $private.iframeLoaded = true;
        if (!$private.queue.length) {
            return;
        }
        for (var i = 0, length = $private.queue.length; i < length; i++) {
            $private.sendMessageToIframe($private.queue[i]);
        }
        $private.queue = [];
    };
    $private.messageHandler = function (event) {
        try {
            var data = JSON.parse(event.data);
            $private.requests[data.id].callback(data.key, data.value);
            delete $private.requests[data.id];
        } catch (err) {}
    };
    $private.communicator = function (method, key, value, callback) {
        if (!$private.validator.isString(key)) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        if (method === 'set' && value === undefined) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        if (!$private.validator.isFunction(callback)) {
            $public.logs.warn('Ocorreu um erro ao requisitar os dados');
            return;
        }
        var request = {
            id: ++$private.id,
            key: key,
            method: method
        };
        if (value) {
            request.value = value;
        }
        var data = {
            request: request,
            callback: callback
        };
        if ($private.iframeLoaded) {
            $private.sendMessageToIframe(data);
        } else {
            $private.queue.push(data);
        }
        if (!$private.iframe) {
            $public.init();
        }
    };
    $private.sendMessageToIframe = function (data) {
        $private.requests[data.request.id] = data;
        data = JSON.stringify(data.request);
        if (!data) {
            return;
        }
        $private.iframe.contentWindow.postMessage(data, $private.path);
    };
    return $public.init();
}
function TrackManager () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.imagesSrc = [];
    $private.baseUrl = '//logger.rm.uol.com.br/v1/?prd=98&grp=Origem:';
    $private.raffledRate = Math.round(Math.random() * 100);
    $private.samplingRate = 1;
    $public.trackSuccess = function (msr) {
        $public.sendPixel($private.getPixelSrc($private.getMeasure(msr)), $private.samplingRate);
    };
    $private.getMeasure = function (msr) {
        if (!$private.typeValidator.isString(msr)) {
            return;
        }
        return '&msr=' + encodeURIComponent(msr) + ':1';
    };
    $private.getPixelSrc = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }
        if (!$public.getModuleName()) {
            return;
        }
        if (!$public.getRepoId()) {
            return;
        }
        return $private.baseUrl + $public.getModuleName() + $public.getRepoId() + src;
    };
    $public.getModuleName = function (moduleName) {
        return $private.moduleName;
    };
    $public.setModuleName = function (moduleName) {
        if ($private.typeValidator.isString(moduleName)) {
            $private.moduleName = 'TM-' + moduleName + ';';
        }
    };
    $public.getRepoId = function () {
        if (!$private.repoId) {
            $public.setRepoId();
        }
        return $private.repoId;
    };
    $public.setRepoId = function() {
        var src = $private.getScriptSrc();
        if (!src) {
            return;
        }
        var srcMatch = src.match(/uoltm.js\?id=(.{6}).*/);
        var repoId = srcMatch ? srcMatch[1] : null;
        if (repoId) {
            $private.repoId = 'tm_repo_id:' + repoId;
        }
    };
    $private.getScriptSrc = function() {
        var script = document.querySelector('script[src*="tm.jsuol.com.br/uoltm.js"]');
        var src = script ? script.src : null;
        return src;
    };
    $public.sendPixel = function (src, samplingRate) {
        if (!$private.typeValidator.isString(src)) {
            return;
        }
        if (!$private.isTrackEnabled(samplingRate)) {
            return;
        }
        var img = document.createElement('img');
        img.setAttribute('src', src);
        $public.imagesSrc.push(img.src);
    };
    $private.isTrackEnabled = function (samplingRate) {
        if (window.location.protocol.match('https')) {
            return false;
        }
        try {
            if (window.localStorage.getItem('trackManager') == 'true') {
                return true;
            }
        } catch (e) {}
        if ($public.getRaffledRate() <= samplingRate) {
            return true;
        }
        return false;
    };
    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };
    $public.trackError = function (errorType, errorEffect) {
        errorType = $private.getErrorType(errorType);
        if (!errorType) {
            return;
        }
        errorEffect = $private.getErrorEffect(errorEffect);
        if (!errorEffect) {
            return;
        }
        var url = $private.getPixelSrc(errorType + errorEffect + $private.getMeasure('Erros'));
        $public.sendPixel(url, 100);
    };
    $private.getErrorType = function (errorType) {
        return $private.generateKeyValue('erro_tipo', errorType);
    };
    $private.generateKeyValue = function (key, value) {
        if (!$private.typeValidator.isString(key)) {
            return;
        }
        if (!$private.typeValidator.isString(value)) {
            return;
        }
        return ';' + key + ':' + value;
    };
    $private.getErrorEffect = function (errorEffect) {
        return $private.generateKeyValue('erro_efeito', errorEffect);
    };
    $public.trackCustom = function (measure, trackType, trackValue) {
        var url;
        var src = $private.generateKeyValue(trackType, trackValue);
        if (!measure || !src) {
            return;
        }
        measure = $private.getMeasure(measure);
        if (measure) {
            url = $private.getPixelSrc(src + measure);
            $public.sendPixel(url, $private.samplingRate);
        }
    };
}
(function (window, document, undefined){
'use strict';
function ScriptAsyncWriter () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.ModulesInjectorAsync');
    $private.validator = new TypeValidator();
    $private.scriptUtils = new ScriptUtils();
    $public.init = function(config) {
        if(!$private.isValidConfig(config)){
            return;
        }
        $private.createAsyncScript(config.javascriptSource);
    };
    $private.isValidConfig = function(config) {
        if(!$private.validator.isObject(config)) {
            $public.logger.warn('O objeto de configuração fornecido está inválido');
            return false;
        }
        if(!$private.validator.isString(config.javascriptSource)) {
            $public.logger.warn('O atributo javascriptSource do objeto de configuração fornecido está inválido');
            return false;
        }
        return true;
    };
    $private.createAsyncScript = function(src) {
        if (!$private.validateJavascriptSource(src)) {
            return;
        }
        if ($private.scriptUtils.hasTagScript(src)) {
            return;
        }
        var script = $private.scriptUtils.createScript(src);
        $private.scriptUtils.appendTag(script);
        $public.logger.log('O script foi criado de forma assíncrona');
    };
    $private.validateJavascriptSource = function (javascriptSource) {
        var validURLRegex = /^(http(?:s)?:)?\/\/([a-zA-Z0-9\.\-\_\/]+)(\.js)?(\?(([a-zA-Z0-9\.\-\_\/\=\&\%]+)))?$/gm;
        if(!validURLRegex.test(javascriptSource)) {
            $public.logger.warn('A URL fornecida para o script está inválida');
            return false;
        }
        return true;
    };
}
var namespace = new NameSpace('ModulesInjectorAsync');
var scriptAsyncWriter = new ScriptAsyncWriter();
namespace.init = scriptAsyncWriter.init;
var config = namespace.config;
})(window, document);
(function (window, document, undefined){
'use strict';
/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(s,p){var m={},l=m.lib={},n=function(){},r=l.Base={extend:function(b){n.prototype=this;var h=new n;b&&h.mixIn(b);h.hasOwnProperty("init")||(h.init=function(){h.$super.init.apply(this,arguments)});h.init.prototype=h;h.$super=this;return h},create:function(){var b=this.extend();b.init.apply(b,arguments);return b},init:function(){},mixIn:function(b){for(var h in b)b.hasOwnProperty(h)&&(this[h]=b[h]);b.hasOwnProperty("toString")&&(this.toString=b.toString)},clone:function(){return this.init.prototype.extend(this)}},
q=l.WordArray=r.extend({init:function(b,h){b=this.words=b||[];this.sigBytes=h!=p?h:4*b.length},toString:function(b){return(b||t).stringify(this)},concat:function(b){var h=this.words,a=b.words,j=this.sigBytes;b=b.sigBytes;this.clamp();if(j%4)for(var g=0;g<b;g++)h[j+g>>>2]|=(a[g>>>2]>>>24-8*(g%4)&255)<<24-8*((j+g)%4);else if(65535<a.length)for(g=0;g<b;g+=4)h[j+g>>>2]=a[g>>>2];else h.push.apply(h,a);this.sigBytes+=b;return this},clamp:function(){var b=this.words,h=this.sigBytes;b[h>>>2]&=4294967295<<
32-8*(h%4);b.length=s.ceil(h/4)},clone:function(){var b=r.clone.call(this);b.words=this.words.slice(0);return b},random:function(b){for(var h=[],a=0;a<b;a+=4)h.push(4294967296*s.random()|0);return new q.init(h,b)}}),v=m.enc={},t=v.Hex={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++){var k=a[j>>>2]>>>24-8*(j%4)&255;g.push((k>>>4).toString(16));g.push((k&15).toString(16))}return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j+=2)g[j>>>3]|=parseInt(b.substr(j,
2),16)<<24-4*(j%8);return new q.init(g,a/2)}},a=v.Latin1={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++)g.push(String.fromCharCode(a[j>>>2]>>>24-8*(j%4)&255));return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j++)g[j>>>2]|=(b.charCodeAt(j)&255)<<24-8*(j%4);return new q.init(g,a)}},u=v.Utf8={stringify:function(b){try{return decodeURIComponent(escape(a.stringify(b)))}catch(g){throw Error("Malformed UTF-8 data");}},parse:function(b){return a.parse(unescape(encodeURIComponent(b)))}},
g=l.BufferedBlockAlgorithm=r.extend({reset:function(){this._data=new q.init;this._nDataBytes=0},_append:function(b){"string"==typeof b&&(b=u.parse(b));this._data.concat(b);this._nDataBytes+=b.sigBytes},_process:function(b){var a=this._data,g=a.words,j=a.sigBytes,k=this.blockSize,m=j/(4*k),m=b?s.ceil(m):s.max((m|0)-this._minBufferSize,0);b=m*k;j=s.min(4*b,j);if(b){for(var l=0;l<b;l+=k)this._doProcessBlock(g,l);l=g.splice(0,b);a.sigBytes-=j}return new q.init(l,j)},clone:function(){var b=r.clone.call(this);
b._data=this._data.clone();return b},_minBufferSize:0});l.Hasher=g.extend({cfg:r.extend(),init:function(b){this.cfg=this.cfg.extend(b);this.reset()},reset:function(){g.reset.call(this);this._doReset()},update:function(b){this._append(b);this._process();return this},finalize:function(b){b&&this._append(b);return this._doFinalize()},blockSize:16,_createHelper:function(b){return function(a,g){return(new b.init(g)).finalize(a)}},_createHmacHelper:function(b){return function(a,g){return(new k.HMAC.init(b,
g)).finalize(a)}}});var k=m.algo={};return m}(Math);
(function(s){function p(a,k,b,h,l,j,m){a=a+(k&b|~k&h)+l+m;return(a<<j|a>>>32-j)+k}function m(a,k,b,h,l,j,m){a=a+(k&h|b&~h)+l+m;return(a<<j|a>>>32-j)+k}function l(a,k,b,h,l,j,m){a=a+(k^b^h)+l+m;return(a<<j|a>>>32-j)+k}function n(a,k,b,h,l,j,m){a=a+(b^(k|~h))+l+m;return(a<<j|a>>>32-j)+k}for(var r=CryptoJS,q=r.lib,v=q.WordArray,t=q.Hasher,q=r.algo,a=[],u=0;64>u;u++)a[u]=4294967296*s.abs(s.sin(u+1))|0;q=q.MD5=t.extend({_doReset:function(){this._hash=new v.init([1732584193,4023233417,2562383102,271733878])},
_doProcessBlock:function(g,k){for(var b=0;16>b;b++){var h=k+b,w=g[h];g[h]=(w<<8|w>>>24)&16711935|(w<<24|w>>>8)&4278255360}var b=this._hash.words,h=g[k+0],w=g[k+1],j=g[k+2],q=g[k+3],r=g[k+4],s=g[k+5],t=g[k+6],u=g[k+7],v=g[k+8],x=g[k+9],y=g[k+10],z=g[k+11],A=g[k+12],B=g[k+13],C=g[k+14],D=g[k+15],c=b[0],d=b[1],e=b[2],f=b[3],c=p(c,d,e,f,h,7,a[0]),f=p(f,c,d,e,w,12,a[1]),e=p(e,f,c,d,j,17,a[2]),d=p(d,e,f,c,q,22,a[3]),c=p(c,d,e,f,r,7,a[4]),f=p(f,c,d,e,s,12,a[5]),e=p(e,f,c,d,t,17,a[6]),d=p(d,e,f,c,u,22,a[7]),
c=p(c,d,e,f,v,7,a[8]),f=p(f,c,d,e,x,12,a[9]),e=p(e,f,c,d,y,17,a[10]),d=p(d,e,f,c,z,22,a[11]),c=p(c,d,e,f,A,7,a[12]),f=p(f,c,d,e,B,12,a[13]),e=p(e,f,c,d,C,17,a[14]),d=p(d,e,f,c,D,22,a[15]),c=m(c,d,e,f,w,5,a[16]),f=m(f,c,d,e,t,9,a[17]),e=m(e,f,c,d,z,14,a[18]),d=m(d,e,f,c,h,20,a[19]),c=m(c,d,e,f,s,5,a[20]),f=m(f,c,d,e,y,9,a[21]),e=m(e,f,c,d,D,14,a[22]),d=m(d,e,f,c,r,20,a[23]),c=m(c,d,e,f,x,5,a[24]),f=m(f,c,d,e,C,9,a[25]),e=m(e,f,c,d,q,14,a[26]),d=m(d,e,f,c,v,20,a[27]),c=m(c,d,e,f,B,5,a[28]),f=m(f,c,
d,e,j,9,a[29]),e=m(e,f,c,d,u,14,a[30]),d=m(d,e,f,c,A,20,a[31]),c=l(c,d,e,f,s,4,a[32]),f=l(f,c,d,e,v,11,a[33]),e=l(e,f,c,d,z,16,a[34]),d=l(d,e,f,c,C,23,a[35]),c=l(c,d,e,f,w,4,a[36]),f=l(f,c,d,e,r,11,a[37]),e=l(e,f,c,d,u,16,a[38]),d=l(d,e,f,c,y,23,a[39]),c=l(c,d,e,f,B,4,a[40]),f=l(f,c,d,e,h,11,a[41]),e=l(e,f,c,d,q,16,a[42]),d=l(d,e,f,c,t,23,a[43]),c=l(c,d,e,f,x,4,a[44]),f=l(f,c,d,e,A,11,a[45]),e=l(e,f,c,d,D,16,a[46]),d=l(d,e,f,c,j,23,a[47]),c=n(c,d,e,f,h,6,a[48]),f=n(f,c,d,e,u,10,a[49]),e=n(e,f,c,d,
C,15,a[50]),d=n(d,e,f,c,s,21,a[51]),c=n(c,d,e,f,A,6,a[52]),f=n(f,c,d,e,q,10,a[53]),e=n(e,f,c,d,y,15,a[54]),d=n(d,e,f,c,w,21,a[55]),c=n(c,d,e,f,v,6,a[56]),f=n(f,c,d,e,D,10,a[57]),e=n(e,f,c,d,t,15,a[58]),d=n(d,e,f,c,B,21,a[59]),c=n(c,d,e,f,r,6,a[60]),f=n(f,c,d,e,z,10,a[61]),e=n(e,f,c,d,j,15,a[62]),d=n(d,e,f,c,x,21,a[63]);b[0]=b[0]+c|0;b[1]=b[1]+d|0;b[2]=b[2]+e|0;b[3]=b[3]+f|0},_doFinalize:function(){var a=this._data,k=a.words,b=8*this._nDataBytes,h=8*a.sigBytes;k[h>>>5]|=128<<24-h%32;var l=s.floor(b/
4294967296);k[(h+64>>>9<<4)+15]=(l<<8|l>>>24)&16711935|(l<<24|l>>>8)&4278255360;k[(h+64>>>9<<4)+14]=(b<<8|b>>>24)&16711935|(b<<24|b>>>8)&4278255360;a.sigBytes=4*(k.length+1);this._process();a=this._hash;k=a.words;for(b=0;4>b;b++)h=k[b],k[b]=(h<<8|h>>>24)&16711935|(h<<24|h>>>8)&4278255360;return a},clone:function(){var a=t.clone.call(this);a._hash=this._hash.clone();return a}});r.MD5=t._createHelper(q);r.HmacMD5=t._createHmacHelper(q)})(Math);
function Format () {
    var $public = this;
    var $private = {};
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('native-ads');
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.NativeAds');
    $private.hasCalledCallback = false;
    $private.flagBehaviour = '0';
    $private.src = window.location.protocol + '//js.siga.uol.com.br/uoljson.min.js';
    $public.loadScript = function () {
        if ($private.scriptHasLoaded()) {
            return;
        }
        var script = document.createElement('script');
        script.src = $private.src;
        script.async = true;
        document.head.appendChild(script);
    };
    $private.scriptHasLoaded = function () {
        if (document.querySelector('[src*="' + $private.src + '"]')) {
            $public.logs.debug('O script ' + $private.src + ' já existe na página');
            return true;
        }
        $public.logs.debug('Inserindo o script ' + $private.src + ' na página');
        return false;
    };
    $public.getCodDisplaySupplier = function (coddisplaysupplier) {
        return $private.coddisplaysupplier;
    };
    $public.setCodDisplaySupplier = function (coddisplaysupplier) {
        if ($private.typeValidator.isString(coddisplaysupplier)) {
            $private.coddisplaysupplier = coddisplaysupplier;
        }
    };
    $public.getNumAds = function (numAds) {
        return $private.numAds;
    };
    $public.setNumAds = function (numAds) {
        if ($private.typeValidator.isInt(Number(numAds))) {
            $private.numAds = numAds;
        }
    };
    $public.getDesLabel = function (desLabel) {
        return $private.desLabel;
    };
    $public.setDesLabel = function (desLabel) {
        if (!$private.typeValidator.isString(desLabel)) {
            $private.desLabel = 'native';
            return;
        }
        $private.desLabel = desLabel;
    };
    $public.getNative = function () {
        return $private.nativeFlag;
    };
    $public.setNative = function (nativeFlag) {
        if (isNaN(nativeFlag)) {
            return;
        }
        if (nativeFlag === '0' || nativeFlag === '1') {
            $private.nativeFlag = nativeFlag;
        }
    };
    $public.getUrlMobile = function () {
        return $private.urlMobile;
    };
    $public.setUrlMobile = function (urlMobile) {
        $private.urlMobile = window.location.href;
        if ($private.typeValidator.isArray(urlMobile) && urlMobile[0] === '1') {
            var formatedUrl = window.location.protocol + "//m." + window.location.hostname;
            $private.urlMobile = formatedUrl;
        }
    };
    $public.getFlagBehaviour = function () {
        return $private.flagBehaviour;
    };
    $public.setFlagBehaviour = function (flagBehaviour) {
        if (flagBehaviour === '1') {
            $private.flagBehaviour = flagBehaviour;
        }
    };
    $public.getCallback = function (callback) {
        return $private.callback;
    };
    $public.setCallback = function (callback) {
        if (!$private.typeValidator.isFunction(callback)) {
            return;
        }
        $private.callback = function (ads) {
            $public.logs.debug('O Callback está sendo chamado pelo formato');
            $private.hasCalledCallback = true;
            if (!$private.typeValidator.isArray(ads)) {
                $public.logs.warn('O serviço não retornou uma lista de anúncios');
                $private.trackManager.trackError('exibicao-vazia', 'parse-ads');
                return;
            }
            if (ads.length < $private.numAds) {
                $public.logs.warn(ads.length + ' anúncios foram recebidos quando foram requisitados ' + $private.numAds);
                $private.trackManager.trackError('exibicao-incompleta', 'missing-ads');
            }
            callback(ads);
        };
    };
    $public.display = function () {
        if ($public.hasFormatLoaded()) {
            if (!$private.coddisplaysupplier || !$private.numAds || !$private.callback || !$private.typeValidator.isDefined($private.nativeFlag) || !$private.typeValidator.isDefined($private.urlMobile)) {
                return;
            }
            $public.logs.debug('O objeto do formato foi carregado e está sendo instanciado');
            setTimeout(function () {
                if (!$private.hasCalledCallback) {
                    $public.logs.debug('O callback ainda não foi chamado pelo formato');
                    $private.trackManager.trackError('exibicao-vazia', 'service-timeout');
                }
            }, 7000);
            var config = {
                'coddisplaysupplier': $private.coddisplaysupplier,
                'numads': $private.numAds,
                'deslabel': $private.desLabel,
                'native': $private.nativeFlag,
                'urlReferer': $private.urlMobile,
                'flagBehaviour': $private.flagBehaviour
            };
            return UOLPD.Siga.sigaFormatJSON.getAds(config, $private.callback);
        }
        setTimeout($public.display, 100);
    };
    $public.hasFormatLoaded = function () {
        if (!$private.typeValidator.isObject(window.UOLPD)) {
            $public.logs.warn('Objeto UOLPD ainda não existe');
            return false;
        }
        if (!$private.typeValidator.isObject(window.UOLPD.Siga)) {
            $public.logs.warn('Objeto Siga ainda não existe');
            return false;
        }
        if (!$private.typeValidator.isDefined(window.UOLPD.Siga.sigaFormatJSON)) {
            $public.logs.warn('Objeto Siga ainda não existe');
            return false;
        }
        if (!$private.typeValidator.isFunction(window.UOLPD.Siga.sigaFormatJSON.getAds)) {
            $public.logs.warn('Objeto Siga ainda não existe');
            return false;
        }
        return true;
    };
}
function Ad () {
    var $public = this;
    var $private = {};
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('native-ads');
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.NativeAds');
    $private.masks = {
        '%NATIVE_URL%': {
            'required': 1,
            'value': 'link'
        },
        '%NATIVE_IMG%': {
            'required': 0,
            'value': 'image'
        },
        '%NATIVE_FULL_DESCRIPTION%': {
            'required': 0,
            'value': 'fullText'
        },
        '%NATIVE_DESCRIPTION%': {
            'required': 0,
            'value': 'shortText'
        },
        '%NATIVE_TITLE%': {
            'required': 0,
            'value': 'title'
        },
        '%NATIVE_DESCRIPTION1%': {
            'required': 0,
            'value': 'description1'
        },
        '%NATIVE_DESCRIPTION2%': {
            'required': 0,
            'value': 'description2'
        },
        '%NATIVE_URL_DISPLAY%': {
            'required': 0,
            'value': 'urlDisplay'
        },
    };
    $public.getMarkup = function () {
        if (!$private.markup) {
            $public.logs.warn('O markup não foi configurado');
            return;
        }
        return $private.markup;
    };
    $public.setMarkup = function (markup) {
        if (!$private.adData) {
            $public.logs.warn('O anúncio não existe');
            return;
        }
        if ($private.isvalidMarkup(markup)) {
            $private.markup = $private.replaceMasks(markup);
        }
    };
    $private.isvalidMarkup = function (markup) {
        if (!$private.typeValidator.isString(markup)) {
            $private.trackError('invalid-markup', 'O markup não é uma string');
            return false;
        }
        for (var key in $private.masks) {
            if ($private.masks[key].required === 0) {
                continue;
            }
            if (markup.indexOf(key) < 0) {
                $private.trackError('invalid-markup', 'O markup não tem a máscara ' + key);
                return false;
            }
        }
        return true;
    };
    $private.replaceMasks = function (markup) {
        for (var key in $private.masks) {
            var field = new RegExp(key, 'g');
            var value = $private.masks[key].value;
            markup = markup.replace(field, $private.adData[value]);
        }
        return markup;
    };
    $public.getShortTextLimit = function (shortTextLimit) {
        return $private.shortTextLimit;
    };
    $public.setShortTextLimit = function (shortTextLimit) {
        shortTextLimit = Number(shortTextLimit);
        if ($private.typeValidator.isInt(shortTextLimit)) {
            $private.shortTextLimit = shortTextLimit;
        } else {
            $private.trackError('invalid-markup', 'O limitador de texto deve ser um número');
        }
    };
    $public.getAdData = function (adData) {
        return $private.adData;
    };
    $public.setAdData = function (adData) {
        if ($private.isValidAd(adData)) {
            $private.adData = $private.convertAdData(adData);
        }
    };
    $private.isValidAd = function (ad) {
        if (!$private.typeValidator.isObject(ad)) {
            $private.trackError('invalid-ad', 'O anúncio não é um objeto');
            return false;
        }
        if (!$private.typeValidator.isString(ad.description1)) {
            $private.trackError('invalid-ad', 'O atributo "description1" do anúncio não é do tipo "string"');
            return false;
        }
        if (!$private.typeValidator.isString(ad.description2)) {
            $private.trackError('invalid-ad', 'O atributo "description2" do anúncio não é do tipo "string"');
            return false;
        }
        if(!$private.typeValidator.isString(ad.title)) {
            $private.trackError('invalid-ad', 'O atributo "title" do anúncio não é do tipo "string"');
            return false;
        }
        if (!$private.typeValidator.isString(ad.urlClick)) {
            $private.trackError('invalid-ad', 'O atributo "urlClick" do anúncio não é do tipo "string"');
            return false;
        }
        if (!$private.typeValidator.isString(ad.urlImage)) {
            $private.trackError('invalid-ad', 'O atributo "urlImage" do anúncio não é do tipo "string"');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.convertAdData = function (ad) {
        var adData = {};
        adData.fullText = ad.title + ' ' + ad.description1 + ' ' + ad.description2;
        adData.link = $private.getLink(ad.urlClick);
        adData.image = ad.urlImage;
        adData.shortText = $private.getShortText(adData.fullText);
        adData.title = ad.title;
        adData.description1 = ad.description1;
        adData.description2 = ad.description2;
        adData.urlDisplay = ad.urlDisplay;
        return adData;
    };
    $private.getLink = function (link) {
        try {
            return link + '&fBhAsH=fbUOL' + CryptoJS.MD5($public.getCode(link));
        } catch (e) {
            $private.trackError('invalid-ad', 'Ocorreu um problema na hora de criptografar a url do anúncio');
            return link;
        }
    };
    $public.getCode = function (a) {
        var b = a.indexOf('msg') + 4;
        var c = a.substring(b);
        var d = c.indexOf('&');
        var e = c.substring(0, d);
        e = e.length;
        e = e.toString();
        return e;
    };
    $private.getShortText = function (fullText) {
        if (!$private.shortTextLimit) {
            $public.logs.debug('O limite de texto do anúncio não foi configurado e sua descrição será exibida na integra');
            return fullText;
        }
        if(fullText.length <=  $private.shortTextLimit) {
            $public.logs.debug('A descrição é menor/igual ao limite de texto e será exibida na integra');
            return fullText;
        }
        var shortText = fullText.substring(0, $private.shortTextLimit);
        var lastChar = shortText.charAt(shortText.length-1);
        if(/[a-zA-Zà-úÀ-Ú0-9]/.test(lastChar)) {
            shortText = fullText.substring(0, ($private.shortTextLimit - 1));
        }
        return shortText + '...';
    };
}
function NativeAds () {
    var $public = this;
    var $private = {};
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('native-ads');
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.NativeAds');
    $private.adsCount = 0;
    $public.setMarkup = function (markup) {
        if ($private.typeValidator.isString(markup)) {
            $private.markup = markup;
        }
    };
    $public.getMarkup = function () {
        return $private.markup;
    };
    $public.setContainer = function (container) {
        if ($private.typeValidator.isString(container)) {
            $private.container = container;
        }
    };
    $public.getContainer = function () {
        return $private.container;
    };
    $public.setBefore = function (before) {
        if ($private.typeValidator.isString(before)) {
            $private.before = before;
        }
    };
    $public.setReplacementElement = function (replacementElement) {
        if ($private.typeValidator.isString(replacementElement)) {
            $private.replacementElement = replacementElement.split(' %NEXT_ELEMENT% ');
        }
    };
    $public.getReplacementElement = function () {
        return $private.replacementElement;
    };
    $public.getShortTextLimit = function (shortTextLimit) {
        return $private.shortTextLimit;
    };
    $public.setShortTextLimit = function (shortTextLimit) {
        shortTextLimit = Number(shortTextLimit);
        if ($private.typeValidator.isInt(shortTextLimit)) {
            $private.shortTextLimit = shortTextLimit;
        }
    };
    $public.setStyleTag = function (styleTag) {
        if ($private.typeValidator.isString(styleTag)) {
            $private.styleTag = styleTag;
        }
    };
    $public.getStyleTag = function () {
        return $private.styleTag;
    };
    $public.display = function (ads) {
        $public.logs.debug('A função de exibição dos anúncios foi chamada');
        if (!$private.markup) {
            return;
        }
        if (!$private.typeValidator.isArray(ads)) {
            $public.logs.warn('O serviço não retornou uma lista de anúncios');
            return;
        }
        if (ads.length === 0) {
            $public.logs.warn('O serviço retornou uma lista de anúncios vazia');
            return;
        }
        return $private.showAds(ads, $public.getElements());
    };
    $private.showAds = function (ads, elements) {
        if (!elements.containerElement) {
            $public.logs.warn('O elemento container ainda não foi carregado');
            window.setTimeout(function () {
                $public.display(ads);
            }, 100);
            return;
        }
        for (var i = 0, length = ads.length; i < length; i++) {
            if ($private.appendAd($private.getAd(ads[i]), elements.containerElement, elements.replacementElement[i], elements.before, i)) {
                $public.logs.debug('Exibindo o anúncio');
                $private.setAdsCount(1);
            }
        }
        if ($private.adsCount > 0) {
            $private.addStyleTag();
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        }
        return $private.adsCount;
    };
    $private.setAdsCount = function (count) {
        $private.adsCount = $private.adsCount + count;
    };
    $public.getAdsCount = function () {
        return $private.adsCount;
    };
    $private.getAd = function (adData) {
        var ad = new Ad();
        ad.setShortTextLimit($private.shortTextLimit);
        ad.setAdData(adData);
        ad.setMarkup($private.markup);
        return ad.getMarkup();
    };
    $public.getElements = function () {
        try {
            return {
                'containerElement' : $private.getContainerElement($private.container),
                'replacementElement' : $private.getReplacementElements(),
                'before' : $private.getContainerElement($private.before)
            };
        } catch (e) {
            return {};
        }
    };
    $private.getReplacementElements = function () {
        var replacementElement = [];
        if (!$private.replacementElement) {
            return replacementElement;
        }
        for (var i = 0, length = $private.replacementElement.length; i < length; i++) {
            replacementElement.push($private.getContainerElement($private.replacementElement[i]));
        }
        return replacementElement;
    };
    $private.getContainerElement = function (element) {
        if (!element) {
            return;
        }
        var querySelectors = element.split('%contentWindow%');
        if (querySelectors.length == 1) {
            return document.querySelector(querySelectors[0]);
        }
        return document.querySelector(querySelectors[0]).contentWindow.document.querySelector(querySelectors[1]);
    };
    $private.getContainerElements = function (element) {
        if (!element) {
            return;
        }
        var querySelectors = element.split('%contentWindow%');
        if (querySelectors.length == 1) {
            return document.querySelectorAll(querySelectors[0]);
        }
        return document.querySelector(querySelectors[0]).contentWindow.document.querySelectorAll(querySelectors[1]);
    };
    $private.appendAd = function (ad, containerElement, replacementElement, before, index) {
        if (!ad) {
            return false;
        }
        window.addEventListener('resize', $private.resizeSigaImg);
        if (replacementElement) {
            replacementElement.innerHTML = ad;
            $public.logs.debug('Substituindo o elemento pelo anúncio');
            $private.resizeSigaImg(index);
            return true;
        }
        if (!containerElement) {
            return false;
        }
        if (before) {
            containerElement.insertBefore($private.getMarkup(ad), before);
            $private.resizeSigaImg(index);
            $public.logs.debug('Inserindo o anúncio antes do elemento configurado');
            return true;
        }
        $private.appendMarkup(containerElement, ad);
        $private.resizeSigaImg(index);
        $public.logs.debug('Inserindo o anúncio após o elemento configurado');
        return true;
    };
    $private.resizeSigaImg = function(index) {
        if (!$private.container) {
            return;
        }
        var elementList = $private.getContainerElements($private.container + ' img');
        var sigaImgList = $private.getContainerElements($private.container + ' img[src*="siga.imguol"]');
        if (!elementList || !sigaImgList) {
            return;
        }
        var refereceImg = elementList[0];
        var sigaImg = sigaImgList[index];
        setTimeout(function () {
            if (!refereceImg || !sigaImg) {
                return;
            }
            if (refereceImg.getAttribute('src') === sigaImg.getAttribute('src')) {
                refereceImg = elementList[1] || elementList[0];
            }
        }, 500);
        var interval = window.setInterval(function() {
            $private.resizeInterval(refereceImg, sigaImg, interval);
        }, 250);
    };
    $private.resizeInterval = function (refereceImg, sigaImg, interval) {
        if (!refereceImg || !sigaImg) {
            window.clearInterval(interval);
            return;
        }
        if (refereceImg.clientHeight === 0 || refereceImg.clientWidth === 0) {
            return;
        }
        window.clearInterval(interval);
        var sigaImgParent = sigaImg.parentElement;
        if (!sigaImgParent) {
            return;
        }
        sigaImgParent.style.maxWidth = refereceImg.clientWidth + 'px';
        sigaImgParent.style.maxHeight = refereceImg.clientHeight + 'px';
        sigaImgParent.style.setProperty('display', 'block', 'important');
        sigaImgParent.style.overflow = 'hidden';
    };
    $private.appendMarkup = function (container, element) {
        container.appendChild($private.getMarkup(element));
    };
    $private.getMarkup = function (outer) {
        var div = document.createElement('div');
        div.innerHTML = outer;
        return div.children[0];
    };
    $private.addStyleTag = function () {
        if (!$private.typeValidator.isString($public.getStyleTag())) {
            return;
        }
        var style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        var tag = $public.getStyleTag();
        if (style.styleSheet) {
            style.styleSheet.cssText = tag;
        } else {
            style.appendChild(document.createTextNode(tag));
        }
        var head = document.head || document.getElementsByTagName('head')[0];
        head.appendChild(style);
    };
}
function Main () {
    var $public = this;
    var $private = {};
    $private.typeValidator = new TypeValidator();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('native-ads');
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.NativeAds');
    $public.init = function (config) {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        $private.format = new Format();
        $private.nativeAds = new NativeAds();
        if ($private.typeValidator.isObject(config)) {
            $private.setNativeAds(config);
            $private.showFormat(config);
        } else {
            $public.logs.error('O objeto de configuração está inválido');
            $private.trackManager.trackError('config_invalido', 'fluxo_interrompido');
        }
    };
    $private.setNativeAds = function (config) {
        $private.nativeAds.setMarkup(config.markup);
        $private.nativeAds.setContainer(config.container);
        $private.nativeAds.setBefore(config.before);
        $private.nativeAds.setReplacementElement(config.replacementElement);
        $private.nativeAds.setShortTextLimit(config.shortTextLimit);
        $private.nativeAds.setStyleTag(config.styleTag);
    };
    $private.showFormat = function (config) {
        if (!$private.nativeAds.getElements().containerElement) {
            setTimeout(function () {
                $private.showFormat(config);
            }, 100);
            return;
        }
        $private.format.loadScript();
        $private.format.setUrlMobile(config.urlMobile);
        $private.format.setFlagBehaviour(config.flagBehaviour);
        $private.format.setNative('1');
        $private.format.setCodDisplaySupplier(config.coddisplaysupplier);
        $private.format.setNumAds(config.numAds);
        $private.format.setDesLabel(config.desLabel);
        $private.format.setCallback($private.nativeAds.display);
        $private.format.display();
    };
}
var nameSpace = new NameSpace('NativeAds');
nameSpace.init = function (config) {
    return (new Main()).init(config);
};
var __monitoracaoJsuol = 'tm.jsuol.com.br/modules/native-ads.js';
})(window, document);
(function (window, document, undefined){
'use strict';
var vitrineCSS = '.container-dynad {\n'+
                    'font-family: "UOLText",Arial,Helvetica,sans-serif;\n'+
                    'font-weight: normal;\n'+
                    'text-rendering: optimizeLegibility;\n'+
                    'text-shadow: 0 0 1px transparent;\n'+
                    '-webkit-font-smoothing: antialiased;\n'+
                    'background: #FFF;\n'+
                    'margin: 0;\n'+
                    'opacity: 0;\n'+
                    'width:1100px; \n'+
                '}\n'+
                '.container-dynad a { text-decoration: none; }\n'+
                '.container-dynad * {\n'+
                    'border: 0;\n'+
                    'color: #000;\n'+
                    'margin: 0;\n'+
                    'outline: 0;\n'+
                    'padding: 0;\n'+
                    'box-sizing: border-box; border:0;\n'+
                '}\n'+
                '\n'+
                '.title-container-dynad {\n'+
                    'clear: both;\n'+
                    'display: inline-block;\n'+
                    'float: left;\n'+
                    'margin-right: 3px;\n'+
                    'max-width: 100%;\n'+
                    'padding-top: 33px;\n'+
                '}\n'+
                '.title-container-dynad img { width: 100%; }\n'+
                '\n'+
                '.form-search-dynad {\n'+
                    'display: inline-block;\n'+
                    'float: right;\n'+
                    'margin-top: 30px;\n'+
                    'max-width: 50%;\n'+
                    'position: relative;\n'+
                    'width: 398px;\n'+
                '}\n'+
                '.form-search-dynad-input {\n'+
                    'border: 1px solid #e6e6e6;\n'+
                    'color: #999;\n'+
                    'font-family: sans-serif;\n'+
                    'font-size: 100%;\n'+
                    'font-weight: 300;\n'+
                    'height: 38px;\n'+
                    'line-height: 38px;\n'+
                    'text-indent: 10px;\n'+
                    'vertical-align: baseline;\n'+
                    'width: 100%;\n'+
                '}\n'+
                '.form-search-dynad-button {\n'+
                    'border-bottom: 2px solid #e6e6e6;\n'+
                    'cursor: pointer;\n'+
                    'height: 38px;\n'+
                    'position: absolute;\n'+
                    'right: 0;\n'+
                    'top: 0;\n'+
                    'width: 60px;\n'+
                    'background: #f3f2f1 url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAWCAMAAAAYXScKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANVQTFRFEREm8/Lc89Wk8/LqERFXpVcRwerxurm5pdzxXqTVZS0RwYhC6/LxEVekEUKIrOPxuo9e88eP5OrxwYgmwfLxESaINF6PQhERO3rAERFlrGUR1vLxNCYROxER8/LjSSYRXiYRwZ1z1qRXwdXj8+rcwdXqSWWW3ce5icfxbEIRusDO3c7AETt68+rVnlARJhER3fLxusfV8/LV68eIJkmPZaTV68CI3aRXutXqJnq5yOrxERFewYhJSXOdutzjJojA5NXHrNzx89WyutzxEREtERER8/LxTpitZAAAAL1JREFUeNp0kecSwiAQhC+JBkti19h77713Pd7/kSSSMKa4f2C+gWXZA+on4ItyJ2SRclIpUUYmyL1+qBxBS++NoHIdobundNKoIYQtKgXtvXkpEOI0WcG8ZvltDbxxmsZC3H67WsRsxqTMQC+JSE1uAcsp6nNBdwj9/2f9fR0ZVgYeNHfe50Dk/f7t0qG0zf6GY9XbA1NM9XR2jXJs9zsiZMj6VTgG1xRMvD66KVXOs9MDvEPrtSj4TvMjwACGKGT+VbnkigAAAABJRU5ErkJggg==") no-repeat center center; \n'+
                '}\n'+
                '\n'+
                '.produtos-content {\n'+
                    'clear: both;\n'+
                    'height: 249px;\n'+
                    'overflow: hidden;\n'+
                    'padding-top: 30px;\n'+
                    'text-align: justify;\n'+
                '}\n'+
                '.produtos-content:after { content: ""; display: inline-block; width: 100%; }\n'+
                '\n'+
                '.item-dynad { display: inline-block !important; margin: 0 auto 18px auto; width: 120px; }\n'+
                '.item-dynad span { color: #666; display: block; text-align: center; }\n'+
                '\n'+
                '.img-container { border: 1px solid #e6e6e6; height: 120px; margin-bottom: 18px; padding-top: 3px; width: 120px; }\n'+
                '\n'+
                '.item-dynad-title { font-size: 13.5px; font-weight: bold; margin-bottom: 6px; max-height: 16px; overflow: hidden; text-transform: uppercase; }\n'+
                '.item-dynad-desc { font-size: 12px; height: 28px; line-height: 14px; margin-bottom: 15px; max-height: 28px; overflow: hidden; }\n'+
                '.item-dynad-price { color: #000 !important; font-size: 14px; font-weight: bold; }\n'+
                '\n'+
                '.subs-dynad {\n'+
                    'border-color: #e6e6e6;\n'+
                    'border-style: solid;\n'+
                    'border-top-width: 1px;\n'+
                    'list-style: none;\n'+
                    'overflow: hidden;\n'+
                    'margin-top: 18px;\n'+
                    'padding: 18px 0 0;\n'+
                '}\n'+
                '.subs-dynad li:first-child { border: 0; margin-left: 0; padding-left: 0; }\n'+
                '.subs-dynad li { border-left: 1px solid #ccc; float: left; margin-left: 10px; padding-left: 10px; }\n'+
                '.subs-dynad li a { color: #666; line-height: 18px; font-size: 13px; }\n';
var vitrineHTML = '<div class="shopping-produtos-uol-header">\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/" target="_parent" class="title-container-dynad">\n'+
                        '<span class="title">\n'+
                            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAAAbCAMAAAAUENCjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAYBQTFRFhgQAh8f+AE+n8devrNTnqOf/yP7/DwcNgoKCx4cB6+vrAABLzI46bqvX//7e2sm5+/v7s7/TR0dHOY3Mh0cX//7Jvczc0r+3rZNfvr+/3v7/1tbWMjIyyMjIzNrt1JxXpKSkbbvsd3d3UQUACnC9XY2rmmxP7t3KunIXHEh2/+eo4uLi2+/7/9ygm8vHu9+vVlZWoN3+G1um6u/5ADaSARpGlpaWS6XdvOb/MliHAACDUCYQzOz69+/raBIAvff/MAAA7+zcj5ezs287ianBaWlpV5/PABd61LSPR2qc36dV9PT0Xa/pilIs7cqZ26tnEztp/+vPN2evkM/9u6eHFyNT/8eHT6fnpk4A5//////n9//////35qZO///vAIfH7///7/f/769f//fv/++2/s+Q//fk1+fx9+/f98eHo1oancPlPz9v7+/sVz8/5/f//8+Hs2MAv5ePIB0a//e//8d3979vakIax59vx3cAj8fvAHfHPz8+kjQAAAAA////3UXkKQAABH9JREFUeNrcmP1fEkkYwLlUhjdxhXxZQEVMiIji0sUXOt1KsizNshcKwrfA0uPK9q66O5r512/ednd23SU/5XEf7/lpeebt+c7M8zJ4kE1Cv86jH5Twx9sx1E3x2BX+YN+PzplX4da5hxhaX/Cee4iuy/8H4u2IPDwjQjQH5OFds9MvA/L4rjhqP2NTkC4HVYf5axl58Krw89aIsZIuPghBOWD7lMZSCQi1SDGg6wvxThAbJUhEmb7JIeoTKv4N7nKjNj7T9gUWtZ4H+2qPSDvMHnHsCy+bd4gC3GddXlUuvCQ94Qs+9w0erB6n6cAbV+vHtIcBAaFPMj5T+HMsArmAVIvry5I7REPdPJqptUbSarRKIXoPlZVdlLkIJ2mPNZDtraLmvZLyE4WA/bj9AL29V4J0APIvXl/KDr6vZUY11sWAmJsAeGeaIXCJdmyUwPR4dX9g/dm1r3aISMv8DEhFAE2J5HS9O4ThBE16zn6o9O+QjzdTdOmGyvfxzdKsl5rG21H4ED5kIzZ7eWxdUvpECO0JvUn148VlOoHyM+vX8xmegAgIn0mRAcJCwBki/1u7n0HgFWNWx+YrhSuzuKF2Wf+N9qjNeH/1rr8vrS6LIxD6RPfcgIhy9RrtEeJHS4KwpxPEWAJapehyEu9UNqWnfhxcdoxOryqLXrqczoiPZofddF1e0zmEeBaukD0XfELvh6/Z0NdZ7+kgUuwWyRLKpeiZRMou12kNBCfpddqD2Xl3iCtT26aFq14rREON2oJyKLjlBnFl6m90KghNMxXSCY019oUAWdCD6ncA3Jwefu8MIdh8QsEJRQhqrjOEOLAjBJMka8yZV8vJsWshqGzRPNGTLpkR0x0C/dlNiDHWGIh0hCDRZbXPo2evpzxi2iA+gLM6CWGm00DIp4bYnDfLjvplcr06+QT1TBEir87ZIUgccvOJbWeIJLGwnSMeQF060dZTHhZZ6+QTwnXi8gdd9ER0mnWPTnvEOBGifkx83zU6xRwhZJqYyc7nqPnlIlUkCUWcHkSh7O7YypalAHzuBOGQJ14I4WrZCpGfsuQJCwSLXA4QzHeBT04yJy4yFtj2+Qos6yV9YtYQYNaAwkJsz009PFr3lUGYGTuvZ+zoDt+GCTDHQsFffIbmISV2geApn8EKEJLFRJiIsztkSvmBC8Q7NciSXbgCsyvjrVZmlFlrh+C1075ZO8FnKwdVXGyt88LOj4vDwRm0f+tpCTxE7hB4TVx07eKCF9eBAgQKFAQTQZG4iWapOpAzBCk7qqx2ejzKqlhwP4acIPQqV69i4d0vvOzlNZQ/eC3NFs32ok4QpGZiK133XBLr9pxJwVwBxQsm1ANkhUhJju8JXOU7vwbMx4LxvKCm4QeIPF4V0iN+NljeDW6CHypk4JBn26KW4r4Ivv6JQjJgaFJtALVCsmULvVoq9+2X3bdEjE7f/RbMq5P/6vO0KxChs30DdxGiaVzAR2oUnVOIPags3JZlGQeSJ7H/FqLRnvzO/wxrG+k2SV5g4eiM/+34R4ABAM73zuwuorTRAAAAAElFTkSuQmCC" alt="logo">\n'+
                        '</span>\n'+
                    '</a>\n'+
                    '<form target="_blank" action="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas" method="get" class="form-search-dynad module" data-empty-url="http://shopping.uol.com.br/" accept-charset="ISO-8859-1" onsubmit="if(!window.retiraacento) window[\'retiraacento\'] = function (p){ca=\'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ\';sa=\'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC\'; nova=\'\';for(i=0;i<p.length;i++){if(ca.search(p.substr(i,1))>=0){nova+=sa.substr(ca.search(p.substr(i,1)),1);}else{nova+=p.substr(i,1);}}return nova;}; window.open(this.action + \'?kw=\' + window.retiraacento(this.querySelector(\'.q\').value)); return false;" >\n'+
                        '<fieldset> \n'+
                            '<input type="text" class="form-search-dynad-input q" name="kw" placeholder="Busque produtos">\n'+
                            '<button type="submit" class="form-search-dynad-button">\n'+
                                '&nbsp;\n'+
                            '</button>\n'+
                        '</fieldset>\n'+
                    '</form>\n'+
                '</div>\n'+
                '<div class="produtos-content">\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad1&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/tv" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/tv02.jpg" alt="TV LED">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">TV LED</span>\n'+
                        '<span class="item-dynad-desc">Wi-fi, 3D, HDMI, Full HD a partir de</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad2&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/geladeira" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/geladeira5.jpg" alt="GELADEIRA" title="GELADEIRA">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">GELADEIRA</span>\n'+
                        '<span class="item-dynad-desc">Duplex, side by side, frost free a partir de</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad3&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/sapatenis" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/sapatenis_moscoloni.jpg" alt="SAPATÊNIS" title="SAPATÊNIS">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">SAPATÊNIS</span>\n'+
                        '<span class="item-dynad-desc">Latego Marrone. Para ocasiões casuais</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad4&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/churrasqueira" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2013/churrasqueira6.jpg" alt="CHURRASQUEIRA" title="CHURRASQUEIRA">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">CHURRASQUEIRA</span>\n'+
                        '<span class="item-dynad-desc">Diversos artigos a partir de</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad5&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/celular-e-smartphone.html?kw=motorola" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/moto_g2g.jpg" alt="MOTO G" title="MOTO G">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">MOTO G</span>\n'+
                        '<span class="item-dynad-desc">3G, wi-fi, Android 4.3, Dual chip a partir de</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad6&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/frigobar" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/frigobar4.jpg" alt="FRIGOBAR" title="FRIGOBAR">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">FRIGOBAR</span>\n'+
                        '<span class="item-dynad-desc">Mais sofisticação, a partir de</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>\n'+
                    '<a href="http://click.uol.com.br/?rf=homec-horizontal-shopping-modulo1-item-dynad7&pos=mod-18;shopping&u=http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL&redir=http://shopping.uol.com.br/impressora" class="item-dynad" target="_parent">\n'+
                        '<span class="img-container">\n'+
                            '<img width="112" height="112" src="http://pubshop.img.uol.com.br/canais2014/impressora2.jpg" alt="IMPRESSORA" title="IMPRESSORA">\n'+
                        '</span>\n'+
                        '<span class="item-dynad-title">IMPRESSORA</span>\n'+
                        '<span class="item-dynad-desc">De vários modelos e marcas</span>\n'+
                        '<span class="item-dynad-price">CONFIRA</span>\n'+
                    '</a>'+
                '</div>\n'+
                '<ul class="subs-dynad">\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=celular">CELULARES</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=tv">SMART TV</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=geladeira">GELADEIRA</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=perfume">PERFUME</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=home-theater">HOME THEATER</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=fogao">FOGÃO</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=notebook">NOTEBOOK</a>                    \n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=tenis">TÊNIS</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=bicicleta">BICICLETA</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=tablet">TABLET</a>\n'+
                    '</li>\n'+
                    '<li>\n'+
                        '<a target="_parent" href="http://clicklogger.rm.uol.com.br/?prd=97&msr=Cliques%20de%20Origem:1&oper=11&grp=src:12;size:19;creative:box_ShoppingUOL_form&redir=http://shopping.uol.com.br/vitrine-ofertas?kw=maquina-de-lavar-roupas">LAVADORA DE ROUPAS</a>\n'+
                    '</li>\n'+
                '</ul>\n';
function DynadVitrineShopping () {
    var $private = {};
    var $public = this;
    $private.validator = new TypeValidator();
    $public.logger = new Logs();
    $public.logger.setPrefix('UOLPD.TagManager.DynadVitrineShopping');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dynad-vitrine-shopping');
    $private.showcaseLoaded = false;
    $private.forceTimeout = false;
    $private.homePlacementId = '121587';
    $private.config = null;
    $public.init = function(config) {
        if (!$private.isValidConfig(config)) {
            return false;
        }
        var lastArgument = arguments[arguments.length - 1];
        if (lastArgument && $private.validator.isBoolean(lastArgument)) {
            $private.forceTimeout = lastArgument;
        }
        $private.config = config;
        $private.waitContainer(function () {
            var container = $private.getById(config.containerId);
            if (!container) {
                $public.logger.warn('Container nao encontrado');
                return;
            }
            $private.setPostMessage();
            var samplingRate = $private.buildSamplingRate(config.samplingRate);
            var randomRate = Math.round(Math.random() * 100);
            if (samplingRate >= randomRate) {
                var script = $private.setScriptElement(config);
                container.appendChild(script);
                $private.waitFowShowcaseLoaded(container);
            } else {
                $private.buildStaticShowcase(container);
            }
        });
        return true;
    };
    $private.isValidConfig = function (config) {
        if (!$private.validator.isObject(config)) {
            $public.logger.warn('Configuração invalida');
            return false;
        }
        if (!$private.validator.isString(config.dominio)) {
            $public.logger.warn('URL não foi configurada corretamente');
            return false;
        }
        if (!$private.validator.isString(config.placementId)) {
            $public.logger.warn('PlacementId nao foi configurado corretamente');
            return false;
        }
        if (!$private.validator.isString(config.containerId)) {
            $public.logger.warn('ContainerId nao foi configurado corretamente');
            return false;
        }
        if (!$private.validator.isString(config.channel)) {
            $public.logger.warn('Canal do ROIMetter nao foi configurado');
        }
        return true;
    };
    $private.waitContainer = function (callback) {
        var interval = window.setInterval(function () {
            if ((document.readyState && document.readyState === 'complete') || $private.getById($private.config.containerId)) {
                window.clearInterval(interval);
                callback();
            }
        }, 5);
    };
    $private.buildSamplingRate = function (samplingRate) {
         if (!$private.validator.isString(samplingRate)) {
            $public.logger.warn('SamplingRate nao foi configurado corretamente');
            return 0;
        }
        samplingRate = samplingRate.substring(0, 3);
        samplingRate = parseInt(samplingRate);
        samplingRate = (isNaN(samplingRate)) ? 0 : samplingRate;
        return samplingRate;
    };
    $private.setPostMessage = function () {
        var hasEventListener = (window.addEventListener !== undefined);
        var method = (hasEventListener) ? 'addEventListener' : 'attachEvent';
        var message = (hasEventListener) ? 'message' : 'onmessage';
        window[method](message, $private.postMessageHandler);
    };
    $private.postMessageHandler = function (event) {
        if (!event.origin && !event.data) {
            return;
        }
        if (event.origin.indexOf('//s.dynad.net') < 0) {
            return;
        }
        try {
            var data = JSON.parse(event.data);
            var isSuccess = (data.message === 'vitrine carregada' && data.placementId == $private.config.placementId);
            if (isSuccess) {
                $private.showcaseLoaded = isSuccess;
            }
        } catch (err) {
            $public.logger.warn('Ocorreu um erro ao verificar dados de post message');
        }
    };
    $private.waitFowShowcaseLoaded = function (container) {
        if ($private.forceTimeout === true) {
            $public.logger.warn('Tempo limite de espera pela resposta do servidor excedido. Exibindo vitrine estática.');
            $private.buildStaticShowcase(container);
            return;
        }
        var timer = 0;
        var showCaseLoadInterval = window.setInterval(function () {
            timer += 5;
            if ($private.showcaseLoaded) {
                window.clearInterval(showCaseLoadInterval);
                $private.trackManager.sendPixel(';plmt:' + $private.config.placementId + '&msr=Impressoes:1');
            }
            if (timer === 6000) {
                window.clearInterval(showCaseLoadInterval);
                $public.logger.warn('Tempo limite de espera pela resposta do servidor excedido. Exibindo vitrine estática.');
                $private.trackManager.sendPixel(';plmt:' + $private.config.placementId + ';erro_tipo:timeout-dynad;erro_efeito:vitrine-em-branco&msr=Erros:1');
                $private.buildStaticShowcase(container);
            }
        }, 5);
    };
    $private.buildStaticShowcase = function (container) {
        var style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        style.innerHTML = vitrineCSS;
        var tmVitrineBkp = document.createElement('div');
        tmVitrineBkp.setAttribute('id', 'tm-vitrine-bkp');
        tmVitrineBkp.setAttribute('class', 'container-dynad');
        tmVitrineBkp.setAttribute('style', 'opacity: 1;');
        tmVitrineBkp.innerHTML = vitrineHTML;
        //VERIFICA SE O PLACEMENT ATUAL É IGUAL AO DA HOME
        if ($private.config.placementId === $private.homePlacementId) {
            container.innerHTML = style.outerHTML +'\n'+ tmVitrineBkp.outerHTML;
        } else {
            container.innerHTML = "";
        }
    };
    $private.setScriptElement = function (config) {
        var scriptElement = document.createElement('script');
        var scriptSrc = $private.buildScriptSrc(config);
        scriptElement.setAttribute('src', scriptSrc);
        scriptElement.setAttribute('async', 'async');
        scriptElement.setAttribute('id', 'tm-dynad-vitrine-include');
        return scriptElement;
    };
    $private.buildScriptSrc = function (config) {
        var protocolMatcher = /^(http(?:s)?(?:\:)?)?(?:\/\/)?/gm;
        var currentProtocol = window.location.protocol + '//';
        var scriptSrc = config.dominio.replace(protocolMatcher, currentProtocol);
        var channel = (config.channel) ? config.channel : '0';
        scriptSrc += '/script/?dc=' + config.placementId + ';ord=' + new Date().getTime() + ';chn=' + channel + ';';
        scriptSrc += 'cn=' + config.containerId + ';click=';
        return scriptSrc;
    };
    $private.getById = function (id) {
        return document.getElementById(id);
    };
}
var nameSpace = new NameSpace('DynadVitrineShopping');
nameSpace.init = function(config) {
    return new DynadVitrineShopping().init(config);
};
})(window, document);
(function (window, document, undefined){
'use strict';
function Format () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CliquesDisplay');
    $private.objectPattern = /\{(((\s*(\'|\")+\w+(\'|\")+\s*:\s*)\s*(\'|\")*\w+(\'|\")*\s*)(\,\s*)*)+\}/g;
    $public.setFileName = function (id) {
        if (!$private.isValidFormatId(id)) {
            return;
        }
        $private.fileName = 'format' + $private.chooseFormat(id) + '.html';
    };
    $private.isValidFormatId = function (id) {
        if (!$private.typeValidator.isString(id)) {
            return false;
        }
        if ($private.trimString(id).length === 0) {
            return false;
        }
        return true;
    };
    $private.trimString = function (string) {
        return string.replace(/\s/g, '');
    };
    $private.chooseFormat = function (id) {
        var formats = id.match($private.objectPattern);
        if (!formats) {
            $public.logs.debug('O formato de id ' + id + ' foi configurado');
            return id;
        }
        if (formats.length === 0) {
            return;
        }
        id = $private.getSortedFormat(formats).id;
        $public.logs.debug('O formato de id ' + id + ' foi sorteado');
        return id;
    };
    $private.getSortedFormat = function (formats) {
        var weightedFormats = $private.getWeightedFormats(formats);
        return weightedFormats[Math.floor(Math.random() * weightedFormats.length)];
    };
    $private.getWeightedFormats = function (formats) {
        var weightedFormats = [];
        for (var i = 0, length = formats.length; i < length; i++) {
            var format = $private.convertFormat(formats[i]);
            weightedFormats.push.apply(weightedFormats, $private.getFormatByWeight(format));
        }
        return weightedFormats;
    };
    $private.convertFormat = function (format) {
        try {
            return JSON.parse(format.replace(/'/g, '"'));
        } catch (e) {
            return;
        }
    };
    $private.getFormatByWeight = function (format) {
        var formats = [];
        if (!$private.isFormatObject(format)) {
            return formats;
        }
        for (var i = 0, length = format.weight; i < length; i++) {
            formats.push(format);
        }
        return formats;
    };
    $private.isFormatObject = function (format) {
        if (!$private.typeValidator.isObject(format)) {
            return false;
        }
        if (!$private.typeValidator.isString(format.id) && !$private.typeValidator.isNumber(format.id)) {
            return false;
        }
        if (!$private.typeValidator.isString(format.weight) && !$private.typeValidator.isNumber(format.weight)) {
            return false;
        }
        return true;
    };
    $public.getFileName = function () {
        return $private.fileName;
    };
}
function UrlParams () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CliquesDisplay');
    $private.typeValidator = new TypeValidator();
    $private.params = [];
    $public.setCoddisplaySupplier = function (coddisplaysupplier) {
        if ($private.isValidCoddisplaySupplier(coddisplaysupplier)) {
            $private.addParam('coddisplaysupplier', coddisplaysupplier);
        }
    };
    $private.isValidCoddisplaySupplier = function (coddisplaysupplier) {
        if (!$private.typeValidator.isString(coddisplaysupplier)) {
            return false;
        }
        if ($private.trimString(coddisplaysupplier).length === 0) {
            return false;
        }
        return true;
    };
    $private.trimString = function (string) {
        return string.replace(/\s/g, '');
    };
    $private.addParam = function (name, value) {
        $private.params.push(name + '=' + value);
    };
    $public.setSize = function (width, height) {
        if ($private.isValidSize(width, height)) {
            $private.addParam('size', width + 'x' + height);
        }
    };
    $public.setDesLabel = function (deslabel, width, height) {
        if($private.isValidDesLabel(deslabel)){
            $private.addParam('deslabel', deslabel);
            return true;
        }
        if (!$private.isValidSize(width, height)) {
            $public.logs.warn('Atributo deslabel inválido na configuração');
            return false;
        }
        $private.addParam('deslabel', width + 'x' + height);
        return true;
    };
    $private.isValidDesLabel = function (deslabel) {
        if(!$private.typeValidator.isString(deslabel)){
            return false;
        }
        if ($private.trimString(deslabel).length <= 0) {
            return false;
        }
        if (deslabel.indexOf('x') < 0) {
            return false;
        }
        var size = deslabel.match(/(\d+)x(\d+).*/);
        if (!size) {
            return false;
        }
        if (!$private.isValidSize(size[1], size[2])) {
            return false;
        }
        return true;
    };
    $private.isValidSize = function (width, height) {
        if (!$private.typeValidator.isString(width) || isNaN(width)) {
            return false;
        }
        if (!$private.typeValidator.isString(height) || isNaN(height)) {
            return false;
        }
        return true;
    };
    $public.setTitleColor = function (titleColor) {
        if (!$private.typeValidator.isString(titleColor)) {
            titleColor = 'A61919';
        }
        $private.addParam('titleColor', titleColor);
    };
    $public.setDescriptionColor = function (descriptionColor) {
        if (!$private.typeValidator.isString(descriptionColor)) {
            descriptionColor = '333';
        }
        $private.addParam('descrColor', descriptionColor);
    };
    $public.setAdBlockCount = function () {
        var adBlockCount;
        try {
            adBlockCount = window.sessionStorage.getItem('adBlockCount');
        } catch (e) {}
        $private.removeAdBlockCountOnUnload();
        if (!$private.isAdBlockCountValid(adBlockCount)) {
            adBlockCount = '-1';
        }
        adBlockCount = (parseInt(adBlockCount) + 1).toString();
        try {
            window.sessionStorage.setItem('adBlockCount', adBlockCount);
        } catch (e) {}
        $private.addParam('adBlockCount', adBlockCount);
    };
    $private.removeAdBlockCountOnUnload = function () {
        window.onunload = function() {
            try {
                window.sessionStorage.removeItem('adBlockCount');
            } catch (e) {}
        };
    };
    $private.isAdBlockCountValid = function (adBlockCount) {
        if (!$private.typeValidator.isString(adBlockCount)) {
            return false;
        }
        if ($private.trimString(adBlockCount).length === 0) {
            return false;
        }
        if (isNaN(adBlockCount)) {
            return false;
        }
        if (parseInt(adBlockCount) < 0) {
            return false;
        }
        return true;
    };
    $public.setUrlReferer = function (urlReferer) {
        if (!$private.typeValidator.isString(urlReferer)  || $private.trimString(urlReferer).length === 0) {
            if (urlReferer) {
                $public.logs.warn('Atributo urlReferer inválido na configuração');
            }
            urlReferer = window.location.protocol + '//' + window.location.hostname;
        }
        $private.addParam('urlReferer', encodeURIComponent(urlReferer));
    };
    $public.setNumAds = function (numAds) {
        if (!$private.isNumAdsValid(numAds)) {
            $public.logs.warn('Número de anúncios inválido na configuração');
            return;
        }
        $private.addParam('numads', encodeURIComponent(numAds));
    };
    $private.isNumAdsValid = function (numAds) {
        if (!$private.typeValidator.isString(numAds)) {
            return false;
        }
        if (numAds.length === 0) {
            return false;
        }
        if (isNaN(numAds)) {
            return false;
        }
        if (parseInt(numAds) <= 0) {
            return false;
        }
        return true;
    };
    $public.setUrlColor = function (urlColor) {
        if (!$private.typeValidator.isString(urlColor)) {
            urlColor = '999';
        }
        $private.addParam('urlColor', urlColor);
    };
    $public.setBorderColor = function (borderColor) {
        if (!$private.typeValidator.isString(borderColor)) {
            borderColor = 'f3f2f1';
        }
        $private.addParam('borderColor', borderColor);
    };
    $public.setFlagRetarget = function (flagRetarget) {
        if ($private.isOptOutEnabled()) {
            $private.addParam('flagRetarget', '0');
            return false;
        }
        if (flagRetarget === '0') {
            $private.addParam('flagRetarget', flagRetarget);
            return false;
        }
        $private.addParam('flagRetarget', '1');
        return true;
    };
    $private.isOptOutEnabled = function () {
        return $private.getCookie('BTOPTOUT') === '1';
    };
    $private.getCookie = function (name) {
        var cookie = document.cookie;
        var separator = ';';
        if (!$private.typeValidator.isString(name)) {
            return;
        }
        if (cookie.substring(cookie.length - 1)) {
            cookie += separator;
        }
        name += '=';
        var startIndex = cookie.indexOf(name);
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = cookie.indexOf('=', startIndex) + 1;
        var endIndex = cookie.indexOf(separator, middleIndex);
        if (endIndex === -1){
            endIndex = cookie.length;
        }
        return unescape(cookie.substring(middleIndex, endIndex));
    };
    $public.setRos = function (ros) {
        if ($private.isValidRos(ros)) {
            $private.addParam('ros', ros);
        }
    };
    $private.isValidRos = function (ros) {
        if (!$private.typeValidator.isString(ros)) {
            return false;
        }
        if ($private.trimString(ros).length === 0) {
            return false;
        }
        return true;
    };
    $public.setFlagBehaviour = function (flagBehaviour) {
        if (flagBehaviour === '0' || flagBehaviour === '1') {
            $private.addParam('flagBehaviour', flagBehaviour);
        }
    };
    $public.setFlagKeyword = function (flagKeyword) {
        if ($private.isValidFlagKeyword(flagKeyword)) {
            $private.addParam('flagKeyword', flagKeyword);
        }
    };
    $private.isValidFlagKeyword = function (flagKeyword) {
        if (!$private.typeValidator.isString(flagKeyword)) {
            return false;
        }
        if ($private.trimString(flagKeyword).length === 0) {
            return false;
        }
        return true;
    };
    $public.setTailTargetProfiles = function () {
        var tailtarget = $private.getTailTargetData();
        if (!$private.typeValidator.isObject(tailtarget)) {
            return;
        }
        for (var profile in tailtarget) {
            var name = $private.convertProfileName(profile);
            if (!name) {
                continue;
            }
            var value = $private.convertProfileValue(tailtarget[profile]);
            if (value) {
                $private.addParam(name, value);
            }
        }
    };
    $private.getTailTargetData = function () {
        try {
           return JSON.parse(window.localStorage.getItem('tailtarget'));
        } catch (e) {
            return;
        }
    };
    $private.convertProfileName = function (name) {
        if (name.indexOf('get') !== 0) {
            return;
        }
        return name.replace('get', 'tt-').toLowerCase();
    };
    $private.convertProfileValue = function (value) {
        if ($private.typeValidator.isArray(value)) {
            return value.join(',');
        }
        return value;
    };
    $public.setGeoPosition = function (geoPosition) {
        if (!$private.isValidGeoPosition(geoPosition)) {
            return;
        }
        $private.addParam('gp', geoPosition);
    };
    $private.isValidGeoPosition = function (geoPosition) {
        if (!$private.typeValidator.isString(geoPosition)) {
            return false;
        }
        if ((geoPosition !== '0') && (geoPosition !== '1')) {
            return false;
        }
        return true;
    };
    $public.setAdult = function (adult) {
        if (!$private.isValidAdult(adult)) {
            return;
        }
        $private.addParam('adult', '1');
    };
    $private.isValidAdult = function (adult) {
        if(!$private.typeValidator.isArray(adult)) {
            return false;
        }
        if (!$private.typeValidator.isString(adult[0])) {
            return false;
        }
        if (adult[0] !== '1') {
            return false;
        }
        return true;
    };
    $public.setCategory = function (category) {
        if (!$private.typeValidator.isString(category)) {
            return;
        }
        $private.addParam('category', category);
    };
    $public.getParams = function () {
        return $private.params.join('&');
    };
}
function Url () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator(); 
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CliquesDisplay'); 
    $private.adClient = 'http://adclient.siga.uol.com.br/';
    $public.setUrl = function (config) { 
        if (!$private.typeValidator.isObject(config)) {
            return;
        }
        $private.format = new Format();
        $private.format.setFileName(config.formatId);
        if (!$private.format.getFileName()) {
            return;
        }
        $private.setQueryString(config);
        var params = $private.params.getParams();
        if (!$private.hasQueryString(params)) {
            return;
        }
        $private.url = $private.adClient + $private.format.getFileName() + '?' + params;
    };
    $private.setQueryString = function (config) {
        $private.params = new UrlParams();
        $private.params.setCoddisplaySupplier(config.coddisplaysupplier);
        $private.params.setSize(config.width, config.height);
        $private.params.setTitleColor(config.titleColor);
        $private.params.setDescriptionColor(config.descrColor);
        $private.params.setUrlColor(config.urlColor);
        $private.params.setBorderColor(config.borderColor);
        $private.params.setUrlReferer(config.urlReferer);
        $private.params.setFlagRetarget(config.flagRetarget);
        $private.params.setNumAds(config.numAds);
        $private.params.setAdBlockCount();
        $private.params.setRos(config.ros);
        $private.params.setFlagBehaviour(config.flagBehaviour);
        $private.params.setFlagKeyword(config.flagKeyword);
        $private.params.setTailTargetProfiles();
        $private.params.setDesLabel(config.deslabel, config.width, config.height);
        $private.params.setGeoPosition(config.geoPosition);
        $private.params.setAdult(config.adult);
        $private.params.setCategory(config.category);
    };
    $private.hasQueryString = function (queryParams) {
        if (!$private.typeValidator.isString(queryParams)) {
            return false;
        }
        if (queryParams.length === 0) {
            return false;
        }
        return $private.hasRequiredParams(queryParams);
    };
    $private.hasRequiredParams = function (queryParams) {
        var requiredParams = ['coddisplaysupplier', 'size', 'adBlockCount', 'numads', 'deslabel'];
        for (var i = 0; i < requiredParams.length; i++) {
            var param = requiredParams[i];
            if (queryParams.indexOf(param) < 0) {
                $public.logs.warn('O atributo ' + param + ' está ausente');
                return false;
            }
        }
        return true;
    };
    $public.getUrl = function () {
        return $private.url;
    };
}
function Iframe () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CliquesDisplay');
    $public.getIframe = function () {
        return $private.iframe;
    };
    $public.setIframe = function (config) {
        $private.url = new Url();
        $private.url.setUrl(config);
        if ($private.typeValidator.isString($private.url.getUrl())) {
            $private.iframe = document.createElement('iframe');
            $private.iframe.setAttribute('src', $private.url.getUrl());
            $private.iframe.setAttribute('width', config.width);
            $private.iframe.setAttribute('height', config.height);
            $private.iframe.setAttribute('marginwidth', '0');
            $private.iframe.setAttribute('marginheight', '0');
            $private.iframe.setAttribute('frameborder', '0');
            $private.iframe.setAttribute('scrolling', 'no');
            $public.logs.debug('Foi configurado um iframe com url ' + $private.url.getUrl());
        }
    }; 
}
function Main () {
    var $private = {};
    var $public = this;
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('cliques-display');
    $private.typeValidator = new TypeValidator();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CliquesDisplay');
    $private.adClient = 'http://adclient.siga.uol.com.br/';
    $public.init = function (config) {
        $private.iframe = new Iframe();
        $public.appendedAd = false;
        $public.waitForSelector(function (container) {
            $private.trackManager.trackSuccess('Execucoes Iniciadas');
            $private.iframe.setIframe(config);
            var adBlock = $private.iframe.getIframe();
            if (adBlock) {
                $private.appendAdBlock(container, adBlock);
            }
        }, config.querySelector, 100, 7000);
    };
    $public.waitForSelector = function (callback, selector, timeout, limit) {
        timeout = timeout || 100;
        limit   = limit   || 1000;
        if (!selector) {
            return;
        }
        var waitInterval = setInterval(function () {
            var container;
            try {
                container = document.querySelector(selector);
            } catch (e) {}
            if (container) {
                callback(container);
                clearInterval(waitInterval);
            }
            if (!limit) {
                $private.trackError('timeout', 'O tempo de limite da operação foi atingido');
                clearInterval(waitInterval);
            }
            limit = limit - timeout;
        }, timeout);
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.appendAdBlock = function (container, adBlock) {
        if ($private.isValidContainer(container) && adBlock) {
            container.appendChild(adBlock);
            $public.logs.debug('Exibindo anúncio de tamanho ' + adBlock.width + 'x' + adBlock.height);
            $public.appendedAd = true;
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        }
    };
    $private.isValidContainer = function (container) {
        if (!container) {
            return false;
        }
        if ($private.containerHasAdBlocks(container)) {
            $private.trackError('anuncio_populado', 'O elemento já foi populado');
            return false;
        }
        return true;
    };
    $private.containerHasAdBlocks = function (container) {
        var children = container.childNodes;
        for (var i = 0, length = container.childNodes.length; i < length; i++) {
            var kid = container.childNodes[i];
            if (kid.src.indexOf($private.adClient) !== -1) {
                return true;
            }
        }
        return false;
    };
}
var nameSpace = new NameSpace('CliquesDisplay');
var cliquesDisplay = new Main();
nameSpace.init = cliquesDisplay.init;
})(window, document);
(function (window, document, undefined){
'use strict';
function IframeCreator () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DNA');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dna');
    $private.src = '//tm.uol.com.br/uoldna.html';
    $public.init = function() {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if(!$private.isIframeInPage()) {
            $public.setIframe();
            $private.appendIframe();
            return true;
        }
        return false;
    };
    $private.isIframeInPage = function() {
        if (document.querySelector('[src*="' + $private.src + '"]')) {
            $private.trackError('duplicate-call', 'Iframe with the same src already exists');
            return true;
        }
        return false;
    };
    $private.trackError = function (errorType, errorMessage) {
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $public.getIframe = function () {
        return $private.iframe;
    };
    $public.setIframe = function () {
        $private.iframe = document.createElement("iframe");
        $private.iframe.style.display = 'none';
        $private.iframe.setAttribute("src", 'https:' + $private.src);
    };
    $private.appendIframe = function () {
        if (!document.body) {
            setTimeout($private.appendIframe, 100);
        } else {
            document.body.appendChild($private.iframe);
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        }
    };
}
var nameSpace = new NameSpace('DNA');
var iframeCreator = new IframeCreator();
var config = nameSpace.config;
nameSpace.init = iframeCreator.init;
})(window, document);
(function (window, document, undefined){
'use strict';
function CookieUtils() {
    var $public = this;
    var $private = {};
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.TailtargetTrack');
    $private.typeValidator = new TypeValidator();
    $public.isCookieName = function(cookieName) {
        if (!$private.typeValidator.isString(cookieName)) {
            $public.logs.warn('O parâmetro deve ser uma string.');
            return false;
        }
        if (cookieName.length === 0 || cookieName == ' ') {
            $public.logs.warn('O parâmetro não pode ser vazio.');
            return false;
        }
        if(cookieName.indexOf(' ') !== -1) {
            $public.logs.warn('O parâmetro não pode conter espaços.');
            return false;
        }
        return true;
    };
    $public.getCookie = function(cookieName) {
        if (!$public.isCookieName(cookieName)) {
            return;
        }
        var cookie = document.cookie;
        var startIndex = cookie.indexOf(cookieName);
        var endIndex;
        if (startIndex === -1) {
            return;
        }
        startIndex += cookieName.length + 1;
        endIndex = cookie.indexOf(';', startIndex);
        endIndex = (endIndex !== -1) ? endIndex : cookie.length;
        return unescape(cookie.substring(startIndex, endIndex));
    };
}
function DataLayerPersistence () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.remoteStorage = new RemoteStorage();
    $private.updateLocalStorage = function () {
        var localData = $private.getLocalStorageData($private.STORAGE_NAME);
        $private.remoteStorage.get($private.STORAGE_NAME, function (key, value) {
            try {
                value = JSON.parse(value);
                if (!value && !localData) {
                    return;
                }
                value = $private.replaceAttributes(value, localData);
                if (!$private.hasAttributes(value)) {
                    return;
                }
                value = JSON.stringify(value);
                $private.remoteStorage.set(key, value, function () {
                    $private.setLocalStorageData(key, value);
                }); 
            } catch (e) {}
        });
    };
    $private.getLocalStorageData = function (key) {
        try {
             return JSON.parse(window.localStorage.getItem(key));
        } catch(e) {}
    };
    $private.setLocalStorageData = function (key, data) {
        if (!data) {
            return;
        }
        try {
            window.localStorage.setItem(key, data);
        } catch (e) {}
    };
    $private.replaceAttributes = function (obj1, obj2) {
        obj1 = obj1 || {};
        if (!obj2) {
            return;
        }
        for (var key in obj2) {
            obj1[key] = obj2[key];
        }
        return obj1;
    };
    $private.hasAttributes = function (obj) {
        if (!$private.typeValidator.isObject(obj)) {
            return false;
        }
        for (var key in obj) {
            return true;
        }
        return false;
    };
    $public.__constructor = (function (argument) {
        $private.STORAGE_NAME = 'uolDataLayer';
        $private.dataLayer = {};
        $private.updateLocalStorage(); 
    })();  
    $public.getNameSpace = function (nameSpace) {
        return $private.nameSpace;
    };
    $public.setNameSpace = function (nameSpace) {
        if ($private.typeValidator.isString(nameSpace)) {
            $private.nameSpace = nameSpace;
            $private.dataLayer[nameSpace] = {};
        }
    };
    $public.addAttribute = function (key, value) {
        if (!$private.typeValidator.isDefined($private.nameSpace)) {
            return false;
        }
        if (!$private.typeValidator.isString(key)) {
            return false;
        }
        value = $private.formatValue(value);
        if (!$private.typeValidator.isString(value)) {
            return false;
        }
        $private.storeKeyValue($private.formatKey(key), value);
        return true;
    };
    $private.formatValue = function (value) {
        if ($private.typeValidator.isArray(value)) {
            return value.join(',');
        }
        return value;
    };
    $private.formatKey = function (key) {
        if (key === 'getProfiles') {
            return 'tt_cluster';
        }
        return key.replace('get', 'tt_').toLowerCase();
    };
    $private.storeKeyValue = function (key, value) {
        $private.dataLayer[$private.nameSpace][key] = value;
    };
    $public.persist = function () { 
        var dataFromAttributes = $private.dataLayer[$private.nameSpace];
        if (!$private.hasAttributes(dataFromAttributes)) {
            return;
        }
        var persistedDataLayer = $private.getLocalStorageData('uolDataLayer') || {};
        persistedDataLayer[$private.nameSpace] = dataFromAttributes;
        $private.setLocalStorageData('uolDataLayer', JSON.stringify(persistedDataLayer));
        $private.updateLocalStorage();
    };  
}
function Identify () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $private.trackManager = new TrackManager();
    $private.cookieUtils = new CookieUtils();
    $private.remoteStorage = new RemoteStorage();
    $private.typeValidator = new TypeValidator();
    $private.dataLayerPersistence = new DataLayerPersistence();
    $public.__constructor = (function () {
        $public.logs.setPrefix('UOLPD.TagManager.TailtargetTrack');
        $private.trackManager.setModuleName('tailtarget-track');
        $private.storageName = 'tailtarget';
        $private.urls = [
            '//jsuol.com.br/p/dmp/tt/profiles.js',
            '//tags.t.tailtarget.com/t3m.js?i=TT-10162-1/CT-10'
        ];
        $private.dataLayerPersistence.setNameSpace('TailtargetTrack');
    })();
    $public.init = function () {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if ($public.isOptOutEnabled()) {
            $private.clearData();
            $private.trackManager.trackSuccess('opt-out-enabled');
            return false;
        }
        $public.persistData();
        if ($private.hasScriptsInPage()) {
            $private.trackManager.trackError('anuncio_populado', 'fluxo_interrompido');
            return false;
        }
        $private.enableServices();
        $private.createScripts();
        $public.updateLocalStorage();
        $private.trackManager.trackSuccess('Execucoes Finalizadas');
        return true;
    };
    $public.isOptOutEnabled = function () {
        var cookieName = 'BTOPTOUT';
        return ($private.cookieUtils.getCookie(cookieName) === '1');
    };
    $public.persistData = function () {
        $private.runWhenProfilesExist($private.persistAttributes);
    };
    $public.updateLocalStorage = function () {
        if (!$private.hasLocalData()) {
            $private.remoteStorage.get($private.storageName, function (key, value) {
                if (!value) {
                    return;
                }
                try {
                    window.localStorage.setItem(key, value);
                } catch (e) {}
            });
        }
        $private.syncTailtargetData();
    };
    $private.clearData = function () {
        try {
            window.localStorage.removeItem($private.storageName);
        } catch (e) {}
        $private.remoteStorage.removeItem($private.storageName);
    };
    $private.runWhenProfilesExist = function (fn) {
        var times = 0;
        if (!$private.typeValidator.isFunction(fn)) {
            return;
        }
        var ttProfilesInterval = window.setInterval(function () {
            times++;
            var profiles = $private.getValidProfiles(window._ttprofiles);
            if ($private.typeValidator.isDefined(profiles) || times >= 210) {
                fn(profiles);
                clearInterval(ttProfilesInterval);
            }
        }, 150);
    };
    $private.persistAttributes = function (profiles) {
        for (var key in profiles) {
            $private.dataLayerPersistence.addAttribute(key, profiles[key]);
        }
        $private.dataLayerPersistence.persist();
    };
    $private.getValidProfiles = function (ttprofiles) {
        var tailtarget = {};
        var length = 0;
        if (!ttprofiles) {
            return;
        }
        for (var key in ttprofiles) {
            if (!$private.isValidKey(key)) {
                continue;
            }
            if (!ttprofiles[key].length) {
                continue;
            }
            tailtarget[key] = ttprofiles[key];
            length++;
        }
        if (length === 0) {
            return;
        }
        return tailtarget;
    };
    $private.isValidKey = function (key) {
        var validKeys = ['getAge', 'getGender', 'getProfiles', 'getSubjects', 'getTeam', 'getSocialClass', 'getMicrosegments', 'getLists', 'getCustomAudience'];
        for (var i = 0; i < validKeys.length; i++) {
            if (key == validKeys[i]) {
                return true;
            }
        }
        return false;
    };
    $private.hasScriptsInPage = function () {
        for (var i = 0, length = $private.urls.length; i < length; i++) {
            if ($private.isScriptInPage($private.urls[i])) {
                return true;
            }
        }
        return false;
    };
    $private.isScriptInPage = function (src) {
        var scripts = document.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; i++) {
            if (src == scripts[i].getAttribute('src')) {
                return true;
            }
        }
        return false;
    };
    $private.enableServices = function () {
        window._ttprofiles = window._ttprofiles || [];
        var profiles = [
            ["_setAccount", "TT-10162-1"],
            ['_sync', false],
            ['_enableServices']
        ];
        for (var i = 0; i < profiles.length; i++) {
            try {
                window._ttprofiles.push(profiles[i]);
            } catch (e) {}
        }
    };
    $private.createScripts = function () {
        var elements = document.getElementsByTagName('script');
        var parent = elements[elements.length - 1].parentNode;
        for (var i = 0; i < $private.urls.length; i++) {
            var script = document.createElement('script');
            script.setAttribute('src', $private.urls[i]);
            parent.appendChild(script);
        }
    };
    $private.hasLocalData = function () {
        var item;
        try {
            item = window.localStorage.getItem($private.storageName);
        } catch (e) {}
        return $private.typeValidator.isString(item);
    };
    $private.syncTailtargetData = function () {
        var times = 0;
        var ttProfilesInterval = window.setInterval(function () {
            times++;
            var profiles = $private.getValidProfiles(window._ttprofiles);
            if ($private.typeValidator.isDefined(profiles) || times >= 210) {
                window.clearInterval(ttProfilesInterval);
                $private.storeTailtargetInfo(profiles);
            }
        }, 150);
    };
    $private.storeTailtargetInfo = function (tailtarget) {
        if (!tailtarget) {
            return;
        }
        try {
            tailtarget = JSON.stringify(tailtarget);
            window.localStorage.setItem($private.storageName, tailtarget);
        } catch (e) {}
        $private.remoteStorage.set($private.storageName, tailtarget, $private.setTailtargetInRemoteStorage);
    };
    $private.setTailtargetInRemoteStorage = function (key, value) {
        try {
            value = JSON.parse(value);
            $public.logs.log('O valor ' + value + ' foi adicionado a chave "' + value + '"');
        } catch (err) {
            $private.trackManager.trackError('json_parse', 'fluxo_interrompido');
            $public.logs.warn('Ocorreu um erro ao armazenar dados');
        }
    };
}
var nameSpace = new NameSpace('TailtargetTrack');
var identify = new Identify();
nameSpace.RemoteStorage = new RemoteStorage();
nameSpace.init = identify.init;
})(window, document);
(function (window, document, undefined){
'use strict';
function CookieUtils () {
    var $public = this;
    var $private = {};
    $private.validator = new TypeValidator();
    $public.getItem = function (name) {
        if (!$private.validator.isString(name)) {
            return '';
        }
        var cookie = $private.getCookies();
        var startIndex = cookie.indexOf(name + '=');
        if (startIndex === -1) {
            return '';
        }
        var middleIndex = cookie.indexOf('=', startIndex) + 1;
        var endIndex = cookie.indexOf(';', middleIndex);
        if (endIndex === -1) {
            endIndex = cookie.length;
        }
        return unescape(cookie.substring(middleIndex, endIndex));
    };
    $private.getCookies = function () {
        return document.cookie;
    };
    $public.setItem = function (name, value) {
        if (!$private.validator.isString(name)) {
            return;
        }
        if (!$private.validator.isString(value)) {
            return;
        }
        $private.setCookie(name + "=" + value);
    };
    $private.setCookie = function (value) {
        document.cookie = value;
    };
}
function BehaviouralTargeting () {
    var $private = {};
    var $public = this;
    $private.validator = new TypeValidator();
    $private.cookieUtils = new CookieUtils();
    $private.remoteStorage = new RemoteStorage();
    $private.clients = [];
    $private.remoteStorage.get('dynad_rt', function (key, value) { 
        try {
            window.localStorage.setItem(key, value);
            $private.setClients(JSON.parse(value));
        } catch (e) {}
    });
    $private.setClients = function (clients) {
        var aux = [];
        for (var i = 0, length = clients.length; i < length; i++) {
            if (clients[i].id) {
                aux.push(clients[i].id);
            }
        }
        $private.clients = aux;
    };
    $public.getBTCookies = function () {
        var rtCodes = $private.getMergeCookies();
        var dntTrack = $public.getDoNotTrack();
        rtCodes.push(dntTrack);
        return rtCodes;
    };
    $public.getDoNotTrack = function () {
        var dntCode = 9000;
        if (!navigator.doNotTrack && !window.doNotTrack && !navigator.MsDoNotTrack) {
            dntCode = 9000;
            return dntCode;
        }
        if (navigator.doNotTrack === '1' || window.doNotTrack === '1' || navigator.MsDoNotTrack === '1') {
            dntCode = 9001;
            return dntCode;
        }
        if (navigator.doNotTrack === 'yes') {
            dntCode = 9003;
            return dntCode;
        }
        return dntCode;
    };
    $private.getMergeCookies = function () {
        var BTCookie = $private.getClientsArray($private.cookieUtils.getItem('BT'));
        var DECookie = $private.getClientsArray($private.cookieUtils.getItem('DEretargeting'));
        var retargeting = $private.mergeCookies(BTCookie, DECookie);
        try {
            $private.setClients(JSON.parse(window.localStorage.getItem('dynad_rt')));
        } catch (e) {}
        retargeting = $private.mergeCookies(retargeting, $private.clients);
        if (!$private.validator.isArray(retargeting)) {
            return [];
        }
        return retargeting;
    };
    $private.getClientsArray = function (cookie) {
        if (!$private.validator.isString(cookie)) {
            return [];
        }
        if (cookie.length === 0) {
            return [];
        }
        var retargeting = cookie.split(';').sort();
        for (var i = 0; i < retargeting.length; i++) {
            var arr = retargeting[0];
            if (arr === '') {
                retargeting.splice(0, 1);
            }
        }
        return retargeting;
    };
    $private.mergeCookies = function (deRetar, bt) {
        var retargeting = deRetar.concat(bt).sort();
        for (var i = 0; i < retargeting.length; i++) {
            if (retargeting[i] === retargeting[i + 1]) {
                retargeting.splice(i, 1);
            }
        }
        return retargeting;
    };
}
function ScriptUtils () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.hasTagScript = function (src) {
        if (!$private.typeValidator.isString(src)) {
            return false;
        }
        if (src.length === 0) {
            return false;
        }
        src = $private.removeProtocol(src);
        var scripts = document.getElementsByTagName('script');
        var length = scripts.length;
        for (var i = 0; i < length; i++) {
            var scriptSrc = scripts[i].getAttribute('src');
            if (scriptSrc && $private.isSrcEqual(scriptSrc, src)) {
                return true;
            }
        }
        return false;
    };
    $private.removeProtocol = function (value) {
        return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/, '');
    };
    $private.isSrcEqual = function (src1, src2) {
        return (src1.indexOf(src2) > -1);
    };
    $public.createAsyncScript = function (src) {
        if(!$private.typeValidator.isString(src)) {
            return;
        }
        var tag = document.createElement('script');
        tag.setAttribute('src', src);
        tag.async = true;
        return tag;
    };
    $public.appendScript = function (script) {
        if (script instanceof HTMLScriptElement) {
            $private.lastScriptsParent().appendChild(script);
            return true;
        }
        return false;
    };
    $private.lastScriptsParent = function () {
        return document.getElementsByTagName('script')[0].parentNode;
    };
}
function Rubicon () {
    var $private = {};
    var $public = this;
    $private.validator = new TypeValidator();
    $public.setRtpBanner = function (bannerId) {
        if (!$private.hasRtp(bannerId)) {
            return;
        }
        $private.bannerId = bannerId;
        $private.rtpBanner = window.rtp[bannerId];
    };
    $private.hasRtp = function (bannerId) {
        if (!$private.validator.isString(bannerId)) {
            return false;
        }
        if (!$private.validator.isObject(window.rtp)) {
            return false;
        }
        if (!$private.validator.isObject(window.rtp[bannerId])) {
            return false;
        }
        return true;
    };
    $public.getDeals = function () {
        if (!$private.rtpBanner) {
            return;
        }
        if (!$private.rtpBanner.pmp) {
            return;
        }
        if (!$private.validator.isArray($private.rtpBanner.pmp.deals)) {
            return;
        }
        if ($private.rtpBanner.pmp.deals.length === 0) {
            return;
        }
        return $private.rtpBanner.pmp.deals.join(', ');
    };
    $public.getCpm = function (bannerId) {
        if (!$private.hasValidEstimate()) {
            return;
        }
        if (!$private.validator.isNumber($private.rtpBanner.estimate.cpm)) {
            return;
        }
        return $private.rtpBanner.estimate.cpm;
    };
    $private.hasValidEstimate = function () {
        if (!$private.rtpBanner) {
            return false;
        }
        if (!$private.estimate) {
            return false;
        }
        if (!$private.validator.isObject(rtpBanner.estimate)) {
            return;
        }
        return true;
    };
    $public.getTier = function () {
        if (!$private.hasValidEstimate()) {
            return;
        }
        if(!$private.validator.isString($private.rtpBanner.estimate.tier)) {
            return;
        }
        return $private.rtpBanner.estimate.tier;
    };
}
function Sesame() {
    var $private = {};
    var $public = this;
    $private.scriptUtils = new ScriptUtils();
    $private.googleTopUrl = '//pagead2.googlesyndication.com/pagead/js/google_top.js';
    $public.createTagScript = function() {
        var script = document.createElement('script');
        script.setAttribute('src', $private.googleTopUrl);
        return script;
    };
    $public.createIframe = function(){
        var iframe = document.createElement('iframe');
        iframe.setAttribute('data-video-ads', 'true');
        iframe.setAttribute('id' , 'google_top');
        iframe.setAttribute('name' , 'google_top');
        iframe.setAttribute('style' , 'display:none;');
        return iframe;
    };
    $public.appendElement = function(element) {
        if(!element) {
            return;
        }
        if(!$private.hasIframe('iframe[id=google_top]')) {
            document.body.appendChild(element);
        }
    };
    $private.hasIframe = function(query) {
        var result = document.querySelectorAll(query);
        if (result.length === 1) {
            return true;
        }
        return false;
    };
    $public.track = function() {
        if (!$private.scriptUtils.hasTagScript($private.googleTopUrl)) {
            $private.scriptUtils.appendScript($public.createTagScript());
        }
        $public.appendElement($public.createIframe());
    };
}
function DfpPath () {
    var $public = this;
    var $private = {};
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DfpAsync');
    $private.typeValidator = new TypeValidator();
    $public.getForcePath = function () {
        return $private.forcePath;
    };
    $public.setForcePath = function (forcePath) {
        if ($private.typeValidator.isArray(forcePath) && forcePath[0] === 'forcePath') {
            $private.forcePath = true;
        }
    };
    $public.setDfpPath = function (dfpPath) {
        if (!$private.typeValidator.isString(dfpPath)) {
            return;
        }
        if ($private.forcePath) {
            $private.dfpPath = dfpPath;
        } else {
            $private.dfpPath = $private.getPathElementByPriority($private.trimDfpPathSlashes(dfpPath));
        }
    };
    $private.trimDfpPathSlashes = function (dfpPath) {
        if (dfpPath.substring(0, 1) === '/') {
            dfpPath = dfpPath.substring(1, dfpPath.length);
        }
        if (dfpPath.substring(dfpPath.length - 1, dfpPath.length) === '/') {
            dfpPath = dfpPath.substring(0, dfpPath.length - 1);
        }
        return dfpPath.split('/');
    };
    $private.getPathElementByPriority = function (path) {
        $public.setNetwork(path[0]);
        var network = $public.getNetwork();
        if (!network) {
            return path;
        }
        path[0] = network;
        $public.setSite(path[1]);
        var site = $public.getSite();
        if (!site) {
            return path.join('/');
        }
        path[1] = site;
        $public.setChan(path[2]);
        var chan = $public.getChan();
        if (!chan) {
            return path.join('/');
        }
        path[2] = chan;
        $public.setSubChan(path[3]);
        var subchan = $public.getSubChan();
        if (!subchan) {
            return path.join('/');
        }
        path[3] = subchan;
        return path.join('/');
    };
    $public.setNetwork = function (network) {
        if ($private.typeValidator.isString(network)) {
            $private.network = network;
        }
    };
    $public.getNetwork = function () {
        return $private.network;
    };
    $public.setSite = function (site) {
        if ($private.isValidSite()) {
            $private.site = window.universal_variable.dfp.custom_params.site;
        }
        if ($private.typeValidator.isString(site)) {
            $private.site = site;
        } else {
            $public.logs.warn('O parâmetro "site" não é uma string.');
        }
    };
    $private.isValidSite = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }
        var site = window.universal_variable.dfp.custom_params.site;
        if (!$private.typeValidator.isString(site)) {
            return false;
        }
        return true;
    };
    $public.getSite = function (site) {
        return $private.site;
    };
    $public.setChan = function (chan) {
        if ($private.isValidChan()) {
            $private.chan = window.universal_variable.dfp.custom_params.chan;
        }
        if ($private.typeValidator.isString(chan)) {
            $private.chan = chan;
        } else {
            $public.logs.warn('O parâmetro "chan" não é uma string.');
        }
    };
    $private.isValidChan = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }
        var chan = window.universal_variable.dfp.custom_params.chan;
        if (!$private.typeValidator.isString(chan)) {
            return false;
        }
        return true;
    };
    $public.getChan = function () {
        return $private.chan;
    };
    $public.setSubChan = function (subchan) {
        if ($private.isValidSubchan()) {
            $private.subChan = window.universal_variable.dfp.custom_params.subchan;
        }
        if ($private.typeValidator.isString(subchan)) {
            $private.subChan = subchan;
        }
    };
    $private.isValidSubchan = function () {
        if (!$private.isValidCustomParams()) {
            return false;
        }
        var subchan = window.universal_variable.dfp.custom_params.subchan;
        if (!$private.typeValidator.isString(subchan)) {
            $public.logs.warn('O parâmetro subchan não é uma string.');
            return false;
        }
        return true;
    };
    $private.isValidCustomParams = function () {
        var universalVariable = window.universal_variable;
        if (!$private.typeValidator.isDefined(universalVariable)) {
            return false;
        }
        if (!$private.typeValidator.isObject(universalVariable)) {
            $public.logs.warn('A variável universal não é um objeto.');
            return false;
        }
        if (!$private.typeValidator.isObject(universalVariable.dfp)) {
            $public.logs.warn('O parâmetro "dfp" da variável universal não é um objeto.');
            return false;
        }
        if (!$private.typeValidator.isObject(universalVariable.dfp.custom_params)) {
            $public.logs.warn('O parâmetro "custom_params", do parâmetro "dfp", da variável universal, não é um objeto.');
            return false;
        }
        return true;
    };
    $public.getSubChan = function () {
        return $private.subChan;
    };
    $public.getDfpPath = function () {
        return $private.dfpPath;
    };
}
function DfpApi () {
    var $private = {};
    var $public = this;
    $public.scriptUtils = new ScriptUtils();
    $private.validator = new TypeValidator();
    $private.behaviouralTargeting = new BehaviouralTargeting();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-async');
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DfpAsync');
    $public.appendGPT = function () {
        var gptUrl = '//www.googletagservices.com/tag/js/gpt.js';
        if ($public.scriptUtils.hasTagScript(gptUrl)) {
            return;
        }
        var script = $public.scriptUtils.createAsyncScript(gptUrl);
        $public.scriptUtils.appendScript(script);
        return script;
    };
    $public.createNameSpace = function () {
        window.googletag = window.googletag || {};
        window.googletag.cmd = window.googletag.cmd || [];
    };
    $public.onDfpReady = function (callback, timeout) {
        timeout = timeout || 50;
        if (!$private.validator.isFunction(callback)) {
            return false;
        }
        if ($private.isGoogleTagReady()) {
            callback();
            return true;
        }
        setTimeout(function() {
            $public.onDfpReady(callback, timeout);
        }, timeout);
    };
    $public.setTargetings = function (slot, targetings) {
        if (!slot) {
            return;
        }
        if (!$private.validator.isArray(targetings)) {
            return;
        }
        for (var i = 0, length = targetings.length; i < length; i++) {
            var targeting = targetings[i];
            try {
                slot.setTargeting(targeting.key, targeting.value);
            } catch (e) {
                continue;
            }
        }
        return slot;
    };
    $public.getCampaignuol = function (campaignuol) {
        if (!$private.validator.isString(campaignuol)) {
            return;
        }
        return campaignuol;
    };
    $public.getExpandable = function (expble) {
        if (expble !== '0' && expble !== '1') {
            return;
        }
        return expble;
    };
    $public.getGroup = function (group) {
        if (!$private.validator.isString(group)) {
            return;
        }
        return group;
    };
    $public.getKeyword = function (keyword) {
        if (!$private.validator.isString(keyword)) {
            keyword = $private.getKeywordFromUniversalVariable();
        }
        if (!$private.validator.isString(keyword)) {
            return;
        }
        return keyword;
    };
    $private.getKeywordFromUniversalVariable = function () {
        if (!$private.isValidCustomParams()) {
            return;
        }
        return window.universal_variable.dfp.custom_params.keyword;
    };
    $private.isValidCustomParams = function () {
        var universalVariable = window.universal_variable;
        if (!$private.validator.isDefined(universalVariable)) {
            return false;
        }
        if (!$private.validator.isObject(universalVariable)) {
            $public.logs.warn('A variável universal não é um objeto.');
            return false;
        }
        if (!$private.validator.isObject(universalVariable.dfp)) {
            $public.logs.warn('O parâmetro "dfp" da variável universal não é um objeto.');
            return false;
        }
        if (!$private.validator.isObject(universalVariable.dfp.custom_params)) {
            $public.logs.warn('O parâmetro "custom_params", do parâmetro "dfp", da variável universal, não é um objeto.');
            return false;
        }
        return true;
    };
    $public.hasTrackedPubads = function () {
        return $private.hasTrackedPubads;
    };
    $public.setPubadsTracking = function (targetings) {
        if (!$private.validator.isArray(targetings)) {
            return;
        }
        $private.hasTrackedPubads = true;
        for (var i = 0, length = targetings.length; i < length; i++) {
            var targeting = targetings[i];
            try {
                window.googletag.pubads().setTargeting(targeting.key, targeting.value);
            } catch (e) {
                continue;
            }
        }
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.validator.isString(errorType)) {
            return;
        }
        if (!$private.validator.isString(errorMessage)) {
            return;
        }
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $public.defineSlot = function (path, id, width, height, outOfPage) {
        if (!$private.isValidTarget(path, id)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }
        if (outOfPage === true) {
            return window.googletag.defineOutOfPageSlot(path, id);
        }
        if (!$private.isValidSize(width, height)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }
        try {
            return window.googletag.defineSlot(path, [parseInt(width), parseInt(height)], id);
        } catch (e) {
            return;
        }
    };
    $private.isValidTarget = function (path, id) {
        if (!$private.validator.isString(path)) {
            return false;
        }
        if (!$private.validator.isString(id)) {
            return false;
        }
        return true;
    };
    $private.isValidSize = function (width, height) {
        if (!$private.validator.isNumericString(width)) {
            return false;
        }
        if (!$private.validator.isNumericString(height)) {
            return false;
        }
        return true;
    };
    $public.defineFluid = function (path, id, outOfPage) {
        if (!$private.isValidTarget(path, id)) {
            $private.trackError('slot_invalido', 'O objeto slot esta invalido e não foi criado');
            return;
        }
        if (outOfPage === true) {
            return window.googletag.defineOutOfPageSlot(path, id);
        }
        try {
            return window.googletag.defineSlot(path, 'fluid', id);
        } catch (e) {
            return;
        }
    };
    $public.setCompanionAds = function(slot, isCompanion) {
        if (!slot) {
            return;
        }
        if (!isCompanion) {
            return slot;
        }
        try {
            return slot.addService(window.googletag.companionAds());
        } catch (e) {
            return;
        }
    };
    $public.addServices = function (slot) {
        if (!slot) {
            return;
        }
        if (!slot.addService) {
            return;
        }
        try {
            return slot.addService(window.googletag.pubads());
        } catch (e) {
            return;
        }
    };
    $public.enableAsyncRendering = function () {
        if(!$private.isGoogleTagReady()) {
            return false;
        }
        return window.googletag.pubads().enableAsyncRendering();
    };
    $private.isGoogleTagReady = function () {
        if (!window.googletag) {
            return false;
        }
        if (!window.googletag.apiReady) {
            return false;
        }
        return true;
    };
    $public.displayAd = function (bannerId) {
        if (!$private.validator.isString(bannerId)) {
            return false;
        }
        $private.enableServices();
        try {
            window.googletag.cmd.push(function () {
                googletag.display(bannerId);
            });
            return true;
        } catch (e) {
            return false;
        }
    };
    $private.enableServices = function () {
        if (!$public.servicesEnabled) {
            try {
                window.googletag.enableServices();
                $public.servicesEnabled = true;
            } catch (e){}
        }
    };
    $public.refreshAds = function (slotRefresh) {
        var slots;
        if (!$private.isValidRefresh()) {
            return;
        }
        if ($private.isValidSlotRefresh(slotRefresh)) {
            slots =  slotRefresh;
        }
        if (slotRefresh instanceof Object && !$private.validator.isArray(slotRefresh)) {
            slots = [];
            slots.push(slotRefresh);
        }
        if (!slotRefresh) {
            return false;
        }
        try {
            return window.googletag.pubads().refresh(slots);
        } catch (e) {
            return;
        }
    };
    $private.isValidRefresh = function () {
        if (!window.googletag) {
            return false;
        }
        if (!window.googletag.pubads) {
            return false;
        }
        if (!window.googletag.pubads().refresh) {
            return false;
        }
        return true;
    };
    $private.isValidSlotRefresh = function (slotRefresh) {
        if (!$private.validator.isArray(slotRefresh) || !slotRefresh.length) {
            return false;
        }
        for (var i = 0; i < slotRefresh.length; i++) {
            if (!(slotRefresh[i] instanceof Object)) {
                return false;
            }
        }
        return true;
    };
    $public.setSizeMappings = function (slot, sizeMappings) {
        if (!slot) {
            return;
        }
        if (!$private.validator.isArray(sizeMappings)) {
            return slot;
        }
        var length = sizeMappings.length;
        if (length === 0) {
            return slot;
        }
        var mapping = googletag.sizeMapping();
        for (var i = 0; i < length; i++) {
            var sizeMapping = sizeMappings[i];
            mapping = mapping.addSize(sizeMapping.viewportSize, sizeMapping.slotSize);
        }
        try {
            mapping = mapping.build();
            slot = slot.defineSizeMapping(mapping);
        } catch (e) {}
        return slot;
    };
}
function Rotative () {
    var $public = this;
    var $private = {};
    $private.validator = new TypeValidator();
    $private.dfpApi = new DfpApi();
    $private.divs = [];
    $public.setAdsLength = function (adsLength) {
        if ($private.validator.isInt(Number(adsLength))) {
            $private.adsLength = parseInt(adsLength);
        }
    };
    $public.getAdsLength = function () {
        return $private.adsLength;
    };
    $public.setCadence = function (cadence) {
        if ($private.validator.isInt(Number(cadence))) {
            $private.cadence = parseInt(cadence);
        }
    };
    $public.getCadence = function () {
        return $private.cadence;
    };
    $public.setDivs = function (bannerId) {
        if (!$private.adsLength) {
            return;
        }
        if (!bannerId) {
            return;
        }
        for (var i = 1, length = $private.adsLength; i <= length; i++) {
            var banner = $private.createDiv(bannerId + '-rotative-' + i);
            if (!banner) {
                continue;
            }
            banner.setAttribute('style', 'display: none;');
            $private.divs.push(banner);
        }
    };
    $private.createDiv = function (id) {
        if (!$private.validator.isString(id)) {
            return;
        }
        var div = document.createElement('div');
        div.setAttribute('id', id);
        return div;
    };
    $public.getDivs = function () {
        return $private.divs;
    };
}
function Slot () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DfpAsync');
    $private.cookieUtils = new CookieUtils();
    $private.rubicon = new Rubicon();
    $public.getDeals = $private.rubicon.getDeals;
    $public.getCpm = $private.rubicon.getCpm;
    $public.getTier = $private.rubicon.getTier;
    $private.raffledRate = Math.round(Math.random() * 100);
    $private.targetings = [];
    $private.rotative = new Rotative();
    $private.rotatives = [];
    $private.sizeMappingsPattern = /(\[\d+\,\s?\d+])+(\,\s?)(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\;?\s?)?/g;
    $public.setDfpPath = function (dfpPath) {
        if ($private.typeValidator.isString(dfpPath)) {
            $private.dfpPath = dfpPath;
        }
    };
    $public.getDfpPath = function () {
        return $private.dfpPath;
    };
    $public.setBannerId = function (bannerId) {
        if ($private.typeValidator.isString(bannerId)) {
            $private.bannerId = bannerId;
            $public.bannerId = bannerId;
            $private.rubicon.setRtpBanner(bannerId);
        }
    };
    $public.getBannerId = function () {
        return $private.bannerId;
    };
    $public.setWidth = function (width) {
        if ($private.typeValidator.isNumericString(width)) {
            $private.width = width;
        }
    };
    $public.getWidth = function () {
        return $private.width;
    };
    $public.setHeight = function (height) {
        if ($private.typeValidator.isNumericString(height)) {
            $private.height = height;
        }
    };
    $public.getHeight = function () {
        return $private.height;
    };
    $public.setCompanion = function (isCompanion) {
        if ($private.typeValidator.isString(isCompanion) && isCompanion.toLowerCase() === 'true') {
            $private.isCompanion = true;
        }
    };
    $public.getCompanion = function () {
        return $private.isCompanion;
    };
    $public.getOutOfPage = function () {
        return $private.outOfPage;
    };
    $public.setOutOfPage = function (outOfPage) {
        if ($private.typeValidator.isArray(outOfPage)) {
            $private.outOfPage = $private.isOutOfPage(outOfPage[0]);
        }
    };
    $private.capitalize = function (str) {
        return str.toLowerCase().replace(/(?:^|\s)\S/g, function(firstChar) {
            return firstChar.toUpperCase();
        });
    };
    $private.getOutOfPageCookieName = function () {
        return $private.outOfPageCookieName;
    };
    $private.isOutOfPage = function (outOfPage) {
        if (!$private.typeValidator.isString(outOfPage) && outOfPage !== 'outOfPage') {
            return false;
        }
        if ($public.hasCookie()) {
            return false;
        }
        return true;
    };
    $private.hasOutOfPageCookie = function () {
        if ($private.cookieUtils.getItem($private.getOutOfPageCookieName()).length > 0) {
            return true;
        }
        return false;
    };
    $public.getCookieName = function () {
        return $private.cookieName;
    };
    $public.setCookieName = function (cookieName) {
        if ($private.typeValidator.isString(cookieName)) {
            $private.cookieName = cookieName;
        }
    };
    $public.setCookieDfp = function () {
        if (!$private.cookieName) {
            return false;
        }
        if ($public.hasCookie()) {
            return false;
        }
        $private.cookieUtils.setItem($private.cookieName, '1; expires=0; path=/');
        $public.logs.debug('Configurando o cookie ' + $private.cookieName + ' com o valor 1');
        return true;
    };
    $public.hasCookie = function () {
        if (!$private.cookieName) {
            $public.logs.warn('O nome do cookie não foi configurado');
            return false;
        }
        if ($private.cookieUtils.getItem($private.cookieName).length > 0) {
            $public.logs.debug('A página contém o cookie ' + $private.cookieName);
            return true;
        }
        return false;
    };
    $private.raffle = function (samplingRate) {
        samplingRate = parseInt(samplingRate, 10);
        if (samplingRate === 0) {
            return false;
        }
        if (samplingRate === 100) {
            return true;
        }
        if ($public.getRaffledRate() <= samplingRate) {
            return true;
        }
        return false;
    };
    $public.getRaffledRate = function () {
        return $private.raffledRate;
    };
    $public.setPosition = function (position) {
        if ($private.typeValidator.isString(position)) {
            $private.position = position;
        }
    };
    $public.getPosition = function () {
        return $private.position;
    };
    $public.addTargeting = function (key, value) {
        if (!$private.typeValidator.isString(key) || !$private.typeValidator.isDefined(value)) {
            return;
        }
        if ($private.hasKey(key)) {
            return;
        }
        $private.targetings.push({
            'key': key,
            'value': value
        });
    };
    $private.hasKey = function (key) {
        var index = $private.targetings.length;
        while (index) {
            index--;
            if ($private.targetings[index].key === key) {
                return true;
            }
        }
        return false;
    };
    $public.getTargetings = function () {
        return $private.targetings;
    };
    $public.getRotative = function () {
        return $private.rotative;
    };
    $public.setRotatives = function (adsLength, cadence) {
        $private.rotative.setAdsLength(adsLength);
        $private.rotative.setCadence(cadence);
        $private.rotative.setDivs($private.bannerId);
    };
    $public.getRotatives = function () {
        return $private.rotative.getDivs();
    };
    $public.setHide = function(hide) {
        if(!$private.typeValidator.isString(hide)) {
            return;
        }
        if (hide.toLowerCase() === 'true') {
            $private.hide = true;
        }
    };
    $public.getHide = function () {
        return $private.hide;
    };
    $public.setSizeMappings = function (sizeMappings) {
        if (!$private.typeValidator.isString(sizeMappings)) {
            return;
        }
        var mappings = $private.getConvertedMappings(sizeMappings);
        if (mappings.length > 0) {
            $private.sizeMappings = mappings;
        }
    };
    $private.getConvertedMappings = function (sizeMappings) {
        var mappingsPattern = /(\[\d+\,\s?\d+])+(\,\s?)(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\;?\s?)?/g;
        var matchedMappings = sizeMappings.match(mappingsPattern);
        var mappings = [];
        if (!matchedMappings) {
            return mappings;
        }
        for (var i = 0, length = matchedMappings.length; i < length; i++) {
            var mapping = $private.getSizeMapping(matchedMappings[i]);
            if (mapping) {
                mappings.push(mapping);
            }
        }
        return mappings;
    };
    $private.getSizeMapping = function (sizeMapping) {
        if (!sizeMapping) {
            return;
        }
        if (!$private.typeValidator.isString(sizeMapping)) {
            return;
        }
        var matches = sizeMapping.match(/(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\])(\,(\s)?(\[\d+\,\s?\d+]|\[(\[\d+\,\s*\d+\](\,\s*)*)+\]))+/);
        if (!matches || !matches[1] || !matches[6]) {
            return;
        }
        try {
            return {
                'viewportSize': JSON.parse(matches[1]),
                'slotSize': JSON.parse(matches[6])
            };
        } catch (e) {
            return;
        }
    };
    $public.getSizeMappings = function () {
        return $private.sizeMappings;
    };
    $public.setIncremental = function(incremental) {
        if(!$private.typeValidator.isString(incremental)) {
            return;
        }
        if (incremental.toLowerCase() === 'true') {
            $private.incremental = true;
        }
    };
    $public.getIncremental = function () {
        return $private.incremental;
    };
    $public.setFluid = function (fluid) {
        if ($private.typeValidator.isBoolean(fluid)){
            $private.fluid = true;
            return;
        }
        if (!$private.typeValidator.isArray(fluid)) {
            return;
        }
        if ($private.typeValidator.isString(fluid[0]) && fluid[0].toLowerCase() === 'true') {
            $private.fluid = true;
        }
    };
    $public.getFluid = function () {
        return $private.fluid;
    };
}
function DomUtils () {
    var $private = {};
    var $public = this;
    $private.validator = new TypeValidator();
    $public.divHasLoaded = function (id, callback, timeout) {
        timeout = timeout || 50;
        if (!$private.validator.isFunction(callback)) {
            return false;
        }
        var div = $public.getDiv(id);
        if (div) {
            callback();
            return true;
        }
        setTimeout(function() {
            if (!$public.loaderLimit) {
                $public.divHasLoaded(id, callback, timeout);
            }
        }, timeout);
    };
    $public.getDiv = function (bannerId) {
        if (!$private.validator.isString(bannerId)) {
            return;
        }
        return document.getElementById(bannerId);
    };
}
function Ad () {
    var $public = this;
    var $private = {};
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-async');
    $private.typeValidator = new TypeValidator();
    $private.DfpPath = new DfpPath();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DfpAsync');
    $public.setPubadsTargeting = function (pubadsTargeting) {
        $private.pubadsTargeting = pubadsTargeting;
    };
    $public.getPubadsTargeting = function () {
        return $private.pubadsTargeting;
    };
    $public.setSlot = function (bannerId, width, height, isCompanion, outOfPage, pos, rotative, rotativeInterval, hide, sizeMappings, incremental, fluid) {
        var slot = new Slot();
        slot.setBannerId(bannerId);
        $private.bannerId = bannerId;
        slot.setWidth(width);
        $private.width = width;
        slot.setHeight(height);
        $private.height = height;
        slot.setCompanion(isCompanion);
        $private.isCompanion = isCompanion;
        slot.setOutOfPage(outOfPage);
        $private.outOfPage = outOfPage;
        slot.setPosition(pos);
        $private.pos = pos;
        slot.setRotatives(rotative, rotativeInterval);
        $private.rotative = rotative;
        $private.rotativeInterval = rotativeInterval;
        slot.setHide(hide);
        $private.hide = hide;
        slot.setSizeMappings(sizeMappings);
        $private.sizeMappings = sizeMappings;
        slot.setIncremental(incremental);
        $private.incremental = incremental;
        slot.setFluid(fluid);
        $private.slot = slot;
    };
    $public.getDfpPath = function () {
        return $private.DfpPath.getDfpPath();
    };
    $public.setDfpPath = function (dfpPath) {
        $private.DfpPath.setDfpPath(dfpPath);
        $private.slot.setDfpPath($public.getDfpPath());
    };
    $public.getForcePath = function () {
        return $private.forcePath;
    };
    $public.setForcePath = function (forcePath) {
        $private.forcePath = forcePath;
        $private.DfpPath.setForcePath(forcePath);
    };
    $public.getBannerId = function () {
        return $private.bannerId;
    };
    $public.getWidth = function () {
        return $private.width;
    };
    $public.getHeight = function () {
        return $private.height;
    };
    $public.getIsCompanion = function () {
        return $private.isCompanion;
    };
    $public.getOutOfPage = function () {
        return $private.outOfPage;
    };
    $public.getPos = function () {
        return $private.pos;
    };
    $public.getRotative = function () {
        return $private.rotative;
    };
    $public.getRotativeInterval = function () {
        return $private.rotativeInterval;
    };
    $public.getHide = function () {
        return $private.hide;
    };
    $public.getSizeMappings = function () {
        return $private.sizeMappings;
    };
    $public.getIncremental = function () {
        return $private.incremental;
    };
    $public.setSlotPosition = function (position) {
        $private.slot.setPosition(position);
    };
    $public.getSlot = function () {
        return $private.slot;
    };
    $public.setCampaignuol = function (campaignuol) {
        $private.campaignuol = campaignuol;
    };
    $public.getCampaignuol = function () {
        return $private.campaignuol;
    };
    $public.setExpble = function (expble) {
        $private.expble = expble;
    };
    $public.getExpble = function () {
        return $private.expble;
    };
    $public.setGroup = function (group) {
        $private.group = group;
    };
    $public.getGroup = function () {
        return $private.group;
    };
    $public.setKeyword = function (keyword) {
        $private.keyword = keyword;
    };
    $public.getKeyword = function () {
        return $private.keyword;
    };
    $public.setAudienceSciencePlacement = function (width, height) {
        var dfpPlacements = window.adPlacements;
        if (!$private.typeValidator.isObject(dfpPlacements)) {
            return;
        }
        var size = $private.formatSize(width, height);
        if ($private.typeValidator.isString(size)) {
            $private.audienceSciencePlacement = $private.getFormatedPlacements(dfpPlacements, size);
        }
    };
    $private.formatSize = function (width, height) {
        if (!$private.typeValidator.isNumericString(width)) {
            return;
        }
        if (!$private.typeValidator.isNumericString(height)) {
            return;
        }
        var size = width + 'x' + height;
        return size;
    };
    $private.getFormatedPlacements = function (dfpPlacements, size) {
        var placements = [];
        for (var placement in dfpPlacements) {
            if (dfpPlacements[placement] === size) {
                placements.push(placement);
            }
        }
        if (placements.length < 1) {
            return;
        }
        return placements.toString();
    };
    $public.getAudienceSciencePlacement = function () {
        return $private.audienceSciencePlacement;
    };
    $public.setDfpApi = function (dfpApi) {
        if (dfpApi !== undefined) {
            $private.dfpApi = dfpApi;
        }
    };
    $public.getDfpApi = function () {
        return $private.dfpApi;
    };
    $public.setCookieName = function (cookie) {
        if (cookie) {
            $private.slot.setCookieName(cookie);
            return;
        }
        var site = $private.DfpPath.getSite();
        var chan = $private.DfpPath.getChan();
        var subchan = $private.DfpPath.getSubChan();
        if (!$private.typeValidator.isString(site)) {
            return;
        }
        cookie = site;
        if ($private.typeValidator.isString(chan)) {
            cookie += '_' + chan;
        }
        if ($private.typeValidator.isString(subchan)) {
            cookie += '_' + subchan;
        }
        $private.slot.setCookieName(cookie);
        return;
    };
    $public.getCookieName = function () {
        return $private.slot.getCookieName();
    };
    $public.setLabel = function (deslabel) {
        if ($private.typeValidator.isString(deslabel)) {
            $private.deslabel = deslabel;
        }
    };
    $public.getLabel = function () {
        return $private.deslabel;
    };
    $public.setNativeType = function (nativeType) {
        if ($private.typeValidator.isNumericString(nativeType)) {
            $private.nativeType = nativeType;
        }
    };
    $public.getNativeType = function () {
        return $private.nativeType;
    };
    $private.showAd = function () {
        if (!$private.slot) {
            return false;
        }
        $private.defineDfpSlot();
        if (!$private.slot.dfp) {
            return false;
        }
        if (!$private.dfpApi.enableAsyncRendering()) {
            return false;
        }
        if ($private.dfpApi.displayAd($private.slot.getBannerId())) {
            $public.logs.debug('Exibindo o anúncio de id ' + $private.slot.getBannerId());
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
            return true;
        }
        return false;
    };
    $private.addTargetings = function () {
        $private.slot.addTargeting('deals', $private.slot.getDeals());
        if ($private.campaignuol !== undefined) {
            $private.slot.addTargeting('campaignuol', $private.dfpApi.getCampaignuol($private.campaignuol));
        }
        if ($private.expble !== undefined) {
            $private.slot.addTargeting('expble', $private.dfpApi.getCampaignuol($private.expble));
        }
        if ($private.group !== undefined) {
            $private.slot.addTargeting('group', $private.dfpApi.getGroup($private.group));
        }
        if ($private.keyword !== undefined) {
            $private.slot.addTargeting('keyword', $private.dfpApi.getKeyword($private.keyword));
        }
        $private.slot.addTargeting('cpm', $private.slot.getCpm());
        $private.slot.addTargeting('tier', $private.slot.getTier());
        $private.slot.addTargeting('pos', $private.slot.getPosition());
        if ($private.audienceSciencePlacement !== undefined) {
            $private.slot.addTargeting('gwd', $private.audienceSciencePlacement);
        }
        if ($private.deslabel !== undefined) {
            $private.slot.addTargeting('label', $private.deslabel);
        }
        if ($private.nativeType !== undefined) {
            $private.slot.addTargeting('native', $private.nativeType);
        }
    };
    $private.defineDfpSlot = function () {
        var dfpSlot;
        if ($private.slot.getFluid()) {
            dfpSlot = $private.dfpApi.defineFluid($private.DfpPath.getDfpPath(), $private.slot.getBannerId(), $private.slot.getOutOfPage());
        } else {
            dfpSlot = $private.dfpApi.defineSlot($private.DfpPath.getDfpPath(), $private.slot.getBannerId(), $private.slot.getWidth(), $private.slot.getHeight(), $private.slot.getOutOfPage());
        }
        if ($private.slot.getCompanion()) {
            dfpSlot = $private.dfpApi.setCompanionAds(dfpSlot, $private.slot.getCompanion());
        }
        dfpSlot = $private.dfpApi.addServices(dfpSlot);
        if ($private.slot.getSizeMappings()) {
            dfpSlot = $private.dfpApi.setSizeMappings(dfpSlot, $private.slot.getSizeMappings());
        }
        if (dfpSlot) {
            $private.addTargetings();
            $private.slot.dfp = $private.dfpApi.setTargetings(dfpSlot, $private.slot.getTargetings());
        }
    };
    $public.isRotative = function () {
        if ($private.slot.getHide()) {
            return false;
        }
        return $private.slot.getRotatives().length > 0;
    };
    $public.display = function (callback) {
        if (!$public.isBannerEnabled()) {
            return;
        }
        if (!window.googletag && !window.googletag.cmd) {
            setTimeout($public.display, 100);
        }
        try {
            window.googletag.cmd.push(function () {
                $private.dfpApi.onDfpReady(function () {
                    $private.showAd();
                    callback();
                });
            });
        } catch (e) {}
    };
    $public.isBannerEnabled = function () {
        if ($private.slot.getHide()) {
            $public.hideSlot();
            return false;
        }
        if (!$private.isDHTML()) {
            return true;
        }
        if ($private.slot.hasCookie() && $public.hasFrequencyControl()) {
            return false;
        }
        $private.slot.setCookieDfp();
        return true;
    };
    $private.isDHTML = function () {
        if ($private.width === '1' && $private.height === '1') {
            return true;
        }
        return false;
    };
    $public.hideSlot = function () {
        var element = document.getElementById($private.slot.getBannerId());
        if (!element) {
            return;
        }
        element.style.display = 'none';
        if (!element.parentNode) {
            return;
        }
        if(element.parentNode.className === 'publicidade' || element.parentNode.className === 'pub') {
            element.parentNode.style.display = 'none';
        }
    };
    $public.getFrequencyControl = function () {
        return $private.frequencyControl;
    };
    $public.setFrequencyControl = function (frequencyControl) {
        $private.frequencyControl = frequencyControl;
    };
    $public.hasFrequencyControl = function () {
        if (!$private.typeValidator.isArray($private.frequencyControl)) {
            return false;
        }
        if ($private.frequencyControl[0] !== 'true') {
            return false;
        }
        return true;
    };
}
function Pubads () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.behaviouralTargeting = new BehaviouralTargeting();
    $private.pubadsTargeting = [];
    $public.defineTargetingList = function () {
        var dataLayer = $private.getDataLayer();
        for (var key in dataLayer) {
            $private.addToPubads(key, dataLayer[key]);
        }
        $private.addToPubads('tags', $public.getTagPages());
        $private.addToPubads('bt', $private.behaviouralTargeting.getBTCookies());
    };
    $private.getDataLayer = function () {
        var uolpd = window.UOLPD;
        if (!$private.typeValidator.isObject(uolpd)) {
           return {};
        }
        if (!$private.typeValidator.isObject(uolpd.dataLayer)) {
            return {};
        }
        return uolpd.dataLayer;
    };
    $private.addToPubads = function (key, value) {
        if (!$private.typeValidator.isString(key) || !$private.typeValidator.isDefined(value)) {
            return;
        }
        var index = $private.getTargetingIndex(key);
        if (index < 0) {
            $private.pubadsTargeting.push({
                'key': key,
                'value': value
            });
        }
        var targeting = $private.pubadsTargeting[index];
        if (!targeting) {
            return;
        }
        if (targeting.value !== value) {
            targeting.value = value;
        }
    };
    $private.getTargetingIndex = function (key) {
        var index = $private.pubadsTargeting.length;
        while (index) {
            index--;
            if ($private.pubadsTargeting[index].key === key) {
                return index;
            }
        }
        return -1;
    };
    $public.getTagPages = function () {
        var uVar = window.universal_variable;
        if (!$private.typeValidator.isObject(uVar)) {
            return;
        }
        if (!$private.typeValidator.isObject(uVar.page)) {
            return;
        }
        if (!$private.typeValidator.isArray(uVar.page.tags)) {
            return;
        }
        return uVar.page.tags;
    };
    $public.getTargetingList = function () {
        return $private.pubadsTargeting;
    };
}
function RotativeAds () {
    var $public = this;
    var $private = {};
    $public.setAd = function (ad) {
        $private.ad = ad;
        $private.rotatives = $private.ad.getSlot().getRotatives();
    };
    $public.getAd = function () {
        return $private.ad;
    };
    $public.setContainer = function (container) {
        container.style.display = 'none';
        container.style.overflow = 'hidden';
        $private.container = container;
    };
    $public.setCallback = function (callback) {
        $private.callback = callback;
    };
    $public.getCallback = function () {
        return $private.callback;
    };
    $public.display = function () {
        $private.appendRotatives();
        $private.showRotatives();
        $private.rotative = $private.ad.getSlot().getRotative();
        $public.rotate();
    };
    $private.appendRotatives = function () {
        var inner = '';
        for (var i = 0, length = $private.rotatives.length; i < length; i++) {
            inner+= $private.rotatives[i].outerHTML;
        }
        $private.container.innerHTML = inner;
    };
    $private.showRotatives = function () {
        for (var i = 0, length = $private.rotatives.length; i < length; i++) {
            $private.showCurrentAd(
                $private.rotatives[i].getAttribute('id'),
                $private.getRotativePosition(i)
            );
        }
    };
    $public.rotate = function() {
        if (!$private.rotative.getCadence()) {
            return;
        }
        setInterval(function() {
            $public.turnRotation();
        }, $private.rotative.getCadence());
    };
    $public.turnRotation = function() {
        var items = $private.container.childNodes;
        for (var i = 0; i < items.length; i++) {
            var children = items[i];
            if (children.style.display !== 'none') {
                children.style.display = 'none';
                items[$private.getNext(i, items.length)].style.display = 'block';
                break;
            }
        }
        $private.container.style.display = 'block';
    };
    $private.getNext = function(actual, size) {
        if (actual === (size - 1)) {
            return 0;
        }
        return ++actual;
    };
    $private.getRotativePosition = function (i) {
        return 'rotativo-' + (i + 1);
    };
    $private.showCurrentAd = function (id, rotativePosition) {
        $private.getAd(id, rotativePosition).display($private.callback);
    };
    $private.getAd = function (bannerId, position) {
        var ad = new Ad();
        ad.setPubadsTargeting($private.ad.getPubadsTargeting());
        ad.setForcePath($private.ad.getForcePath());
        ad.setSlot(
            bannerId,
            $private.ad.getWidth(),
            $private.ad.getHeight(),
            $private.ad.getIsCompanion(),
            $private.ad.getOutOfPage(),
            position,
            $private.ad.getRotative(),
            $private.ad.getRotativeInterval(),
            $private.ad.getHide(),
            $private.ad.getSizeMappings(),
            $private.ad.getIncremental(),
            $private.ad.getSlot().getFluid()
        );
        ad.setDfpPath($private.ad.getDfpPath());
        ad.setCampaignuol($private.ad.getCampaignuol());
        ad.setExpble($private.ad.getExpble());
        ad.setGroup($private.ad.getGroup());
        ad.setKeyword($private.ad.getKeyword());
        ad.setAudienceSciencePlacement($private.ad.getWidth(), $private.ad.getHeight());
        ad.setDfpApi($private.ad.getDfpApi());
        ad.setCookieName($private.ad.getCookieName());
        ad.setLabel($private.ad.getLabel());
        ad.setNativeType($private.ad.getNativeType());
        ad.setFrequencyControl($private.ad.getFrequencyControl());
        return ad;
    };
}
function Main () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $private.dfpApi = new DfpApi();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('dfp-async');
    $private.pubads = new Pubads();
    $private.domUtils = new DomUtils();
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.DfpAsync');
    $public.adsDisplayed = [];
    $public.refreshAds = $private.dfpApi.refreshAds;
    window.__configuredDFPTags = window.__configuredDFPTags || [];
    $private.hasUOLADS = $private.typeValidator.isArray(window.uolads);
    $public.init = function (config) {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if ($private.isValidBanner(config)) {
            $private.prepareDfpApi();
            $private.pubads.defineTargetingList();
            if (!$private.typeValidator.isArray(window.__configuredDFPTags)) {
                window.__configuredDFPTags = [];
            }
            window.__configuredDFPTags.push(config);
            $public.enqueue(config, 6);
        }
    };
    $private.isValidBanner = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'O objeto de configuração está inválido');
            return false;
        }
        if (!$private.typeValidator.isString(config.bannerId)) {
            $private.trackError('bannerId_invalido', 'O atributo bannerId do objeto de configuração está inválido');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.prepareDfpApi = function () {
        $private.dfpApi.appendGPT();
        $private.dfpApi.createNameSpace();
    };
    $public.enqueue = function (config, limit) {
        if (limit < 1) {
            var firstAd = window.uolads[0];
            if (firstAd) {
                $private.removeFromQueue(firstAd.id);
                window.uolads = window.uolads.concat([firstAd]);
                $public.logs.warn('O anúncio de ID ' + firstAd.id + ' não foi encontrado nas configurações do repositório no tempo limite e foi despriorizado para o final da fila de exibição');
            }
            window.uolads.push = $private.push;
        }
        if (!$private.showFirstElementOnQueue(config.bannerId, config.isIncremental)) {
            limit--;
            setTimeout(function () {
                $public.enqueue(config, limit);
            }, 150);
        }
    };
    $private.push = function (ad) {
        if (!ad.id) {
            return false;
        }
        if ($private.isAdPopulated(ad.id)) {
            $private.removeSlotFromAd(ad.id);
            $private.removeChildsFromElement(ad.parent);
            ad.id = $private.appendRefreshedElement(document.getElementById(ad.id), ad);
            $private.showFromUolAds(ad, true);
            return true;
        }
        var childId = $private.getChildId(ad.id);
        if (!childId) {
            $private.showFromUolAds(ad, false);
            return true;
        }
        ad.parent = document.getElementById(ad.id);
        ad.id = childId;
        return $private.push(ad);
    };
    $private.getChildId = function (id) {
        var element = document.getElementById(id);
        var childId;
        if (!element) {
            return;
        }
        if (element.children.length > 0) {
            childId = element.children[0].getAttribute('id');
        }
        if (!childId) {
            return;
        }
        if (childId.indexOf(id + '-refreshed-') < 0) {
            return;
        }
        return childId;
    };
    $private.showFromUolAds = function (ad, isPopulated) {
        var configuredTags = window.__configuredDFPTags;
        if (!$private.typeValidator.isArray(configuredTags)) {
            window.__configuredDFPTags = [];
            return;
        }
        var tag = {};
        for (var i = 0, length = configuredTags.length; i < length; i++) {
            if (ad.id.indexOf(configuredTags[i].bannerId) < 0) {
                continue;
            }
            tag = $private.cloneObject(configuredTags[i]);
            tag.ignoreTrackedPubads = true;
            if (isPopulated) {
                tag.isIncremental = 'true';
            }
            if ($private.showIncremental(tag, ad)) {
                break;
            }
        }
    };
    $private.cloneObject = function (obj1) {
        var obj2 = {};
        for (var key in obj1) {
            obj2[key] = obj1[key];
        }
        return obj2;
    };
    $private.removeSlotFromAd = function (id) {
        $private.clearSlot(id);
        $private.removeFromAdsDisplayed(id);
        var element = document.getElementById(id);
        $private.removeChildsFromElement(element);
        $public.logs.debug('removendo slot');
    };
    $private.clearSlot = function (id) {
        var slot = $private.getSlot(id);
        if (slot) {
            googletag.pubads().clear([slot]);
        }
    };
    $private.removeFromAdsDisplayed = function (id) {
        var index = $private.getDisplayedAdIndex(id);
        if (index >= 0) {
            $public.adsDisplayed.splice(index, 1);
        }
    };
    $private.removeChildsFromElement = function (element) {
        if (!element) {
            return;
        }
        for (var i = 0; i < element.childNodes.length; i++) {
            element.removeChild(element.childNodes[i]);
        }
    };
    $private.appendRefreshedElement = function (parent, ad) {
        var element = document.createElement('div');
        var id = ad.id;
        if (ad.parent) {
            id = ad.parent.getAttribute('id');
            parent = ad.parent;
        }
        id = id + '-refreshed-' + Math.random().toString(36).substring(2);
        element.setAttribute('id', id);
        parent.appendChild(element);
        return id;
    };
    window.uolads = window.uolads || [];
    window.uolads.push = $private.push;
    $private.showIncremental = function (config, ad) {
        if (config.isIncremental === 'true') {
            config.bannerId = ad.id;
        } else if (ad.id !== config.bannerId) {
            return false;
        }
        $private.removeFromQueue(ad.id);
        $public.displayBanner(config);
        return true;
    };
    $private.showFirstElementOnQueue = function (id, isIncremental) {
        window.uolads.push = $private.push;
        var queue = $public.getQueue();
        if (!$private.hasUOLADS) {
            $public.logs.debug('A fila window.uolads não existe.');
            window.uolads.push({ 'id': id });
            return true;
        }
        if (!queue) {
            return false;
        }
        if (!queue[0]) {
            return false;
        }
        if ($private.typeValidator.isString(isIncremental) && isIncremental.toLowerCase() === 'true') {
            return $private.pushIncrementalAds(id, queue);
        }
        if (queue[0].id === id) {
            $public.logs.debug('O elemento ' + id + ' foi achado na fila e será exibido');
            window.uolads.push(queue[0]);
            return true;
        }
        return false;
    };
    $private.pushIncrementalAds = function  (id, queue) {
        var hasIncrementalAds = false;
        for (var i = 0; i < queue.length; i++) {
            if (queue[i].id.indexOf(id) < 0 || !$private.push(queue[i])) {
                return false;
            }
            i--;
            hasIncrementalAds = true;
        }
        return hasIncrementalAds;
    };
    $public.getQueue = function () {
        try {
            if ($private.typeValidator.isArray(window.uolads)) {
                return window.uolads;
            } else {
                return;
            }
        } catch (e) {
            return [];
        }
    };
    $private.removeFromQueue = function (id) {
        var queue = $public.getQueue();
        if (queue) {
            window.uolads = $private.removeElementFromListById(queue, id);
        }
    };
    $private.removeElementFromListById = function (list, id) {
        for (var i = 0; i < list.length; i++) {
            var element = list[i];
            if (!element.id) {
                continue;
            }
            if (element.id === id) {
                list.splice(i, 1);
                i--;
            }
        }
        return list;
    };
    $public.displayBanner = function (config) {
        $private.domUtils.divHasLoaded(config.bannerId, function () {
            var div = $private.domUtils.getDiv(config.bannerId);
            if (!div) {
                return;
            }
            if ($private.isAdPopulated(config.bannerId)) {
                $private.trackError('anuncio_populado', 'O elemento já foi populado');
                return;
            }
            $private.dfpApi.onDfpReady(function () {
                if (!$private.dfpApi.hasTrackedPubads() || config.ignoreTrackedPubads === true) {
                    $private.pubads.defineTargetingList();
                    $private.dfpApi.setPubadsTracking($private.pubads.getTargetingList());
                }
                $private.display(config, div);
            });
        });
    };
    $private.isAdPopulated = function (bannerId) {
        if ($private.getDisplayedAdIndex(bannerId) < 0) {
            return false;
        }
        return true;
    };
    $private.getDisplayedAdIndex = function (bannerId) {
        for (var i = 0, length = $public.adsDisplayed.length; i < length; i++) {
            if ($public.adsDisplayed[i].getBannerId() == bannerId) {
                return i;
            }
        }
        return -1;
    };
    $private.display = function (config, div) {
        $private.showAd($private.getAd(config), div);
    };
    $private.showAd = function (ad, element) {
        if (ad.isRotative()) {
            $private.showRotatives(ad, element);
        } else {
            ad.display($private.updateDfpApi(ad));
        }
    };
    $private.getAd = function (config) {
        var ad = new Ad();
        ad.setPubadsTargeting($private.pubads.getTargetingList());
        config.rotativeInterval = 3000;
        ad.setForcePath(config.forcePath);
        ad.setSlot(config.bannerId, config.bannerWidth, config.bannerHeight, config.isCompanion, config.outOfPage, config.pos, config.rotative, config.rotativeInterval, config.hide, config.sizeMapping, config.isIncremental, config.fluid);
        ad.setDfpPath(config.dfppath);
        ad.setCookieName(config.cookie);
        ad.setCampaignuol(config.campaignuol);
        ad.setExpble(config.expble);
        ad.setGroup(config.group);
        ad.setKeyword(config.keyword);
        ad.setAudienceSciencePlacement(config.bannerWidth, config.bannerHeight);
        ad.setDfpApi($private.dfpApi);
        ad.setLabel(config.deslabel);
        ad.setNativeType(config.nativeType);
        ad.setFrequencyControl(config.frequencyControl);
        return ad;
    };
    $private.updateDfpApi = function (ad) {
        return function () {
            $private.dfpApi = ad.getDfpApi();
            $public.adsDisplayed.push(ad);
        };
    };
    $private.showRotatives = function (ad, container) {
        var rotativeAds = new RotativeAds();
        rotativeAds.setAd(ad);
        rotativeAds.setContainer(container);
        rotativeAds.setCallback($private.updateDfpApi(ad));
        rotativeAds.display();
    };
    $public.refreshSlot = function (id) {
        if (!$private.typeValidator.isString(id)) {
            return;
        }
        var slot = $private.getSlot(id);
        if (!slot) {
            return;
        }
        $private.dfpApi.refreshAds(slot);
    };
    $private.getSlot = function (id) {
        var slots = googletag.pubads().getSlots();
        for (var i = 0, length = slots.length; i < length; i++) {
            var slot = slots[i];
            if (slot.getSlotElementId() === id) {
                return slot;
            }
        }
        return;
    };
}
var nameSpace = new NameSpace('DfpAsync');
var dfpAsync = new Main();
nameSpace.init = dfpAsync.init;
nameSpace.refreshAds = dfpAsync.refreshAds;
nameSpace.refreshSlot = dfpAsync.refreshSlot;
var __monitoracaoJsuol = 'tm.jsuol.com.br/modules/dfp-async.js';
})(window, document);
(function (window, document, undefined){
'use strict';
function Injector () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager.CodeInjector');
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('code-injector');
    $public.codes = [];
    $private.typeValidator = new TypeValidator();
    $public.init = function (config) {
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if ($private.isValidConfig(config)) {
            $private.evalCode($private.trimScriptTag(config.codeInjector));
        }
    };
    $private.isValidConfig = function (config) {
        if (!$private.typeValidator.isObject(config)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        if (!$private.typeValidator.isString(config.codeInjector)) {
            $private.trackError('config_invalido', 'Uma configuração do Tag Manager não está implementada corretamente');
            return false;
        }
        return true;
    };
    $private.trackError = function (errorType, errorMessage) {
        if (!$private.typeValidator.isString(errorType)) {
            return;
        }
        if (!$private.typeValidator.isString(errorMessage)) {
            return;
        }
        $public.logs.warn(errorMessage);
        $private.trackManager.trackError(errorType, 'fluxo_interrompido');
    };
    $private.trimScriptTag = function (code) {
        var replaceScriptRegex = /^(\s+)?<script>(\s+)?|(\s+)?<\/script>(\s+)?$/gm;
        code = code.replace(replaceScriptRegex, '');
        $public.codes.push(code);
        return code;
    };
    $private.evalCode = function (code) {
        try {
            var dynMet = new Function(code);
            dynMet();
            $private.trackManager.trackSuccess('Execucoes Finalizadas');
        } catch (err) {
            $private.trackError('parse_error', 'O Tag Manager previniu um erro de execução de javascript. Favor verificar a implementação de sua tag');
        }
    };
}
var nameSpace = new NameSpace('CodeInjector');
var injector = new Injector();
nameSpace.init = injector.init;
})(window, document);
var defaultTags = [
    {
        "module" : "DNA",
        "async" : true,
        "rules" : {
            "enable" : ["/\\.uol\\.com\\.br/.test(window.location.href)"],
            "disable" : []
        },
        "events": ["onload"],
        "src" : "//tm.jsuol.com.br/modules/uol-dna.js"
    },
    {
        "module" : "CodeInjector",
        "async" : true,
        "rules" : {
            "enable" : [ "document.location.href.match(/.*/)" ],
            "disable" : [ ]
        },
        "events" : [ "onload" ],
        "config" : {
            "codeInjector" : "(function() {\n var script = document.createElement('script');\n var src = 'http://tracker.bt.uol.com.br/partner?source=tagmanager';\n if (window.location.protocol === 'https:') {\n src = 'https://tracker.bt.uol.com.br/track?source=tagmanager';\n }\n \n script.setAttribute('src', src);\n document.head.appendChild(script);\n})()"
        },
        "src" : "//tm.jsuol.com.br/modules/code-injector.js"
    }
];
function VersionManager () {
    var $private = {};
    var $public = this;
    $public.logger = new Logs();
    $private.validator = new TypeValidator();
    $private.stringUtils = new StringUtils();
    $private.scriptUtils = new ScriptUtils();
    $private.tmVersionToken = 'uoltm_version';
    $public.setSpecificVersion = function () {
        if (!$public.specificVersionWasRequested()) {
            return;
        }
        $private.scriptUtils.createSyncScript($public.getSpecificVersionURL());
    };
    $public.specificVersionWasRequested = function () {
        var requestedVersion = $private.getRequestedVersion();
        var versionRegex = /^\d+$/gm;
        var isValidVersion = versionRegex.test(requestedVersion);
        if (!requestedVersion || !isValidVersion) {
            return false;
        }
        if ($private.hasVersionScriptInPage()) {
            return false;
        }
        return true;
    };
    $public.getSearch = function () {
        return window.location.search;
    };
    $public.getScriptUrl = function () {
        var __monitoracaoJsuol = 'tm.jsuol.com.br/5n/zu/5nzulr.js';
        return __monitoracaoJsuol;
    };
    $public.getSpecificVersionURL = function () {
        var codRepository = $private.getCodRepository();
        var versionURL = $private.buildVersionURL(codRepository);
        return versionURL;
    };
    $private.getRequestedVersion = function () {
        var requestedVersion = $private.stringUtils.getValueFromKeyInString($public.getSearch(), $private.tmVersionToken, '&');
        return requestedVersion;
    };
    $private.hasVersionScriptInPage = function () {
        var versionURL = $public.getSpecificVersionURL();
        var scripts = document.querySelectorAll('script[src*="'+ versionURL +'"]');
        if (!scripts || !scripts.length) {
            return false;
        }
        return true;
    };
    $private.buildVersionURL = function (codRepository) {
        var mainPath = '//tm.jsuol.com.br/';
        var firstDirLevel = codRepository.substring(0, 2) + '/';
        var secondDirLevel = codRepository.substring(2, 4) + '/';
        var fullPath = mainPath + firstDirLevel + secondDirLevel + codRepository;
        var requestedVersion = $private.getRequestedVersion();
        if (requestedVersion != '0') {
            fullPath += '-' + $private.getRequestedVersion() + '.js';
        } else {
            fullPath += '.js';
        }
        return fullPath;
    };
    $private.getCodRepository = function () {
        var baseURLRegex = /[\w\d]{6}\.js$/gm;
        var codRepository = $public.getScriptUrl().match(baseURLRegex, '')[0];
        codRepository = codRepository.replace('.js', '');
        return codRepository;
    };
}
function TMEvent () {
    var $private = {};
    var $public = this;
    $private.typeValidator = new TypeValidator();
    $public.getName = function () {
        return $private.name;
    };
    $public.setName = function (name) {
        if ($private.typeValidator.isString(name)) {
            $private.name = name;
        }
    };
    $public.getModule = function () {
        return $private.module;
    };
    $public.setModule = function (moduleName) {
        var module;
        if (!$private.typeValidator.isString(moduleName)) {
            return;
        }
        module = UOLPD.TagManager[moduleName];
        if (!$private.typeValidator.isDefined(module)) {
            return;
        }
        module.nameSpace = moduleName;
        if ($private.typeValidator.isFunction(module.init)) {
            $private.module = module;
        }
    };
    $public.getConfig = function () {
        return $private.config;
    };
    $public.setConfig = function (config) {
        if ($private.typeValidator.isObject(config)) {
            $private.config = config;
        }
    };
}
function Trigger () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.typeValidator = new TypeValidator();
    $private.events = {};
    $public.getEvents = function () {
        return $private.events;
    };
    $public.addEvent = function (eventName, moduleName, config) {
        var currentEvent = new TMEvent();
        currentEvent.setName(eventName);
        currentEvent.setModule(moduleName);
        currentEvent.setConfig(config);
        $private.createEvent(currentEvent.getName(), currentEvent.getModule(), currentEvent.getConfig());
    };
    $private.createEvent = function (eventName, module, config) {
        if (!eventName || !module) {
            return;
        }
        var currentEvent = $public.getEventsByName(eventName) || [];
        module.calls = module.calls || 0;
        currentEvent.push({
            'name': module.nameSpace,
            'module': module,
            'config': config,
            'calls': 0
        });
        $private.events[eventName] = currentEvent;
    };
    $public.getEventsByName = function (eventName) {
        if (!$private.typeValidator.isString(eventName)) {
            return;
        }
        var events = $private.events[eventName];
        if (!$private.typeValidator.isArray(events)) {
            return;
        }
        return events;
    };
    $public.removeFirstEvent = function (eventName) {
        if ($public.getEventsByName(eventName)) {
            $private.events[eventName].splice(0, 1);
        }
    };
    $public.init = function (eventName) {
        var tags = $public.getEventsByName(eventName);
        if (!tags) {
            $public.logs.warn('Não existe nenhuma tag a ser disparada com o gatilho solicitado');
            return;
        }
        for (var i = 0; i < tags.length; i++) {
            var tag = tags[i];
            tag.module.init(tag.config);
            tag.calls++;
            tag.module.calls++;
        }
    };
}
function DataLayer () {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.typeValidator = new TypeValidator();
    $public.createDataLayer = function () {
        var namespace = window.UOLPD;
        if (!$private.typeValidator.isObject(namespace)) {
            window.UOLPD = {};
        }
        var dataLayer = window.UOLPD.dataLayer;
        if (!$private.typeValidator.isObject(dataLayer)) {
            window.UOLPD.dataLayer = {
            };
        }
    };
    $public.setDataLayer = function (module) {
        window.UOLPD.dataLayer.referer = window.UOLPD.dataLayer.referer || window.location.href;
        var dataLayer = $private.hasModuleDataLayer(module);
        if (!$private.typeValidator.isObject(dataLayer)) {
            return;
        }
        for (var key in dataLayer) {
            window.UOLPD.dataLayer[key] = dataLayer[key];
            $public.logs.debug('Foi adicionado "'+ key + ' : ' + dataLayer[key] + '" à window.UOLPD.dataLayer pelo módulo: ' + module);
        }
    };
    $private.hasModuleDataLayer = function (module) {
        var data = {};
        try {
            data = JSON.parse(window.localStorage.getItem('uolDataLayer'));
        } catch (e) {}
        if (!$private.typeValidator.isObject(data)) {
            return;
        }
        if (!$private.typeValidator.isString(module)) {
            return;
        }
        var dataLayer = data[module];
        if (!$private.typeValidator.isObject(dataLayer)) {
            return;
        }
        return dataLayer;
    };
}
function TagManager (nameSpace) {
    var $private = {};
    var $public = this;
    $public.logs = new Logs();
    $public.logs.setPrefix('UOLPD.TagManager');
    $private.validator = new TypeValidator();
    $private.versionManager = new VersionManager();
    $private.trackManager = new TrackManager();
    $private.trackManager.setModuleName('core');
    $public.trigger = UOLPD.TagManager.trigger || new Trigger();
    $private.dataLayer = new DataLayer();
    $private.version = '${parent.version}';
    $private.triggerQueue = [];
    $public.init = function (tags) {
        $private.startTime = (new Date()).getTime();
        $private.trackManager.trackSuccess('Execucoes Iniciadas');
        if (!$private.validator.isArray(tags)) {
            return false;
        }
        if ($private.versionManager.specificVersionWasRequested()) {
            return $private.versionManager.setSpecificVersion();
        }
        tags = tags.concat(defaultTags);
        $private.cloneTriggerQueue();
        $private.dataLayer.createDataLayer();
        return $private.createModules(tags);
    };
    $private.cloneTriggerQueue = function () {
        var triggerUOLTM = window.triggerUOLTM;
        if ($private.validator.isArray(triggerUOLTM)) {
            while (triggerUOLTM.length) {
                $private.triggerQueue.push(triggerUOLTM.shift());
            }
        }
        window.triggerUOLTM = $private.triggerUOLTM;
        window.triggerUOLTM.push = $private.pushTrigger;
    };
    $private.createModules = function (tags) {
        var result = true;
        if (tags.length === 0) {
            return false;
        }
        while (tags.length) {
            if (!$private.configureTag(tags.shift())) {
                result = false;
            }
        }
        $private.triggerOnDocComplete();
        $private.updateEvents();
        $private.trackManager.trackSuccess('Execucoes Finalizadas');
        var execTime = (new Date()).getTime() - $private.startTime;
        $public.logs.debug('Tempo de execuçao: ' + execTime);
        $private.trackManager.trackCustom('Execution', 'time', execTime);
        return result;
    };
    $private.configureTag = function (tag) {
        if (!$private.isTagValid(tag)) {
            return false;
        }
        if (!$private.isTagEnabled(tag)) {
            return false;
        }
        $private.createEvents(tag);
        $private.dataLayer.setDataLayer(tag.module);
        return true;
    };
    $private.isTagValid = function (tag) {
        if (!$private.validator.isObject(tag)) {
            return false;
        }
        if (!$private.validator.isString(tag.module)) {
            return false;
        }
        if (!$private.validator.isBoolean(tag.async)) {
            return false;
        }
        if (!$private.validator.isObject(tag.rules)) {
            return false;
        }
        if (!$private.validator.isArray(tag.rules.enable)) {
            return false;
        }
        if (!$private.validator.isArray(tag.rules.disable)) {
            return false;
        }
        if (!$private.validator.isArray(tag.events)) {
            return false;
        }
        if (!$private.validator.isString(tag.src)) {
            return false;
        }
        if (tag.config && !$private.validator.isObject(tag.config)) {
            return false;
        }
        return true;
    };
    $private.isTagEnabled = function (tag) {
        if ($private.checkRules(tag.rules.disable)) {
            return false;
        }
        if ($private.checkRules(tag.rules.enable)) {
            return true;
        }
    };
    $private.checkRules = function (rules) {
        try {
            if (eval(rules.join(' || '))) {
                return true;
            }
        } catch (e) {
            return false;
        }
    };
    $private.createEvents = function (tag) {
        if (tag.async !== true) {
            $public.trigger.addEvent('autostart', tag.module, tag.config);
            $public.trigger.init('autostart');
            $public.trigger.removeFirstEvent('autostart');
            return;
        }
        if (tag.events.length === 0) {
            tag.events.push('autostart');
        }
        for (var i = 0, length = tag.events.length; i < length; i++) {
            var eventName = tag.events[i];
            $public.trigger.addEvent(eventName, tag.module, tag.config);
            if (eventName === 'autostart') {
                $public.trigger.init('autostart');
                $public.trigger.removeFirstEvent('autostart');
            }
        }
    };
    $private.triggerOnDocComplete = function() {
        if (document.readyState === "complete") {
            $public.trigger.init('onload');
            return;
        }
        var method = (document.addEventListener) ? "readystatechange" : "onreadystatechange";
        var methodListener = (document.addEventListener) ? "addEventListener" : "attachEvent";
        document[methodListener](method, function() {
            if (document.readyState === "complete") {
                $public.trigger.init('onload');
            }
        });
    };
    $private.updateEvents = function () {
        while ($private.triggerQueue.length) {
            $private.pushTrigger($private.triggerQueue.shift());
        }
    };
    $private.pushTrigger = function (trigger) {
        if ($private.validator.isObject(trigger)) {
            $private.triggerUOLTM(trigger.eventName);
            return;
        }
        $private.triggerUOLTM(trigger);
    };
    $private.triggerUOLTM = function(eventType) {
        if (!$private.validator.isString(eventType)) {
            $public.logs.warn("O gatilho solicitado deve ser uma string");
            return;
        }
        if (eventType === "onload") {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "onload"');
            return;
        }
        if (eventType === "autostart") {
            $public.logs.warn('O gatilho solicitado não pode ser do tipo "autostart"');
            return;
        }
        $public.trigger.init(eventType);
    };
}
var nameSpace = (new NameSpace()).create();
nameSpace['5nzulr'] = nameSpace['5nzulr'] || {};
nameSpace['5nzulr'].version = '36';
var tagManager = new TagManager(nameSpace);
nameSpace.trigger = tagManager.trigger;
tagManager.init([ {
  "module" : "NativeAds",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "desLabel" : "responsive-native-home",
    "shortTextLimit" : null,
    "markup" : "<a href=\"%NATIVE_URL%\" class=\"thumbnail media AdBox\" title=\"%NATIVE_DESCRIPTION%\" target=\"_blank\">\n  <div class=\"thumbnail-image media-default media-small\">\n      <figure class=\"no-adapt crop-300x200 figure\">\n          <div class=\"image \">\n              <div class=\"placeholder\">\n                  <img src=\"%NATIVE_IMG%\" data-src=\"%NATIVE_IMG%\" alt=\"%NATIVE_DESCRIPTION%\" data-crop=\"{\"xs\":\"615x300\", \"sm\":\"300x200\"}\" width=\"300\" height=\"200\" data-crazyload=\"loaded\" class=\"loaded AdImage\"> \n                  <i class=\"placeholder-mask\" style=\"padding-bottom: 66.66666666666666%\"></i>\n              </div>\n          </div>\n      </figure>\n        <span class=\"native-ad-chapeu-a\">Conteúdo Publicitário</span>\n        <span class=\"media-position\">\n          <span class=\"icon-default\"></span>\n        </span>\n    </div>\n  <div class=\"thumbnail-text italic\">\n    <span class=\"native-ad-chapeu-b\">Conteúdo Publicitário</span>\n    <h5 class=\"h-headline\">%NATIVE_TITLE%\n<i class=\"icon-single-arrow-right\"></i></h5>\n    <p class=\"p-headline thumbnail-caption\">%NATIVE_DESCRIPTION%</p>\n  </div>\n</a>",
    "flagBehaviour" : "1",
    "replacementElement" : ".responsive-native-home",
    "before" : null,
    "container" : ".responsive-native-home",
    "urlMobile" : [ ],
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "1",
    "styleTag" : "@media screen and (min-width: 320px){\n\t.native-ad-chapeu-a {\n\t\tdisplay: none !important;\n\t}\n\n\t.native-ad-chapeu-b {\n\t\tdisplay: block !important;\n\t\tbackground: #1a1a1a;\n\t\tfont-size: 11px;\n\t\tline-height: 18px;\n\t\tcolor: #fff;\n\t\ttext-align: center;\n\t\tborder-radius: 4px;\n\t\tpadding: 2px 0px;\n    \t        width: 128px;\n\t        margin: 5px 0px 5px 0px;\n\t        height: 20px;\n                font-style: normal !important;\n\t}\n}\n\n@media screen and (min-width: 768px){\n\t.responsive-native-timeline .placeholder {\n\t\tmax-width: 270px !important;\n\t\tmax-height: 180px !important;\n\t\tdisplay: block !important;\n\t\toverflow: hidden;\n\t}\n\t.native-ad-chapeu-a {\n  \t\tdisplay: block !important;\n  \t\tbackground: #1a1a1a !important;\n  \t\tfont-size: 11px !important;\n  \t\tline-height: 18px !important;\n  \t\tcolor: #fff !important;\n  \t\ttext-align: center !important;\n  \t\tposition: absolute !important;\n  \t\ttop: 3px !important;\n  \t\tleft: 3px !important;\n  \t\tborder-radius: 4px !important;\n  \t\tpadding:0 10px !important;\n\t}\n\n\t.native-ad-chapeu-b {\n\t\tdisplay: none !important;\n\t}\n}\n\n.native-ad-chapeu-a,  {\n  display: block;\n  background: #1a1a1a;\n  font-size: 11px;\n  line-height: 18px;\n  color: #fff;\n  text-align: center;\n  position: absolute;\n  top: 3px;\n  left: 3px;\n  border-radius: 4px;\n  padding:0 10px;\n}\n\n.native-ad-chapeu-b {\n\tdisplay: none;\n}\n\n.italic {\n\tfont-style: italic;\n}\n\n\n.AdBox .AdText {\n  color: #4c4c4c;\n  font-style: italic;\n}\n"
  },
  "src" : "//tm.jsuol.com.br/modules/native-ads.js"
}, {
  "module" : "NativeAds",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "desLabel" : "responsive-native-timeline",
    "shortTextLimit" : null,
    "markup" : "<a href=\"%NATIVE_URL%\" class=\"thumbnail media AdBox\" title=\"%NATIVE_DESCRIPTION%\" target=\"_blank\">\n  <div class=\"thumbnail-image media-default media-small\">\n      <figure class=\"no-adapt crop-300x200 figure\">\n          <div class=\"image \">\n              <div class=\"placeholder\">\n                  <img src=\"%NATIVE_IMG%\" data-src=\"%NATIVE_IMG%\" alt=\"%NATIVE_DESCRIPTION%\" data-crop=\"{\"xs\":\"615x300\", \"sm\":\"300x200\"}\" width=\"300\" height=\"200\" data-crazyload=\"loaded\" class=\"loaded AdImage\"> \n                  <i class=\"placeholder-mask\" style=\"padding-bottom: 66.66666666666666%\"></i>\n              </div>\n          </div>\n      </figure>\n        <span class=\"native-ad-chapeu-a\">Conteúdo Publicitário</span>\n        <span class=\"media-position\">\n          <span class=\"icon-default\"></span>\n        </span>\n    </div>\n  <div class=\"thumbnail-text italic\">\n    <span class=\"native-ad-chapeu-b\">Conteúdo Publicitário</span>\n    <h5 class=\"h-headline\">%NATIVE_TITLE%\n<i class=\"icon-single-arrow-right\"></i></h5>\n    <p class=\"p-headline thumbnail-caption\">%NATIVE_DESCRIPTION%</p>\n  </div>\n</a>",
    "flagBehaviour" : "1",
    "replacementElement" : ".responsive-native-timeline",
    "before" : null,
    "container" : ".responsive-native-timeline",
    "urlMobile" : [ ],
    "coddisplaysupplier" : "51eed576aff54be8b1311d90735f36a6",
    "numAds" : "1",
    "styleTag" : "@media screen and (min-width: 320px){\n\t.native-ad-chapeu-a {\n\t\tdisplay: none !important;\n\t}\n\n\t.native-ad-chapeu-b {\n\t\tdisplay: block !important;\n\t\tbackground: #1a1a1a;\n\t\tfont-size: 11px;\n\t\tline-height: 18px;\n\t\tcolor: #fff;\n\t\ttext-align: center;\n\t\tborder-radius: 4px;\n\t\tpadding: 2px 0px;\n    \t        width: 128px;\n\t        margin: 5px 0px 5px 120px;\n\t        height: 20px;\n                font-style: normal !important;\n\t}\n}\n\n@media screen and (min-width: 768px){\n\t.responsive-native-timeline .placeholder {\n\t\tmax-width: 270px !important;\n\t\tmax-height: 180px !important;\n\t\tdisplay: block !important;\n\t\toverflow: hidden;\n\t}\n\t.native-ad-chapeu-a {\n  \t\tdisplay: block !important;\n  \t\tbackground: #1a1a1a !important;\n  \t\tfont-size: 11px !important;\n  \t\tline-height: 18px !important;\n  \t\tcolor: #fff !important;\n  \t\ttext-align: center !important;\n  \t\tposition: absolute !important;\n  \t\ttop: 3px !important;\n  \t\tleft: 3px !important;\n  \t\tborder-radius: 4px !important;\n  \t\tpadding:0 10px !important;\n\t}\n\n\t.native-ad-chapeu-b {\n\t\tdisplay: none !important;\n\t}\n}\n\n.native-ad-chapeu-a,  {\n  display: block;\n  background: #1a1a1a;\n  font-size: 11px;\n  line-height: 18px;\n  color: #fff;\n  text-align: center;\n  position: absolute;\n  top: 3px;\n  left: 3px;\n  border-radius: 4px;\n  padding:0 10px;\n}\n\n.native-ad-chapeu-b {\n\tdisplay: none;\n}\n\n.italic {\n\tfont-style: italic;\n}\n\n.AdBox .AdText {\n  color: #4c4c4c;\n  font-style: italic;\n}\n"
  },
  "src" : "//tm.jsuol.com.br/modules/native-ads.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "cookie" : null,
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo",
    "campaignuol" : "1",
    "outOfPage" : [ ],
    "hide" : "False",
    "isIncremental" : "false",
    "bannerId" : "banner-300x250-area",
    "keyword" : null,
    "bannerHeight" : "250",
    "rotative" : null,
    "group" : null,
    "sizeMapping" : null,
    "forcePath" : [ ],
    "expble" : "1",
    "pos" : "bottom",
    "bannerWidth" : "300"
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "cookie" : null,
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo",
    "campaignuol" : "1",
    "outOfPage" : [ ],
    "hide" : "False",
    "isIncremental" : "false",
    "bannerId" : "banner-588x414-area",
    "keyword" : null,
    "bannerHeight" : "414",
    "rotative" : null,
    "group" : null,
    "sizeMapping" : null,
    "forcePath" : [ ],
    "expble" : "0",
    "pos" : "top",
    "bannerWidth" : "588"
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)/album/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo/albuns/top_teste",
    "outOfPage" : [ ],
    "keyword" : null,
    "rotative" : null,
    "sizeMapping" : "[0, 0], [[320, 50],[300, 50]]; [468, 0], [[468, 60],[320, 50],[300, 50]]; [768, 0], [[728, 90],[678, 175]]; [970, 0], [[970, 90],[728, 90]]",
    "forcePath" : [ ],
    "nativeType" : "0",
    "bannerWidth" : "728",
    "pos" : "top",
    "expble" : "1",
    "cookie" : null,
    "campaignuol" : "1",
    "hide" : "False",
    "fluid" : [ ],
    "isIncremental" : "true",
    "bannerId" : "banner-responsive-top-horizontal",
    "bannerHeight" : "90",
    "deslabel" : null,
    "group" : null
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo",
    "outOfPage" : [ ],
    "keyword" : null,
    "rotative" : null,
    "sizeMapping" : "[0, 0], [[320, 50],[300, 50],[300, 250]]; [468, 0], [[468, 60],[320, 50],[300, 50],[300, 250]]; [768, 0], [[728, 90],[678, 175]]; [970, 0], [[970, 90],[728, 90],[970, 250],[1261, 325]]",
    "forcePath" : [ ],
    "nativeType" : "0",
    "bannerWidth" : "728",
    "pos" : "middle",
    "expble" : "1",
    "cookie" : null,
    "campaignuol" : "1",
    "hide" : "False",
    "fluid" : [ ],
    "isIncremental" : "true",
    "bannerId" : "banner-responsive-horizontal-",
    "bannerHeight" : "90",
    "deslabel" : null,
    "group" : null
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)/album/(.*)?$/)" ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo/top_teste",
    "outOfPage" : [ ],
    "keyword" : null,
    "rotative" : null,
    "sizeMapping" : "[0, 0], [[320, 50],[300, 50]]; [468, 0], [[468, 60],[320, 50],[300, 50]]; [768, 0], [[728, 90],[678, 175]]; [970, 0], [[970, 90],[728, 90]]",
    "forcePath" : [ ],
    "nativeType" : "0",
    "bannerWidth" : "728",
    "pos" : "top",
    "expble" : "1",
    "cookie" : null,
    "campaignuol" : "1",
    "hide" : "False",
    "fluid" : [ ],
    "isIncremental" : "true",
    "bannerId" : "banner-responsive-top-horizontal",
    "bannerHeight" : "90",
    "deslabel" : null,
    "group" : null
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "DfpAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "isCompanion" : "False",
    "dfppath" : "/8804/uol/estilo",
    "outOfPage" : [ ],
    "keyword" : null,
    "rotative" : null,
    "sizeMapping" : null,
    "forcePath" : [ ],
    "bannerWidth" : "300",
    "pos" : "middle",
    "expble" : "1",
    "cookie" : null,
    "campaignuol" : "1",
    "hide" : "False",
    "fluid" : [ ],
    "isIncremental" : "true",
    "bannerId" : "banner-responsive-vertical-",
    "bannerHeight" : "600",
    "group" : null
  },
  "src" : "//tm.jsuol.com.br/modules/dfp-async.js"
}, {
  "module" : "CliquesDisplay",
  "async" : true,
  "rules" : {
    "enable" : [ ],
    "disable" : [ ]
  },
  "events" : [ "call-responsive-vitrine-320" ],
  "config" : {
    "ros" : null,
    "borderColor" : null,
    "flagRetarget" : null,
    "flagBehaviour" : null,
    "urlReferer" : null,
    "width" : "320",
    "urlColor" : null,
    "coddisplaysupplier" : "465",
    "numAds" : "2",
    "titleColor" : null,
    "adult" : [ ],
    "descrColor" : null,
    "category" : null,
    "height" : null,
    "formatId" : "[{id:106_a, weight:50},{id:106_b, weight:50}]",
    "querySelector" : "#responsive-vitrine-publicidade",
    "flagKeyword" : null,
    "deslabel" : null,
    "gp" : "noParam"
  },
  "src" : "//tm.jsuol.com.br/modules/cliques-display.js"
}, {
  "module" : "DynadVitrineShopping",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : {
    "placementId" : "121587",
    "channel" : "0",
    "samplingRate" : "100",
    "containerId" : " responsive-vitrine-publicidade",
    "dominio" : "//t5.dynad.net"
  },
  "src" : "//tm.jsuol.com.br/modules/dynad-vitrine-shopping.js"
}, {
  "module" : "CodeInjector",
  "async" : true,
  "rules" : {
    "enable" : [ "(function (){ var listSesame = ['modlistavideos', 'modflash', 'playlist-video', 'modhomevideo', 'uolplayer']; try { \tfor\t(i=0; i < listSesame.length; i++) { \t\tif(document.getElementsByClassName(listSesame[i]).length>0 ){ \t\t\treturn true;\t\t\t \t\t}\t\t \t}\t } catch(err) { \treturn false; } \treturn false; } )(); == 'true'" ],
    "disable" : [ ]
  },
  "events" : [ "onload" ],
  "config" : {
    "codeInjector" : "(function() {\n// Iframe Sesame\n\tvar ifrm = document.createElement('iframe');\n\tifrm.id = 'google_top';\n\tifrm.name = 'google_top';\n\tifrm.style.display = 'none';\n\tifrm.setAttribute('data-video-ads', 'true');\n\tdocument.body.appendChild(ifrm);\n// Script Sesame\n\tvar scr = document.createElement(\"script\");\n\tscr.async = true;\n\tscr.src = \"//pagead2.googlesyndication.com/pagead/js/google_top.js\";\n\tvar el = document.getElementsByTagName(\"script\")[0];\n\tel.parentNode.insertBefore(scr, el);\n})();"
  },
  "src" : "//tm.jsuol.com.br/modules/code-injector.js"
}, {
  "module" : "TailtargetTrack",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/^http(S)?:\\/\\/estilo\\.uol\\.com\\.br\\/(.*)?$/)" ],
    "disable" : [ ]
  },
  "events" : [ "autostart" ],
  "config" : { },
  "src" : "//tm.jsuol.com.br/modules/tailtarget-track.js"
}, {
  "module" : "ModulesInjectorAsync",
  "async" : true,
  "rules" : {
    "enable" : [ "document.location.href.match(/.*/)" ],
    "disable" : [ ]
  },
  "events" : [ ],
  "config" : {
    "javascriptSource" : "//me.jsuol.com.br/omtr/estilo-2016.js"
  },
  "src" : "//tm.jsuol.com.br/modules/modules-injector-async.js"
} ]);
})(window, document);