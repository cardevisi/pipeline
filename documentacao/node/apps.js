var gulp = require("gulp"),
    inlinesource = require('gulp-inline-source'),
    htmlreplace = require('gulp-html-replace'),
    includer = require('gulp-html-ssi'),
    argv = require('yargs').argv;

const fs = require('fs');
const path = require("path");

function recursive(srcPath)  {
    var files = fs.readdirSync(srcPath).filter(function(file) {
        console.log("AQUI", srcPath);
        
        if (fs.statSync(path.join(srcPath, file)).isDirectory()) {
            recursive(path.join(srcPath, file));
        } else {
            //console.log(srcPath);
            //console.log(srcPath, path.join(srcPath, file));
            //var templatePath = argv.input;
        }

        /*console.log(fs.statSync());
        console.log(stats.isFile());
        console.log(stats.isDirectory());
        console.log(stats.isBlockDevice());
        console.log(stats.isCharacterDevice());
        console.log(stats.isSymbolicLink());
        console.log(stats.isFIFO());
        console.log(stats.isSocket());*/
    });
}

gulp.task("build", function(){
    gulp.src([srcPath+"*.html"])
    .pipe(htmlreplace())
    .pipe(inlinesource({ compress: false }))
    .pipe(includer())
    .pipe(gulp.dest("build/"+srcPath));
  
});

gulp.task("default", ["build"]);

recursive('./src');

/*for (var i = 0; i < files.length; i++) {
    console.log(files[i]);
}*/

/*fs.readdir('.', function(obj){
    console.log('Hello', obj);
});*/

/*
gulp.task("dev", function(){
  var templatePath = argv.input;
    gulp.src([templatePath+"*.html"])
    .pipe(htmlreplace({
        'js': '//works.intranet.uol.com.br/atendimento/dynad/api/dynad-api.js'
    },{
        keepUnassigned: false,
        keepBlockTags: false,
        resolvePaths: false
    }))
    .pipe(inlinesource({ compress: false }))
    .pipe(includer())
    .pipe(gulp.dest("build/"+templatePath));
});

gulp.task("default", ["build"]);
*/


