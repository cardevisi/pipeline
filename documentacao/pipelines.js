#DynaAd Server v1.5332

**Fluxo de Retargeting de inclusão de um novo cliente:**

Cliente envia um email ou url com o catálogo de produtos.
Executar o arquivo sh ou groove para gerar os dados no Redis. *(O arquivo xml será convertido para um arquivo do tipo txt, onde os dados são persistidos)*
Acessar o terminal para verificar se os dados foram inseridos.
[Para acessar o terminal]: (http://sna.dynad.net/terminal)
*Url para consulta dos banco de dados dos clientes*

####Acessos ao servidor:

*Para testes utilizar a conta da Test.*

| Usuário 			| Senha    		| 
| --------			|---------		|	
| peo_caoliveira  	| trocar123 	|

Urls de acesso para os AdServers

[Para acessar:] (https://adc.dynad.net)
[Para acessar:] (https://a.dynad.net)

**URL para validação das pixels criadas pelo sistema.**

[Para acessar:] (https://s.dynad.net)


**Url para consulta dos banco de dados dos clientes**

[Para acessar:] (http://sna.dynad.net/datasources)

Esta arquitetura de quatro servidores foi configurada como round-robin (RR). Mais detalhes acessar (https://pt.wikipedia.org/wiki/Round-robin)

| Servidores 		| 
| --------			|
| 200.147.166.24 	| 
| 200.147.166.25 	|
| 200.147.166.26 	|
| 200.147.166.27 	|


*Exemplo de acesso a um servidor 200.147.166.24/updater*

###Assinaturas_uol:

```
http://t.dynad.net/script/?dc=5550001155;ord=[timestamp];srctype=ifrm;click=
```

###URL para capturar o valor da Businnes Role:

```
//57 => É o id da Businnes Role...
http://sna.dynad.net/eval/57?p=125008,125016,207853,207861,091367,030058,374032,373974,159140,438545
```

###Aba Sites: 

Lista os portais ou sites, parceiros, publishers.

<!-- Esta entregará o resultado do Javascrit Body -->
```
<script language="JavaScript1.1" src="http://{TRACKER}/script/?dc={AD_PLACEMENT};
	<!-- Este trecho pode ser modificicado com parâmetros customizados do cliente -->
ord=[timestamp];click=%%CLICK_URL_UNESC%%"></script>
```

```
<noscript>

<a href="%%CLICK_URL_UNESC%%http://{TRACKER}/c/?dc={AD_PLACEMENT};ord=[timestamp];c=0;srctype=bkp" target="{TARGET}">

<img src="http://{TRACKER}/serve/?dc={AD_PLACEMENT};srctype=bkp;ord=[timestamp]" border="0" alt="" width="{WIDTH}" height="{HEIGHT}" />
</a>
</noscript>
```


###Path Admin/Formato : 

Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.


###Aba Placements: 

É local onde criativo será renderizado.
Cost --> Data: 
Use location: serve para capturar as informações de posição(No globo - latitude e longitude - GEO Location) do usuário.
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.
Javascript Body: Text Area para rodar as funções que vão controlar a peça.

**Exemplo de body:**

```
function callAlert(value) {
	alert(value);
}

if(step === 2) {
 callAlert('Hello');
}
return true;
```

###Aba Creatives : 

E a peça que será impresssa ou entregue.

HTML- Static -> É necessário fazer uma chamada para API da Dynad para repasse dos dados do AdServer para o criativo (Peça cliente).
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.

###Aba Campaigns : 

Criação da campanha de veiculação.

Priority*: zero tem peso maior(Quanto menor o peso é maior).
Retargeting: Para ser habilitado é necessário ter um criativo dinâmico.
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.

###Aba Groups :

Grupo PageSeguro Moderninha
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.
A associação é feita na aba Criativos.


###Exportar Tags para enviar para o cliente:

//http://sna.dynad.net/terminal

Primeiro comando para executar dbsize (valor dividido por 2) - retorna a quantidade de dados do banco no caso - onofre
get metadata
get + sku number - retorna os dados complementares desse SKU.

```
SKU_NUMBER|1|http://static.dynad.net/let/E4S4ynwM1bDI0to8hEhLBz9ASahN7yT-0dEqQR6GHYuKOg13XMLTxjRZOWdnEKG8xXSXlLmvGQtnXKI88YZQOcgEmA0MtTXfFV5YEK02Ar4|1|Absorvente Always Básico Com Abas Malha Seca Com 08|http://www.onofre.com.br/absorvente-always-basico-com-abas-malha-seca-com-08/177/05|5,11|5,11|1|5,11|076805,357413,434949,130583,368059,358843,195448,427608,100544,236632,044725
+OK
```

```
http://sna.dynad.net/eval/57?p=125008,125016,207853,207861,091367,030058,374032,373974,159140,438545
```

###Geração do hash para gerar a imagem no tamanho correto.

http://sna.dynad.net/terminal

url para gerar o hash

Comando executado:

Helper.staticProxyEncryptParams("f=135x165")

```
<script type="text/javascript" src="http://works.intranet.uol.com.br/atendimento/dynad/api/dynad-api.js"></script>
```
###Validar se o DCO está funcionando corretamente.

No AdServer -> acessar a aba Placements e clicar em "Test Tags". Caso existam produtos traqueados pelo iruol o retargetting será exebido.


Exemplo de tags utilizadas pelos clientes:

```
<!DOCTYPE html>
<html>
	<head>
		<title>Dynad Tags</title>
	</head>
	<body>
		<iframe src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];srctype=ifrm;click=" style="width: 200px; height: 446px; border: 0;" marginwidth="0" width="200" height="446" marginheight="0" align="top" scrolling="No" bordercolor="#000000" frameborder="0" hspace="0" vspace="0">
			<script language="JavaScript1.1" src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];click="></script>
			<noscript>
				<a href="http://t.dynad.net/c/?dc=5550001083;ord=[timestamp];c=0;srctype=bkp" target="_blank">
					<img src="http://t.dynad.net/serve/?dc=5550001083;srctype=bkp;ord=[timestamp]" border="0" alt="" width="200" height="446" />
				</a>
			</noscript>
			<script language="JavaScript1.1" src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];click="></script>
			<noscript>
				<a href="http://t.dynad.net/c/?dc=5550001083;ord=[timestamp];c=0;srctype=bkp" target="_blank">
				<img src="http://t.dynad.net/serve/?dc=5550001083;srctype=bkp;ord=[timestamp]" border="0" alt="" width="200" height="446" />
			</a>
			</noscript>
		</iframe>
	</body>
</html>
```
[Link to DynaAd Server v1.5332](#DynaAd Server v1.5332)

#Criação de pixels -> MENU -> LIST 

As pixels criadas no menu LIST são utilizadas para marcar as páginas de carrinho, produto, categoria e checkout com os dados utilizados nos DCOs. Exemplos de dados são skus, ou qualquer outro dados que se deseje exibir em um criativo.