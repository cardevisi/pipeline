##Criando a variável universal a partir do DataLayer

O `dataLayer` ou camada de dados é o padrão utilizado pelo google em suas ferramentas e soluções. É utilizado para a coleta e o tráfego de informações entre sites/aplicações com gerenciadores de tags e ferramentas de análise em geral. Estes dados são utilizados para o preenchimento das tags de remarketing, redes de afiliados, mídia programática, tags de acompanhamento de conversão e por uma infinidade de aplicações. Por tratar-se de uma camada de dados largamente utilizada no mercado, no acompanhamento de sites e aplicações, principalmente no comércio eletrônico, demonstraremos através deste documento como integrá-lo aos nossos produtos e serviços.

<!-- Também pode ser definido como um conjunto de dados disponibilizados como um mapa de informações estruturadas de sua aplicação.  -->

O padrão UOL para o tráfego dessas informações é conhecido como variável universal `(window.universal_variable)` e por este motivo, será necessário que você crie a UV de duas maneiras. A primeira como explicado anteriormente na seção `Criando a Variável Universal`, você cria a UV, populando suas propriedades com os dados extraídos diretamente do seu servidor, como por exemplo os dados do seu banco de dados, de váriaveis locais de sua página, dados extraídos de elementos HTML, a escolha do método de extração fica a seu critério. A segunda opção, objetivo principal deste documento, você cria a UV, populando suas propriedades com as informações extraídas do seu `dataLayer`, isto é claro, quando o mesmo estiver presente em sua página/aplicação. Caso ele não exista, as próximas seções deste documento o conduzirão para esse entendimento. <!-- Através de nossas sugestões, exemplos práticos e referências na internet você será capaz de construir a camada de dados facilitando a integração como nossas aplicações. -->

Ainda no sentido de um maior esclarecimento sobre o que é a camada de dados, tecnicamente o `dataLayer` é um objeto global, um Array de objetos javascript, composto de pares de chaves/valor, mesmo princípio da UV, como descrito nos tópicos anteriores, cuja finalidade é descrever ou informar os dados que não estejam disponíveis como parte de um documento HTML ou em outras variáveis javascript. Para utilizar o objeto `dataLayer` em sua página você precisará inicializá-lo conforme o exemplo abaixo:

##Exemplo de inicialização do objeto dataLayer:

```js
<script>
	window.dataLayer = window.dataLayer = [];
</script>
```

O trecho de código acima atribuí a variável `window.dataLayer` a esquerda o valor da variável `window.dataLayer` que aparece logo depois do sinal de atribuição (=), isto para o caso dela já ter sido declarada em sua página, do contrário, o valor atríbuido será o de um array vazio. Feito isso você já pode popular a lista recem criada. Vamos à um exemplo prático. Imaginemos que você deseja definir variáveis de camada de dados para indicar que uma página, é um cadastro, e que o visitante é um cliente em potencial. Para isto, nós preencheremos nossa camada de dados da seguinte maneira:


```js
<script>
	window.dataLayer = window.dataLayer = [];
	window.dataLayer = [{
		"page" : {
		    "category" : "<Insira a categoria da página>"
		},
		'user' : {
		    "user_id" : "<Insira o ID do usuário logado>"
		}
	}];
</script>

<!-- Inicío da declaração da tag do UOL Tag Manager -->
<script type="text/javascript">
    (function () {
        window.universal_variable = window.universal_variable || {};
        window.universal_variable.dfp = window.universal_variable.dfp || {};
        window.uolads = window.uolads || [];
    })();
</script>
<script type="text/javascript" src="//tm.jsuol.com.br/uoltm.js?id=xxxxxx" async></script> //xxxxxx id do clinte
<!-- final da declaração da tag do UOL Tag Manager -->
```

Como desmonstrado no exemplo anterior o `dataLayer` deve ser declarado entre uma tag `<script></script>` e antes da declaração da tag do UOL Tag Manager. Essa sequência é importante pois írá garantir que no momento que a tag do UOL Tag Manager seja executada pelo navegador os dados da camada de dados já estejam visíveis para leitura.


##Mais se as informações de meus produtos e serviços estão disponíveis no meu HTML, por que devo utilizar a camada de dados? O que ganho com isso, quais são as vantagens?


Utilizando o `dataLayer` você ganha, disponibilidade, organização, confiabilidade:

- **Disponibilidade:** Os dados são disponibilizados globalmente na página do site/aplicação, podendo ser acessados e consumidos via javascript ou por ferramentas de terceiros como por exemplo o UOL Tag Manager, Google Tag Manager, Adobe DTM, etc.

- **Organização:** Com as informações organizadas de acordo com a arquitetura do site/aplicação, podem facilmente ser conumidas por diversas ferramentas.

- **Confiabilidade:** Diferente das informações capturadas diretamente no DOM, esses dados que podem vir do servidor ou de alguma interação na página, estão imunes a alterações de layout ou de conteúdo da página.

##Referências:

De acordo com a documentação do google tag manager, [veja o link](https://developers.google.com/tag-manager/enhanced-ecommerce) é recomendada a utilização do DataLayer para medir as seguintes atividades em comércio eletrônico:

- Impressão de Produtos
- Click em Produtos
- Impressões em detalhes de produtos
- Adicionar/Remover produtos do carrinho
- Impressão de promoções
- Click de promoções
- Checkout
- Purchases
- Refunds

Abaixo você encontrará exemplos de camada de dados com as propriedades minimas necessárias para a construção das UVs utilizadas aqui no UOL.

###Exemplo de um `dataLayer` para visualização de produto:

As páginas de detalhes de produtos podem ser definidas como páginas que exibem produtos únicos, páginas com maiores informações de um único produto. Para configurar o `dataLayer` você deve fazer o pushing de um `detail` conforme o exemplo abaixo:

```js
<script>
  window.dataLayer = window.dataLayer = []; 
  dataLayer.push({
    'ecommerce': {
      'currencyCode': 'BRL', //Código de moeda
      'detail': {
        'products': [{
          'name': 'Nome', //Nome ou descrição do produto.
          'id': '12345', //Código do produto ou SKU.
          'price': '15.25', //Preço do produto.
          'brand': 'Marca', //Marca do produto.
          'category': 'Categoria', //Categoria do produto.
          'variant': 'Gray' //Informações adicionais ou variáveis.
        }]
       }
     }
  });
</script>
```

###Exemplo de um `dataLayer` para páginas de carrinho de compras:

Páginas de Carrinho de compras são conhecidas como páginas onde são listados os produtos que foram adicionados ao carrinho, que foram selecionados para compra.

```js
<script>
  dataLayer.push({
    'event': 'addToCart',
    'ecommerce': {
      'currencyCode': 'BRL', //Código de moeda
      'add': {               // 'add' ação de adicionar ao carrinho.
        'products': [        //  lista de produtos adicionados ao carrinho.
          {                        
            'name': 'Nome', //Nome do produto.
            'id': '12345', //Código do produto ou SKU.
            'price': '15.25', //Preço do produto
            'brand': 'Marca', //Marca do produto
            'category': 'Categoria', //Categoria do produto
            'variant': 'Cinza', //Informações que variam como por exemplo a cor de um produto
            'quantity': 1 //Quantidade do produto
          },
          {                        //  lista de produtos adicionados ao carrinho.
            'name': 'Nome', //Nome do produto.
            'id': '12345', //Código do produto ou SKU.
            'price': '15.25', //Preço do produto
            'brand': 'Marca', //Marca do produto
            'category': 'Categoria', //Categoria do produto
            'variant': 'Cinza', //Informações que variam como por exemplo a cor de um produto
            'quantity': 1 //Quantidade do produto
          }
        ]
      }
    }
  });
</script>
```

###Exemplo de um `dataLayer` para páginas de checkout:

Páginas de Checkout de checkout, página caracterizada pela conclusão de uma compra.

```js
<script>
dataLayer.push({
    'event': 'checkout',
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 1, 'option': 'Visa'},
        'products': [{
          'name': 'Triblend Android T-Shirt',
          'id': '12345',
          'price': '15.25',
          'brand': 'Google',
          'category': 'Apparel',
          'variant': 'Gray',
          'quantity': 1
       }]
     }
   },
   'eventCallback': function() {
      document.location = 'checkout.html';
   }
  });
  </script>
```

##Dados da transação:

| Nome da variável					| Descrição								|	Tipo 						 |
| --------------------------------- | ------------------------------------- | -----------------------------	 |
| transactionId (obrigatório)		| Identificador exclusivo da transação	|	string 						 |	 
| transactionAffiliation (opcional)	| Parceiro ou loja						|	string 						 |
| transactionTotal (obrigatório)	| Valor total da transação				|	numérico 					 |
| transactionShipping (opcional) 	| O custo do frete para a transação		|	numérico 					 |
| transactionTax (opcional) 		| O valor do imposto da transação		|	numérico 					 |
| transactionProducts (opcional)    | Lista de itens comprados na transação	|	matriz de objetos de produto |

##Dados do produto:

| Nome da variável		 | Descrição			|	Tipo 			    | 
| ---------------------- | -------------------- | --------------------- |
| name (obrigatório)	 | Nome do produto		| string 				|
| sku (obrigatório)		 | SKU do produto		| string 				|
| category (opcional) 	 | Categoria do produto	| string 				|
| price (obrigatório)	 | Preço unitário		| numérico 				|
| quantity (obrigatório) | Número de itens		| numérico 				|


##Exemplo de um dataLayer para evento de visualização de produto

```js
<script>
  window.dataLayer = window.dataLayer = [];
	dataLayer.push({
      "ecommerce":{
	  		"detail":{
	  			"products":[{
	  				"name":'Viña Amalia Reserva Malbec 2012',
	  				"id":'12217',
	  				"price":'73.00',
	  				"brand":'Viña Amalia',
	  				"category":'Vinhos'
	  			}]
	  		}
	  	}
	});
</script>
```

Veja um exemplo genérico da camada de dados implementada em javascript:

####DataLayer para a página de home:

```js
<script>
	window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        }
    });
</script>
```

###DataLayer para a página de produto:

```js
<script>
	window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>"
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>"
                    }
                }
            ]
        }
    });
</script>
```

###DataLayer para a página de produto:

```js
<script>
	window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "product": {
            "sku_code": "<sku>",
            "description": "Description about this product",
            "category": "Shoe",
            "subcategory": "Heels",
            "unit_price": 130,
            "currency": "GBP"
        }
    });
</script>
```

###DataLayer para a página de categorias:

```js
<script>
	window.dataLayer = window.dataLayer || [];
	window.datalayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "listing": {
            "items": [
                {
                    "sku_code": "<sku>",
                    "description": "Description about this product",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 130
                },
                {
                    "sku_code": "<sku2>",
                    "description": "Another description",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 150
                }
            ]
        }
    });
</script>
```

###DataLayer para a página de resultados de Busca:

```js
<script>
	window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "listing": {
            "items": [
                {
                    "sku_code": "<sku>",
                    "description": "Description about this product",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 130
                },
                {
                    "sku_code": "<sku2>",
                    "description": "Another description",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 150
                }
            ]
        }
    });
</script>
```

###DataLayer para a página de carrinho:

```js
<script>   
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "currency": "GBP",
            "total": 280,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>",
                        "description": "Description about this product",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 130
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>",
                        "description": "Another description",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 150
                    }
                }
            ]
        }
    });
</script>
```

###DataLayer para a página de carrinho:

```js
<script>
    window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "currency": "GBP",
            "total": 280,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>",
                        "description": "Description about this product",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 130
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>",
                        "description": "Another description",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 150
                    }
                }
            ]
        }
    });
</script>
```

###DataLayer para a página de carrinho:

```js
<script>
	window.dataLayer = window.dataLayer || []
	dataLayer.push({
	    'transactionId': '1234',
	    'transactionAffiliation': 'Roupas Acme',
	    'transactionTotal': 38.26,
	    'transactionTax': 1.29,
	    'transactionShipping': 5,
	    'transactionProducts': [{
	       'sku': 'DD44',
	       'name': 'Camiseta',
	       'category': 'Vestuário',
	       'price': 11.99,
	       'quantity': 1
	    },
	    {
	       'sku': 'AA1243544',
	       'name': 'Meias',
	       'category': 'Vestuário',
	       'price': 9.99,
	       'quantity': 2
	    }]
	});
</script>
```

###DataLayer para a página de Checkout:

```js
<script>    
  window.dataLayer = window.dataLayer || [];
	dataLayer.push({
        "page" : {
            "category" : "<Insira a categoria da página>"
        },
        "user" : {
            "user_id" : "<Insira o ID do usuário logado>"
        },
        "basket": {
            "currency": "Insira a moeda em que o valor está cotado",
            "total": Insira o valor total da compra em numerais,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<Insira o sku do produto>",
                        "description": "Insira a descrição do primeiro produto exibida na tela",
                        "category": "Insira a categoria dos produtos exibidos na página",
                        "subcategory": "Insira a sub-categoria dos produtos exibidos na página, caso haja",
                        "unit_price": Insira o valor do primeiro produto em numerais
                    }
                },
                {
                    "product": {
                        "sku_code": "<Insira o sku do produto>",
                        "description": "Insira a descrição do segundo produto exibida na tela",
                        "category": "Insira a categoria dos produtos exibidos na página",
                        "subcategory": "Insira a sub-categoria dos produtos exibidos na página, caso haja",
                        "unit_price": Insira o valor do segundo produto em numerais
                    }
                }
            ]
        }
    });
</script>
```

```js
<script>
	window.dataLayer = window.dataLayer || []    
    window.dataLayer.push({
        "page" : {
            "category" : "<Insira a categoria da página>"
        },
        "user" : {
            "user_id" : "<Insira o ID do usuário logado>"
        },
        "transaction": {
            "order_id": "Insira o ID da transação",
            "currency": ""Insira a moeda em que o valor está cotado",
            "payment_type": "Insira o método de pagamento utilizado",
            "total": Insira o valor total da compra em numerais,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<Insira o sku do produto>",
                        "description": "Insira a descrição do primeiro produto exibida na tela",
                        "category": "Insira a categoria dos produtos exibidos na página",
                        "subcategory": "Insira a sub-categoria dos produtos exibidos na página, caso haja",
                        "unit_price": Insira o valor do primeiro produto em numerais
                    }
                },
                {
                    "product": {
                        "sku_code": "<Insira o sku do produto>",
                        "description": "Insira a descrição do segundo produto exibida na tela",
                        "category": "Insira a categoria dos produtos exibidos na página",
                        "subcategory": "Insira a sub-categoria dos produtos exibidos na página, caso haja",
                        "unit_price": Insira o valor do segundo produto em numerais
                    }
                }
            ]
        }
    });
</script>
```

**Referências:**

https://medium.com/analytics-talks/vantagens-de-utilizar-informa%C3%A7%C3%B5es-na-camada-de-dados-datalayer-a1709f36b025
https://medium.com/analytics-talks/vantagens-de-utilizar-informa%C3%A7%C3%B5es-na-camada-de-dados-datalayer-a1709f36b025
https://support.criteo.com/hc/pt/articles/204851311-Google-Tag-Manager-Vari%C3%A1veis
http://www.dp6.com.br/o-que-e-a-camada-de-dados-ou-data-layer/
https://support.google.com/tagmanager/answer/6107169?hl=pt-BR
https://developers.google.com/tag-manager/enhanced-ecommerce









