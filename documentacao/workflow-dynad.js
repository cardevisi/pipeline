#DynaAd Server v1.5332

**Fluxo de Retargeting de inclusão de um novo cliente:**

Cliente envia um email ou url com o catálogo de produtos.
Executar o arquivo sh ou groove para gerar os dados no Redis. *(O arquivo xml será convertido para um arquivo do tipo txt, onde os dados são persistidos)*
Acessar o terminal para verificar se os dados foram inseridos.
[Para acessar o terminal]: (http://sna.dynad.net/terminal)
*Url para consulta dos banco de dados dos clientes*

####Acessos ao servidor:

*Para testes utilizar a conta da Test.*

| Usuário 			| Senha    		| 
| --------			|---------		|	
| peo_caoliveira  	| trocar123 	|

Urls de acesso para os AdServers

[Para acessar:] (https://adc.dynad.net)
[Para acessar:] (https://a.dynad.net)

**URL para validação das pixels criadas pelo sistema.**

[Para acessar:] (https://s.dynad.net)

#Cadastro fake utilizado para passar pela etapa do carrinho.

****** Dados para contas de teste

Email: danyd.etste@gmail.com : dynad123
CPF: 021.555.544-94
RG: 429434121
Nome: Teste dos Santos
Nascimento: 01/01/1990
Cep: 05835-004
Celular: (11) 98096-0457
Endereço: Estrada de Itapecerica 1000, Vila Prel, São Paulo SP

#MASTER KEY

Themis Vassiliadis: o ecommerce pode passar ai a chave q identifica este usuário no banco de dados dele
Edited 14:34:20] Themis Vassiliadis: a ideia era construir um mecanismo q pudesse montar uma base cross-device
Themis Vassiliadis: de forma colaborativa
Themis Vassiliadis: ou seja... se a pixel passa o ID x no desktop e depois repete o mesmo ID no celular... agente ligaria os dois cookies em um só
Themis Vassiliadis: mas não vingou
Themis Vassiliadis: resumindo... esse campoi pode ser deixado em branco

**Url para consulta dos banco de dados dos clientes**

[Para acessar:] (http://sna.dynad.net/datasources)

Esta arquitetura de quatro servidores foi configurada como round-robin (RR). Mais detalhes acessar (https://pt.wikipedia.org/wiki/Round-robin)

| Servidores Antigos | 
| --------			 |
| 200.147.166.24 	 | 
| 200.147.166.25 	 |
| 200.147.166.26 	 |
| 200.147.166.27 	 |


| Servidores 		| 
| --------			|
http://d3-asap-supernova-fe1/datasources
http://d3-asap-supernova-fe2/datasources
http://d3-asap-supernova-fe3/datasources
http://d3-asap-supernova-fe4/datasources
http://d3-asap-supernova-fe6/datasources
http://d3-asap-supernova-fe7/datasources
http://d3-asap-supernova-fe8/datasources 	|


*Exemplo de acesso a um servidor 200.147.166.24/updater*

###Assinaturas_uol:

```
http://t.dynad.net/script/?dc=5550001155;ord=[timestamp];srctype=ifrm;click=
```

###URL para capturar o valor da Businnes Role:

```
//57 => É o id da Businnes Role...
http://sna.dynad.net/eval/57?p=125008,125016,207853,207861,091367,030058,374032,373974,159140,438545
```

Para saber qual business role está sendo utilizada no momento

http://sna.dynad.net/eval/1000001?p=16903&site=80044&cachebuster=1467379299223

###Aba Sites: 

Lista os portais ou sites, parceiros, publishers.

<!-- Esta entregará o resultado do Javascrit Body -->
```
<script language="JavaScript1.1" src="http://{TRACKER}/script/?dc={AD_PLACEMENT};
	<!-- Este trecho pode ser modificicado com parâmetros customizados do cliente -->
ord=[timestamp];click=%%CLICK_URL_UNESC%%"></script>
```

```
<noscript>

<a href="%%CLICK_URL_UNESC%%http://{TRACKER}/c/?dc={AD_PLACEMENT};ord=[timestamp];c=0;srctype=bkp" target="{TARGET}">

<img src="http://{TRACKER}/serve/?dc={AD_PLACEMENT};srctype=bkp;ord=[timestamp]" border="0" alt="" width="{WIDTH}" height="{HEIGHT}" />
</a>
</noscript>
```


###Path Admin/Formato : 

Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.


###Aba Placements: 

É local onde criativo será renderizado.
Cost --> Data: 
Use location: serve para capturar as informações de posição(No globo - latitude e longitude - GEO Location) do usuário.
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.
Javascript Body: Text Area para rodar as funções que vão controlar a peça.

**Exemplo de body:**

```
function callAlert(value) {
	alert(value);
}

if(step === 2) {
 callAlert('Hello');
}
return true;
```

###Aba Creatives : 

E a peça que será impresssa ou entregue.

HTML- Static -> É necessário fazer uma chamada para API da Dynad para repasse dos dados do AdServer para o criativo (Peça cliente).
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.

###Aba Campaigns : 

Criação da campanha de veiculação.

Priority*: zero tem peso maior(Quanto menor o peso é maior).
Retargeting: Para ser habilitado é necessário ter um criativo dinâmico.
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.

###Aba Groups :

Grupo PageSeguro Moderninha
Obs: Meta para ser configurado um valor que poderá ser capturado no momento do traqueamento.
A associação é feita na aba Criativos.


###Exportar Tags para enviar para o cliente:

//http://sna.dynad.net/terminal

Primeiro comando para executar dbsize (valor dividido por 2) - retorna a quantidade de dados do banco no caso - onofre
get metadata
get + sku number - retorna os dados complementares desse SKU.

```
SKU_NUMBER|1|http://static.dynad.net/let/E4S4ynwM1bDI0to8hEhLBz9ASahN7yT-0dEqQR6GHYuKOg13XMLTxjRZOWdnEKG8xXSXlLmvGQtnXKI88YZQOcgEmA0MtTXfFV5YEK02Ar4|1|Absorvente Always Básico Com Abas Malha Seca Com 08|http://www.onofre.com.br/absorvente-always-basico-com-abas-malha-seca-com-08/177/05|5,11|5,11|1|5,11|076805,357413,434949,130583,368059,358843,195448,427608,100544,236632,044725
+OK
```

```
http://sna.dynad.net/eval/57?p=125008,125016,207853,207861,091367,030058,374032,373974,159140,438545
```

###Geração do hash para gerar a imagem no tamanho correto.

http://sna.dynad.net/terminal

url para gerar o hash

Comando executado:

Helper.staticProxyEncryptParams("f=135x165")

```
<script type="text/javascript" src="http://works.intranet.uol.com.br/atendimento/dynad/api/dynad-api.js"></script>
```
###Validar se o DCO está funcionando corretamente.

No AdServer -> acessar a aba Placements e clicar em "Test Tags". Caso existam produtos traqueados pelo iruol o retargetting será exebido.


Exemplo de tags utilizadas pelos clientes:

```
<!DOCTYPE html>
<html>
	<head>

		<title>Dynad Tags</title>
	</head>
	<body>
		<iframe src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];srctype=ifrm;click=" style="width: 200px; height: 446px; border: 0;" marginwidth="0" width="200" height="446" marginheight="0" align="top" scrolling="No" bordercolor="#000000" frameborder="0" hspace="0" vspace="0">
			<script language="JavaScript1.1" src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];click="></script>
			<noscript>
				<a href="http://t.dynad.net/c/?dc=5550001083;ord=[timestamp];c=0;srctype=bkp" target="_blank">
					<img src="http://t.dynad.net/serve/?dc=5550001083;srctype=bkp;ord=[timestamp]" border="0" alt="" width="200" height="446" />
				</a>
			</noscript>
			<script language="JavaScript1.1" src="http://t.dynad.net/script/?dc=5550001083;ord=[timestamp];click="></script>
			<noscript>
				<a href="http://t.dynad.net/c/?dc=5550001083;ord=[timestamp];c=0;srctype=bkp" target="_blank">
				<img src="http://t.dynad.net/serve/?dc=5550001083;srctype=bkp;ord=[timestamp]" border="0" alt="" width="200" height="446" />
			</a>
			</noscript>
		</iframe>
	</body>
</html>
```
[Link to DynaAd Server v1.5332](#DynaAd Server v1.5332)


#Configurando a expansão do placement 

```
if (step == 2 && type == "H") {
	var content = document.querySelector('#_dynad_c_'+reqid);  
	content.onmouseover = function(){  
	window['iframe' + reqid].style.width = '600px';
	window['iframe' + reqid].width = '600';   
	window['iframe' + reqid].style.height = '300px';
	window['iframe' + reqid].height = '300';      
 }
 
 content.onmouseout =function(){
	window['iframe' + reqid].style.width = '300px';
	window['iframe' + reqid].width = '300';
	window['iframe' + reqid].style.height = '250px';
	window['iframe' + reqid].height = '250';
 }

	window['iframe' + reqid] = document.querySelector('#' + reqid.replace('I', 'IF'));
	window['iframe' + reqid].style.position = 'absolute';
	window['iframe' + reqid].style.top = '0';
	window['iframe' + reqid].style.right = '0';
}

return true;
```

#Servidores dynad:

https://adc.dynad.net/ equivale ao t5.dynad.net
https://a.dynad.net/ equivale ao t.dynad.net


#Para forçar a criação de skus válidos no localstorage:

A querystring c /c=17692/ é o valor de um sku, como que o gerado no site do cliente.

```
<script type="text/javascript" src="http://t5.dynad.net/lsep/?l=100&ord=[timestamp]&c=17692&k=[MASTER KEY]" >
```

#Para forçar o RT em um placement:

placements -> 

Habilitar o Double Stage:

Os valores 17692,17779,17755 são valores fixos de skus de clientes, neste exemplo os skus são de SafariShop.

```
loader("//sna.dynad.net/eval/"+businessId+"?p=17692,17779,17755",
    function(e){
      if(e != "") {
        nextStage(true, e);
      } else {
        nextStage(false);
      }
    }, 
    function(e){nextStage(false);}
);
```

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	 <!-- <script type="text/javascript" src="http://t5.dynad.net/lsep/?l=100&ord=[timestamp]&c=17692&k=[MASTER KEY]" ></script>
	 <script type="text/javascript" src="http://t5.dynad.net/lsep/?l=100&ord=[timestamp]&c=17779&k=[MASTER KEY]" ></script>
	 <script type="text/javascript" src="http://t5.dynad.net/lsep/?l=100&ord=[timestamp]&c=17755&k=[MASTER KEY]" ></script> -->
	<div style="position:absolute; left:1000px;">
		<script language="JavaScript1.1" src="http://t5.dynad.net/script/?dc=130217;ord=%%CACHEBUSTER%%;click="></script>
	</div>
</body>
</html>

```

#Criação de pixels -> MENU -> LIST 

As pixels criadas no menu LIST são utilizadas para marcar as páginas de carrinho, produto, categoria e checkout com os dados utilizados nos DCOs. Exemplos de dados são skus, ou qualquer outro dados que se deseje exibir em um criativo.

#Validação de XML:

Abaixo seguem o nós necessários nos xmls para qualquer cliente. Uma vez feita a validação do mesmo, utiliza-se o Digester para fazer a leitura e o captura dos dados.

#Software utilizado para edição/leitua do xml FIRSTOBJECT XML EDITOR

<> PRODUTO

<> NOME/TITLE 
<> SKU/ID
<> PRECO
<> PRECO_PROMOCIONAL    	NOBRIGATORIO
<> NUMERO PARCELA           NOBRIGATORIO
<> VALOR PARCELA            NOBRIGATORIO
<> URL_IMAGEM
<> CATEGORIAS        		(No geral utiliza-se somente três categorias. Essa categorias são utilizadas para gerar as recomendações)
<> LINK
<> MARCA   					NOBRIGATORIO

1) Validação do xml
2) Criação do banco de dados Ex. nome: dynad_wine

Acessar o mysql com o comando: 

cmd -> mysql -u root (mysql -u root -p -h localhost  => acesso com senha)
cmd -> create database dynad_wine;
cmd -> use dynad_wine;
cmd -> source estrutura.sql
cmd -> source normaliza_moeda.sql (Aqui o mysql vai gerar um erro, para corrigir o problema executar o comando abaixo)

Em produção
cmd -> mysql -u root -p phenom02

```
-----------------------------------------------------
Execute the following in the MySQL console:
-----------------------------------------------------

mysql> SET GLOBAL log_bin_trust_function_creators = 1;

-----------------------------------------------------
Add the following to the mysql.ini configuration file:
-----------------------------------------------------

log_bin_trust_function_creators = 1;

```

3) Geração do arquivo .groovy (Etapa feita localmente Ex. nome DigesterWine.groovy)

cmd -> groovy DigesterWine.groovy

4) Geração do arquivo .sh para rodar na maquina em produção

**Atenção total para o nome e o ID do banco.

1) Solicitar o xml
2) Validar o xml
3) Criação do Customer
4) Criação dos usuários
5) Criação dos  formatos
6) Criação do Site
7) Criar Placements 
8) Criação dos Criativos - Aqui são necessários os arquivos PSDs para geração dos DCOs.
9) Criação da Campanha (É o mesmo que ADS)
10) Criação do grupo (É o mesmo que Campanha)

#Localstoge -> Formato da Key (Chave)

142 - produtos
144 - sold

  Código de cliente	    SKU

d20160119p   | [{"v":"1","t":1453410961}]

v - Valor do SKU

C - Carrinho
c - Categoria
s - sold
p - produto

t- timestamp


###Acesso da maquina de produção da Dynad

d3-asap-digester1
asapcode

//**********************************
//#### Acesso para máquina de QA
//**********************************

Galera,

Nossa máquina de QA já está funcionando, conseguimos realizar testes, instalar programas etc, os dados da máquina são:

Host: d3-techops-qa1
Ip: 10.138.224.239
 
User: techops
Senha: a

Lembrando que o acesso é via ssh.


Qualquer coisa é só falar.



### Fluxo de validação de UOL Cliques e Barra de Ofertas

Barra de Ofertas:
[URL de valição Barra de ofertas:] (http://iruol.dynad.net/wildcard/endpoints/mock/503/1/3nuVWn8LdauUL1Cn5E97mA)

UOL Cliques:
[URL de valicação UOL Cliques:] (http://uolcliques.dynad.net/wildcard/API/interfaces/iframe/mock/503/1/3nuVWn8LdauUL1Cn5E97mA?reqid=1454620535559)

[URL de retargeting de UOL Cliques para uso em produção:] (http://uolcliques.dynad.net/wildcard/API/interfaces/iframe/uolcliquesv3/503/1/3nuVWn8LdauUL1Cn5E97mA?reqid=1454620535559)[URL de retargeting de UOL Cliques para uso em produção:] (http://uolcliques.dynad.net/wildcard/API/interfaces/iframe/uolcliquesv3/503/1/3nuVWn8LdauUL1Cn5E97mA?reqid=1454620535559)

Para forçar um tracking do lado da Dynad pode-se utilizar uma chamada como esta. (forcar DynadTrack).
http://iruol.dynad.net/wildcard/tracking/?page=product&customer=2015081102&ord=1455851927530&sku=173967

DFP Premium access:

usuário: cin_coliveira@uolinc.com
senha: Casi@Uol1010


/*********************************/
Dyand Pixel como funciona o repasse das informações de valores por request:
/*********************************/
neste exemplo, multiplicamos por 100, ficando assim:
http://t5.dynad.net/lsep/?l=96&ord=[timestamp]&c=465465;46465&k=&v=4400
se for uma campra de R$1,00 
http://t5.dynad.net/lsep/?l=96&ord=[timestamp]&c=465465;46465&k=&v