> ###Workflow para validação de Richmedia no UOL:

#####Acessos necessários:


#####Repositório com todo o material já veiculado nos últimos anos e outros coisas como fontes, adsGenerator...
[Para Acessar:] (https://svnuol.intranet/insercao)
*SVN - necessária a instalação do TortoiseSVN*

#####Repositório com os novos templates HTML5
[Para Acessar:] (https://stash.uol.intranet (Ferramenta para gerenciamento de repositórios)
[Para Acessar:] (https://stash.uol.intranet/scm/pub-dallas/richmedia-dfp-templates.git)

#####Passos:

**1) A solicitação virá através do sistema de tickets. A url abaixo da acesso ao sistema:**

[Para acessar]: (https://ticket.insercao.intranet/)
*A permissão de acesso é dada pelo Adriano*

**2) Validação dos materiais recebidos:**

Geralmente os materiais recebidos do cliente são anexados no próprio ticket e costumam vir no formato zip.

**O processo de validação deve contemplar os seguintes passos:**

Para a maioria dos formatos veiculados duas peças são necessárias. Peça fechada, formato inicial e peça aberta, formato expandido.

#####Os principais formatos das peças fechadas são:

* 300x250 
* 728x90 
* 200x446 
* 300x600 
* 1190x70

O formato da peça aberta pode variar: Glider, Mosaico, Barra, Viewport, Tab...
*Neste endereço vocês encontrarão toda a documentação que envolvem os formatos comercializados pelo UOL.*

[Para acessar:] (http://www.publicidade.uol.com.br)

Validar visualmente se o comportamento das peças estão de acordo com o esperado. A peça é renderizada nos navegadores? 
Se possuir vídeo, o áudio está funcionando corretamente? 
Verificar se a clickTag foi implementada.
Ou qualquer interação do funcionalidade esperada.
O que faltar podemos corrigir se for simples ou solicitar que o cliente faça a correção.

#####Como testar local a veiculação de um richmedia:

1) Baixar o markup da página a ser veiculada
2) Alterar a chamada da função TM.Display ou funcão de carregamento de banner conforme o exemplo abaixo:
3) A identificação das posições dos banners no UOL seguem a nomenclatura abaixo:

#####Na Home por exemplo:

O banner id -> banner-300x250-5 -> exibirá um banner com o formato de 300x250 (Arroba)
O banner id -> banner-200x446-6 -> exibirá um banner com o formato de 200x446 (Barra)

Configuração padrão de uma chamada de banner no UOL (Lembrando que essa chamada pode ser modificada....)

<div id="banner-300x250-5">
	<script type="text/javascript">TM.display();</script>
</div>

Configuração para rodar um formato local:

```
<div id="banner-300x250-5">
	<script src="template-nome-do-temlate.js"></script>
	<script>
		(function() {
			template_reposition = {};

			template_reposition.id = "banner-300x250-5";
			template_reposition.block = document.getElementById("banner-300x250-5");
			template_reposition.area = document.getElementById("banner-300x250-5-Area"); //Este posição id também pode variar (Area || area)....
			TM.displayedBanners.push(template_reposition);
		})();
	</script>
</div>
```

####Solicitar para o Adriano o acesso ao webdrive....

W:\public_html\teste\richmedia\2015


Cookies utilizados nos templates do DFP:

template DHTML : POPhomeUOL
template Cutting: CEHome