Para executar o tm-stc local:
 
ter o seguinte vhost:
 
<VirtualHost *:80>
    ServerName tm.uol.com.br
    RewriteEngine on
    RewriteMap lowercase int:tolower
    RewriteCond %{HTTP_HOST} [A-Z]
    RewriteRule (.*) [http://$%7blowercase:%25%7bHTTP_HOST%7d%7d$1]http://${lowercase:%{HTTP_HOST}}$1 [R,L]
    RewriteRule (.*\.js)   http://tm.uol.com.br:8080/vm_tagmanager.js?ORIGINAL_PATH=$1&%{QUERY_STRING} [NE,P,L]
    RewriteRule (.*\.html) http://tm.uol.com.br:8080/vm_tagmanager.html?ORIGINAL_PATH=$1&%{QUERY_STRING} [NE,P,L]
</VirtualHost>
 
Instalar o template-cache na sua máquina: https://cms.wiki.intranet/index.php/TemplateCache:TemplateCache_Local
 
Criar um link simbólico da pasta ../tag-manager/tm-stc/src/main/resources/tm.uol.com.br para dentro da pasta templates que estará dentro do template-cache e executar o comando run.sh ou run.bat se estiver no Windows e adicionar o seguinte no seu arquivo de hosts:

Exemplo de comando para criação do link simbolico.

MKLINK /D DIRETORIO DESTINO PATH COMPLETO  DIRETORIO ORIGEM PATH COMPLETO
MKLINK /D C:\CASIFILES\WORKS\stg\templatecache-local\templates\tm.uol.com.br C:\CASIFILES\WORKS\stg\tag-manager\tm-stc\src\main\resources\tm.uol.com.br
 
#
# TAG MANAGER
#
127.0.0.1           tm.uol.com.br
#
# TAG MANAGER STG
#10.133.10.160   tm.uol.com.br #a1-htcresource-s-prt1
#10.133.10.161   tm.jsuol.com  #a1-htcresource-s-prt1
 
# TAG MANAGER QA
#10.133.135.141  tm.uol.com.br #a1-htcresource-q-prt1
#10.133.135.142  tm.jsuol.com  #a1-htcresource-q-prt1
