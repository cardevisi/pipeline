//*********************************************************
//	Cookies de bidding:
//*********************************************************

O Plugin Dynad Tracking possui uma configuração chamada cookies de bidding. Esta configuração é utilizada pelo pessoal do UOL Cliques para fazer a amarração dos cookies com os ids do lado do siga.

//*********************************************************
			IDS no siga 			Cookies de Bidding
//*********************************************************

Categoria: 	52						30
Produto: 	53						30.1
Carrinho: 	54						30.2

Essa amarração é feita no sistema:

//*********************************************************
// http(s)?:\/\/(www\.)?dafiti.com.br\/(.+)\.html
//*********************************************************

###Documentação de sobre blocos de anuncio no UOL CLiques

[documentação para os formatos de uol cliques] (https://confluence.intranet.uol.com.br/confluence/pages/viewpage.action?pageId=38078574)


1) Criar cookie com o tempo de expiração da janela.
2) Identificar o cookie com a janela de tempo.
3) Chamar a url de rt com o código - http://uolcliques.dynad.net/uolcliques/API/interfaces/iframe/uolcliquesv3/503/1/3nuVWn8LdauUL1Cn5E97mA
4) 

o	- Carrinho 1 dia - 2013103001 - 20130818
o	- Carrinho 3 dias - 2013103001 - 20130818
o	- Carrinho 7 dias ou mais - 2013103001 - 20130818


o	- Produto 1 dia
o	- Produto 3 dias
o	- Produto 7 dias ou mais

o	- Categoria 1 dia
o	- Categoria 3 dias
o	- Categoria 7 dias ou mais
