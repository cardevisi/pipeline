#!/bin/sh
# This is a comment
#echo "List of files:"
#EXP_VAR="Hello"
#echo EXP_VAR: ${EXP_VAR}

# Load in the git branch prompt script.
source "$HOME/.git-prompt.sh" #source ~/.git-prompt.sh
source "$HOME/.git-completion.bash" #source ~/.git-completion.bash

#PIK
[[ -s $USERPROFILE/.pik/.pikrc ]] && source $USERPROFILE/.pik/.pikrc

# Plugin Maker - [Tag Manager]
if [[ -f "$HOME/.plugin-maker.sh" ]]; then
    . "$HOME/.plugin-maker.sh";
fi

#********************************************************************************
#
# HUMANIZE
#
#********************************************************************************
# COLORS
#********************************************************************************
# @usage: printf "${Bla}black ${Red}red ${NC} ...\n"
#********************************************************************************
# Regular         Bold               Underline          High Intensity     BoldHigh Intens..   Background         High Intensity Bgs
Yel='\e[0;33m';   BYel='\e[1;33m';   UYel='\e[4;33m';   IYel='\e[0;93m';   BIYel='\e[1;93m';   On_Yel='\e[43m';   On_IYel='\e[0;103m';
Pur='\e[0;35m';   BPur='\e[1;35m';   UPur='\e[4;35m';   IPur='\e[0;95m';   BIPur='\e[1;95m';   On_Pur='\e[45m';   On_IPur='\e[0;105m';
Bla='\e[0;30m';   BBla='\e[1;30m';   UBla='\e[4;30m';   IBla='\e[0;90m';   BIBla='\e[1;90m';   On_Bla='\e[40m';   On_IBla='\e[0;100m';
Gre='\e[0;32m';   BGre='\e[1;32m';   UGre='\e[4;32m';   IGre='\e[0;92m';   BIGre='\e[1;92m';   On_Gre='\e[42m';   On_IGre='\e[0;102m';
Whi='\e[0;37m';   BWhi='\e[1;37m';   UWhi='\e[4;37m';   IWhi='\e[0;97m';   BIWhi='\e[1;97m';   On_Whi='\e[47m';   On_IWhi='\e[0;107m';
Blu='\e[0;34m';   BBlu='\e[1;34m';   UBlu='\e[4;34m';   IBlu='\e[0;94m';   BIBlu='\e[1;94m';   On_Blu='\e[44m';   On_IBlu='\e[0;104m';
Cya='\e[0;36m';   BCya='\e[1;36m';   UCya='\e[4;36m';   ICya='\e[0;96m';   BICya='\e[1;96m';   On_Cya='\e[46m';   On_ICya='\e[0;106m';
Red='\e[0;31m';   BRed='\e[1;31m';   URed='\e[4;31m';   IRed='\e[0;91m';   BIRed='\e[1;91m';   On_Red='\e[41m';   On_IRed='\e[0;101m';

#LS_COLORS='di=37;97;44:fi=0;32:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90'
#export LS_COLORS

#echo "Please enter some input: "
#read input_variable
#echo "You entered: $input_variable"
# alias g='find /X -name ".git"'
# for var in ${g[@]}
# do
# 	#command1 on $var
#  	#command2 on $var
#  	echo ${var}
# done

# alias bk='';
# for var in ${bk[@]}
# do
# 	#command1 on $var
#  	#command2 on $var
#  	echo ${var}
# done

#g='find /X -name .git'
#for repo in ${g[@]}
#do
#     cd ${repo}
#     cd ..
#     git pull
# done


#********************************************************************************
#Back navigation folders
#********************************************************************************
b() {
	if [ "$1" -gt "10" ] # [-gt] is greater than
	then
		return;
	fi
	
	for ((i=0; i<$1 ; i++));
	do
	   	cd ..;
	   	total=$((i+1));
	done
	echo 'back' $total 'times...';
}

u() {
	if [ "$1" -gt "10" ] # [-gt] is greater than
	then
		return;
	fi
	
	for ((i=0; i<$1 ; i++));
	do
	   cd -;
	done
}

bitc() {
	
	username=$1;
	password=$2;
	repository_name=$3;
	dir_name=`basename $(pwd)`;

	if [ "$username" == "" ]; then
		echo "Enter with your username:";
		read -s username;
	fi

	if [ "$username" == "" ]; then
		echo "Exited! Your username is empty";
		return;
	fi

	if [ "$password" == "" ]; then
		echo "Enter with your password:";
		read -s password; # read -s to password
	fi

	if [ "$password" == "" ]; then
		echo "Exited! Your password is empty";
		return;
	fi

	if [ "$repository_name" = "" ]; then 
		echo "Repo name (hit enter to use '$dir_name')?";
		read repository_name;
	fi

	if [ "$repository_name" = "" ]; then 
		repository_name="$dir_name";
	fi

	exec_curl='curl --user '"$username"':'"$password"' https://api.bitbucket.org/1.0/repositories/ --data name='"$repository_name";
	exec_git_a='git init';
	exec_git_b='git remote add origin https://'"$username"'@bitbucket.org/'"$username"'/'"$repository_name"'.git';
	exec_sh_a='touch .gitconfig';
	
	echo `$exec_curl`;
	echo `$exec_git_a`;
	echo `$exec_git_b`;
	echo `$exec_sh_a`;

}

hublist() {
	#exec_curl="curl https://api.bitbucket.org/2.0/repositories/cardevisi | grep -E --color=auto '\{\"scm\"\:(\s+)?\"git\"\,(\s+)?\"has_wiki\":(\s+)?false\,(\s+)\"name\"\:(\s+)?\"(.\w+)?\"'";
	exec_curl='curl -s https://api.bitbucket.org/2.0/repositories/cardevisi | grep -E --color=auto "(\"name\"\:(\s+)?\"([a-zA-Z]))"';
	echo `$exec_curl`;
}

alias blist=hublist;
 	
hubc() {
	
	username=$1;
	repository_name=$2;

	if [ "$username" == "" ]; then
		echo "Enter with your username:";
		read -s username;
	fi

	if [ "$username" == "" ]; then
		echo "Exited! Your username is empty";
		return;
	fi
	
	dir_name=`basename $(pwd)`
	
	if [ "$repository_name" = "" ]; then 
		echo "Repo name (hit enter to use '$dir_name')?"
		read repository_name
	fi

	if [ "$repository_name" = "" ]; then 
		repository_name="$dir_name"
	fi

	
	#curl -u '"$username"' https://api.github.com/user/repos?page=0
 	#curl -u '"$username"' https://api.github.com/user/repos -d {"name": "teste", "description": "This is your first repository","homepage": "https://github.com","private": false,"has_issues": true,"has_wik i": true,"has_downloads": true}
 	#curl -u '"$username"' https://api.github.com/user/repos -d '{"name":"teste"}'
	
	exec_curl='curl -u '"$username"' https://api.github.com/user/repos -d {"name":"'"$repository_name"'"}'
 	echo `$exec_curl`;
}

alias desk='cd $HOME/Desktop';
alias docs='cd $HOME/Documents';
alias pics='cd $HOME/Pictures';
alias down='cd $HOME/Downloads; o;';

#************************************************************************
# No Color
#************************************************************************
NC='\e[0m'

#************************************************************************
# Status
#************************************************************************
function __git_dirty() {
    [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*";
}

function upsearch () {
    test / == "$PWD" && return || test -e "$1" && echo "found: " "$PWD" && return || cd .. && upsearch "$1"
}


#************************************************************************
#alias ls='ls -F --color=auto --show-control-chars'
alias lsd='ls -Ghpla --color=auto'
alias l='ls -CF'
alias ll='ls -l'
alias llc='ll -1 | wc -l'
alias ls='ls -l --color=auto'
alias reload='source ~/.bashrc'
alias gConfig='source ~/.gitconfig'
 
alias home='cd ~/'
alias apt-get='aptget'
#alias egrep='egrep --color=auto'
#alias fgrep='fgrep --color=auto'
#alias grep='grep --color=auto'
 
alias vi='vim'
alias profile='vi ~/.bash_aliases'
 
alias o='start explorer .'
alias np='start "" "C:\Program Files (x86)\Notepad++\notepad++.exe" '
alias subl='start "" "C:\Program Files\Sublime Text 3\sublime_text.exe" '
alias c='start "" "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" '
alias profile='np ~/.bash_profile'
alias s='subl .'
alias ab='start "" //B //WAIT "C:\Program Files (x86)\Apache Software Foundation\Apache2.2\bin\ab.exe" '
alias sws='start "/c/wamp/wampmanager.exe" //&'
alias fdt='start "/c/CASIFILES/DEV/FDT-64/FDT.exe"'
alias ecli='start "/c/CASIFILES/DEV/eclipse/eclipse.exe"'
alias flBuilder='start "/c/Program Files/Adobe/Adobe Flash Builder 4.7 (64 Bit)/FlashBuilder.exe"'

#************************************************************************
#Open files to edit
#************************************************************************
alias ba='s  -a ~/.bashrc' 
alias gconf='s  -a ~/.gitconfig'
alias ho='s -a /c/windows/System32/drivers/etc/hosts' 
alias vh='s -a /c/wamp/bin/apache/apache2.4.9/conf/extra/httpd-vhosts.conf'

#************************************************************************
# Alias para as pastas de trabalho
#************************************************************************
alias ws='cd /c/CASIFILES/WORKS/stg'
alias tk='cd /c/CASIFILES/TASKS'
alias wl='cd /c/CASIFILES/WORKS/local'
alias wq='cd /c/CASIFILES/WORKS/qa'
alias wp='cd /c/CASIFILES/WORKS/prod'
alias w='cd  /c/CASIFILES/WORKS/'
alias adg='cd /c/CASIFILES/WORKS/adg/trunk/adsGenerator/'
alias ad14='cd /c/CASIFILES/WORKS/adg/trunk/richmedia_2014/homolog/; ll'
alias ad13='cd /c/_SVN/trunk/richmedia_2013/homolog; ll'
alias ad15='cd /c/CASIFILES/WORKS/adg/trunk/richmedia_2015/homolog/; o;'
alias es='cd /c/CASIFILES/ESTUDOS/'
alias mcli='cd /c/Users/cad_caoliveira/Dropbox/UOL-HORAS/cli'
alias wd='cd /w/public_html/teste/richmedia/2015; o;'

#************************************************************************
#Alias para as vms do vagrant
#************************************************************************
alias vag='cd /c/CASIFILES/VMS/'

#************************************************************************
#Alias para o gerador de senha
#************************************************************************
alias gerasenha='cd /c/CASIFILES/WORKS/local/gerasamba; java -jar GeraSenha.jar;'
alias dygem='cd /c/CASIFILES/WORKS/atendimento/dynad/dynad-catho/2016-02-generator; java –jar creative-generator.jar  /c/CASIFILES/WORKS/atendimento/dynad/dynad-catho/2016-02-generator/project/  /c/CASIFILES/WORKS/atendimento/dynad/dynad-catho/2016-02-generator/project/CutyCapt.exe'

#************************************************************************
#Alias para os comandos git
#************************************************************************
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'
alias got='git '
alias get='git '
alias ggr='git graph'
alias gl='git log --graph --oneline --decorate'
alias glcasi='git log --graph --oneline --decorate --author="Carlos Augusto Silveira de Oliveira"'
alias hism='git reset --hard origin/master'
alias goc='git checkout digitar o SHA .'
alias calc='calc &'
alias pt='netstat'

#************************************************************************
#Jenkins commands
#************************************************************************
alias jstop='cd /c/CASIFILES/DEV/jenkins/; java -jar jenkins.war;'
alias jstart='cd /c/CASIFILES/DEV/jenkins/; java -jar jenkins.war --httpPort=9090 &'
alias jrestart='cd /c/CASIFILES/DEV/jenkins/; java -jar jenkins.war'
#alias djenkins='cd /c/CASIFILES/DEV/jenkins/; java -jar jenkins.war;'

#************************************************************************
#DOCKER
#************************************************************************
#alias dcomp= 'docker-compose logs -f'

#************************************************************************
#CURRENT JOB
#************************************************************************
alias tm='cd /c/CASIFILES/WORKS/stg/tag-manager/tm-stc'
alias tm.panel="cd /c/CASIFILES/WORKS/stg/tm-panel/"
alias tm.panel.fe="tm.panel && cd tm-panel-fe/ && cd tm-panel-fe/"
alias tm.panel.fe.grunt="tm.panel.fe && cd src/main/resources/grunt/"

#******************************************
# DOCKER START
#******************************************
alias doc='cd /c/Program\ Files/Docker\ Toolbox/; start.sh; vag;'
alias djenkins='docker run -u -p 8080:8080 -p 50000:50000 jenkins'
#docker run -u -p 8080:8080 -p 50000:50000 -v /c/Users/peo_caoliveira:/var/jenkins_home jenkins
alias dng='docker run --name docker-nginx -p 80:80 nginx'

#************************************************************************
# PS1 colored with username, machine name, count, timestamp, branch name and git status
#************************************************************************
#********************************************************************************
#PS1='$(__git_ps1 "[${Yel}%s${NC}${Red}$(__git_dirty)${NC}] ")'"${Cya}\w${NC}"' \n\$ '
#PS1="\[$Yel\]\t\[$Blu\]-\[$Blu\]\u\[$Yel\]\[$Yel\]\w\[\033[m\]\[$Cya\]\$(__git_dirty)\[$Whi\]\$ "
PS1="\[$Blu\][\t]\[$Whi\]-[\u@\[$Yel\]\h]\w\[$Blu\] \[$Whi\]-\[$Gre\]\[$Whi\]\[$Gre\]$(__git_ps1)\[$Whi\]\$ "
