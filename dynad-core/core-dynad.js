(function() {
	window._dt_dynad_130255_1453904849440 = '';
	window._tp_dynad_130255_1453904849440 = {};
	window._dynad_tag_protocol_ = 'http://';

	function loader(e, t, n) {
		if (0 == e.indexOf("//")) {
			e = window.location.protocol + e;
		};
		try {
			e += (e.indexOf("?") > -1 ? "&" : "?") + "cachebuster=" + new Date().getTime();
			var r = null;
			if (function() {
					try {
						return parseInt(navigator.userAgent.toLowerCase().split("msie")[1])
					} catch (e) {
						return 999
					}
				}() < 10) {
				r = new XDomainRequest;
				r.onload = function() {
					t(r.responseText)
				};
				r.onerror = function() {
					n(r.responseText)
				}
			} else {
				if (typeof XMLHttpRequest !== "undefined") r = new XMLHttpRequest;
				else {
					var i = ["MSXML2.XmlHttp.5.0", "MSXML2.XmlHttp.4.0", "MSXML2.XmlHttp.3.0", "MSXML2.XmlHttp.2.0", "Microsoft.XmlHttp"];
					for (var s = 0, o = i.length; s < o; s++) {
						try {
							r = new ActiveXObject(i[s]);
							break
						} catch (u) {
							r = null
						}
					}
				}
				if (r == null) {
					n(u);
					return false
				}
				r.onreadystatechange = function() {
					if (r.readyState === 4) {
						if (r.status === 200) {
							t(r.responseText);
							return true
						} else {
							n({
								message: "invalid status [" + r.status + "]"
							});
							return false
						}
					}
				}
			}
			r.open("GET", e, true);
			r.send()
		} catch (u) {
			n(u);
			return false
		}
	}
	if (typeof JSON !== "object") {
		JSON = {};
		JSON.parse = JSON.parse || function(o) {
			return eval('(' + o + ')')
		};
		JSON.stringify = JSON.stringify || function(o) {
			var n;
			var a;
			var t = typeof(o);
			var json = [];
			if (t != 'object' || o === null) {
				if (t == "string") {
					o = '"' + o + '"'
				}
				return String(o)
			} else {
				a = (o && o.constructor == Array);
				for (var key in o) {
					n = o[key];
					t = typeof(n);
					if (t == "string") {
						n = '"' + n.replace(/["]/g, "\\\"") + '"'
					} else if (t == "object" && n !== null) {
						n = JSON.stringify(n)
					}
					json.push((a ? "" : '"' + key + '":') + String(n))
				}
				return (a ? "[" : "{") + String(json) + (a ? "]" : "}")
			}
		}
	}

	function DADB(t) {
		return DADB.inst instanceof DADB ? DADB.inst._rs = t(DADB.inst) : (this._if = null, this._ok = !1, this._db = (typeof window['_dynad_tag_protocol_'] !== 'undefined' ? window['_dynad_tag_protocol_'] : ("http:" != window.location.protocol ? "https://" : "http://")) + "s.dynad.net", this._pt = "/stack/KMA9C2O70iP6CHSgXk0LGaQ8ML9m6vJE4RIi1Rf61p4.html?v63", this._qu = [], this._rq = {}, this._id = (new Date).getTime() + Math.floor(1e4 * Math.random()), this._ls = this._dg() == this._db, this._rs = "", DADB.inst = this, void this.init(function() {
			"function" == typeof t && (DADB.inst._rs = t(DADB.inst))
		}))
	}
	"object" != typeof JSON && (JSON = {}, JSON.parse = JSON.parse || function(o) {
		return eval("(" + o + ")")
	}, JSON.stringify = JSON.stringify || function(t) {
		var i, e, s = typeof t,
			n = [];
		if ("object" != s || null === t) return "string" == s && (t = '"' + t + '"'), t + "";
		e = t && t.constructor == Array;
		for (var o in t) i = t[o], s = typeof i, "string" == s ? i = '"' + i.replace(/["]/g, '\\"') + '"' : "object" == s && null !== i && (i = JSON.stringify(i)), n.push((e ? "" : '"' + o + '":') + (i + ""));
		return (e ? "[" : "{") + (n + "") + (e ? "]" : "}")
	}), DADB.prototype = {
		constructor: DADB,
		init: function(t) {
			var i = this;
			this._ls || this._if || (this._if = document.createElement("iframe"), this._if.style.cssText = "position:absolute;width:1px;height:1px;left:-9999px;", (document.body == null || typeof document.body === 'undefined' ? document.getElementsByTagName('body')[0] : document.body).appendChild(this._if), window.addEventListener ? (this._if.addEventListener("load", function() {
				i._ild()
			}, !1), window.addEventListener("message", function(t) {
				i._hm(t)
			}, !1)) : this._if.attachEvent && (this._if.attachEvent("onload", function() {
				i._ild()
			}, !1), window.attachEvent("onmessage", function(t) {
				i._hm(t)
			})), this._if.src = this._db + this._pt), "function" == typeof t && t()
		},
		getResult: function() {
			return this._rs
		},
		getCk: function(t, i) {
			var e = {
				request: {
					id: ++this._id,
					type: "gc",
					key: t
				},
				callback: i
			};
			this._s(e)
		},
		setCk: function(t, i, e, s) {
			"function" == typeof e && (s = e, e = 2592e3);
			var n = {
				request: {
					id: ++this._id,
					type: "sc",
					key: t,
					value: i,
					ttl: e
				},
				callback: s
			};
			this._s(n)
		},
		setNav: function(t, i, e, s, n) {
			var o = {
				request: {
					id: ++this._id,
					type: "snav",
					key: t,
					value: i + "|" + e + "|" + s
				},
				callback: n
			};
			this._s(o)
		},
		getNav: function(t, i) {
			var e = {
				request: {
					id: ++this._id,
					type: "nav",
					key: t
				},
				callback: i
			};
			this._s(e)
		},
		getItem: function(t, i) {
			if (this._ls) return localStorage[t];
			var e = this,
				s = {
					request: {
						id: ++e._id,
						type: "get",
						key: t
					},
					callback: i
				};
			e._s(s)
		},
		setItem: function(t, i, e) {
			if (this._ls) return void(localStorage[t] = i);
			var s = this,
				n = {
					request: {
						id: ++s._id,
						type: "set",
						key: t,
						value: i
					},
					callback: e
				};
			s._s(n)
		},
		delItem: function(t) {
			if (this._ls) return delete localStorage[t];
			var i = {
				request: {
					id: ++this._id,
					type: "unset",
					key: t
				}
			};
			this._s(i)
		},
		_s: function(t) {
			this._ok ? this._if && (this._rq[t.request.id] = t, this._if.contentWindow.postMessage(JSON.stringify(t.request), this._db)) : this._qu.push(t)
		},
		_ild: function() {
			if (this._ok = !0, this._qu.length) {
				for (var t = 0, i = this._qu.length; i > t; t++) this._s(this._qu[t]);
				this._qu = []
			}
		},
		_hm: function(t) {
			if (t.origin == this._db) {
				var i = JSON.parse(t.data);
				try {
					void 0 !== this._rq[i.id].deferred && this._rq[i.id].deferred.resolve(i.value)
				} catch (t) {}
				void 0 !== this._rq[i.id] && "function" == typeof this._rq[i.id].callback && (this._rs = this._rq[i.id].callback(i.key, i.value)), delete this._rq[i.id]
			}
		},
		_dg: function() {
			var t = window.location.href,
				i = document.createElement("a");
			return i.href = t, i.hostname
		}
	};
	window._DADB_130255_1453904849440 = DADB;
	window._loader_130255_1453904849440 = loader;
	document.write('<div style="height:250px; visibility:visible; overflow:hidden; width:300px; background-color:white; display:block; margin:0; border:0; z-index:1; " id="_dynad_c_I130255_1453904849440">');
	var cpam = '',
		cpam64 = '',
		setCPam = function(c, z) {
			if (!z) cpam = ';p=' + encodeURIComponent(c);
			else cpam64 = ';p64=' + cpam64.replace(new RegExp('=', 'g'), '-').replace(new RegExp("\\+", 'g'), '_');
		},
		eci = '',
		ecr = '',
		ci = '195152',
		cr = '4000007527.0',
		fb = null,
		nextStage = function(s, v, r, k, p) {
			if (fb != null) clearTimeout(fb);
			if (typeof v !== "undefined" && v != null) {
				window._dt_dynad_130255_1453904849440 = v;
			}
			if (typeof s !== "undefined" && s != false && s != null) {
				if (typeof r !== "undefined" && r != false && r != '' && r != null) r = true;
				if (typeof k !== "undefined" && k != false && k != null && k != '195152') eci = k;
				if (typeof p !== "undefined" && p != false && p != null) ecr = p;
				var script = document.createElement('script');
				script.src = "http://t5.dynad.net/script/?dc=130255;ord=1453904849440;st=2;eci=" + eci + ";ecr=" + ecr + ";ci=" + ci + ";cr=" + cr + ";rt=" + r + ";ts=1453904849440" + cpam + cpam64 + ";fsd=" + s + ";click=";
				script.async = "true";
				document.getElementById('_dynad_c_I130255_1453904849440').appendChild(script);
			} else {
				var dt_dynad = window._dt_dynad_130255_1453904849440;
				var divAppender = function(s) {
					var n = '';
					var r = s.replace(new RegExp('<script[^>]*>([\s\S]*?)<\/script>', 'gi'), function() {
						n += arguments[1] + '\n';
						return '';
					});
					document.getElementById('_dynad_c_I130255_1453904849440').innerHTML = r;
					if (window.execScript && n != '') window.execScript(n);
					else if (n != '') eval(n);
				};
				divAppender('<A HREF="http://t5.dynad.net/c/?dc=130255;ct=1;ci=195152;cr=4000007527.0;cb=4000007527.0;tracker=v2_3;C=0&dynadcb=1453904849440" TARGET="_blank" STYLE="" ><IMG ID="I130255_1453904849440" SRC="http://s.dynad.net/stack/E6cIz57eXFyiQT3TM7DYAPHMHjcXDgG58P3vH1DbjAA.gif" WIDTH="300" HEIGHT="250" ALT="" BORDER="0"></a>');

				ci = '195152';
				cr = '4000007527.0';
			}
		}
	fb = setTimeout(function() {
		nextStage(false);
	}, 3000);
	(function(reqid, customerId, businessId, geoData, weatherData, containerId, placementId, hasRetargeting, campaignId, tagParameters, defaultCampaignId) {
		
		try {
		
			var customerCart = 'd' + customerId + 'C';
			var customerProduct = 'd' + customerId + 'p';
			var today = getCurrentDate();
			var lastFourDays = new Date(getLastDate(getCurrentDate(), 4)).getTime();
			var lastTwoDays = new Date(getLastDate(getCurrentDate(), 2)).getTime();
		
			function getCurrentDate() {
				var today = new Date();
				var year = today.getFullYear();
				var day = today.getDate();
				var month = today.getMonth() + 1;
				return new Date (month + "/" + day + "/" + year).getTime();
			}

			function getLastDate(date, diference) {
				var lastDate = new Date(date);
				var dayOfMonth = lastDate.getDate();
				diference = (diference) ? diference : 0;
				return new Date(lastDate.setDate(dayOfMonth - diference)).getTime();
			}

			new DADB(function(db) {
				checkFilterCart(db, function(data){
					if(data === false) {
						
						checkFilterProduct(db, function(data) {
							if (data === 'produto_4') {
								nextStage("produtos4dias", null, false);
							} else if (data === 'produto_2') {
								nextStage("produtos2dias", null, false);
							} else {
								console.log('nenhum produto');
							}
						});
						return;	
					}

					if(data === 'carrinho_4') {
						nextStage("carrinho4dias", null, false);
					} else if(data === 'carrinho_2') {
						nextStage("carrinho2dias", null, false);
					} else {
						console.log('nenhum carrinho');
					}
				});
			});
			
			function checkFilterProduct(db, callback) {
		  		db.getItem (customerProduct, function(key, value) {
					if(value === null) {
						callback(false);
						return;
					}
					
					var skus = JSON.parse(value);
						
					if(!validateSkus(skus)) {
						callback(false);
						return;
					}

	  				var localStorageTime = formatLocalStorageDate(skus[0].t);

					if (localStorageTime >= lastFourDays && localStorageTime <= lastTwoDays) {
						callback("produto_4");
						return;
					} else if (localStorageTime > lastTwoDays) {
						callback("produto_2");
						return;
					} 
					
					callback(false);
					return;
					
				});
		  	}

		  	function formatLocalStorageDate(timestamp) {
		  		var fullDate = Number (timestamp + '000');
				var changeHour = new Date(fullDate).setHours(0,0,0,0);
				return new Date(changeHour).getTime();
		  	}

		  	function checkFilterCart (db, callback) {
		  		db.getItem (customerCart, function(key, value) {
					if(value === null) {
						callback(false);
						return;
					}
					
					var skus = JSON.parse(value);  
					
					if(!validateSkus(skus)) {
						callback(false);
						return;
					}

					var localStorageTime = formatLocalStorageDate(skus[0].t);

					if (localStorageTime >= lastFourDays && localStorageTime <= lastTwoDays) {
						callback('carrinho_4');
						return;
					} else if (localStorageTime > lastTwoDays) {
						callback('carrinho_2');
						return;
					} 
				
					callback(false);
					return;
				});
		  	}
			
			function validateSkus(json) {
		  		if(!(json || json.constructor !== Array || json.length == 0)) {
					return false;
				}

				if(!json[0] || !json[0].t) {
					return false;
				}

				return true;
		  	}
			
		} catch (e) {
			(function() {
				var el = document.createElement("img");
				el.setAttribute("src", "http://t5.dynad.net/gfep/?dc=130255;ord=[timestamp];click=&e=error30&m=" + escape(typeof e.stack !== 'undefined' ? e.stack : (e.message || '')));
				el.setAttribute("height", "0");
				el.setAttribute("width", "0");
				el.style.display = "none";
				document.getElementById("_dynad_c_I130255_1453904849440").appendChild(el);
			})();
			if (typeof window.console !== 'undefined' && typeof window.console.log !== 'undefined') console.log(e.message);
			nextStage(false);
		}
	})('130255_1453904849440', '20160119', '0', {
		city: window._cy_dynad_130255_1453904849440,
		state: window._st_dynad_130255_1453904849440,
		country: window._ct_dynad_130255_1453904849440,
		latitude: window._lt_dynad_130255_1453904849440,
		longitude: window._lg_dynad_130255_1453904849440
	}, {
		temperature: window._wt_dynad_130255_1453904849440,
		weather: window._we_dynad_130255_1453904849440,
		weatherCode: window._wc_dynad_130255_1453904849440,
		wLatitude: window._wlt_dynad_130255_1453904849440,
		wLongitude: window._wlg_dynad_130255_1453904849440,
		wLocalTime: window._wl_dynad_130255_1453904849440,
		wIcon: window._wi_dynad_130255_1453904849440,
		wind: window._ww_dynad_130255_1453904849440,
		pressure: window._wp_dynad_130255_1453904849440,
		humidity: window._wh_dynad_130255_1453904849440
	}, '_dynad_c_I130255_1453904849440', '130255', false, '195152', window._tp_dynad_130255_1453904849440, '195152');
	document.write('</div>');
})();