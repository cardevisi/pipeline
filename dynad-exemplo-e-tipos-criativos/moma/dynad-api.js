(function() {
	if (typeof JSON !== "object") {
		JSON = {};
		JSON.parse = JSON.parse || function(o) {
			return eval('(' + o + ')')
		};
		JSON.stringify = JSON.stringify || function(o) {
			var n;
			var a;
			var t = typeof(o);
			var json = [];
			if (t != 'object' || o === null) {
				if (t == "string") {
					o = '"' + o + '"'
				}
				return String(o)
			} else {
				a = (o && o.constructor == Array);
				for (var key in o) {
					n = o[key];
					t = typeof(n);
					if (t == "string") {
						n = '"' + n.replace(/["]/g, "\\\"") + '"'
					} else if (t == "object" && n !== null) {
						n = JSON.stringify(n)
					}
					json.push((a ? "" : '"' + key + '":') + String(n))
				}
				return (a ? "[" : "{") + String(json) + (a ? "]" : "}")
			}
		}
	}
	"document" in self && ("classList" in document.createElement("_") ? ! function() {
		"use strict";
		var t = document.createElement("_");
		if (t.classList.add("c1", "c2"), !t.classList.contains("c2")) {
			var e = function(t) {
				var e = DOMTokenList.prototype[t];
				DOMTokenList.prototype[t] = function(t) {
					var n, i = arguments.length;
					for (n = 0; i > n; n++) t = arguments[n], e.call(this, t)
				}
			};
			e("add"), e("remove")
		}
		if (t.classList.toggle("c3", !1), t.classList.contains("c3")) {
			var n = DOMTokenList.prototype.toggle;
			DOMTokenList.prototype.toggle = function(t, e) {
				return 1 in arguments && !this.contains(t) == !e ? e : n.call(this, t)
			}
		}
		t = null
	}() : ! function(t) {
		"use strict";
		if ("Element" in t) {
			var e = "classList",
				n = "prototype",
				i = t.Element[n],
				s = Object,
				r = String[n].trim || function() {
					return this.replace(/^\s+|\s+$/g, "")
				},
				o = Array[n].indexOf || function(t) {
					for (var e = 0, n = this.length; n > e; e++)
						if (e in this && this[e] === t) return e;
					return -1
				},
				a = function(t, e) {
					this.name = t, this.code = DOMException[t], this.message = e
				},
				c = function(t, e) {
					if ("" === e) throw new a("SYNTAX_ERR", "An invalid or illegal string was specified");
					if (/\s/.test(e)) throw new a("INVALID_CHARACTER_ERR", "String contains an invalid character");
					return o.call(t, e)
				},
				l = function(t) {
					for (var e = r.call(t.getAttribute("class") || ""), n = e ? e.split(/\s+/) : [], i = 0, s = n.length; s > i; i++) this.push(n[i]);
					this._updateClassName = function() {
						t.setAttribute("class", this.toString())
					}
				},
				u = l[n] = [],
				f = function() {
					return new l(this)
				};
			if (a[n] = Error[n], u.item = function(t) {
					return this[t] || null
				}, u.contains = function(t) {
					return t += "", -1 !== c(this, t)
				}, u.add = function() {
					var t, e = arguments,
						n = 0,
						i = e.length,
						s = !1;
					do t = e[n] + "", -1 === c(this, t) && (this.push(t), s = !0); while (++n < i);
					s && this._updateClassName()
				}, u.remove = function() {
					var t, e, n = arguments,
						i = 0,
						s = n.length,
						r = !1;
					do
						for (t = n[i] + "", e = c(this, t); - 1 !== e;) this.splice(e, 1), r = !0, e = c(this, t); while (++i < s);
					r && this._updateClassName()
				}, u.toggle = function(t, e) {
					t += "";
					var n = this.contains(t),
						i = n ? e !== !0 && "remove" : e !== !1 && "add";
					return i && this[i](t), e === !0 || e === !1 ? e : !n
				}, u.toString = function() {
					return this.join(" ")
				}, s.defineProperty) {
				var h = {
					get: f,
					enumerable: !0,
					configurable: !0
				};
				try {
					s.defineProperty(i, e, h)
				} catch (g) {
					-2146823252 === g.number && (h.enumerable = !1, s.defineProperty(i, e, h))
				}
			} else s[n].__defineGetter__ && i.__defineGetter__(e, f)
		}
	}(self));
	if (!document.querySelectorAll && document.createStyleSheet) {
		(function() {
			var style = document.createStyleSheet(),
				select = function(selector, maxCount) {
					var all = document.all,
						l = all.length,
						i, resultSet = [];
					style.addRule(selector, "foo:bar");
					for (i = 0; i < l; i += 1) {
						if (all[i].currentStyle.foo === "bar") {
							resultSet.push(all[i]);
							if (resultSet.length > maxCount) {
								break;
							}
						}
					}
					style.removeRule(0);
					return resultSet;
				};
			if (document.querySelectorAll || document.querySelector) {
				return;
			}
			document.querySelectorAll = function(selector) {
				return select(selector, Infinity);
			};
			document.querySelector = function(selector) {
				return select(selector, 1)[0] || null;
			};
		}());
	}

	function e() {
		this.r = null
	}

	function t(e) {
		try {
			window["DYNADAPI_DATA"] = JSON.parse(e.data)
		} catch (t) {
			if (typeof console !== "undefined") console.log('DynAd API error: ' + t.message);
			return
		}
		if (window["DYNADAPI"].r != null) clearTimeout(window["DYNADAPI"].r);
		window["DYNADAPI"].onload()
	}
	window.load = function() {
		window.onload()
	};
	if (window.load) {
		window.onload = function() {
			window["DYNADAPI"].init()
		}
	}
	e.prototype = {
		contructor: e,
		setBackup: function(e) {
			var t = false;
			try {
				if (typeof window["DYNADAPI_DATA"] === "undefined" || JSON.stringify(window["DYNADAPI_DATA"]) == "{}" || window["DYNADAPI_DATA"]["data"] === "undefined" || JSON.stringify(window["DYNADAPI_DATA"]["data"]) == "[{}]") t = true
			} catch (n) {
				t = true
			}
			if (t) window["DYNADAPI_DATA"] = typeof e === "object" ? e : JSON.parse(e)
		},
		init: function() {
			this.r = setTimeout(function() {
				window["DYNADAPI"].onload()
			}, 10)
		},
		getClickTAG: function() {
			return window["DYNADAPI_DATA"]["clickTAG"]
		},
		getClickTAG2: function() {
			return window["DYNADAPI_DATA"]["clickTAG2"]
		},
		getClickTAG3: function() {
			return window["DYNADAPI_DATA"]["clickTAG3"]
		},
		getClickTAG4: function() {
			return window["DYNADAPI_DATA"]["clickTAG4"]
		},
		getClickTAG5: function() {
			return window["DYNADAPI_DATA"]["clickTAG5"]
		},
		getTarget: function() {
			return window["DYNADAPI_DATA"]["target"]
		},
		getDataLength: function() {
			return window["DYNADAPI_DATA"]["data"].length
		},
		getDataItem: function(e) {
			return window["DYNADAPI_DATA"]["data"][e]
		},
		getProperty: function(e) {
			return window["DYNADAPI_DATA"][e]
		}
	};
	if (window.addEventListener) {
		window.addEventListener("message", t, false)
	} else if (window.attachEvent) {
		window.attachEvent("onmessage", t)
	}
	window["DYNADAPI"] = new e;
	var n = null;
})();