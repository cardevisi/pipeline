(function() {

    var tl = new TimelineMax(
        {   
            onComplete:function() {
    	       this.restart();
    	   }
        }
    );

    var tl_arrow = new TimelineMax(
        {   onComplete:function() {
            //this.restart();
        }
    });

    tl_arrow.set('.step-arrow .sprite-arrow', {opacity:0});
    tl_arrow.to('.step-arrow .sprite-arrow', 0.8, { opacity: 1 }, 1);
    
    /*tl_arrow.to('.step-1 .sprite-arrow', 0.5, { ease: Back.easeOut, y: 20, opacity: 1});
    tl_arrow.to('.step-1 .sprite-arrow', 0.5, {opacity: 0}, "=-0.1");
    
    tl_arrow.to('.step-2 .sprite-arrow', 0.5, { ease: Back.easeOut, y: 20, opacity: 1});
    tl_arrow.to('.step-2 .sprite-arrow', 0.5, {opacity: 0}, "=-0.1");

    tl_arrow.to('.step-3 .sprite-arrow', 0.5, { ease: Back.easeOut, y: 20, opacity: 1});
    tl_arrow.to('.step-3 .sprite-arrow', 0.5, {opacity: 0}, "=-0.1");*/



    
    tl.from('.step-1 .red-ball', 1, { ease: Back.easeOut, scale: 0.5, opacity: 0});
    tl.from('.step-1 .sprite-text-1', 1, { opacity: 0 }, "=-0.3");
    
    tl.to('.step-1 .red-ball', 1, { ease: Back.easeOut, scale: 1.5, opacity: 0});
    tl.to('.step-1 .sprite-text-1', 0.5, {  opacity: 0}, "=-0.8");

    tl.from('.step-2 .red-ball', 1, { ease: Back.easeOut, scale: 0.5, opacity: 0});
    tl.from('.step-2 .sprite-text-2', 1, { opacity: 0 }, "=-0.3");
    
    tl.to('.step-2 .sprite-arrow', 1, { ease: Back.easeOut, y: 20, opacity: 0});
    tl.to('.step-2 .red-ball', 1, { ease: Back.easeOut, scale: 1.5, opacity: 0});
    tl.to('.step-2 .sprite-text-2', 0.5, {  opacity: 0}, "=-0.8");

    tl.from('.step-3 .red-ball', 1, { ease: Back.easeOut, scale: 0.5, opacity: 0});
    tl.from('.step-3 .sprite-text-3', 1, { opacity: 0 }, "=-0.3");
    
    tl.to('.step-3 .sprite-arrow', 1, { ease: Back.easeOut, y: 20, opacity: 0});
    tl.to('.step-3 .red-ball', 1, { ease: Back.easeOut, scale: 1.5, opacity: 0, clearProps:"transformOrgin,transform,scale"});
    tl.to('.step-3 .sprite-text-3', 0.5, {  opacity: 0}, "=-0.8");

    
    

	


    

    
    

    

    

    


    /*tl.from('#main .sprite-text-2', .8, { ease: Back.easeOut, opacity: 0, delay: 1 }, 'step-2');
    
    tl.addLabel('step-2', '+=2');
    tl.to('#main .red-ball', 1, { ease: Back.easeOut, scale: 1.5, opacity: 0, delay: 1}, 'step-2');
    tl.to('#main .sprite-text-2', .8, { ease: Back.easeOut, opacity: 0, delay: 1.2 }, 'step-2');*/
    

    /*tl.to('#main .sprite-text-2', .8, { ease: Back.easeOut, opacity: 0, delay: .9 }, 'step-1');
    tl.to('#main .sprite-text-3', .8, { ease: Back.easeOut, opacity: 0, delay: .9 }, 'step-1');*/
    
    /*tl.addLabel('step-1', '+=2');
    

    /*tl.from('#texto .linha-2', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: 1 }, 'step-1');
    tl.from('#texto .linha-3', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: 1.1 }, 'step-1');
    tl.addLabel('step-2', '+=2')
    tl.from('#texto .linha-4', .8, { ease: Back.easeOut, left: 70, opacity: 0 }, 'step-2');
    tl.from('#texto .linha-5', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: .1  }, 'step-2');
    tl.from('#texto .linha-6', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: .1  }, 'step-2');
    tl.from('#texto .linha-7', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: .2 }, 'step-2');
    tl.from('#texto .linha-8', .8, { ease: Back.easeOut, left: 70, opacity: 0, delay: .3 }, 'step-2');

    // Step 3
    tl.addLabel('step-3', '+=2');
    tl.from('#step-2', .8, { opacity: 0 }, 'step-3');
    tl.from('#logo', .8, { ease: Power1.easeIn, opacity: 0, scale: .5, delay: .8 }, 'step-3');
    tl.from("#call", .8, { ease: Power1.easeIn, scale:0.5, opacity:0, delay: .8 }, 'step-3');
    // Step final
    tl.addLabel('call', '+=0.5');
    tl.to("#seta", .3, { ease: Power1.easeInOut, marginRight: -5 });
    tl.to("#seta", .3, { ease: Power1.easeInOut, marginRight: 0 });
    tl.to("#seta", .3, { ease: Power1.easeInOut, marginRight: -5 });
    tl.to("#seta", .3, { ease: Power1.easeInOut, marginRight: 0 });

    document.querySelector("#main").className = "animate";*/





	  /*var collapsedURL = "FRAME 03.png",
	  $main = document.querySelector("#main"),
      $click = $main.querySelector("#click-area");

      DYNADAPI.onload = function() {
        var clickTag = DYNADAPI.getClickTAG(),
        target = DYNADAPI.getTarget();
        $click.href = clickTag;
        $click.target = target;
      }*/
})();
