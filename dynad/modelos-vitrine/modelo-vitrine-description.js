Pessoal, hoje as vitrines do DynAd estão sendo utilizadas em vários espaços (home desktop, mobile, afiliados, etc) e ao mesmo tempo há 3 variações de APIs em uso... com o objetivo de iniciar uma documentação, estou enviando este e-mail descrevendo os 3 modelos e em quais situações devem ser aplicados. Por favor me chamem se a explicação não estiver clara para podermos detalhar tecnicamente.



Existem 3 tipos de APIs e padrões para implementação de criativos para vitrine:

1 - com número de slots fixos, normalmente não responsivos ou com carrossel de giro automático. Neste modelo a cada impressão do banner o Ad Server contabiliza cada oferta como impressa independente da oferta estar visível. Pensando-se em vitrines de largura fixa, como a da home do UOL, faz todo o sentido, pois sempre as ofertas contabilizadas como impressas serão realmente as que estão visíveis;

2 - de números de slots variáveis em função da largura da página. Neste modelo, apenas as ofertas visíveis no momento da impressão são contabilizadas e a cada movimento de navegação do carrossel a vitrine emite notificações ao ad server para a contabilização das ofertas vistas até completar o ciclo. Este modelo tem sido utilizado na home do UOL mobile e BOL desktop;

3 - vitrine integrada com parceiros, permite que ofertas sejam impressas através de chamadas a sistemas externos preenchendo um ou mais slots. Atualmente este modelo tem sido utilizado nas vitrines de afiliados para as ofertas da Criteo;


MODELO COM NÚMERO DE SLOTS FIXOS
O modelo mais simples e básico é este modelo, onde a API é chamada da mesma forma que um banner dinâmico de retargeting, veja exemplos abaixo:

 

neste modelo, a função "DYNADAPI.getDataLength()" contém o total de ofertas para preencher a vitrine. Neste modelo o número de ofertas será contabilizado sempre com o total disponível, independente de estarem visíveis ou serem utilizados pela vitrine. Contudo é um modelo mais otimizado do ponto de vista de requisições pois não há novas chamadas ao ad server pelo navegador do cliente. Por fim a função "DYNADAPI.getDataItem(i)", onde "i" é o indexador começando com 0, devolve os dados da oferta na posição "i";


MODELO COM SLOTS VARIÁVEIS
O segundo modelo de implementação ("trueview") adiciona chamadas assíncronas a cada evento de exibição das ofertas. Basicamente são duas as funções que devem ser chamadas para este controle:

1 - quando o banner é renderizado e é conhecido o número de ofertas vistas: neste momento deve ser chamada a função "unrender" para que as ofertas ainda não visíveis sejam removidas das listas de ofertas impressas. Isso é necessário porque o Ad Server opera de forma passiva e portanto ao encaminhar os dados para impressão assume que todas as ofertas estarão visíveis, cabendo ao criativo notificar quais ficaram ocultas. A função abaixo deve ser chamada neste momento:

 

IMPORTANTE: O array "FIRED_ITENS" deve ser declarado antes para registrar ofertas já notificadas evitando duplicidades de notificações e consequentemente falhas nos relatórios.

2 - Quando ocorre as impressões de novas ofertas, por exemplo havendo uma mudança na resolução do navegador ou quando houver o clique nas setas de navegação do carrossel, deverá ser chamado o código javascript abaixo:

 



MODELO INTEGRADO A PARCEIROS
Neste modelo as ofertas são notificadas de forma assíncrona, porém vinculadas a uma função de "callback" para estabelecer o controle. Observando o exemplo abaixo a função "DYNADAPI.getDataItem(i, fx)" é chamada recursivamente até o completo carregamento da vitrine. Isso é necessário, pois os dados de parceiros, como Criteo, são repassados fora do escopo do Ad Server e portanto de momento de resposta imprevisível.

 


Em anexo os 3 tipos de vitrines para análise. Em linhas gerais, o modelo 2 deverá ser utilizado em todos os espaços onde houver variação de largura (responsivo) e o modelo 3 sempre onde for utilizar impressão de parceiros como a Criteo (atualmente todas as vitrines de afiliados).

Qualquer dúvida estou à disposição.
