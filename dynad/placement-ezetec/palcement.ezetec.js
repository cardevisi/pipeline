if (step == 2) {
        
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        
        // Listen to message from child window
        eventer(messageEvent,function(e) {
            var key = e.message ? "message" : "data";
            var data = e[key];
            if( data != null && typeof data !== undefined && data.indexOf('{"dynad-ce-20150502-eztec":') == 0 ) {
                if( console && console.log ) 
                    console.log('[dynad] - captured clickTag from inner frame', data);
                var clickTag = data.substr(28); clickTag = clickTag.substr(0, clickTag.length - 2);
                document.querySelector('#div_hit').setAttribute('clickTag', clickTag);
            }
        },false);
        
        var CREATIVE_PATH = 'https://static.dynad.net/stack/8roEZq0eFCWJzRQkpz984566wz0CuWG8u7ZKhT2G3CrerR2iE3Nx94HmqVUjOk-S.html?v=20170428_02';

        window['getId'] = function(id) {
            return document.getElementById(id);
        }
        var criativo = getId('_dynad_c_' + reqid);
        criativo.style.height = '0px';
        
        try {
            criativo.querySelector('a').style.display = 'inline-block';
            criativo.querySelector('a').style.outline = '0';
        } catch(erro) {}


        var hit = document.createElement('div');
        hit.setAttribute('id', 'div_hit');
        hit.setAttribute('style', 'width:1190px; height:250px; position:absolute; top:0px; right:0px; cursor:pointer; z-index:9; background: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7") repeat;');
        hit.setAttribute("onclick", "var elHit = document.querySelector('#div_hit');  if( !elHit.hasAttribute('clickTag') ) return false; var clickTag = elHit.getAttribute('clickTag'); var w = top.window.innerWidth || top.document.documentElement.clientWidth || top.document.body.clientWidth; var h = top.window.innerHeight || top.document.documentElement.clientHeight || top.document.body.clientHeight; var al = (h > 600 ? 'top' : 'bottom'); var pl = Math.round((w - 1190)/2); var ifrm = document.createElement('iframe'); ifrm.setAttribute('id', 'ifrm_expanded'); ifrm.setAttribute('name', 'ifrm_expanded'); ifrm.setAttribute('marginwidth', '0'); ifrm.setAttribute('width', w); ifrm.setAttribute('height', h); ifrm.setAttribute('marginheight', '0'); ifrm.setAttribute('align', 'top'); ifrm.setAttribute('scrolling', 'No'); ifrm.setAttribute('bordercolor', '#000000'); ifrm.setAttribute('frameborder', '0'); ifrm.setAttribute('hspace', '0'); ifrm.setAttribute('vspace', '0'); ifrm.setAttribute('style', 'position:fixed; '+al+':0px; left:0px; cursor:pointer; z-index:29999998; width: 100%; height: 100%; border: 0; margin: 0 auto;'); ifrm.setAttribute('src', '"+CREATIVE_PATH+"'); top.document.body.appendChild(ifrm); var ht2 = document.createElement('div'); ht2.setAttribute('id', 'div_dynad_hit_extended'); ht2.setAttribute('style', 'position: fixed; top: 0px; width:1190px; height:'+h+'px; left:'+pl+'px; cursor:pointer; z-index:29999999; background: url(\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7\") repeat;'); ht2.setAttribute('onclick', 'if( console && console.log ) console.log(\"'+clickTag+'\"); window.open(\"'+clickTag+'\");'); var bc = document.createElement('div'); bc.setAttribute('id', 'bt_close'); bc.setAttribute('style', 'width:55px; height:15px; position:absolute; top:0px; right:0px; cursor:pointer; z-index:10; background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAYAAABA8leGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDkyOTYwNTU3QTRFMTFFNEI0QjRGMzcyQTBFNzVEQzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDkyOTYwNTY3QTRFMTFFNEI0QjRGMzcyQTBFNzVEQzEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowOTI5NjA1MzdBNEUxMUU0QjRCNEYzNzJBMEU3NURDMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowOTI5NjA1NDdBNEUxMUU0QjRCNEYzNzJBMEU3NURDMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvenNPgAAAFjSURBVHja7FehjoNAEB3IOYKtgA/A9C+qKQh+AFuBJhRZSJD9AgwSQfgWBBhcEUjwcMwmNJSD63LkcrS5l2x2Z8LszsvuvF2Ytm3Bsqy2LEvA9tfY7Xakua7LoK3renu73aAoCqp4QRBAFEXwfZ9hZFluYaPI83xVPAsbBsuy70uuaZr3JTfG5XKBJElI30NRFOILwxB4nn/4/mNqkjiO7+Pj8bjYh+O+XxL7DLZtEwKqqkKaplDXNTiOAyg4nfAQ+ym5uQVpfXPzDQnN+WgIohqapknsqqrAMIwvxL49llOL0iZCGzvcXVogCc/z7nYQBJBl2bKaW7tLtL6lO4fHclhzp9MJJElaLyiYyDiZKd9vAYl1lzM5lufzmTQE+qYIvtQlfr1e4XA4QBRFpPZ6texFRdO0h9r7f6G8KliO4zaZGOY1vpSX1ieDfwWdam3uaHYiRf4K9vv9j3LrXi3MpwADAC4Ps4yCewXvAAAAAElFTkSuQmCC\") no-repeat top right;'); bc.setAttribute(\"onclick\", \"var ifrm = top.document.querySelector('#ifrm_expanded'); ifrm.parentNode.removeChild(ifrm); var hit = event.target.parentNode; hit.parentNode.removeChild( hit ); event.stopPropagation();\"); ht2.appendChild(bc); top.document.body.appendChild(ht2); ");
        criativo.appendChild( hit );

        var bc = document.createElement('div');
        bc.setAttribute('id', 'bt_close');
        bc.setAttribute('style', "width:55px; height:15px; position:absolute; top:0px; right:0px; cursor:pointer; z-index:10; background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAYAAABA8leGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDkyOTYwNTU3QTRFMTFFNEI0QjRGMzcyQTBFNzVEQzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDkyOTYwNTY3QTRFMTFFNEI0QjRGMzcyQTBFNzVEQzEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowOTI5NjA1MzdBNEUxMUU0QjRCNEYzNzJBMEU3NURDMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowOTI5NjA1NDdBNEUxMUU0QjRCNEYzNzJBMEU3NURDMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvenNPgAAAFjSURBVHja7FehjoNAEB3IOYKtgA/A9C+qKQh+AFuBJhRZSJD9AgwSQfgWBBhcEUjwcMwmNJSD63LkcrS5l2x2Z8LszsvuvF2Ytm3Bsqy2LEvA9tfY7Xakua7LoK3renu73aAoCqp4QRBAFEXwfZ9hZFluYaPI83xVPAsbBsuy70uuaZr3JTfG5XKBJElI30NRFOILwxB4nn/4/mNqkjiO7+Pj8bjYh+O+XxL7DLZtEwKqqkKaplDXNTiOAyg4nfAQ+ym5uQVpfXPzDQnN+WgIohqapknsqqrAMIwvxL49llOL0iZCGzvcXVogCc/z7nYQBJBl2bKaW7tLtL6lO4fHclhzp9MJJElaLyiYyDiZKd9vAYl1lzM5lufzmTQE+qYIvtQlfr1e4XA4QBRFpPZ6texFRdO0h9r7f6G8KliO4zaZGOY1vpSX1ieDfwWdam3uaHYiRf4K9vv9j3LrXi3MpwADAC4Ps4yCewXvAAAAAElFTkSuQmCC) no-repeat top right;");
        bc.setAttribute("onclick", "document.getElementById('"+criativo.getAttribute('id') +"').style.height='0px'; setTimeout(function () { top.document.getElementById('banner-1190x70-2-Area').style.display='none'; }, 500 ); document.cookie = 'CEHome=0;domain=.uol.com.br;path=/;';");
        criativo.appendChild( bc );

        try {
            var divG = top.document.getElementById('banner-1190x70-2-Area');
            /**if( typeof divG === 'undefined' || divG == null ) {         	
            	divG = parent.document.getElementById(window.name).parentNode.parentNode.parentNode;
            }**/
            
        	divG.style.marginBottom = "14px";
        	bc.setAttribute("onclick", "this.parentNode.parentNode.style.marginBottom = '0px'; document.getElementById('"+criativo.getAttribute('id') +"').style.height='0px'; setTimeout(function () { top.document.getElementById('banner-1190x70-2-Area').style.display='none'; }, 500 ); document.cookie = 'CEHome=0;domain=.uol.com.br;path=/;';");
            
            if (divG) {
                //divG.appendChild(criativo);                                    
                divG.style.marginTop = '1px';
                divG.style.height = 'auto';
                
                setTimeout(function(){
                    criativo.style.height = '250px';    
                    criativo.style.opacity = '1';    
                }, 10);   
            }
        } catch (e) {
            console.log(e);
        }
        
    }
    return true;