! function() {
    function rep(p, z) {
        if (void 0 !== p)
            for (var x in p) z = z.replace(RegExp("{{" + x + "}}", "gi"), p[x]);
        return z.replace(RegExp("{{.*?}}", "g"), "")
    }

    function e() {
        this.r = null
    }

    function t(t) {
        try {
            if (window.DYNADAPI_DATA = JSON.parse(t.data), void 0 === window.DYNADAPI_DATA.clickTAG || void 0 === window.DYNADAPI_DATA.target) return
        } catch (n) {
            return;
        }
        null != window.DYNADAPI.r && clearTimeout(window.DYNADAPI.r), window.DYNADAPI.onload()
    }
    "object" != typeof JSON && (JSON = {}, JSON.parse = JSON.parse || function(o) {
        return eval("(" + o + ")")
    }, JSON.stringify = JSON.stringify || function(t) {
        var n, e, i = typeof t,
            o = [];
        if ("object" != i || null === t) return "string" == i && (t = '"' + t + '"'), t + "";
        e = t && t.constructor == Array;
        for (var r in t) n = t[r], i = typeof n, "string" == i ? n = '"' + n.replace(/["]/g, '\\"') + '"' : "object" == i && null !== n && (n = JSON.stringify(n)), o.push((e ? "" : '"' + r + '":') + (n + ""));
        return (e ? "[" : "{") + (o + "") + (e ? "]" : "}")
    }), "document" in self && ("classList" in document.createElement("_") ? ! function() {
        "use strict";
        var t = document.createElement("_");
        if (t.classList.add("c1", "c2"), !t.classList.contains("c2")) {
            var n = function(t) {
                var n = DOMTokenList.prototype[t];
                DOMTokenList.prototype[t] = function(t) {
                    var e, i = arguments.length;
                    for (e = 0; i > e; e++) t = arguments[e], n.call(this, t)
                }
            };
            n("add"), n("remove")
        }
        if (t.classList.toggle("c3", !1), t.classList.contains("c3")) {
            var e = DOMTokenList.prototype.toggle;
            DOMTokenList.prototype.toggle = function(t, n) {
                return 1 in arguments && !this.contains(t) == !n ? n : e.call(this, t)
            }
        }
        t = null
    }() : ! function(t) {
        "use strict";
        if ("Element" in t) {
            var n = "classList",
                e = "prototype",
                i = t.Element[e],
                o = Object,
                r = String[e].trim || function() {
                    return this.replace(/^\s+|\s+$/g, "")
                },
                c = Array[e].indexOf || function(t) {
                    for (var n = 0, e = this.length; e > n; n++)
                        if (n in this && this[n] === t) return n;
                    return -1
                },
                A = function(t, n) {
                    this.name = t, this.code = DOMException[t], this.message = n
                },
                u = function(t, n) {
                    if ("" === n) throw new A("SYNTAX_ERR", "An invalid or illegal string was specified");
                    if (/\s/.test(n)) throw new A("INVALID_CHARACTER_ERR", "String contains an invalid character");
                    return c.call(t, n)
                },
                s = function(t) {
                    for (var n = r.call(t.getAttribute("class") || ""), e = n ? n.split(/\s+/) : [], i = 0, o = e.length; o > i; i++) this.push(e[i]);
                    this._updateClassName = function() {
                        t.setAttribute("class", "" + this)
                    }
                },
                a = s[e] = [],
                l = function() {
                    return new s(this)
                };
            if (A[e] = Error[e], a.item = function(t) {
                    return this[t] || null
                }, a.contains = function(t) {
                    return t += "", -1 !== u(this, t)
                }, a.add = function() {
                    var t, n = arguments,
                        e = 0,
                        i = n.length,
                        o = !1;
                    do t = n[e] + "", -1 === u(this, t) && (this.push(t), o = !0); while (++e < i);
                    o && this._updateClassName()
                }, a.remove = function() {
                    var t, n, e = arguments,
                        i = 0,
                        o = e.length,
                        r = !1;
                    do
                        for (t = e[i] + "", n = u(this, t); - 1 !== n;) this.splice(n, 1), r = !0, n = u(this, t); while (++i < o);
                    r && this._updateClassName()
                }, a.toggle = function(t, n) {
                    t += "";
                    var e = this.contains(t),
                        i = e ? n !== !0 && "remove" : n !== !1 && "add";
                    return i && this[i](t), n === !0 || n === !1 ? n : !e
                }, a.toString = function() {
                    return this.join(" ")
                }, o.defineProperty) {
                var d = {
                    get: l,
                    enumerable: !0,
                    configurable: !0
                };
                try {
                    o.defineProperty(i, n, d)
                } catch (D) {
                    -2146823252 === D.number && (d.enumerable = !1, o.defineProperty(i, n, d))
                }
            } else o[e].__defineGetter__ && i.__defineGetter__(n, l)
        }
    }(self)), !document.querySelectorAll && document.createStyleSheet && ! function() {
        var t = document.createStyleSheet(),
            n = function(n, e) {
                var i, o = document.all,
                    r = o.length,
                    c = [];
                for (t.addRule(n, "foo:bar"), i = 0; r > i && ("bar" !== o[i].currentStyle.foo || (c.push(o[i]), c.length <= e)); i += 1);
                return t.removeRule(0), c
            };
        document.querySelectorAll || document.querySelector || (document.querySelectorAll = function(t) {
            return n(t, 1 / 0)
        }, document.querySelector = function(t) {
            return n(t, 1)[0] || null
        })
    }(), window.load = function() {
        window.onload()
    }, window.load && (window.onload = function() {
        window.DYNADAPI.init()
    }), e.prototype = {
        contructor: e,
        setBackup: function(t) {
            var n = !1,
                e = void 0 !== window.DYNADAPI_DATA && void 0 !== window.DYNADAPI_DATA.clickTAG && "" != window.DYNADAPI_DATA.clickTAG ? window.DYNADAPI_DATA.clickTAG : "";
            try {
                (void 0 === window.DYNADAPI_DATA || "{}" == JSON.stringify(window.DYNADAPI_DATA) || "undefined" === window.DYNADAPI_DATA.data || "[{}]" == JSON.stringify(window.DYNADAPI_DATA.data)) && (n = !0)
            } catch (i) {
                n = !0
            }
            n && (window.DYNADAPI_DATA = "object" == typeof t ? t : JSON.parse(t), window.DYNADAPI_DATA.clickTAG = e)
        },
        init: function() {
            this.r = setTimeout(function() {
                window.DYNADAPI.onload()
            }, 3e3)
        },
        getClickTAG: function(p) {
            return rep(p, window.DYNADAPI_DATA.clickTAG)
        },
        getClickTAG2: function(p) {
            return rep(p, void 0 === window.DYNADAPI_DATA.clickTAG2 ? window.DYNADAPI_DATA.clickTAG : window.DYNADAPI_DATA.clickTAG2)
        },
        getClickTAG3: function(p) {
            return rep(p, void 0 === window.DYNADAPI_DATA.clickTAG3 ? window.DYNADAPI_DATA.clickTAG : window.DYNADAPI_DATA.clickTAG3)
        },
        getClickTAG4: function(p) {
            return rep(p, void 0 === window.DYNADAPI_DATA.clickTAG4 ? window.DYNADAPI_DATA.clickTAG : window.DYNADAPI_DATA.clickTAG4)
        },
        getClickTAG5: function(p) {
            return rep(p, void 0 === window.DYNADAPI_DATA.clickTAG5 ? window.DYNADAPI_DATA.clickTAG : window.DYNADAPI_DATA.clickTAG5)
        },
        getTarget: function() {
            return window.DYNADAPI_DATA.target
        },
        getDataLength: function() {
            return window.DYNADAPI_DATA.data.length
        },
        getDataItem: function(t) {
            return window.DYNADAPI_DATA.data[t]
        },
        getProperty: function(t) {
            return window.DYNADAPI_DATA[t]
        }
    }, window.addEventListener ? window.addEventListener("message", t, !1) : window.attachEvent && window.attachEvent("onmessage", t), window.DYNADAPI = new e;
    var n = null
}();