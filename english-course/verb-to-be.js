Courses:
https://learnenglish.britishcouncil.org/en/english-grammar/verbs/present-tense


Verb To be

I       am
You     are
He      is
She     is
It      is
We      are
You     are
They    are


New Words:

proof -> prova 
prova -> proof

shrink -> escolher/reduzir
escolher/reduzir -> shrink

bundle -> pacote/embrulhar
embrulhar/pacote -> bundle

revenue -> receita/rendimento
receita/rendimento -> revenue


There are two tenses in English - past and present.

Simple Past

Past simple:                I worked
Past continuous:            I was working
Past perfect:               I had worked
Past perfect continuous:    I had been working