if (step == 2) {

  var content = document.querySelector('#_dynad_c_'+reqid);

  function fncNewMessage ( e ){
     if( e.data == 'friboi-natal-expand' ) {
      window['iframe' + reqid].style.transition = 'all 0.8s ease-out';
      window['iframe' + reqid].style.height = '460px';
      window['iframe' + reqid].height = '460';
      content.style.height = '460px';
      console.log('EXPAND');
    } else if( e.data == 'friboi-natal-retrai' ) {
      console.log('RETRAI');
      window['iframe' + reqid].style.transition = 'all 0.1s ease-out';
      window['iframe' + reqid].style.height = '250px';
      window['iframe' + reqid].height = '250';
      content.style.height = '250px';
      console.log('RETRAI');
    } else if( e.data == 'friboi-natal-close' ) {
      console.log('CLOSE BUTTON');
      window['iframe' + reqid].style.transition = '';
      window['iframe' + reqid].style.height = '0';
      window['iframe' + reqid].height = '0';
    }
  }

  if (window.addEventListener) {
      window.addEventListener("message", fncNewMessage, false);
  } else {
      window.attachEvent("onmessage", fncNewMessage);
  }

    window['iframe' + reqid] = document.querySelector('#' + reqid.replace('I', 'IF'));
    window['iframe' + reqid].style.position = 'absolute';
    window['iframe' + reqid].style.top = '0';
    window['iframe' + reqid].style.left = '0';
    window['iframe' + reqid].style.zIndex = '2147483647';
  }

  return true;