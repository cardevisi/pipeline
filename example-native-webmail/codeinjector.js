(function(){
    window.uolads = window.uolads || [];
    var cssTitleColor, cssDescColor, cssBgColor, cssListSpotlight;
    var skin = window.skinColorInfo;
    cssTitleColor = (skin && skin.general && skin.general.titleColor) ? skin.general.titleColor : '000000';
    cssDescColor = (skin && skin.general && skin.general.textColor) ? skin.general.textColor : '000000';
    cssBgColor = (skin && skin.general && skin.general.backgroundColor) ? skin.general.backgroundColor : 'FFFFFF';
    cssListSpotlight = (skin && skin.specific && skin.specific.webmail_listSpotlight) ? skin.specific.webmail_listSpotlight : 'AAAAAA';
    var config = [{
            'id':'banner-native-oferta-ad-1', // Exemplo de id container
            'pos': 1,
            'batch': 'natives-topo',
            'numAds': 2, 
            'customTargetings': {
                'cssTitleColor': cssTitleColor.toString(),
                'cssDescrColor': cssDescColor.toString(),
                'cssBgColor': cssBgColor.toString(),
                'cssListSpotlight': cssListSpotlight.toString(),
                'modelo': '95',
                'adblock': '1'
            }
        },{
        'id':'banner-native-oferta-ad-2', // Exemplo de id container
        'pos': 2,
        'numAds': 2, 
        'batch': 'natives-topo',
        'customTargetings': {
            'cssTitleColor': cssTitleColor.toString(),
            'cssDescrColor': cssDescColor.toString(),
            'cssBgColor': cssBgColor.toString(),
            'cssListSpotlight': cssListSpotlight.toString(),
            'modelo': '95',
            'adblock': '2'
        }
    }];

    for (var i = 0; i < config.length; i++) {
        window.uolads.push(config[i]);
    };
})();