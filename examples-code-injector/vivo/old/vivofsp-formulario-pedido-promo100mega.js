(function(){
    window.DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            if (DebugTracking.getDebug() === 'true') console.log('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId);
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
        var trackAff = new TrackingAfiliados();
        var saved = false;

        function saveValuesInLocalstorage() {
            var cep, phone;
            var queryString = window.location.search.split('&');
            for (var i = 0; i < queryString.length; i++) {
                if (queryString[i].indexOf('cep=') != -1) {
                    cep = queryString[i].substring(4);
                } else if(queryString[i].indexOf('telefone=') != -1) {
                   phone = queryString[i].substring(9);
                } else {
                    continue;
                }
                console.log('aqui');
            }

            if(!phone || !cep) {
                console.log('Não foi possível capturar o valores >', 'phone:', phone, 'cep:', cep);
                return false;
            }

            phoneValue = phone;
            phoneValue = phoneValue.replace(/\(/, '');
            phoneValue = phoneValue.replace(/\)/, '');
            phoneValue = phoneValue.replace(/\s/g, '-');
            
            var trackingValuesVivo = {'value': phoneValue+'_'+cep};
            localStorage.setItem('trackingValuesVivo', JSON.stringify(trackingValuesVivo));
        }

        function getUserID() {
            return (localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)) ? localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)[1] : '654654';
        }

        function callTracking(){
            var trackingValues = localStorage.getItem('trackingValuesVivo');
            if (!trackingValues) {
                return;   
            }
            
            var json = JSON.parse(trackingValues);
            
            //Get value from locasorage
            var orderId = json.value;
            
            //Get value from locastorage
            var userId = getUserID();

            if (!orderId) {
                console.log('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                 console.log('UserId inválido:', userId);
                 return;
            }
            
            //Call afiliate tracking
            trackAff.sendConversion('VIVOSPCOMBO', orderId, userId);
        }

        function validateForm() {
            var form = document.querySelectorAll('#form-cadastro input');
            if (DebugTracking.getDebug() === 'true') console.log('::: VALIDATE :::');
            for (var i = 0; i < form.length; i++) {
                if (form[i].hasAttribute('class')) {
                    if (form[i].getAttribute('class').indexOf('text-error') != -1) return false;
                }
            }
            return true;
        }

        function validateAndSave() {
            if (validateForm()) {
                saveValuesInLocalstorage();
            }
        }

        this.init = function () {
            var submit = document.querySelector('#btn-end');
            var submitTop = document.querySelector('#btn-end-top');
            if(submitTop) {
                submitTop.addEventListener('click', function(){ 
                    validateAndSave()
                });
            }
            if(submit) {
                submit.addEventListener('click', function(){
                    validateAndSave();
                });
            }
            var location = window.location;
            if(localStorage.getItem('trackingValuesVivo') && location.href.indexOf("/assine/Carrinho/Sucesso") != -1) {
                callTracking();
            }
            if (DebugTracking.getDebug() === 'true') console.log('::: INIT :::');
        }
    }

    DebugTracking.setDebug();
    //DebugTracking.clearDebug();

    //************************************************
    //------- VIVO SP por formulario: VIVOSPCOMBO -----
    //************************************************
    var form = new FormElements();
    form.init();
    //************************************************
    //------- VIVO SP por formulario: VIVOSPCOMBO -----
    //************************************************

})();