(function(){
    window.DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    if (DebugTracking.getDebug() === 'true') console.log('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId)
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
                
        var trackAff = new TrackingAfiliados();
        
        function getInputValueByName(name) {
            var inputs = document.querySelectorAll('.mCSB_container form input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].hasAttribute('name')) {
                    if (inputs[i].getAttribute('name').toLowerCase() === name) {
                        return inputs[i].value;
                    }
                }
            }
            return false;
        }

        function getBtnSubmit(text) {
            var btns = document.querySelectorAll('.mCSB_container form .btn-consult');
            for (var i = 0; i < btns.length; i++) {
                if (DebugTracking.getDebug() === 'true') console.log('::: MESSAGE:::', btns[i].innerText)
                if (btns[i].innerText === text) {
                    return btns[i];
                }
            }
            return false;
        }

        function checkFormIsValid() {
            if(!angular.element('form:eq(2)').scope())  {
                return false;
            }
            
            var scope = angular.element('form:eq(2)').scope();
            if(!scope.formCheckCoverage) {
                return false;
            }

            var formCheckCoverage = angular.element('form:eq(2)').scope().formCheckCoverage;
            if(formCheckCoverage.cep.$valid === false) {
                return false;
            } else if (formCheckCoverage.numero.$valid === false) {
                return false;
            } else if (formCheckCoverage.nome.$valid === false) {
                return false;
            } else if (formCheckCoverage.telefone.$valid === false) {
                return false;
            }

            return true;
        }

        function getPhoneAndCep() {
            var telefone = getInputValueByName('telefone');
            var cep = getInputValueByName('cep');

            if(!telefone || !cep) {
                console.log('Não foi possível capturar o valores >', 'telefone:', telefone, 'cep:', cep);
                return false;
            }

            telefone = telefone.replace(/\(/, '');
            telefone = telefone.replace(/\)/, '');
            telefone = telefone.replace(/\s/g, '-');
            
            return (telefone+'_'+cep);
        }

        function getUserID() {
            return (localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)) ? localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)[1] : 'dna';
        }

        function findSuccessMessage(callback) {
            var count = 0;
            function loop() {
                var element = document.querySelectorAll('.mCSB_container strong');
                if(element) {
                    for (var i = 0; i < element.length; i++) {
                        if (DebugTracking.getDebug() === 'true') console.log('Message', element[i], element[i].innerText);
                        if (element[i].innerText.toLowerCase() === 'obrigado pelo contato') {
                            callback(true);
                        } else if (count < 10){
                            if (DebugTracking.getDebug() === 'true') console.log('::: FIND SUCCESS MESSAGE :::', count);
                            setTimeout(loop, 1000);
                            count++;
                        }
                    }
                } else {
                    if (DebugTracking.getDebug() === 'true') console.log('::: FIND SUCCESS MESSAGE:::');
                    setTimeout(loop, 1000);
                }
            }
            loop();
        }

        function callTracking(data) {
            if(!data) {
                return;
            }

            //Get value from form
            var orderId = data.orderId;
                
            //Get value from locastorage
            var userId = data.userId;
            
            if (!orderId) {
                console.log('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                console.log('UserId inválido:', userId);
                return;
            }

            //Call afiliate tracking
            trackAff.sendConversion('VIVOFSPFONE', orderId, userId);
        }

        this.init = function () {
            function loop() {
                var button = getBtnSubmit('Receber Ligação');
                if (button) {
                    button.addEventListener('click', function() {
                        var aux = checkFormIsValid();
                        if (aux) {
                            var result = {'orderId': getPhoneAndCep(), 'userId' : getUserID()}
                            findSuccessMessage(function(e) {
                                if (e) {
                                    callTracking(result);
                                } else {
                                    console.log('Não foi possível chamar o traqueamento de afiliado');
                                }
                            });
                        }
                    })
                } else {
                    if (DebugTracking.getDebug() === 'true') console.log(' ::: FIND SUBMIT BUTTON :::');
                    setTimeout(loop, 1000);
                }
            }
            //find submit button
            loop();
        }
    }

    DebugTracking.setDebug();
    //DebugTracking.clearDebug();
    
    //************************************************
    //------- VIVO FSP por telefone: VIVOFSPFONE -----
    //************************************************
    var dom = new FormElements();
    dom.init();
    //************************************************
    //------- VIVO FSP por telefone: VIVOFSPFONE -----
    //************************************************

})();