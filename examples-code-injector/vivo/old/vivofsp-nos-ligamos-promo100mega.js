(function(){
    window.DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    if (DebugTracking.getDebug() === 'true') console.log('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId)
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
                
        var trackAff = new TrackingAfiliados();
        
        function getPhoneAndCep() {
            var phone = document.querySelector('#form_c2rc > div:nth-child(2) > input') || document.querySelector('#phone') || document.querySelector('#cellphone') || document.querySelector('#workphone');
            var cep = document.querySelector('#cep-delivery');
            
            if(!phone || !cep) {
                console.log('Não foi possível capturar o valores >', 'phone:', phone, 'cep:', cep);
                return false;
            }

            phone = phone.value;
            phone = phone.replace(/\(/, '');
            phone = phone.replace(/\)/, '');
            phone = phone.replace(/\s/g, '-');
            
            return (phone+'_'+cep.value);
        }

        function getUserID() {
            return (localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)) ? localStorage.getItem('tt_uid').match(/1\|(\w+)(;.+)?/)[1] : 'dna';
        }

        function callTracking(data) {
            if(!data) {
                return;
            }

            //Get value from form
            var orderId = data.orderId;
                
            //Get value from locastorage
            var userId = data.userId;
            
            if (!orderId) {
                console.log('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                console.log('UserId inválido:', userId);
                return;
            }

            if (DebugTracking.getDebug() === 'true') console.log('::: CALLTRACKING :::', data);

            //Call afiliate tracking
            trackAff.sendConversion('VIVOSPFONE', orderId, userId);
        }

        this.init = function () {
            if (DebugTracking.getDebug() === 'true') console.log('::: VIVOSPFONE :::');
            var result = {'orderId': getPhoneAndCep(), 'userId' : getUserID()};
            callTracking(result);
        }
    }

    DebugTracking.setDebug();
    //DebugTracking.clearDebug();
    
    //************************************************
    //------- VIVO SP por telefone: VIVOSPFONE -----
    //************************************************
    var dom = new FormElements();
    dom.init();
    //************************************************
    //------- VIVO SP por telefone: VIVOSPFONE -----
    //************************************************

})();