(function() {
    var jsuri = 'jsuol.com.br/p/afiliados/adrequisitor/aff_convpixel.js';

    function hasScript() {
        var tags = document.getElementsByTagName('script');
        for (var i = 0; i < tags.length; i++) {
            if (tags[i].src.indexOf(jsuri) != -1) {
                return true;
            }
        }
        return false;
    }

    function insertScript() {
        if (!hasScript()) {
            var js = document.createElement('script');
            js.src = 'https://' + jsuri;
            document.head.appendChild(js);
        }
    }

    insertScript();

    function sendConversion(codPromo, orderId, userId) {
        var tryUolAfConversion = setInterval(function() {
            if (window.uolAffConversion) {
                window.uolAffConversion(18, 'Vivo', codPromo, orderId, 1, 1, userId);
                clearInterval(tryUolAfConversion);
            }
        }, 100);
    }

    sendConversion([COD_PROMO], [ORDER_ID], [USER_ID]);
})();