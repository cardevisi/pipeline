(function() {
    
    window.triggerUOLTM = window.triggerUOLTM || [];
    window.DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            for (var i = 0; i <= arguments.length; i++) {
                message.push(arguments[i]);
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };
    
    DebugTracking.showMessage('::: INIT TAG SCRIPT AFILIADO :::');

    const ROI_URL = 'https://logger.rm.uol.com.br/v1?prd=246&msr=Erros:1&grp=';
    var productConfig = {
        "18": {"requiredParams":["source_id", "promo", "order_id", "total", "userId"],
               "templateAffiliated":"//click.afiliados.uol.com.br/Conversao?source=[source_id]&type=link&codPromotion=[promo]&idtTransacao=[order_id]&Value=[total]&idtAnunciante=CFL¶meters=userId:[userId]",
               "templateRm":"https://clicklogger.rm.uol.com.br/ts?oper=1&msr=Conversoes:1&prd=4&grp=order_id:[order_id];codPromotion:[promo];Value:[total]"
               },
        "default" : {"requiredParams":["source_id", "promo", "order_id", "total"],
               "templateAffiliated":"//click.afiliados.uol.com.br/Conversao?source=[source_id]&type=link&codPromotion=[promo]&idtTransacao=[order_id]&Value=[total]&idtAnunciante=CFL",
               "templateRm":"https://clicklogger.rm.uol.com.br/ts?oper=1&msr=Conversoes:1&prd=246&grp=order_id:[order_id];codPromotion:[promo];Value:[total]"
               }
    };
    function sanitize(variable) {
        return (typeof(variable) === 'string' && variable.length == 0) ? 'empty' : variable;
    }
    function getPrdConfig(sourceId) {   
        return (sourceId in productConfig) ? productConfig[sourceId] : productConfig["default"];
    }
    function isStringOrNumber(variable) {
        return typeof(variable) !== 'undefined' && variable !== null &&
        ((typeof(variable) === 'string' && variable.length > 0) || (typeof(variable) === 'number' && variable > 0));
    }
    function checkRequiredParams(input) {
        var prdConfig = getPrdConfig(input["source_id"]);
        for (var i = 0; i < prdConfig["requiredParams"].length; i++) {
            if (!isStringOrNumber(input[prdConfig["requiredParams"][i]])) {
                return false;
            }
        }
        return true;
    }
    function uolAffConversion(source_id_or_object, origin, promo, order_id, total) {
        var input = {};
        if (isStringOrNumber(source_id_or_object) && isStringOrNumber(origin)
         && isStringOrNumber(promo) && isStringOrNumber(order_id) && isStringOrNumber(total)) {
            input = {           
                "source_id": source_id_or_object,
                "origin": origin,
                "promo": promo,
                "order_id": order_id,
                "total": total
            };
        } else if (source_id_or_object !== null && typeof(source_id_or_object) === 'object') {
            input = source_id_or_object;
        }
        var prdConfig = getPrdConfig(input["source_id"]);
        if (!isStringOrNumber(input["source_id"]) || !checkRequiredParams(input)) {
            var roiGroupings = "";
            prdConfig["requiredParams"].forEach(function (param) {
                if (!isStringOrNumber(input[param])) {
                    roiGroupings += param + ":" + sanitize(input[param]) + ";";
                }
            });
            roiGroupings += 'origin:' + input["origin"];
            (new Image()).src = encodeURI(ROI_URL + roiGroupings);
            return;
        }
        var linkSrc = prdConfig["templateAffiliated"];
        Object.keys(input).forEach(function (param) {
            linkSrc = linkSrc.replace('[' + param + ']', input[param]);
        });
        (new Image()).src = encodeURI(linkSrc);
        var timeSeriesUrl = prdConfig["templateRm"];
        Object.keys(input).forEach(function (param) {
            timeSeriesUrl = timeSeriesUrl.replace('[' + param + ']', input[param]);
        });
        (new Image()).src = encodeURI(timeSeriesUrl);
    }
    window.uolAffConversion = uolAffConversion;
    DebugTracking.showMessage('::: CALL TRIGGER :::', 'afiliate-tag-loaded');
    window.triggerUOLTM.push({"eventName":"afiliate-tag-loaded"});
})()