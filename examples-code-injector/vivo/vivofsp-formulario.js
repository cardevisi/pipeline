(function(){
    var DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            for (var i = 0; i <= arguments.length; i++) {
                message.push(arguments[i]);
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    DebugTracking.showMessage('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId)
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
        
        var trackAff = new TrackingAfiliados();
        var TOTAL_TIME = 3600;
        
        function saveValuesInLocalstorage() {
            var phone = document.querySelector('#txtFone') || document.querySelector('#txtCelular');
            var cep = document.querySelector('#txtCEP');

            if(!phone || !cep) {
                console.log('Não foi possível capturar o valores >', 'phone:', phone, 'cep:', cep);
                return false;
            }

            phone = phone.value;
            phone = phone.replace(/\(/, '');
            phone = phone.replace(/\)/, '');
            phone = phone.replace(/\s/g, '-');

            DebugTracking.showMessage('::: SAVED IN LOCALSTORAGE :::', phone+'_'+cep.value);
            var trackingValuesVivo = {'value': phone+'_'+cep.value};
            localStorage.setItem('trackingValuesVivo', JSON.stringify(trackingValuesVivo));
        }

        function getUserID() {
            var tt_uid = localStorage.getItem('tt_uid'), dnaValue;
            if (tt_uid) {
                var dnaValue = tt_uid.match(/1\|(\w+)(;.+)?/);
                return (dnaValue && dnaValue[1]) ? dnaValue[1] : 'dna';
            }
        }

        function validateForm() {
            var inputs = document.querySelectorAll('#formCheckoutIdentificacao input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].getAttribute('name') === 'telefone' || inputs[i].getAttribute('name') === 'cep') {
                    console.log(inputs[i].getAttribute('name'), inputs[i].hasAttribute('class') , inputs[i].getAttribute('class').indexOf('erro'));
                    if(inputs[i].hasAttribute('class') && inputs[i].getAttribute('class').indexOf('erro') != -1) return false;
                }
            }
            return true;
        }

        function callTracking() {
            var trackingValues = localStorage.getItem('trackingValuesVivo');
            if (!trackingValues) {
                return;   
            }
            
            var json = JSON.parse(trackingValues);
            
            //Get value from locasorage
            var orderId = json.value;
            
            //Get value from locastorage
            var userId = getUserID();

            if (!orderId) {
                DebugTracking.showMessage('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                DebugTracking.showMessage('UserId inválido:', userId);
                return;
            }
            
            //Call afiliate tracking
            trackAff.sendConversion('VIVOFSPCOMBO', orderId, userId);
        }

        function checkLocation() {
            var location = window.location;
            return (location.href.indexOf("assine.vivo.com.br/checkout/sucesso") != -1) ? true : false;
        }

        function findSubmitButton() {
            var count = 0;
            function loop() {
                var button = document.querySelector('#formCheckoutIdentificacao > div > button');
                if (button) {
                    button.addEventListener('click', function(){
                        var aux = validateForm();
                        if (aux) {
                            saveValuesInLocalstorage();
                            findSucessMessage();
                        }
                    });

                    //****************************
                    saveValuesInLocalstorage();
                } else if (count < TOTAL_TIME){
                    if (DebugTracking.getDebug() === 'true') console.log('::: FIND SUBMIT BUTTON 2:::');
                    if (!checkLocation()) setTimeout(loop, 1000);
                    count++;
                }
            }
            loop();
        }

        function findSucessMessage () {
            var count = 0;
            function loop() {
                var fieldMessage = document.querySelector('.wrap-container-checkout .sucesso');
                if(fieldMessage) {
                    DebugTracking.showMessage('::: FIND IF SUCCESS MESSAGE :::', (localStorage.getItem('trackingValuesVivo') && checkLocation()));
                    if(localStorage.getItem('trackingValuesVivo') && checkLocation()) {
                      callTracking();
                    }
                } else if(count < TOTAL_TIME) {
                    DebugTracking.showMessage('::: FIND LOOP SUCCESS MESSAGE :::');
                    setTimeout(loop, 1000);
                    count++;
                }
            }
            loop();
        }

        this.init = function () {
            findSubmitButton();
            findSucessMessage();
            DebugTracking.showMessage('::: INIT :::');
        }
    }

    //DebugTracking.setDebug();
    //DebugTracking.clearDebug();
    
    //************************************************
    //------- VIVO FSP por formulario: VIVOFSPCOMBO -----
    //************************************************
    
    // TAG NAME: Tracking Afiliado - VIVOFSPCOMBO - FORMULARIO

    var dom = new FormElements();
    dom.init();
    //************************************************
    //------- VIVO FSP por formulario: VIVOFSPCOMBO -----
    //************************************************

})();