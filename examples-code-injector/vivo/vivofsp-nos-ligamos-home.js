(function(){
    var DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            for (var i = 0; i <= arguments.length; i++) {
                message.push(arguments[i]);
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    DebugTracking.showMessage('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId)
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
        
        var trackAff = new TrackingAfiliados();
        var TOTAL_TIME = 3600;
        
        function getInputValueByName(name) {
            var inputs = document.querySelectorAll('.mCSB_container form input');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].hasAttribute('name')) {
                    if (inputs[i].getAttribute('name').toLowerCase() === name) {
                        return inputs[i].value;
                    }
                }
            }
            return false;
        }

        function getBtnSubmit(text) {
            var btns = document.querySelectorAll('.mCSB_container form .btn-consult');
            for (var i = 0; i < btns.length; i++) {
                DebugTracking.showMessage('::: MESSAGE:::', btns[i].innerText)
                if (btns[i].innerText === text) {
                    return btns[i];
                }
            }
            return false;
        }

        function checkFormIsValid() {
            if(!angular.element('form:eq(2)').scope())  {
                return false;
            }
            
            var scope = angular.element('form:eq(2)').scope();
            if(!scope.formCheckCoverage) {
                return false;
            }

            var formCheckCoverage = angular.element('form:eq(2)').scope().formCheckCoverage;
            if(formCheckCoverage.cep.$valid === false) {
                return false;
            } else if (formCheckCoverage.numero.$valid === false) {
                return false;
            } else if (formCheckCoverage.nome.$valid === false) {
                return false;
            } else if (formCheckCoverage.telefone.$valid === false) {
                return false;
            }

            return true;
        }

        function getPhoneAndCep() {
            var telefone = getInputValueByName('telefone');
            var cep = getInputValueByName('cep');

            if(!telefone || !cep) {
                DebugTracking.showMessage('Não foi possível capturar o valores >', 'telefone:', telefone, 'cep:', cep);
                return false;
            }

            telefone = telefone.replace(/\(/, '');
            telefone = telefone.replace(/\)/, '');
            telefone = telefone.replace(/\s/g, '-');
            
            return (telefone+'_'+cep);
        }

        function getUserID() {
            var tt_uid = localStorage.getItem('tt_uid'), dnaValue;
            if (tt_uid) {
                var dnaValue = tt_uid.match(/1\|(\w+)(;.+)?/);
                return (dnaValue && dnaValue[1]) ? dnaValue[1] : 'dna';
            }
        }

        function findSuccessMessage(callback) {
            var count = 0;
            function loop() {
                //var element = document.querySelectorAll('.mCSB_container strong');
                var element = document.querySelectorAll('.mCSB_container > div > div > section > header > h2 > strong');
                if(element) {
                    for (var i = 0; i < element.length; i++) {
                        DebugTracking.showMessage('Message', element[i], element[i].innerText);
                        if (element[i].innerText.toLowerCase() === 'obrigado pelo contato') {
                            callback(true);
                        } else if (count < TOTAL_TIME){
                            DebugTracking.showMessage('::: FIND SUCCESS MESSAGE :::', count);
                            setTimeout(loop, 1000);
                            count++;
                        }
                    }
                } else {
                    DebugTracking.showMessage('::: FIND SUCCESS MESSAGE:::');
                    setTimeout(loop, 1000);
                }
            }
            loop();
        }

        function callTracking(data) {
            if(!data) {
                return;
            }

            //Get value from form
            var orderId = data.orderId;
                
            //Get value from locastorage
            var userId = data.userId;
            
            if (!orderId) {
                DebugTracking.showMessage('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                DebugTracking.showMessage('UserId inválido:', userId);
                return;
            }

            //Call afiliate tracking
            trackAff.sendConversion('VIVOFSPFONE', orderId, userId);
        }

        this.init = function () {
            var count = 0;
            function loop() {
                var button = getBtnSubmit('Receber Ligação');
                if (button) {
                    button.addEventListener('click', function() {
                        var aux = checkFormIsValid();
                        if (aux) {
                            var result = {'orderId': getPhoneAndCep(), 'userId' : getUserID()}
                            findSuccessMessage(function(e) {
                                if (e) {
                                    callTracking(result);
                                }
                            });
                        }
                    })
                } else if(count < TOTAL_TIME) {
                    DebugTracking.showMessage(' ::: FIND SUBMIT BUTTON :::');
                    setTimeout(loop, 1000);
                    count++;
                }
            }
            //find submit button
            loop();
        }
    }

    //DebugTracking.setDebug();
    //DebugTracking.clearDebug();
    
    //************************************************
    //------- VIVO FSP por telefone: VIVOFSPFONE -----
    //************************************************

    // TAG NAME: Tracking Afiliado - VIVOFSPFONE - HOME
    
    var dom = new FormElements();
    dom.init();
    //************************************************
    //------- VIVO FSP por telefone: VIVOFSPFONE -----
    //************************************************

})();