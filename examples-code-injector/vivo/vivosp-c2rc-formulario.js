(function(){
    var DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            for (var i = 0; i <= arguments.length; i++) {
                message.push(arguments[i]);
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };
    function TrackingAfiliados() {
        this.sendConversion = function(codPromo, orderId, userId) {
            DebugTracking.showMessage('::: SENDCONVERSION :::', 18, 'Vivo', codPromo, orderId, 1, 1, userId);
            var tryUolAfConversion = setInterval(function() {
                if (window.uolAffConversion) {
                    window.uolAffConversion({source_id: 18, origin: 'Vivo', promo: codPromo, order_id: orderId, total: 1, userId: userId});
                    clearInterval(tryUolAfConversion);
                }
            }, 100);
        }
    }

    function FormElements() {
        
        var trackAff = new TrackingAfiliados();
        var TOTAL_TIME = 3600;

        function saveValuesInLocalstorage() {
            var cep, phone;
            var queryString = window.location.search.split('&');
            for (var i = 0; i < queryString.length; i++) {
                if (queryString[i].indexOf('cep=') != -1) {
                    cep = queryString[i].split('=')[1];
                } else if(queryString[i].indexOf('telefone=') != -1) {
                    phone = queryString[i].split('=')[1];
                    phone = decodeURI(phone);
                } else {
                    continue;
                }
            }

            if(!phone || !cep) {
                DebugTracking.showMessage('Não foi possível capturar o valores >', 'phone:', phone, 'cep:', cep);
                return false;
            }

            phoneValue = phone;
            phoneValue = phoneValue.replace(/\s/g, '');
            phoneValue = phoneValue.replace(/\(/, '');
            phoneValue = phoneValue.replace(/\)/, '-');
            phoneValue = phoneValue.replace(/\s/g, '-');

            DebugTracking.showMessage('::: SAVE IN LOCALSTORAGE :::', phoneValue+'_'+cep);

            var trackingValuesVivo = {'value': phoneValue+'_'+cep};
            localStorage.setItem('trackingValuesVivo', JSON.stringify(trackingValuesVivo));
        }

        function getUserID() {
            var tt_uid = localStorage.getItem('tt_uid'), dnaValue;
            if (tt_uid) {
                var dnaValue = tt_uid.match(/1\|(\w+)(;.+)?/);
                return (dnaValue && dnaValue[1]) ? dnaValue[1] : 'dna';
            }
        }

        function callTracking(){
            var trackingValues = localStorage.getItem('trackingValuesVivo');
            if (!trackingValues) {
                return;   
            }
            
            var json = JSON.parse(trackingValues);
            
            //Get value from locasorage
            var orderId = json.value;
            
            //Get value from locastorage
            var userId = getUserID();

            if (!orderId) {
                DebugTracking.showMessage('Orderid inválido:', orderId);
                return;
            }

            if (!userId) {
                DebugTracking.showMessage('UserId inválido:', userId);
                return;
            }

            //Call afiliate tracking
            var location = window.location;
            if(location.href.indexOf("/assine/Carrinho/Sucesso") != -1) {
                trackAff.sendConversion('VIVOSPCOMBO', orderId, userId);
            } else if(location.href.indexOf('www.vivotv.com.br/assine/Monte') != -1) {
                trackAff.sendConversion('VIVOSPFONE', orderId, userId);
            } else if(location.href.indexOf('www.vivotv.com.br/assine/Carrinho/Cadastro') != -1) {
                trackAff.sendConversion('VIVOSPFONE', orderId, userId);
            }
        }

        function findMessageOk(callback) {
            var count = 0;
            function loop () {
                var messageOk = document.querySelector('#msg_ok');
                if(messageOk) {
                    callback();
                } else if(count < TOTAL_TIME) {
                    DebugTracking.showMessage('::: FIND SUCCESS MESSAGE :::');
                    setTimeout(loop, 1000);
                }
            }

            loop();
        }

        this.init = function () {
            DebugTracking.showMessage('::: INIT :::');
            
            saveValuesInLocalstorage();

            var phoneButton = document.querySelector('#form_c2rc > div:nth-child(3) > input');
            if (phoneButton) {
                phoneButton.addEventListener('click', function(){
                    findMessageOk(function (){
                        callTracking();
                    });
                });
            } else {
                callTracking();
            }

        }
    }

    //DebugTracking.setDebug();
    //DebugTracking.clearDebug();

    //************************************************
    //------- VIVO SP por C2RC e formulario:  -----
    //************************************************

    //TAG NAME: Tracking Afiliado - VIVO SP - C2RC e  Formulário
    
    var form = new FormElements();
    form.init();
    //************************************************
    //------- VIVO SP por C2RC e formulario:  -----
    //************************************************

})();