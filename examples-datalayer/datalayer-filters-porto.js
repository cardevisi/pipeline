(function() {

  //window.dataLayer = [];

  /******** STEP PURCHASE **************/
  /*window.dataLayer = [{
    "ecommerce": {
      "purchase": {
        "actionField": {
          "action": "purchase",
          "affiliation": "",
          "coupon": null,
          "currency": "BRL",
          "id": 102847,
          "revenue": 199,
          "shipping": 0,
          "tax": 0
        },
        "products": [{
          "category": false,
          "id": "18022",
          "name": "Instalação de Fechadura Digital (fechadura não inclusa)",
          "price": 199,
          "quantity": "1",
          "sku": "23732",
          "variant": "23732",
          "brand": ""
        }]
      }
    }
  }, {
    "gtm.start": 1501621644865,
    "event": "gtm.js",
    "gtm.uniqueEventId": 0
  }, {
    "event": "gtm.dom",
    "gtm.uniqueEventId": 2
  }, {
    "gtm.start": 1501621645408,
    "event": "gtm.js",
    "gtm.uniqueEventId": 3
  }, {
    "event": "gtm.load",
    "gtm.uniqueEventId": 5
  }, {
    "event": "gtm.pageError",
    "gtm.errorMessage": "Script error.",
    "gtm.errorUrl": "",
    "gtm.errorLineNumber": 0,
    "gtm.uniqueEventId": 6
}];/

/******** STEP PURCHASE **************/
  /* PRODUTO */
  /*window.dataLayer = [{
    "ecommerce": {
      "detail": {
        "actionField": {
          "list": "Catálogo de Serviços",
          "action": "detail"
        },
        "products": [{
          "name": "Instalação de Fechadura Digital (fechadura não inclusa)",
          "id": 17905,
          "price": "219",
          "brand": "",
          "category": "",
          "variant": ""
        }]
      }
    }
  }, {
    "gtm.start": 1501621644865,
    "event": "gtm.js",
    "gtm.uniqueEventId": 0
  }, {
    "event": "gtm.dom",
    "gtm.uniqueEventId": 2
  }, {
    "gtm.start": 1501621645408,
    "event": "gtm.js",
    "gtm.uniqueEventId": 3
  }, {
    "event": "gtm.load",
    "gtm.uniqueEventId": 5
  }, {
    "event": "gtm.pageError",
    "gtm.errorMessage": "Script error.",
    "gtm.errorUrl": "",
    "gtm.errorLineNumber": 0,
    "gtm.uniqueEventId": 6
}];*/
/*****************/

  /* CARRINHO */
  window.dataLayer = [{
    "event": "checkout",
    "ecommerce": {
      "checkout": {
        "products": [{
          "name": "Instalação de Fechadura Digital (fechadura não inclusa)",
          "id": 22787,
          "price": 500,
          "brand": "",
          "category": "",
          "variant": "",
          "quantity": 1
        },{
          "name": "Instalação de Fechadura Digital (fechadura não inclusa)",
          "id": 22787,
          "price": 359,
          "brand": "",
          "category": "",
          "variant": "",
          "quantity": 1
        }]
      }
    },
    "gtm.uniqueEventId": 0
  }, {
    "event": "addToCart",
    "ecommerce": {
      "currencyCode": "BRL",
      "add": {
        "products": [{
          "name": "Instalação de Fechadura Digital (fechadura não inclusa) B",
          "id": 22787,
          "price": 359,
          "brand": "",
          "category": "",
          "variant": "",
          "quantity": 1
        }, {
          "name": "Instalação de Fechadura Digital (fechadura não inclusa) A",
          "id": 33333,
          "price": 500,
          "brand": "",
          "category": "",
          "variant": "",
          "quantity": 2
        }]
      }
    },
    "gtm.uniqueEventId": 1
  }, {
    "gtm.start": 1501703346932,
    "event": "gtm.js",
    "gtm.uniqueEventId": 2
  }, {
    "event": "gtm.dom",
    "gtm.uniqueEventId": 4
  }, {
    "gtm.start": 1501703347243,
    "event": "gtm.js",
    "gtm.uniqueEventId": 5
  }, {
    "gtm.element": {
      "jQuery311026155669364890511": {
        "display": ""
      },
      "jQuery311026155669364890512": {
        "order_button_text": ""
      }
    },
    "gtm.elementClasses": "input-radio",
    "gtm.elementId": "payment_method_mundipagg-credit-card",
    "gtm.elementTarget": "",
    "event": "gtm.click",
    "gtm.elementUrl": "",
    "gtm.uniqueEventId": 7
  }, {
    "event": "gtm.load",
    "gtm.uniqueEventId": 8
  }];


  try {

    var uv = window.universal_variable || {};
    var pageType, total;
    uv.page = {};
    uv.user = {};

    function normalizePrice(value) {
      return Number(value) * 100;
    }

    function filterPropertys(source, query) {
      var result = [];
      source.filter(function(item) {
        return item[query];
      }).forEach(function(item) {
        result.push(item[query]);
      });
      return result;
    }

    function checkPageType(ecommerce) {
      var type;
      if (ecommerce.hasOwnProperty("detail")) {
        type = "detail";
      } else if (ecommerce.hasOwnProperty("checkout")) {
        type = "checkout";
      } else if (ecommerce.hasOwnProperty("purchase")) {
        type = "purchase";
      }
      return type;
    }

    function createProduct(arr) {
      var product = arr.map(function(item) {
        return {
          "sku_code": item.id.toString(),
          "description": item.name,
          "category": item.category,
          "unit_price": normalizePrice(item.price),
          "subcategory": "",
          "currency": "BRL"
        };
      });
      return product[0];
    }

    function createBasket(arr) {
      var list = arr.map(function(item) {
        return {
          "product": {
            "sku_code": item.id.toString(),
            "description": item.name,
            "category": item.category,
            "unit_price": normalizePrice(item.price),
            "subcategory": ""
          }
        };
      });
      return list;
    }

    function updateProductUvar() {
      var productValue = document.querySelector('.preco-total span > span');
      if (productValue) {
        productValue = productValue.textContent.replace('R$', '').replace(',', '.').trim();
        uv.product.unit_price = normalizePrice(productValue);
      }
    }

    function setListeners() {
      var inputs = document.querySelectorAll('#content-agenda .checkbox-product');
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('click', function(e) {
          updateProductUvar();
          callTrigger();
        });
      }
    }

    function callTrigger() {
      console.log('call-trigger', 'uvar-created');
      window.triggerUOLTM = window.triggerUOLTM || [];
      window.triggerUOLTM.push({"eventName": "uvar-created"}); 
    }

    var ecommerce = filterPropertys(dataLayer, "ecommerce")[0];
    var detalhesPedido = document.querySelector('.thankyou-detalhes-pedido > h2');
    
    if (ecommerce) {
      pageType = checkPageType(ecommerce);
    } else if (detalhesPedido) {
      pageType = "purchase";
      
    }
    
    uv.page.category = pageType;
    uv.user.user_id = "";

    switch (pageType) {
      case "detail":
        uv.product = {};
        uv.product = createProduct(ecommerce.detail.products);
        window.localStorage.setItem("detail-product", JSON.stringify(uv.product));
      break;
      case "checkout":
        uv.basket = {};
        uv.basket = {
            "currency": "BRL",
            "total": "",
            "line_items": createBasket(ecommerce.checkout.products)
        };
      break;
      case "purchase":

        var numeroPedido = document.querySelector(".numero-pedido");
        if(numeroPedido) {
            numeroPedido = numeroPedido.textContent.replace(/\s/g, '').trim();
            numeroPedido = numeroPedido.replace('#', '');
        }

        total = document.querySelector("#thankyou-valor-total");
        if(total) {
          total = total.textContent.replace(/\s/g, '');
          total = total.replace('R$', '');
          total = total.replace(',', '.');
        } else {
          total = ecommerce.purchase.actionField.revenue;
        }

        var productStorage = window.localStorage.getItem("detail-product");
        if(productStorage) {
          productStorage = JSON.parse(productStorage);
        }

        uv.basket = {};
        uv.basket = {
            "currency": (ecommerce  && ecommerce.purchase.actionField.currency) ? ecommerce.purchase.actionField.currency : "BRL",
            "total": normalizePrice(total),
            "line_items": (ecommerce  && ecommerce.purchase.products) ? createBasket(ecommerce.purchase.products) : [{'product':productStorage}]
        };

        
          // uv.transaction = {};
          // uv.transaction.order_id = "";
          // uv.transaction.currency = "";
          // uv.transaction.payment_type = "";
          // uv.transaction.total = "";
        
      
      break;
    }

    window.universal_variable = uv;
    setListeners();
    callTrigger();

  
  } catch (error) {
    console.warn('Não foi possível criar a universal_variable: ', error);
  }

  function get(a) {
    var b = window.dataLayer;
    
    a = a.split(".");

    for (var c = 0; c < a.length; c++) {
      console.log(b, b[a[c]]);
      if (void 0 === b[a[c]]) return;
      b = b[a[c]]
      console.log('B', b);
    }
    return b
  };
  
  console.log('get', get("event"));

})();