(function(){
      window.dataLayer = [
          {
            //"pageName": "Static.checkoutsuccess",
            "pageName": "cart",
            "ET": "successpage",
            "ProdId": [
              "PO091SHM71FQI-51"
            ],
            "Pcat": [
              ""
            ],
            "transactionValue": "119.9",
            "grandTotal": "119.90",
            "transactionPaymentType": "braspag_boleto",
            "partnerId": null,
            "skuList": "PO091SHM71FQI",
            "skuListComma": "PO091SHM71FQI",
            "priceList": "119.90",
            "webtrekkOrderDataNoScript": "ov=119.90&oi=93746318&ba=PO091SHM71FQI&co=119.90&qn=1&st=conf&ca1=sapatenis&ca2=&ca3=&ca4=Polo HPC&cb1=Polo HPC&cb2=Azul&cb3=&cb4=braspag_boleto&cb5=&cb6=&cb7=&cb8=&cb9=ExistingCustomer&cb10=braspag_cc;braspag_boleto;full_refund_voucher;groupon&cb11=0&cb12=0&cb13=&cb14=119.90&cd=7082305",
            "categoryList": "calcados",
            "utmsource": "checkout",
            "webtrekkOrderData": "",
            "name": "",
            "sku": "[\"PO091SHM71FQI-51\"]",
            "gender": "",
            "categoryName": "",
            "subCategory": "",
            "price": "",
            "brand": "",
            "color": "",
            "productList": "Sapatênis Polo HPC Logo Azul",
            "brandList": "Polo HPC",
            "colorList": "Azul",
            "sizeList": "37",
            "birthdayCustomer": "1990-01-01",
            "cityCustomer": "",
            "regionCustomer": "São Paulo",
            "paymentMethod": "braspag_boleto",
            "voucherUsed": "0",
            "genderCustomer": "",
            "id_customer": "7082305",
            "customerType": "existing",
            "zanpid": "",
            "coupon": "",
            "netotiateEnabled": 1,
            "transactionId": 47474747,
            "transactionProducts": [
              {
                "sku": "PO091SHM71FQIB",
                "name": "Sapatênis Polo HPC Logo Preto",
                "category": "calcados|calcados-femininos",
                "price": "219.90",
                "quantity": 2,
                "negotiation_price_id": null,
                "negotiation_reference_id": null
              },
              {
                "sku": "PO091SHM71FQI",
                "name": "Sapatênis Polo HPC Logo Azul",
                "category": "calcados|calcados-masculinos",
                "price": "119.90",
                "quantity": 1,
                "negotiation_price_id": null,
                "negotiation_reference_id": null
              }
            ],
            "transactionShipping": 0,
            "transactionTotal": 119.9,
            "oMC": 3000795,
            "brandID": "[22091]",
            "categoryID": "[4,24,678]"
          },
          {
            "transactionValue": "1000",
            "gtm.start": 1458674221515,
            "event": "gtm.js"
          },
          {
            "pageName": "carrinho",
            "event": "gtm.dom"
          },
          {
            "event": "gtm.load"
          },
          {
            "pageName": "produto",
            "gtm.element": {},
            "gtm.elementClasses": "modal-overlay",
            "gtm.elementId": "",
            "gtm.elementTarget": "",
            "event": "gtm.click",
            "gtm.elementUrl": ""
          }]


  try {

    function normalizePrice(value) {
      return Number(value)*100;
    }
    
    function filterPropertys(source, query) {
      var result = [];
      var obj = {};
      source.filter(function(item) {
          return item[query];
      }).forEach(function(item){
          result.push(item[query]);
      });
      return result.map(function(item){
        //debugger;
        if (!(item instanceof Array)) {
          var json = JSON.parse('{"'+query+'":"'+item+'"}');
        } else {
          json = item;
        }
        
        return json;
      });
    }

    console.log('transactionProducts', filterPropertys(dataLayer, 'transactionProducts'));
    console.log('transactionValue', filterPropertys(dataLayer, 'transactionValue'));
    console.log('pageName', filterPropertys(dataLayer, 'pageName'));

    var transactionProducts = filterPropertys(dataLayer, 'transactionProducts')[0].map(function(item, index, arr){
        console.log('map', item, index);
        return {
          "product": {
            "sku_code": item.sku,
            "description": item.name,
            "category": item.category,
            "subcategory": "",
            "unit_price": normalizePrice(item.price),
            "currency": "BR"
          }
        };
    });

    console.log('TRANSACTIONPRODUCTS', transactionProducts);

  } catch(error) {
    console.log(error);
  }

})();