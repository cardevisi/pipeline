var getPixels = require("get-pixels")
 
getPixels("lena.png", function(err, pixels) {
  if(err) {
    console.log("Bad image path")
    return
  }

  for (var i = 0; i < pixels.length; i++) {
      console.log('PIXEL', pixels[i]);
  };
  //console.log("got pixels", pixels, pixels.shape.slice())
})