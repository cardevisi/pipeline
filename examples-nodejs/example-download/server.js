const http = require('http');
const express = require('express');
const app = express();

app.get('/', function(req, res){
	res.send('Hello');
});

app.get('/download', function(req, res){
	console.log(req, res);
	var file = 'files/index.html';
	res.download(file, 'rename.html');
});

http.createServer(app).listen(3000, function() {
    console.log('::::::::: Server Running :::::::');
});