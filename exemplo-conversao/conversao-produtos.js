Tags Trillium

Teremos que substituir as tags já implementadas.
Como tem uma métrica a ser capturada dinamicamente, não sei subir as tags por aqui.

Segue resposta do parceiro:

Tendo em conta que as tags TrilliumInteractive são entregues através da chamada de script do Tag Manager, sendo todo o processo feito por javascript, tive então que mudar o protocolo de tracking para s2s callback, para por o pixel a funcionar. Seguem abaixo então os pixeis correctos para conseguir testar e trackear correctamente as ofertas:

Games - CPA - Mobile - UOL Sala de Jogos
Pixel: http://trilliuminteractive.go2cloud.org/SPAo?transaction_id=TRANSACTION_ID
Test Link: http://trilliuminteractive.go2cloud.org/aff_c?offer_id=1068&aff_id=1&source=testoffer&aff_sub=testoffer

Utility Tools - CPA - Mobile - UOL Antivírus
Pixel: http://trilliuminteractive.go2cloud.org/SPAq?transaction_id=TRANSACTION_ID
Test Link: http://trilliuminteractive.go2cloud.org/aff_c?offer_id=1066&aff_id=1&source=testoffer&aff_sub=testoffer

Utility Tools - CPA - Mobile - UOL Assistência Técnica
Pixel: http://trilliuminteractive.go2cloud.org/SPAs?transaction_id=TRANSACTION_ID
Test Link: http://trilliuminteractive.go2cloud.org/aff_c?offer_id=1064&aff_id=1&source=testoffer&aff_sub=testoffer

Utility Tools - CPA - Mobile - UOL Backup
Pixel: http://trilliuminteractive.go2cloud.org/SPAu?transaction_id=TRANSACTION_ID
Test Link: http://trilliuminteractive.go2cloud.org/aff_c?offer_id=1062&aff_id=1&source=testoffer&aff_sub=testoffer

Utility Tools - CPA - Mobile - UOL WiFi
Pixel: http://trilliuminteractive.go2cloud.org/SPAw?transaction_id=TRANSACTION_ID
Test Link: http://trilliuminteractive.go2cloud.org/aff_c?offer_id=1060&aff_id=1&source=testoffer&aff_sub=testoffer


//*********************
// UOL SALA DE JOGOS
//*********************
try {
    var TRANSACTION_ID = skinControl.inscriptionId;
    var ifrm = document.createElement('iframe');
    ifrm.setAttribute('src', '//trilliuminteractive.go2cloud.org/SPAo?transaction_id='+TRANSACTION_ID);
    ifrm.setAttribute('scrolling', 'no');
    ifrm.setAttribute('frameborder', '0');
    ifrm.setAttribute('width', '0');
       ifrm.setAttribute('height', '0');
    document.body.appendChild(ifrm);
} catch(e){}


//*********************
// UOL BACKUP
//*********************
try {
    var TRANSACTION_ID = new Date().getTime();
    if(window.skinControl && window.skinControl.inscriptionId) {
        TRANSACTION_ID = window.skinControl.inscriptionId;
    }
    var ifrm = document.createElement('iframe');
    ifrm.setAttribute('src', '//trilliuminteractive.go2cloud.org/SPAu?transaction_id='+TRANSACTION_ID);
    ifrm.setAttribute('scrolling', 'no');
    ifrm.setAttribute('frameborder', '0');
    ifrm.setAttribute('width', '0');
        ifrm.setAttribute('height', '0');
    document.body.appendChild(ifrm);
} catch(e){}


//*********************
// UOL ASSISTÊNCIA TÉCNICA
//*********************
try {
    var TRANSACTION_ID = new Date().getTime();
    if(window.skinControl && window.skinControl.inscriptionId) {
        TRANSACTION_ID = window.skinControl.inscriptionId;
    }
    var ifrm = document.createElement('iframe');
    ifrm.setAttribute('src', '//trilliuminteractive.go2cloud.org/SPAs?transaction_id='+TRANSACTION_ID);
    ifrm.setAttribute('scrolling', 'no');
    ifrm.setAttribute('frameborder', '0');
    ifrm.setAttribute('width', '0');
    ifrm.setAttribute('height', '0');
    document.body.appendChild(ifrm);
} catch(e){}


//*********************
// UOL ANTIVÍRUS
//*********************
try {
    var TRANSACTION_ID = new Date().getTime();
    if(window.skinControl && window.skinControl.inscriptionId) {
        TRANSACTION_ID = window.skinControl.inscriptionId;
    }
    var ifrm = document.createElement('iframe');
    ifrm.setAttribute('src', '//trilliuminteractive.go2cloud.org/SPAq?transaction_id=TRANSACTION_ID');
    ifrm.setAttribute('scrolling', 'no');
    ifrm.setAttribute('frameborder', '0');
    ifrm.setAttribute('width', '0');
    ifrm.setAttribute('height', '0');
    document.body.appendChild(ifrm);
} catch(e){}



//*********************
// UOL SALA DE JOGOS
//*********************
try {
    var TRANSACTION_ID = new Date().getTime();
    if(window.skinControl && window.skinControl.inscriptionId) {
        TRANSACTION_ID = window.skinControl.inscriptionId;
    }
    var ifrm = document.createElement('iframe');
    ifrm.setAttribute('src', '//trilliuminteractive.go2cloud.org/SPAq?transaction_id=TRANSACTION_ID');
    ifrm.setAttribute('scrolling', 'no');
    ifrm.setAttribute('frameborder', '0');
    ifrm.setAttribute('width', '0');
    ifrm.setAttribute('height', '0');
    document.body.appendChild(ifrm);
} catch(e){}

//*********************
// UOL OPTMIZE
//*********************

    window.universal_variable = {
        'transaction' : {
            'order_id' : '4654654654654131345',
            'total': '40.00',
            'line-itens': [
                '54654', 
                'fsdsfs'
            ]
        }
    }
    
    try {
        var order_id = "";
        if(window.universal_variable && window.universal_variable.transaction) {
            var order_id = window.universal_variable.transaction.order_id;
        }
        var pixel = document.createElement('script');
        pixel.src = '//track.omguk.com/815538/transaction.asp?APPID=' + order_id + '&MID=815538&PID=15507&status=30.00';
        pixel.async = true;
        document.head.appendChild(pixel);
    } catch(e) {}

    <script src="//tm.jsuol.com.br/uoltm.js?id=z428z6"></script>


//*********************
// ------------------------------------------------
//*********************


.responsive-native-home