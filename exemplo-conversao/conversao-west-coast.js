(function(){
			'use-sctrict'

			window.dataLayer = window.dataLayer || [];
			try {

				(function(window){
					function Trigger() {}
					
					Trigger.prototype.callByName = function(name) {
						console.log('trigger', name);
				      	window.triggerUOLTM = window.triggerUOLTM || [];
				      	window.triggerUOLTM.push({"eventName": name});
					};

					window.Trigger = Trigger;
				})(window);


				(function(window){
					function Filters() {}

					Filters.prototype.filterPropertys = function(source, query) {
						var result = source.filter(function(item) {
				        	return item[query];
				      	});
				      	return result;
					};

					window.Filters = Filters;
				
				})(window);


				(function(window){
					function Main(source) {
						this.filter = new Filters();
						this.trigger = new Trigger();
						this.dataLayer = source; 
					}
					
					Main.prototype.init = function(keyName) {
						var that = this;
						var products = this.filter.filterPropertys(this.dataLayer, keyName);
						if (products instanceof Array && products.length === 0) {
							return;
						}
					    products[0].transactionProducts.forEach(function(element) {
					    	if (element.productId === '1982-0') {
								that.trigger.callByName('sneaker-azul-marinho');
					    	} else if (element.productId === '1983-0'){
								that.trigger.callByName('sneacker-cp-verde');
							} else if (element.productId === '2228-0'){
								that.trigger.callByName('sneacker-azul');
					    	} else if (element.productId === '295-0'){
								that.trigger.callByName('worker-culture-camel');
					    	}
					    });
					};

					window.Main = Main;

				})(window);

				var main = new Main(window.dataLayer);
				main.init('transactionProducts');
			
			} catch (error) {
		    	console.warn('Não foi possível disparar os gatilhos: ', error);
		  	}
		})(window)