(function() {
    try {
        function appendIframe(src) {
            var iframe = document.createElement('iframe');
            iframe.setAttribute('src', src);
            iframe.setAttribute('width', 0);
            iframe.setAttribute('height', 0);
            iframe.style.display = 'none';

            document.body.appendChild(iframe);
        }

        function getIframeSrc(cookie, cookieExp, client, expires) {
            var src  = '//tm.uol.com.br/dynad-track-cookies.html';
                src += '?name=' + cookie;
                src += '&expname=' + cookieExp;
                src += '&client=' + client;
                if (expires === 0) {
                    src += '&delete=true';
                } else {
                    src += '&expires=' + expires;
                }
                src += '&expdomain=1';

            return src;
        }

        function addCookies(client, expires) {
            appendIframe(getIframeSrc('DEretargeting', 'DEretargetingExp', client, expires));
            appendIframe(getIframeSrc('dynad_rt', 'dynad_rt_exp', client, expires));
        }

        setTimeout(function () {
            addCookies('503.1', 0);
            addCookies('503.3', 3);
            addCookies('503.7', 7);
        }, 1);
    } catch(e){}
})();

