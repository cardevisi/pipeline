function appendIframe(src) {
    var iframe = document.createElement('iframe');
    iframe.setAttribute('src', src);
    iframe.setAttribute('width', 0);
    iframe.setAttribute('height', 0);
    iframe.style.display = 'none';

    document.body.appendChild(iframe);
}

function getIframeSrc(cookie, cookieExp, client, expires) {
    var src  = '//tm.uol.com.br/dynad-track-cookies.html';
        src += '?name=' + cookie;
        src += '&expname=' + cookieExp;
        src += '&client=' + client;
        if (expires === 0) {
            src += '&delete=true';
        } else {
            src += '&expires=' + expires;
        }
        src += '&expdomain=1';

    return src;
}

function addCookies(client, expires) {
    appendIframe(getIframeSrc('DEretargeting', 'DEretargetingExp', client, expires));
    appendIframe(getIframeSrc('dynad_rt', 'dynad_rt_exp', client, expires));
}

setTimeout(function () {
    addCookies('2015061803.2', 60);
}, 50);

function TypeValidator(){var $public=this;var $private={};$public.isDefined=function(value){return value!==undefined&&value!==null;};$public.isString=function(value){return value!==undefined&&(typeof value)==="string"&&($private.stringIsNotEmpty(value));};$private.stringIsNotEmpty=function(string){if($private.trimString(string).length>0){return true;}return false;};$private.trimString=function(string){return string.replace(/^(\s+)|(\s+)$/gm,"").replace(/\s+/gm," ");};$public.isArray=function(value){return value&&value.constructor===Array;};$public.isObject=function(entity){return entity&&entity.constructor===Object;};$public.isFunction=function(value){return value!==undefined&&value.constructor===Function;};$public.isNumber=function(value){return Number(value)===value;};$public.isInt=function(value){return $public.isNumber(value)&&value%1===0;};$public.isRegExp=function(value){return value!==undefined&&value.constructor===RegExp;};$public.isNumericString=function(value){return $public.isString(value)&&!isNaN(value);};$public.isBoolean=function(value){return value!==undefined&&value.constructor==Boolean;};return $public;}function QueryString(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.getValue=function(name){if(!$private.queryStrings){return;}return $private.queryStrings[name];};$public.getQueryStrings=function(){return $private.queryStrings;};$public.setValues=function(){if(!$private.typeValidator.isString($public.getSearch())){return;}var substrings=$public.getSearch().substring(1).split("&");if(!$private.typeValidator.isArray(substrings)){return;}if(substrings.length===0){return;}for(var i=0,length=substrings.length;i<length;i++){if($private.typeValidator.isString(substrings[i])){$private.addQueryString(substrings[i]);}}};$public.getSearch=function(){return window.location.search;};$private.addQueryString=function(substring){var queryObject=$private.getQueryObject(substring);if(!queryObject){return;}$private.queryStrings=$private.queryStrings||{};$private.queryStrings[queryObject.key]=queryObject.value;};$private.getQueryObject=function(substring){var query=substring.split("=");if(!$private.typeValidator.isString(query[0])){return;}var result={"key":query[0],"value":"true"};if($private.typeValidator.isString(query[1])){result.value=query[1];}return result;};$public.setValues();}function Logs(){var $private={};var $public=this;$private.queryString=new QueryString();$private.typeValidator=new TypeValidator();$private.history={"warn":[],"error":[],"info":[],"debug":[],"log":[]};$public.getPrefix=function(prefix){return $private.prefix;};$public.setPrefix=function(prefix){if($private.typeValidator.isString(prefix)){$private.prefix="["+prefix+"] ";}};$public.warn=function(msg){return $private.print(msg,"warn");};$public.error=function(msg){return $private.print(msg,"error");};$public.info=function(msg){return $private.print(msg,"info");};$public.debug=function(msg){return $private.print(msg,"debug");};$public.log=function(msg){return $private.print(msg,"log");};$private.print=function(msg,fn){if(!$private.prefix){return;}if(!$private.typeValidator.isString(msg)){return;}msg=$private.prefix+msg;$public.setHistory(fn,msg);if($public.isActive()===false||!$private.hasConsole()){return;}return $private.runLogMethod(fn,msg);};$public.isActive=function(){if($private.queryString.getValue("tm")==="debug"){return true;}return false;};$public.getHistory=function(methodName){if($private.typeValidator.isArray($private.history[methodName])){return $private.history[methodName];}return;};$public.setHistory=function(fn,msg){if($private.typeValidator.isString(msg)&&$private.typeValidator.isArray($private.history[fn])){$private.history[fn].push(msg);}};$private.hasConsole=function(){if(!$private.typeValidator.isDefined($public.getConsole())){return false;}if(!$private.typeValidator.isDefined($public.getConsoleLog())){return false;}return true;};$public.getConsole=function(){return window.console;};$public.getConsoleLog=function(){return $public.getConsole().log;};$private.runLogMethod=function(fn,msg){if($private.typeValidator.isDefined($public.getConsole()[fn])){$public.getConsole()[fn](msg);return fn;}window.console.log(msg);return"log";};}function NameSpace(packageName){var $private={};var $public=this;$private.version="1.0.122";$private.validator=new TypeValidator();$public.init=function(packageName){if($private.validator.isString(packageName)){return $public.create(packageName);}};$public.create=function(packageName){$private.createUOLPD();$private.createTagManager();return $private.createPackage(packageName);};$private.createUOLPD=function(){if(!$private.validator.isObject(window.UOLPD)){window.UOLPD={};}};$private.createTagManager=function(){if(!$private.validator.isObject(UOLPD.TagManager)&&!$private.validator.isFunction(UOLPD.TagManager)&&typeof UOLPD.TagManager!=="object"){UOLPD.TagManager={};}};$private.createPackage=function(packageName){if(!$private.validator.isString(packageName)){return UOLPD.TagManager;}if(!$private.validator.isObject(UOLPD.TagManager[packageName])){UOLPD.TagManager[packageName]={};}UOLPD.TagManager[packageName].version=$private.version;if(!$private.validator.isArray(UOLPD.TagManager[packageName].config)){UOLPD.TagManager[packageName].config=[];}if(!$private.validator.isObject(UOLPD.TagManager[packageName].log)){UOLPD.TagManager[packageName].log=new Logs();UOLPD.TagManager[packageName].log.setPrefix("UOLPD.TagManager."+packageName);}return UOLPD.TagManager[packageName];};return $public.init(packageName);}function ScriptUtils(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$private.dataCalls="data-uoltm-calls";$public.hasTagScript=function(src){if(!$private.typeValidator.isString(src)){return false;}if(src.length===0){return false;}if($public.findScript($private.removeProtocol(src))){return true;}return false;};$private.removeProtocol=function(value){return value.replace(/(^\/\/|^http:\/\/|^https:\/\/)/,"");};$public.findScript=function(src){var scripts=document.getElementsByTagName("script");for(var i=0,length=scripts.length;i<length;i++){var scriptSrc=scripts[i].getAttribute("src");if(scriptSrc&&$private.isSrcEqual(scriptSrc,src)){return scripts[i];}}return;};$private.isSrcEqual=function(src1,src2){return(src1.indexOf(src2)>-1);};$public.createScript=function(src){if(!$private.typeValidator.isString(src)){return;}var tag=document.createElement("script");tag.setAttribute("src",src);tag.async=true;return tag;};$public.appendTag=function(script){if(!$private.typeValidator.isDefined(script)){return;}if(script.constructor===HTMLScriptElement){$private.lastScriptsParent().appendChild(script);return true;}};$private.lastScriptsParent=function(){return document.getElementsByTagName("script")[0].parentNode;};}function StringUtils(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.trim=function(value){if(!$private.typeValidator.isString(value)){return;}if(value.length===0){return;}value=value.replace(/^(\s+)|(\s+)$/gm,"").replace(/\s+/gm," ");return value;};$public.getValueFromKeyInString=function(str,name,separator){if(!$private.typeValidator.isString(name)||name.length===0){return;}if(!$private.typeValidator.isString(str)||str.length===0){return;}if(!$private.typeValidator.isString(separator)||separator.length===0){return;}if(str.substring(str.length-1)){str+=separator;}name+="=";var startIndex=str.indexOf(name);if(startIndex===-1){return"";}var middleIndex=str.indexOf("=",startIndex)+1;var endIndex=str.indexOf(separator,middleIndex);if(endIndex===-1){endIndex=str.length;}return unescape(str.substring(middleIndex,endIndex));};return $public;}function RemoteStorage(){var $private={};var $public=this;$public.logs=new Logs();$public.logs.setPrefix("UOLPD.TagManager");$private.validator=new TypeValidator();$private.path="https://tm.uol.com.br/mercurio.html";$private.iframe=undefined;$private.iframeLoaded=false;$private.queue=[];$private.requests={};$private.id=0;$public.init=function(){$private.hasLoaded(function(){if(!$private.isIframeInPage()){$private.setIframe();}$private.setListener($private.iframe);return true;});return $public;};$private.isIframeInPage=function(){var iframes=document.getElementsByTagName("iframe");for(var i=0;i<iframes.length;i++){if(iframes[i].getAttribute("src")==$private.path){$private.iframe=iframes[i];return true;}}return false;};$public.get=function(key,callback){$private.communicator("get",key,undefined,callback);};$public.set=function(key,value,callback){$private.communicator("set",key,value,callback);};$public.removeItem=function(key,callback){callback=callback||function(){};$private.communicator("delete",key,undefined,callback);};$private.hasLoaded=function(fn,timeout){timeout=(timeout?timeout+10:10);if(typeof fn!="function"){return false;}if(document.body!==null){fn();return true;}setTimeout(function(){$private.hasLoaded(fn,timeout);},timeout);};$private.setIframe=function(){$private.iframe=$private.buildIframe();};$private.buildIframe=function(){var iframe=document.createElement("iframe");iframe.setAttribute("src",$private.path);iframe.setAttribute("id","tm-remote-storage");iframe.setAttribute("style","position:absolute;width:1px;height:1px;left:-9999px;display:none;");document.body.appendChild(iframe);return iframe;};$private.setListener=function(iframe){var hasEventListener=(window.addEventListener!==undefined);var method=(hasEventListener)?"addEventListener":"attachEvent";var message=(hasEventListener)?"message":"onmessage";var load=(hasEventListener)?"load":"onload";iframe[method](load,$private.iframeLoadHandler);window[method](message,$private.messageHandler);return iframe;};$private.iframeLoadHandler=function(event){$private.iframeLoaded=true;if(!$private.queue.length){return;}for(var i=0,length=$private.queue.length;i<length;i++){$private.sendMessageToIframe($private.queue[i]);}$private.queue=[];};$private.messageHandler=function(event){try{var data=JSON.parse(event.data);$private.requests[data.id].callback(data.key,data.value);delete $private.requests[data.id];}catch(err){}};$private.communicator=function(method,key,value,callback){if(!$private.validator.isString(key)){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}if(method==="set"&&value===undefined){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}if(!$private.validator.isFunction(callback)){$public.logs.warn("Ocorreu um erro ao requisitar os dados");return;}var request={id:++$private.id,key:key,method:method};if(value){request.value=value;}var data={request:request,callback:callback};if($private.iframeLoaded){$private.sendMessageToIframe(data);}else{$private.queue.push(data);}if(!$private.iframe){$public.init();}};$private.sendMessageToIframe=function(data){$private.requests[data.request.id]=data;data=JSON.stringify(data.request);if(!data){return;}$private.iframe.contentWindow.postMessage(data,$private.path);};return $public.init();}function TrackManager(){var $private={};var $public=this;$private.typeValidator=new TypeValidator();$public.imagesSrc=[];$private.baseUrl="//logger.rm.uol.com.br/v1/?prd=98&grp=Origem:";$private.raffledRate=Math.round(Math.random()*100);$private.samplingRate=1;$public.trackSuccess=function(msr){$public.sendPixel($private.getPixelSrc($private.getMeasure(msr)),$private.samplingRate);};$private.getMeasure=function(msr){if(!$private.typeValidator.isString(msr)){return;}return"&msr="+encodeURIComponent(msr)+":1";};$private.getPixelSrc=function(src){if(!$private.typeValidator.isString(src)){return;}if(!$public.getModuleName()){return;}if(!$public.getRepoId()){return;}return $private.baseUrl+$public.getModuleName()+$public.getRepoId()+src;};$public.getModuleName=function(moduleName){return $private.moduleName;};$public.setModuleName=function(moduleName){if($private.typeValidator.isString(moduleName)){$private.moduleName="TM-"+moduleName+";";}};$public.getRepoId=function(){if(!$private.repoId){$public.setRepoId();}return $private.repoId;};$public.setRepoId=function(){var src=$private.getScriptSrc();if(!src){return;}var srcMatch=src.match(/uoltm.js\?id=(.{6}).*/);var repoId=srcMatch?srcMatch[1]:null;if(repoId){$private.repoId="tm_repo_id:"+repoId;}};$private.getScriptSrc=function(){var script=document.querySelector('script[src*="tm.jsuol.com.br/uoltm.js"]');var src=script?script.src:null;return src;};$public.sendPixel=function(src,samplingRate){if(!$private.typeValidator.isString(src)){return;}if(!$private.isTrackEnabled(samplingRate)){return;}var img=document.createElement("img");img.setAttribute("src",src);$public.imagesSrc.push(img.src);};$private.isTrackEnabled=function(samplingRate){if(window.location.protocol.match("https")){return false;}if(window.localStorage.getItem("trackManager")=="true"){return true;}if($public.getRaffledRate()<=samplingRate){return true;}return false;};$public.getRaffledRate=function(){return $private.raffledRate;};$public.trackError=function(errorType,errorEffect){errorType=$private.getErrorType(errorType);if(!errorType){return;}errorEffect=$private.getErrorEffect(errorEffect);if(!errorEffect){return;}var url=$private.getPixelSrc(errorType+errorEffect+$private.getMeasure("Erros"));$public.sendPixel(url,100);};$private.getErrorType=function(errorType){return $private.generateKeyValue("erro_tipo",errorType);};$private.generateKeyValue=function(key,value){if(!$private.typeValidator.isString(key)){return;}if(!$private.typeValidator.isString(value)){return;}return";"+key+":"+value;};$private.getErrorEffect=function(errorEffect){return $private.generateKeyValue("erro_efeito",errorEffect);};$public.trackCustom=function(measure,trackType,trackValue){var url;var src=$private.generateKeyValue(trackType,trackValue);if(!measure||!src){return;}measure=$private.getMeasure(measure);if(measure){url=$private.getPixelSrc(src+measure);$public.sendPixel(url,$private.samplingRate);}};}

var firstList = '2015061803';
var secondList = '2015061803.2';
var trackedItems = [];

remoteStorage = new RemoteStorage();
remoteStorage.get('dynad_rt', function(key, value) {
    try {
        trackedItems = JSON.parse(value);
        trackItem(secondList);
        UOLPD.TagManager.DynadTrack.setCookie(0);
        removeItem(firstList);
        remoteStorage.set('dynad_rt', JSON.stringify(trackedItems), function(key, value) {});
    } catch (e) {}
});

function trackItem (id) {
    var index = getTrackingIndex(id);
    var tracking = {
        "id": id,
        "expiration": new Date()
    };

    if (index < 0) {
        trackedItems.push(tracking);
    } else {
        trackedItems[index] = tracking;
    }
}

function getTrackingIndex (id) {
    var index = -1;
    if (!trackedItems) {
        trackedItems = [];
        return -1;
    }
    for (var i = 0; i < trackedItems.length; i++) {
        if (trackedItems[i].id === id) {
            return i;
        }
    }
    return index;
}

function removeItem (removeId) {
    var index = getTrackingIndex(removeId);
    if (index > -1) {
        trackedItems.splice(index, 1);
    }
}

function isDateExpired (expiration) {
    var date = new Date();
    expiration = new Date(expiration);
    return date > expiration;
}

window.localStorage.setItem('dynad_rt', '2015061803.2');