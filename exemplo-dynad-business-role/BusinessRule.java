RedisDS ds = null;
Jedis jedis = null;

try {  
  ds = datasources[1]; // pega o primeiro datasource (default)
  jedis = ds.getResource(); // abre conexao no redis

  //recupera o id do usuario
  String[] fieldsToFind = {"nome", "link", "imagem", "preco", "recomendacoes"};
  String[] outputFields = {"nome", "link", "imagem", "preco"};
  String[][] _allRows = new String[20][outputFields.length];
  String id_usuario = request.getParameter("p");
  String id_oferta = Helper.getDBRow(jedis, id_usuario);  
  String[] recomendacoes = null;
  int idx = 0;
  boolean jaImprimiu = false;
  
  response.getWriter().write(id_oferta);

  if (!id_oferta.equals("")) {
    String row  = Helper.getDBRow(jedis, "AV1-"+id_oferta);      
    String campos[] = Helper.getDBFields(ds, row, fieldsToFind);
    recomendacoes = campos[4].split(",");
    _allRows[idx][0] = campos[0];
    _allRows[idx][1] = campos[1];
    _allRows[idx][2] = campos[2];
    _allRows[idx][3] = campos[3];  
    jaImprimiu = true;      
    idx++;
  }

	// busca produtos no historico
  for(String recId : recomendacoes) {
    response.getWriter().write("RECO" + recId);  
    if( recId == null || recId.trim().equals("") ) continue;
    String row  = Helper.getDBRow(jedis, recId);
    String campos[] = Helper.getDBFields(ds, row, fieldsToFind);          
    _allRows[idx][0] = campos[0];
    _allRows[idx][1] = campos[1];
    _allRows[idx][2] = campos[2];
    _allRows[idx][3] = campos[3];    
    jaImprimiu = true;
    idx++;      
  }   	
      
    
  String [][]allRows = new String[2][outputFields.length]; // estrutura final
  int zz=0;
  for(int z=0;z<(idx<2?idx:2);z++) {
    if(_allRows[z]!=null) allRows[zz++] = _allRows[z];  // limpa elementos que nao foram preenchido na matriz inicial
  }
    
  if(jaImprimiu) {
    response.getWriter().write(Helper.toFlashvars(allRows, outputFields));    
    return;
  }
}catch(Exception ex) { 
  response.getWriter().write(ex.getMessage());
  ex.printStackTrace();
}finally{ 
  ds.release(jedis); // libera a conexao
}
response.getWriter().write("");