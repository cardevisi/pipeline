RedisDS ds = null;
Jedis jedis = null;

try {
  
    //ds = datasources[35]; // pega o primeiro datasource (default)
    //jedis = ds.getResource(); // abre conexao no redis

    ds = RedisHelper.find("banco_de_test"); // pega o datasource correspondente
    jedis = ds.getResource(); // abre conexao no redis

    //recupera o id do cliente
    String[] fieldsToFind = {"titulo", "preco", "descricao", "destino"};
    String[] outputFields = {"titulo", "preco", "descricao", "destino"};
    String[] recomendacoes = null;
    String[][] _allRows = new String[20][outputFields.length];
    
    String idCliente = request.getParameter("p");
    String idOferta = Helper.getDBRow(jedis, idCliente);
    int idx = 0;
    
    boolean jaImprimiu = false;
    
    if (!idOferta.equals("")) {
        String row  = Helper.getDBRow(jedis, idOferta);
        String campos[] = Helper.getDBFields(ds, row, fieldsToFind);
        //recomendacoes = campos[4].split(",");
        _allRows[idx][0] = campos[0];
        _allRows[idx][1] = campos[1];
        _allRows[idx][2] = campos[2];
        _allRows[idx][3] = campos[3];  
        jaImprimiu = true;
        idx++;
    }

    String [][]allRows = new String[2][outputFields.length]; // estrutura final
    int zz=0;
    for(int z=0;z<(idx<2?idx:2);z++) {
        if(_allRows[z]!=null) allRows[zz++] = _allRows[z];  // limpa elementos que nao foram preenchido na matriz inicial
    }
    
    if(jaImprimiu) {
        response.getWriter().write(Helper.toFlashvars(allRows, outputFields));    
        return;
    }

} catch(Exception ex) {
    response.getWriter().write(ex.getMessage());
    ex.printStackTrace();
} finally{ 
    ds.release(jedis); // libera a conexao
}

response.getWriter().write("");