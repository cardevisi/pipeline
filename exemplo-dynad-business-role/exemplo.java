RedisDS ds = null;
Jedis jedis = null;

try {
  ds = datasources[0]; // datasource default
  jedis = ds.getResource(); // abre conexao no redis

  // variavel oob para Outlook
  String oob = request.getParameter("oob");
  if (oob == null) oob = "";
  oob = Helper.urlEncode(oob);

  // captura lista de SKUs via params
  String _skusViaParam = request.getParameter("p");
  String[] skusViaParam = _skusViaParam != null ? _skusViaParam.split(",") : new String[0];
  // fim da captura de skus

  // monta lista final de SKUs e recomendacoes
  if (skusViaParam.length > 0) {
    String[] recomendacoes = null;
    String[] fieldsToFind = null;

    fieldsToFind = new String[] {
      "ativo", "nome", "link", "preco_original", "preco_promocional", "imagem", "valor_parcelas", "numero_parcelas", "recomendacoes"
    }; // define os atributos buscados no banco

    String fieldRecomendacao[] = new String[] {
      "recomendacoes"
    };

    String[] outputFields = {
      "nome", "link", "preco_original", "preco_promocional", "imagem", "valor_parcelas", "numero_parcelas", "OOBClickTrack"
    }; // define os atributos a enviar ao flash

    String[][] _allRows = new String[20][outputFields.length]; // define o tamanho da matriz de produtos e atributos
    Boolean jaImprimiu = false;
    boolean isSecure = request.isSecure() || request.getHeader("X-Forwarded-Proto") != null;
    String sku = skusViaParam[0];
    String row = Helper.getDBRow(jedis, sku); // busca o registro no banco
    int idx = 0;

    if (row != null) { // se achou elemento
      String fields[] = Helper.getDBFields(ds, row, fieldsToFind); // extrai os atributos solicitados 
      String fieldRec[] = Helper.getDBFields(ds, row, fieldRecomendacao); // extrai os atributos solicitados
      recomendacoes = fieldRec != null && fieldRec.length == 1 ? fieldRec[0].split(",") : new String[0];
      if (fields != null && fields[0].equals("1")) { // se estiver ativo        
        _allRows[idx][0] = Helper.htmlEntities(fields[1]); // nome
        _allRows[idx][1] = Helper.urlEncode(fields[2]); // link
        _allRows[idx][2] = Helper.htmlEntities(fields[3]); // preco_original
        _allRows[idx][3] = Helper.htmlEntities(fields[4]); // preco_promocional
        _allRows[idx][4] = Helper.urlEncode(((isSecure) ? fields[5].replace("http:", "https:") : fields[5])); // imagem
        _allRows[idx][5] = Helper.htmlEntities(fields[6]); // valor das parcelas
        _allRows[idx][6] = Helper.htmlEntities(fields[7]); // numero parcelas
        _allRows[idx][7] = oob; // outlook clicktrack
        idx++;
        jaImprimiu = true;
      }
    }

    // busca produtos no historico
    for (String histSKU: skusViaParam) {
      if (idx == _allRows.length) break;
      if (!histSKU.equals(sku)) {
        row = Helper.getDBRow(jedis, histSKU); // busca o registro no banco
        if (row != null) { // se achou elemento
          String fields[] = Helper.getDBFields(ds, row, fieldsToFind); // extrai os atributos solicitados 
          if (fields != null && fields[0].equals("1")) { // se estiver ativo
            if (recomendacoes == null || recomendacoes.length == 0) {
              String fieldRec[] = Helper.getDBFields(ds, row, fieldRecomendacao);
              recomendacoes = fieldRec != null && fieldRec.length == 1 ? fieldRec[0].split(",") : new String[0];
            }            
            _allRows[idx][0] = Helper.htmlEntities(fields[1]); // nome
            _allRows[idx][1] = Helper.urlEncode(fields[2]); // link
            _allRows[idx][2] = Helper.htmlEntities(fields[3]); // preco_original
            _allRows[idx][3] = Helper.htmlEntities(fields[4]); // preco_promocional
            _allRows[idx][4] = Helper.urlEncode(((isSecure) ? fields[5].replace("http:", "https:") : fields[5])); // imagem
            _allRows[idx][5] = Helper.htmlEntities(fields[6]); // valor das parcelas
            _allRows[idx][6] = Helper.htmlEntities(fields[7]); // numero parcelas
            _allRows[idx][7] = oob; // outlook clicktrack
            idx++;
            jaImprimiu = true;
          }
        }
      }
    }
    // fim da busca no historico

    // completa com recomendações
    if (recomendacoes != null) {
      for (String recSKU: recomendacoes) {
        if (idx == _allRows.length) break;
        if (!recSKU.equals(sku)) {
          row = Helper.getDBRow(jedis, recSKU); // busca o registro no banco
          if (row != null) { // se achou elemento
            String fields[] = Helper.getDBFields(ds, row, fieldsToFind); // extrai os atributos solicitados 
            if (fields != null && fields[0].equals("1")) { // se estiver ativo              
              _allRows[idx][0] = Helper.htmlEntities(fields[1]); // nome
              _allRows[idx][1] = Helper.urlEncode(fields[2]); // link
              _allRows[idx][2] = Helper.htmlEntities(fields[3]); // preco_original
              _allRows[idx][3] = Helper.htmlEntities(fields[4]); // preco_promocional
              _allRows[idx][4] = Helper.urlEncode(((isSecure) ? fields[5].replace("http:", "https:") : fields[5])); // imagem
              _allRows[idx][5] = Helper.htmlEntities(fields[6]); // valor das parcelas
              _allRows[idx][6] = Helper.urlEncode(fields[7]); // numero parcelas
              _allRows[idx][7] = oob; // outlook clicktrack
              idx++;
              jaImprimiu = true;
            }
          }
        }
      }
      // fim do complemento com recomendações
    }

    String[][] allRows = new String[6][outputFields.length]; // estrutura final
    int zz = 0;
    for (int z = 0; z < (idx < 6 ? idx : 6); z++) {
      if (_allRows[z] != null) allRows[zz++] = _allRows[z]; // limpa elementos que nao foram preenchido na matriz inicial
    }
    if (jaImprimiu) response.getWriter().write(Helper.toFlashvars(allRows, outputFields));
    return;
  }
} catch (Exception ex) {
  ex.printStackTrace();
} finally {
  if (ds != null) ds.release(jedis); // libera a conexao
}
response.getWriter().write("");