window[placementId + '_dft_produtos'] = ''; //armazena os skus de produtos da dafiti
window[placementId + '_dft_carrinho'] = ''; //armazena os skus de carrinho da dafiti
window[placementId + '_dft_recencia_carrinho'] = -1; //armazena numero de dias do ultimo produto adicionado ao carrinho
window[placementId + '_dft_recencia_produto'] = -1; //armazena numero de dias do ultimo produto adicionado ao carrinho

/** carrega os dados do produto e dispara o callback com a flag correspondente ***/
var loadRTData = function(idrule, skus, callback) {
  loader("//sna.dynad.net/eval/" + idrule + "?p=" + skus + "&nivel_funil=produto&oob=", function(e) {
    callback((e != ""), e);
  }, function() {
    callback(false);
  });
}

/** aplica regra inicial dependendo do nivel de interacao do visitante ***/
var applyRules = function() {
    if (window[placementId + '_dft_carrinho'] != '') {
        applyCartGeral();
    } else if (window[placementId + '_dft_produtos'] != '') {
        if (window[placementId + '_dft_recencia_produto'] > -1 && window[placementId + '_dft_recencia_produto'] < 24) {
            applyProduct24Hours();
        } else if (window[placementId + '_dft_recencia_produto'] > -1 && window[placementId + '_dft_recencia_produto'] < (24 * 7) ){
            applyProductOneWeek();
        } else
            applyGeneralProduct();
    } else {
        applySample();
    }
}

var applyCartGeral = function() {
  loadRTData(1000060, window[placementId + '_dft_carrinho'], function(f, e) {
    if (f) nextStage(";bu=_audiencia_carrinho_de_compras_", e, true);
    else applyProduct24Hours();
  });
}

var applyProduct24Hours = function() {
  loadRTData(1000060, window[placementId + '_dft_produtos'], function(f, e) {
    if (f) nextStage(";bu=_audiencia_produto_1d_", e, true);
    else applySample();
  });
}

var applyProductOneWeek = function() {
  loadRTData(1000060, window[placementId + '_dft_produtos'], function(f, e) {
    if (f) nextStage(";bu=_audiencia_produto_1w_", e, true);
    else applySample();
  });
}

var applyGeneralProduct = function() {
  loadRTData(1000060, window[placementId + '_dft_produtos'], function(f, e) {
    if (f) nextStage(";bu=_audiencia_produto_geral_", e, true);
    else applySample();
  });
}

var applyTopSellers = function() {
  loadRTData(1000060, 'topseller', function(f, e) {
    if (f) nextStage(";bu=_audiencia_top_seller_geral_", e, true);
    else applyStaticContent();
  });
}

var applyStaticContent = function() {
  nextStage(false, "", false);
}

var applySample = function() {
    if (Math.random() <= 0.5)
        applyTopSellers();
    else
        applyStaticContent();
}

try {
  new DADB(function(db) {
    db.getItem("d" + customerId + "C", function(k, v) {
      //recupera dados do carrinho (dafiti)
      var v = null == v || "" == v ? {} : JSON.parse(v);
      if (v.length > 0) {
        window[placementId + '_dft_recencia_carrinho'] = Math.round((new Date().getTime() / 1000 - parseInt(v[0].t)) / 3600);
        var _arrSKUs = new Array();
        for (var x in v) if (v[x].v != '') _arrSKUs.push(v[x].v);
        window[placementId + '_dft_carrinho'] = _arrSKUs.toString();
      }

      db.getItem("d" + customerId + "p", function(k, v) {
        //recupera dados do produto (dafiti)
        var v = null == v || "" == v ? {} : JSON.parse(v);
        if (v.length > 0) {
            window[placementId + '_dft_recencia_produto'] = Math.round((new Date().getTime() / 1000 - parseInt(v[0].t)) / 3600);
            var _arrSKUs = new Array();
            for (var x in v) if (v[x].v != '') _arrSKUs.push(v[x].v);
            window[placementId + '_dft_produtos'] = _arrSKUs.toString();
        }
        applyRules();
      });
    });
  });

} catch (exDados) {}