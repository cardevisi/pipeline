CMP_UOL_DIARIA_P3_300x250_TOPSELLERS

Placements Media Math:

160x600
250x250
300x250
300x600
468x60
728x90

Verificar esta informação com o Altamiro.

Backup Image: link default
Backup Landing Page: link default

MediaMath_UNICA_DCO-300x250_CPA
MediaMath_CPM_RMKT_Home_DCO-300x250
MediaMath_UNICA_DCO-300x600_CPA
MediaMath_UNICA_DCO-300x600_CPA


MediaMath_UNICA_DCO-300x250	MEDIAMATH	300x250	597	2	0.34%	0	BRL0.00
MediaMath_UNICA_DCO-728x90	MEDIAMATH	728x90	498	1	0.2%	0	BRL0.00
MediaMath_UNICA_DCO-300x600	MEDIAMATH	300x600	237	0	0%	0	BRL0.00
MediaMath_UNICA_DCO-160x600	MEDIAMATH	160x600	92	0	0%	0	BRL0.00
MediaMath_UNICA_DCO-468x60	MEDIAMATH	468x60	5	0	0%	0	BRL0.00
MediaMath_UNICA_DCO-250x250	MEDIAMATH	250x250	1	0	0%	0	BRL0.00


Could not find matching constructor for: java.lang.RuntimeException(java.lang.String, java.lang.String)

New – 30 dias
(prioridade 20) A descrição do target está no corpo do e-mail.
//return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=new30dias;")>-1;

New – 60 dias
(prioridade 22) A descrição do target está no corpo do e-mail.
//return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=new60dias;")>-1;

New – 90 dias ou mais
(prioridade 25) A descrição do target está no corpo do e-mail.
//return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=new90diasOuMais;")>-1;

Recurring – RF 1
(prioridade 30) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=recurringRF1;")>-1;

Recurring – RF 2
(prioridade 32) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=recurringRF2;")>-1;

Recurring – RF 3
(prioridade 35) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=recurringRF3;")>-1;


Reactivated
(prioridade 40) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=reactivated;")>-1;

Inactive – 90 dias
(prioridade 50) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=inactive90dias;")>-1;

Inactive – 120 dias
(prioridade 52) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=inactive120dias;")>-1;

Inactive – 150 dias
(prioridade 55) A descrição do target está no corpo do e-mail.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=inactive150dias;")>-1;

Basket – 24hrs
(prioridade 5) Audiência com produto abandonado no carrinho nas últimas 24hrs.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=basket24dias;")>-1;

Basket – 3 dias
(prioridade 7) Audiência com produto abandonado no carrinho nos últimos 3 dias.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=basket3dias;")>-1;

Basket – acima de 3 dias
(prioridade 9) Audiência com produto abandonado no carrinho a mais de 3 dias.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=basketAcima3dias;")>-1;

Product  – 24hrs
(prioridade 11) Audiência que visualizou um produto nas últimas 24hrs.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=produtct24hrs;")>-1;

Product – 3 dias
(prioridade 13) Audiência que visualizou um produto nos últimos 3 dias.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=produtct3dias;")>-1;

Product – acima de 3 dias
(prioridade 15) Audiência que visualizou um produto a mais 3 dias.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=produtctAcima3dias;")>-1;


Category / Search
(prioridade 17) Audiência que visualizou uma categoria ou realizou um pesquisa no portal Empório.
//return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=categorySearch;")>-1;

Top Seller
(prioridade 60) Campanha que exibirá os top sellers devolvidos pelo Machine Learning, deve haver um filtro topseller que será disparado pela BR.
return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=topSeller;")>-1;

General
(prioridade 80) Campanha a ser veiculado caso o visitante esteja fora de todos os cenários descritos acima.
//return request.getParameter("dc")!=null&&request.getParameter("dc").indexOf(";fsd=general;")>-1;


New: Todos os usuários que fizeram a primeira e única compra nos últimos 3 meses;
	Aqui seria legal a gente quebrar em:
    - New últimos 30 dias: Usuários que fizeram a primeira e única compra nos últimos 30 dias;
    - New últimos 60 dias: Usuários que fizeram a primeira e única compra nos últimos 60 dias;
    - New últimos 90 dias: Usuários que fizeram a primeira e única compra nos últimos 90 dias;

Ainda não fazemos esta quebra no CRM, mas dependendo da estratégia de mídia podemos quebrar;
 
Recurring: Todos os usuários que fizeram mais de 1 compra nos últimos 3 meses, sendo que já eram compradores;
    - Recurring frequencia 1: Usuários que fizeram somente 1 compra nos últimos 3 meses
    - Recurring frequencia 2: Usuários que fizeram 2 compras nos últimos 3 meses;
    - Recurring frequencia 3: Usuários com 3 ou mais compras nos últimos 3 meses; 

Também não temos esta segunda quebra, mas de acordo com o plano conseguimos puxar;
 
Reactivated: Todos os usuários que fizeram somente 1 compra nos últimos 3 meses, mas estavam há mais de 3 meses sem realizar compras (ou seja, já foram inativos);
Para a mídia, este cluster pode entrar junto com o Recurring frequencia 1, pois a ideia é somente aumentar a frequencia de compra.
 

Inactive: Usuários que estão há 3 meses (90 dias) sem realizar nenhuma compra. Precisamos de estratégias para reativar esta base. Podemos ter a quebra por:
- Usuários Inativos há 90 dias (acabaram de entrar no Cluster)
- Usuários Inativos há 120 dias (aqui podemos ter políticas mais agressivas de reativação, tipo descontos)
- Usuários Inativos há 150 dias: Quando começarmos a rodar, precisamos entender em que momento consideramos um inativo perdido. A mídia pode nos ajudar a entender até qual recência de inatividade temos resultados.





https://t5.dynad.net/script/?dc=130269;ord=1454679146568;st=2;eci=;ecr=;ci=195189;cr=4000006837.0;rt=true;ts=1454679146568;p=strategy%3D%5BAD_ATTR.strategy%5D;fsd=tru




