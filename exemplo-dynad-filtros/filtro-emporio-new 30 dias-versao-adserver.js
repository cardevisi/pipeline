var oob = "";

        try {
          var ss = document.getElementsByTagName('script');
          for (var x = 0; x < ss.length; x++) {
            if (typeof ss[x].src !== "undefined" && ss[x].src.indexOf('dynad.net') > -1 && ss[x].src.indexOf('dc='+placemnetId+';') > -1 && typeof ss[x].getAttribute('OOBClickTrack') !== "undefined") {
              oob = ss[x].getAttribute('OOBClickTrack');
              break;
            }
          }
        } catch (e) {}

        try {
          if( typeof tagParameters !== "undefined" && typeof tagParameters['strategy'] !== "undefined" ) {
          setCPam('strategy=' + tagParameters['strategy'], false);
            if( tagParameters['strategy'] != '' ) { 
                /*try {
                  setTimeout(function () {
                    window['callbackiruol'] = function(d){}; 
                  var e = document.createElement('script'); e.setAttribute('type', 'text/javascript'); e.setAttribute('async', 'true');
                e.setAttribute('src', '//metrics.dynad.net/resources/counter/jsonp/incWithFk/emporiocerveja/display_mm/strategy/'+encodeURIComponent(tagParameters['strategy'])+'/?callback=callbackiruol&ord=' + Date.now() );
                (document.body || document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild(e);
                  }, 30);
            } catch(exdyn) {}*/
            }
          }
        } catch (exMM) {}

        var customerCart     = 'd' + customerId + 'C';
        var customerCategory = 'd' + customerId + 'c';
        var customerSold     = 'd' + customerId + 's';
        var customerProduct  = 'd' + customerId + 'p';
        
        var last30Days  = new Date(getLastDate(getCurrentDate(), 30)).getTime();
        var last60Days  = new Date(getLastDate(getCurrentDate(), 60)).getTime();
        var last90Days  = new Date(getLastDate(getCurrentDate(), 90)).getTime();
        var last120Days = new Date(getLastDate(getCurrentDate(), 120)).getTime();
        var last150Days = new Date(getLastDate(getCurrentDate(), 150)).getTime();
        var last24Hrs   = new Date(getLastDate(getCurrentDate(), 1)).getTime();
        var last3Days   = new Date(getLastDate(getCurrentDate(), 3)).getTime();

        function loadRTData(skus, callback) {
          loader("//sna.dynad.net/eval/"+ businessId +"?p=" + skus + "&oob=" + encodeURIComponent(oob), function(e) { 
            callback((e != ""), e);
          }, function() {
            callback(false);
          });
        }

        function getCurrentDate() {
            var today = new Date();
            var year = today.getFullYear();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            return new Date (month + "/" + day + "/" + year).getTime();
        }

        function getLastDate(date, diference) {
            var lastDate = new Date(date);
            var dayOfMonth = lastDate.getDate();
            diference = (diference) ? diference : 0;
            return new Date(lastDate.setDate(dayOfMonth - diference)).getTime();
        }

        function formatLocalStorageDate(timestamp) {
            var fullDate = Number (timestamp + '000');
            var changeHour = new Date(fullDate).setHours(0,0,0,0);
            return new Date(changeHour).getTime();
        }

        function validateSkus(json) {
            if(!(json || json.constructor !== Array || json.length == 0)) {
                return false;
            }
            if(!json[0] || !json[0].t) {
                return false;
            }
            return true;
        }

        function applyFilterInactive(db, callback){
          db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }                
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var skuList = [];  

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }  

                if(localStorageTime <= last90Days && localStorageTime > last120Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive90dias', f, e);
                  });
                  return;
                } 

                if(localStorageTime <= last120Days && localStorageTime > last150Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive120dias', f, e);
                  });
                  return;
                } 

                if(localStorageTime <= last150Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive150dias', f, e);
                  });
                  return;
                } 
                
                callback(false);
                return;
            
            });
        }

        function applyFilterReactivated(db, callback) {
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }                
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                if(skus.length < 2) {
                  callback(false);
                  return;
                }
                
                var monthsWithoutBuying = formatLocalStorageDate(skus[1].t);
                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if(monthsWithoutBuying > last90Days) {
                    callback(false);
                    return;
                }

                var lastBuy = formatLocalStorageDate(skus[0].t);

                if (lastBuy >= last90Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('reactivated', f, e);
                  });
                  return; 
                }

                callback(false);
                return;
            });
        }

        function applyFilterBasket(db, callback) {
            db.getItem (customerCart, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }       

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var forgetCart = formatLocalStorageDate(skus[0].t);
                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if(forgetCart >= last24Hrs) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basket24dias', f, e);
                  });
                  return;
                }

                if(forgetCart < last24Hrs && forgetCart >= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basket3dias', f, e);
                  });
                  return; 
                }

                if(forgetCart < last24Hrs && forgetCart <= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basketAcima3dias', f, e);
                  });
                  return; 
                }
                
                callback(false);
                return; 
            });
        }

        function applyFilterProduct(db, callback) {
            db.getItem (customerProduct, function(key, value) {
                
                if(value === null) {
                    callback(false);
                    return;
                }       

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var viewProduct = formatLocalStorageDate(skus[0].t);
                var skuList = [];

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }  
                
                if (viewProduct >= last24Hrs) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('product24hrs', f, e);
                  });
                  return;
                }
                 
                if(viewProduct < last24Hrs && viewProduct >= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('product3dias', f, e);
                  });
                  return; 
                }

                if(viewProduct < last24Hrs && viewProduct <= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('productAcima3dias', f, e);
                  });
                  return; 
                }
                
                callback(false);
                return; 
            });
        }

        function applyFilterCategorySearch(db, callback) {
          db.getItem (customerCategory, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if (skus.length >= 1) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("categorySearch", f, e);
                  });
                  return;
                }

                callback(false);
                return;
          });
        }

        function applyFilterRecurring(db, callback){
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var skuList = []; 

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }
                
                if(localStorageTime >= last90Days && skus.length == 1) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF1", f, e);
                  });
                  return;
                } 

                if(localStorageTime >= last90Days && skus.length == 2) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF2", f, e);
                  });
                  return;
                } 

                if(localStorageTime >= last90Days && skus.length == 3) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF3", f, e);
                  });
                  return;
                } 
                
                callback(false);
                return;
            });
        }

        function applyFilterNewDay(db, callback) {
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                if(skus.length > 1) {
                    callback(false);
                    return;
                }
                
                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var skuList = []; 

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }
                
                if (localStorageTime >= last30Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new30dias', f, e);
                  });
                    return;
                } 

                if (localStorageTime >= last60Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new60dias', f, e);
                  });
                  return;
                } 

                if (localStorageTime >= last90Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new90diasOuMais', f, e);
                  });
                  return;
                }
                
                callback(false);
                return;
            });
        }

        function applyFilterTopSeller() {
          loadRTData('topSeller', function(f, e) {
            if (f === true) {
              nextStage('topSeller', e, true);
            } else {
              applyGeneralFilter();
            }
          });
        }

        function applyGeneralFilter() {
          nextStage(false);
        }

        var applySample = function() {
          if (Math.random() <= 0.5) {
            applyFilterTopSeller();
          } else {
            applyGeneralFilter();
          }
        }

        new DADB(function(db) {

          function applyNextRules(index) {
            switch(queue[index]) {
              case "basket":
                applyFilterBasket(db, callbackFilter);
              break;
              case "product":
                applyFilterProduct(db, callbackFilter);
              break;
              case "categorySearch":
                applyFilterCategorySearch(db, callbackFilter);
              break;
              case "newDay":
                applyFilterNewDay(db, callbackFilter);
              break;
              case "recurring":
                applyFilterRecurring(db, callbackFilter);
              break;
              case "reactivated":
                applyFilterReactivated(db, callbackFilter);
              break;
              case "inactive":
                applyFilterInactive(db, callbackFilter);
              break;
              case "topseller":
                applySample();
              break;
            }
          }

          var queue = ['basket', 'product', 'categorySearch', 'newDay', 'recurring', 'reactivated', 'inactive', 'topseller'];
          var index = 0;
          function callbackFilter(filterName, f, e){
            if(f === true) {
              nextStage(filterName, e, true);
              return;
            } else {
              index++;
              applyNextRules(index);
            }
          }
          
          applyNextRules(index);
        });