(function() {
  window._dt_dynad_130269_1454681450863 = '';
  window._tp_dynad_130269_1454681450863 = {
    'strategy': '[AD_ATTR.strategy]',
    'campaign': '[AD_ATTR.campaign]'
  };
  window._dynad_tag_protocol_ = 'https://';

  function loader(e, t, n) {
    if (0 == e.indexOf("//")) {
      e = window.location.protocol + e;
    };
    try {
      e += (e.indexOf("?") > -1 ? "&" : "?") + "cachebuster=" + new Date().getTime();
      var r = null;
      if (function() {
          try {
            return parseInt(navigator.userAgent.toLowerCase().split("msie")[1])
          } catch (e) {
            return 999
          }
        }() < 10) {
        r = new XDomainRequest;
        r.onload = function() {
          t(r.responseText)
        };
        r.onerror = function() {
          n(r.responseText)
        }
      } else {
        if (typeof XMLHttpRequest !== "undefined") r = new XMLHttpRequest;
        else {
          var i = ["MSXML2.XmlHttp.5.0", "MSXML2.XmlHttp.4.0", "MSXML2.XmlHttp.3.0", "MSXML2.XmlHttp.2.0", "Microsoft.XmlHttp"];
          for (var s = 0, o = i.length; s < o; s++) {
            try {
              r = new ActiveXObject(i[s]);
              break
            } catch (u) {
              r = null
            }
          }
        }
        if (r == null) {
          n(u);
          return false
        }
        r.onreadystatechange = function() {
          if (r.readyState === 4) {
            if (r.status === 200) {
              t(r.responseText);
              return true
            } else {
              n({
                message: "invalid status [" + r.status + "]"
              });
              return false
            }
          }
        }
      }
      r.open("GET", e, true);
      r.send()
    } catch (u) {
      n(u);
      return false
    }
  }
  if (typeof JSON !== "object") {
    JSON = {};
    JSON.parse = JSON.parse || function(o) {
      return eval('(' + o + ')')
    };
    JSON.stringify = JSON.stringify || function(o) {
      var n;
      var a;
      var t = typeof(o);
      var json = [];
      if (t != 'object' || o === null) {
        if (t == "string") {
          o = '"' + o + '"'
        }
        return String(o)
      } else {
        a = (o && o.constructor == Array);
        for (var key in o) {
          n = o[key];
          t = typeof(n);
          if (t == "string") {
            n = '"' + n.replace(/["]/g, "\\\"") + '"'
          } else if (t == "object" && n !== null) {
            n = JSON.stringify(n)
          }
          json.push((a ? "" : '"' + key + '":') + String(n))
        }
        return (a ? "[" : "{") + String(json) + (a ? "]" : "}")
      }
    }
  }

  function DADB(t) {
    return DADB.inst instanceof DADB ? DADB.inst._rs = t(DADB.inst) : (this._if = null, this._ok = !1, this._db = (typeof window['_dynad_tag_protocol_'] !== 'undefined' ? window['_dynad_tag_protocol_'] : ("http:" != window.location.protocol ? "https://" : "http://")) + "s.dynad.net", this._pt = "/stack/KMA9C2O70iP6CHSgXk0LGaQ8ML9m6vJE4RIi1Rf61p4.html?v63", this._qu = [], this._rq = {}, this._id = (new Date).getTime() + Math.floor(1e4 * Math.random()), this._ls = this._dg() == this._db, this._rs = "", DADB.inst = this, void this.init(function() {
      "function" == typeof t && (DADB.inst._rs = t(DADB.inst))
    }))
  }
  "object" != typeof JSON && (JSON = {}, JSON.parse = JSON.parse || function(o) {
    return eval("(" + o + ")")
  }, JSON.stringify = JSON.stringify || function(t) {
    var i, e, s = typeof t,
      n = [];
    if ("object" != s || null === t) return "string" == s && (t = '"' + t + '"'), t + "";
    e = t && t.constructor == Array;
    for (var o in t) i = t[o], s = typeof i, "string" == s ? i = '"' + i.replace(/["]/g, '\\"') + '"' : "object" == s && null !== i && (i = JSON.stringify(i)), n.push((e ? "" : '"' + o + '":') + (i + ""));
    return (e ? "[" : "{") + (n + "") + (e ? "]" : "}")
  }), DADB.prototype = {
    constructor: DADB,
    init: function(t) {
      var i = this;
      this._ls || this._if || (this._if = document.createElement("iframe"), this._if.style.cssText = "position:absolute;width:1px;height:1px;left:-9999px;", (document.body == null || typeof document.body === 'undefined' ? document.getElementsByTagName('body')[0] : document.body).appendChild(this._if), window.addEventListener ? (this._if.addEventListener("load", function() {
        i._ild()
      }, !1), window.addEventListener("message", function(t) {
        i._hm(t)
      }, !1)) : this._if.attachEvent && (this._if.attachEvent("onload", function() {
        i._ild()
      }, !1), window.attachEvent("onmessage", function(t) {
        i._hm(t)
      })), this._if.src = this._db + this._pt), "function" == typeof t && t()
    },
    getResult: function() {
      return this._rs
    },
    getCk: function(t, i) {
      var e = {
        request: {
          id: ++this._id,
          type: "gc",
          key: t
        },
        callback: i
      };
      this._s(e)
    },
    setCk: function(t, i, e, s) {
      "function" == typeof e && (s = e, e = 2592e3);
      var n = {
        request: {
          id: ++this._id,
          type: "sc",
          key: t,
          value: i,
          ttl: e
        },
        callback: s
      };
      this._s(n)
    },
    setNav: function(t, i, e, s, n) {
      var o = {
        request: {
          id: ++this._id,
          type: "snav",
          key: t,
          value: i + "|" + e + "|" + s
        },
        callback: n
      };
      this._s(o)
    },
    getNav: function(t, i) {
      var e = {
        request: {
          id: ++this._id,
          type: "nav",
          key: t
        },
        callback: i
      };
      this._s(e)
    },
    getItem: function(t, i) {
      if (this._ls) return localStorage[t];
      var e = this,
        s = {
          request: {
            id: ++e._id,
            type: "get",
            key: t
          },
          callback: i
        };
      e._s(s)
    },
    setItem: function(t, i, e) {
      if (this._ls) return void(localStorage[t] = i);
      var s = this,
        n = {
          request: {
            id: ++s._id,
            type: "set",
            key: t,
            value: i
          },
          callback: e
        };
      s._s(n)
    },
    delItem: function(t) {
      if (this._ls) return delete localStorage[t];
      var i = {
        request: {
          id: ++this._id,
          type: "unset",
          key: t
        }
      };
      this._s(i)
    },
    _s: function(t) {
      this._ok ? this._if && (this._rq[t.request.id] = t, this._if.contentWindow.postMessage(JSON.stringify(t.request), this._db)) : this._qu.push(t)
    },
    _ild: function() {
      if (this._ok = !0, this._qu.length) {
        for (var t = 0, i = this._qu.length; i > t; t++) this._s(this._qu[t]);
        this._qu = []
      }
    },
    _hm: function(t) {
      if (t.origin == this._db) {
        var i = JSON.parse(t.data);
        try {
          void 0 !== this._rq[i.id].deferred && this._rq[i.id].deferred.resolve(i.value)
        } catch (t) {}
        void 0 !== this._rq[i.id] && "function" == typeof this._rq[i.id].callback && (this._rs = this._rq[i.id].callback(i.key, i.value)), delete this._rq[i.id]
      }
    },
    _dg: function() {
      var t = window.location.href,
        i = document.createElement("a");
      return i.href = t, i.hostname
    }
  };
  window._DADB_130269_1454681450863 = DADB;
  window._loader_130269_1454681450863 = loader;
  document.write('<div style="height:600px; visibility:visible; overflow:hidden; width:160px; background-color:white; display:block; margin:0; border:0; z-index:1; " id="_dynad_c_I130269_1454681450863">');
  var cpam = '',
    cpam64 = '',
    setCPam = function(c, z) {
      if (!z) cpam = ';p=' + encodeURIComponent(c);
      else cpam64 = ';p64=' + cpam64.replace(new RegExp('=', 'g'), '-').replace(new RegExp("\\+", 'g'), '_');
    },
    eci = '',
    ecr = '',
    ci = '195189',
    cr = '4000006837.0',
    fb = null,
    nextStage = function(s, v, r, k, p) {
      if (fb != null) clearTimeout(fb);
      if (typeof v !== "undefined" && v != null) {
        window._dt_dynad_130269_1454681450863 = v;
      }
      if (typeof s !== "undefined" && s != false && s != null) {
        if (typeof r !== "undefined" && r != false && r != '' && r != null) r = true;
        if (typeof k !== "undefined" && k != false && k != null && k != '195189') eci = k;
        if (typeof p !== "undefined" && p != false && p != null) ecr = p;
        var script = document.createElement('script');
        script.src = "https://t5.dynad.net/script/?dc=130269;ord=1454681450863;st=2;eci=" + eci + ";ecr=" + ecr + ";ci=" + ci + ";cr=" + cr + ";rt=" + r + ";ts=1454681450863" + cpam + cpam64 + ";fsd=" + s + ";click=[ENCODED_CLICK_REDIRECT]";
        script.async = "true";
        document.getElementById('_dynad_c_I130269_1454681450863').appendChild(script);
      } else {
        var dt_dynad = window._dt_dynad_130269_1454681450863;
        var divAppender = function(s) {
          var n = '';
          var r = s.replace(new RegExp('<script[^>]*>([\s\S]*?)<\/script>', 'gi'), function() {
            n += arguments[1] + '\n';
            return '';
          });
          document.getElementById('_dynad_c_I130269_1454681450863').innerHTML = r;
          if (window.execScript && n != '') window.execScript(n);
          else if (n != '') eval(n);
        };
        try {
          divAppender("<iframe id='IF130269_1454681450863' name='I130269_1454681450863' src='https://s.dynad.net/stack/NLgPQpNAS9e3M9hA8O12L5Q4tD6e9QN0-VbPk1RpBu4.html' style='visibility: visible; width: 160px; height: 600px; border: 0;' marginwidth='0' marginwidth='0' width='160' height='600' marginheight='0' align='top' scrolling='No' bordercolor='#000000' frameborder='0' hspace='0' vspace='0'></iframe>");
          (function() {
            function call() {
              theIframe.contentWindow.postMessage(data, 'https://s.dynad.net');
            }
            var theIframe = document.getElementById('IF130269_1454681450863');
            var dyndata = (function() {
              var t = dt_dynad;
              if (typeof t === "object") return t;
              else if (typeof t === "undefined") return "";
              else return (function(e) {
                var t = function(e) {
                  var t = "";
                  for (var n = e.length - 1; n >= 0; n--)
                    if (e[n] >= "0" && e[n] <= "9") t += e[n];
                    else break;
                  return [e.substring(0, n + 1), t]
                };
                var n = e.replace(/"/g, "'").split(/&(?!([#a-z0-9]{1,4})(;))/gi);
                var r = "";
                var i = "";
                var s = "";
                for (var o = 0; o < n.length; o++) {
                  if (typeof n[o] === "undefined") continue;
                  var u = n[o].split("=");
                  if (u[0] == "_nofobjs" || u[0] == "_dyn_hash_" || u.length != 2) continue;
                  var a = t(u[0]);
                  if (a[1] != r && i != "") {
                    s += (s == "" ? "" : ",") + "{" + i + "}";
                    i = ""
                  }
                  if (a[0] != "") {
                    r = a[1];
                    i += (i == "" ? "" : ",") + '"' + a[0] + '":"' + decodeURIComponent(u[1]) + '"'
                  }
                }
                s += (s == "" ? "" : ",") + "{" + i + "}";
                return "[" + s + "]"
              })(t)
            })(decodeURIComponent());
            var data = '{"target":"_blank","clickTAG":"[ENCODED_CLICK_REDIRECT]https://t5.dynad.net/c/?dc=130269;ct=1;ci=195189;cr=4000006837.0;cb=4000006844.0;tracker=v2_2;C1;","clickTAG2":"","clickTAG3":"","clickTAG4":"","clickTAG5":"","data":' + dyndata + '}';
            if (window.addEventListener) {
              theIframe.addEventListener('load', call);
            } else {
              theIframe.attachEvent('onload', call);
            }
          })();
        } catch (e) {
          localCache = "";
          var v = '<A HR';
          v += 'EF="https://t5.dynad.net/c/?dc=130269;ci=195189;cr=4000006844.0;ord=1454681450863;srctype=bkp;C=0;tp=HTML;st=1;er=';
          v += encodeURI(e.message);
          v += '" TARGET="_blank" STYLE="" ><IMG ID="I130269_1454681450863" S';
          v += 'RC="https://s.dynad.net/stack/48a6ksuJgpcBaoT7_7INJ-jDC5Jp4kV-SqGt3cPzwvw.jpg" WIDTH="160" HEIGHT="600" ALT="" BORDER="0"></a><IMG SRC="https://t5.dynad.net/n/?dc=130269;ci=195189;bi=4000006844.0;cr=4000006837.0;srctype=bkp;1454681450863&e=error3&m=' + escape(typeof e.stack !== 'undefined' ? e.stack : (e.message || '')) + '" STYLE="display:none;" WIDTH="0" HEIGHT="0" BORDER="0" />';
          divAppender(v);
          if (typeof window.console !== 'undefined' && typeof window.console.log !== 'undefined') console.log(e.message);
        }

        ci = '195189';
        cr = '4000006837.0';
      }
    }
  fb = setTimeout(function() {
    nextStage(false);
  }, 3000);
  (function(reqid, customerId, businessId, geoData, weatherData, containerId, placementId, hasRetargeting, campaignId, tagParameters, defaultCampaignId) {
    
    try {

      var oob = "";

        try {
          var ss = document.getElementsByTagName('script');
          for (var x = 0; x < ss.length; x++) {
            if (typeof ss[x].src !== "undefined" && ss[x].src.indexOf('dynad.net') > -1 && ss[x].src.indexOf('dc='+placemnetId+';') > -1 && typeof ss[x].getAttribute('OOBClickTrack') !== "undefined") {
              oob = ss[x].getAttribute('OOBClickTrack');
              break;
            }
          }
        } catch (e) {}

        try {
          if( typeof tagParameters !== "undefined" && typeof tagParameters['strategy'] !== "undefined" ) {
          setCPam('strategy=' + tagParameters['strategy'], false);
            if( tagParameters['strategy'] != '' ) { 
                /*try {
                  setTimeout(function () {
                    window['callbackiruol'] = function(d){}; 
                  var e = document.createElement('script'); e.setAttribute('type', 'text/javascript'); e.setAttribute('async', 'true');
                e.setAttribute('src', '//metrics.dynad.net/resources/counter/jsonp/incWithFk/emporiocerveja/display_mm/strategy/'+encodeURIComponent(tagParameters['strategy'])+'/?callback=callbackiruol&ord=' + Date.now() );
                (document.body || document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild(e);
                  }, 30);
            } catch(exdyn) {}*/
            }
          }
        } catch (exMM) {}

        var customerCart     = 'd' + customerId + 'C';
        var customerCategory = 'd' + customerId + 'c';
        var customerSold     = 'd' + customerId + 's';
        var customerProduct  = 'd' + customerId + 'p';
        
        var last30Days  = new Date(getLastDate(getCurrentDate(), 30)).getTime();
        var last60Days  = new Date(getLastDate(getCurrentDate(), 60)).getTime();
        var last90Days  = new Date(getLastDate(getCurrentDate(), 90)).getTime();
        var last120Days = new Date(getLastDate(getCurrentDate(), 120)).getTime();
        var last150Days = new Date(getLastDate(getCurrentDate(), 150)).getTime();
        var last24Hrs   = new Date(getLastDate(getCurrentDate(), 1)).getTime();
        var last3Days   = new Date(getLastDate(getCurrentDate(), 3)).getTime();

        function loadRTData(skus, callback) {
          loader("//sna.dynad.net/eval/"+ businessId +"?p=" + skus + "&oob=" + encodeURIComponent(oob), function(e) { 
            callback((e != ""), e);
          }, function() {
            callback(false);
          });
        }

        function getCurrentDate() {
            var today = new Date();
            var year = today.getFullYear();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            return new Date (month + "/" + day + "/" + year).getTime();
        }

        function getLastDate(date, diference) {
            var lastDate = new Date(date);
            var dayOfMonth = lastDate.getDate();
            diference = (diference) ? diference : 0;
            return new Date(lastDate.setDate(dayOfMonth - diference)).getTime();
        }

        function formatLocalStorageDate(timestamp) {
            var fullDate = Number (timestamp + '000');
            var changeHour = new Date(fullDate).setHours(0,0,0,0);
            return new Date(changeHour).getTime();
        }

        function validateSkus(json) {
            if(!(json || json.constructor !== Array || json.length == 0)) {
                return false;
            }
            if(!json[0] || !json[0].t) {
                return false;
            }
            return true;
        }

        function applyFilterInactive(db, callback){
          db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }                
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var skuList = [];  

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }  

                if(localStorageTime <= last90Days && localStorageTime > last120Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive90dias', f, e);
                  });
                  return;
                } 

                if(localStorageTime <= last120Days && localStorageTime > last150Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive120dias', f, e);
                  });
                  return;
                } 

                if(localStorageTime <= last150Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('inactive150dias', f, e);
                  });
                  return;
                } 
                
                callback(false);
                return;
            
            });
        }

        function applyFilterReactivated(db, callback) {
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }                
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                if(skus.length < 2) {
                  callback(false);
                  return;
                }
                
                var monthsWithoutBuying = formatLocalStorageDate(skus[1].t);
                var lastBuy = formatLocalStorageDate(skus[0].t);
                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if (monthsWithoutBuying < last90Days && lastBuy >= last90Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('reactivated', f, e);
                  });
                  return; 
                }

                callback(false);
                return;
            });
        }

        function applyFilterBasket(db, callback) {
            db.getItem (customerCart, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }       

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var forgetCart = formatLocalStorageDate(skus[0].t);
                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if(forgetCart >= last24Hrs) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basket24dias', f, e);
                  });
                  return;
                }

                if(forgetCart < last24Hrs && forgetCart >= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basket3dias', f, e);
                  });
                  return; 
                }

                if(forgetCart < last24Hrs && forgetCart <= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('basketAcima3dias', f, e);
                  });
                  return; 
                }
                
                callback(false);
                return; 
            });
        }

        function applyFilterProduct(db, callback) {
            db.getItem (customerProduct, function(key, value) {
                
                if(value === null) {
                    callback(false);
                    return;
                }       

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var viewProduct = formatLocalStorageDate(skus[0].t);
                var skuList = [];

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }  
                
                if (viewProduct >= last24Hrs) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('product24hrs', f, e);
                  });
                  return;
                }
                 
                if(viewProduct < last24Hrs && viewProduct >= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('product3dias', f, e);
                  });
                  return; 
                }

                if(viewProduct < last24Hrs && viewProduct <= last3Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('productAcima3dias', f, e);
                  });
                  return; 
                }
                
                callback(false);
                return; 
            });
        }

        function applyFilterCategorySearch(db, callback) {
          db.getItem (customerCategory, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var skuList = [];  
                
                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }

                if (skus.length >= 1) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("categorySearch", f, e);
                  });
                  return;
                }

                callback(false);
                return;
          });
        }

        function applyFilterRecurring(db, callback){
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }
                
                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var lastBuy = formatLocalStorageDate(skus[1].t);
                var skuList = []; 

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }
                
                if(localStorageTime >= last90Days && lastBuy >= last90Days && skus.length == 2) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF1", f, e);
                  });
                  return;
                } 

                if(localStorageTime >= last90Days && lastBuy >= last90Days && skus.length == 3) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF2", f, e);
                  });
                  return;
                } 

                if(localStorageTime >= last90Days && lastBuy >= last90Days && skus.length == 4) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback("recurringRF3", f, e);
                  });
                  return;
                } 
                
                callback(false);
                return;
            });
        }

        function applyFilterNewDay(db, callback) {
            db.getItem (customerSold, function(key, value) {
                if(value === null) {
                    callback(false);
                    return;
                }

                var skus = JSON.parse(value);

                if(!validateSkus(skus)) {
                    callback(false);
                    return;
                }

                if(skus.length > 1) {
                    callback(false);
                    return;
                }
                
                var localStorageTime = formatLocalStorageDate(skus[0].t);
                var skuList = []; 

                for ( var i = 0; i < skus.length; i++ ) {  
                  skuList.push(skus[i].v);  
                }
                
                if (localStorageTime >= last30Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new30dias', f, e);
                  });
                    return;
                } 

                if (localStorageTime >= last60Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new60dias', f, e);
                  });
                  return;
                } 

                if (localStorageTime >= last90Days) {
                  loadRTData(skuList.toString(), function(f, e) {
                    callback('new90diasOuMais', f, e);
                  });
                  return;
                }
                
                callback(false);
                return;
            });
        }

        function applyFilterTopSeller() {
          loadRTData('topSeller', function(f, e) {
            if (f === true) {
              nextStage('topSeller', e, true);
            } else {
              applyGeneralFilter();
            }
          });
        }

        function applyGeneralFilter() {
          console.log('applyGeneralFilter');
          nextStage(false);
        }

        var applySample = function() {
          if (Math.random() <= 0.5) {
            applyFilterTopSeller();
          } else {
            applyGeneralFilter();
          }
        }

        new DADB(function(db) {

          function applyNextRules(index) {
            switch(queue[index]) {
              case "basket":
                applyFilterBasket(db, callbackFilter);
              break;
              case "product":
                applyFilterProduct(db, callbackFilter);
              break;
              case "categorySearch":
                applyFilterCategorySearch(db, callbackFilter);
              break;
              case "newDay":
                applyFilterNewDay(db, callbackFilter);
              break;
              case "recurring":
                applyFilterRecurring(db, callbackFilter);
              break;
              case "reactivated":
                applyFilterReactivated(db, callbackFilter);
              break;
              case "inactive":
                applyFilterInactive(db, callbackFilter);
              break;
              case "topseller":
                applySample();
              break;
            }
          }

          var queue = ['basket', 'product', 'categorySearch', 'newDay', 'recurring', 'reactivated', 'inactive', 'topseller'];
          var index = 0;
          function callbackFilter(filterName, f, e){
            console.log("CURRENT FILTER", filterName, f, e, index);
            if(f === true) {
              nextStage(filterName, e, true);
              return;
            } else {
              index++;
              applyNextRules(index);
            }
          }
          
          applyNextRules(index);
        });

    } catch (e) {
      (function() {
        var el = document.createElement("img");
        el.setAttribute("src", "https://t5.dynad.net/gfep/?dc=130269;ord=[RANDOM_NUMBER];strategy=[AD_ATTR.strategy];campaign=[AD_ATTR.campaign];click=[ENCODED_CLICK_REDIRECT]&e=error30&m=" + escape(typeof e.stack !== 'undefined' ? e.stack : (e.message || '')));
        el.setAttribute("height", "0");
        el.setAttribute("width", "0");
        el.style.display = "none";
        document.getElementById("_dynad_c_I130269_1454681450863").appendChild(el);
      })();
      if (typeof window.console !== 'undefined' && typeof window.console.log !== 'undefined') console.log(e.message);
      nextStage(false);
    }
  })('130269_1454681450863', '2015070803', '1000053', {
    city: window._cy_dynad_130269_1454681450863,
    state: window._st_dynad_130269_1454681450863,
    country: window._ct_dynad_130269_1454681450863,
    latitude: window._lt_dynad_130269_1454681450863,
    longitude: window._lg_dynad_130269_1454681450863
  }, {
    temperature: window._wt_dynad_130269_1454681450863,
    weather: window._we_dynad_130269_1454681450863,
    weatherCode: window._wc_dynad_130269_1454681450863,
    wLatitude: window._wlt_dynad_130269_1454681450863,
    wLongitude: window._wlg_dynad_130269_1454681450863,
    wLocalTime: window._wl_dynad_130269_1454681450863,
    wIcon: window._wi_dynad_130269_1454681450863,
    wind: window._ww_dynad_130269_1454681450863,
    pressure: window._wp_dynad_130269_1454681450863,
    humidity: window._wh_dynad_130269_1454681450863
  }, '_dynad_c_I130269_1454681450863', '130269', false, '195189', window._tp_dynad_130269_1454681450863, '195189');
  document.write('</div>');
})();