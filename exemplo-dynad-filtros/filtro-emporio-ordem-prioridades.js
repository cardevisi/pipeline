//*************************************************
// ORDEM DE PRIORIDADES DAS CAMPANHAS DE MEDIA MATH
//*************************************************

Basket – 24hrs				(prioridade 5) 
Basket – 3 dias				(prioridade 7) 
Basket – acima de 3 dias	(prioridade 9) 
Product  – 24hrs			(prioridade 11) 
Product – 3 dias			(prioridade 13) 
Product – acima de 3 dias	(prioridade 15) 
Category / Search			(prioridade 17) 
New – 30 dias				(prioridade 20)
New – 60 dias				(prioridade 22)
New – 90 dias ou mais		(prioridade 25)
Recurring – RF 1			(prioridade 30)
Recurring – RF 2			(prioridade 32)
Recurring – RF 3			(prioridade 35)
Reactivated					(prioridade 40)
Inactive – 90 dias			(prioridade 50)
Inactive – 120 dias			(prioridade 52)
Inactive – 150 dias			(prioridade 55)
Top Seller					(prioridade 60) 
General						(prioridade 80) 

Para validar se os filtros estão funcionando correntamente, devemos verificar as tags do tipo anchor e checar se o paramentro CI da uma conincide com a o id da campanha

<a class="item" href="[ENCODED_CLICK_REDIRECT]https://t5.dynad.net/c/?dc=130269;ct=1;p=strategy%3D%5BAD_ATTR.strategy%5D;ci=195212;cr=4000006837.0;cb=4000006844.0;tracker=v2_2;C1;r64=aHR0cDovL3d3dy5lbXBvcmlvZGFjZXJ2ZWphLmNvbS5ici9wcm9kdWN0L2hvZWdhYXJkZW4vNDU4NjIwL2tpdC1ob2VnYWFyZGVuLTMzMG1sLW5hLWNvbXByYS1kZS0zLWxldmUtNg--;p64=bm9tZV9wcm9kdXRvPUtpdC1Ib2VnYWFyZGVuLTMzME1MLS0tTmEtQ29tcHJhLWRlLTMtLUxldmUtNiZza3VfcHJvZHV0bz00NTg2MjA-;" target="_blank" onclick="metrics(this.getAttribute('href'));" style="visibility: visible;">
					<img class="imagem" src="http://static.dynad.net/let/u4Fn4MflqY1TpqkS_FX-kFh7ubS9dcFqFYRPial7FjuFM037uhvpIx-73tXPtAGwO0YJdaOjEU8CvLRA49p7dtwXV6lkhlynB_-WYgFIfCs?hash=QSGVYWxhBSOW8udzvMZx1g">
					<div class="descricao animated">
						<span class="nome">Kit Hoegaarden 330ML - Na Compra de 3, Leve 6</span>
						<div class="call bold animated">
							<span>COMPRAR</span>
						</div>
					</div>
				</a>



#195210	CMP_MM_TESTE_Basket_24hrs	OK
#195211	CMP_MM_TESTE_Basket_3_dias	OK
#195212	CMP_MM_TESTE_Basket_acima_de_3_dias	OK

#195213	CMP_MM_TESTE_Product_24hrs	OK
#195214	CMP_MM_TESTE_Product_3_dias	OK
#195215	CMP_MM_TESTE_Product_acima_de_3_dias	OK

#195216	CMP_MM_TESTE_Category_Search	OK

#195186	CMP_MM_TESTE_New_30_dias	OK
#195202	CMP_MM_TESTE_New_60_dias	OK
#195203	CMP_MM_TESTE_New_90_dias_ou_mais OK	

#195204	CMP_MM_TESTE_Recurring_RF1	OK
#195205	CMP_MM_TESTE_Recurring_RF2	OK
#195206	CMP_MM_TESTE_Recurring_RF3	OK

#195218	CMP_MM_TESTE_Reactivated	OK

#195207	CMP_MM_TESTE_Inactive_90_dias	OK
#195208	CMP_MM_TESTE_Inactive_120_dias	OK
#195209	CMP_MM_TESTE_Inactive_150_dias	OK
#195217	CMP_MM_TESTE_Top_Seller	OK
#195189	CMP_MM_TESTE_General OK