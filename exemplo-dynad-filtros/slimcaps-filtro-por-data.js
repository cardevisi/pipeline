try {

	function getCurrentDate() {
		var today = new Date();
		var year = today.getFullYear();
		var day = today.getDate();
		var month = today.getMonth() + 1;
		return new Date (month + "/" + day + "/" + year).getTime();
	}

	function getLastDate(date, diference) {
		var lastDate = new Date(date);
		var dayOfMonth = lastDate.getDate();
		diference = (diference) ? diference : 0;
		return new Date(lastDate.setDate(dayOfMonth - diference)).getTime();
	}

	var customerCart = 'd' + customerId + 'C';
	var customerProduct = 'd' + customerId + 'p';
	var today = getCurrentDate();
	var lastFourDays = new Date(getLastDate(getCurrentDate(), 4)).getTime();
	var lastTwoDays = new Date(getLastDate(getCurrentDate(), 2)).getTime();

	new DADB(function(db) {
		checkFilterCart(db, function(data){
			if(data === false) {
				
				checkFilterProduct(db, function(data) {
					if (data === 'produto_4') {
						nextStage("produtos4dias", null, false);
					} else if (data === 'produto_2') {
						nextStage("produtos2dias", null, false);
					} else {
						console.log('nenhum produto');
					}
				});
				return;	
			}

			if(data === 'carrinho_4') {
				nextStage("carrinho4dias", null, false);
			} else if(data === 'carrinho_2') {
				nextStage("carrinho2dias", null, false);
			} else {
				console.log('nenhum carrinho');
			}
		});
	});
	
	function checkFilterProduct(db, callback) {
  		db.getItem (customerProduct, function(key, value) {
			if(value === null) {
				callback(false);
				return;
			}
			
			var skus = JSON.parse(value);
				
			if(!validateSkus(skus)) {
				callback(false);
				return;
			}

				var localStorageTime = formatLocalStorageDate(skus[0].t);

			if (localStorageTime >= lastFourDays && localStorageTime <= lastTwoDays) {
				callback("produto_4");
				return;
			} else if (localStorageTime > lastTwoDays) {
				callback("produto_2");
				return;
			} 
			
			callback(false);
			return;
			
		});
  	}

  	function formatLocalStorageDate(timestamp) {
  		var fullDate = Number (timestamp + '000');
		var changeHour = new Date(fullDate).setHours(0,0,0,0);
		return new Date(changeHour).getTime();
  	}

  	function checkFilterCart (db, callback) {
  		db.getItem (customerCart, function(key, value) {
			if(value === null) {
				callback(false);
				return;
			}
			
			var skus = JSON.parse(value);  
			
			if(!validateSkus(skus)) {
				callback(false);
				return;
			}

			var localStorageTime = formatLocalStorageDate(skus[0].t);

			if (localStorageTime >= lastFourDays && localStorageTime <= lastTwoDays) {
				callback('carrinho_4');
				return;
			} else if (localStorageTime > lastTwoDays) {
				callback('carrinho_2');
				return;
			} 
		
			callback(false);
			return;
		});
  	}

  	
  	function validateSkus(json) {
  		if(!(json || json.constructor !== Array || json.length == 0)) {
			return false;
		}

		if(!json[0] || !json[0].t) {
			return false;
		}

		return true;
  	}

} catch () {}