Peixe Urbano:

Configuração da Tag de "Produto":

A Tag "Visualização de oferta/produto" será utilizada para capturar os dados:
- ID único do visitante 
- SKU do produto visualizado.

Configuração da Tag de "Categoria":

Tag "Categoria / Resultado de busca por região", utilizada para capturar os dados:
- ID único do visitante 
- Lista de SKUs mais relevantes dentro da categoria ou resultado de pesquisa.

Configuração da Tag de "Checkout":

Tag "Sucesso de compras", utilizada para capturar os dados:
- ID único do visitante.
- Lista de ofertas/produtos convertidos.
- Valor total da compra.

Configuração da Tag de "Carrinho de compras":

Tag “Carrinho de Compras” utilizada para capturar os dados: 
- ID único do visitante.
- Lista de produtos adicionados ao carrinho.



//Fri Feb 05 2016 15:00:09 GMT-0200 (E. South America Daylight Time)
//Sat Nov 07 2015 15:00:09 GMT-0200 (E. South America Daylight Time)

//[{"v":"456678","t":1447002009}, {"v":"456679","t":1446915609}, {"v":"456680","t":1446915609}]


