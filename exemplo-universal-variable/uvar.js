/*----------------------------------
Carrinho
----------------------------------*/
window.universal_variable = {
  "page": {
    "type": "basket"
  },
  "basket": {
    "line_items": [
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      },
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      }
    ]
  }
};

/*----------------------------------
Categoria
----------------------------------*/
window.universal_variable = {
  "page": {
    "type": "category"
  },
  "listing": {
    "items": [
      {
        "sku_code": "<product_sku>"
      },
      {
       "sku_code": "<product_sku>"
      }
    ]
  }
};

/*----------------------------------
Checkout
----------------------------------*/
window.universal_variable = {
  "page": {
    "type": "checkout"
  },
  "basket": {
    "line_items": [
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      },
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      }
    ]
  },
  "transaction": {
    "order_id": "<order_id>",
    "total": "<order_total>",
    "line_items": [
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      },
      {
        "product": {
          "sku_code": "<product_sku>"
        }
      }
    ]
  }
};

/*----------------------------------
Produto
----------------------------------*/
window.universal_variable = {
  "page": {
    "type": "product"
  },
  "product": {
    "sku_code": "<product_sku>"
  }
};
