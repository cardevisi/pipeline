###Criação ed variaveis universais

No geral os valores capturados do DataLayer precisam sempre estar em string.

###Validação de variaveis universais nas páginas dos clientes:

Notas: Tudo o que não for produto e carrinho e Checkout é considerado categoria.

[Documentação sobre UVARS](http://tm.uol.com.br/retargeting-tracking.html)

##Página de Produto:
	```
	window.universal_variable = {
			"page": {
				"type": "Product"
			},
			"product":{
				"sku_code": "3UQ9A"
			}
	};
	````


##Página de Categorias:
	```
	window.universal_variable = {
		"page": {
		"type": "Category",
			"breadcrumb" : [
			"suits",
			"single breasted suits",
			"oxford"
			]
		},
		"listing": {
			"items": [{"sku_code": "1234567890red"},
			{"sku_code": "1234567891red"}]
		}
	};
	````


##Página de Checkout:
	```
	window.universal_variable = {
	  "page": {
	    "type": "checkout"
	  },
	  "basket": {
	    "line_items": [
	      {
	        "product": {
	          "sku_code": "<product_sku>"
	        }
	      },
	      {
	        "product": {
	          "sku_code": "<product_sku>"
	        }
	      }
	    ]
	  }
	  "transaction": {
	    "order_id": "<order_id>",
	    "total": "<order_total>",
	    "line_items": [
	      {
	        "product": {
	          "sku_code": "<product_sku>"
	        }
	      },
	      {
	        "product": {
	          "sku_code": "<product_sku>"
	        }
	      }
	    ]
	  }
	};
	````

##Página de Carrinho:

	```
	window.universal_variable = {
		"page": {
			"type": "basket"
		},
		"basket": {
			"line_items": [{
				"product": {
					"sku_code": "3UQ9A"
				}
			}, {
				"product": {
					"sku_code": "3UQ9B"
				}
			}]
		}
	};

	```


###Estrutura da UVAR do Plugin DFP Conversion: 

```

(function(){
    try {
        var uvar = window.universal_variable;
        uvar.transaction = {};
        var order_id = document.querySelector('.order-number strong').textContent || "ORDER_ID";
        var total = document.querySelector('.order-number strong').textContent || "TOTAL";
    } catch(e) {
        console.log('UOLTM: Não foi possível criar basket');
    }

    try {
        uvar.transaction.order_id = order_id;
    } catch(e) {
        console.log('UOLTM: Não foi possível criar order_id');
    }

    try {
        uvar.transaction.total = total;
    } catch(e) {
        console.log('UOLTM: Não foi possível criar total');
    }

    //window.triggerUOLTM('uvar-confirmation-done');
})();

```