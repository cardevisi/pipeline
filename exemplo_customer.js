var oob = "";
try {
  var ss = document.getElementsByTagName('script');
  for (var x = 0; x < ss.length; x++) {
    if (typeof ss[x].src !== "undefined" && ss[x].src.indexOf('dynad.net') > -1 && ss[x].src.indexOf('dc=' + placementId + ';') > -1 && typeof ss[x].getAttribute('OOBClickTrack') !== "undefined") {
      oob = ss[x].getAttribute('OOBClickTrack');
      break;
    }
  }
} catch (e) {}

var listaSku = [];

function callRule() {
  loader("//sna.dynad.net/eval/" + businessId + "?p=" + listaSku.toString() + "&oob=" + encodeURIComponent(oob),
  function(e) { //OK
    if (e != "") {
      nextStage(true, e, true);      
    } else {
      nextStage(false);      
    }
  },
  function(e) {
    nextStage(false);
  });
}

new DADB(function(db) {
  db.getItem('d' + customerId + 'p', function(key, value) {
    if (value != null && value != '') {
      var skus = JSON.parse(value);
      for (var i = 0; i < skus.length; i++) {
        listaSku.push(skus[i].v);
      }
    }
    if (listaSku.length > 0) {
      callRule();
      return;
    } else {
      db.getNav(customerId, function(key, value) {
        if (value) {
          listaSku.push(value.split('.')[2]);
          callRule();
        } else {
          nextStage(false);          
        }
      });
    }
  });
});