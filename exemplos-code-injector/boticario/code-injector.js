(function(window) {

	try {

		window.triggerUOLTM = window.triggerUOLTM || [];
		
		window.DebuggerMessages = (function(){
		    var instantiated;

		    function init() {
		        return {
		            setDebug : function (){
	            		localStorage.setItem('debug_tm', 'true');
			        },
			        getDebug : function (){
			            return localStorage.getItem('debug_tm');
			        },
			        clearDebug : function (){
			            localStorage.removeItem('debug_tm');
			        },
			        showMessage : function () {
			            var message = [];
			            for (var i = 0; i <= arguments.length; i++) {
			                message.push(arguments[i]);
			            }
			            if (this.getDebug() === 'true') console.log(message.join(' '));
			        }
		        };
		    }

		    return {
		        getInstance : function() {
		            if(!instantiated) {
		                instantiated = init();
		            }
		            return instantiated;
		        }
		    }
		})();
		
		(function(window) {
			function Validator(element) {}
			Validator.prototype.validateInputs = function(elementName){
				var bool = true;

				if (!this.inputs) {
					return bool;
				}

				this.inputs.forEach( function(element, index) {
					if (element && element.getAttribute('type') != 'hidden') {
						var className = element.getAttribute('class');
						var pattern = /has-error/;
						if(pattern.test(className)) {
							DebuggerMessages.getInstance().showMessage('::: ELEMENT :::', className);
							bool = false;
						}
					} 
				});	

				return bool;
			};

			Validator.prototype.validateByExistsClass = function(length){
				return (length > 0) ? false : true;
			};
			window.Validator = Validator;
		})(window);


		var $ = document.querySelector.bind(document);

		var btnStep3 = $('#step-1 .register-submit button');
		if (btnStep3) {
			btnStep3.addEventListener('click', function(){
				this.inputs = $('#step-1').querySelectorAll('input');
				var aux = Validator.prototype.validateInputs.call(this);
				if (aux) {
					DebuggerMessages.getInstance().showMessage('::: CALL TRIGGER :::', 'aux', aux, 'call-trigger', 'trigger-step-1');
					window.triggerUOLTM.push({"eventName": "trigger-passo-1"}); 
				}
			});
		}

		var btnStep2 = $('#step-2 .register-submit button:nth-child(2)');
		if(btnStep2) {
			btnStep2.addEventListener('click', function(){
				this.inputs = $('#step-2').querySelectorAll('input');
				var aux = Validator.prototype.validateInputs.call(this);
				if (aux) {
					DebuggerMessages.getInstance().showMessage('::: CALL TRIGGER :::', 'aux', aux, 'call-trigger', 'trigger-step-2');
					window.triggerUOLTM.push({"eventName": "trigger-passo-2"});
				}
			});
		}

		var btnStep4 = $('#step-4 .register-submit button:nth-child(2)');
		if(btnStep4) {
			btnStep4.addEventListener('click', function(){
				var classList = $('#step-4').querySelectorAll('.has-error').length;
				var aux = Validator.prototype.validateByExistsClass.call(this, classList);
				if (aux) {
					DebuggerMessages.getInstance().showMessage('::: CALL TRIGGER :::','aux', aux, 'call-trigger', 'trigger-step-3');
					window.triggerUOLTM.push({"eventName": "trigger-passo-3"});
				}
			});
		}
	} catch(e) {
		console.log('Erro ao executar o code injetor', e);
	}
	
})(window)