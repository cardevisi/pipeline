(function(){

    function CreateAdBlock(config) {

        // para debugar a inclusão dos blocos nas páginas passar o parâmetro "debuga-cliques" por queryString

        var path = "//adrequisitor-af.lp.uol.com.br/uolaf.js";
        var debug = (document.location.search.indexOf('debuga-cliques') !== -1) ? true : false;
        var params = [];

        function validate(config) {
            if (!config || !config.default) {
                if (debug) console.log('[UOL CLIQUES DEBUG] Config sem valor default');
                return false;
            }

            if (!config || !config.default.coddisplaysupplier) {
                if (debug) console.log('[UOL CLIQUES DEBUG] Config sem valor de coddisplaysupplier');
                return false;
            }

            if (!config || !config.blocks || !(config.blocks instanceof Array)) {
                if (debug) console.log('[UOL CLIQUES DEBUG] Config sem a propriedade blocks');
                return false;
            }

            return true;
        }
        
        function generateDefaultParams(params){
            var arr = [];
            for (var key in params) {
                if (key === 'remove' || params[key] === '' || params[key] === undefined) {
                    continue;
                }
                arr.push (key+'='+params[key]);
            }
            return arr;
        }

        function generateBlocksParams(item){
            var arr = [];
            for (var key in item) {
                var arr = [];            
                
                if (key === 'selector') {
                    continue;
                }

                if (item[key] === '' || item[key] === undefined) {
                    continue;
                }
                
                arr.push (key+'='+item[key]);
                arr.push ('ord='+new Date().getTime());
            }

            return arr;
        }

        function containerExists(block) {
            var container = document.querySelector(block.selector);
        
            if (!container) {
                if (debug) {
                    console.log("[UOL CLIQUES DEBUG] Container inválido", block.selector);
                }
                return false;
            }

            return container;
        }

        function removeChildNodes(element) {
            while (element.hasChildNodes()) {
                element.removeChild(element.firstChild);
            }
            return true;
        }

        function appendInDom(container, url, remove) {
            var script = document.createElement('script');
            script.src = url;
            var removeChild = (remove) ? remove : false;
            var ads = document.createElement('div');
            ads.setAttribute('class', 'tm-dfp-cliques');
            ads.appendChild(script);
            if (removeChild) removeChildNodes(container);
            container.appendChild(ads);
        }
        
        this.init = function() {

            if (!validate(config)) {
                return;
            }

            var defaultValues = generateDefaultParams(config.default);

            for (var i = 0; i < config.blocks.length; i++) {
                var container = containerExists(config.blocks[i]);

                if (!container) {
                    continue;
                }

                var item = config.blocks[i];
                var blockValues = generateBlocksParams(item);
                var url = path + '?' + defaultValues.sort().concat(blockValues).join('&');

                appendInDom(container, url, config.default.remove);
            }
        }
        
        return this;
    }

     CreateAdBlock({
            'default': {
                'cssBgColor': 'FFFFFF',
                'cssTitleColor': '598D32',
                'cssUrlColor': '598D32',
                'cssDescrColor': 'ff0000',
                'coddisplaysupplier' : '7dd338a69433464d819ade3e8e97c879',
                'excludeTM' : true,
                'remove': true
            },
            'blocks': [
                {
                    'selector' : '#anuncio728',
                    'formatId' : '1',
                },{
                    'selector' : '#jogo_ads_novo1',
                    'formatId' : '5'
                },{
                    
                    'selector' : '#jogo_ads_novo',
                    'formatId' : '5'
                },{
                    
                    'selector' : '#removerrrr',
                    'formatId' : '1'
                }
            ]
        }
    ).init();

})();


https://adrequisitor-af.lp.uol.com.br/uolaf.js?coddisplaysupplier=7dd338a69433464d819ade3e8e97c879&cssBgColor=FFFFFF&cssDescrColor=ff0000&cssTitleColor=598D32&cssUrlColor=598D32&excludeTM=true&remove=true&formatId=1&ord=1471886371023

