var bannerOpen = new Amadre_Animation();



var corte_erro01 = new Amadre_Animation();
var corte_erro02 = new Amadre_Animation();
var corte_acerto = new Amadre_Animation();

var erro_entrada_popup = new Amadre_Animation();
var erro_entrada = new Amadre_Animation();
var erro_saida = new Amadre_Animation();
var erro_ver_corte_certo = new Amadre_Animation();
var erro_tentar_denovo = new Amadre_Animation();
var acerto_primeira_tentativa = new Amadre_Animation();
var acerto_segunda_tentativa = new Amadre_Animation();

var tentativas = 0;


var controle_corte = false;
var controle_btn_final = false;

var clickTag_ativo = false;



init = function(){ 

	

/*████████████████ ◢ OPENED - STEP01 ◣ ████████████████████████████████████████████████████████████*/
/*▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ OPENED - STEP01 ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓*/
	
// ████████████████ ◢ STEP01 ◣ ████████████████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP01 ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

	bannerOpen.animationElements("simpleShapeOut",[[".simpleShape",.1]]);
	bannerOpen.animationElements("BorderIn01",[[".borderColor01A",0]]);
	bannerOpen.animationElements("BorderIn02",[[".borderColor02A",0]]);
	bannerOpen.animationElements("BorderIn03",[[".borderColor03A",0]]);
	bannerOpen.animationElements("BorderIn04",[[".borderColor04A",0]]);

	bannerOpen.animationElements("simpleShapeOut",[[".BGEffect",.05]]);
	bannerOpen.animationElements("BGImageIn",[[".BGImage",.05]]);
	bannerOpen.animationElements("BGImageIn",[[".meatContainer",0]]);

	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage01",1]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage02",.025]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage03",.05]]);
	bannerOpen.animationElements("drawIn",[[".CampaignLogoImage04",.1]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage05",.1]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage06",.1]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage07",.1]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage08",.1]]);
	bannerOpen.animationElements("drawIn",[[".CampaignLogoImage09",.1]]);
	bannerOpen.animationElements("scaleIn02",[[".CampaignLogoImage10",-.6]]);
	bannerOpen.animationElements("scaleIn02",[[".CampaignLogoImage11",.025]]);
	bannerOpen.animationElements("scaleIn01",[[".CampaignLogoImage12",.5]]);

	bannerOpen.animationElements("TextIn01",[[".TXT01A",-.2]]);
	bannerOpen.animationElements("TextIn01",[[".TXT01B",.05]]);
	bannerOpen.animationElements("TextIn01",[[".TXT01C",.05]]);
	bannerOpen.animationElements("TextIn01",[[".TXT01D",.05]]);

	bannerOpen.animationElements("fadeIn",[[".gameTimer",.015]]);
	bannerOpen.animationElements("timerSprite",[[".gameTimerSprite",0]]);
	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber01",0]]);

	bannerOpen.animationElements("kniveCursor_TutorialAnimation",[[".knifeIcon",1]]);	
	bannerOpen.animationElements("blockedIconOut",[[".blockedIcon",2.5]]);	
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG00",1]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG01",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG02",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG03",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG04",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG05",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG06",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG07",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG08",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG09",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG10",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG11",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG12",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG13",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG14",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG15",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG16",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG17",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG18",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG19",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG20",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG21",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG22",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG23",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG24",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG25",.015]]);
	bannerOpen.animationElements("dotlineIn",[[".dotlineIMG26",.015]]);

	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber01",-4.5]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber02",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber02",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber03",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber03",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber04",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber04",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber05",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber05",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber06",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber06",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber07",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber07",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber08",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber08",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber09",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber09",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber10",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber10",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber11",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber11",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber12",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber12",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber13",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber13",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber14",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber14",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber15",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber15",1]]);

	bannerOpen.animationElements("visibleTrue",[[".gameTimerNumber16",0]]);
	bannerOpen.animationElements("visibleFalse",[[".gameTimerNumber16",1]]);

	// TRANSITION OUT	

	bannerOpen.animationElements("kniveCursorOut",[[".knifeIcon",.1]]);	
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG00",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG01",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG02",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG03",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG04",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG05",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG06",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG07",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG08",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG09",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG10",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG11",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG12",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG13",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG14",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG15",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG16",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG17",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG18",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG19",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG20",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG21",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG22",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG23",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG24",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG25",.008]]);
	bannerOpen.animationElements("transitionOut01",[[".dotlineIMG26",.008]]);

	bannerOpen.animationElements("transitionOut01",[[".TXT01A",-.2]]);
	bannerOpen.animationElements("transitionOut01",[[".TXT01B",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".TXT01C",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".TXT01D",.05]]);

	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage01",-.1]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage02",.025]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage03",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage04",-.2]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage05",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage06",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage07",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage08",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage09",.05]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage10",-.2]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage11",.025]]);
	bannerOpen.animationElements("transitionOut01",[[".CampaignLogoImage12",.05]]);

	bannerOpen.animationElements("BGMove",[[".BGImage",0]]);
	bannerOpen.animationElements("BGMove",[[".meatContainer",0]]);

// ████████████████ ◢ STEP02 ◣ ████████████████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP02 ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

	bannerOpen.animationElements("fadeIn",[[".cutmarkImage01",.5]]);
	bannerOpen.animationElements("fadeIn",[[".cutmarkImage02",.05]]);
	bannerOpen.animationElements("fadeIn",[[".cutmarkImage03",.05]]);

	bannerOpen.animationElements("translateIn01",[[".CTGBGColor03",.5]]);
	bannerOpen.animationElements("translateIn01",[[".CTGBGColor04",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGBGColor02",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGBGColor05",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGBGColor01",.05]]);

	bannerOpen.animationElements("translateIn01",[[".CTGTextImage05",0]]);
	bannerOpen.animationElements("translateIn01",[[".CTGTextImage04",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGTextImage03",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGTextImage02",.05]]);
	bannerOpen.animationElements("translateIn01",[[".CTGTextImage01",.05]]);


	bannerOpen.animationElements("STOP_HERE",[[".CTGTextImage01",1]],function(){

			
		game_start();
			

	});


	

/*▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ OPENED - FIM ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓*/
}





// ████████████████ ◢ GAME ◣ ████████████████████████████████████████████████████████████



function game_start(){

	//////// CLICK
	el(".hitarea_01").onclick = function(){
		console.log("hitarea_01 certa");

		if(controle_corte==false){
			controle_corte=true;
			el(".cutmarkImage01").style.opacity = "0";
			el(".cutmarkImage02").style.opacity = "0";
			el(".cutmarkImage03").style.opacity = "0";
			tentativas++
			corte_errado_01();
		}


	}

	el(".hitarea_02").onclick = function(){
		console.log("hitarea_02 erro2");
		if(controle_corte==false){
			controle_corte=true;
			el(".cutmarkImage01").style.opacity = "0";
			el(".cutmarkImage02").style.opacity = "0";
			el(".cutmarkImage03").style.opacity = "0";
			tentativas++;
			corte_errado_02();
		}
		
	}

	el(".hitarea_03").onclick = function(){
		console.log("hitarea_03 erro3");
		if(controle_corte==false){
			controle_corte=true;
			el(".cutmarkImage01").style.opacity = "0";
			el(".cutmarkImage02").style.opacity = "0";
			el(".cutmarkImage03").style.opacity = "0";
			corte_acerto_final();
		}
		
	}







}





// ████████████████ ◢ STEP03 - ERRO ◣ ████████████████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP03 - ERRO ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

erro_entrada_assinatura = function(){
	
	//TRANSITION OUT	

	erro_entrada_popup.animationElements("transitionOut02",[[".CTGTextImage05",.5]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGTextImage04",.05]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGTextImage03",.05]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGTextImage02",.05]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGTextImage01",-.05]]);

	erro_entrada_popup.animationElements("transitionOut02",[[".CTGBGColor05",0]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGBGColor01",.025]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGBGColor03",.05]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGBGColor02",.025]]);
	erro_entrada_popup.animationElements("transitionOut02",[[".CTGBGColor04",.05]]);



	erro_entrada.animationElements("scaleIn01",[[".resultImage01A",1]]);
	erro_entrada.animationElements("translateIn01",[[".resultText01A",.2]]);
	erro_entrada.animationElements("translateIn01",[[".resultText01B",.05]]);

	erro_entrada.animationElements("scaleIn01",[[".SeeTheAnswer",0]]);
	erro_entrada.animationElements("scaleIn01",[[".TryAgainBTN",.05]]);

	

	erro_entrada.animationElements("STOP_HERE",[[".TryAgainBTN",0.5]],function(){

				el(".TryAgainBTN").onclick = function(){
					console.log("TryAgainBTN");
					if(controle_btn_final==false){
						controle_btn_final=true;
						//erro_saida_assinatura();
						tentar_denovo();

						setTimeout(function(){ 
							controle_corte=false;
							corte_erro01.clearAnimation(".all");
							corte_erro02.clearAnimation(".all");
							erro_entrada_popup.clearAnimation(".all");
							erro_entrada.clearAnimation(".all");
							
							el(".cutmarkImage01").style.opacity = "1";
							el(".cutmarkImage02").style.opacity = "1";
							el(".cutmarkImage03").style.opacity = "1";
							
							setTimeout(function(){ 
								controle_btn_final=false;
								erro_tentar_denovo.clearAnimation(".all");

							}, 1000);


						}, 1000);
						
					}
					
				}

				el(".SeeTheAnswer").onclick = function(){
					console.log("SeeTheAnswer");
					if(controle_btn_final==false){
						controle_btn_final=true;
						tentar_denovo();

						setTimeout(function(){ 
							
							corte_erro01.clearAnimation(".all");
							corte_erro02.clearAnimation(".all");
							erro_entrada.clearAnimation(".all");
							
							setTimeout(function(){ 
								corte_acerto_final();

							}, 500);


						}, 1000);
						
					}
				}

	});

}


	
erro_saida_assinatura = function(){
	// // TRANSITION OUT - CONTENT ERRO
	console.log("saida");

	erro_saida.animationElements("visibleFalse",[[".resultImage01A",.6]]);
	erro_saida.animationElements("visibleFalse",[[".resultText01A",0]]);
	erro_saida.animationElements("visibleFalse",[[".resultText01B",0]]);
	erro_saida.animationElements("visibleFalse",[[".SeeTheAnswer",0]]);
	erro_saida.animationElements("visibleFalse",[[".TryAgainBTN",0]]);
}



// ████████████████ ◢ GAME - CORTE ERRADO 01 ◣ ███████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ GAME - CORTE ERRADO 01 ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

corte_errado_01 = function(){

	
	corte_erro01.animationElements("KnifeIn",[[".knifeIconImage04",.5]]);
	corte_erro01.animationElements("BlinkIn",[[".KnifecutImage04",.25]]);

	corte_erro01.animationElements("meatCutLeft02",[[".meatImage01",0]]);
	corte_erro01.animationElements("meatCutRight02",[[".meatImage02",0]]);
	corte_erro01.animationElements("meatCutRight02",[[".meatImage03",0]]);
	corte_erro01.animationElements("meatCutRight02",[[".meatImage04",0]]);

	corte_erro01.animationElements("scaleIn01",[[".meatImage07",0]]);
	
	corte_erro01.animationElements("STOP_HERE",[[".meatImage07",1]],function(){
		erro_entrada_assinatura();
	});
	

}




	

// ████████████████ ◢ GAME - CORTE ERRADO 02 ◣ ███████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ GAME - CORTE ERRADO 02 ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

corte_errado_02 = function(){


	corte_erro02.animationElements("KnifeIn",[[".knifeIconImage03",.5]]);
	corte_erro02.animationElements("BlinkIn",[[".KnifecutImage03",.25]]);

	corte_erro02.animationElements("meatCutLeft03b",[[".meatImage01",0]]);
	corte_erro02.animationElements("meatCutLeft03",[[".meatImage02",0]]);
	corte_erro02.animationElements("meatCutRight02",[[".meatImage03",0]]);
	corte_erro02.animationElements("meatCutRight02",[[".meatImage04",0]]);

	corte_erro02.animationElements("scaleIn01",[[".meatImage06",0]]);

	corte_erro02.animationElements("STOP_HERE",[[".meatImage06",1]],function(){
		erro_entrada_assinatura();
	});

}


	

// ████████████████ ◢ GAME - CORTE CERTO ◣ ███████████████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ GAME - CORTE CERTO ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓



corte_acerto_final = function(){


	corte_acerto.animationElements("KnifeIn",[[".knifeIconImage02",.5]]);
	corte_acerto.animationElements("BlinkIn",[[".KnifecutImage02",.25]]);

	corte_acerto.animationElements("meatCutLeft01b",[[".meatImage01",0]]);
	corte_acerto.animationElements("meatCutLeft01",[[".meatImage02",0]]);
	corte_acerto.animationElements("meatCutLeft01",[[".meatImage03",0]]);
	corte_acerto.animationElements("meatCutRight01",[[".meatImage04",0]]);
	corte_acerto.animationElements("meatCutLeft01c",[[".meatImage06",0]]);
	corte_acerto.animationElements("meatCutLeft01c",[[".meatImage07",0]]);

	corte_acerto.animationElements("scaleIn01",[[".meatImage05",0]]);

	corte_acerto.animationElements("STOP_HERE",[[".meatImage05",1]],function(){

		if(tentativas==0){
			acerto_primeira_tentativa_final();
		}
		else{
			acerto_segunda_tentativa_final();
		}
		

	});


}




// ████████████████ ◢ STEP03 - ACERTO MA PRIMEIRA TENTATIVA ◣ ███████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP03 - ACERTO MA PRIMEIRA TENTATIVA ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

acerto_primeira_tentativa_final = function(){


	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGTextImage05",.5]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGTextImage04",.05]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGTextImage03",.05]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGTextImage02",.05]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGTextImage01",-.05]]);

	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGBGColor05",0]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGBGColor01",.025]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGBGColor03",.05]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGBGColor02",.025]]);
	acerto_primeira_tentativa.animationElements("transitionOut02",[[".CTGBGColor04",.05]]);

	acerto_primeira_tentativa.animationElements("scaleIn01",[[".resultImage02A",.5]]);
	
	acerto_primeira_tentativa.animationElements("BGMove2",[[".BGImage",2]]);
	acerto_primeira_tentativa.animationElements("BGMove2",[[".meatContainer",0]]);
	acerto_primeira_tentativa.animationElements("fadeOut",[[".resultImage02A",.4]]);
	acerto_primeira_tentativa.animationElements("scaleIn01",[[".callToAction",0]]);
	acerto_primeira_tentativa.animationElements("translateIn01",[[".gameLogoBox",.2]]);
	acerto_primeira_tentativa.animationElements("translateIn01",[[".gameTextBox",0]]);

	acerto_primeira_tentativa.animationElements("translateIn01",[[".gameTipContent",.3]]);

	acerto_primeira_tentativa.animationElements("STOP_HERE",[[".gameTipContent",.1]],function(){
		clickTag_ativo=true;
	});


}

	

// ████████████████ ◢ STEP03 - ACERTO MA PRIMEIRA TENTATIVA ◣ ███████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP03 - ACERTO MA PRIMEIRA TENTATIVA ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓


acerto_segunda_tentativa_final = function(){
	
	//TRANSITION OUT	

	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGTextImage05",.5]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGTextImage04",.05]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGTextImage03",.05]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGTextImage02",.05]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGTextImage01",-.05]]);

	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGBGColor05",0]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGBGColor01",.025]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGBGColor03",.05]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGBGColor02",.025]]);
	acerto_segunda_tentativa.animationElements("transitionOut02",[[".CTGBGColor04",.05]]);

	acerto_segunda_tentativa.animationElements("scaleIn01",[[".resultImage03A",.5]]);
	
	acerto_segunda_tentativa.animationElements("BGMove2",[[".BGImage",2]]);
	acerto_segunda_tentativa.animationElements("BGMove2",[[".meatContainer",0]]);
	acerto_segunda_tentativa.animationElements("fadeOut",[[".resultImage03A",.4]]);
	acerto_segunda_tentativa.animationElements("scaleIn01",[[".callToAction",0]]);
	acerto_segunda_tentativa.animationElements("translateIn01",[[".gameLogoBox",.2]]);
	acerto_segunda_tentativa.animationElements("translateIn01",[[".gameTextBox",0]]);

	acerto_segunda_tentativa.animationElements("translateIn01",[[".gameTipContent",.3]]);

	acerto_segunda_tentativa.animationElements("STOP_HERE",[[".gameTipContent",.1]],function(){
		clickTag_ativo=true;
	});




}





// ████████████████ ◢ STEP03 - ERRO VER CORTE CERTO ◣ ███████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP03 - ERRO VER CORTE CERTO ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

erro_ver_corte_certo_final = function(){


	erro_ver_corte_certo.animationElements("borderTransition01",[[".borderColor01B",.5]]);
	erro_ver_corte_certo.animationElements("borderTransition02",[[".borderColor02B",0]]);
	erro_ver_corte_certo.animationElements("borderTransition03",[[".borderColor03B",0]]);
	erro_ver_corte_certo.animationElements("borderTransition04",[[".borderColor04B",0]]);

	

}

// ████████████████ ◢ STEP03 - ERRO TENTAR DENOVO ◣ █████████████████████████████████████████████
// ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ◢ STEP03 - ERRO TENTAR DENOVO ◣ ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓


tentar_denovo = function(){
	

	erro_tentar_denovo.animationElements("borderTransition01",[[".borderColor01B",.05]]);
	erro_tentar_denovo.animationElements("borderTransition02",[[".borderColor02B",0]]);
	erro_tentar_denovo.animationElements("borderTransition03",[[".borderColor03B",0]]);
	erro_tentar_denovo.animationElements("borderTransition04",[[".borderColor04B",0]]);

	// TRANSITION OUT - CONTENT ERRO

	erro_tentar_denovo.animationElements("visibleFalse",[[".resultImage01A",.6]]);
	erro_tentar_denovo.animationElements("visibleFalse",[[".resultText01A",0]]);
	erro_tentar_denovo.animationElements("visibleFalse",[[".resultText01B",0]]);
	erro_tentar_denovo.animationElements("visibleFalse",[[".SeeTheAnswer",0]]);
	erro_tentar_denovo.animationElements("visibleFalse",[[".TryAgainBTN",0]]);

	
}


















