

////////////////////////////////////// DOCUMENT AMADRE ANIMATION VERSION 1.3 ///////////////////////////////////////////////////////////////





	 								//////////////////////*  PORTUGUÊS  *//////////////////////




	/*  Criando uma Instancia do objeto de animação Amadre_Animation
	-----> var 'nome_linhaDoTempo' = new Amadre_Animation()
__________________________________________________________________________________________________________________________________

	/*  Método para adicionar animações aos elementos
	-----> nome_linhaDoTempo.animationElements("classeAnimacao",[["elementoQueVaiReceber",delay],["elementoQueVaiReceber",delay],...]);
	
	 Ex.
	nome_linhaDoTempo.animationElements("classeAnimacao",[[".text_2",1],[".text_3",1],[".text_4",4]]);
	
	 OU PARA DAR UM TEMPO NA PEÇA USE A CLASS 'STOP_HERE'

	nome_linhaDoTempo.animationElements("STOP_HERE",[[".text_2",1]],função_callback(opcional));  ///text_2 tem que ser o último elemente animado antes do STOP_HERE

	 OU PARA DAR UM LOOP NA PEÇA USE A CLASS 'LOOPING'

	nome_linhaDoTempo.animationElements("LOOPING",[[".text_2",1]],função_chamaAnimações);  ///text_2 tem que ser o último elemente animado antes do LOOPING

__________________________________________________________________________________________________________________________________

	/* Método para contar tempo da animação total, passando como referência o último elemento animado
	-----> nome_linhaDoTempo.animationControlTime("últimoElementoAnimado");
	 Ex.
	nome_linhaDoTempo.animationControlTime(".text_4");

__________________________________________________________________________________________________________________________________

	/* Método para limpar timer e remover a classe do objeto
	-----> nome_linhaDoTempo.clearAnimation("elemento");
	 Ex.
	nome_linhaDoTempo.clearAnimation(".text_4");    Limpa de um obj específico
	
	 OU
	nome_linhaDoTempo.clearAnimation(".all");       Limpa de todos os objs do 'nome_linhaDoTempo'

__________________________________________________________________________________________________________________________________


	/* Método para separar uma string a partir de um caracter especifico, retornando um array [0] e [1]
	-----> nome_linhaDoTempo.splitWord("string","string_especifica_refencia");
	 Ex.
	var text_separado = nome_linhaDoTempo.splitWord("Bom dia!tudo bem?","!");
	
	text_separado[0] = "Bom dia"
	text_separado[1] = "tudo bem?"

__________________________________________________________________________________________________________________________________


	/* Método para alinhar um texto em um lugar especifico, passando como referencia o tamanho do box largura e altura, e a posição x e y
	-----> nome_linhaDoTempo.alignText("elementoQueVaiReceber",widthBoxRef,heightBoxRef,posTop,posLeft);
	 Ex.
	nome_linhaDoTempo.alignText(".text_4",200,150,50,45);


__________________________________________________________________________________________________________________________________


	/* Método para identificar se o dispositivo é mobile
	-----> nome_linhaDoTempo.isMobile();
	 Ex.
	var variável = nome_linhaDoTempo.isMobile(); ///Retorna true se estiver em um mobile, e false se estiver em desk

	if(variável==true){
		mobile
	}
	else{
		desk
	}

__________________________________________________________________________________________________________________________________

	/* Método para controlar o tamanho da string levando em conta o número de caracteres e o width do container
	-----> nome_linhaDoTempo.textAutomate("elementoQueVaiReceber",width,fontInicial);
	 Ex.
	nome_linhaDoTempo.textAutomate(".text_4",250,20);



*/


/////////////////////////////////////////////////////////////***************///////////////////////////////////////////////////////////////
var d = document;
var el = function(e){return d.querySelector(e)}
// if(typeof(obj)) var banner = JSON.parse(obj);

///////START Trace 
function trace(txt){
	 console.log(txt);
}
///////END Trace



function Amadre_Animation(){
	///
	var timeLineAnimation = 0;
	var clear = [];
	var objClearTimer;
	var debug = document.createElement( "div" );
	///	
	///Set debug
	debug.setAttribute("style","margin:2px;width:95px;height:auto;background-color:#000000;color:white;z-index:9999;position:relative;text-align:center;top:0px;left:0px;opacity: 0.5;border-radius: 10px;font-size: 10px;line-height: 50px;");
	debug.innerHTML = "<span>DEBUG:</span>";
	debug.querySelector('span').setAttribute("style","position: absolute;font-size: 9px;color: #fff;top: -16px;left: 31px;");
	
	//Set functions
	this.animationElements = animationElements;
	this.animationControlTime = animationControlTime;
	this.clearAnimation = clearAnimation;
	this.splitWord = splitWord;
	this.alignText = alignText;
	this.isMobile = isMobile;
	this.textAutomate = textAutomate;

	///////START Function automate animation 
	function animationElements(classAnimation,element,functionCall){
		var j=0;

		for(var e=0;e<element.length;e++){
			///Checks whether the element exists in the document
			if(!document.querySelector(element[e][0])){
				debug.innerHTML = debug.innerHTML+element[e][0]+": error<br>";
				document.body.appendChild(debug);
			}
			else{

					///If the element exists
					timeLineAnimation+=(element[e][1]*1000);
					objClearTimer = element[e][0].replace(".", "");
					window['timer'+e] = timeLineAnimation;
						window['clear_'+objClearTimer] = setTimeout(
							function(){

								if(classAnimation=="STOP_HERE"){
										if(functionCall){
										   functionCall();
										}
								}
								if(classAnimation=="LOOPING"){
										clearAnimation(".all");
										if(functionCall){
										   functionCall();
										}
								}
								else{

									for(var i=j;i==j;j++){
											////Assigning elements to dynamic variables
											window['elementMoment'+i] = element[i][0];
											var allElements = document.querySelectorAll(window['elementMoment'+i]);
											
											///Loop element to associate the class and callback function
											for ( var u = 0; u < allElements.length; u++ ) {
											    allElements[u].classList.add(classAnimation);
											}
									}

								}		

						},window['timer'+e]);
						
						////Polula array
						clear.push([objClearTimer,classAnimation,window['clear_'+objClearTimer]]);
				
				}

			


			}

	}

	///////END Function automate animation
	///////START Count timer animation
	function animationControlTime(element){
		var countAnimation = 1;	
		var control = document.createElement( "div" );
		control.setAttribute("style","width:95px;height:40px;background-color:#B50707;color:white;z-index:9999;position:relative;text-align:center;top:0px;left:0px;opacity: 0.5;border-radius: 50px;font-size: 22px;line-height: 50px;");
		control.innerHTML = "<span>Timer animation:</span>";
		control.querySelector('span').setAttribute("style","position: absolute;font-size: 9px;color: #fff;top: -16px;left: 9px;");

		document.body.appendChild( control );
		var timerInterval = setInterval(function(){
			////// Convert timer to string
			var countString = countAnimation.toString();
			var count;
			if(countString.length==1){
				count = "0."+countString.substr(0,1);
			}
			else if(countString.length==2){
				count = countString.substr(0,1)+"."+countString.substr(1,1);
			}
			else if(countString.length==3){
				count = countString.substr(0,1)+countString.substr(1,1)+"."+countString.substr(2,1);
			}
			////End
			control.innerHTML = count+"<span>Timer animation:</span>s";
			control.querySelector('span').setAttribute("style","position: absolute;font-size: 9px;color: #fff;top: -16px;left: 9px;");
			countAnimation++;
			el(element).addEventListener('transitionend',function(event){clearInterval(timerInterval);},false);
			el(element).addEventListener('webkitTransitionEnd',function(event){clearInterval(timerInterval);},false);
			el(element).addEventListener('oTransitionEnd',function(event){clearInterval(timerInterval);},false);
			el(element).addEventListener('animationend',function(event){clearInterval(timerInterval);},false);
			el(element).addEventListener('webkitAnimationEnd',function(event){clearInterval(timerInterval);},false);
			el(element).addEventListener('MSAnimationEnd',function(event){clearInterval(timerInterval);},false);

			
		},100)
	}
	///////END Count timer animation
	
	///////START Count timer animation
	function clearAnimation(element){
		if(element==".all"){
			////Zera variável
			timeLineAnimation = 0;
			////
			for(var i=0;i<clear.length;i++){
					var for_Elements_all = document.querySelectorAll("."+clear[i][0]);
					for(var a=0;a<for_Elements_all.length;a++){
						for_Elements_all[a].classList.remove(clear[i][1]);
					}
					clearTimeout(clear[i][2]);
					
			}
			
		}
		else{
			for(var j=0;j<clear.length;j++){
				if(clear[j][0]==element.replace(".","")){
					var for_Elements = document.querySelectorAll("."+clear[j][0]);
					for(var a=0;a<for_Elements.length;a++){
						for_Elements[a].classList.remove(clear[j][1]);
					}
					//clearTimeout(clear[j][2]);	
				}
			
			}
		}

	}
	///////END Count timer animation

	///////START Word Processing
	function splitWord(text,condition){
		////
		var splittingTxt = text.split(condition);
		console.log("Use nome_variável[0] para: "+splittingTxt[0]+"\n"+"Use nome_variável[1] para: "+splittingTxt[1]);
		return splittingTxt;
	}
	///////END Word Processing 


	///////START Processing Text
	function alignText(element,widthBoxRef,heightBoxRef,posTop,posLeft) {
       
				
       	 	var marginTop = 0;
	        var spaceHeight = heightBoxRef;
	        var marginLeft = 0;
	        var spaceWidth = widthBoxRef;
	        el(element).style.top = marginTop + ((spaceHeight - el(element).offsetHeight) / 2) + posTop + "px";
	        el(element).style.left = marginLeft + ((spaceWidth - el(element).offsetWidth) / 2) + posLeft + "px";
   
    }
	///////END Processing Text


	///////START Is Mobile
	function isMobile()
	{
		var userAgent = navigator.userAgent.toLowerCase();
		if(userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1)
		{
			return true;
		}
		else{
			return false;
		}
	}
	///////END Is Mobile


	///////START Text Automate (BETA)
	function textAutomate(element,width,fontInit){
		var text = el(element).innerHTML;
		var countText = text.length;
		var font = ((width/countText)-0.5)*2;
		
		if(fontInit>=font){
			el(element).style.fontSize = font+"px";
			console.log(font);
		}


	}
	///////END Text Automate (BETA)



	
}