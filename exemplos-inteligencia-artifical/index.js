//var robot = require("robotjs");

//Automation example 1
//robot.typeString("Hello World");
//robot.keyTap("enter");

//Automation example 2
//var mouse = robot.getMousePos();
//var hex = robot.getPixelColor(mouse.x, mouse.y);
//console.log("#" + hex + "at x:" + mouse.x + "y:" + mouse.y);


var perceptron = require('perceptron')

var and = perceptron()

and.train([1, 1], 1)
and.train([0, 1], 0)
and.train([1, 0], 0)
and.train([0, 0], 0)

// practice makes perfect (we hope...)
while(!and.retrain()) {}

console.log(and.perceive([1, 1])); // => 1
console.log(and.perceive([0, 1])); // => 0
console.log(and.perceive([1, 0])); // => 0
console.log(and.perceive([0, 0])); // => 0