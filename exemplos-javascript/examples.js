var ModuleA = (function () {

    var a = 1;

    function funcA (argument) {
        a = 2;
        // body...
    }

    function funcB (argument) {
        // body...
    }

    function funcC (argument) {
        // body...
    }


    return {
        funcA: funcA
    };
})();

ModuleA.funcA();
ModuleA();
ModuleA();

define(['module'], function (module) {
    
    var a = 1;

    function funcA (argument) {
        a = 2;
        // body...
    }

    function funcB (argument) {
        // body...
    }

    function funcC (argument) {
        // body...
    }

    module.exports.funcA = funcA;

});


var http = require('http');
function funcA (argument) {
    a = 2;
    // body...
}

function funcB (argument) {
    // body...
}

function funcC (argument) {
    // body...
}
server = http.createServer();
module.exports = server;
module.exports.funcA = funcA;


/*************
    ECMA 6
***********/

Constantes no ecma6 são declaradas da seguinte forma:

const PI = 3.1416;

/*************
    Variáveis
***********/

No ecma6 as inicialização de uma variável passa a ser let em substituição ao var.