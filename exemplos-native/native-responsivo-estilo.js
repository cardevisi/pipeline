@media screen and (min-width: 320px){
	/*.native-ad-chapeu-a {
  		display: block !important;
  		background: #1a1a1a !important;
  		font-size: 8px !important;
  		line-height: 18px !important;
  		color: #fff !important;
  		text-align: center !important;
  		position: absolute !important;
  		top: 3px !important;
  		left: 3px !important;
  		border-radius: 4px !important;
  		padding:0 5px !important;
	}*/
	.native-ad-chapeu-a {
		display: none !important;
	}

	.native-ad-chapeu-b {
		display: block !important;
		background: #1a1a1a;
		font-size: 11px;
		line-height: 18px;
		color: #fff;
		text-align: center;
		border-radius: 4px;
		padding: 2px 0px;
  	width: 128px;
    margin: 5px 0px 5px 120px;
    height: 20px;
    font-style: normal !important;
	}
}

@media screen and (min-width: 768px){
	.responsive-native-timeline .placeholder {
		max-width: 270px !important;
		max-height: 180px !important;
		display: block !important;
		overflow: hidden;
	}
	.native-ad-chapeu-a {
  		display: block !important;
  		background: #1a1a1a !important;
  		font-size: 11px !important;
  		line-height: 18px !important;
  		color: #fff !important;
  		text-align: center !important;
  		position: absolute !important;
  		top: 3px !important;
  		left: 3px !important;
  		border-radius: 4px !important;
  		padding:0 10px !important;
	}

	.native-ad-chapeu-b {
		display: none !important;
	}
}

.native-ad-chapeu-a,  {
  display: block;
  background: #1a1a1a;
  font-size: 11px;
  line-height: 18px;
  color: #fff;
  text-align: center;
  position: absolute;
  top: 3px;
  left: 3px;
  border-radius: 4px;
  padding:0 10px;
}

.native-ad-chapeu-b {
	display: none;
}

.italic {
	font-style: italic;
}

.AdBox .AdText {
  color: #4c4c4c;
  font-style: italic;
}

<a href="%NATIVE_URL%" class="thumbnail media AdBox" title="%NATIVE_DESCRIPTION%" target="_blank">
  <div class="thumbnail-image media-default media-small">
      <figure class="no-adapt crop-300x200 figure">
          <div class="image ">
              <div class="placeholder">
                  <img src="%NATIVE_IMG%" data-src="%NATIVE_IMG%" alt="%NATIVE_DESCRIPTION%" data-crop="{"xs":"615x300", "sm":"300x200"}" width="300" height="200" data-crazyload="loaded" class="loaded AdImage"> 
                  <i class="placeholder-mask" style="padding-bottom: 66.66666666666666%"></i>
              </div>
          </div>
      </figure>
        <span class="native-ad-chapeu">Conteúdo Publicitário</span>
        <span class="media-position">
          <span class="icon-default"></span>
        </span>
    </div>
  <div class="thumbnail-text italic">
    <span class="native-ad-chapeu">Conteúdo Publicitário</span>
    <h5 class="h-headline">%NATIVE_TITLE%<i class="icon-single-arrow-right"></i></h5>
    <p class="p-headline thumbnail-caption AdText">%NATIVE_DESCRIPTION%</p>
  </div>
</a>