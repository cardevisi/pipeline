S2s pixel (server to server)
Server pixel works absolutely different. There is no need to place it to any page and fire it. This method works by server communication. Each time the advertiser counts the conversion completed the advertiser’s server requests PropellerAds server link and passes the information about the conversion along with the special parameter value which helps to understand what the source of the conversion was for optimization purposes.
To be able to put the parameter value and pass it back we need to add the parameter holder to the Landing page. For example if the campaign tracking link (our unique LP) is http://lp.imesh.com/?appid=1158&subid   and our postback subid parameter is SUBID (recommended), then the link in use will be: http://lp.imesh.com/?appid=1158&subid=SUBID
Each time our user clicks on the banner and goes to the LP (or comes to LP directly) our server automatically generates the unique value for SUBID parameter, for example  http://lp.imesh.com/?appid=1158&subid=12345,  successful!
http://lp.imesh.com/?appid=1158&subid=12346, 
http://lp.imesh.com/?appid=1158&subid=12347
In case of successful conversion the advertiser’s server  calls PropellerADs adserver  at following address (which we call s2s pixel):  ad.propellerads.com/conversion.php?id=SUBID
and advertisers adserver passes the unique SUBID parameter value in an appropriate parameter holder SUBID
ad.propellerads.com/conversion.php?id=12345
which means that only 12345 user made the conversion, but the rest users never completed it.
Thus we can understand which source and ad zone the conversion came from and will be able to optimize our work.
