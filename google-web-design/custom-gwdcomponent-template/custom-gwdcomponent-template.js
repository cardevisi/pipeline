if (document.registerElement) {
    var proto = Object.create(HTMLElement.prototype, {
        
        createdCallback: {
            value: 
            function() { 
                console.log('createdCallback');
            }, enumerable: true
        },

        attachedCallback: {
            value: 
            function() {
                console.log('attachedCallback');
            }, enumerable: true
        },

        attributeChangedCallback: {
            value: 
            function(attributeName) {
                console.log('attributeChangedCallback');
            }, enumerable: true  
        }
});
    document.registerElement('gwd-component-template', {prototype: proto});
}