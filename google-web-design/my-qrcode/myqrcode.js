/**
 * @fileoverview Implementation of my-qrcode component. 
 * @author ksubramanian@google.com (Kishore Subramanian)
 */
if (document.registerElement) {

  var proto = Object.create(HTMLElement.prototype, {

    createdCallback: {
      value:
      function() {
        this.img = null;
      },
      enumerable: true
    },

    attachedCallback: {
      value:
      /**
       * Lifecycle callback that is invoked when this element is added to the
       * DOM.
       */
      function() {
        this.generateImage();
      },
      enumerable: true
    },

    attributeChangedCallback: {
      value:
      /**
       * Lifecycle callback that is invoked when an attribute is changed.
       * @param {string} attributeName Name of the attribute being changed.
       */
      function(attributeName) {
        if (!this.img) {
          // It is possible that the attribute is set before before the
          // component is added to the DOM.
          return;
        }
        switch (attributeName) {
          case 'data':
            this.generateImage();
            break;
        }
      },
      enumerable: true
    },

    generateImage: {
      value:
      function() {
        var data = this.getAttribute('data');
        if (data) {
          if (!this.img) {
            this.img = document.createElement('img');
            this.img.style.height = '100%';
            this.appendChild(this.img);
          }
          this.img.setAttribute('src', QRCode.generatePNG(data));
        }	
      }
    }
  });

  document.registerElement('my-qrcode', {prototype: proto});
}
