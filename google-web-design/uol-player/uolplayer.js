if (document.registerElement) {
    var proto = Object.create(HTMLElement.prototype, {
        
        createdCallback: {
            value: 
            function() { 
                this.generatePlayers();
            }, enumerable: true
        },

        attachedCallback: {
            value: 
            function() {}, enumerable: true
        },

        attributeChangedCallback: {
            value: 
            function(attributeName) {
                switch (attributeName) {
                    case 'videoId':
                    break;
                }
            }, enumerable: true  
        },

        createOverlayPreview: {
            value:
            function(player){
                var that = this;
                var overlayDiv = document.createElement('div');
                overlayDiv.className = "youtube-overlay";
                overlayDiv.style.cursor = 'pointer';
                overlayDiv.style.position = 'absolute';
                overlayDiv.style.top = '0';
                overlayDiv.style.left = '0';
                overlayDiv.style.width = '100%';
                overlayDiv.style.height = '100%';
                overlayDiv.style.background = "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc4NCcgaGVpZ2h0PSc2MCc+PHBhdGggZD0nTTg0LjE1LDI2LjR2Ni4zNWMwLDIuODMzLTAuMTUsNS45NjctMC40NSw5LjRjLTAuMTMzLDEuNy0wLjI2NywzLjExNy0wLjQsNC4yNWwtMC4xNSwwLjk1Yy0wLjE2NywwLjc2Ny0wLjM2NywxLjUxNy0wLjYsMi4yNWMtMC42NjcsMi4zNjctMS41MzMsNC4wODMtMi42LDUuMTVjLTEuMzY3LDEuNC0yLjk2NywyLjM4My00LjgsMi45NWMtMC42MzMsMC4yLTEuMzE2LDAuMzMzLTIuMDUsMC40Yy0wLjc2NywwLjEtMS4zLDAuMTY3LTEuNiwwLjJjLTQuOSwwLjM2Ny0xMS4yODMsMC42MTctMTkuMTUsMC43NWMtMi40MzQsMC4wMzQtNC44ODMsMC4wNjctNy4zNSwwLjFoLTIuOTVDMzguNDE3LDU5LjExNywzNC41LDU5LjA2NywzMC4zLDU5Yy04LjQzMy0wLjE2Ny0xNC4wNS0wLjM4My0xNi44NS0wLjY1Yy0wLjA2Ny0wLjAzMy0wLjY2Ny0wLjExNy0xLjgtMC4yNWMtMC45LTAuMTMzLTEuNjgzLTAuMjgzLTIuMzUtMC40NWMtMi4wNjYtMC41MzMtMy43ODMtMS41LTUuMTUtMi45Yy0xLjAzMy0xLjA2Ny0xLjktMi43ODMtMi42LTUuMTVDMS4zMTcsNDguODY3LDEuMTMzLDQ4LjExNywxLDQ3LjM1TDAuOCw0Ni40Yy0wLjEzMy0xLjEzMy0wLjI2Ny0yLjU1LTAuNC00LjI1QzAuMTMzLDM4LjcxNywwLDM1LjU4MywwLDMyLjc1VjI2LjRjMC0yLjgzMywwLjEzMy01Ljk1LDAuNC05LjM1bDAuNC00LjI1YzAuMTY3LTAuOTY2LDAuNDE3LTIuMDUsMC43NS0zLjI1YzAuNy0yLjMzMywxLjU2Ny00LjAzMywyLjYtNS4xYzEuMzY3LTEuNDM0LDIuOTY3LTIuNDM0LDQuOC0zYzAuNjMzLTAuMTY3LDEuMzMzLTAuMywyLjEtMC40YzAuNC0wLjA2NiwwLjkxNy0wLjEzMywxLjU1LTAuMmM0LjktMC4zMzMsMTEuMjgzLTAuNTY3LDE5LjE1LTAuN0MzNS42NSwwLjA1LDM5LjA4MywwLDQyLjA1LDBMNDUsMC4wNWMyLjQ2NywwLDQuOTMzLDAuMDM0LDcuNCwwLjFjNy44MzMsMC4xMzMsMTQuMiwwLjM2NywxOS4xLDAuN2MwLjMsMC4wMzMsMC44MzMsMC4xLDEuNiwwLjJjMC43MzMsMC4xLDEuNDE3LDAuMjMzLDIuMDUsMC40YzEuODMzLDAuNTY2LDMuNDM0LDEuNTY2LDQuOCwzYzEuMDY2LDEuMDY2LDEuOTMzLDIuNzY3LDIuNiw1LjFjMC4zNjcsMS4yLDAuNjE3LDIuMjg0LDAuNzUsMy4yNWwwLjQsNC4yNUM4NCwyMC40NSw4NC4xNSwyMy41NjcsODQuMTUsMjYuNHogTTMzLjMsNDEuNEw1NiwyOS42TDMzLjMsMTcuNzVWNDEuNHonIGZpbGw9JyNDQzE4MUUnIGZpbGwtb3BhY2l0eT0nLjUnPjwvcGF0aD48cG9seWdvbiBwb2ludHM9JzMzLjMsNDEuNCAzMy4zLDE3Ljc1IDU2LDI5LjYnIGZpbGw9JyNGRkYnPjwvcG9seWdvbj48L3N2Zz4K')";
                overlayDiv.style.backgroundRepeat = 'no-repeat';
                overlayDiv.style.backgroundPosition = 'center center';
                overlayDiv.style.backgroundColor = 'rgba(255,255,255, 0.3)';
                overlayDiv.addEventListener('click', function () {
                    this.style.display = 'none';
                    if (player) {
                        this.style.display = 'none';
                        try {
                            player.unMute();
                            player.seekTo(0, true);
                            that.previewMode = 'desativado';
                        }catch(e){}
                    }
                });
                return overlayDiv;
            }
        },

        trackingByParams: {
            value: 
            function (urls, value){
                var that = this;
                that.debug = this.getAttribute('debug');

                if (that.previewMode == 'ativado') {
                    return;
                }

                for (var key in urls) {
                    var path = urls[key].replace('%%CACHE_BUSTER%%', new Date().getTime());
                    if(that.debug === 'ativado') {
                        console.log('[UOL RICHMEDIA TRACKING]', value, path);
                    } else {
                        var image = new Image();
                        image.src = path;
                    }
                }
            }
        },

        progressUpdate: {
            value: 
            function(event) {
                var that = this;
                that.playerTotalTime = event.target.getDuration();
                that.updateTimer = setInterval(function() {
                    that.playerCurrentTime = Math.floor(event.target.getCurrentTime());
                    that.playerTimePercent = Math.floor((that.playerCurrentTime / that.playerTotalTime) * 100);
                    if (that.currentTimePercent === undefined || that.currentTimePercent !== that.playerTimePercent) {
                        switch(that.playerTimePercent) {
                            case 25:
                                that.trackingByParams(that.getEvents().firstQuartile, 'firstQuartile');
                            break;
                            case 50:
                                that.trackingByParams(that.getEvents().midpoint, 'midpoint');
                            break;
                            case 75:
                                that.trackingByParams(that.getEvents().thirdQuartile, 'thirdQuartile');
                            break;
                            case 100:
                                that.trackingByParams(that.getEvents().complete, 'complete');
                            break;
                        }
                        that.currentTimePercent = that.playerTimePercent;
                    }
                }, 1000);
            }
        },

        getEvents : {
            value: 
            function () {
                var that = this;
                that.playId = this.getAttribute('playing');
                that.muteId = this.getAttribute('muted');
                that.unMuteId = this.getAttribute('unMute');
                that.resumedId = this.getAttribute('resumed');
                that.pausedId = this.getAttribute('paused');
                that.firstQuartileId = this.getAttribute('firstQuartile');
                that.midpointId = this.getAttribute('midpoint');
                that.thirdQuartileId = this.getAttribute('thirdQuartile');
                
                var events = {
                    'mute': [
                        'http://t5.dynad.net/pc/?dc='+that.muteId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'unMute': [
                        'http://t5.dynad.net/pc/?dc='+that.unMuteId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'resumed': [
                        'http://t5.dynad.net/pc/?dc='+that.resumedId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'playing': [
                        'http://t5.dynad.net/pc/?dc='+that.playId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'paused': [
                        'http://t5.dynad.net/pc/?dc='+that.pausedId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'firstQuartile': [ 
                        'http://t5.dynad.net/pc/?dc='+that.firstQuartileId+';ord=%%CACHE_BUSTER%%'
                    ],
                    'midpoint': [
                        'http://t5.dynad.net/pc/?dc='+that.midpoint+';ord=%%CACHE_BUSTER%%'
                    ],
                    'thirdQuartile': [ 
                        'http://t5.dynad.net/pc/?dc='+that.thirdQuartile+';ord=%%CACHE_BUSTER%%'
                    ],
                    'complete': [
                        'http://t5.dynad.net/pc/?dc='+that.complete+';ord=%%CACHE_BUSTER%%'
                    ]
                };
                return events;
            }
        },

        generatePlayers: {
            value: 
            function() {
                var that = this;
                that.videoId = this.getAttribute('videoId');
                that.autoplay = this.getAttribute('autoplay');
                that.mute = this.getAttribute('mute');
                that.autohide = this.getAttribute('autohide');
                that.controls = this.getAttribute('controls');
                that.fullscreen = this.getAttribute('fullscreen');
                that.previewMode = this.getAttribute('preview');

                that.event = new Event('build');
                that.updateTimer;
                that.currentTimePercent;

                console.log('that.autoplay', that.autoplay);
                console.log('that.videoId', that.videoId);
                
                window.addEventListener('build', function(e){
                    
                    this.tagName = document.getElementsByTagName('uol-player');

                    for (var i = 0; i < this.tagName.length; i++) {
                        this.id = this.tagName[i].getAttribute('id');
                        this.tagName[i].innerHTML = '<div id="video-'+this.id+'"></div>';
                        
                        that.player = new YT.Player("video-"+this.id, {
                            videoId: that.videoId,
                            width: '100%',
                            height: '100%',
                            playerVars: {
                                'autoplay': ((that.autoplay == 'ativado' || that.previewMode == 'ativado') ? 1 : 0),
                                'autohide': ((that.autoplay == 'autohide') ? 1 : 0),
                                'modestbranding' : 0,
                                'showinfo': 0,
                                'controls': that.controls,
                                'rel': 0,
                                'fs': ((that.fullscreen == 'ativado') ? 1 : 0),
                                'iv_load_policy' :  1
                            },
                            events: {
                                'onReady': function () {
                                    if(that.mute == 'ativado' || that.previewMode == 'ativado') {
                                        that.player.mute();
                                    }
                                    console.log('Ready');
                            },
                            'onStateChange': function(event) {
                                if (event.data == YT.PlayerState.PLAYING) {
                                    that.trackingByParams(that.getEvents().playing, 'playing');
                                    that.progressUpdate(event);
                                } else if (event.data == YT.PlayerState.PAUSED) {
                                    that.trackingByParams(that.getEvents().paused, 'paused');
                                    clearTimeout(that.updateTimer);
                                } else if (event.data == YT.PlayerState.ENDED) {
                                    that.trackingByParams(that.getEvents().complete, 'complete');
                                    clearTimeout(that.updateTimer);
                                } else {
                                    clearTimeout(that.updateTimer);
                                }

                            }
                          }
                        });

                        console.log(that.player);

                        if (that.previewMode == 'ativado') {
                            this.tagName[i].appendChild(that.createOverlayPreview(that.player));
                        }
                    }
                });
                
                that.body = document.querySelector('body');
                that.body.style.background = "#ccc";
                that.tag = document.createElement('script');
                that.tag.src = "https://www.youtube.com/iframe_api";
                that.firstScriptTag = document.getElementsByTagName('script')[0];
                that.firstScriptTag.parentNode.insertBefore(that.tag, that.firstScriptTag);
                
                window.onYouTubeIframeAPIReady = function () {
                    window.dispatchEvent(that.event);
                }

            }, enumerable: true
        }
        
    });
    document.registerElement('uol-player', {prototype: proto});
}