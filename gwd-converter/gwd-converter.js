const fs = require('fs');
const path = require("path");


    var rootDir = './src';
    var dest = './build';
    
    function recursive(dir)  {
        var dirs = [];
        var files = fs.readdirSync(dir).filter(function(file) {
            var paths = path.join(dir, file)
            if (fs.statSync(paths).isDirectory()) {
                dirs.push(paths);
                recursive(paths);
            } 
        });
        return dirs;
    }

    function makeDest(dest) {
        //fs.rmdirSync(dir);
        if (!fs.existsSync(dest)) {
            fs.mkdirSync(dest);
            return true;
        } 
        console.log('Diretorio existente!');
        return false;
    }
    
    function converter (folders) {

        var type;

        for (var key in folders) {
            var currentFolder = folders[key];
            var files = fs.readdirSync(currentFolder);


            //console.log(currentFolder, files);
            
            for (var key in files) {
                var fileName = files[key];
                if (fileName.indexOf('.html') != -1) {
                    var paths = path.join(currentFolder, fileName);
                    var read = fs.readFileSync(paths, 'utf8');
                    var myLines = read.toString().match(/^.+$/gm);

                    console.log("MYLINES", myLines);

                    for (var key in myLines) {

                        console.log(key);
                        
                        var regex = /([^data\-]source|src)\=(\"|\')([.a-z0-9-_]+\.(jpg|png))(\"|\')/g;
                        var result = regex.exec(myLines[key]);
                        
                        if (!result) {
                            continue;
                        }
                        
                        //console.log("RESULT", result);
                        var currentImage = result[3];
                        var pathImage = currentFolder+'\\'+currentImage;
                        
                        console.log('pathImage', pathImage);
                        var image = fs.readFileSync(pathImage);
                        
                        if (currentImage.indexOf('.jpg') != -1) {
                            type = 'data:image/jpg;base64,';
                        } else if(currentImage.indexOf('.png') != -1){
                            type = 'data:image/png;base64,';
                        }

                        var base64data = new Buffer(image).toString('base64');
                        read = read.replace(currentImage, type+base64data);
                    }

                    currentFolder = currentFolder.replace(/src/, '');
                    currentFolder = currentFolder.replace(/\\/, '');
                    var subFolders = './build/';

                    if (!fs.existsSync(subFolders)) {
                        fs.mkdirSync(subFolders);
                    } else {
                        console.log('diretorio já criado');
                    }
                    
                    var file = subFolders+'/'+currentFolder.toLowerCase().replace(/_/g, '-')+'.html'
                    if (!fs.existsSync( file )) {
                        var write = fs.writeFileSync(file, read, 'utf-8');
                    } else {
                        console.log('Arquivo não existe');
                    }
                
                }
            }
        }
    }

    function start() {
        makeDest(dest);
        var folders = recursive(rootDir);
        converter(folders);
    }


    start();