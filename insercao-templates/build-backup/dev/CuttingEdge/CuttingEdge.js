(function(){

  var prefixed = ["transition", "transform"];
  var prefixes = ["webkit", "moz", "ms", "o"];

  function DOM() {

  };

  DOM.prototype.findParentNode = function() {
    return Boolean(window.frameElement) ? window.frameElement.parentNode : document.currentScript.parentNode;
  };

  DOM.prototype.findParentWindow = function() {
    return Boolean(window.frameElement) ? window.parent : window;
  };

  DOM.prototype.createElement = function(tagName, properties) {
    var element = document.createElement(tagName);
    for(var p in properties) { element.setAttribute(p, properties[p]); }

    return element;
  };

  DOM.prototype.prefixStyles = function(styles) {
    for(var s in styles) {
      if(prefixed.indexOf(s) >= 0) {
        for(var p in prefixes) {
          styles["-"+prefixes[p]+"-"+s] = styles[s];
        }
      }
    }
  };

  DOM.prototype.applyStyles = function(element, styles) {
    this.prefixStyles(styles);
    for(var s in styles) { element.style[s] = styles[s]; }
  };

  DOM.prototype.addClass = function(element, _class) {
    element.className = element.className + " " + _class;
  };

  DOM.prototype.removeClass = function(element, _class) {
    element.className = element.className.replace(new RegExp("\\s?"+_class+"\\s?"), "");
  };

  DOM.prototype.createIFrame = function(options) {
    var iframe = this.createElement("iframe", {
      "border": 0,
      "scrolling": "no"
    });

    this.applyStyles(iframe, {
      "width": options.width+"px",
      "height": options.height+"px",
      "border": "none",
      "background": "url(data:image/gif;base64,R0lGODlhIAAgAKUAAPyOBPzKhPzmxPyuTPz25PzarPyeJPy+bPzu1PymNPzivPzGfPzSnPzq1Py6ZPz+/PzevPzCfPzy5PyqRPyWHPzOlPzqzPz69PzetPyiNPzCdPzy3PyqPPzKjPzmzPyyVPz27PzatPyeLPy+dPzu3PymPPzixPzGhPzWpPy6bPyaHP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJCQArACwAAAAAIAAgAAAG/sCVcEgkXg6jS3HJbAoFg4HHSRVaDgyiKTodLkqC6hDlcDSGUOlQkcl0xEJJOfIQbtWrxyCTIMGFZA4QT1xCDG1vfysXKQ4HSndTIBwZHEpOJBgERAplKCtXB2cLbZ9DDREKRCcLJxiXDxEpBUUPHQkfdSsgGhQUBkQFC8MdAnUkm0wkZw8MIr4UB0UkHQsRJxUSYhYl0AmqSw8mAa0gYg0qvxW6TRcFGH8RGsmKYuxFFxIb+vz1ChghAGLAAKJABQYHD9JSlAGAw4cZDCKcuPBPw4cOReTjJ4GENkUKCoRAEaKkuXooiTywcAZOAwv3wmUKeInKBYEkYgohoCBgfQgFJ6mA+BcQaBELAd9tEAKiJj5zDzIJtEBkIIYGuh78a0mkAQYTQ1YKJEKggdOZZ4aaMIcUg58hFywEZXJzYB0JADfVxaCTitcQbwkETEYiIFcxF4rqEqxJiFaafyxgKBAUL+Uhlg9TIXCV7GAiFiDQS8l4dMpwCkz0ZRIEACH5BAkJACgALAAAAAAgACAAhfyOBPzKhPzmxPyuTPzarPz25PyeLPy+bPzu1PzSnPzivPy2XPymPPyWFPzOlPzevPz+/Pzy5PzOjPzq1PzetPz69PzGfPzy3Py6bPyqPPySFPzKjPzqzPyyVPzatPz27PyiNPzu3PzWpPzixPy6ZPyWHPzGhPyqRP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+QJRwSCRWTKZKcclsCjkk0sRJFSJMHiJUStx0ONUhxWIJDQWkxVQoGAwSYeHHYnJAnhguCkIadC5xQh5kAmxRawRucIEoR3RKEwsYUx8dfkpOEQIfRAImFlkTn2YSbhREIRuFQwkOCQKYEA4mp0QQCR0YdygfASAgGUQKrQkJE3cRnEwRCHsiJ78gJkUXIsUJIspUEx3RHQpMEBzE2k4hDCAMIrtNFSOrYQEm5Yz1QxAFH/n59FUjCg8AKlDwQYGHgwcpjKh3okGDEg5LDBhBoSIFAgoZQtzY4ASECAVA6utH5V/AgATtLWFXRdyaMHlI3gtBwQMFTNsAANAgASeCkQIGa6asIsCATgAGag3hYBMjIF4+jXCCsEHD0QVELB4TAkEBhZdDJmQUUoCETg0/J/ik6YGSghGcmFIwsxSDUiYVLCKrWaCR3npiPdAtYLMvihA2wVapYFPBLsIUDHe1GdUJh4vaImAeopmAYicFvv4srOWBYZUoIJ9GvfItSypBAAAh+QQJCQAvACwAAAAAIAAgAIX8jgT8yoz8rkz85sT8vmz8niT89uT82qz87tT8lhT8tlz8xnz84rz8pjz8khT80pz86tT8wnz8ojT8/vz83rz88uT8umz8kgz8slz86sz8wnT8oiz8+vT83rT88tz8mhz8umT8jgz8zpT8slT85sz8vnT8niz89uz82rT87tz8lhz8tmT8xoT84sT81qT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/sCXcEgkckQPTnHJbAoRi0XKSRV6Hi0ihBWZDh8ESHXYeogqQ0jU+8qAQK6x8PR4uCZCtVQ40bwNckJlImIvUF1CHSArB4FCHHVJL1t7JwQgJUpOBhCaeXUMLykiIh4vD4uhQx4iGUQdKB0QeC8TLg8DRbYWLLRHAgIKRBmwHR2mLycnTQamEwcYwAIiRQYMsSgMy1UQFtIWJEwTKcYdnk4eIwIjHbRNHBCFY6TnjvZDEwYn+vrbgSQMWgQMyOEaioOxsjhSIKGhQwUtynU40EFhoBESNmTMqGBCBQMf9/mTA1CgyXr3agWakEFelQgaRi4ZVwzlkhQfEhR44I6IiDVs2saQaKBCRYIGqoZkiEUR2QmbhsJNeFDAqAoNr4zN4sNAlq4SADbQOqHBqAmfnYiQQyHmRMBlCgAAoJYmQlImHMrhqQALUIULABzI5AbLi4FYgF6wkEvAEYdYDGgd7pCYwwe5LqkQO+CPL+chKORaCGTA65DJiYVguNAh5WnErp1MCNiTShAAIfkECQkAKAAsAAAAACAAIACF/I4E/MqE/ObE/K5M/Pbk/J4k/Nqs/O7U/L5s/KY0/NKc/OK8/OrU/Lpk/P78/PLk/MZ8/KpE/JYc/M6U/OrM/Pr0/KI0/N68/PLc/Ko8/MqM/ObM/LJU/Pbs/J4s/N60/O7c/L50/KY8/Nak/OLE/Lps/MaE/Joc////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABv5AlHBIJFYMBkdxyWwKMRPFw0kVEj4MIlSBIRpMoOqQYfh0hiDFZCpkmCAfsfD4WShRW7ZjAjGd5SgMHwZhKA9RXSgkEBAGgEIOH5JKaWsoFW8mFVQEDJtoZVkPSFMGfRtEDyNZQ4NYdw4kWEWRJhN3FSMNDSFEgmUXiR2fSx1TDgsIuw0KRQQLZQYkf1QgEMsQrLQgF5LETg+7JRd3TRUM2lUjI9+P7kMOBB3y8tRyDAIb+fkV0EhIH0i4QzCgoEEEsrpdKCPwEUGDBUM4eGCsg0V7YvDp29junTsHFNJR0eCHigMQrjouwZDAQoYR5Yg8izYtIwcLODkIKEIh2nkFNsOagKCAwsEIETgtmCDS7RWkBbOIOIBwIsKdDgFwZpDpiQhKA1ku4FyAIoQECc3QBGhoTtIHJQYAAHDU4YQEDxir/CoUd66QCWchPKJD4k5fR0UTSDhxANAvaiPkIkbxITCgK+kOE0FwgqxHIZo/N+ngwULeJkEAACH5BAkJACsALAAAAAAgACAAhfyOBPzKhPzmxPyuTPzarPz25PyeLPzCdPzu1PzSnPy2XPzivPymNPyWHPzOlPzq1PzevPz+/Pzy5PyqRPySFPzOjPzqzPyyXPzetPz69PzGhPzy3Py6bPyqPPySDPzKjPzmzPyyVPzatPz27PyiNPzGfPzu3PzWpPy6ZPzixPymPP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+wJVwSCRGFqlIcclsCiUYUcFJFRYwD2JBhJkOFwlJdfjgeldQ6dCUcAjGwgwBs1Cutl1h5JRIjOBCD1EmVmaBDm6AehiMSmlTGX0Jdk0FDxlEJnNZIwsLfwsOCVlDV4RDUVh2R1hLAgkndhkYGgcfRBapGBtCI5hMI38RIBolBxoiRQULXCKfYxIOJSUaDqdFESaMGL9UBbUaIJRMGQ+kY4zdiutGBSPu7n+KCA8W9eYZzCL7XCnrGigCCtSQYhuGOf4UlUChgCFDDREkFJD4Th4gcxjxsds4JII9QIjULcmWSiQTCSEGhBAxrhSzKM+qIEAxoCYHEEUscJnDa4WCgGtFNmSJIEJBzQEOiGx7sMoAgAPYPqgIYWdEgpoKtFwi8gEAABQrFkyY4E8DCRIEMjnA6WSEBwAe/mBo0GDBHRUkJpikgsLrrRVz6wpJcDaAogdeDfwKbHdFhgEkGADl6zWZkLkeGoc9+3eMCA9ZUdHVvOJAh4QcGXNs22GCxSpBAAAh+QQJCQAoACwAAAAAIAAgAIX8jgT8yoz85sT8rkz89uT82qz8vmz8niT87tz84rz8lhT8tlz8xnz80pz86tT8/vz83rz8pjz86sz8+vT83rT8wnT8ojT88tz8miT8umz8kgz8zpT85sz8slT89uz82rT8vnT8niz84sT8lhz8umT8xoT81qT88uT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/kCUcEgkPhKiR3HJbApPlA/BSRUSKA4i4UOZDiUUT3Xo4HpRUOnwxM2OUZMCJaFEbbvCI4UyeQsdUQhWZkIIHx9ufg97FEppUxMQe3VNBA59QwhyWR4JCWISXIJDHiIXRFFYdXqJZHNDEwINGyZEYFwUpygemEsTYg8ODbMNCUUECVwfn2MeJsMbBaNFDwiMvU4exA6UTBMOrVQiSX7lTg8EHunpYuUI7/AIE8mHhxQi5hsM+/wbIowU5OArp49BhRIHGzw4QYChunZ+4sHDZsTcGwILQPgxYYIitQAaAACY5oRABhIG6DApcEAkgBAcxiCoQILEghLhFrgEsEEJhQeSRE4IOgKiJolaQ0ICyNDuQQQMJag16JChzgQTNjUO+UCi1YYRCiqgELCggwQUAQYMoBDURLgiHkIoCCEmgQUL+Dx0GLDAI5UKYBsIsYtXSAG1gv04ABuhDmFjKB6QGNBB1xjACiCjeDxEAGI/FA5oHXxXM9oOMS0OISxANRUPZf0yCQIAIfkECQkAKQAsAAAAACAAIACF/I4E/MqE/ObE/K5M/Nqs/Pbk/J4k/L5s/O7c/OK8/KY8/NKc/MZ8/JYc/OrU/Lpk/N68/P78/M6U/OrM/N60/Pr0/KI0/MJ8/PLc/Ko8/Joc/MqM/ObM/LJU/Nq0/Pbs/J4s/L50/OLE/Nak/MaE/Lps/PLk/KpE/Jok////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABv7AlHBIJEYSokhxyWwKTRRPwUkVFigOYsFDmQ4nlE916OB6U1Dp0MTNjlMVAiWhTG27wiOFUnkLHVEIVmZCCB4ebn4RexRKaVMVEHt1TQUOfUMIclkfCQliE1yCQxUOZylRWHV6iWRzQxGhe0RgXBQYQh+YSxViEQh7XBNFBQlcHp9jnakJp7DAe7tOH6kIlEylrVQOE9d+30XUqVwJ4CYmGOjoESAA7u8W4AQLEvT0BO3v7vHfBPX/Cwh88ECA4KFy386lS2etiTdwTT4cYOAnGpUIEgw00ICrygcSFwIIeIhKQYOTCoZVQSCBAUgJo4YcONnAwAIlDjouKSAoAoIHEgyCeiCCYuMFTBE6KJBQJMKIEiTqVKAAdAMRChdiphhhwUKAFBwOHMgy4sEDhE8oaF3yQYEFBWIEDBjAIcWHEg9CSBsToOsIIXLpCqFglsA3BF071BExt26KCBfMOnNCoquAIYwFC5lg9u+bBBkoDgnsWMiCsRCJkE5NRaJeP0EAACH5BAkJAC0ALAAAAAAgACAAhfyOBPzKhPzmxPyuTPyeJPz25PzarPy+bPzu1PymNPy2XPzivPyWFPzSnPzq1Pz+/PzevPzGfPzy5PyqRPySDPzOlPzqzPyyXPyiNPz69PzetPzy3PyqPPy6bPyODPzKjPzmzPyyVPyeLPz27PzatPy+dPzu3PymPPy6ZPzixPyWHPzWpPzGhP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+wJZwSCQ+FqlHcclsCiUaUsFJFRY0DmKBpJkOLZpRdejgeltQ6VDCzY5bGYNmoWxtu8KjRpN5Cx1RJlZmQiYkJG5+D3saSmlTGRB7dU0kB4kthlh2KQtiFlyCQxkOZy0UAAAdYi16mH9zQw+ge0QKqQAUFUojfUwZYg8me1wWRQYEuCICYyMLUXOmoyweqQjN0CaUTAUKJX4OFttU435EzhAL6RDM5iMF7/EPEwwqDPUMA+YtKYf+BilO2BuYb98zRntSoFOnrp2fERIKRIxYLs++NyNYfPCTIoWvJg9WnMBwQkKzBigdVEwxAIPLEK+4rUDZYIVJIhFcYuBgQAmEgptLILay0KACygVERiYIwOoBihANiixisSvPgqIriCz4IEqIgQEDKrRwwILFNRIRIoA4l2IDlQwhBlzoYwEFiiwZWET48PFNBbAGhIBAocBNirQazG0A26FO3bt5KkRgwWrM3wFrBdtN5ACxHwEXxH4hjMkAi64XWzyOmZpIXhZ9qQQBACH5BAkJAC0ALAAAAAAgACAAhfyOBPzOjPyuTPzqzPyeJPzetPy+bPz25PyWHPzWpPy2XPzy3PzmxPymPPzGfPySFPzu3PzixPzCfPz+/Py6bPySDPzSnPzu1PyiNPzivPzCdPz69PyaHPzatPy6ZPyODPzOlPyyVPzq1PyeLPzevPy+dPz27PzarPy2ZPzy5PzmzPyqRPyaJP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+wJZwSCROMpFJcclsClOFzsFJFXYqHuKhU5gOBwVTdegBAApDqDTNFY2FIjNB2VJ7j4XC5i0sAwJPUV4QHR1ufC0mFQAVYnYtG3kFdE0kEhdEAWZZJkhiA1wQRBsiXkIECBwSYpAsHxpLIgURQxOgeUQUCLsEFkoMokwbYhMQeVwDRQUNuwgryVWdUQUZpkYBqBzBVJGhlEwHBg58IgPfVOeIQxsRSO4q6iYH8vQTIRj4+QrqEVz+1Pfy4duHKEOBEwejRNiQoaFDeIjozTtwIJ26eCAskDOH7kQIASFSjOlWAIJFFR4EqKSAKZpBLhlYDQmgUoCCSS0gyCxigph7sWnQhHwUYIHVBAcUTiyJYOEEHVIHSRBRAULkEBIePCTICQLEghYZMh4SYuLCziUbDHigsEeEAweiNlgAkWAPogRZpba48DbYgIy0+KTIKoGOWwnBJiSwUJQPXg9j3cIdAgEwnwEUNA7hO3lIBgtWL8JxgFg0t4x2xwQBACH5BAkJACsALAAAAAAgACAAhfyOBPzKhPzmxPyuTPz25PzarPyeJPy+bPzu1PymNPzivPzGfPzSnPzq1Py6ZPz+/PzevPzCfPzy5PyqRPyWHPzOlPzqzPz69PzetPyiNPzCdPzy3PyqPPzKjPzmzPyyVPz27PzatPyeLPy+dPzu3PymPPzixPzGhPzWpPy6bPyaJP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+wJVwSCSCMhlQcclsCgsAAMZJFUIMIyIUECJaMMqqUEOhKIbbrpAQwjTEwka59BCiouqHon2Bj8sMT3hCJBgFb34rIAYUBkp3XCsXGJR1TiYBJEQMZRorECIiZxZtmkMXDQREJRkJHX0rDwkqC0sNGGdCDw0FlEQLSBkTBXUCpksXSg+FGG0WRQoDwR+IVCAKzbiqSw8MHK0bYiDZJJZNIAsdfg0W5lXuiUQXHgL09NVwIAT6/A8HAwADekpkoo1BXCkCCoy3x5BDE/PqScQnjsC+i/DiabzAoMC6dlQeYDjgIMU2KpNKZVzRIIKDlxHCVbmWTUGYIQxeOjhgoo57hJtFksVi5owISQcoYD2ocGJKkVsmhjz40sYLCglEBCxY0EVCgQJYqcoUAgLBSSYXTqjts6ECg3CTMEBY6QTDVgGEGFTAuoJEUT/oIlSw1JYBXz29YIkJseDEMRJ6+a6Q0IyiEwQnPA6R4Hbsii9ANRb2rBFtgRB0lwQBACH5BAkJACwALAAAAAAgACAAhfyOBPzKhPzmxPyuTPyeJPz25PzarPy+bPzu1PyWFPymPPzivPzSnPy2XPySFPzq1Pz+/PzevPzGfPzy5PySDPzOlPzqzPyiNPz69PzetPzy3PyqPPy6ZPyODPzKjPzmzPyyVPyiLPz27PzatPy+dPzu3PyWHPzixPzWpPy2ZPzGhPyqRP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb+QJZwSCSKVgNRcclsCjMJ08JJFZ42KmLGlJgOG45Mdai6XATDUdTLMgAAh7GwdAmBIE+uF0N4P+RCAWYoT2tCKm8cgEIiChcrSltdLBMUAA5KTh8MGkQoZgEsCysraClvFUYPmUIgAwMMGEIQIAqpRBAHAAR4LBAPGSNiQx6vAw0ZeBYlTSUfviXBGRkWRQIcxhzMVSIL0gsTTBAGriDh3NIlvU0iFbdjDxbri/REGPH4y/QiBfz+EBI4CByYZdEJYQgzLFAxkEMKDqEWeTOQgaKBExgs5HuAYJ+/fgXm1auHgSKgBw9EFoFwIoAKFaycYKiYQR2TEhUk6KxwjkqFt28x2+iUoOIDHhGymGBQAiGatGpkiI5ICgEFAzRFgLG5J40IghEFiDxgUOEEi24nlFgQtk1IxqBFMDCYK6uAsLAzhamkcoJsR0rBwrKINuIPIBFkDfSaEHiWtwxJx/St0EkIY7BDJowwYHiMBgZmh9jNIFiIhQilR45OPXIJhAUn9jIJAgAh+QQJCQArACwAAAAAIAAgAIX8jgT8zoz86sz8rkz83rT8vmz8niT89uT81qT88tz8tlz85sT8lhT8xnz8pjz87tz84sT8/vz80pz87tT8slz84rz8wnz8ojT8+vT82rT8umz8mhz8khT8zpT86tT8slT83rz8vnT8niz89uz82qz88uT8umT85sz8lhz8xoT8qkT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG/sCVcEgkjhQKTHHJbAorlwvESRWePh0iVEosGCrVYWcwOA23YCEBxbCEhQmyJvKMpiMOBurxFo4HJHUXaR16DX1CGB8DFEpoKyMGKAYjVB4IJUQEZFkLHx9mBWwIRAIaBF0mJghKKxEmHxJFEQ0bDnSQGgAAHEQSqiYhFXQTmUwPHq4dHLsACkUeFsApfFULBs0bGUwRBBomGgdhHs0drU0YEoFv38aIYbhLGA8T9PQJ7yMjB/v7ER0NAqZoYEEWogoZEiqEADCFBYEG+yAkQJFihXn1PCDDh2jfgRIfD8R7RzIRhAV9PAgYuSSCBwkwzznBQCADgQcshZRAALMDhIJKVUZUqEmgAtAzEjrA9EBnhEwiGCpFeEDRpgBfMIcJiTA0GTQCU7YKIEACBJEERolQzZBMKIRKAmxWS+RBnJNuNumUqCmOJsWclihWO2DT7gObXt9gIJFB6wrCBOxytfnUydjIQ/ZmsLtiL4nEVQ4QAO356IqxpklC5lySWwUIgJkEAQA7VTMrY2xkVVZDMmNlYzhjci9zWUIxREZCWk0zbG1uOGlhRUsxME9mMTNMeSt1cmR2c2VRMEdwTUdDV2FvcFVaUg==) center center no-repeat"
    });

    return iframe;
  };

  DOM.prototype.loadIFrameContent = function(iframe, options) {
    if( options.src ) {
      iframe.src = options.src;
    } else {
      iframe.contentDocument.open();
      iframe.contentDocument.write(options.html);
      iframe.contentDocument.close();
    }
  };

  window.DOM = DOM;

})();

(function(){

  function Utils() {

  };

  Utils.prototype.get = function(url, callback) {
    var req = new XMLHttpRequest();
    req.addEventListener("load", function(e){
      callback(req.responseText);
    });
    req.open("get", url, true);
    req.send();
  }

  window.Utils = Utils;

})();

(function(){

  function ClickTag() {

  }

  ClickTag.prototype.send = function(iframe, clicktags) {
    var info = {
      "clicktags": clicktags,
      "target": "_blank"
    };
    iframe.contentWindow.postMessage(JSON.stringify(info), "*");
  };

  ClickTag.prototype.handler = function() {
    var ClickTagHandler = {
      init: function() {
        var self = this;
        window.addEventListener("message", function(event) {
          try {
            var config = self.config = JSON.parse(event.data);
            for(var i in config.clicktags) {
              var ctag = i == 0 ? 'CLICKTAG' : 'CLICKTAG'+(parseInt(i)+1);
              var anchors = document.querySelectorAll('a[href="'+ctag+'"');
              for(var a = 0; a < anchors.length; a++) {
                anchors[a].href = "javascript:window.openClickTag("+i+")";
              }
              window[ctag] = config.clicktags[i];
            }
          } catch(e) {
            console.warn(e.message);
          }
        });
      },
      open: function(i) {
        window.open(this.config.clicktags[i], this.config.target);
      }
    };

    ClickTagHandler.init();
    window.openClickTag = function(i){ ClickTagHandler.open(i) };
  };

  ClickTag.prototype.apply = function(iframe) {
    var script = document.createElement("script");
    script.innerHTML = ClickTag.prototype.handler.toString().slice(13,-1);
    iframe.contentDocument.body.appendChild(script);
  };

  ClickTag.prototype.set = function(iframe, clicktags) {
    if(!Boolean(iframe.src)) { this.apply(iframe); }
    this.send(iframe, clicktags);
  };

  ClickTag.prototype.mask = function(iframe, clicktags, config) {
    var parent = iframe.parentNode;
    var mask = document.createElement("a");
    mask.style.width = config.width;
    mask.style.height = config.height;
    mask.style.position = "absolute";
    mask.style.left = config.left;
    mask.style.top = config.top;
    mask.style.background = "#fff";
    mask.style.opacity = "0";
    mask.href = clicktags[0];
    mask.target = "_blank";
    parent.appendChild(mask);
  };

  window.ClickTag = ClickTag;
})();


(function(DOM, Utils, ClickTag){

  function Banner(config) {
    this.CONFIG = config;
    this.DOM = new DOM();
    this.Utils = new Utils();
    this.ClickTag = new ClickTag();
    this.$sf = window.$sf || false;
  };

  Banner.prototype.init = function() {
    this.createRootContainer();
    this.createFirstFrame();
  };

  Banner.prototype.createRootContainer = function() {
    if(this.$sf) {
      document.open();
      document.write("<style>html,body{margin:0;padding:0;}</style><body></body>");
      document.close();
      this.DFP_ROOT = this.PARENT = document.body;
      this.$sf.ext.register(parseInt(this.CONFIG.default.width), parseInt(this.CONFIG.default.height));
    } else {
      if(this.CONFIG.default.adInAd.toString() === 'true') {
        this.DFP_ROOT = (Boolean(window.parent.frameElement)) ? window.parent.frameElement.parentNode : this.DOM.findParentNode();
      } else {
        this.DFP_ROOT = this.DOM.findParentNode();
      }
      this.PARENT = this.DFP_ROOT.parentNode;
    }

    this.ROOT = this.DOM.createElement("div");
    this.DOM.applyStyles(this.ROOT, {
      "width": this.CONFIG.default.width+"px",
      "height": this.CONFIG.default.height+"px",
      "position": "relative",
      "overflow": "hidden",
      "margin": "0 auto"
    });
    this.DFP_ROOT.appendChild(this.ROOT);

    if(!this.$sf && window.frameElement) {
      this.DOM.applyStyles(window.frameElement, {
        "display": "block",
        "width": this.CONFIG.default.width+"px",
        "height": this.CONFIG.default.height+"px",
        "margin": "0 auto"
      });
      this.DOM.applyStyles(this.ROOT, {
        "position": "relative",
        "margin": "0 auto"
      });
      this.DOM.applyStyles(this.DFP_ROOT.querySelector("iframe"), { "position" : "absolute" });
      this.DOM.applyStyles(this.PARENT, {
        "width": this.CONFIG.default.width+"px",
        "height": "auto",
        "position": "relative",
        "marginLeft": "auto",
        "marginRight": "auto"
      });
    }
  };

  Banner.prototype.createFirstFrame = function() {
    this.FIRST_FRAME = this.DOM.createIFrame({
      "width": this.CONFIG.default.width,
      "height": this.CONFIG.default.height
    });
    this.ROOT.appendChild(this.FIRST_FRAME);

    var banner = this;
    this.FIRST_FRAME.addEventListener("load", function(){
      if(banner.CONFIG.default.mask.enable.toString() == "true")
        banner.ClickTag.mask(banner.FIRST_FRAME, banner.CONFIG.default.clicktag, banner.CONFIG.default.mask);
      else
        banner.ClickTag.set(banner.FIRST_FRAME, banner.CONFIG.default.clicktag);
    });

    this.DOM.loadIFrameContent(this.FIRST_FRAME,{
      "src": this.CONFIG.default.src,
      "html": this.CONFIG.default.html
    });

    if(!this.$sf && window.frameElement) {
      this.DOM.addClass(this.PARENT, "ad-rendered");
    }
  };

  window.Banner = Banner;
})(window.DOM, window.Utils, window.ClickTag);

(function(window){

  function Tracking() {

  };

  Tracking.prototype.track = function(trackers) {
    for(var i in trackers) {
      var img = new Image();
      img.src = trackers[i].replace("%%CACHEBUSTER%%", (new Date()).getTime());
    }
  }

  window.Tracking = Tracking;

})(window);

(function(window){

  function Cookies() {

  };

  Cookies.prototype.get = function(cookieName) {
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(cookieName).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  };

  Cookies.prototype.set = function(name, value, expires) {
    var cookie = name+"="+value;
    cookie += "; domain="+window.parent.location.hostname;
    if(expires) {
      var date = new Date();
      date.setTime(date.getTime() + expires*60*60*1000);
      cookie += "; expires="+date.toUTCString();
    }
    document.cookie = cookie;
  };

  window.Cookies = Cookies;

})(window);


(function(Banner, Tracking){

  function CuttingEdge(config) {
    Banner.call(this, config);
    this.Tracking = new Tracking();
    this.Cookies = new Cookies();
  };
  CuttingEdge.prototype = Object.create(Banner.prototype);

  CuttingEdge.prototype.init = function() {
    if(this.Cookies.get("hide") == "true") return;

    this.createRootContainer();

    if (this.CONFIG.default.mirror) {
      this.MIRROR = this.DOM.findParentWindow().document.querySelector(this.CONFIG.default.mirror);
      if(this.MIRROR) {
        this.DOM.applyStyles(this.MIRROR, {
          "width": this.CONFIG.default.width+"px",
          "height": this.CONFIG.default.height+"px"
        });
      }
    }

    this.createFirstFrame();

    if(this.CONFIG.expand.enable.toString() == "true") {
      this.createSecondFrame();
      this.setupExpansion();
    } else {
      this.createCloseButton();
    }

    this.STATE = "collapsed";
  };

  CuttingEdge.prototype.createSecondFrame = function() {
    this.SECOND_FRAME = this.DOM.createIFrame({
      "width": this.CONFIG.expand.width,
      "height": this.CONFIG.expand.height
    });
    this.ROOT.appendChild(this.SECOND_FRAME);

    var banner = this;
    this.SECOND_FRAME.addEventListener("load", function(){
      if(banner.CONFIG.default.mask.enable.toString() == "true")
        banner.ClickTag.mask(banner.SECOND_FRAME, banner.CONFIG.default.clicktag, banner.CONFIG.default.mask);
      else
        banner.ClickTag.set(banner.SECOND_FRAME, banner.CONFIG.default.clicktag);
    });

    this.DOM.applyStyles(this.SECOND_FRAME, {
      'display': 'none',
      'position': 'absolute',
      'left': '0',
      'top': '0'
    });
  };

  CuttingEdge.prototype.expand = function() {
    this.DOM.applyStyles(this.ROOT, {
      "width": this.CONFIG.expand.width+"px",
      "height": this.CONFIG.expand.height+"px"
    });
    if(window.frameElement) {
      this.DOM.applyStyles(window.frameElement, {
        "width": this.CONFIG.expand.width+"px",
        "height": this.CONFIG.expand.height+"px"
      });
    }
    if(this.MIRROR) {
      this.DOM.applyStyles(this.MIRROR, {
        "width": this.CONFIG.expand.width+"px",
        "height": this.CONFIG.expand.height+"px"
      });
    }
    this.DOM.applyStyles(this.FIRST_FRAME, { "display": "none" });
    this.DOM.applyStyles(this.SECOND_FRAME, { "display": "block" });
    this.DOM.loadIFrameContent(this.SECOND_FRAME,{
      "src": this.CONFIG.expand.src,
      "html": this.CONFIG.expand.html
    });
    this.DOM.applyStyles(this.CLOSE_BUTTON, { "display": "none" });
    this.DOM.applyStyles(this.COLLAPSE_BUTTON, { "display": "block" });
    if(this.EXPAND_BUTTON) this.DOM.applyStyles(this.EXPAND_BUTTON, { "display": "none" });
    if(this.CONFIG.default.tracking.open) this.Tracking.track(this.CONFIG.default.tracking.open);
    this.STATE = "expanded";
  };

  CuttingEdge.prototype.collapse = function() {
    this.DOM.applyStyles(this.ROOT, {
      "width": this.CONFIG.default.width+"px",
      "height": this.CONFIG.default.height+"px"
    });
    if(window.frameElement) {
      this.DOM.applyStyles(window.frameElement, {
        "width": this.CONFIG.default.width+"px",
        "height": this.CONFIG.default.height+"px"
      });
    }
    if(this.MIRROR) {
      this.DOM.applyStyles(this.MIRROR, {
        "width": this.CONFIG.default.width+"px",
        "height": this.CONFIG.default.height+"px"
      });
    }
    this.DOM.applyStyles(this.FIRST_FRAME, { "display": "block" });
    this.DOM.applyStyles(this.SECOND_FRAME, { "display": "none" });
    this.DOM.applyStyles(this.COLLAPSE_BUTTON, { "display": "none" });
    this.DOM.applyStyles(this.CLOSE_BUTTON, { "display": "block" });
    if(this.EXPAND_BUTTON) this.DOM.applyStyles(this.EXPAND_BUTTON, { "display": "block" });
    if(this.CONFIG.default.reload.toString() == "true") {
      this.FIRST_FRAME.src = this.FIRST_FRAME.src;
      this.SECOND_FRAME.src = "";
    }
    if(this.CONFIG.default.tracking.close) this.Tracking.track(this.CONFIG.default.tracking.close);
    this.STATE = "collapsed";
  };

  CuttingEdge.prototype.hide = function() {
    this.DOM.applyStyles(this.ROOT, {
      "width": this.CONFIG.default.width+"px",
      "height": "0"
    });
    if(window.frameElement) {
      this.DOM.applyStyles(window.frameElement, {
        "width": this.CONFIG.default.width+"px",
        "height": "0"
      });
    }
    if(this.MIRROR) {
      this.DOM.applyStyles(this.MIRROR, {
        "width": this.CONFIG.default.width+"px",
        "height": "0"
      });
    }
    this.DOM.removeClass(this.PARENT, "ad-rendered");
    this.Cookies.set('hide', 'true', parseInt(this.CONFIG.default.cookieExpire) || 0);
    if(this.CONFIG.default.tracking.hide) this.Tracking.track(this.CONFIG.default.tracking.hide);
    var self = this;
    window.setTimeout(function(){
      self.ROOT.removeChild(self.FIRST_FRAME);
      if(self.SECOND_FRAME) self.ROOT.removeChild(self.SECOND_FRAME);
    }, parseFloat(this.CONFIG.expand.duration) * 1000);
  };

  CuttingEdge.prototype.setupExpansion = function() {
    if(this.CONFIG.expand.duration) {
      this.DOM.applyStyles(this.ROOT, { "transition": this.CONFIG.expand.duration+"s" });
      if(window.frameElement) this.DOM.applyStyles(window.frameElement, { "transition": this.CONFIG.expand.duration+"s" });
      if(this.MIRROR) this.DOM.applyStyles(this.MIRROR, { "transition": this.CONFIG.expand.duration+"s" });
    }
    if(this.CONFIG.default.event == "hover") this.setupHoverExpansion();
    else this.setupClickExpansion();
  };

  CuttingEdge.prototype.setupHoverExpansion = function() {
    var banner = this;
    var delay = banner.CONFIG.expand.delay ? parseFloat(banner.CONFIG.expand.delay) * 1000 : 0;
    var timer = null;
    this.ROOT.addEventListener("mouseenter", function(e){
      if(timer) window.clearTimeout(timer);
      timer = window.setTimeout(function(){
        if(banner.STATE == "collapsed") banner.expand();
      }, delay);
    });
    this.ROOT.addEventListener("mouseleave", function(e){
      window.clearTimeout(timer);
    });
    this.createCloseButton();
    this.createCollapseButton();
  };

  CuttingEdge.prototype.setupClickExpansion = function() {
    this.createExpandButton();
    this.createCloseButton();
    this.createCollapseButton();
  };

  CuttingEdge.prototype.createExpandButton = function() {
    var banner = this;
    var delay = banner.CONFIG.expand.delay ? parseFloat(banner.CONFIG.expand.delay) * 1000 : 0;

    this.EXPAND_BUTTON = this.DOM.createElement("span");
    this.DOM.applyStyles(banner.EXPAND_BUTTON, {
      "position": "absolute",
      "right": "0",
      "top": "0",
      "display": "block",
      "width": this.CONFIG.default.width+"px",
      "height": this.CONFIG.default.height+"px",
      "cursor": "pointer",
      "background": "transparent",
      "z-index": "2",
      "background": "#fff",
      "opacity": "0"
    });
    this.EXPAND_BUTTON.addEventListener("click", function(e){
      window.setTimeout(function(){
        banner.expand();
      }, delay);
    });
    this.ROOT.appendChild(banner.EXPAND_BUTTON);
  };

  CuttingEdge.prototype.createCloseButton = function() {
    var banner = this;

    this.CLOSE_BUTTON = this.DOM.createElement("span");
    this.DOM.applyStyles(banner.CLOSE_BUTTON, {
      "position": "absolute",
      "right": "0",
      "top": "0",
      "display": "block",
      "width": "55px",
      "height": "15px",
      "cursor": "pointer",
      "background": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAIAAADPkMDRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjVDRTg4NkFEMDFDODExRTY4QzcxOTY3MTNEOUI2NUVCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjVDRTg4NkFFMDFDODExRTY4QzcxOTY3MTNEOUI2NUVCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUNFODg2QUIwMUM4MTFFNjhDNzE5NjcxM0Q5QjY1RUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUNFODg2QUMwMUM4MTFFNjhDNzE5NjcxM0Q5QjY1RUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4kSyxfAAABdElEQVR42uRVPauCYBQ2uUMQbi7a5JTQb1H7Ac2NrYk3mlRwEtx0cXFwcCid+h8FueikLU21ON5Dgbyp972KF7lwn+H1eDi8z+P5ckQQhKIot9vter0SPcAwDJz4SyCGpmld18FerVZpmuZ5jolnWZbjOMdxRoIgEIMjSZJO8WRPvjAMB/iqj0YyURSJvwQSVVZiSAWqqp5OJzhfr5IkwWsQBOPxuJpLfDUruuv+igetzHeeEtvtlqKoxWJxuVwej4emaVmWLZfLoih+Vgn3oiJQsrroipS6enwXbTYbz/NkWQb7fr+v12tU4ptKPAeGrJG+01SBJtM0bdsG2/f9OI6bp6c9fcvBrxcaA2jB3W73smGPHo/HilCy65b59dUDEqHc0+n08wnwuK47m82ac1lPZPgEmtdGZ889ahgGz/P7/f5wOLw8MECWZcGwl935P/49A231yWQyMCUwohu7TeOO4DFw0aMognM+n7eMP5/PXwIMAOhbvLvJ0BFCAAAAAElFTkSuQmCC) center center no-repeat",
      "z-index": "2"

    });
    this.CLOSE_BUTTON.addEventListener("click", function(e){
      banner.hide();
    });
    this.ROOT.appendChild(banner.CLOSE_BUTTON);
  };

  CuttingEdge.prototype.createCollapseButton = function() {
    var banner = this;

    this.COLLAPSE_BUTTON = this.DOM.createElement("span");
    this.DOM.applyStyles(banner.COLLAPSE_BUTTON, {
      "position": "absolute",
      "right": "0",
      "top": "0",
      "display": "none",
      "width": "55px",
      "height": "15px",
      "cursor": "pointer",
      "background": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAIAAADPkMDRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkNEOTc0ODlCMDFDNzExRTY4QTZGQTQ2OEM5NkFEMUE2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkNEOTc0ODlDMDFDNzExRTY4QTZGQTQ2OEM5NkFEMUE2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Q0Q5NzQ4OTkwMUM3MTFFNjhBNkZBNDY4Qzk2QUQxQTYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Q0Q5NzQ4OUEwMUM3MTFFNjhBNkZBNDY4Qzk2QUQxQTYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5o+1GjAAABfUlEQVR42tRVsY6CQBDFyzXE2NEAFZWov+AvaEJpQg2dhM4ApSHQWlLTEsz5C7aUR0eoCI097c25CZkDXRATL75id4YdlsfsvNkRwzCWZV0ul7IsmSfA8zyM9E0ghuM413XB1jQtz/OiKCjxoihKkhQEwWi1WjEvR5ZlD8V/MO+AX5Zff0F/oTNgWLxhGN9XLJfL9uonmdbrNd4Xu0+iz1az2Wyz2RDbNM0kSaqqusGyMxPkY8TFD3Gq2i7+Z7y0WCxqm2XZw+EwmUxs24Zxt9v5vr/dbmkscSLbNuHRSA92cXzPvIZhKAgCUDwej+BOp1NFUfb7veM4TZb3qufRKhzwiqqq+HydKzrqkgiodh8t0JvnSwehCOqBDgqGruvn87mjEzVoDcjlADTUA5Xa3S9rEdRZwR2KrFJ6Vp92hoHV43meLMugnkbM/989URQBs1o9oBtQTxzHuDrf5IYcj8cvpghfbFcevSRGML04nafTCcb5fN4zPk3THwEGAELY0Kf5HoWGAAAAAElFTkSuQmCC) center center no-repeat",
      "z-index": "2"

    });
    this.COLLAPSE_BUTTON.addEventListener("click", function(e){
      banner.collapse();
    });
    this.ROOT.appendChild(banner.COLLAPSE_BUTTON);
  };

  window.CuttingEdge = CuttingEdge;

})(window.Banner, window.Tracking);


var config = {
  "default" : {
    "width": "1190",
    "height": "90",
    "src": "banner.html",
    "html": "",
    "reload": "true",
    "clicktag": ["http://www.uol.com.br"],
    "event": "click",
    "cookieExpire": "24",
    "tracking": {
      "open": ["http://www5.smartadserver.com/imp?imgid=1&tmstp=%%TIMESTAMP%%&tgt=&open"],
      "close": ["http://www5.smartadserver.com/imp?imgid=2&tmstp=%%TIMESTAMP%%&tgt=&close"],
      "hide": ["http://www5.smartadserver.com/imp?imgid=3&tmstp=%%TIMESTAMP%%&tgt=&hide"]
    },
    "mask": {
      "enable": "false",
      "width": "100%",
      "height": "100%",
      "left": "0",
      "top": "0"
    },
    "mirror": ""
  },
  "expand": {
    "enable": "true",
    "src": "banner-expanded.html",
    "html": "",
    "width": "1190",
    "height": "420",
    "direction": "right down",
    "delay": "0",
    "duration": "0.3"
  }
};
var banner = new CuttingEdge(config);
banner.init();
