(function (window, document, undefined){
'use strict';
function Config () {
    return {
        debug: true, //habilita o modo visual das mascaras
        screenshot : 'http://insercao.uol.com.br/teste/richmedia/2015/default/screenshot.jpg',
        title : 'cutting', //nome da campanha ou peca
        expand : true, //habilita a expansão
        cookieExpire : true, //o cookie de fechamento expira apenas depois do numero de horas definido na varaivel "cookieHours"
        cookieHours : 8, //define o tempo(horas) para a expiração do cookie
        format: {
            initial: {
                width : 1190, // largura do formato inicial
                height : 180, // altura do formato inicial
                background : '#ffffff', // cor de fundo Ex. #000000
                src : 'http://placehold.it/1190x180', //path do html ou variavel do conteudo html
                openByHover: false, //flag para habilitar a expansão por mouseover
                delayToOpen: 1000 //tempo para abertura com mouseover
            },
            expanded : {
                width : 955, //largura do formato expandido
                height : 500, //altura do formato expandido
                background : '#ffffff',
                src : 'http://insercao.uol.com.br/teste/richmedia/2015/default/expandido/', //path do html ou variavel do conteudo html
                clickArea : 'true', // em true habilita a mascara - false desabilita
                closeBtn: {
                    marginRight : 5,
                    marginTop : 5
                },
            },
            write: false // em false utilizado o modo de carregamento via src - em true utilizado o modo write
        },
        clickTag : [
            '%%CLICKTAG%%'
        ],
        track : {
            open : '%%ID_OPEN%%',
            close : '%%ID_CLOSE%%'
        },
        bannerId : 'banner-1190x70-2-Area' // banner ID do container
    };
}

function Appender () {
    var $public = this;
    var $private = {};

    $public.setContainer = function (bannerId) {
        $private.container = document.getElementById(bannerId) || top.document.getElementById(bannerId);
        //$private.hideChildren($public.getContainer());
    };

    $public.applyClass = function (id, className, override) {
        var element = $public.byId(id);
        var classValue = element.getAttribute('class');
        if (classValue && !override) {
            classValue = classValue.trim();
            classValue = classValue.split(' ');
            element.setAttribute('class', classValue[0] + ' ' + className);
            return element;
        }

        element.setAttribute('class', className);
        return element;
    };

    $public.isBrowser = function (listBrowsers) {
        return new RegExp(listBrowsers.join('|'), 'i').test(navigator.userAgent);
    };

    $public.getContainer = function () {
        return $private.container;
    };

    $public.insertOnContainer = function (element) {
        if (!$private.container) {
            return;
        }
        $private.container.appendChild(element);
    };

    $public.getOwnerDocument = function () {
        return $private.container.ownerDocument;
    };

    $public.byId = function (id) {
        return $public.getOwnerDocument().getElementById(id);
    };

    $private.byClassName = function (id) {
        return $public.getOwnerDocument().getElementsByClassName(id);
    };

    $public.getContainerById = function (id, first, second) {
        var idFound;
        if(id.indexOf('first') > -1) {
            idFound = first;
        } else if (id.indexOf('second') > -1) {
            idFound = second;
        }
        return $public.byId(idFound);
    };


    $public.applyAttributes = function (element, properties) {
        for (var property in properties) {
            if (!properties.hasOwnProperty(property)) {
                continue;
            }
            element.setAttribute(property, properties[property]);
        }
        return element;
    };

    $public.applyStyles = function (element, styles) {
        for (var style in styles) {
            if (!styles.hasOwnProperty(style)) {
                continue;
            }
            element.style[style] = styles[style];
        }
        return element;
    };

    $public.appenderMainContainer = function (id, element) {
        var main = $public.byId(id) || $private.byClassName(id);
        if (main.length > 0) {
            main[0].appendChild(element);
            return;
        }
        main.appendChild(element);
    };
}

function Tracking(debug) {
    
    var $private = {};
    var $public = this;
    $private.appender = new Appender();

    $public.trackByParams = function (params, id) {
        if (id === "" || id === undefined) {
            return;
        }
        var url = 'http://www5.smartadserver.com/imp?imgid=' + id + '&tmstp=' + (new Date()).getTime() + '&tgt=&' + params;
        if (debug.toString() === 'true') {
            console.log('[UOL RICHMEDIA SMART TRACKING DEBUG MODE]', url);
            return;
        }
        
        if(params === 'open') {
            var urlClient = 'http://url.clienet';
            $private.appender.applyAttributes(new Image(), {'src': urlClient});
        }

        $private.appender.applyAttributes(new Image(), {'src': url});
    };
    
}
function Constants(configConstants) {
    var config = configConstants || {};
    return {
        FIRST_CONTAINER   : 'uol-container-' + config.title,
        FIRST_IFRAME      : 'uol-iframe-' + config.title,
        PRELOAD_CONTAINER : 'uol-preloader-' + config.title,
        STATE : 'retract'
    };
}

function CuttingEdge(configCutting){

    var $public = this;
    var $private = {};

    $private.config = configCutting || {};
    $private.appender = new Appender();
    $private.constants = new Constants($private.config);
    $private.tracking = new Tracking($private.config.debug);

    $private.IMAGE_CLOSE_BUTTON = 'http://bn.imguol.com/1110/uol/cutting/fechar.gif';
    $private.IMAGE_PRELOADER = 'http://ci.i.uol.com.br/album/wait_inv.gif';
    $private.BASE_TRACK_PATH = 'http://www5.smartadserver.com/imp?imgid=';
    // containers
    $private.FIRST_IFRAME = 'uol-iframe-first-content-' + $private.config.title;
    $private.FIRST_CONTAINER = 'first-container-' + $private.config.title;
    $private.SECOND_CONTAINER = 'second-container-' + $private.config.title;
    $private.GLIDER_WRAPPER = 'glider-wrapper';
    $private.GLIDER_CONTENT = 'glider-content-' + $private.config.title;
    $private.PRELOAD_CONTAINER = 'preload-container-' + $private.config.title;
    $private.GLIDER_IFRAME = 'glider-iframe-second-' + $private.config.title;
    $private.WRAPPER_ID = 'uol-richmedia-' + $private.config.title;

    $public.render = function () {
        if ($private.getCookie()) {
            return;
        }

        $private.appender.setContainer($private.config.bannerId);
        if (!$private.appender.getContainer()) {
            if ($private.config.debug.toString() === 'true') {
                console.log('[UOL RICHMEDIA] banner id nao encontrado');
            }
            return;
        }

        var first = $private.createFirstContainer($private.config.format.initial.width,$private.config.format.initial.height);
        var second = $private.createSecondContent();
        var wrapper = $private.createContainer($private.WRAPPER_ID);
        wrapper.appendChild(first);
        wrapper.appendChild(second);
        
        $private.createCustomCssOnHeader();
        $private.addCssAnimation();
        top.window.addEventListener("scroll", $private.scrollEventListener);
        $private.appender.insertOnContainer(wrapper);
        $private.loadIframeContent($private.constants.FIRST_IFRAME, $private.config.format.initial.src);

        var root = $private.appender.getContainer().getElementsByTagName('iframe')[0];
        $private.appender.applyStyles(first, {
            //'left': root.offsetLeft+'px',
            //'top': root.offsetTop+'px'
        });
    };

    $private.scrollEventListener = function () {
        var scrollTop = $private.appender.getOwnerDocument().body.scrollTop || $private.appender.getOwnerDocument().documentElement.scrollTop;
        var secondContainer = $private.appender.byId($private.SECOND_CONTAINER);
        secondContainer.style.top = -scrollTop + 'px';
    };

    $private.loadIframeContent = function (id, src) {
        if($private.config.format.write.toString() === 'true') {
            if (typeof src !== 'string') {
                console.log('[UOL RICHMEDIA] src em modo write incorreto');
                return;
            }
            $private.writeIframeContent(id, src);
        } else {
            $private.sourceIframeContent(id, src);
        }
    };

    $public.getConfigTitle = function () {
        return $private.config.title.toUpperCase();
    };

    $public.clickTag = function () {
        $public.close();
        try {
            window.open($public.getClickTag(), '_blank');
        } catch (e) {
            console.log('[CLICKTAG COM VALOR INVALIDO]');
        }
    };

    $public.getClickTag = function (value) {
        var index = (value === undefined) ? 0 : value;
        return $private.config.clickTag[index];
    };

    $public.open = function() {
        if ($private.appender.isBrowser(['msie 9.0', 'msie 10.0'])) {
            $public.clickTag();
            return;
        }
        $private.showGlider();
        $private.refreshHome('pause');
        $private.tracking.trackByParams('open', $private.config.track.open);
    };

    $public.close = function() {
        $private.closeAnimation();
        $private.refreshHome('start');
        $private.tracking.trackByParams('close', $private.config.track.close);
    };

    $private.createCustomCssOnHeader = function () {
        var head = $private.appender.getOwnerDocument().head || $private.appender.getOwnerDocument().getElementsByTagName('head')[0];
        var css = '#'+ $private.config.bannerId +'{height:auto; cursor:pointer; background-color:#ffffff; transition: .5s ease-in;}';
        //var css = '#'+ $private.config.bannerId +'{height:auto; cursor:pointer; position:relative; background-color:#ffffff; width: 1190px; margin-top:5px; transition: .5s ease-in;} #'+$private.config.bannerId+'span {position:absolute; top:0px; right:5px;} #closeCuttingBtn {top:0; right:0; width:55px; height:15px; position:absolute; cursor:pointer; z-index: 1;}';

        var styleElement = document.createElement("style");
        styleElement.type = "text/css";

        if (styleElement.styleSheet) {
            styleElement.styleSheet.cssText = css;
        } else {
            styleElement.appendChild(document.createTextNode(css));
        }

        head.insertBefore(styleElement, head.firstChild);
    };

    $public.hideCuttingEdge = function() {
        $private.setCookie();
        var div = $private.appender.byId($private.constants.FIRST_CONTAINER);
        div.style.height = '0px';
    };

    $private.setCookie = function () {
        var date = new Date();
        date.setTime(date.getTime()+(($private.config.cookieHours)*60*60*1000));
        var expires = 'expires='+date.toUTCString() + '; path=/';
        $private.appender.getOwnerDocument().cookie='CEHome=0;domain=.uol.com.br;' + (($private.config.cookieExpire) ? expires : '');
    };

    $private.getCookie = function () {
        if (document.cookie.indexOf("CEHome") != -1) {
           return true;
        }
        return false;
    };

    $private.refreshHome = function(value) {
        if (window.homeUOL === undefined) {
            return;
        }
        if (window.homeUOL.modules === undefined) {
            return;
        }
        if (window.homeUOL.modules.refresh === undefined) {
            return;
        }
        if(value === 'pause') {
            window.homeUOL.modules.refresh.pause();
        } else if(value === 'start') {
            window.homeUOL.modules.refresh.start();
        }
    };

    $private.reloadIframe = function (id) {
        $private.appender.applyAttributes($private.appender.byId(id), {'src': ''});
    };

    $public.loadComplete = function (element) {};


    $private.writeIframeContent = function (id, src) {
        var iframe = $private.appender.applyAttributes($private.appender.byId(id), {
            'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);'
        });
        iframe.contentWindow.document.open();
        iframe.contentWindow.document.write(src);
        iframe.contentWindow.document.close();
    };

    $private.sourceIframeContent = function (id, url) {
        $private.appender.applyAttributes($private.appender.byId(id), {
            'onload' : 'window.UOLI.'+$public.getConfigTitle()+'.loadComplete(this);',
            'src' : url
        });
    };

    $private.createFirstContainer = function (width, height) {
        var div = $private.createContainer($private.constants.FIRST_CONTAINER);
        div.style.background = $private.config.format.initial.background;

        $private.appender.applyStyles(div, {
            'position':'relative',
            'width':width+'px',
            'height':height+'px',
            'transition': '.5s ease-in',
            'overflow': 'hidden'
        });

        div.appendChild($private.createClickArea(width, height));

        var iframe = $private.createIframe($private.constants.FIRST_IFRAME);
        div.appendChild(iframe);

        $private.appender.applyStyles(iframe, {
            'position' : 'absolute',
            'width' : iframe.parentNode.style.width,
            'height' : iframe.parentNode.style.height,
            'top' : iframe.parentNode.style.top,
            'right' : iframe.parentNode.style.right,
            'transition': '.5s ease-in'
        });

        $private.btn = $private.createCuttingCloseButtom();
        div.appendChild($private.btn);

        return div;
    };

    $private.createContainer = function (name) {
        var div = $private.appender.applyAttributes(document.createElement('div'), {'id' : name});
        return div;
    };

    $private.createClickArea = function (width, height) {
        var clickArea = document.createElement('a');
        $private.appender.applyStyles(clickArea, {
            'position':'absolute',
            'width':width+'px',
            'height':height+'px',
            'top':'0',
            'left':'0',
            'zIndex':1,
            'cursor':'pointer',
            'transition': '.5s ease-in'
        });

        $private.debugClickArea(clickArea);

        clickArea.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.target.hover = false;
            $public.open();
        });

        if ($private.config.format.initial.openByHover) {
            clickArea.addEventListener('mouseenter', function(e){
                if($private.constants.STATE === 'expanded') {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = true;
                window.setTimeout(function(){
                    if (e.target.hover) {
                        $public.open();
                    }
                }, $private.config.format.initial.delayToOpen || 1500);
            });

            clickArea.addEventListener('mouseleave', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = false;
            });
        }
        return clickArea;
    };

    $private.debugClickArea = function (clickArea) {
        if ($private.config.debug.toString() === 'true') {
            clickArea.style.background = '#ff0000';
            clickArea.style.opacity = 0.5;
        }
    };

    $private.getScrollHeight = function () {
        var doc = $private.appender.getOwnerDocument();
        return Math.max(
            Math.max(doc.body.scrollHeight, doc.documentElement.scrollHeight),
            Math.max(doc.body.offsetHeight, doc.documentElement.offsetHeight),
            Math.max(doc.body.clientHeight, doc.documentElement.clientHeight)
        );
    };

    $private.getBrowserWidth = function () {
        var doc = $private.appender.getOwnerDocument();
        return window.innerWidth || doc.documentElement.clientWidth || doc.body.clientWidth;
    };

    $private.createIframe = function (id) {
        var iframe = $private.appender.applyAttributes(document.createElement('iframe'), {
            'id' : id,
            'marginwidth' : '0',
            'marginheight' : '0',
            'frameborder' : '0',
            'scrolling' : 'no'
        });
        return iframe;
    };

    $private.createCuttingCloseButtom = function () {
        var img = $private.appender.applyAttributes(document.createElement('img'),
            {'src' : 'http://bn.imguol.com/1110/uol/cutting/fechar.gif'});

        var btn = $private.appender.applyAttributes(document.createElement('a'),
            {'value' : 'Fechar'
        });

        var css = {
            'position':'absolute',
            'width':'55px',
            'height':'15px',
            'top': ($private.config.format.expanded.closeBtn.marginTop || 0)+'px',
            'right': ($private.config.format.expanded.closeBtn.marginRight || 0)+'px',
            'zIndex':1,
            'cursor':'pointer'
        }

        $private.appender.applyStyles(btn, css);

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            $public.hideCuttingEdge();
            //if($private.constants.STATE === 'retract') {
            //} else {
                //$public.close();
            //}
        });

        btn.appendChild(img);
        return btn;
    };

    $private.showGlider = function () {
        top.window.scrollTo(0, 0);
        $private.toggleContainerZindex($private.config.bannerId);
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.toggleVisibility($private.GLIDER_WRAPPER);
        $private.applyAnimationEvents($private.appender.applyClass($private.GLIDER_WRAPPER, 'openGliderContent'), $private.animationEndToOpen);
    };

    $private.animationEndToOpen = function (e) {
        if (e.animationName === 'rotateToOpenReverse') {
            $private.toggleVisibility($private.GLIDER_CONTENT);
            $private.loadIframeContent($private.GLIDER_IFRAME, $private.config.format.expanded.src);
            $private.appender.applyClass($private.GLIDER_CONTENT, 'showGliderContent', true);
            $private.removeAnimationEvents(this, $private.animationEndToOpen);
            $private.countAnimation = 0;
            $private.secondContainerIsOpened = true;
        }
    };

    $private.animationEndToCloseGliderContent = function (e) {
        if (e.animationName !== 'fadeOutGliderContent') {
            return;
        }
        $private.toggleVisibility($private.GLIDER_CONTENT);
        var wrapper = $private.appender.applyClass($private.GLIDER_WRAPPER, 'closeGliderContent');
        $private.applyAnimationEvents(wrapper, $private.animationEndToCloseSecondContainer);
        $private.removeAnimationEvents(this, $private.animationEndToCloseGliderContent);
    };

    $private.animationEndToCloseSecondContainer = function (e) {
        if (e.animationName !== 'fadeOutGliderContent') {
            return;
        }
        $private.reloadIframe($private.GLIDER_IFRAME);
        $private.toggleVisibility($private.SECOND_CONTAINER);
        $private.toggleVisibility($private.GLIDER_WRAPPER);
        $private.toggleContainerZindex($private.config.bannerId);
        $private.removeAnimationEvents(this, $private.animationEndToCloseSecondContainer);
        $private.secondContainerIsOpened = false;
    };

    $private.closeAnimation = function () {
        var gliderContent = $private.appender.applyClass($private.GLIDER_CONTENT, 'hideGliderContent', true);
        $private.applyAnimationEvents(gliderContent, $private.animationEndToCloseGliderContent);
    };

    $private.createSecondContent = function () {
        var div = $private.appender.applyAttributes($private.createContainer($private.GLIDER_WRAPPER), {'class': $private.GLIDER_WRAPPER});
        var img = $private.appender.applyAttributes(document.createElement('img'), {'src': $private.config.screenshot});

        $private.appender.applyStyles(div, {
            'position': 'absolute',
            'width': '100%',
            'height': '1600px',
            'top': '0px',
            'display': 'none',
            'background': $private.config.format.expanded.background
        });

        $private.appender.applyStyles(img, {
            'position': 'relative',
            'marginRight': '-50%',
            'marginLeft': '-50%'
        });

        div.appendChild(img);
        div.appendChild($private.createMask('100%', '100%', true, $public.close));

        var wrapper = $private.createSecondWrapper($private.SECOND_CONTAINER);
        var glider = $private.createGliderContent($private.config.format.expanded.width, $private.config.format.expanded.height);
        var btn = $private.createGliderCloseButton();

        wrapper.appendChild(div);
        wrapper.appendChild(glider);
        wrapper.appendChild(btn);
        return wrapper;
    };

    $private.createSecondWrapper = function (id) {
        var wrapper = $private.createContainer(id);
        $private.appender.applyStyles(wrapper, {
            'position': 'fixed',
            'width': '100%',
            'height': $private.getScrollHeight() + 'px',
            'top': '0px',
            'left': '0px',
            'display': 'none',
            'background': $private.config.format.expanded.background,
            'zIndex': 1
        });
        return wrapper;
    };

    $private.createGliderContent = function (width, height) {
        var div = $private.appender.applyStyles($private.createContainer($private.GLIDER_CONTENT), {
            'position': 'relative',
            'width': width + 'px',
            'height': height + 'px',
            'top': '0px',
            'left': '0px',
            'display': 'none',
            'margin': '0 auto',
            'background': $private.config.format.expanded.background
        });

        if ($private.config.format.expanded.clickArea.toString() === 'true') {
            div.appendChild($private.createMask(width, height, true, $public.clickTag));
        }

        var iframe = $private.appender.applyStyles($private.createIframe($private.GLIDER_IFRAME), {
            'position': 'absolute',
            'width': div.style.width,
            'height': div.style.height,
            'top': div.style.top,
            'left': div.style.left
        });

        div.appendChild(iframe);
        return div;
    };

    $private.createGliderCloseButton = function () {
        var img = $private.appender.applyAttributes(document.createElement('img'), {'src': $private.IMAGE_CLOSE_BUTTON});
        var btn = $private.appender.applyAttributes(document.createElement('a'), {'value': 'Fechar'});

        $private.appender.applyStyles(btn, {
            'position': 'absolute',
            'width': '55px',
            'height': '15px',
            'top': $private.config.format.expanded.closeBtn.marginTop+'px',
            'left': '50%',
            'margin': '0 0 0 '+(($private.config.format.expanded.width*0.5)-(55+$private.config.format.expanded.closeBtn.marginRight))+'px',
            'zIndex': 3, 'cursor': 'pointer'
        });

        btn.addEventListener('click', function (e) {
            e.preventDefault();
            if ($private.secondContainerIsOpened === false) {
                return;
            }
            $public.close();
        });
        btn.appendChild(img);
        return btn;
    };

    $private.getPosX = function () {
        var posX = 0;
        var configWidth = $private.config.format.expanded.width * 0.5;
        var browserWidth = $private.getBrowserWidth() * 0.5;

        if (configWidth > browserWidth) {
            posX = configWidth - browserWidth;
        } else {
            posX = browserWidth - configWidth;
        }
        return posX;
    };

    $private.createMask = function (width, height, flag, callback) {
        var mask = $private.appender.applyStyles(document.createElement('a'), {
            'position': 'absolute',
            'top': '0',
            'left': '0',
            'zIndex': 1,
            'cursor': 'pointer',
            'display': 'block'
        });

        if (flag) {
            mask.style.width = '100%';
            mask.style.height = '100%';
        } else {
            mask.style.width = width + 'px';
            mask.style.height = height + 'px';
        }
        $private.debugMask(mask);

        mask.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.target.hover = false;
            callback();
        });

        if ($private.config.format.initial.openByHover && flag===false) {
            mask.addEventListener('mouseenter', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = true;
                window.setTimeout(function(){
                    if (e.target.hover) callback();
                }, $private.config.format.initial.delayToOpen || 1500);
            });

            mask.addEventListener('mouseleave', function(e){
                e.preventDefault();
                e.stopPropagation();
                e.target.hover = false;
            });
        }

        return mask;
    };

    $private.debugMask = function (mask) {
        if ($private.config.debug.toString() !== 'true') {
            return;
        }
        $private.appender.applyStyles(mask, {'background': '#ff0000', 'opacity': 0.5});
    };

    $private.addCssAnimation = function () {
        var style = $private.appender.applyAttributes(document.createElement('style'), {'type': 'text/css'});
        var css = '@keyframes moveToBottom{from{top: 0;left: 0}to{top: ' + $private.config.format.expanded.height + 'px;left: 0}}@keyframes moveToTop{from{top: ' + $private.config.format.expanded.height + 'px;left: 0}to{top: 0;left: 0}}@keyframes rotateToOpen{from{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}to{-webkit-transform: perspective(1200px) rotateX(20deg);-moz-transform: perspective(1200px) rotateX(20deg);-o-transform: perspective(1200px) rotateX(20deg);-ms-transform: perspective(1200px) rotateX(20deg);transform: perspective(1200px) rotateX(20deg)}}@keyframes rotateToOpenReverse{from{-webkit-transform: perspective(1200px) rotateX(20deg);-moz-transform: perspective(1200px) rotateX(20deg);-o-transform: perspective(1200px) rotateX(20deg);-ms-transform: perspective(1200px) rotateX(20deg);transform: perspective(1200px) rotateX(20deg)}to{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}}@keyframes rotateToClose{from{top: ' + $private.config.format.expanded.height + 'px;-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}to{top: ' + $private.config.format.expanded.height + 'px;-webkit-transform: perspective(1200px) rotateX(-20deg);-moz-transform: perspective(1200px) rotateX(-20deg);-o-transform: perspective(1200px) rotateX(-20deg);-ms-transform: perspective(1200px) rotateX(-20deg);transform: perspective(1200px) rotateX(-20deg)}}@keyframes stepAinit{from{-webkit-transform: perspective(1200px) rotateX(-20deg);-moz-transform: perspective(1200px) rotateX(-20deg);-o-transform: perspective(1200px) rotateX(-20deg);-ms-transform: perspective(1200px) rotateX(-20deg);transform: perspective(1200px) rotateX(-20deg)}to{-webkit-transform: perspective(1200px) rotateX(0);-moz-transform: perspective(1200px) rotateX(0);-o-transform: perspective(1200px) rotateX(0);-ms-transform: perspective(1200px) rotateX(0);transform: perspective(1200px) rotateX(0)}}@keyframes fadeInGliderContent{from{opacity: 0}to{opacity: 1}}@keyframes fadeOutGliderContent{from{opacity: 1}to{opacity: 0}}.glider-wrapper{-webkit-perspective-origin: top,center;-moz-perspective-origin: top,center;-o-perspective-origin: top,center;-ms-perspective-origin: top,center;perspective-origin: top,center;-webkit-transform-origin: top center;-moz-transform-origin: top center;-o-transform-origin: top center;-ms-transform-origin: top center;transform-origin: top center;-webkit-transform-style: preserve-3d;-moz-transform-style: preserve-3d;-o-transform-style: preserve-3d;-ms-transform-style: preserve-3d;transform-style: preserve-3d;text-align: center}.openGliderContent{animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-webkit-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-moz-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s;-o-animation: fadeInGliderContent 1s,rotateToOpen 1s,moveToBottom 1s forwards 1s,rotateToOpenReverse 1s forwards 1s}.closeGliderContent{animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-webkit-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-moz-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s;-o-animation: rotateToClose 1s forwards,moveToTop 1s forwards 1s,stepAinit 1.2s forwards 1.2s,fadeOutGliderContent 1s forwards 2s}.showGliderContent{animation: fadeInGliderContent 1s;-webkit-animation: fadeInGliderContent 1s;-moz-animation: fadeInGliderContent 1s;-o-animation: fadeInGliderContent 1s}.hideGliderContent{animation: fadeOutGliderContent .5s;-webkit-animation: fadeOutGliderContent .5s;-moz-animation: fadeOutGliderContent .5s;-o-animation: fadeOutGliderContent .5s}';
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        var head = $private.appender.getOwnerDocument().head || $private.appender.getOwnerDocument().getElementsByTagName('head')[0];
        head.appendChild(style);
    };

    $private.removeAnimationEvents = function (element, eventCallback) {
        element.removeEventListener('animationend', eventCallback);
        element.removeEventListener('webkitAnimationEnd', eventCallback);
        element.removeEventListener('MSAnimationEnd', eventCallback);
        element.removeEventListener('oAnimationEnd', eventCallback);
    };

    $private.applyAnimationEvents = function (element, eventCallback) {
        element.addEventListener('animationend', eventCallback);
        element.addEventListener('webkitAnimationEnd', eventCallback);
        element.addEventListener('MSAnimationEnd', eventCallback);
        element.addEventListener('oAnimationEnd', eventCallback);
    };

    $private.toggleContainerZindex = function (id) {
        var div = $private.appender.byId(id);
        $private.zIndexOrigin = $private.zIndexOrigin || div.style.zIndex;
        if ($private.zIndexOrigin === undefined || $private.zIndexOrigin === '' || $private.zIndexOrigin === '2147483647') {
            $private.zIndexOrigin = 2147483647;
        } else {
            $private.zIndexOrigin = null;
        }
        $private.appender.applyStyles(div, {'position': 'relative', 'zIndex': $private.zIndexOrigin});
    };

    $private.toggleVisibility = function (id) {
        var div = $private.appender.byId(id);
        div.style.display = (div.style.display === 'none') ? 'block' : 'none';
    };
}

var cuttingEdge = new CuttingEdge(new Config());
cuttingEdge.render();
top.window.UOLI = top.window.UOLI || {};
top.window.UOLI[cuttingEdge.getConfigTitle()] = {};
top.window.UOLI[cuttingEdge.getConfigTitle()].clickTag = cuttingEdge.clickTag;
top.window.UOLI[cuttingEdge.getConfigTitle()].getClickTag = cuttingEdge.getClickTag;
top.window.UOLI[cuttingEdge.getConfigTitle()].loadComplete = cuttingEdge.loadComplete;
top.window.UOLI[cuttingEdge.getConfigTitle()].open = cuttingEdge.open;
top.window.UOLI[cuttingEdge.getConfigTitle()].close = cuttingEdge.close;
})(this, document);