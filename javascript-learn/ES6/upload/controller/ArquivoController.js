class ArquivoController {

    construtor() {

        this._inputDados = document.querySelector('.dados-arquivo');
    }

    envia() {
        //cria um Arquivo com as suas propriedades;
        //let dados = this._inputDados.value.split('/').map(item => item.toUpperCase());
        let dados = ArquivoHelper.cria(this._inputDados.value);
        let arquivo = new Arquivo(...dados);
        console.log(`Dados do aquivo> ${arquivo.nome}, ${arquivo.tamanho}, ${arquivo.tipo}`);
        this._limpaFormulario();
        //exibe mensagem no console com os dados do arquivo.
    }

    _limpaFormulario() {

        this._inputDados.value = '';
        this._inputDados.focus();
    }

}