function teste(x, y, z) {
	console.log(x);
	console.log(y);
	console.log(z);
}
teste(10, 20, 30);


// Funções com Arrow functions

testeArrow = (x, y, z) => {
	console.log('\ntesteArrow');
	console.log(x);
	console.log(y);
	console.log(z);
}
testeArrow(40, 50, 60);

console.log('\n', (() => "foobar")());