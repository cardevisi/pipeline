var Singleton = (function(){
    var instantiated;

    function init() {
        return {
            publicMethod : function() {
                console.log('hello world');
            },
            publicProperty: 'test'
        };
    }

    return {
        getInstance : function() {
            if(!instantiated) {
                instantiated = init();
            }
            return instantiated;
        }
    }
})();