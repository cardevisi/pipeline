(function(window) {
	// console.log(document.firstChild);
	// console.log(document.lastChild);
	// console.log(document.firstChild.childNodes);
	var div = document.querySelector('#dom-element');
	
	console.log('div', div);
	//document.body.insertBefore(element, div);

	// 'beforebegin'
	// 'afterbegin'
	// 'beforeend'
	// 'afterend'

	div.insertAdjacentHTML('afterbegin', '<div id="two">two</div>');
	div.insertAdjacentHTML('beforebegin', '<div id="two">two</div>');
	div.insertAdjacentHTML('afterend', '<div id="two">two</div>');
	div.insertAdjacentHTML('beforeend', '<div id="two">two</div>');

	var element = document.createElement('div');
	element.setAttribute('id', 'inner-div');
	div.insertAdjacentElement('beforebegin',element);
	div.insertAdjacentElement('afterbegin',element);
	div.insertAdjacentElement('afterend',element);
	div.insertAdjacentElement('beforeend',element);
	
	var tempDiv = document.createElement('div');
  tempDiv.style.backgroundColor = '#ff0000';
  div.insertAdjacentElement('beforebegin',tempDiv);
  div.insertAdjacentElement('afterbegin',tempDiv);
  div.insertAdjacentElement('afterend',tempDiv);
  div.insertAdjacentElement('beforeend',tempDiv);
})();