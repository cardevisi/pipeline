##Padrão UOL de Variável Universal - UV:

A variável universal é um objeto javascript global que contém todas as informações dinâmicas e de relevância de uma página. Essas informações são utilizadas para entender o comportamento de usuários durante a navegação e assim possibilitar a entrega de soluções personalizadas, mais focadas em suas reais necessidades. Por este motivo é fundamental que elas sejam configuradas corretamente. Este guia irá ajudá-lo a determinar o tipo mais adequado para cada situação, através de exemplos e boas práticas que possibilitarão uma coleta de dados mais efetiva.

A variável universal é definida pelo namespace `window.universal_variable` e precisa ser codificada em seu site dentro da tag `<head>`. Suas propriedades, chaves e valores, precisam ser preenchidos dinamicamente através do uso de javascript ou qualquer outra linguagem de sua preferência.

O primeiro passo para a criação de uma UV é determinar o seu tipo. A escolha do tipo está diretamente ligada ao tipo de página que se deseja analisar, aqui no UOL utilizamos os seguintes modelos:

### Tipos de UV:

- `Home` - indicada para a página principal de um site ou ecommerce.
- `Conteúdo` - indicada para páginas de conteúdo.
- `Produto` - indicada para páginas que exibem produtos únicos, página de detalhe com maiores informações de um produto.
- `Categoria` - indicada para páginas de categoria, caracterizadas por exibirem múltiplos produtos, agrupados em categorias e subcategorias.
- `Busca` - indicada para páginas de busca ou filtros de exibição de produtos.
- `Carrinho` - indicada para a página de carrinho onde são listados os produtos que foram selecionados para compra.
- `Checkout` - indicada para a página de checkout, página caracterizada pela conclusão de uma compra.
- `Transação` - indicada para a página de transação, página onde o pagamento é efetivado.

###Home

Exemplo de código Javascript:

```js
 <script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        }
    };
</script>
```

- O objeto `page` descreve a página corrente do site, seu valor é definido pelo preenhimento da propriedade `category`. No exemplo `"<pageCategory>"`.
- O objeto `user` descreve o usuário corrente do site, seu valor é definido pela propriedade `user_id`. No exemplo `"<userID>"`.
- Os objetos `page` e `user` são comuns a todos os tipos de UV.


Propriedades dos objetos page e user:

|   Propriedade                | Chave JSON     |   Tipo |    Descrição                   |
|   -----------                | -------------- | ------ |  -------------------------     |
|   Categoria                  | category       | String |  Informa o tipo de página atual. Ex. home, produto, categoria, checkout, carrinho, etc...       |
|   Identificador de usuário   | user_id        | String |  Identificador único utilizado pelo site para identificar o usuário corrente. |


###Conteúdo

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>"
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>"
                    }
                }
            ]
        }
    };
</script>
```

- O objeto `basket` descreve o estado atual do carrinho de compras. O `basket` é formado pelo objeto `line_itens`, uma listagem de itens ou produtos.

Propriedades do objeto basket:

|   Propriedade                |   Chave JSON  |   Tipo |    Descrição                   |
|   -----------                | -----------   | ------ |  -------------------------     |
|   Itens                      | line_items    | Array  |  Os items presentes no seu carrinho ou cesta de compras.   |
|   Produto                    | product       | Objeto |  O produto que foi adicionado no carrinho ou na transação. |
|   Sku Code                   | sku_code      | String |  Código do produto.                |


###Produto

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "product": {
            "sku_code": "<sku>",
            "description": "Description about this product",
            "category": "Shoe",
            "subcategory": "Heels",
            "unit_price": 130,
            "currency": "GBP"
        }
    };
</script>
```

- O objeto `product` deve ser utilizado em páginas que detalhem um único produto.

Propriedades do objeto product:

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                         |
|   -----------     | ----------- | ------ |  -------------------------           |  
|   Descrição       | description | String |  Descrição do item ou produto.       |
|   Categoria       | category    | String |  Nome da categoria de um produto.    |  
|   Subcategoria    | subcategory | String |  Nome da subcategoria de um produto. |
|   Moeda           | currency    | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos.      |
|   Preço           | unit_price  | Number |  O preço unitário do produto, sem considerar desconto ou promoções. |
|   Sku Code        | sku_code    | String |  Código do produto.                  |


###Categoria

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "listing": {
            "items": [
                {
                    "sku_code": "<sku>",
                    "description": "Description about this product",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 130
                },
                {
                    "sku_code": "<sku2>",
                    "description": "Another description",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 150
                }
            ]
        }
    };
</script>
```

O objeto `listing` é formado pelo array `items`, que deverá ser populado apartir da listagem de produtos exibidos na página, excluindo-se as recomendações.

Propriedades do objeto listing:

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                      |
|   -----------     | ----------- | ------ |  -------------------------        |  
|   Itens           | items       | Array  |  Os items ou produtos exibidos em uma categoria ou subcategoria. |

Propriedades de cada item ou produto da lista (`items`):

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                                  |
|   -----------     | ----------- | ------ |  -------------------------                    |  
|   Descrição       | description | String |  Descrição do item ou produto na lista.       |  
|   Categoria       | category    | String |  Nome da categoria de um produto na lista.    |  
|   Subcategoria    | subcategory | String |  Nome da subcategoria de um produto na lista. |
|   Moeda           | currency    | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos.      |
|   Preço           | unit_price  | Number |  O preço unitário do produto, sem considerar desconto ou promoções. |
|   Sku Code        | sku_code    | String |  Código do produto.                           |

###Busca

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "listing": {
            "items": [
                {
                    "sku_code": "<sku>",
                    "description": "Description about this product",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 130
                },
                {
                    "sku_code": "<sku2>",
                    "description": "Another description",
                    "category": "Shoe",
                    "subcategory": "Heels",
                    "unit_price": 150
                }
            ]
        }
    };
</script>
```

O objeto `listing` descreve uma listagem de produtos, como múltiplos produtos exibidos apartir do resultado de uma pesquisa, excluindo-se as recomendações.

Propriedades do objeto listing:

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                      |
|   -----------     | ----------- | ------ |  ---------------------------------    |  
|   Itens           | items       | Array  |  Os items ou produtos exibidos na página corrente logo após a busca. |

Propriedades de cada item ou produto da lista 'items':

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                               |
|   -----------     | ----------- | ------ |  -------------------------------           |  
|   Descrição       | description | String |  Descrição do item ou do produto na lista.  |
|   Categoria       | category    | String |  Nome da categoria de um produto.           |  
|   Subcategoria    | subcategory | String |  Nome da subcategoria de um produto.        |
|   Moeda           | currency    | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos. |
|   Preço           | unit_price  | Number |  O preço unitário do produto, sem considerar desconto ou promoções. |
|   Sku Code        | sku_code    | String |  Código do produto.                         |


###Carrinho

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "currency": "GBP",
            "total": 280,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>",
                        "description": "Description about this product",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 130
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>",
                        "description": "Another description",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 150
                    }
                }
            ]
        }
    }
</script>
```

- O objeto `basket` descreve o estado atual do carrinho de compras. O `basket` é formado pelo objeto `line_itens`, uma listagem de itens ou produtos.

Propriedades do objeto basket:

|   Propriedade                |   Chave JSON  |   Tipo |    Descrição                   |
|   -----------                | -----------   | ------ |  -------------------------     |
|   Moeda                      | currency      | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos. |
|   Total                      | total         | Number |  Custo total dos itens ou produtos que foram selecionados para compra.  |  
|   Itens                      | line_items    | Array  |  Os items ou produtos presentes no seu carrinho ou cesta de compras.   |

Propriedades de cada objeto `product`:

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                      |
|   -----------     | ----------- | ------ |  -------------------------        |  
|   Descrição       | description | String |  Descrição do item ou produto.    |  
|   Categoria       | category    | String |  Nome da categoria do produto.    |  
|   Subcategoria    | subcategory | String |  Nome da subcategoria do produto. |
|   Moeda           | currency    | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos. |
|   Preço           | unit_price  | Number |  O preço unitário do produto, sem considerar desconto ou promoções. |
|   Sku Code        | sku_code    | String |  Código do produto.               |


###Checkout

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "basket": {
            "currency": "GBP",
            "total": 280,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>",
                        "description": "Description about this product",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 130
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>",
                        "description": "Another description",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 150
                    }
                }
            ]
        }
    };
</script>
```

- O objeto `basket` descreve o estado atual do carrinho de compras. O `basket` é formado pelo objeto `line_itens`, uma listagem de itens ou produtos.

Propriedades do objeto basket:

|   Propriedade                |   Chave JSON  |   Tipo |    Descrição                   |
|   -----------                | -----------   | ------ |  -------------------------     |
|   Moeda                      | currency      | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos. |
|   Total                      | total         | Number |  Custo total dos itens ou produtos que foram selecionados para compra.  |  
|   Itens                      | line_items    | Array  |  Os items ou produtos presentes no seu carrinho ou cesta de compras.   |

Propriedades de cada objeto `product`:

|   Propriedade     |  Chave JSON |   Tipo |    Descrição                      |
|   -----------     | ----------- | ------ |  -------------------------        |  
|   Descrição       | description | String |  Descrição do item ou produto.    |  
|   Categoria       | category    | String |  Nome da categoria do produto.    |  
|   Subcategoria    | subcategory | String |  Nome da subcategoria do produto. |
|   Moeda           | currency    | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos. |
|   Preço           | unit_price  | Number |  O preço unitário do produto, sem considerar desconto ou promoções. |
|   Sku Code        | sku_code    | String |  Código do produto.               |

###Transação

O objeto `transaction` descreve a liquidação de um compra através da confirmação de pagamento ou recebimento.

Exemplo de código Javascript:

```js
<script>
    window.universal_variable = {
        "page" : {
            "category" : "<pageCategory>"
        },
        "user" : {
            "user_id" : "<userID>"
        },
        "transaction": {
            "order_id": "123456",
            "currency": "GBP",
            "payment_type": "Visa Credit Card",
            "total": 280,
            "line_items": [
                {
                    "product": {
                        "sku_code": "<sku>",
                        "description": "Description about this product",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 130
                    }
                },
                {
                    "product": {
                        "sku_code": "<sku2>",
                        "description": "Another description",
                        "category": "Shoe",
                        "subcategory": "Heels",
                        "unit_price": 150
                    }
                }
            ]
        }
    };
</script>
```

Propriedades do objeto transaction:

|   Propriedade         |  Chave JSON   |  Tipo  |    Descrição                      |
|   -----------         | -----------   | ------ |  -------------------------        |  
|   ID do pedido        | order_id      | String |  Um id único para a transação.    |  
|   Moeda               | currency      | String |  O código [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217){:target="_blank"} para moeda utilizado em preços de produtos      |
|   Tipo de pagamento   | payment_type  | String |  Método de pagamento, ex. Visa, PayPal, Master, Boleto, etc.  |
|   Total               | total         | Number |  Custo total da transação.  |  
|   Descrição           | description   | String |  Breve descrição sobre o item ou produto.  |  
|   Categoria           | category      | String |  Nome da categoria do item ou produto.  |  
|   Subcategoria        | subcategory   | String |  Nome da subcategoria do item ou produto |
|   Preço               | unit_price    | Number |  O preço unitário do produto, sem considerar desconto ou promoção. |
|   Sku Code            | sku_code      | String |  Código do produto.                |
|   Itens               | line_items    | Array  |  Os items presentes no seu carrinho ou cesta de compras.   |
|   Produto             | product       | Objeto |  O produto que foi adicionado no transação |


###Exemplo de codificação em javascript (UV Home):

Crie um novo documento no seu editor de textos para código favorito, copie e cole o código abaixo e salve o documento como exemplo-uv.html. Abra a página recém criada em um navegador de sua preferência. No navegador, abra o **devtool**, ferramenta de desenvolvimento e inspeção através das opções de menu ou utilize a tecla F12 (Para a maioria dos navegadores). Procure pela aba **Console** e verifique o resultato, veja que o navegador exibirá o objeto global **window.universal_variable** com todas as propriedades definidas.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exemplo de codificação</title>
    <script type="text/javascript">
        /* 
        * Os valores abaixo são apenas para exemplificar a construção da uv, todos esses valores deverão ser capturados do seu 
        * site e preenchidos dinamicamente como comentado na introdução
        */
        window.universal_variable = window.universal_variable || {}; //inicializando o namespace window.universal_variable
        window.universal_variable.page = {
            'category':'home' //definindo o valor do page.category
        };
        window.universal_variable.user = {
            'user_id':'222536444' //definindo o valor do user.user_id
        };
    </script>
</head>
<body>
    <script type="text/javascript">
        //console.log utilizado apenas para validar a criação da UV
        console.log(window.universal_variable);
    </script>
</body>
</html>
```