//Shape - superclasse
function Shape() {
	this.x = 0;
	this.y = 0;
	this.move = function() { //referências de propriedades são resolvidas primeriramente no objeto, recorrendo à inspeção do protótipo apenas se isso falhar. Este método irá sobreescrever o método do protótipo.
		console.log('move constructor');
	};
}

//método da subclasse
Shape.prototype.move = function(x, y) {
	this.x += x;
	this.x += y;
	console.info('Shape moved');
};

//Rectangle - subclasse
function Rectangle(){
	Shape.call(this);
}


//Subclasse extende superclasse
Rectangle.prototype = Object.create(Shape.prototype);

console.log('Sem referência ao Rectangle', Rectangle.prototype.constructor)

Rectangle.prototype.constructor = Rectangle;

console.log('Com referência ao Rectangle', Rectangle.prototype.constructor)

var rect = new Rectangle();

console.log('Rect é uma instância de Rectangle?', rect instanceof Rectangle);
console.log('Rect é uma instância de Shape?', rect instanceof Shape);
rect.move(1, 1); //Saída: 'Shape moved'.