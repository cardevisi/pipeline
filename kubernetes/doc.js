Tutorial para instalação do kubernetes localmente
https://rominirani.com/tutorial-getting-started-with-kubernetes-on-your-windows-laptop-with-minikube-3269b54a226#.nsbik2gx8
https://rominirani.com/tutorial-getting-started-with-kubernetes-on-your-windows-laptop-with-minikube-3269b54a226#.51w131h1e

Comando para listar as versões disponíveis no minikube
minikube get-k8s-versions

Comando utilizado para criar o namespace de um novo sandbox
kubectl create namespace l-dev-techops-sandbox

Comando para iniciar a construção do kubernetes em seu ambiente local.
minikube start --kubernetes-version="v1.4.0" --vm-driver="virtualbox" --show-libmachine-logs --alsologtostderr

Comando para rodar um pod 
kubectl.exe run hello-nginx --image=nginx --port=80 deployment "hello-nginx" created
kubectl.exe run casinodejs --image=cardevisi/casinodejs:2.0 - RODOU
kubectl.exe run hello-casinodejs:2.0 --image=cardevisi/casinodejs:2.0 --port=3000 deployment "hello-casinodejs:2.0" created

Comando para expor um deployment como servico
kubectl.exe expose deployment hello-nginx --type=NodePort service "hello-nginx" exposed
kubectl.exe expose deployment hello-casinodejs --type=NodePort service "hello-casinodejs" exposed
kubectl expose deployment casinodejs --port=3000 --target-port=3000 - RODOU

Comando para exibir servicos
kubectl get service

Comando para exibir e descrever servicos
kubectl describe service hello-nginx

Comando para exibir a url de um servico kubernetes
minikube service --url=true hello-nginx

Comando para exibir um service kubernetes no navegador
minikube service hello-nginx

Stopping a localhost minikube
minikube stop

Comando para identificar os ip do cluster-info
kubectl proxy

Comando para remover o minikube
minikube delete

Exibi o contexto atual configurado no arquivo config
kubectl config get-contexts

Descreve a configuração de um pod
kubectl describe pod casinode2-2637088120-jjmbb

Busca os deployments
kubectl get deployments

Delete deploy
kubectl delete deployment casinode

Comando para executar comandos dentro do container
kubectl exec casinodejs-3894462151-g9xbz -c casinodejs -i -t -- sh
