var webshot = require('webshot');
var fs = require('fs');
const path = require("path");
    
    var rootDir = './src';
    
    function recursive(dir)  {
        var dirs = [];
        var files = fs.readdirSync(dir).filter(function(file) {
            var paths = path.join(dir, file)
            if (fs.statSync(paths).isDirectory()) {
                dirs.push(paths);
                recursive(paths);
            } 
        });
        return dirs;
    }

    function readFiles() {
        var dir = './src/';
        var files = fs.readdirSync(dir).filter(function(file) {
            var paths = path.join(dir, file)
            if (fs.statSync(paths).isFile()) {
                console.log(file);
            }
        });
    }

    readFiles();
    
    var folders = recursive(rootDir);
    
    for (var key in folders) {

        var currentFolder = folders[key];
        var files = fs.readdirSync(currentFolder);

        for (var key in files) {
            var fileName = files[key];
            var paths = path.join(currentFolder, fileName);

            //console.log('build/'+currentFolder);

            var read = fs.readFileSync(paths, 'utf8');
            var image = fs.readFileSync('./src/icon-netshoes.jpg');
            var base64data = new Buffer(image).toString('base64');

            //console.log(base64data);

            read = read.replace('${dataset.bgColor.value}', '#fff');
            read = read.replace('${dataset.titleColor.value}', '#025EC7');
            read = read.replace('${dataset.urlColor.value}', '#008200');
            read = read.replace('${dataset.descColor.value}', '#000000');
            read = read.replace('${dataset.imagem.value}', 'data:image/png;base64,'+base64data);
            read = read.replace('${dataset.titulo.value}', 'Lançamento Netshoes');
            read = read.replace(/\$\{dataset\.descricao\.value\}/gm, 'Tênis Converse ALL... a partir de 4x de R$27,61');
            read = read.replace('${dataset.url.value}', 'Netshoes.com.br');

            //console.log("READ", read);
            //console.log(read);
            
            var myLines = read.toString().match(/^.+$/gm);
            //console.log("myLines", myLines);

            for (var key in myLines) {
                if(myLines[key].indexOf('"ad.size') != -1) {
                    //console.log("result", result.length, result[1], result[2]);
                    
                    var regex = /content=\"width\=(\d*)\,height\=(\d*)\"/gm;
                    var result = regex.exec(myLines[key]);
                    var options = {
                        quality: 85,
                        screenSize: {
                            width: result[1], 
                            height: result[2]
                        },
                        shotSize: {
                            width: result[1], 
                            height: result[2]
                        },
                        siteType: 'html',
                        userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us)' + ' AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g',
                        onLoadFinished: function() {
                            console.log('loaded');
                        }
                    }
                }
            }

            
            //var str = '<meta name="ad.size" content="width=364,height=90">';
            //var str = read.toString().match(/content=\"width\=(\d*)\,height\=(\d*)\"/gm);
            //var teste = str.split('=');
            //console.log(typeof str, str);

            //console.log(read.match(/^\<meta\s*name\=\"ad.size\"\s*content\=\"width\=(\d*)\,height\=(\d*)\"\>$/)[2]);

            //console.log(read.indexOf('content'));
            var dest = path.join('./build/', currentFolder, '/');
            //if (!fs.existsSync(pathV)) fs.mkdirSync(pathV,'0777', true);

            //console.log(dest+fileName.replace('.html', '')+'.png');
            
            webshot(read, dest+fileName.replace('.html', '')+'.png', options, function(err) {
                console.log('Imagem gerada com sucesso: ', fileName, err);
            });
            

        
        }
    }
    


    /*files.forEach(file => {
                var paths = path.join(basePath, file);
                
                console.log("paths", paths);

                var read = fs.readFileSync(paths.toString(), 'utf8');
                console.log(read);
                webshot(read, file.replace('.html', '')+'.png', options, function(err) {});
            });*/


/*gulp.src(folders[key]+"/*.html", function () {})
.pipe(gulp.dest("build/"+folders[key]));*/
//var read = fs.readFileSync('template-1-ad-364x90-b-728x90.html', 'utf8');
//console.log(read);
//webshot('template-1-ad-364x90-b-728x90.html', 'google.png', options, function(err) {});
//webshot(read, 'google.png', options, function(err) {});

