const gulp = require('gulp');
const webshot = require('gulp-webshot');
const fs = require('fs');
const path = require("path");
const exec = require('child_process').exec;


gulp.task('task', function (cb) {
    var options = {
    continueOnError: false, // default = false, true means don't emit error event
    pipeStdout: false, // default = false, true means stdout is written to file.contents
    customTemplatingThing: "test" // content passed to gutil.template()
  };
  var reportOptions = {
    err: true, // default = true, false means don't write err
    stderr: true, // default = true, false means don't write stderr
    stdout: true // default = true, false means don't write stdout
  }
  exec('node app.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  }, options);
  //exec.reporter(reportOptions);
})


gulp.task('default', function() {
    
    var rootDir = './src';
    
    function recursive(dir)  {
        var dirs = [];
        var files = fs.readdirSync(dir).filter(function(file) {
            var paths = path.join(dir, file)
            if (fs.statSync(paths).isDirectory()) {
                dirs.push(paths);
                recursive(paths);
            } 
        });
        return dirs;
    }
    
    var folders = recursive(rootDir);
    
    for (var key in folders) {

        var currentFolder = folders[key];
        var files = fs.readdirSync(currentFolder);
        for (var key in files) {
            var fileName = files[key];
            var paths = path.join(currentFolder, fileName);
            //console.log('build/'+currentFolder);
            var read = fs.readFileSync(paths, 'utf8');

            
            //console.log("READ", read);
            var myLines = read.toString().match(/^.+$/gm);
            //console.log("myLines", myLines);

            for (var key in myLines) {
                if(myLines[key].indexOf('"ad.size') != -1) {
                    var regex = /content=\"width\=(\d*)\,height\=(\d*)\"/gm;
                    var result = regex.exec(myLines[key]);
                    console.log("result", result.length, result[1], result[2]);
                    var options = {
                        screenSize: {
                            width: result[1], 
                            height: result[2]
                        },
                        shotSize: {
                            width: result[1], 
                            height: result[2]
                        },
                        siteType: 'html',
                        userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us)' + ' AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g'
                    };

                }
            }

            
            //var str = '<meta name="ad.size" content="width=364,height=90">';
            //var str = read.toString().match(/content=\"width\=(\d*)\,height\=(\d*)\"/gm);
            //var teste = str.split('=');
            //console.log(typeof str, str);

            //console.log(read.match(/^\<meta\s*name\=\"ad.size\"\s*content\=\"width\=(\d*)\,height\=(\d*)\"\>$/)[2]);

            //console.log(read.indexOf('content'));
            
            //webshot(read, fileName.replace('.html', '')+'.png', options, function(err) {
              //  console.log('Imagem gerada com sucesso: ', fileName);
           // });
            //makeImages(read, fileName.replace('.html', '')+'.png', options);

        }
    }

    /*function makeImages(file) {
        gulp.src(file)
        .pipe(webshot({ dest:'build/',root:'Theme'}));
    }*/

});