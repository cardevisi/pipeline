WIP = Work in progress ->  Total de trabalho presente no seu quadro Kanban. É toda tarefa que foi iniciada mais ainda não foi finalizada. O Ideal é sempre limitar o Wip, umas das práticas utilizadas é limitar o wip pelo número de responsáveis pelo processo, por exemplo o número de desenvolvedores de uma equipe.

Impedimentos: Mensurar as tarefas que estão com impedimentos.

Throughput: É a quantidade de tarefas finalizadas durante um período de tempo. Quantidade de tarefas entregues.

Cycle time: É a diferença de tempo de quando uma tarefa entra em desenvolvimento, quando está em progresso, até o momento em que a mesma é considerada concluída.

Lide Time: É o tempo que uma tarefa leva para percorrer todo o quadro Kanban. Inicia-se no Backlog, momento em que a tarefa é colocada no quadro e finaliza-se quando a tarefa vai para o Done.