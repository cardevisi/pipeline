Segue trecho do e-mail que detalha o modelo de upload.

Utilizar as seguintes linhas para TAM.

Upload do arquivo de tarifas ADVP.
sh /root/dynad_scripts/sharding/upload.sh /mnt/TMP/sna-files/tam.txt true 3 1296000 tam true || exit 1

Upload do arquivo de prospecção. 
sh /root/dynad_scripts/sharding/upload.sh /mnt/TMP/sna-files/tam-prosp.txt true 12 1296000 tam_prosp true || exit 1

Os parâmetros são:
sh /root/dynad_scripts/sharding/upload.sh [arquivo sna] true [index do banco] [time to live ou ttl] [nome do banco] [flush data] || exit 1

Este comando irá processar o arquivo txt informado e efetuar o upload dos dados no sharding, que por sua vez será replicado automaticamente nas 8 máquinas do pool;

O único limitador ainda existe é que ainda não implementamos o “zookeeper” nestas máquinas, assim, quando houver um novo cliente ainda é preciso cadastrar o registro do datasource nas 8 máquinas (uma única vez). Uma vez implementado o “zookeeper” notificarei a todos e o processo de configuração também será direcionado apenas na máquina digester.

Qualquer dúvida só me avisar.

Abs,
