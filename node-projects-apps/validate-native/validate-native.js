const fs = require('fs');
const path = require('path');
const args = process.argv;
let params = args.slice(2, args.length);

console.log(params, args.length);

let directory = args.slice(2)[0];
let fileName = args.slice(2)[1];

console.log('fileName', fileName);
console.log('directory', directory);

let dir = (directory) ? directory : './';
let files = fs.readdirSync(dir).filter(function(file){
	if(file === fileName) {
		let paths = path.join(dir, file);
		if(fs.statSync(paths).isFile()) {
			let read = fs.readFileSync(file, 'utf8');
			let regex = /\<a\s+class\=\"ad-box\s+\%\%PATTERN:label\%\%\"|native\-box \%\%PATTERN\:label\%\%/g;
			let result = regex.exec(read);
			if (result instanceof Array && result[0]) {
				console.warn('Validação concluída no arquivo:', file);
			} else {
				console.warn('Remova o label utilizado para o teste...', file);
			}
		}
	}
});