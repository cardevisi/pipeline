Problemas com as abordagens:

Como os métodos no prototype ficam expostos no window, qualquer método pode ser facilmente sobreescrito.

Tag Manager 7859
Plugin DfpAsync  2486

Function.prototype.call(this, propertys);
Function.prototype.apply(this, propertys);
Object.create();


(function(){
	var Animal = (function () {
	    function Animal(name) {
	        this.name = name;
	    }
	    // Methods
	    Animal.prototype.doSomething = function () {
	        console.log("I'm a " + this.name);
	    };
	    return Animal;
	})();

	var lion = new Animal("Lion");
	lion.doSomething();
})();


(function(){
	function Banner () {

		$public = this;
		$private = {};

		$public.init = function() {
			console.log('Hello');
		}

		$private.open = function() {

		}
	}

	var banner = new Banner();
	banner.init();
})();


function Product(name, price) {
  this.name = name;
  this.price = price;

  if (price < 0) {
    throw RangeError('Cannot create product ' +
                      this.name + ' with a negative price');
  }
	return this;
}

Product.prototype.teste = function(){
	alert("HelloTest");
}

function Food(name, price) {
  Product.call(this, name, price);
  this.category = 'food';
}

Food.prototype = Object.create(Product.prototype);

function Toy(name, price) {
  Product.call(this, name, price, teste);
  this.category = 'toy';
}

Toy.prototype = Object.create(Product.prototype);

var cheese = new Food('feta', 5);
var fun = new Toy('robot', 40);



//**********************************
function Quad() {}

Quad.prototype.callBg = function(x, y){
	alert('Quad', x, y);
}

// Shape - superclass
function Shape() {
  this.x = 0;
  this.y = 0;
}

// superclass method
Shape.prototype.move = function(x, y) {
  this.x += x;
  this.y += y;
  console.info('Shape moved.');
};

// Rectangle - subclass
function Rectangle() {
  Shape.call(this); // call super constructor.
  Quad.call(this);
}

// subclass extends superclass
Rectangle.prototype = Object.create(Shape.prototype);
Rectangle.prototype.constructor = Rectangle;

var rect = new Rectangle();

console.log('Is rect an instance of Rectangle?', rect instanceof Rectangle);// true
console.log('Is rect an instance of Shape?', rect instanceof Shape);// true
rect.move(1, 1); // Outputs, 'Shape moved.'
rect.callBg(10, 10);


//**********************************
// Shape - superclass
function Shape() {
  this.x = 0;
  this.y = 0;
}

// superclass method
Shape.prototype.move = function(x, y) {
  this.x += x;
  this.y += y;
  console.info('Shape moved.');
};

// Rectangle - subclass
function Rectangle() {
  Shape.call(this); // call super constructor.
  Quad.call(this);
}

// subclass extends superclass
Rectangle.prototype = Object.create(Shape.prototype);
Rectangle.prototype.constructor = Rectangle;

var rect = new Rectangle();

console.log('Is rect an instance of Rectangle?', rect instanceof Rectangle);// true
console.log('Is rect an instance of Shape?', rect instanceof Shape);// true
rect.move(1, 1); // Outputs, 'Shape moved.'


function Banner() {}

Banner.prototype.open = function(x, y){
	alert('Open');
}

Banner.prototype.close = function(x, y){
	alert('Close');
}

Banner.prototype.clickTag = function(x, y){
	alert('clickTag');
}

function Barra(){
	console.log(this);
}
Barra.prototype = Object.create(Banner.prototype);

var barra = new Barra();
barra.open();



