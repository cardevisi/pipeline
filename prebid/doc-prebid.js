Prebid é um biblioteca open source utilizada para Header Bidding.

How Does Prebid Work?
At a high level, header bidding involves just a few steps:

The ad server’s tag on page is paused, bound by a timer, while the Prebid library fetches bids and creatives from various SSPs & exchanges you want to work with.

Prebid passes information about those bids (including price) to the ad server’s tag on page, which passes it to the ad server as query string parameters.

The ad server has line items targeting those bid parameters.

If the ad server decides Prebid wins, the ad server returns a signal to Prebid telling the library to write the winning creative to the page. All finished!