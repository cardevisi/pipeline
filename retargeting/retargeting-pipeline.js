>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Considerar o DOC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

1) Retargeting Dynad: para Categoria, Produto, Carrinho e Checkout.
2) Barra de Ofertas.
3) Dynad Banners ROS.

//BARRA E BANNERS ROS E HOME PROVAVELMENTE //RETARGETING BARRA E CLIQUES:

Configuração de Retargeting:

1) Inclusão da chamada do TagManager do UOL no site:
1.1) A inserção no site deve ser feita nas páginas de categoria, produto, carrinho e checkout a, etc conforme essa documentação aqui: http://tm.uol.com.br/retargeting-tracking.html

Tag do Tag Manager: 

"%ID_GERADO_PELO_FRONT_EDN%" = "z3tohc"

<script src="//tm.jsuol.com.br/uoltm.js?id=%ID_GERADO_PELO_FRONT_END%"></script>

No passo 4 da documentação, utilizar a chamada do TM do UOL que está no TXT em anexo.

4) Setup do XML com os anúncios:

Também precisamos que o cliente passe para nós:

• Tracking que deseja aplicar na campanha, deve seguir o modelo já adotado na ferramenta de analytics dele;
• XML/Feed com todos os produtos que deseja veicular na campanha de Retargeting;
• Definirmos um anúncio default: esse anúncio servirá para criarmos a campanha de Retargeting. Esse anúncio é um “backup”, e será exibido somente em situações de lentidão de conexão do usuário, etc. Segue as mesmas especificações são as mesmas de um anúncio “normal” de UOL Cliques. Aqui seguem as specs http://publicidade.uol.com.br/formatos/web/uol-cliques.html

5) Conversão UOL Cliques (MENSURAÇÃO DE CONVERSÕES).

O cliente deve colocar a chamada do TM do UOL na página final de compra. Para detalhes da implementação, ele pode conferir no PPT anexo.

6) DYNAD TRACKING:

Formato do código gerado para o cliente:
//*********************************************************
//	ANO   MÊS    DIA  INDEXADOR (Este valor é utilizado para )
//	2015  11	 30   02
//*********************************************************

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Considerar o DOC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>