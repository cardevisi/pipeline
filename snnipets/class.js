(function(window){
	function Class (config) {
		console.log(config);
	};

	Class.prototype.init = function(first_argument) {
		console.log('init in Class');
	};

	Class.prototype.teste = function(first_argument) {
		console.log('teste in Class');
	};
	
	window.Class = Class;
})(window);

(function(window){
	function Banner() {
		Class.call(this, {'width':100});
	}

	Banner.prototype = Object.create(Class.prototype);

	Banner.prototype.init = function(first_argument) {
		alert('init in Banner');
	};

	Banner.prototype.teste = function(first_argument) {
		alert('teste in Banner');
	};

	window.Banner = Banner;
})(window);


