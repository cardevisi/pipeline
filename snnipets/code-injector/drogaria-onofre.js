(function() {
    "use strict";
    window.triggerUOLTM = window.triggerUOLTM || [];    
    var dataLayerLocal, pageType, uVar, i, x, skuCodes, skuCode, products, name, names, quantity, eventName, count = 0;
    var TOTAL_TIME = 2000;

    var DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            var obj = [];
            for (var i = 0; i <= arguments.length; i++) {
                if (typeof arguments[i] === 'object') {
                    message.push(JSON.stringify(arguments[i]));
                } else {
                    message.push(arguments[i]);
                }
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };

    function callByTime(daley, callback) {
        delay = (delay) ? delay : 0;
        setTimeout(callback, delay);
    }
    
    function CheckDataLayer() {
        DebugTracking.showMessage('::: CheckDataLayer :::');
        var count = 0;
        function loop() {
            for (i in window.dataLayer) {
                if (!!window.dataLayer[i].hasOwnProperty('page') && !!window.dataLayer[i].page.hasOwnProperty('pageType')) {
                    pageType = window.dataLayer[i].page.pageType;
                    if(pageType === 'product' || pageType === 'checkout' || pageType === 'list' || pageType === 'cart' || pageType === 'payment' || pageType === 'purchase') {
                        dataLayerLocal = window.dataLayer[i];
                    }
                    eventName = (pageType === 'purchase' || pageType === 'payment') ? 'checkout' : pageType;
                    if (typeof dataLayerLocal === 'object') {  
                        break;
                    }
                }
            }
            DebugTracking.showMessage(eventName, dataLayerLocal);
            
            if (typeof dataLayerLocal === 'object') {
                init();
            } else if(count < TOTAL_TIME){
                DebugTracking.showMessage('::: FIND DATALAYERLOCAL :::');
                setTimeout(loop, 1000);
                count++;
            }
        }
        loop();
    }
  
    function init(){
        DebugTracking.showMessage('::: INIT :::');
        try {
            uVar = {
                "page": {
                    'type': pageType
                },
                "user" : {
                    "user_id" : ""
                }
            };
            switch (pageType) {
                case "product":
                    var product = dataLayerLocal.product;
                    uVar.product = {};
                    uVar.product.sku_code = product.productId;
                    uVar.product.description = product.productName;
                    uVar.product.category = product.productCategory;
                    uVar.product.subcategory = product.productSubCategory;
                    uVar.product.unit_price = product.productPrice;
                    uVar.product.currency = "";
                break;
                case "list":
                    products = [];
                    var list = dataLayerLocal.list;
                    for (i = 0, x = list.length; i < x; i++) {
                        products.push({
                            "sku_code": list[i].productId.toString(),
                            "description": list[i].productName,
                            "category": "",
                            "subcategory": "",
                            "unit_price": ""
                        });
                    }
                    uVar.listing = {};
                    uVar.listing.items = products;
                break;
                case "cart":
                case "payment":
                    products = [];
                    var items = dataLayerLocal.cart.items;
                    for (i = 0, x = items.length; i < x; i++) {
                        products.push({
                            "product": {
                                "sku_code": items[i].productId.toString(),
                                "description": items[i].productName,
                                "category": items[i].productCategory,
                                "unit_price": items[i].productPrice.toString(),
                                "subcategory": items[i].productSubCategory,
                                "currency": ""
                            }
                        });
                    }
                    uVar.basket = {},
                    uVar.basket.line_items = products;
                break;
                case "purchase":
                    products = [];
                    quantity = [];
                    var purchase = dataLayerLocal.purchase;
                    for (i = 0, x = purchase.items.length; i < x; i++) {
                        var item = purchase.items[i];
                        products.push({
                            "product": {
                                "sku_code": item.productId.toString(),
                                "description": item.productName,
                                "category": item.productCategory,
                                "unit_price": item.productPrice.toString(),
                                "subcategory": item.productSubCategory,
                                "quantity": item.productQuantity,
                                "currency": ""
                            }
                        });
                    }
                    uVar.basket = {};
                    uVar.basket.line_items = products;

                    uVar.transaction = {};
                    uVar.transaction.order_id = purchase.transactionId;
                    uVar.transaction.total = purchase.transactionValue;
                    uVar.transaction.line_items = products;
                break;
                default:
                    window.console.warn("Erro ao cadastrar universal_variable, dataLayer possui pageType desconhecido: " + pageType);
                break;
            }

            window.universal_variable = uVar;
            
            if (typeof eventName === 'string') {
                DebugTracking.showMessage(':::TRIGER :::', 'uvar-'+eventName+'-created');
                window.triggerUOLTM.push({"eventName": "uvar-"+eventName+'-created'});
            }
        } catch (e) {
            window.console.warn("Erro ao cadastrar universal_variable, erro com dataLayer: " + e);
        }
    }
  
   
    CheckDataLayer();

})();