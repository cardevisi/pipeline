(function(){
   !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    
    //****************
    //FB_PIXEL_ID
    //****************
    fbq('init', '1215367275155199');
    
    var uv = window.universal_variable;
  
    function getSkuList(list) {
        var skuList = [];
        for (var key in list) {
            if (list[key].product) {
                if (skuList.length <= 10) skuList.push(list[key].product.sku_code.toString())
            }
        }
        return skuList;
    }

    var skulist = (uv.transaction && uv.transaction.line_items) ? getSkuList(uv.transaction.line_items) : '';

    if (uv && uv.page && (uv.page.type === "Home" || uv.page.type === "Content" || uv.page.type === "Category")) {
        var category = (uv.page.category) ? uv.page.category.toString() : '';
        fbq('track', 'ViewContent', {
            content_ids : skulist,
            content_category : category,
            content_type : uv.page.type.toString()
        });
    }

    if (uv && uv.page && uv.page.type === "Basket") {
        var category = (uv.page.category) ? uv.page.category.toString() : '';
        fbq('track', 'AddToCart', {
            content_ids : skulist,
            content_category : category,
            content_type : uv.page.type.toString()
        });
    } 

    if (uv && uv.page && uv.page.type === "Conclusion" && !uv.transaction) {
        fbq('track', 'CompleteRegistration');
    }

    if (uv && uv.page && uv.page.type === "Conclusion" && uv.transaction) {
        var total = (/^\s*$/.test(uv.transaction.total)) ? '0.00' : uv.transaction.total;
        var category = (uv.page.category) ? uv.page.category.toString() : '';
        fbq('track', 'Purchase', {
          content_ids: skulist,
          content_category : category,
          content_type : uv.page.type.toString(),
          value: total,
          currency: uv.transaction.currency
        });
    }
})()