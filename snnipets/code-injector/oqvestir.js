(function() {
  "use strict";
    var dataLayerLocal, pageType, uVar, i, x, skuCodes, skuCode, products, name, names, quantity, log, count = 0;

    var DebugTracking = {
        setDebug : function (){
            localStorage.setItem('debug_tm', 'true');
        },
        getDebug : function (){
            return localStorage.getItem('debug_tm');
        },
        clearDebug : function (){
            localStorage.removeItem('debug_tm');
        },
        showMessage : function () {
            var message = [];
            for (var i = 0; i <= arguments.length; i++) {
                message.push(arguments[i]);
            }
            if (this.getDebug() === 'true') console.log(message.join(' '));
        }
    };
    
    function checkUvars() {
      
      DebugTracking.showMessage('checkUvars');

      function loop() {

        for (i in window.dataLayer) {
          if (!!window.dataLayer[i].event) {
            
            if(window.dataLayer[i].event === 'transactionComplete' && !!window.dataLayer[i].hasOwnProperty('transactionId') && window.location.pathname.indexOf('confirmacao') !== -1) {
                dataLayerLocal = window.dataLayer[i];
                log = 'checkout';
            } else if (window.dataLayer[i].event === 'catalog_view' && !!window.dataLayer[i].hasOwnProperty('category_name') && window.location.pathname.indexOf('.html') === -1) {
                dataLayerLocal = window.dataLayer[i];
                log = 'category';
            } else if (window.dataLayer[i].event === 'product_view' && !!window.dataLayer[i].hasOwnProperty('product_name') && window.location.pathname.indexOf('.html') !== -1) {
                dataLayerLocal = window.dataLayer[i];
                log = 'product';
            } else if(window.dataLayer[i].event === 'basket_view' && window.location.pathname.indexOf('sacola') !== -1) {
                dataLayerLocal = window.dataLayer[i];
                log = 'basket';
            }
          }
          if (typeof dataLayerLocal === 'object') {  
            break;
          }
        }
        
        if (typeof dataLayerLocal === 'object') {
            DebugTracking.showMessage(log, dataLayerLocal);
            init();
        } else if(count < 2000){
            DebugTracking.showMessage('::: FIND DATALAYERLOCAL :::');
            setTimeout(loop, 2000);
            count++;
        }

      }
      loop();
    }
  
function init(){
    DebugTracking.showMessage('::: INIT :::');
    try {
        pageType = dataLayerLocal.pageCategory || dataLayerLocal.event;
        uVar = {
            "page": {},
            "user" : {
                "user_id" : ""
            }
        };
        switch (pageType) {
            case "product_view":
                uVar.page.type = pageType;
                uVar.product = {},
                uVar.product.sku_code = dataLayerLocal.product_id;
                uVar.product.description = dataLayerLocal.product_name;
             break;
            case "catalog_view":
                products = [];
                var name = dataLayerLocal.category_name;
                for (i = 0, x = dataLayerLocal.products_list_id.length; i < x; i++) {
                    products.push({
                        "sku_code": dataLayerLocal.products_list_id[i],
                        "description": name
                    });
                }
                uVar.page.type = pageType;
                uVar.listing = {};
                uVar.listing.items = products;
            break;
            case "basket_view":
                products = [];
                for (i = 0, x = dataLayerLocal.products.length; i < x; i++) {
                    products.push({
                        "product":{
                            "sku_code": dataLayerLocal.products[i].id,
                            "description": dataLayerLocal.products[i].name
                        }
                    });
                }
                uVar.basket = {},
                uVar.page.type = pageType;
                uVar.basket.line_items = products;
            break;
            case "transactionComplete":
                products = [];
                for (i = 0, x = dataLayerLocal.transactionProducts.length; i < x; i++) {
                    products.push({
                        "product":{
                            "sku_code": dataLayerLocal.transactionProducts[i].id,
                            "description": dataLayerLocal.transactionProducts[i].name,
                            "quantity": dataLayerLocal.transactionProducts[i].quantity
                        }
                    });
                }
                uVar.page.type = pageType;
                uVar.basket = {};
                uVar.basket.line_items = products;
                uVar.transaction = {};
                uVar.transaction.order_id = dataLayerLocal.transactionId;
                uVar.transaction.total = dataLayerLocal.transactionTotal;
                uVar.transaction.line_items = products;
            break;
            default:
                window.console.warn("Erro ao cadastrar universal_variable, dataLayer possui pageType desconhecido: " + pageType);
            break;
        }
        window.universal_variable = uVar;
        DebugTracking.showMessage('::: TRIGGER :::', 'uvar-created-'+log);
        if (typeof log === 'string') {
            window.triggerUOLTM.push({"eventName": "uvar-created-"+log});
        }
    } catch (e) {
        window.console.warn("Erro ao cadastrar universal_variable, erro com dataLayer: " + e);
    }
  }
  
function trigger() {
    var count = 0;
    var lastDocumentURL = document.URL;
    window.triggerUOLTM = window.triggerUOLTM || [];

    function loop() {
        if( document.URL != lastDocumentURL ) {
            DebugTracking.showMessage('::: CALL TRIGGER trigger-uvar-create :::');
            lastDocumentURL = document.URL;
            window.triggerUOLTM.push({"eventName": "trigger-uvar-create"});
        } else if (count < 3600){
            DebugTracking.showMessage('::: TRIGGER V29 :::');
            setTimeout(loop, 2000);
            count++;
        }
    }
    loop();
}

trigger();
checkUvars();

})();