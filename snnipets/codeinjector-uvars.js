

  try {
    window.triggerUOLTM = window.triggerUOLTM || [];
    var dataLayerLocal, pageType, uVar, i, x, skuCodes, skuCode, products, name, names, quantity, category, subcategory, unit_price, currency, userId, triggerType;
    for (i in window.dataLayer) {
      if (!!window.dataLayer[i].hotel_pagetype ) {
            dataLayerLocal = window.dataLayer[i];
            break;
      }
    }
    pageType = dataLayerLocal.hotel_pagetype || dataLayerLocal.event;
    userId = (dataLayerLocal.CustomerID) ? dataLayerLocal.CustomerID : "";
    
    uVar = {
      "page": { },
        "user": {
            "user_id": userId
        }
    };
    switch (pageType) {
      case "product":
                skuCode = dataLayerLocal.offer_id;
                name = dataLayerLocal.nome;
                category = dataLayerLocal.offer_category;
                subcategory = '';
                unit_price = dataLayerLocal.offer_value.toString();
                currency = '';
                uVar.page.category = pageType;
                uVar.product = {};
                uVar.product.sku_code = skuCode;
                uVar.product.description = name;
                uVar.product.category = category,
                uVar.product.subcategory = subcategory,
                uVar.product.unit_price = unit_price,
                uVar.product.currency = currency
                triggerType = "uvar-products-created";
                break;
      case "category":
                skuCodes = [];
                products = [];

                products.push({
                    "sku_code": dataLayerLocal.idDestino,
                    "description": dataLayerLocal.offer_search,
                    "category": "",
                    "subcategory": "",
                    "unit_price": 0
                });
                uVar.page.category = pageType;
                uVar.listing = {};
                uVar.listing.items = products;
                triggerType = "uvar-category-created";
                break;

        case "purchase":
            products = [];
            products.push({
                "product" : {
                    "sku_code": dataLayerLocal.sku,
                    "description": dataLayerLocal.nome_produto,
                    "category": dataLayerLocal.offer_category,
                    "subcategory": dataLayerLocal.categoria_sub,
                    "unit_price": dataLayerLocal.valor_unitario.toString()
                }
            });

            uVar.page.category = pageType;
            uVar.basket = {};
            uVar.basket.currency = "";
            uVar.basket.total = dataLayerLocal.valor_total;
            uVar.basket.line_items = products;
            triggerType = "uvar-checkout-created";
            break;

        case "cart":
                names = [];
                skuCodes = [];
                products = [];
                products.push(
                    {
                    "product" : {
                        "sku_code": dataLayerLocal.sku,
                        "description": dataLayerLocal.nome_produto,
                        "category": dataLayerLocal.offer_category,
                        "subcategory": dataLayerLocal.categoria_sub,
                        "unit_price": dataLayerLocal.order_value.toString()
                    }
                });
                uVar.page.category = pageType;
                uVar.basket = {};
                uVar.basket.line_items = products;
                uVar.basket.currency = "";
                uVar.basket.total = dataLayerLocal.valor_total;
                triggerType = "uvar-cart-created";
                break;
              default:
                window.console.warn("Erro ao cadastrar universal_variable, dataLayer possui pageType desconhecido: " + pageType);
                break;
    }
    window.universal_variable = uVar;
    window.triggerUOLTM.push({"eventName":triggerType});
  } catch (e) {
    window.console.warn("Erro ao cadastrar universal_variable, erro com dataLayer: " + e);
  }
