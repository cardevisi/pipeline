var gulp = require("gulp"),
    include = require("gulp-include"),
    uglify = require("gulp-uglify"),
    replace = require("gulp-replace-task"),
    rename = require("gulp-rename"),
    src = "src/js",
    html = "src/html",
    build = "build/",
    classFiles = ["Tween"];

    for(var t in classFiles) {
        var classFile = classFiles[t];
        gulp.task(classFile, function(){
            console.log("-- Compilando classFile '"+classFile+"'");
            gulp.src(src+'/Tween.js')
                .pipe(include())
                .pipe(gulp.dest(build))
            gulp.src(html+"/*.html")
                .pipe(gulp.dest(build));
            gulp.src(src+'/Init.js')
                .pipe(include())
                .pipe(gulp.dest(build))
        });
    }

gulp.task("build", classFiles);
gulp.task("default", ["build"]);