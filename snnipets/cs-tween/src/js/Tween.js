(function(window) {
	'use strict';
	function Tween(selector, time, property, callBack, delay) {
		var timeLineActions = [];

		var obj = document.querySelector(selector);

		if(obj === null) {
			return;
		} else {
			console.log('could not found element html');
		}

		try {
			var style = window.getComputedStyle(obj, '');
			for (var i=0; i<style.length; i++) {
		    	if(style[i] === 'position') {
		    		var value = style.getPropertyValue(style[i]);
		    		if(value !== 'relative' || value !== 'absolute') {
		        		//console.log( style[i] + ':' + style.getPropertyValue(style[i]));
		    			//obj.style.position = 'relative';
		    		}
		    	}
	    	}
		} catch(e) {
			console.log('window.getComputedStyle');
		}

		delay = (delay) ? delay : 0;
		//obj.style.transition = 'all '+time+'s ease-out';
		//obj.style.transitionTimingFunction: ease-out;
			
		setTimeout(function() {
			var timeLineActions = [];
			for (var prop in property) {
				if(prop === 'x' || prop === 'y') {
					prop = 'transform';
				} 
				//console.log(timeLineActions.indexOf('transfom'));
				if(timeLineActions.indexOf('transform') != -1) {
					continue;	
				}
				timeLineActions.push(prop);
			}

	//				console.log(timeLineActions);
			//obj.style.top = '0';
	    	//obj.style.left = '0';
	//	        	console.log('PROP', prop);

			for (var prop in property) {
				obj.style.transitionProperty = timeLineActions.join(',');
	        	obj.style.transitionDuration = time+'s';
				obj.style.transitionDelay = delay;
				obj.style.transitionTimingFunction =  "ease-out";
				//obj.style.transitionTimingFunction =  "cubic-bezier(0,0.5,0.75,0.75)";
				//cubic-bezier (px2, py2, px2, py2);
				
				if(prop === 'x' || prop === 'y') {
					var x = (property['x']) ? property['x'] : 0;
					var y = (property['y']) ? property['y'] : 0;
					obj.style.transform = 'translate3d('+x+','+y+',0)';
					continue;
				}

				obj.style[prop] = property[prop];
				obj.addEventListener("transitionend", handleAnimation, false);
				obj.addEventListener('webkitTransitionEnd',handleAnimation},false);
				obj.addEventListener('oTransitionEnd',handleAnimation},false);
				obj.addEventListener('animationend',handleAnimation},false);
				obj.addEventListener('webkitAnimationEnd',handleAnimation},false);
				obj.addEventListener('MSAnimationEnd',handleAnimation},false);
			}
			function handleAnimation(e) {
				//console.log('End Animation', e);
				callBack(selector);
			}
		}, delay*1000);

	}

	window.Tween = Tween;

})(window)