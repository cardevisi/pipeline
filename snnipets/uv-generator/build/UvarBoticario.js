(function(){
    try {
        var index = 0;
        var skus = [];
        var uvar = {
        "page": {
            "type": "Category",
            "breadcumb": {}
        },
            "listing": {}
        };  
        if(window.dataLayer && window.dataLayer[index] && window.dataLayer[index].shelfProductIds) {
            var skuList = window.dataLayer[index].shelfProductIds;
            for(var key in skuList) { 
                skus.push({"sku_code": skuList[key]});
            }
            uvar.listing.items = skus;
        }
        window.universal_variable = uvar;
        window.triggerUOLTM.push({"eventName":"uvar"});
    } catch (e) {
        console.warn('Não foi possível criar a universal_variable. ' + e.message);
    }
})();