var gulp = require("gulp"),
    server = require('karma').Server,
    include = require("gulp-include"),
    uglify = require("gulp-uglify"),
    replace = require("gulp-replace-task"),
    rename = require("gulp-rename"),
    version = require("gulp-version-number"),
    zip = require("gulp-zip"),
    argv = require('yargs').argv,
    src = "src/js",
    html = "src/html",
    deploy = "deploy",
    build = "build/",
    classFiles = ["CreateDynadPixel","CreateUV"];

    for(var t in classFiles) {
        var classFile = classFiles[t];
        
        gulp.task(classFile, function(){
            console.log("-- Compilando classFile '"+classFile+"'");
            gulp.src(src+'/UVGenerator.js')
                .pipe(include())
                .pipe(gulp.dest(build))
            gulp.src(html+"/*.html")
                .pipe(gulp.dest(build))
            gulp.src(src+'/UVGenerator.js')
                .pipe(version({
                        /**
                        * Global version value
                        * default: %MDS%
                        * https://github.com/shinate/gulp-version-number
                        */
                        'value' : '%DATE%',
                
                        /**
                         * MODE: REPLACE
                         * eg:
                         *    'keyword'
                         *    /regexp/ig
                         *    ['keyword']
                         *    [/regexp/ig, '%MD5%']]
                         */
                        'replaces' : [
                
                            /**
                             * {String|Regexp} Replace Keyword/Rules to global value (config.value)
                             */
                            '#{VERSION_REPlACE}#',
                
                            /**
                             * {Array}
                             * Replace keyword to custom value
                             * if just have keyword, the value will use the global value (config.value).
                             */    
                            [/#{VERSION_REPlACE}#/g, '%TS%']
                        ],
                
                
                        /**
                         * MODE: APPEND
                         * Can coexist and replace, after execution to replace
                         */
                        'append' : {
                
                            /**
                         * Parameter
                     */
                    'key' : '_v',

                    /**
                     * Whether to overwrite the existing parameters
                     * default: 0 (don't overwrite)
                     * If the parameter already exists, as a "custom", covering not executed.
                     * If you need to cover, please set to 1
                     */
                    'cover' : 0,

                    /**
                     * Appended to the position (specify type)
                     * {String|Array|Object}
                     * If you set to 'all', will apply to all type, rules will use the global setting.
                     * If an array or object, will use your custom rules.
                     * others will passing.
                     * 
                     * eg:
                     *     'js'
                     *     ['js']
                     *     {type:'js'}
                     *     ['css', '%DATE%']
                     */
                    'to' : [

                        /**
                         * {String} Specify type, the value is the global value
                         */
                        'css',

                        /**
                         * {Array}
                         * Specify type, keyword and cover rules will use the global 
                         * setting, If you need more details, please use the object 
                         * configure.
                         *
                         * argument 0 necessary, otherwise passing.
                         * argument 1 optional, the value will use the global value
                         */
                          ['image', '%TS%'],

                        /**
                         * {Object}
                         * Use detailed custom rules to replace, missing items will 
                         * be taken in setting the global completion

                         * type is necessary, otherwise passing.
                         */
                        {
                            'type' : 'js',
                            'key' : '_v',
                            'value' : '%DATE%',
                            'cover' : 1
                        }
                    ]
                },

                /**
                 * Output to config file
                 */
                'output' : {
                    'file' : 'version.json'
                }

                }))
                .pipe(gulp.dest(build));

        });

        gulp.task('zip', function(e){
            console.log('--- Making deploy', argv.name);
            gulp.src(build+"*")
                .pipe(zip(argv.name+'.zip'))
                .pipe(gulp.dest(deploy));
            
        });

        /**
         * Run test once and exit
         */
        gulp.task('test', function (done) {
            new server({
                configFile: __dirname + '/karma.conf.js',
                singleRun: false
            }, done).start();
        });
    }

gulp.task("build", classFiles);
gulp.task("default", ["build"]);