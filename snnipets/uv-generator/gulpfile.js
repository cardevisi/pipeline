var gulp = require("gulp"),
    server = require('karma').Server,
    include = require("gulp-include"),
    uglify = require("gulp-uglify"),
    replace = require("gulp-replace-task"),
    rename = require("gulp-rename"),
    version = require("gulp-version-number"),
    zip = require("gulp-zip"),
    argv = require('yargs').argv,
    src = "src/js",
    html = "src/html",
    deploy = "deploy",
    build = "build/",
    classFiles = ["CreateDynadPixel","CreateUV"];

    for(var t in classFiles) {
        var classFile = classFiles[t];
        
        gulp.task(classFile, function(){
            console.log("-- Compilando classFile '"+classFile+"'");
            gulp.src(src+'/UVGenerator.js')
                .pipe(include())
                .pipe(gulp.dest(build))
            gulp.src(html+"/*.html")
                .pipe(gulp.dest(build))
            gulp.src(src+'/UVGenerator.js')
        });

        gulp.task('zip', function(e){
            console.log('--- Making deploy', argv.name);
            gulp.src(build+"*")
                .pipe(zip(argv.name+'.zip'))
                .pipe(gulp.dest(deploy));
            
        });

        /**
         * Run test once and exit
         */
        gulp.task('test', function (done) {
            new server({
                configFile: __dirname + '/karma.conf.js',
                singleRun: false
            }, done).start();
        });
    }

gulp.task("build", classFiles);
gulp.task("default", ["build"]);