module.exports = function(config) {
    config.set({
        browsers: [
            //'Chrome',
            'PhantomJS'
        ],
        frameworks: [
            'jasmine'
            //'karma-jasmine-html-reporter',
            //'karma-phantomjs-launcher'
        ],
        files: [
            //'src/**/*.js',
            'test/**/*.js',
            'src/**/hello.js'
            //'test/**/*.spec.js'
        ],
        /*junitReporter: {
            outputFile: 'test-results.xml'
        },*/
        port: 8000,
        autoWatch: true,
        background: true
    });
};