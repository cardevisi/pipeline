function CreateDynadPixel() {
    
    var DYNAD_PIXEL = '//t5.dynad.net/lsep/?l=$id$&ord=$ord$&c=$skus$';

    this.create = function(id, uv) {
        var skus  = [];
        var total = 0;
        if(!uv && !uv.basket && uv.basket.line_items) {
            return;
        }

        total = uv.basket.line_items.length;
        for (var i =0; i < total; i++) {
            skus.push(uv.basket.line_items[i].product.sku_code);
        }
        var script = document.createElement('script');
        DYNAD_PIXEL = DYNAD_PIXEL.replace('$ord$', new Date().getTime())
        DYNAD_PIXEL = DYNAD_PIXEL.replace('$skus$', skus);
        DYNAD_PIXEL = DYNAD_PIXEL.replace('$id$', id);
        script.src = DYNAD_PIXEL;
        if(document.body) {
            document.body.appendChild(script);
        }
    }

    return this;
}