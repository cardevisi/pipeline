function CreateUV(type) {
    
    var dynadPixel = new CreateDynadPixel();

    function convertObjectToString(obj) {
        var str = JSON.stringify(obj);
        str = str.substr(1);
        str = str.substring(0, str.length-1);
        return str;
    }

    function createPageObject(pageType) {
        return  {
            "page": {
                "type": pageType.toLowerCase()
            }
        }
    }
    
    function createBasketObject(skuList) {
        return  {
            "basket": {
                "line_items": [{
                    "product":
                        {
                            "sku_code": skuList
                        }
                    }
                ]}
        }
    }

    function createCategoryObject(skuList) {
        return  {
            "listing": {
                "items": 
                    [{
                        "sku_code": skuList
                    }]
            }
        }
    }

    function createTransactionNode(order_id, total, skuList) {
        return  { 
            "transaction": {
                "order_id": order_id,
                "total": total,
                "line_items": 
                    [{
                        "product": {
                            "sku_code": skuList
                        }
                    }]  
            }
        }
    }

    function createProductObject(skuList) {
        return {
            "product": {
                "sku_code": skuList
            }
        }
    }

    function getDataLayerAttribute(array, index, name) {
        var result;
        if(!(array instanceof Array)) {
            return;
        }

        array = (index !== 0) ? array[index] : array;

        for (var key in array) {
            if(key === name) {
                result = array[key];
                result = result.toString();
                return result;
                break;
            }
            
        };
        return null;
    }

    this.init = function(pixelId, pageType, triggerName){
        var uvar = [];
        var transactionId = getDataLayerAttribute(window.dataLayer, 1, 'transactionId');
        var total = getDataLayerAttribute(window.dataLayer, 1, 'transactionTotal');
        var skuList = getDataLayerAttribute(window.dataLayer, 1, 'skuList');
        uvar.push(convertObjectToString(createPageObject(pageType)));

        if(pageType === 'product') {
            uvar.push(convertObjectToString(createProductObject(skuList)));
        } else if(pageType === 'basket') {
            uvar.push(convertObjectToString(createBasketObject(skuList)));
        } else if(pageType === 'checkout') {
            uvar.push(convertObjectToString(createBasketObject(skuList)));
            uvar.push(convertObjectToString(createTransactionNode(transactionId, total, skuList)));
        } else if(pageType === 'category') {
            uvar.push(convertObjectToString(createCategoryObject(skuList)));
        }
        
        uvar.join(',');
        uvar = '{'+uvar.toString()+'}';
        window.universal_variable = JSON.parse(uvar);
        if (pixelId !== 0) dynadPixel.create(pixelId, window.universal_variable);
        window.triggerUOLTM.push({'eventName': triggerName});
    }
    return this;
}