describe('greeter', function () {
    it('should say Hello to the World', function () {
        expect(greet('World')).toEqual('Hello, World!');
    });

    it('Dado que os parametros a e b sejam validos', function () {
        expect(sumValues(5,10)).toEqual(15);
    });

    it('Dado que os parametros a e b sejam invalidos', function () {
        expect(sumValues()).toEqual(false);
    });
});