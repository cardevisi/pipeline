var express = require("express");
var bodyParser = require('body-parser');
var app = express();

module.exports = function(app) {

	var HomeController = require('../../controllers/home'),
		NativeGeneratorController = require('../../controllers/NativeGenerator');

	app.get(
		'/'
		, HomeController.index
	);
	
	app.get(
		'/native-generator'
		, NativeGeneratorController.index
	);
	
	var urlencodedParser = bodyParser.urlencoded({ extended: false});
	app.post('/native-generator', 
		urlencodedParser, 
		NativeGeneratorController.generator
	);
}