var express = require('express'), 
path = require('path'), 
bodyParser = require('body-parser'), 
consign = require('consign'), 
app = express();

app.use(express.static('./public'));
app.set('views','public/html');
app.set('view engine', 'jade');

consign()
	.include('app/routes')
	.into(app);

module.exports = app;