// ------------- Usage ---------------
const gulp = require('gulp');
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');
const include = require("gulp-include");
const nodemon = require('gulp-nodemon');
// -----------------------------------

const gulpif = require('gulp-if');
const prefixer = require('gulp-autoprefixer');
const minifyCSS = require('gulp-csso');
const annotate = require('gulp-ng-annotate');
const plumber = require('gulp-plumber');
const soucermaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const argv = require('yargs').argv;

gulp.task('build', function() {
	gulp.src('./modules/main.js')
	.pipe(include())
	.pipe(gulp.dest('./public/js/'));
});

gulp.task('jshint', function(){
	gulp.src('./modules/**/*.js')
	.pipe(jshint())
	.pipe(jshint.reporter(stylish));
});

gulp.task('start', function() {
	nodemon({
		script: 'server.js',
		ignore: ['./public/'],
		ext: 'js html',
		tasks: ['jshint', 'build']
		//env: {'NODE_ENV': 'development'}
	})
});

gulp.task('default', ['start']);