function Breakpoints() {
    
    var containerAddBreakpoints, formAdBreakpoints, currentForm, inputMinVal, inputMaxVal, breakpointMinValue, breakpointMaxValue, btnSubmit, submitForm;
    
    containerAddBreakpoints = $('#container-add-breakpoints');
    formAdBreakpoints = $('#ad-breakpoints');
    btnSubmit = $('#btn-submit');
    submitForm = new SubmitForm();

    function createRemoveButton(id) {
        var anchor = $('<a></a>');
        anchor.attr('class', 'btn btn-default btn-remove pull-right');
        anchor.css('padding', '3px 10px');
        anchor.on('click', function(e){
            e.preventDefault();
            $('#'+id).remove();
            breakpointMinValue = undefined;
            breakpointMaxValue = undefined;
        });
        return anchor.text('remove');
    }

    function showSubmitButton() {
        if(!btnSubmit.is(':visible')) {
            btnSubmit.show();
        }
        submitForm.init();
    }

    function evaluateMessage() {
        var message;
        if (breakpointMinValue) {
            message = '@media (min-width: ' + breakpointMinValue + ')';
        }
        if (breakpointMaxValue) {
            message +=  ' and (max-width: ' + breakpointMaxValue + ')';
        }
        return message; 
    }

    function configAddPoints() {
        formAdBreakpoints.find('.input-group-addon').on('click', function (e) {
            inputMinVal = formAdBreakpoints.find('input[name=breakpoint-min-value]');
            inputMaxVal = formAdBreakpoints.find('input[name=breakpoint-max-value]');
            currentForm = $(Data.getInstance().getCurrentForm());
            
            if (breakpointMinValue === null || breakpointMinValue !== inputMinVal.val()) {
                breakpointMinValue = inputMinVal.val();
            } else {
                Modal.getInstance().show({'title' : 'Info:', 'text': 'Valor mínimo já existente, digite um novo valor.'});
                return;
            }

            if (breakpointMaxValue === null || breakpointMaxValue !== inputMinVal.val()) {
                breakpointMaxValue = inputMaxVal.val();
            } else {
                Modal.getInstance().show({'title' : 'Info:', 'text': 'Valor máximo já existente, digite um novo valor.'});
                return;
            }

            if (!currentForm) {
                Modal.getInstance().show({'title' : 'Info:', 'text': 'Selecione algum modelo de native dentre as opções acima.'});
            } else if(!breakpointMinValue) {
                Modal.getInstance().show({'title' : 'Info:', 'text': 'Digite um valor mínimo ou máximo para adicionar um Breakpoint.'});
            } else {
                var form = currentForm.clone();
                renameFields(form, breakpointMinValue);

                var formId = form.attr('id')+breakpointMinValue;
                form.attr('id', formId);
                form.find('.panel-title').addClass('pull-left').text(evaluateMessage()).css('padding', '2.5px 0 0 0');
                form.find('.panel-heading').append(createRemoveButton(formId));
                form.find('.panel-heading').attr('data-target', '#breakpoint-form'+ breakpointMinValue);
                form.find('.panel-heading').attr('data-parent', '#breakpoints-forms');
                form.find('.panel-heading').addClass('panel-breakpoint').addClass('clearfix');
                form.find('> div').addClass('panel-breakpoint-primary');
                form.find('.panel-collapse').attr('id', 'breakpoint-form'+ breakpointMinValue);
                
                $('#breakpoints-forms').append(form);
                
                showSubmitButton();
            }
        });
    }

    function renameFields(form, value) {
        var inputs = form.find('input');
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].setAttribute('name', 'breakpointValues'+value+'[]');
        }
    }
    
    this.reset = function() {
        $('#breakpoints-forms').empty();
        breakpointMinValue = undefined;
        breakpointMaxValue = undefined;
    };

    this.init = function() {
        configAddPoints();
    };
}