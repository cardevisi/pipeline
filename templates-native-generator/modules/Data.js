var Data = (function(){
    var instantiated;
    
    function init() {
        return {
            currentForm: undefined,
            getCurrentForm : function() {
                return this.currentForm;
            },
            setCurrentForm : function(value) {
                this.currentForm = value;
            }
        };
    }

    return {
        getInstance : function() {
            if(!instantiated) {
                instantiated = init();
            }
            return instantiated;
        }
    };
})();