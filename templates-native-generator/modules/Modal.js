var Modal = (function(){
    var instantiated;
    
    function init() {
        return {
            modalDialog: $('#modalDialog'),
            show : function(message) {
                if (!message && !message.text && !message.title) {
                    console.log('O Título ou o Texto não foram configurados');
                    return;
                }
                this.modalDialog.modal('show');
                this.message(message);
            },
            hide : function(value) {
                this.modalDialog.modal('hide');
            },
            message: function(message) {
                this.modalDialog.find('.modal-title').text(message.title);
                this.modalDialog.find('.modal-body .message').text(message.text);
            }
        };
    }

    return {
        getInstance : function() {
            if(!instantiated) {
                instantiated = init();
            }
            return instantiated;
        }
    };
})();