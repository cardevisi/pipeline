function SelectForm() {

    var breakPointsFunction = new Breakpoints();
    var namesContainersForms = [
        '#container-vertical-inner-sponsor',
        '#container-vertical-outer-sponsor',
        '#container-horizontal-inner-sponsor',
        '#container-horizontal-outer-sponsor'
    ];
    var modelDescription = $('#model-description > div > div');
    var containerAddBreakpoints = $('#container-add-breakpoints');

    function showModelDescription(index) {
        for (var i = 0; i < modelDescription.length; i++) {
            modelDescription[i].style.display = 'none';
        }
        modelDescription[index].style.display = 'block';
    }

    function showContainerAddBreakpoints() {
        if(!containerAddBreakpoints.is(':visible')) {
            containerAddBreakpoints.show();
        }       
    }

    function hideForm(name) {
        for (var i = 0; i < namesContainersForms.length; i++) {
            if (!validateName(namesContainersForms[i], name)) {
                $(namesContainersForms[i]).hide();
            } else {
                $(namesContainersForms[i]).show();
            }
        }
    }

    function validateName(formId, searchName) {
        var pattern = formId.replace('#container-', '').toString();
        pattern = new RegExp(pattern, 'g');
        return pattern.test(searchName);
    }

    function setCurrentForm(name) {
        for (var i = 0; i < namesContainersForms.length; i++) {
            if (validateName(namesContainersForms[i], name)) {
                var currentForm = namesContainersForms[i].replace('container-', 'config-');
                Data.getInstance().setCurrentForm(currentForm);
            }
        }
    }

    function showForm(name) {
        
        hideForm(name);
        setCurrentForm(name);
        breakPointsFunction.reset();
        
        switch(name) {
            case 'vertical-inner-sponsor':
                $('#config-vertical-inner-collapse').collapse();
                showModelDescription(0);
            break;
            case 'vertical-outer-sponsor':
                $('#config-vertical-outer-collapse').collapse();
                showModelDescription(1);
            break;
            case 'horizontal-inner-sponsor':
                $('#config-horizontal-inner-collapse').collapse();
                showModelDescription(2);
            break;
            case 'horizontal-outer-sponsor':
                $('#config-horizontal-outer-collapse').collapse();
                showModelDescription(3);
            break;
        }
    }

    this.init = function() {
        $('[data-toggle="tooltip"]').tooltip();
        $(".selectContainer input").on("click", function() {
            showForm($(".selectContainer input:checked").val());
            showContainerAddBreakpoints();
        });
    };
}