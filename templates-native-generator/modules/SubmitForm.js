function SubmitForm() {
		
	function getValuesFromInputs() {
		return $('#main-form-generator').serialize();
	}

	function submit(){
		$.ajax({
		  	url: "/native-generator",
		  	type: "POST",
		  	data: getValuesFromInputs(),
		  	dataType: 'json'
		}).done(function(data) {
			Modal.getInstance().show({'title' : 'Info:', 'text': 'Formulário enviado como sucesso!'});
		}).fail(function(error) {
    		Modal.getInstance().show({'title' : 'Info:', 'text': 'Erro ao enviar o formulário'});
  		});
	}

	this.init = function () {
		$('#btn-submit .submit').on('click', function(e) {
			submit();
		});
	};
}