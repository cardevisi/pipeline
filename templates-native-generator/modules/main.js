$(function() {
    
    //=include ../modules/Data.js
    //=include ../modules/Modal.js
    //=include ../modules/Breakpoints.js
    //=include ../modules/SelectForm.js
    //=include ../modules/SubmitForm.js

	function Main() {
    	var breakpoints = new Breakpoints();
    	var selectForm = new SelectForm();

    	this.init = function() {
		    breakpoints.init();
		    selectForm.init();
    	};

    	return this;
    }

    Main().init();

});