$(function() {
    
    var Data = (function(){
    
        var instantiated;
    
        
    
        function init() {
    
            return {
    
                currentForm: undefined,
    
                getCurrentForm : function() {
    
                    return this.currentForm;
    
                },
    
                setCurrentForm : function(value) {
    
                    this.currentForm = value;
    
                }
    
            };
    
        }
    
    
    
        return {
    
            getInstance : function() {
    
                if(!instantiated) {
    
                    instantiated = init();
    
                }
    
                return instantiated;
    
            }
    
        };
    
    })();
    var Modal = (function(){
        var instantiated;
        
        function init() {
            return {
                modalDialog: $('#modalDialog'),
                show : function(message) {
                    if (!message && !message.text && !message.title) {
                        console.log('O Título ou o Texto não foram configurados');
                        return;
                    }
                    this.modalDialog.modal('show');
                    this.message(message);
                },
                hide : function(value) {
                    this.modalDialog.modal('hide');
                },
                message: function(message) {
                    this.modalDialog.find('.modal-title').text(message.title);
                    this.modalDialog.find('.modal-body .message').text(message.text);
                }
            };
        }
    
        return {
            getInstance : function() {
                if(!instantiated) {
                    instantiated = init();
                }
                return instantiated;
            }
        };
    })();
    function Breakpoints() {
        
        var containerAddBreakpoints, formAdBreakpoints, currentForm, inputMinVal, inputMaxVal, breakpointMinValue, breakpointMaxValue, btnSubmit, submitForm;
        
        containerAddBreakpoints = $('#container-add-breakpoints');
        formAdBreakpoints = $('#ad-breakpoints');
        btnSubmit = $('#btn-submit');
        submitForm = new SubmitForm();
    
        function createRemoveButton(id) {
            var anchor = $('<a></a>');
            anchor.attr('class', 'btn btn-default btn-remove pull-right');
            anchor.css('padding', '3px 10px');
            anchor.on('click', function(e){
                e.preventDefault();
                $('#'+id).remove();
                breakpointMinValue = undefined;
                breakpointMaxValue = undefined;
            });
            return anchor.text('remove');
        }
    
        function showSubmitButton() {
            if(!btnSubmit.is(':visible')) {
                btnSubmit.show();
            }
            submitForm.init();
        }
    
        function evaluateMessage() {
            var message;
            if (breakpointMinValue) {
                message = '@media (min-width: ' + breakpointMinValue + ')';
            }
            if (breakpointMaxValue) {
                message +=  ' and (max-width: ' + breakpointMaxValue + ')';
            }
            return message; 
        }
    
        function configAddPoints() {
            formAdBreakpoints.find('.input-group-addon').on('click', function (e) {
                inputMinVal = formAdBreakpoints.find('input[name=breakpoint-min-value]');
                inputMaxVal = formAdBreakpoints.find('input[name=breakpoint-max-value]');
                currentForm = $(Data.getInstance().getCurrentForm());
                
                if (breakpointMinValue === null || breakpointMinValue !== inputMinVal.val()) {
                    breakpointMinValue = inputMinVal.val();
                } else {
                    Modal.getInstance().show({'title' : 'Info:', 'text': 'Valor mínimo já existente, digite um novo valor.'});
                    return;
                }
    
                if (breakpointMaxValue === null || breakpointMaxValue !== inputMinVal.val()) {
                    breakpointMaxValue = inputMaxVal.val();
                } else {
                    Modal.getInstance().show({'title' : 'Info:', 'text': 'Valor máximo já existente, digite um novo valor.'});
                    return;
                }
    
                if (!currentForm) {
                    Modal.getInstance().show({'title' : 'Info:', 'text': 'Selecione algum modelo de native dentre as opções acima.'});
                } else if(!breakpointMinValue) {
                    Modal.getInstance().show({'title' : 'Info:', 'text': 'Digite um valor mínimo ou máximo para adicionar um Breakpoint.'});
                } else {
                    var form = currentForm.clone();
                    renameFields(form, breakpointMinValue);
    
                    var formId = form.attr('id')+breakpointMinValue;
                    form.attr('id', formId);
                    form.find('.panel-title').addClass('pull-left').text(evaluateMessage()).css('padding', '2.5px 0 0 0');
                    form.find('.panel-heading').append(createRemoveButton(formId));
                    form.find('.panel-heading').attr('data-target', '#breakpoint-form'+ breakpointMinValue);
                    form.find('.panel-heading').attr('data-parent', '#breakpoints-forms');
                    form.find('.panel-heading').addClass('panel-breakpoint').addClass('clearfix');
                    form.find('> div').addClass('panel-breakpoint-primary');
                    form.find('.panel-collapse').attr('id', 'breakpoint-form'+ breakpointMinValue);
                    
                    $('#breakpoints-forms').append(form);
                    
                    showSubmitButton();
                }
            });
        }
    
        function renameFields(form, value) {
            var inputs = form.find('input');
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].setAttribute('name', 'breakpointValues'+value+'[]');
            }
        }
        
        this.reset = function() {
            $('#breakpoints-forms').empty();
            breakpointMinValue = undefined;
            breakpointMaxValue = undefined;
        };
    
        this.init = function() {
            configAddPoints();
        };
    }
    function SelectForm() {
    
        var breakPointsFunction = new Breakpoints();
        var namesContainersForms = [
            '#container-vertical-inner-sponsor',
            '#container-vertical-outer-sponsor',
            '#container-horizontal-inner-sponsor',
            '#container-horizontal-outer-sponsor'
        ];
        var modelDescription = $('#model-description > div > div');
        var containerAddBreakpoints = $('#container-add-breakpoints');
    
        function showModelDescription(index) {
            for (var i = 0; i < modelDescription.length; i++) {
                modelDescription[i].style.display = 'none';
            }
            modelDescription[index].style.display = 'block';
        }
    
        function showContainerAddBreakpoints() {
            if(!containerAddBreakpoints.is(':visible')) {
                containerAddBreakpoints.show();
            }       
        }
    
        function hideForm(name) {
            for (var i = 0; i < namesContainersForms.length; i++) {
                if (!validateName(namesContainersForms[i], name)) {
                    $(namesContainersForms[i]).hide();
                } else {
                    $(namesContainersForms[i]).show();
                }
            }
        }
    
        function validateName(formId, searchName) {
            var pattern = formId.replace('#container-', '').toString();
            pattern = new RegExp(pattern, 'g');
            return pattern.test(searchName);
        }
    
        function setCurrentForm(name) {
            for (var i = 0; i < namesContainersForms.length; i++) {
                if (validateName(namesContainersForms[i], name)) {
                    var currentForm = namesContainersForms[i].replace('container-', 'config-');
                    Data.getInstance().setCurrentForm(currentForm);
                }
            }
        }
    
        function showForm(name) {
            
            hideForm(name);
            setCurrentForm(name);
            breakPointsFunction.reset();
            
            switch(name) {
                case 'vertical-inner-sponsor':
                    $('#config-vertical-inner-collapse').collapse();
                    showModelDescription(0);
                break;
                case 'vertical-outer-sponsor':
                    $('#config-vertical-outer-collapse').collapse();
                    showModelDescription(1);
                break;
                case 'horizontal-inner-sponsor':
                    $('#config-horizontal-inner-collapse').collapse();
                    showModelDescription(2);
                break;
                case 'horizontal-outer-sponsor':
                    $('#config-horizontal-outer-collapse').collapse();
                    showModelDescription(3);
                break;
            }
        }
    
        this.init = function() {
            $('[data-toggle="tooltip"]').tooltip();
            $(".selectContainer input").on("click", function() {
                showForm($(".selectContainer input:checked").val());
                showContainerAddBreakpoints();
            });
        };
    }
    function SubmitForm() {
    		
    	function getValuesFromInputs() {
    		return $('#main-form-generator').serialize();
    	}
    
    	function submit(){
    		$.ajax({
    		  	url: "/native-generator",
    		  	type: "POST",
    		  	data: getValuesFromInputs(),
    		  	dataType: 'json'
    		}).done(function(data) {
    			Modal.getInstance().show({'title' : 'Info:', 'text': 'Formulário enviado como sucesso!'});
    		}).fail(function(error) {
        		Modal.getInstance().show({'title' : 'Info:', 'text': 'Erro ao enviar o formulário'});
      		});
    	}
    
    	this.init = function () {
    		$('#btn-submit .submit').on('click', function(e) {
    			submit();
    		});
    	};
    }

	function Main() {
    	var breakpoints = new Breakpoints();
    	var selectForm = new SelectForm();

    	this.init = function() {
		    breakpoints.init();
		    selectForm.init();
    	};

    	return this;
    }

    Main().init();

});